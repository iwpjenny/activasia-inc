<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['client_name'] = 'ActivAsia Inc.';
$config['client_acronym'] = 'ActivAsia Inc.';
$config['client_website'] = 'http://www.activasiainc.com';
/* End of file config.php */
/* Location: ./application/config/config_client.php */