<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* BootStrap Notification Type: default, primary, success, info, warning, danger */

$config['login']['success']['success'] = 'Login User successful.';
$config['login']['error']['danger'] = 'Login error, check your Username or Password and try again.';
$config['login']['not-exist']['danger'] = 'Login error, account may have not existed or have been deactivated or blocked.';

$config['logout']['success']['success'] = 'Logout User successful.';

$config['user']['can-not-delete']['warning'] = 'You can not delete Delevoper or Admin account.';
$config['username']['taken']['warning'] = 'Username already exist or taken by other User.';

$config['email']['not-found']['danger'] = 'Email not found. Please try again.';
$config['email']['block']['danger'] = 'Email account  has been Blocked or Unpublished. Please contact our Support.';
$config['email']['error']['danger'] = 'Sending email Error.';
$config['email']['taken']['warning'] = 'Email already exist or taken by other User.';

$config['password']['request-success']['success'] = 'Request for new password successful.';
$config['password']['request-error']['danger']  = 'Request for new password unsuccessful.';
$config['password']['change-success']['success'] = 'Changed password successful.';
$config['password']['change-error']['danger'] = 'Changed password unsuccessful.';
$config['password']['did-not-match']['warning'] = 'Password and Confirm Password did not match.';

$config['form-security']['did-not-match']['danger'] = 'Security key did not match.';

$config['profile']['add-success']['success'] = 'Adding Profile successful.';
$config['profile']['add-error']['danger'] = 'Adding Profile unsuccessful.';
$config['profile']['update-success']['success'] = 'Update Profile successful.';
$config['profile']['update-error']['danger'] = 'Update Profile unsuccessful.';

$config['add']['success']['success'] = 'Adding new record(s) successful.';
$config['add']['error']['danger'] = 'Adding new record(s) unsuccessful.';
$config['update']['success']['success'] = 'Updating record(s) successful.';
$config['update']['error']['danger'] = 'Updating record(s) unsuccessful.';
$config['delete']['success']['success'] = 'Deleting record(s) successful.';
$config['delete']['error']['danger'] = 'Deleting record(s) unsuccessful.';

$config['record']['not-exist']['warning'] = 'Deleting record(s) unsuccessful.';
$config['save']['required']['warning'] = 'Field must have value.';
$config['save']['numeric']['warning'] = 'Field value must be in numeric format.';
$config['save']['email']['warning'] = 'Field value must be in email format.';

$config['publish']['success']['success'] = 'Publishing of record(s) successful.';
$config['publish']['error']['danger'] = 'Publishing of record(s) unsuccessful.';
$config['unpublish']['success']['success'] = 'Unpublishing of record(s) successful.';
$config['unpublish']['error']['danger'] = 'Unpublishing of record(s) unsuccessful.';

$config['trash']['success']['success'] = 'Marking record(s) as Trash successful.';
$config['trash']['error']['danger'] = 'Marking record(s) as Trash unsuccessful.';
$config['untrash']['success']['success'] = 'Marking record(s) as Un-trash successful.';
$config['untrash']['error']['danger'] = 'Marking record(s) as Un-trash unsuccessful.';

$config['file']['upload-success']['success'] = 'Uploading of file(s) successful.';
$config['file']['upload-error']['danger'] = 'Uploading of file(s) unsuccessful.';
$config['file']['delete-success']['success'] = 'Deleting of file(s) successful.';
$config['file']['delete-error']['danger'] = 'Deleting of file(s) unsuccessful.';
$config['file']['update-success']['success'] = 'Updating of file(s) successful.';
$config['file']['update-error']['danger'] = 'Updating of file(s) unsuccessful.';

$config['picture']['upload-success']['success'] = 'Uploading of file(s) successful.';
$config['picture']['upload-error']['danger'] = 'Uploading of file(s) unsuccessful.';

$config['backup']['success']['success'] = 'Making Backup of file(s) successful.';
$config['backup']['error']['danger'] = 'Making Backup of file(s) unsuccessful.';

$config['import']['success']['success'] = 'File import successful.';
$config['import']['error']['danger'] = 'File import unsuccessful.';

$config['upload']['success']['success'] = 'File upload successful.';
$config['upload']['error']['danger'] = 'File upload unsuccessful.';

$config['export']['success']['success'] = 'File export successful.';
$config['export']['error']['danger'] = 'File export unsuccessful.';

/* End of file config.php */
/* Location: ./application/config/config_notifications.php */