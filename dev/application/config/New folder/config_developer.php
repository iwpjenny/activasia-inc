<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['dev_name'] = 'Innovative Web Provider (iWP)';
$config['dev_description'] = 'Innovative Web Provider (iWP)';
$config['dev_website'] = 'http://iwp.ph';
$config['dev_copyright'] = 'Copyright &copy; '.date('Y').' Web App Framework, All Rights Reserved';
$config['dev_developed'] = 'Developed by: <a href="http://iwp.ph" target="_blank">Innovative Web Provider (iWP)</a> | <a href="index.php?c=FLog">Member Login</a>';
$config['dev_mode_urc'] = FALSE;
$config['dev_admin_access_all'] = array();
$config['dev_app_name'] = 'Record Management System';
$config['dev_app_acronym'] = 'RMS';

//$config['dev_admin_access_all'] = array('iwpadmin','developer');
/* End of file config.php */
/* Location: ./application/config/dev_developer.php */