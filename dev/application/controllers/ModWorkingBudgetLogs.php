<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModWorkingBudgetLogs extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;
	private $login_user_type;
	private $dashboard_page;

	public function index(){		
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,'method'=>array('library'=>array('ModUser')));
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/ 
		$data = $this->module->get_data_records( $data ); 
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	private function set_controller( $params ){  
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->load->model('modules/Working_Budget_Logs_Model', 'WBL');
		/*---------------------------------------------*/
		$this->model = $this->WBL;
		$this->page = $this->WBL->page;
		$this->view = $this->WBL->view;
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		/*---------------------------------------------*/
		$this->model->ini_custom_models();
		/*---------------------------------------------*/
		/*Files*/
		/*---------------------------------------------*/
		$this->load->library('Module');
		$this->module->model = $this->model; 
		$this->module->model_user = $this->User;
		$this->module->location = $this->location;	
		/*---------------------------------------------*/
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		/*---------------------------------------------*/
		$this->load->library('RecordsFilter',array('model'=>$this->model));

		return $data;
	}

	public function view(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->module->get_data_view( $data ); 
		$data['items'] = $this->WBIL->get_items_list( $data['id'] );  

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function edit(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('member_models','helper'=>array('fields'),'library'=>array('files'))
		);
		$data = $this->set_controller( $controller_params );
		$data = $this->module->get_data_edit( $data );

		$fields = $data['fields'];    
		$id = $data['id'];    
		/*---------------------------------------------*/ 
		$data['select_region'] = $this->Reg->get_select( $this->model, $fields );
	 
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'add');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		//$this->modulelogs->create();
		/* $params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('modulelogs',$params);
		$this->modulelogs->create(); */
		$this->module->save();
	}

	public function delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		//$this->modulelogs->create();
		/* $params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('modulelogs',$params);
		$this->modulelogs->create(); */
		$this->module->delete();
	}

	public function trashed(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/			
		
		$data['form_action'] = site_url('c='.$this->page.'&m='.__FUNCTION__);
		/*---------------------------------------------*/

		$data = $this->module->records( $this->model, $data, array($this->model->tblpref.'trashed'=>1) );

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function trash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,'method'=>array('library'=>array('TrashItems')));
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/ 
		/* $id = $this->input->get('id'); 
		$this->trashitems->trash($this->model,$id);  */
		/* $params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('modulelogs',$params);
		$this->modulelogs->create(); */
		$this->module->trash();
	}

	public function untrash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		/*   */
		$this->module->untrash();
	}
 
}
