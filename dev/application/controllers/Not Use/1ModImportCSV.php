<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModImportCsv extends CI_Controller {

	private $loggedin = NULL;
	
	public function index(){
		
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>'records',
			'method'=>array('library'=>array('files'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/ 
		$data['form_action'] = site_url('c='.$this->model->page.'&m=upload_csv');
		/*---------------------------------------------*/ 
		$data = $this->module->records( $this->model, $data, array( $this->model->tblpref.'module_type' => 'ce_records' ) ); 
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/ce-record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	 
	private function set_controller( $params ){
		
		$this->backend->set();
		$this->frontend->set();
		$this->load->model('modules/Import_CSV_Model', 'IC');
		/*---------------------------------------------*/
		
		$this->model = $this->IC;
		$this->page = $this->IC->page;
		$this->view = $this->IC->view;
		
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		$this->model->ini_custom_models();
		/*---------------------------------------------*/
		$this->module->model = $this->model;
		$this->module->location = $this->location;
		/*---------------------------------------------*/
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/	
		$data = $this->method->set_controller( $params );
		
		return $data;
		
	}
	
	public function working_budget(){
		
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>'records');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/  
		$data['form_action'] = site_url('c='.$this->model->page.'&m=upload_csv');
		/*---------------------------------------------*/
		
		$data = $this->module->records( $this->model, $data, array( $this->model->tblpref.'module_type' => 'working_budget' ) ); 
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/wb-record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	public function upload_csv(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$this->IC->upload_csv();
		/* redirect( site_url('c='.$this->CE->page) );	 */
		 
	}
	
	public function do_upload()
	{ 
	
		/* $config['upload_path']          = 'uploads/csv/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 100;
		$config['max_width']            = 1024;
		$config['max_height']           = 768;
		echo substr(sprintf('%o', fileperms('uploads/csv')), -4);
		$this->load->library('upload', $config);
		$this->upload->do_upload('userfile');
		if ( ! $this->upload->do_upload('userfile'))
		{
				$error = array('error' => $this->upload->display_errors()); 
				printx($error);
		}
		else
		{
				printx('Success');
		} */
		
		$CI->load->library('files');
		$CI->files->id = $id;
		$CI->files->model_main = $model;
		$CI->files->model_files = $model_files;
		$CI->files->upload();
	}
	
	
}
