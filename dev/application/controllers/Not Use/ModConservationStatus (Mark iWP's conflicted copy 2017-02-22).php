<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ModConservationStatus extends CI_Controller {
	
	private $model;
	private $page;
	private $view;
	private $location;	
	private $login_user_type;
	private $dashboard_page;
			
	public function index(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>'records',
			'method'=>array('library'=>array('pic'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->module->records( $this->model, $data, array($this->model->tblpref.'trashed'=>0) );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	private function set_controller( $params ){		
		$this->backend->set();
		$this->frontend->set();
		$this->load->model('modules/Conservation_Status_Model', 'CS'); 
		/*---------------------------------------------*/
		
		$this->model = $this->CS;
		$this->page = $this->CS->page;
		$this->view = $this->CS->view;
		
		$this->location = $this->BLog->location;
		$this->login_user_type = $this->BLog->login_user_type;
		$this->dashboard_page = $this->BLog->dashboard_page;
		$this->module->location = $this->location;
		
		$this->model->ini_custom_models();
		$this->frontend->set_libaries( $this->model );
		/*---------------------------------------------*/	
		$data = $this->method->set_controller( $params );
		
		return $data;
	}
	
	public function view(){	
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'page','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('helper'=>array('fields'),'library'=>array('pic'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data['form_action'] = site_url('c='.$this->page.'&m=save');
		/*---------------------------------------------*/
		$id = $this->input->get('id');
		$data['id'] = $id;
		/*---------------------------------------------*/  
		$fields = $this->module->record( $this->model, $id );
		$this->moduletag->model = $this->model;
		$this->moduletag->data = $fields;
		$data['fields'] = $fields; 
		
		$this->userslogs->save( 'view', 'Viewed Processed Province ('.$id.')' );
		
		$latitude =  get_value( $fields, $this->model->tblpref.'latitude' );
		$longitude =  get_value( $fields, $this->model->tblpref.'longitude' ); 
		$data['map'] = $this->googlemap->generate_map( $latitude, $longitude ); 
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	public function edit(){	
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('member_models','helper'=>array('fields'),'library'=>array('files')));
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data['form_action'] = site_url('c='.$this->page.'&m=save');
		/*---------------------------------------------*/
		$id = $this->input->get('id');
		$data['id'] = $id;
		/*---------------------------------------------*/ 	
		$fields = $this->module->record( $this->model, $id );
		$this->moduletag->model = $this->model;
		$this->moduletag->data = $fields;
		$data['fields'] = $fields; 
		//$data['select_member'] = $this->Mem->get_select( $this->model, $fields );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	 
	public function save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'add',
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->save( $this->model );
	}
	
	public function delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->delete( $this->model );
	}
	
	public function trashed(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>'records',
			'method'=>array('library'=>array('pic'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data['form_action'] = site_url('c='.$this->page.'&m='.__FUNCTION__);
		/*---------------------------------------------*/

		$data = $this->module->records( $this->model, $data, array($this->model->tblpref.'trashed'=>1) );

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function trash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'trash',
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/

		$this->module->trash( $this->model );
	}

	public function untrash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'trash',
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/

		$this->module->trash( $this->model, 0 );
	}

}