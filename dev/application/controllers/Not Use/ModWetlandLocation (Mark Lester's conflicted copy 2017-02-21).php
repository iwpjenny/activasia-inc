<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ModWetlandLocation extends CI_Controller {
	
	private $model;
	private $page;
	private $view;
	private $location;	
	private $login_user_type;
	private $dashboard_page;
			
	public function index(){ 
		/*standard init in every function---------------------*/
		$this->set_controller();
		$this->method->set(array('library'=>array('pic')));
		$this->logged->check_login_permission();
		$this->usersrole->has_user_has_capability($this->model->urc_name,'records');
		/*page function init----------------------------------*/
		$data = $this->method->set_data_page();
		/*logged user function init---------------------------*/
		$data = $this->method->set_backend_logged_page( $data ); 
		$data = $this->method->set_backend_logged_page_user( $data );
		/*set end---------------------------------------------*/
		
		$data = $this->module->records( $this->model, $data, array($this->model->tblpref.'trashed'=>0) );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	private function set_controller( $params ){		
		$this->backend->set();
		$this->frontend->set();
		$this->load->model('modules/Wetland_Location_Model', 'WL'); 
		/*---------------------------------------------*/
		$this->model = $this->WL;
		$this->page = $this->WL->page;
		$this->view = $this->WL->view;
		
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		$this->module->location = $this->location;
		
		$this->model->ini_custom_models();
		$this->backend->set_libaries( $this->model );
		
		/* Initialize Custom Model here */
		$this->load->model('modules/Waterbird_Species_Model', 'WS');
		$this->load->model('modules/Wetland_Location_Files_Model', 'WLF');
		$this->load->model('modules/City_Model', 'City');
		$this->load->model('modules/Province_Model', 'Prov');
		$this->load->library('GoogleMap');
		/* End of initialize custom model here */
	}
	
	public function view(){	
		/*standard init in every function---------------------*/
		$this->set_controller(); 
		$this->method->set(array('helper'=>array('fields'),'library'=>array('pic')));
		$this->logged->check_login_permission();
		$this->usersrole->has_user_has_capability($this->model->urc_name,__FUNCTION__);
		/*page function init----------------------------------*/
		$data = $this->method->set_data_page();
		/*logged user function init---------------------------*/
		$data = $this->method->set_backend_logged_page( $data ); 
		$data = $this->method->set_backend_logged_page_user( $data );
		/*set end---------------------------------------------*/
		
		$data['form_action'] = site_url('c='.$this->page.'&m=save');
		/*---------------------------------------------*/
		$id = $this->input->get('id');
		$data['id'] = $id;
		/*---------------------------------------------*/  
		$fields = $this->module->record( $this->model, $id );
		$this->moduletag->model = $this->model;
		$this->moduletag->data = $fields;
		$data['fields'] = $fields; 
		
		$this->userslogs->save( 'view', 'Viewed Processed Province ('.$id.')' );
		
		$latitude =  get_value( $fields, $this->model->tblpref.'latitude' );
		$longitude =  get_value( $fields, $this->model->tblpref.'longitude' ); 
		$data['map'] = $this->googlemap->generate_map( $latitude, $longitude ); 
 		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
		
	}
	
	public function edit(){	
		/*standard init in every function---------------------*/
		$this->set_controller();
		$this->method->set(array('library'=>array('pic','files')));
		$this->logged->check_login_permission();
		$this->usersrole->has_user_has_capability($this->model->urc_name,__FUNCTION__);
		/*page function init----------------------------------*/
		$data = $this->method->set_data_page();
		/*logged user function init---------------------------*/
		$data = $this->method->set_backend_logged_page( $data ); 
		$data = $this->method->set_backend_logged_page_user( $data );
		/*set end---------------------------------------------*/
		
		$data['form_action'] = site_url('c='.$this->page.'&m=save');
		/*---------------------------------------------*/
		$id = $this->input->get('id');
		$data['id'] = $id;
		/*---------------------------------------------*/ 	
		$fields = $this->module->record( $this->model, $id );
		$this->moduletag->model = $this->model;
		$this->moduletag->data = $fields;
		$data['fields'] = $fields; 
		
		$data['prov_id'] = get_value( $fields, $this->Prov->tblid );
		$data['ct_id'] = get_value( $fields, $this->City->tblid );
		$data['select_city'] = $this->City->get_select( $this->model, $fields );
		$data['select_province'] = $this->Prov->get_select( $this->model, $fields );
		
		$survey_type = app_get_val($fields,$this->WL->tblpref.'survey_type');
		$data['select_survey_type'] = $this->model->get_select_survey_type( $survey_type );
		$condition_wetland = app_get_val($fields,$this->WL->tblpref.'condition_wetland');
		$data['select_condition_wetland'] = $this->model->get_select_condition_wetland( $condition_wetland );
		$coverage = app_get_val($fields,$this->WL->tblpref.'coverage');
		$data['select_coverage'] = $this->model->get_select_coverage( $coverage ); 
		$wetland_type = app_get_val($fields,$this->WL->tblpref.'wetland_type');
		$data['select_wetland_type'] = $this->model->get_select_wetland_type( $wetland_type );
		$threats = app_get_val($fields,$this->WL->tblpref.'threats') ;
		$data['select_threats'] = $this->model->get_select_threats( $threats );
		$uses = app_get_val($fields,$this->WL->tblpref.'uses');
		$data['select_uses'] = $this->model->get_select_uses( $uses );
		$source = app_get_val($fields,$this->WL->tblpref.'source');
		$data['select_source'] = $this->model->get_select_source( $source );
		$salinity = app_get_val($fields,$this->WL->tblpref.'salinity');
		$data['select_salinity'] = $this->model->get_select_salinity( $salinity );
		$site = app_get_val($fields,$this->WL->tblpref.'site'); 
		$data['select_site'] = $this->model->get_select_site( $site );
		$vegetation_cover = app_get_val($fields,$this->WL->tblpref.'vegetation_cover');
		$data['select_vegetation_cover'] = $this->model->get_select_vegetation_cover( $vegetation_cover );
		$vegetation_type = app_get_val($fields,$this->WL->tblpref.'vegetation_type') ;
		$data['select_vegetation_type'] = $this->model->get_select_vegetation_type( $vegetation_type );
		$protection = app_get_val($fields,$this->WL->tblpref.'protection');
		$data['select_protection'] = $this->model->get_select_protection( $protection );
		$fishing = app_get_val($fields,$this->WL->tblpref.'fishing');
		$data['select_fishing'] = $this->model->get_select_fishing( $fishing );
		$hunting = app_get_val($fields,$this->WL->tblpref.'hunting');
		$data['select_hunting'] = $this->model->get_select_hunting( $hunting );
		$agriculture = app_get_val($fields,$this->WL->tblpref.'agriculture');
		$data['select_agriculture'] = $this->model->get_select_agriculture( $agriculture );
		$cattle_grazing = app_get_val($fields,$this->WL->tblpref.'cattle_grazing');
		$data['select_cattle_grazing'] = $this->model->get_select_cattle_grazing( $cattle_grazing );
		$threats_uses = app_get_val($fields,$this->WL->tblpref.'threats_uses');
		$data['select_threats_uses'] = $this->model->get_select_threats_uses( $threats_uses );
		$pollution = app_get_val($fields,$this->WL->tblpref.'pollution'); 
		$data['select_pollution'] = $this->model->get_select_pollution( $pollution );
		$active_conservation = app_get_val($fields,$this->WL->tblpref.'active_conservation');
		$data['select_active_conservation'] = $this->model->get_select_active_conservation( $active_conservation );
		$tidal_variation = app_get_val($fields,$this->WL->tblpref.'tidal_variation');
		$data['select_tidal_variation'] = $this->model->get_select_tidal_variation( $tidal_variation );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
		
		
	}
	 
	public function save(){
		/*standard init in every function---------------------*/
		$this->set_controller();
		$this->method->set(array('library'=>array('users')));
		$this->logged->check_login_permission();
		$this->usersrole->has_user_has_capability($this->model->urc_name,'add');
		/*function init----------------------------------*/
		$data = $this->method->set_data_function();
		/*set end---------------------------------------------*/
		
		$this->module->save( $this->model );
	}
	
	public function delete(){
		/*standard init in every function---------------------*/
		$this->set_controller();
		$this->method->set(array('library'=>array('users')));
		$this->logged->check_login_permission();
		$this->usersrole->has_user_has_capability($this->model->urc_name,__FUNCTION__);
		/*function init----------------------------------*/
		$data = $this->method->set_data_function();
		/*set end---------------------------------------------*/
		
		$this->module->delete( $this->model );
	}
	
	public function trashed(){
		/*standard init in every function---------------------*/
		$this->set_controller();
		$this->method->set(array('library'=>array('pic')));
		$this->logged->check_login_permission();
		$this->usersrole->has_user_has_capability($this->model->urc_name,'records');
		/*page function init----------------------------------*/
		$data = $this->method->set_data_page();
		/*logged user function init---------------------------*/
		$data = $this->method->set_backend_logged_page( $data ); 
		$data = $this->method->set_backend_logged_page_user( $data );
		/*set end---------------------------------------------*/ 
		
		$data['form_action'] = site_url('c='.$this->page.'&m='.__FUNCTION__);
		/*---------------------------------------------*/

		$data = $this->module->records( $this->model, $data, array($this->model->tblpref.'trashed'=>1) );

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function trash(){
		/*standard init in every function---------------------*/
		$this->set_controller();
		$this->method->set(array('library'=>array('users')));
		$this->logged->check_login_permission();
		$this->usersrole->has_user_has_capability($this->model->urc_name,'trash');
		/*function init----------------------------------*/
		$data = $this->method->set_data_function();
		/*set end---------------------------------------------*/

		$this->module->trash( $this->model );
	}

	public function untrash(){
		/*standard init in every function---------------------*/
		$this->set_controller();
		$this->method->set(array('library'=>array('users')));
		$this->logged->check_login_permission();
		$this->usersrole->has_user_has_capability($this->model->urc_name,'trash');
		/*function init----------------------------------*/
		$data = $this->method->set_data_function();
		/*set end---------------------------------------------*/

		$this->module->trash( $this->model, 0 );
	}
	
	public function generate_pdftwo(){
		/*standard init in every function---------------------*/
		$this->set_controller();
		$this->method->set(array('library'=>array('users')));
		$this->logged->check_login_permission();
		$this->usersrole->has_user_has_capability($this->WL->urc_name,'add'); 
		/*set end---------------------------------------------*/
		$this->load->library('PDF');
		/*---------------------------------------------*/
		$id = $this->input->get('id');
		$data['id'] = $id;
		/*---------------------------------------------*/
		$record = $this->WL->record( $id );
		
		$data['title'] = get_value( $record, $this->WL->tblpref.'title');
		$data['description'] = get_value( $record, $this->WL->tblpref.'description');
		$data['date'] = date("jS F, Y", strtotime( get_value( $record, $this->WL->tblpref.'date') ));
		$data['altitude'] =  get_value( $record, $this->WL->tblpref.'altitude');
		$data['site_counted'] =  get_value( $record, $this->WL->tblpref.'site_counted');
		$data['site_code'] = get_value( $record, $this->WL->tblpref.'site_code');
		$data['survey_type'] = get_value( $record, $this->WL->tblpref.'survey_type');
		$data['coverage'] = get_value( $record, $this->WL->tblpref.'coverage');
		$data['participants'] = get_value( $record, $this->WL->tblpref.'participants');	
		$data['condition_wetland'] = get_value( $record, $this->WL->tblpref.'condition_wetland');
		$data['threats'] = get_value( $record, $this->WL->tblpref.'threats');		
		$data['uses'] = get_value( $record, $this->WL->tblpref.'uses');		
		$data['protection'] = get_value( $record, $this->WL->tblpref.'protection');	
		$data['protect'] = get_value( $record, $this->WL->tblpref.'protect');	
		$data['time_in'] = get_value( $record, $this->WL->tblpref.'time_in');	
		$data['time_out'] = get_value( $record, $this->WL->tblpref.'time_out');	
		$data['latitude'] = get_value( $record, $this->WL->tblpref.'latitude');	
		$data['longitude'] = get_value( $record, $this->WL->tblpref.'longitude');
		$data['province'] = get_value( $record, $this->Prov->tblpref.'title');
		$data['city'] = get_value( $record, $this->City->tblpref.'title'); 
		 
		$survey_type_serialize = get_value( $record, $this->model->tblpref.'survey_type');
		$survey_type_array = unserialize($survey_type_serialize);
		// Survey Type
		if (in_array("A", $survey_type_array)) {
			$data['survey_type_A'] = '<div class="numberCircle">A</div>'; 
		} else {
			$data['survey_type_A'] = 'A'; 
		} 
		if (in_array("F", $survey_type_array)) {
			$data['survey_type_F'] = '<div class="numberCircle">F</div>'; 
		} else {
			$data['survey_type_F'] = 'F'; 
		} 
		if (in_array("B", $survey_type_array)) {
			$data['survey_type_B'] = '<div class="numberCircle">B</div>'; 
		} else {
			$data['survey_type_B'] = 'B'; 
		} 
		if (in_array("M", $survey_type_array)) {
			$data['survey_type_M'] = '<div class="numberCircle">M</div>'; 
		} else {
			$data['survey_type_M'] = 'M'; 
		}
		if (in_array("A", $survey_type_array)) {
			$data['survey_type_A'] = '<div class="numberCircle">A</div>'; 
		} else {
			$data['survey_type_A'] = 'A'; 
		} 
		if (in_array("F", $survey_type_array)) {
			$data['survey_type_F'] = '<div class="numberCircle">F</div>'; 
		} else {
			$data['survey_type_F'] = 'F'; 
		} 
		if (in_array("B", $survey_type_array)) {
			$data['survey_type_B'] = '<div class="numberCircle">B</div>'; 
		} else {
			$data['survey_type_B'] = 'B'; 
		} 
		if (in_array("M", $survey_type_array)) {
			$data['survey_type_M'] = '<div class="numberCircle">M</div>'; 
		} else {
			$data['survey_type_M'] = 'M'; 
		}

		$coverage_serialize = get_value( $record, $this->model->tblpref.'coverage');
		$coverage_array = unserialize($coverage_serialize);
		// Coverage	
		if (in_array("V", $coverage_array)) {
			$data['coverage_V'] = '<div class="numberCircle">V</div>'; 
		} else {
			$data['coverage_V'] = 'V'; 
		} 
		if (in_array("W", $coverage_array)) {
			$data['coverage_W'] = '<div class="numberCircle">W</div>'; 
		} else {
			$data['coverage_W'] = 'W'; 
		} 
		if (in_array("X", $coverage_array)) {
			$data['coverage_X'] = '<div class="numberCircle">X</div>'; 
		} else {
			$data['coverage_X'] = 'X'; 
		} 
		if (in_array("Y", $coverage_array)) {
			$data['coverage_Y'] = '<div class="numberCircle">Y</div>'; 
		} else {
			$data['coverage_Y'] = 'Y'; 
		} 
		if (in_array("Z", $coverage_array)) {
			$data['coverage_Z'] = '<div class="numberCircle">Z</div>'; 
		} else {
			$data['coverage_Z'] = 'Z'; 
		} 
		
		$threats_serialize = get_value( $record, $this->model->tblpref.'threats');
		$threats_array = unserialize($threats_serialize);
		// Threats	
		if (in_array("0", $threats_array)) {
			$data['threats_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['threats_0'] = '0'; 
		} 
		if (in_array("1", $threats_array)) {
			$data['threats_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['threats_1'] = '1'; 
		} 
		if (in_array("2", $threats_array)) {
			$data['threats_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['threats_2'] = '2'; 
		} 
		if (in_array("3", $threats_array)) {
			$data['threats_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['threats_3'] = '3'; 
		} 
		if (in_array("4", $threats_array)) {
			$data['threats_4'] = '<div class="numberCircle">4</div>'; 
		} else {
			$data['threats_4'] = '4'; 
		} 
		if (in_array("5", $threats_array)) {
			$data['threats_5'] = '<div class="numberCircle">5</div>'; 
		} else {
			$data['threats_5'] = '5'; 
		} 
		if (in_array("6", $threats_array)) {
			$data['threats_6'] = '<div class="numberCircle">6</div>'; 
		} else {
			$data['threats_6'] = '6'; 
		} 
		if (in_array("7", $threats_array)) {
			$data['threats_7'] = '<div class="numberCircle">7</div>'; 
		} else {
			$data['threats_7'] = '7'; 
		} 
		if (in_array("8", $threats_array)) {
			$data['threats_8'] = '<div class="numberCircle">8</div>'; 
		} else {
			$data['threats_8'] = '8'; 
		} 
		if (in_array("9", $threats_array)) {
			$data['threats_9'] = '<div class="numberCircle">9</div>'; 
		} else {
			$data['threats_9'] = '9'; 
		} 
		
		$uses_serialize = get_value( $record, $this->model->tblpref.'uses');
		$uses_array = unserialize($uses_serialize);
		// Uses	
		if (in_array("A", $uses_array)) {
			$data['uses_A'] = '<div class="numberCircle">A</div>'; 
		} else {
			$data['uses_A'] = 'A'; 
		} 
		if (in_array("B", $uses_array)) {
			$data['uses_B'] = '<div class="numberCircle">B</div>'; 
		} else {
			$data['uses_B'] = 'B'; 
		} 
		if (in_array("C", $uses_array)) {
			$data['uses_C'] = '<div class="numberCircle">C</div>'; 
		} else {
			$data['uses_C'] = 'C'; 
		} 
		if (in_array("D", $uses_array)) {
			$data['uses_D'] = '<div class="numberCircle">D</div>'; 
		} else {
			$data['uses_D'] = 'D'; 
		} 
		if (in_array("E", $uses_array)) {
			$data['uses_E'] = '<div class="numberCircle">E</div>'; 
		} else {
			$data['uses_E'] = 'E'; 
		} 
		if (in_array("F", $uses_array)) {
			$data['uses_F'] = '<div class="numberCircle">F</div>'; 
		} else {
			$data['uses_F'] = 'F'; 
		} 
		if (in_array("G", $uses_array)) {
			$data['uses_G'] = '<div class="numberCircle">G</div>'; 
		} else {
			$data['uses_G'] = 'G'; 
		} 
		if (in_array("H", $uses_array)) {
			$data['uses_H'] = '<div class="numberCircle">H</div>'; 
		} else {
			$data['uses_H'] = 'H'; 
		} 
		if (in_array("I", $uses_array)) {
			$data['uses_I'] = '<div class="numberCircle">I</div>'; 
		} else {
			$data['uses_I'] = 'I'; 
		} 
		if (in_array("J", $uses_array)) {
			$data['uses_J'] = '<div class="numberCircle">J</div>'; 
		} else {
			$data['uses_J'] = 'J'; 
		} 
		if (in_array("K", $uses_array)) {
			$data['uses_K'] = '<div class="numberCircle">K</div>'; 
		} else {
			$data['uses_K'] = 'K'; 
		} 
		if (in_array("L", $uses_array)) {
			$data['uses_L'] = '<div class="numberCircle">L</div>'; 
		} else {
			$data['uses_L'] = 'L'; 
		} 
		
		$condition_wetland_serialize = get_value( $record, $this->model->tblpref.'condition_wetland');
		$condition_wetland_array = unserialize($condition_wetland_serialize);
		// condition-wetland
		if (in_array("1", $condition_wetland_array)) {
			$data['condition_wetland_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['condition_wetland_1'] = 'A'; 
		} 
		if (in_array("2", $condition_wetland_array)) {
			$data['condition_wetland_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['condition_wetland_2'] = '2'; 
		} 
		if (in_array("3", $condition_wetland_array)) {
			$data['condition_wetland_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['condition_wetland_3'] = '3'; 
		} 
		
		$data['yes_site'] = '<span style="border:1px solid black;padding:5px;">x</span>';
		$data['no_site'] = '';
		
		// Waterfall Counts
		$data['waterfall_counts_1']  = $this->WS->get_total_number_of_species( $id, 'Little Grebe Tachybaptus ruficollis' );
		$data['waterfall_counts_2']  = $this->WS->get_total_number_of_species( $id, 'Great Cormorant Phalecrocorax carbo' );
		$data['waterfall_counts_3']  = $this->WS->get_total_number_of_species( $id, 'Indian Shag P. fuscicollis' );
		$data['waterfall_counts_4']  = $this->WS->get_total_number_of_species( $id, 'Little Cormorant P. nige' );
		$data['waterfall_counts_5']  = $this->WS->get_total_number_of_species( $id, 'Unidentified cormorant' );
		$data['waterfall_counts_6']  = $this->WS->get_total_number_of_species( $id, 'Oriental Darter Anhinga melanogaster' );
		$data['waterfall_counts_7']  = $this->WS->get_total_number_of_species( $id, 'Great Bittern Botaurus stellaris' );
		$data['waterfall_counts_8']  = $this->WS->get_total_number_of_species( $id, 'Yellow Bittern Ixobrychus sinensis' );
		$data['waterfall_counts_9']  = $this->WS->get_total_number_of_species( $id, 'Schrenck’s Bittern I. eurhythmus' );
		$data['waterfall_counts_10']  = $this->WS->get_total_number_of_species( $id, 'Cinnamon Bittern /. cinnamomeus' );
		$data['waterfall_counts_11']  = $this->WS->get_total_number_of_species( $id, 'Black Bittern I. flavicollis' );
		$data['waterfall_counts_12']  = $this->WS->get_total_number_of_species( $id, 'Japanese Night Heron Gorsachius goisagi' );
		$data['waterfall_counts_13']  = $this->WS->get_total_number_of_species( $id, 'Malayan Night Heron (Tiger Bittern) G. melanolophus' );
		$data['waterfall_counts_14']  = $this->WS->get_total_number_of_species( $id, 'Black-crowned Night Heron Nycticorax nycticorax' );
		$data['waterfall_counts_15']  = $this->WS->get_total_number_of_species( $id, 'Rufous Night Heron N. caledonicus' );
		$data['waterfall_counts_16']  = $this->WS->get_total_number_of_species( $id, 'Indian Pond Heron Ardeola grayii' );
		$data['waterfall_counts_17']  = $this->WS->get_total_number_of_species( $id, 'Chinese Pond Heron A. bacchus' );
		$data['waterfall_counts_18']  = $this->WS->get_total_number_of_species( $id, 'Javan Pond Heron A. speciosa' );
		$data['waterfall_counts_19']  = $this->WS->get_total_number_of_species( $id, 'Cattle Egret Bubulcus ibis' );
		$data['waterfall_counts_20']  = $this->WS->get_total_number_of_species( $id, 'Striated (Little Green) Heron Butorides striatus' );
		$data['waterfall_counts_21']  = $this->WS->get_total_number_of_species( $id, 'Eastern Reef Egret Egretta sacra' );
		$data['waterfall_counts_22']  = $this->WS->get_total_number_of_species( $id, 'Chinese (Swinhoe’s) Egret E. eulophotes' );
		$data['waterfall_counts_23']  = $this->WS->get_total_number_of_species( $id, 'Little Egret E. garzetta' );
		$data['waterfall_counts_24']  = $this->WS->get_total_number_of_species( $id, 'Intermediate Egret E. intermedia' );
		$data['waterfall_counts_25']  = $this->WS->get_total_number_of_species( $id, 'Great Egret E. alba' );
		$data['waterfall_counts_26']  = $this->WS->get_total_number_of_species( $id, 'Purple Heron Ardea purpurea' );
		$data['waterfall_counts_27']  = $this->WS->get_total_number_of_species( $id, 'Grey Heron A. cinerea' );
		$data['waterfall_counts_28']  = $this->WS->get_total_number_of_species( $id, 'Unidentified herons and egrets' );
		$data['waterfall_counts_29']  = $this->WS->get_total_number_of_species( $id, 'Cattle Egret Bubulcus ibis' );
		$data['waterfall_counts_30']  = $this->WS->get_total_number_of_species( $id, 'Milky Stork Mycteria cinerea' );
		$data['waterfall_counts_31']  = $this->WS->get_total_number_of_species( $id, 'Painted Stork M. leucocephala' );
		$data['waterfall_counts_32']  = $this->WS->get_total_number_of_species( $id, 'Asian Openbill Anastomus oscitans' );
		$data['waterfall_counts_33']  = $this->WS->get_total_number_of_species( $id, 'Black Stork Ciconia nigra' );
		$data['waterfall_counts_34']  = $this->WS->get_total_number_of_species( $id, 'Wooly-necked Stork C. episcopus' );
		$data['waterfall_counts_35']  = $this->WS->get_total_number_of_species( $id, 'Storm’s Stork C. stormi' );
		$data['waterfall_counts_36']  = $this->WS->get_total_number_of_species( $id, 'Black-necked Stork Ephippiorhynchus asiaticus' );
		$data['waterfall_counts_37']  = $this->WS->get_total_number_of_species( $id, 'Lesser Adjutant Leptoptilos javanicus' );
		$data['waterfall_counts_38']  = $this->WS->get_total_number_of_species( $id, 'Greater Adjutant L. dubius' );
		$data['waterfall_counts_39']  = $this->WS->get_total_number_of_species( $id, 'Unidentified storks' );
		$data['waterfall_counts_40']  = $this->WS->get_total_number_of_species( $id, 'Black-headed (White) Ibis Threskiornis melanocephalus' );
		$data['waterfall_counts_41']  = $this->WS->get_total_number_of_species( $id, 'White-shouldered Ibis Pseudibis davisoni' );
		$data['waterfall_counts_42']  = $this->WS->get_total_number_of_species( $id, 'Giant Ibis Thaumatibis gigantea' );
		$data['waterfall_counts_43']  = $this->WS->get_total_number_of_species( $id, 'Glossy Ibis Plegadis falcinellus' );
		$data['waterfall_counts_44']  = $this->WS->get_total_number_of_species( $id, 'White Spoonbill Platalea leucorodia' );
		$data['waterfall_counts_45']  = $this->WS->get_total_number_of_species( $id, 'Black-faced Spoonbill P. minor' );	
		$data['waterfall_counts_46']  = $this->WS->get_total_number_of_species( $id, 'Unidentified Spoonbills' );
		/*--------------------------------------------------------------------*/
		$data['waterfall_counts_47']  = $this->WS->get_total_number_of_species( $id, 'Moorhen Gallinula chloropus' );
		$data['waterfall_counts_48']  = $this->WS->get_total_number_of_species( $id, 'Purple Swamphen Porphyrio porphyrio' );
		$data['waterfall_counts_49']  = $this->WS->get_total_number_of_species( $id, 'Common Coot Fulica atra' );
		$data['waterfall_counts_50']  = $this->WS->get_total_number_of_species( $id, 'Masked Finfoot Heliopais personata' );
		$data['waterfall_counts_51']  = $this->WS->get_total_number_of_species( $id, 'Comb-crested Jacana Irediparra gallinacea' );
		$data['waterfall_counts_52']  = $this->WS->get_total_number_of_species( $id, 'Pheasant-tailed Jacana Hydrophasianus chirurgus' );
		$data['waterfall_counts_53']  = $this->WS->get_total_number_of_species( $id, 'Bronze-winged Jacana Metopidius indicus' );
		$data['waterfall_counts_54']  = $this->WS->get_total_number_of_species( $id, 'Painted Snipe Rostratula benghalensis' );
		$data['waterfall_counts_55']  = $this->WS->get_total_number_of_species( $id, 'Crab Plover Dromas ardeola' );
		$data['waterfall_counts_56']  = $this->WS->get_total_number_of_species( $id, 'Black-winged Stilt Himantopus himantopus' );
		$data['waterfall_counts_57']  = $this->WS->get_total_number_of_species( $id, 'Australian (White-headed) Stilt H. leucocephalus' );
		$data['waterfall_counts_58']  = $this->WS->get_total_number_of_species( $id, 'Avocet Recurvirostra avosetta' );
		$data['waterfall_counts_59']  = $this->WS->get_total_number_of_species( $id, 'Great Thick-knee Esacus recurvirostris' );
		$data['waterfall_counts_60']  = $this->WS->get_total_number_of_species( $id, 'Beach Thick-knee E. magnirostris' );
		$data['waterfall_counts_61']  = $this->WS->get_total_number_of_species( $id, 'Oriental Pratincole Glareola maldivarum' );
		$data['waterfall_counts_62']  = $this->WS->get_total_number_of_species( $id, 'Little Pratincole G. lactea' );
		$data['waterfall_counts_63']  = $this->WS->get_total_number_of_species( $id, 'Northern Lapwing Vanellus vanellus' );
		$data['waterfall_counts_64']  = $this->WS->get_total_number_of_species( $id, 'River Lapwing V. duvaucelii' );
		$data['waterfall_counts_65']  = $this->WS->get_total_number_of_species( $id, 'Grey-headed Lapwing V. cinereus' );
		$data['waterfall_counts_66']  = $this->WS->get_total_number_of_species( $id, 'Red-wattled Lapwing V. indicus' );
		$data['waterfall_counts_67']  = $this->WS->get_total_number_of_species( $id, 'Pacific Golden Plover Pluvialis fulva' );
		$data['waterfall_counts_68']  = $this->WS->get_total_number_of_species( $id, 'Grey Plover P. squatarola' );
		$data['waterfall_counts_69']  = $this->WS->get_total_number_of_species( $id, 'Long-billed Plover Charadrius placidus' );
		$data['waterfall_counts_70']  = $this->WS->get_total_number_of_species( $id, 'Little Ringed Plover C. dubius' );
		$data['waterfall_counts_71']  = $this->WS->get_total_number_of_species( $id, 'Kentish Plover C. alexandrinus' );
		$data['waterfall_counts_72']  = $this->WS->get_total_number_of_species( $id, 'Malaysian Plover C. peronii' );
		$data['waterfall_counts_73']  = $this->WS->get_total_number_of_species( $id, 'Mongolian Plover C. mongolus' );
		$data['waterfall_counts_74']  = $this->WS->get_total_number_of_species( $id, 'Greater Sand Plover C. leschenaultii' );
		$data['waterfall_counts_75']  = $this->WS->get_total_number_of_species( $id, 'Oriental Plover C. veredus' );
		$data['waterfall_counts_76']  = $this->WS->get_total_number_of_species( $id, 'Black-tailed Godwit Limosa limosa' );
		$data['waterfall_counts_77']  = $this->WS->get_total_number_of_species( $id, 'Bar-tailed Godwit L. lapponica' );
		$data['waterfall_counts_78']  = $this->WS->get_total_number_of_species( $id, 'Little Curlew Numenius minutus' );
		$data['waterfall_counts_79']  = $this->WS->get_total_number_of_species( $id, 'Whimbrel N. phaeopus' );
		$data['waterfall_counts_80']  = $this->WS->get_total_number_of_species( $id, ' Eurasian Curlew N. arquata' );
		$data['waterfall_counts_81']  = $this->WS->get_total_number_of_species( $id, ' Far Eastern Curlew N. madagascariensis' );
		$data['waterfall_counts_82']  = $this->WS->get_total_number_of_species( $id, 'Spotted Redshank Tringa erythropus' );
		$data['waterfall_counts_83']  = $this->WS->get_total_number_of_species( $id, 'Redshank T. totanus' );
		$data['waterfall_counts_84']  = $this->WS->get_total_number_of_species( $id, 'Marsh Sandpiper T. stagnatilis' );
		$data['waterfall_counts_85']  = $this->WS->get_total_number_of_species( $id, 'Greenshank T. nebularia' );
		$data['waterfall_counts_86']  = $this->WS->get_total_number_of_species( $id, 'Nordmann’s Greenshank T. guttifer' );
		$data['waterfall_counts_87']  = $this->WS->get_total_number_of_species( $id, 'Green Sandpiper T. ochropus' );
		$data['waterfall_counts_88']  = $this->WS->get_total_number_of_species( $id, 'Wood Sandpiper T. glareola' );
		$data['waterfall_counts_89']  = $this->WS->get_total_number_of_species( $id, 'Terek Sandpiper Xenus cinereus' );
		$data['waterfall_counts_90']  = $this->WS->get_total_number_of_species( $id, 'Common Sandpiper Actitis hypoleucos' );
		/* ------------------------------------------------------ */
		$data['waterfall_counts_91']  = $this->WS->get_total_number_of_species( $id, 'Spotted Whistling Duck Dendrocygna guttata' );
		$data['waterfall_counts_92']  = $this->WS->get_total_number_of_species( $id, 'Fulvous (Large) Whistling Duck D. bicolor' );
		$data['waterfall_counts_93']  = $this->WS->get_total_number_of_species( $id, 'Wandering Whistling Duck D. arcuata' );
		$data['waterfall_counts_94']  = $this->WS->get_total_number_of_species( $id, 'Lesser Whistling Duck (Lesser Tree Duck) D. javanica' );
		$data['waterfall_counts_95']  = $this->WS->get_total_number_of_species( $id, 'Greylag Goose Anser anser' );
		$data['waterfall_counts_97']  = $this->WS->get_total_number_of_species( $id, 'Bar-headed Goose A. indicus' );
		$data['waterfall_counts_98']  = $this->WS->get_total_number_of_species( $id, 'Ruddy Shelduck Tadorna ferruginea' );
		$data['waterfall_counts_99']  = $this->WS->get_total_number_of_species( $id, 'Common Shelduck T. tadorna' );
		$data['waterfall_counts_100']  = $this->WS->get_total_number_of_species( $id, 'White-winged Wood Duck Cairina scutulata' );
		$data['waterfall_counts_101']  = $this->WS->get_total_number_of_species( $id, 'Comb Duck Sarkidiornis melanotos' );
		$data['waterfall_counts_102']  = $this->WS->get_total_number_of_species( $id, 'Indian Cotton Teal Nettapus coromandelianus' );
		$data['waterfall_counts_103']  = $this->WS->get_total_number_of_species( $id, 'Eurasian Wigeon Anas penelope' );
		$data['waterfall_counts_104']  = $this->WS->get_total_number_of_species( $id, 'Falcated Teal A. falcata' );
		$data['waterfall_counts_105']  = $this->WS->get_total_number_of_species( $id, 'Gadwall A. strepera' );
		$data['waterfall_counts_106']  = $this->WS->get_total_number_of_species( $id, 'Common (Green-winged) Teal A. crecca' );
		$data['waterfall_counts_107']  = $this->WS->get_total_number_of_species( $id, 'Grey Teal A. gibberifrons' );
		$data['waterfall_counts_108']  = $this->WS->get_total_number_of_species( $id, 'Mallard A. platyrhynchos' );
		$data['waterfall_counts_109']  = $this->WS->get_total_number_of_species( $id, 'Spot-billed Duck A. poecilorhyncha' );	
		$data['waterfall_counts_110']  = $this->WS->get_total_number_of_species( $id, 'Philippine Duck A. luzonica' );
		$data['waterfall_counts_111']  = $this->WS->get_total_number_of_species( $id, 'Northern Pintail A. acuta' );
		$data['waterfall_counts_112']  = $this->WS->get_total_number_of_species( $id, 'Garganey A. querquedula' );
		$data['waterfall_counts_113']  = $this->WS->get_total_number_of_species( $id, 'Northern Shoveler A. clypeata' );
		$data['waterfall_counts_114']  = $this->WS->get_total_number_of_species( $id, 'Red-crested Pochard Netta rufina' );
		$data['waterfall_counts_115']  = $this->WS->get_total_number_of_species( $id, 'Common Pochard Aythya ferina' );
		$data['waterfall_counts_116']  = $this->WS->get_total_number_of_species( $id, 'Baer’s Pochard A. baeri' );
		$data['waterfall_counts_117']  = $this->WS->get_total_number_of_species( $id, 'Ferruginous Duck A. nyroca' );
		$data['waterfall_counts_118']  = $this->WS->get_total_number_of_species( $id, 'Tufted Duck A. fuligula' );
		$data['waterfall_counts_119']  = $this->WS->get_total_number_of_species( $id, 'Goosander M. merganser' );
		$data['waterfall_counts_120']  = $this->WS->get_total_number_of_species( $id, 'Unidentified ducks' );
		$data['waterfall_counts_121']  = $this->WS->get_total_number_of_species( $id, 'Common Crane Grus grus' );
		$data['waterfall_counts_122']  = $this->WS->get_total_number_of_species( $id, 'Sarus Crane G. antigone' );
		$data['waterfall_counts_123']  = $this->WS->get_total_number_of_species( $id, 'Water Rail Rallus aquaticus' );
		$data['waterfall_counts_124']  = $this->WS->get_total_number_of_species( $id, 'Slaty-breasted Rail R. striatus' );
		$data['waterfall_counts_125']  = $this->WS->get_total_number_of_species( $id, 'Banded Rail R. philippensis' );
		$data['waterfall_counts_126']  = $this->WS->get_total_number_of_species( $id, 'Barred Rail R. torquatus' );
		$data['waterfall_counts_127']  = $this->WS->get_total_number_of_species( $id, 'Red-legged Crake Rallina fasciata' );
		$data['waterfall_counts_128']  = $this->WS->get_total_number_of_species( $id, 'Slaty-legged Crake R. eurizonoides' );
		$data['waterfall_counts_129']  = $this->WS->get_total_number_of_species( $id, 'Baillon’s Crake Porzana pusilia' );
		$data['waterfall_counts_130']  = $this->WS->get_total_number_of_species( $id, 'Ruddy Crake P. fusca' );
		$data['waterfall_counts_131']  = $this->WS->get_total_number_of_species( $id, 'Band-bellied Crake P. paykullii' );
		$data['waterfall_counts_132']  = $this->WS->get_total_number_of_species( $id, 'Spotless Crake P. tabuensis' );
		$data['waterfall_counts_133']  = $this->WS->get_total_number_of_species( $id, 'White-browed Crake P. Cinereus (Poliolimnas cinereus)' );
		$data['waterfall_counts_134']  = $this->WS->get_total_number_of_species( $id, 'Brown Crake Amaurornis akool' );
		$data['waterfall_counts_135']  = $this->WS->get_total_number_of_species( $id, 'Bush-Hen A. olivacea' );
		$data['waterfall_counts_136']  = $this->WS->get_total_number_of_species( $id, 'White-breasted Waterhen A. phoenicurus' );
		$data['waterfall_counts_137']  = $this->WS->get_total_number_of_species( $id, 'Watercock Gallicrex cinerea' );
		/* ------------------------------------------------------------------------ */
		$data['waterfall_counts_138']  = $this->WS->get_total_number_of_species( $id, 'Grey-tailed (Grey-rumped) Tattler Heteroscelus brevipes' );
		$data['waterfall_counts_139']  = $this->WS->get_total_number_of_species( $id, 'Ruddy Turnstone Arenaria interpres' );
		$data['waterfall_counts_140']  = $this->WS->get_total_number_of_species( $id, 'Red-necked Phalarope Phalaropus Iobatus' );
		$data['waterfall_counts_141']  = $this->WS->get_total_number_of_species( $id, 'Eurasian Woodcock Scolopax rusticola' );
		$data['waterfall_counts_142']  = $this->WS->get_total_number_of_species( $id, 'Pintail Snipe Gallinago stenura' );
		$data['waterfall_counts_143']  = $this->WS->get_total_number_of_species( $id, 'Swinhoe’s Snipe G. megala' );
		$data['waterfall_counts_144']  = $this->WS->get_total_number_of_species( $id, 'Common Snipe G. gallinago' );
		$data['waterfall_counts_145']  = $this->WS->get_total_number_of_species( $id, 'Asiatic Dowitcher Limnodromus semipalmatus' );
		$data['waterfall_counts_146']  = $this->WS->get_total_number_of_species( $id, 'Red Knot Calidris canutus' );
		$data['waterfall_counts_147']  = $this->WS->get_total_number_of_species( $id, 'Great Knot C. tenuirostris' );
		$data['waterfall_counts_148']  = $this->WS->get_total_number_of_species( $id, 'Sanderling C. alba' );
		$data['waterfall_counts_149']  = $this->WS->get_total_number_of_species( $id, 'Red-necked (Rufous-necked) Stint C. ruficollis' );
		$data['waterfall_counts_150']  = $this->WS->get_total_number_of_species( $id, 'Temminck’s Stint C. temminckii' );
		$data['waterfall_counts_151']  = $this->WS->get_total_number_of_species( $id, 'Long-toed Stint C. subminuta' );
		$data['waterfall_counts_152']  = $this->WS->get_total_number_of_species( $id, 'Sharp-tailed Sandpiper C. acuminata' );
		$data['waterfall_counts_153']  = $this->WS->get_total_number_of_species( $id, 'Dunlin C. alpina' );
		$data['waterfall_counts_154']  = $this->WS->get_total_number_of_species( $id, 'Curlew Sandpiper C. ferruginea' );
		$data['waterfall_counts_155']  = $this->WS->get_total_number_of_species( $id, 'Moorhen Gallinula chloropus' );
		$data['waterfall_counts_156']  = $this->WS->get_total_number_of_species( $id, 'Spoon-billed Sandpiper Eurynorhynchus pygmeus' );
		$data['waterfall_counts_157']  = $this->WS->get_total_number_of_species( $id, 'Broad-billed Sandpiper Limicola falcinellus' );
		$data['waterfall_counts_158']  = $this->WS->get_total_number_of_species( $id, 'Unidentified shorebirds' );
		$data['waterfall_counts_159']  = $this->WS->get_total_number_of_species( $id, 'Herring Gull Larus argentatus' );
		$data['waterfall_counts_160']  = $this->WS->get_total_number_of_species( $id, 'Brown-headed Gull L. brunnicephalus' );
		$data['waterfall_counts_161']  = $this->WS->get_total_number_of_species( $id, 'Black-headed Gull L. ridibundus' );
		$data['waterfall_counts_162']  = $this->WS->get_total_number_of_species( $id, 'Saunders’ Gull L. saundersi' );
		$data['waterfall_counts_163']  = $this->WS->get_total_number_of_species( $id, 'Unidentified gulls' );
		$data['waterfall_counts_164']  = $this->WS->get_total_number_of_species( $id, 'Whiskered Tern Chlidonias hybridus' );
		$data['waterfall_counts_165']  = $this->WS->get_total_number_of_species( $id, 'White-winged Black Tern C. leucopterus' );
		$data['waterfall_counts_166']  = $this->WS->get_total_number_of_species( $id, 'Gull-billed Tern Gelochelidon nilotica' );
		$data['waterfall_counts_167']  = $this->WS->get_total_number_of_species( $id, 'Avocet Recurvirostra avosetta' );
		$data['waterfall_counts_168']  = $this->WS->get_total_number_of_species( $id, 'Great Thick-knee Esacus recurvirostris' );
		$data['waterfall_counts_169']  = $this->WS->get_total_number_of_species( $id, 'Caspian Tern Hydroprogne caspia' );
		$data['waterfall_counts_170']  = $this->WS->get_total_number_of_species( $id, 'Indian River Tern Sterna aurantia' );
		$data['waterfall_counts_171']  = $this->WS->get_total_number_of_species( $id, 'Common Tern S. hirundo' );
		$data['waterfall_counts_172']  = $this->WS->get_total_number_of_species( $id, 'Black-naped Tern S. sumatrana' );
		$data['waterfall_counts_173']  = $this->WS->get_total_number_of_species( $id, 'Black-bellied Tern S. melanogaster' );
		$data['waterfall_counts_174']  = $this->WS->get_total_number_of_species( $id, 'Little Tern S. albifrons' );
		$data['waterfall_counts_175']  = $this->WS->get_total_number_of_species( $id, 'Great Crested Tern S. bergii' );
		$data['waterfall_counts_176']  = $this->WS->get_total_number_of_species( $id, 'Lesser Crested Tern S. bengalensis' );
		$data['waterfall_counts_177']  = $this->WS->get_total_number_of_species( $id, 'Unidentified terns' );
		$data['waterfall_counts_178']  = $this->WS->get_total_number_of_species( $id, 'Indian Skimmer Rynchops albicollis' );
		
		
		$data['waterbird_content'] = $this->WL->generate_pdftwo( $id );
		
		//printx( $wetland_location_content );
		$data_params =  array(); 
		$html =  $this->load->view( $this->location.'/modules/pdf-template/waterbird-species-pdf', $data, true ); 
		$time_stamp =  date('Y-m-d H-i-s');

		$params = array(
			'file_name'=>'Waterbird Species Site Form '.$time_stamp,
			'paper_orientation'=>'portrait',
		);
		//generate pdf for form 1
		$this->pdf->generate_pdf( $html, $params );
	}
	
	public function generate_waterfowl(){
		/*standard init in every function---------------------*/
		$this->set_controller();
		$this->method->set(array('library'=>array('users')));
		$this->logged->check_login_permission();
		$this->usersrole->has_user_has_capability($this->WL->urc_name,'add');
		/*set end---------------------------------------------*/
		$this->load->library('PDF');
		/*---------------------------------------------*/
		$id = $this->input->get('id');
		$data['id'] = $id;
		/*---------------------------------------------*/
		$record = $this->WL->record( $id );
		
		$data['title'] = get_value( $record, $this->WL->tblpref.'title');
		$data['description'] = get_value( $record, $this->WL->tblpref.'description');
		$data['date'] = date("jS F, Y", strtotime( get_value( $record, $this->WL->tblpref.'date') ));
		$data['altitude'] =  get_value( $record, $this->WL->tblpref.'altitude');
		$data['area'] =  get_value( $record, $this->WL->tblpref.'area');
		$data['site_code'] = get_value( $record, $this->WL->tblpref.'site_code');
		$data['survey_type'] = get_value( $record, $this->WL->tblpref.'survey_type');
		$data['wetland_type'] = get_value( $record, $this->WL->tblpref.'wetland_type');
		$data['source'] = get_value( $record, $this->WL->tblpref.'source');
		$data['agriculture'] = get_value( $record, $this->WL->tblpref.'agriculture');	
		$data['cattle_grazing'] = get_value( $record, $this->WL->tblpref.'cattle_grazing');	
		$data['fishing'] = get_value( $record, $this->WL->tblpref.'fishing');	
		$data['hunting'] = get_value( $record, $this->WL->tblpref.'hunting');	
		$data['pollution'] = get_value( $record, $this->WL->tblpref.'pollution');	
		$data['active_conservation'] = get_value( $record, $this->WL->tblpref.'active_conservation');	
		$data['maximum_depth'] = get_value( $record, $this->WL->tblpref.'maximum_depth');
		$data['maximum_flooding'] = get_value( $record, $this->WL->tblpref.'maximum_flooding');
		$data['threats_uses'] = get_value( $record, $this->WL->tblpref.'threats_uses');	
		$data['vegetation_cover'] = get_value( $record, $this->WL->tblpref.'vegetation_cover');	
		$data['vegetation_type'] = get_value( $record, $this->WL->tblpref.'vegetation_type');	
		$data['protection'] = get_value( $record, $this->WL->tblpref.'protection');	
		$data['coverage'] = get_value( $record, $this->WL->tblpref.'coverage');
		$data['site'] = get_value( $record, $this->WL->tblpref.'site');
		$data['tidal_variation'] = get_value( $record, $this->WL->tblpref.'tidal_variation');
		$data['annual_rain'] = get_value( $record, $this->WL->tblpref.'annual_rain');
		$data['latitude'] = get_value( $record, $this->WL->tblpref.'latitude');			
		$data['longitude'] = get_value( $record, $this->WL->tblpref.'longitude');
		$data['participants'] = get_value( $record, $this->WL->tblpref.'participants');		
		$data['province'] = get_value( $record, $this->Prov->tblpref.'title');
		$data['city'] = get_value( $record, $this->City->tblpref.'title');
		
		$wetland_type_serialize = get_value( $record, $this->model->tblpref.'wetland_type');
		$wetland_type_array = unserialize($wetland_type_serialize);
		
		if (in_array("0", $wetland_type_array)) {
			$data['wetland_type_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['wetland_type_0'] = '0'; 
		} 
		
		if (in_array("1", $wetland_type_array)) {
			$data['wetland_type_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['wetland_type_1'] = '1'; 
		} 
		if (in_array("2", $wetland_type_array)) {
			$data['wetland_type_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['wetland_type_2'] = '2'; 
		} 
		if (in_array("3", $wetland_type_array)) {
			$data['wetland_type_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['wetland_type_3'] = '3'; 
		} 
		if (in_array("4", $wetland_type_array)) {
			$data['wetland_type_4'] = '<div class="numberCircle">4</div>'; 
		} else {
			$data['wetland_type_4'] = '4'; 
		} 
		if (in_array("5", $wetland_type_array)) {
			$data['wetland_type_5'] = '<div class="numberCircle">5</div>'; 
		} else {
			$data['wetland_type_5'] = '5'; 
		} 
		if (in_array("6", $wetland_type_array)) {
			$data['wetland_type_6'] = '<div class="numberCircle">6</div>'; 
		} else {
			$data['wetland_type_6'] = '6'; 
		} 
		if (in_array("7", $wetland_type_array)) {
			$data['wetland_type_7'] = '<div class="numberCircle">7</div>'; 
		} else {
			$data['wetland_type_7'] = '7'; 
		} 
		if (in_array("8", $wetland_type_array)) {
			$data['wetland_type_8'] = '<div class="numberCircle">8</div>'; 
		} else {
			$data['wetland_type_8'] = '8'; 
		} 
		if (in_array("9", $wetland_type_array)) {
			$data['wetland_type_9'] = '<div class="numberCircle">9</div>'; 
		} else {
			$data['wetland_type_9'] = '9'; 
		} 
		if (in_array("10", $wetland_type_array)) {
			$data['wetland_type_10'] = '<div class="numberCircle">10</div>'; 
		} else {
			$data['wetland_type_10'] = '10'; 
		} 
		if (in_array("11", $wetland_type_array)) {
			$data['wetland_type_11'] = '<div class="numberCircle">11</div>'; 
		} else {
			$data['wetland_type_11'] = '11'; 
		} 
	
		$source_serialize = get_value( $record, $this->model->tblpref.'source');
		$source_array = unserialize($source_serialize);
		// sources
		if (in_array("0", $source_array)) {
			$data['source_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['source_0'] = '0'; 
		} 
		if (in_array("1", $source_array)) {
			$data['source_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['source_1'] = '1'; 
		} 
		if (in_array("2", $source_array)) {
			$data['source_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['source_2'] = '2'; 
		} 
		if (in_array("3", $source_array)) {
			$data['source_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['source_3'] = '3'; 
		} 
		if (in_array("4", $source_array)) {
			$data['source_4'] = '<div class="numberCircle">4</div>'; 
		} else {
			$data['source_4'] = '4'; 
		} 
		
		$salinity_serialize = get_value( $record, $this->model->tblpref.'salinity');
		$salinity_array = unserialize($salinity_serialize);
		 // salinity
		 if (in_array("0", $salinity_array)) {
			$data['salinity_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['salinity_0'] = '0'; 
		}
		if (in_array("1", $salinity_array)) {
			$data['salinity_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['salinity_1'] = '1'; 
		}
		if (in_array("2", $salinity_array)) {
			$data['salinity_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['salinity_2'] = '2'; 
		}
		if (in_array("3", $salinity_array)) {
			$data['salinity_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['salinity_3'] = '3'; 
		}
		
		$site_serialize = get_value( $record, $this->model->tblpref.'site');
		$site_array = unserialize($site_serialize);
		//Site
		if (in_array("0", $site_array)) {
			$data['site_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['site_0'] = '0'; 
		}
		if (in_array("1", $site_array)) {
			$data['site_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['site_1'] = '1'; 
		}
		if (in_array("2", $site_array)) {
			$data['site_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['site_2'] = '2'; 
		}
		if (in_array("3", $site_array)) {
			$data['site_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['site_3'] = '3'; 
		}

		$vegetation_cover_serialize = get_value( $record, $this->model->tblpref.'vegetation_cover');
		$vegetation_cover_array = unserialize($vegetation_cover_serialize);
		// Vegetation cover
		if (in_array("0", $vegetation_cover_array)) {
			$data['vegetation_cover_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['vegetation_cover_0'] = '0'; 
		}
		if (in_array("1", $vegetation_cover_array)) {
			$data['vegetation_cover_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['vegetation_cover_1'] = '1'; 
		}
		if (in_array("2", $vegetation_cover_array)) {
			$data['vegetation_cover_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['vegetation_cover_2'] = '1'; 
		}
		if (in_array("3", $vegetation_cover_array)) {
			$data['vegetation_cover_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['vegetation_cover_3'] = '1'; 
		}
		if (in_array("4", $vegetation_cover_array)) {
			$data['vegetation_cover_4'] = '<div class="numberCircle">4</div>'; 
		} else {
			$data['vegetation_cover_4'] = '4'; 
		}
		
		$vegetation_type_serialize = get_value( $record, $this->model->tblpref.'vegetation_type');
		$vegetation_type_array = unserialize($vegetation_type_serialize);
		// Vegetation cover
		if (in_array("1", $vegetation_type_array)) {
			$data['vegetation_type_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['vegetation_type_1'] = '1'; 
		}
		if (in_array("2", $vegetation_type_array)) {
			$data['vegetation_type_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['vegetation_type_2'] = '2'; 
		}
		if (in_array("3", $vegetation_type_array)) {
			$data['vegetation_type_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['vegetation_type_3'] = '3'; 
		}
		if (in_array("4", $vegetation_type_array)) {
			$data['vegetation_type_4'] = '<div class="numberCircle">4</div>'; 
		} else {
			$data['vegetation_type_4'] = '4'; 
		}
		if (in_array("5", $vegetation_type_array)) {
			$data['vegetation_type_5'] = '<div class="numberCircle">5</div>'; 
		} else {
			$data['vegetation_type_5'] = '4'; 
		}
		if (in_array("0", $vegetation_type_array)) {
			$data['vegetation_type_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['vegetation_type_0'] = '0'; 
		}
		
		$protection_serialize = get_value( $record, $this->model->tblpref.'protection');
		$protection_type_array = unserialize($protection_serialize);
		// Protection
		if (in_array("1", $protection_type_array)) {
			$data['protection_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['protection_1'] = '1'; 
		}
		if (in_array("2", $protection_type_array)) {
			$data['protection_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['protection_2'] = '2'; 
		}
		if (in_array("3", $protection_type_array)) {
			$data['protection_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['protection_3'] = '3'; 
		}
		if (in_array("4", $protection_type_array)) {
			$data['protection_4'] = '<div class="numberCircle">4</div>'; 
		} else {
			$data['protection_4'] = '4'; 
		}
		if (in_array("0", $protection_type_array)) {
			$data['protection_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['protection_0'] = '0'; 
		}
		
		$fishing_serialize = get_value( $record, $this->model->tblpref.'fishing');
		$fishing_array = unserialize($fishing_serialize);
		// Protection
		if (in_array("0", $fishing_array)) {
			$data['fishing_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['fishing_0'] = '0'; 
		}
		if (in_array("1", $fishing_array)) {
			$data['fishing_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['fishing_1'] = '1'; 
		}
		if (in_array("2", $fishing_array)) {
			$data['fishing_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['fishing_2'] = '2'; 
		}
		if (in_array("3", $fishing_array)) {
			$data['fishing_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['fishing_3'] = '3'; 
		}
		if (in_array("4", $fishing_array)) {
			$data['fishing_4'] = '<div class="numberCircle">4</div>'; 
		} else {
			$data['fishing_4'] = '4'; 
		}
		
		$hunting_serialize = get_value( $record, $this->model->tblpref.'hunting');
		$hunting_array = unserialize($hunting_serialize);
		// Hunting
		if (in_array("1", $hunting_array)) {
			$data['hunting_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['hunting_1'] = '1'; 
		}
		if (in_array("2", $hunting_array)) {
			$data['hunting_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['hunting_2'] = '2'; 
		}
		if (in_array("3", $hunting_array)) {
			$data['hunting_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['hunting_3'] = '3'; 
		}
		if (in_array("4", $hunting_array)) {
			$data['hunting_4'] = '<div class="numberCircle">4</div>'; 
		} else {
			$data['hunting_4'] = '4'; 
		}
		if (in_array("0", $hunting_array)) {
			$data['hunting_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['hunting_0'] = '0'; 
		}
		
		$agriculture_serialize = get_value( $record, $this->model->tblpref.'agriculture');
		$agriculture_array = unserialize($agriculture_serialize);
		// agriculture
		if (in_array("0", $agriculture_array)) {
			$data['agriculture_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['agriculture_0'] = '0'; 
		}
		if (in_array("1", $agriculture_array)) {
			$data['agriculture_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['agriculture_1'] = '1'; 
		}
		if (in_array("2", $agriculture_array)) {
			$data['agriculture_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['agriculture_2'] = '2'; 
		}
		if (in_array("3", $agriculture_array)) {
			$data['agriculture_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['agriculture_3'] = '3'; 
		}
		if (in_array("4", $agriculture_array)) {
			$data['agriculture_4'] = '<div class="numberCircle">4</div>'; 
		} else {
			$data['agriculture_4'] = '4'; 
		}
		
		$cattle_grazing_serialize = get_value( $record, $this->model->tblpref.'cattle_grazing');
		$cattle_grazing_array = unserialize($cattle_grazing_serialize);
		// cattle grazing
		if (in_array("1", $cattle_grazing_array)) {
			$data['cattle_grazing_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['cattle_grazing_1'] = '1'; 
		}
		if (in_array("2", $cattle_grazing_array)) {
			$data['cattle_grazing_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['cattle_grazing_2'] = '2'; 
		}
		if (in_array("3", $cattle_grazing_array)) {
			$data['cattle_grazing_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['cattle_grazing_3'] = '3'; 
		}
		if (in_array("4", $cattle_grazing_array)) {
			$data['cattle_grazing_4'] = '<div class="numberCircle">4</div>'; 
		} else {
			$data['cattle_grazing_4'] = '4'; 
		}
		if (in_array("0", $cattle_grazing_array)) {
			$data['cattle_grazing_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['cattle_grazing_0'] = '0'; 
		}
		
		$threats_uses_serialize = get_value( $record, $this->model->tblpref.'threats_uses');
		$threats_uses_array = unserialize($threats_uses_serialize);
		// threats_uses
		if (in_array("0", $threats_uses_array)) {
			$data['threats_uses_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['threats_uses_0'] = '0'; 
		}
		if (in_array("1", $threats_uses_array)) {
			$data['threats_uses_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['threats_uses_1'] = '1'; 
		}
		if (in_array("2", $threats_uses_array)) {
			$data['threats_uses_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['threats_uses_2'] = '2'; 
		}
		if (in_array("3", $threats_uses_array)) {
			$data['threats_uses_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['threats_uses_3'] = '3'; 
		}
		if (in_array("4", $threats_uses_array)) {
			$data['threats_uses_4'] = '<div class="numberCircle">4</div>'; 
		} else {
			$data['threats_uses_4'] = '4'; 
		}
		if (in_array("5", $threats_uses_array)) {
			$data['threats_uses_5'] = '<div class="numberCircle">5</div>'; 
		} else {
			$data['threats_uses_5'] = '5'; 
		}
		if (in_array("6", $threats_uses_array)) {
			$data['threats_uses_6'] = '<div class="numberCircle">6</div>'; 
		} else {
			$data['threats_uses_6'] = '6'; 
		}
		if (in_array("7", $threats_uses_array)) {
			$data['threats_uses_7'] = '<div class="numberCircle">7</div>'; 
		} else {
			$data['threats_uses_7'] = '7'; 
		}
		if (in_array("8", $threats_uses_array)) {
			$data['threats_uses_8'] = '<div class="numberCircle">8</div>'; 
		} else {
			$data['threats_uses_8'] = '8'; 
		}
		if (in_array("9", $threats_uses_array)) {
			$data['threats_uses_9'] = '<div class="numberCircle">9</div>'; 
		} else {
			$data['threats_uses_9'] = '9'; 
		}
		
		$pollution_serialize = get_value( $record, $this->model->tblpref.'pollution');
		$pollution_array = unserialize($pollution_serialize);
		// Pollution
		if (in_array("1", $pollution_array)) {
			$data['pollution_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['pollution_1'] = '1'; 
		}
		if (in_array("2", $pollution_array)) {
			$data['pollution_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['pollution_2'] = '2'; 
		}
		if (in_array("3", $pollution_array)) {
			$data['pollution_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['pollution_3'] = '3'; 
		}
		if (in_array("4", $pollution_array)) {
			$data['pollution_4'] = '<div class="numberCircle">4</div>'; 
		} else {
			$data['pollution_4'] = '3'; 
		}
		if (in_array("5", $pollution_array)) {
			$data['pollution_5'] = '<div class="numberCircle">5</div>'; 
		} else {
			$data['pollution_5'] = '5'; 
		}
		if (in_array("6", $pollution_array)) {
			$data['pollution_6'] = '<div class="numberCircle">6</div>'; 
		} else {
			$data['pollution_6'] = '6'; 
		}
		if (in_array("7", $pollution_array)) {
			$data['pollution_7'] = '<div class="numberCircle">7</div>'; 
		} else {
			$data['pollution_7'] = '7'; 
		}
		
		$active_conservation_serialize = get_value( $record, $this->model->tblpref.'active_conservation');
		$active_conservation_array = unserialize($active_conservation_serialize);
		// active conservation
		if (in_array("0", $active_conservation_array)) {
			$data['active_conservation_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['active_conservation_0'] = '0'; 
		}
		if (in_array("1", $active_conservation_array)) {
			$data['active_conservation_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['active_conservation_1'] = '1'; 
		}
		if (in_array("2", $active_conservation_array)) {
			$data['active_conservation_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['active_conservation_2'] = '2'; 
		}
		if (in_array("3", $active_conservation_array)) {
			$data['active_conservation_3'] = '<div class="numberCircle">3</div>'; 
		} else {
			$data['active_conservation_3'] = '3'; 
		}
		if (in_array("4", $active_conservation_array)) {
			$data['active_conservation_4'] = '<div class="numberCircle">4</div>'; 
		} else {
			$data['active_conservation_4'] = '4'; 
			
		}
		
		$tidal_variation_serialize = get_value( $record, $this->model->tblpref.'tidal_variation');
		$tidal_variation_array = unserialize($tidal_variation_serialize);
		// Tidal Variation
		if (in_array("1", $tidal_variation_array)) {
			$data['tidal_variation_1'] = '<div class="numberCircle">1</div>'; 
		} else {
			$data['tidal_variation_1'] = '1'; 
		}
		if (in_array("2", $tidal_variation_array)) {
			$data['tidal_variation_2'] = '<div class="numberCircle">2</div>'; 
		} else {
			$data['tidal_variation_2'] = '2'; 
		}
		if (in_array("0", $tidal_variation_array)) {
			$data['tidal_variation_0'] = '<div class="numberCircle">0</div>'; 
		} else {
			$data['tidal_variation_0'] = '0'; 
		}
				
		$data['wetland_location_content'] = $this->WL->generate_waterfowl( $id );
		
		//printx( $wetland_location_content );
		$data_params =  array(); 
		$html =  $this->load->view( $this->location.'/modules/pdf-template/waterbird-species-site-form-pdf', $data, true ); 
		$time_stamp =  date('Y-m-d H-i-s');

		$params = array(
			'file_name'=>'Waterbird Species Site Form '.$time_stamp,
			'paper_orientation'=>'portrait',
		);
		//generate pdf for form 1
		$this->pdf->generate_pdf( $html, $params );
	}
}