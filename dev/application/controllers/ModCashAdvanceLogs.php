<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModCashAdvanceLogs extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;
	private $login_user_type;
	private $dashboard_page;

	public function index(){		
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>'records','method'=>array('library'=>array('ModUser')));
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/ 
		$data = $this->module->records( $this->model, $data);	 
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	private function set_controller( $params ){ 
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->load->model('modules/Cash_Advance_Logs_Model', 'CAL');
		/*---------------------------------------------*/
		$this->model = $this->CAL;
		$this->page = $this->CAL->page;
		$this->view = $this->CAL->view;
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		/*---------------------------------------------*/
		$this->model->ini_custom_models();
		/*---------------------------------------------*/ 
		/*---------------------------------------------*/
		$this->load->library('module');
		$this->module->model = $this->model; 
		$this->module->model_user = $this->User;
		$this->module->location = $this->location;	
		/*---------------------------------------------*/
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		/*---------------------------------------------*/
		$this->load->library('RecordsFilter',array('model'=>$this->model));
		
		return $data;
	}

	public function view(){
		/*standard init in every function---------------------*/
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('ModUser'))
		); 
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->module->get_data_view( $data );
		$fields = $data['fields']; 
		$id = $data['id']; 
		if( $fields ){
			$wb_id = get_value( $fields, $this->WB->tblid );
			$balance = get_value( $fields, $this->WB->tblpref.'bcs');
			// $balance = $this->CA->get_current_ca_balance( $wb_id, $balance) ;
			// $ca_amount = $this->CAI->get_total_amount($id) ; 
			$ca_amount = $this->WB->get_total_amount($wb_id, $id ) ;
		} 
		$new_balance = $balance - $ca_amount;
		$data['ca_amount'] = $ca_amount; 
		$data['balance'] = $new_balance; 
		$wb_id =  get_value( $fields, $this->WB->tblid ); 
		$user_approved_id = get_value( $fields, $this->CA->tblpref.'user_approved'); 
		$operator_id = get_value( $fields, $this->CA->tblpref.'created_by_user_id');
		$data['operator_id'] = $operator_id;  
		$data['operator_fullname'] = $this->moduser->get_user_fullname_by_id( $operator_id, '{lastname}, {firstname}');
		$data['user_approved_id'] = $user_approved_id;
		$data['user_approved'] = $this->moduser->get_user_fullname_by_id( $user_approved_id, '{lastname}, {firstname}');
		$ca_status = get_value( $fields, $this->CA->tblpref.'status');
	 	$ca_details = $this->Data->get_record( $this->CAS, array( 'where_params' => array( $this->CA->tblid => $id ),'orderby'=>$this->CAS->tblpref.'created', 'ordertype'=>'DESC' ) ); 
		
		$user_complete_name = '';
		if ( $ca_details ){ 
			$ca_status_user_id = get_value( $ca_details, $this->User->tblid );
			$user_complete_name = $this->moduser->get_user_fullname_by_id( $ca_status_user_id, '{lastname}, {firstname}'); 
		} 
		 
		$ca_latest_status_user_activity = get_value( $ca_details, $this->CAS->tblpref.'status'); 
		$ca_latest_status_created = get_value( $fields, $this->CA->tblpref.'modified');
		$data['date_released'] = get_value( $fields, $this->CA->tblpref.'date_released'); 
		if( $ca_latest_status_user_activity ){
			$ca_latest_status_user_activity = $ca_latest_status_user_activity;
		} else {
			$ca_latest_status_user_activity = $ca_status;
		} 
		$user_pproved = get_value( $fields, $this->CA->tblpref.'user_approved');
		$data['user_pproved'] = $user_pproved;
		$data['ca_status_user_id'] = isset($ca_status_user_id)?$ca_status_user_id:'';  
		$data['ca_latest_status_created'] = isset($ca_latest_status_created)?$ca_latest_status_created:'';  
		$data['ca_latest_status_user'] = isset($user_complete_name)?$user_complete_name:'';  
		$data['ca_latest_status_user_activity'] = isset($ca_latest_status_user_activity)?$ca_latest_status_user_activity:'';
		$data['user_approved'] =  $this->moduser->get_user_fullname_by_id( $user_pproved, '{lastname},	 {firstname}');
		$user_id = get_value( $fields, $this->CA->tblpref.'created_by_user_id');   
		$status = get_value( $fields, $this->CA->tblpref.'status','Pending');
		$data['status'] = $status;
		$data['name_of_employee'] = $this->moduser->get_user_fullname_by_id( $user_id, '{lastname}, {firstname}' );
		$data['user_id1'] = $user_id;    
	    $user_details = $this->Data->get_record( $this->User, array(
				'where_params' => array( $this->User->tblid => $user_id ) 
		));  
		$data['user_details'] = $user_details;
		
		//items  
	 	$data['sub_section_1'] = $this->module->modal_sub_section_view( $this->CAIL, $data['id'] );

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function edit(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('member_models','helper'=>array('fields'),'library'=>array('files'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$data['form_action'] = site_url('c='.$this->page.'&m=save');
		/*---------------------------------------------*/
		$id = $this->input->get('id'); 
		$data['id'] = $id;
		/*---------------------------------------------*/

		$fields = $this->module->record( $this->model, $id ); 
		$data['select_region'] = $this->Reg->get_select( $this->model, $fields );
		$this->moduletag->model = $this->model;
		$this->moduletag->data = $fields;
		$data['fields'] = $fields;  
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'add');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$this->module->save( $this->model );
	}

	public function delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->delete( $this->model );
	}

	public function trashed(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/			
		
		$data['form_action'] = site_url('c='.$this->page.'&m='.__FUNCTION__);
		/*---------------------------------------------*/

		$data = $this->module->records( $this->model, $data, array($this->model->tblpref.'trashed'=>1) );

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function trash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->trash( $this->model );
	}

	public function untrash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->trash( $this->model, 0 );
	}
 
}
