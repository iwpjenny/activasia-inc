<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ModTransactions extends CI_Controller {
	
	private $model;
	private $page;
	private $view;
	private $location;
	private $dashboard_page;
			
	public function index(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->module->get_data_records( $data );
				
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	private function set_controller( $params ){
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->load->model('modules/Transactions_Model', 'Trans');
		/*---------------------------------------------*/
		$this->model = $this->Trans;
		$this->page = $this->Trans->page;
		$this->view = $this->Trans->view;
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		/*---------------------------------------------*/
		$this->model->ini_custom_models();
		/*---------------------------------------------*/
		$this->load->library('modulefiles');
		$this->modulefiles->model = $this->model;
		$this->modulefiles->model_files = $this->TransF;
		/*---------------------------------------------*/
		$this->load->library('module');
		$this->module->model = $this->model;
		$this->module->model_files = $this->TransF;
		$this->module->model_user = $this->User;
		$this->module->location = $this->location;	
		/*---------------------------------------------*/
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		/*---------------------------------------------*/
		$this->load->library('RecordsFilter',array('model'=>$this->model));
		
		return $data;
	}
	
	public function view(){	
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->module->get_data_view( $data );
		
		$data['sub_section_1'] = $this->module->modal_sub_section_view( $this->TransSub, $data['id'] );
		// $data['sub_section_2'] = $this->module->modal_sub_section_view( $this->TransSub2, $data['id'] );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->model->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	public function edit(){	
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->module->get_data_edit( $data );
		
		$data['select_member'] = $this->Mem->get_select( $this->model, $data['fields'] );
		$data['sub_section_1'] = $this->module->modal_sub_section( $this->TransSub, $data['id'] );
		// $data['sub_section_2'] = $this->module->modal_sub_section( $this->TransSub2, $data['id'] );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->model->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	 
	public function save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('modulelogs',$params);
		$this->modulelogs->create();
		
		$this->module->save();
	}
	
	public function delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('modulelogs',$params);
		$this->modulelogs->create();
		
		$this->module->delete();
	}
			
	public function trashed(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->module->get_data_records( $data, 1 );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	public function trash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->trash();
	}
	
	public function untrash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->untrash();
	}
	
	public function delfile(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*---------------------------------------------*/
		
		$this->module->delete_file();
	}
}