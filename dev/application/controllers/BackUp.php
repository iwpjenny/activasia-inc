<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BackUp extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;
	private $dashboard_page;
	
	public function index(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data['records'] = $this->model->records();
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/'.$this->model->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	private function set_controller( $params ){
		$this->backend->set(); 
		$this->frontend->set(); 
		/*---------------------------------------------*/
		$this->model = $this->BU;
		$this->page = $this->BU->page;
		$this->view = $this->BU->view;
		
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		
		$this->model->ini_custom_models();
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		
		return $data;
	}
	
	
	public function backup_database(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$result = $this->model->backup_database();
		
		if( $result ){
			$notify_params = array('action'=>'backup','type'=>'success','log'=>TRUE);
		} else {
			$notify_params = array('action'=>'backup','type'=>'error','log'=>TRUE);
		}
		$this->notify->set( $notify_params );
		
		redirect( site_url('c='.$this->page) );
	}
	public function backup_application(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$result = $this->model->backup_database_and_application_folder();
		
		if( $result ){
			$notify_params = array('action'=>'backup','type'=>'success','log'=>TRUE);
		} else {
			$notify_params = array('action'=>'backup','type'=>'error','log'=>TRUE);
		}
		$this->notify->set( $notify_params );
		
		redirect( site_url('c='.$this->page) );
	}
	
	public function backup_all(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$result = $this->model->backup_all();
		
		if( $result ){
			$notify_params = array('action'=>'backup','type'=>'success','log'=>TRUE);
		} else {
			$notify_params = array('action'=>'backup','type'=>'error','log'=>TRUE);
		}
		$this->notify->set( $notify_params );
		
		redirect( site_url('c='.$this->page) );
	}
	 
	public function delete_session(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$result = $this->model->delete_session();
		
		if( $result ){
			$notify_params = array('action'=>'backup','type'=>'success','log'=>TRUE);
		} else {
			$notify_params = array('action'=>'backup','type'=>'error','log'=>TRUE);
		}
		$this->notify->set( $notify_params );
		
		redirect( site_url('c='.$this->page) );
	}

    function download(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->load->helper('download');
		
		$basename = $this->input->get('basename');
		
		$base_path = str_replace('\\','/',FCPATH);
		$backup_file = $base_path.'backup/'.$basename;
		$data = file_get_contents($backup_file);
		
		$this->userslogs->save( array('action'=>'download', 'description'=>'Downloaded module Backup Record ('.$basename.').') ); 
		
		force_download($basename, $data); 
    }

    function delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$basename = $this->input->get('basename');
		
		$base_path = str_replace('\\','/',FCPATH);
		$backup_file = $base_path.'backup/'.$basename;
		
		$notify_text = 'Filename (<strong>'.$basename.'</strong>)';
		if( file_exists($backup_file) ){
			if( is_writable($backup_file) ){
				$result = @unlink($backup_file);
				if( $result ){
					$notify_params = array('action'=>'file','type'=>'delete-success','log'=>TRUE,'text'=>'Filename ('.$basename.')');
				} else {
					$notify_params = array('action'=>'file','type'=>'delete-error','log'=>TRUE,'text'=>$notify_text);
				}
			} else {
				$notify_params = array('action'=>'file','type'=>'not-writable','log'=>TRUE,'text'=>$notify_text);
			}
		} else {
			$notify_params = array('action'=>'file','type'=>'dont-exist','log'=>TRUE,'text'=>$notify_text);
		}
		$this->notify->set( $notify_params );
		
		redirect( site_url('c='.$this->page) );
    }
	
	function trancate_module_records(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'trancate_module_records',
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$result = $this->model->truncate_transactions();
		
		if( $result ){
			$notify_params = array('action'=>'truncate','type'=>'success','log'=>TRUE);
		} else {
			$notify_params = array('action'=>'truncate','type'=>'error','log'=>TRUE);
		}
		$this->notify->set( $notify_params );
		
		redirect( site_url('c='.$this->page) );
    }
	
	function trancate_logs(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'trancate_logs',
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
	
		$result = $this->model->truncate_logs();
		
		if( $result ){
			$notify_params = array('action'=>'truncate','type'=>'success','log'=>TRUE);
		} else {
			$notify_params = array('action'=>'truncate','type'=>'error','log'=>TRUE);
		}
		$this->notify->set( $notify_params );
		
		redirect( site_url('c='.$this->page) );
    }
	 
	public function auto_backup_database(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process' );
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$result = $this->model->backup_database(); 
		if( $result ){
			$notify_params = array('action'=>'backup','type'=>'success','log'=>FALSE);
		} else {
			$notify_params = array('action'=>'backup','type'=>'error','log'=>FALSE);
		}
		$this->notify->set( $notify_params );
		 
	}
	 
	public function auto_backup_all(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process' );
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$result = $this->model->backup_all();
		
		if( $result ){
			$notify_params = array('action'=>'backup','type'=>'success','log'=>TRUE);
		} else {
			$notify_params = array('action'=>'backup','type'=>'error','log'=>TRUE);
		}
		$this->notify->set( $notify_params ); 
	}
	 
	public function auto_backup_log(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process' );
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$result = $this->model->archive_database_user_logs();
		 
	}
	public function auto_backup_log_archive(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process' );
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$result = $this->model->backup_database_log_archive();
		 
	}
	 
	public function auto_send_email(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process' );
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		  
		$basename = $this->model->auto_send_backup_database($data);
		
    }
	
	public function send_email(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'send_email');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		 
		$basename = $this->input->get('basename');
		$base_path = str_replace('\\','/',FCPATH);
		$backup_file = $base_path.'backup/'.$basename; 
		
		if( file_exists( $backup_file )){
			 
			$general_email = app_get_val( $data['settings'] ,'email');
			$system_name = app_get_val( $data['settings'] ,'system_name');
			$website = app_get_val( $data['settings'] ,'website'); 
			
			$file_name_date = date('F j, Y',filemtime($backup_file));
			$file_name_time = date('g:i a',filemtime($backup_file)); 
			$file_name_size =  (filesize($backup_file) * .0009765625) * .0009765625; // bytes to MB 
			
			$message = '<table border="0" cellpadding="0" cellspacing="0">';
			$message .= '<tr><td><p><b>Dear Admin, </b></p></td></tr></table>';	
			$message .= '<p>The system database has been successfully backup.'; 
			  
			$message .= '<br /><br /><p>System: <b>'.$system_name.'</b></p>';
			$message .= '<p>URL: '.$website.'</p>';
			$message .= '<p>Date: <b>'.$file_name_date.' at '.$file_name_time.'</b></p>';
			$message .= '<p>Filename: <b>'.$basename.'</b></p>';
			$message .= '<p>File size: <b>'.number_format($file_name_size,2,'.',',').'mb</b></p>'; 
			
			$message .= '<br /><p>You can download the attached Zip file to view Backup.</p>'; 
			$message .= '<p>Do not reply on this email.</p>'; 
			  
			$send = $this->model->send_backup_as_email( $general_email, $message, $backup_file, $basename ); 
			  
			if( $send ){
				$notify_params = array('action'=>'backup','type'=>'success','log'=>TRUE,'text'=>'You have successfully email this file ('.$basename.')');
			} else {
				$notify_params = array('action'=>'backup','type'=>'error','log'=>TRUE, 'text'=>'Error please resend the file ('.$basename.')');
			}
			
		} else {
			$notify_params = array('action'=>'backup','type'=>'error','log'=>TRUE, 'text'=>'Error file ('.$basename.') can not be found.');
		}
		
		$this->notify->set( $notify_params ); 
		redirect( site_url('c='.$this->page) );
    }
}
