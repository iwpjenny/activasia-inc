<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ModWorkingBudgetCSV extends CI_Controller {
	
	private $model;
	private $page;
	private $view;
	private $location;
	private $dashboard_page;
			
	public function index(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('files'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$data['form_action'] = site_url('c='.$this->page.'&m=upload');
		/*----------------------------------------------------*/
		
		$data = $this->moduleuploads->get_data_records( $data );
				
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	private function set_controller( $params ){
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->load->model('modules/Working_Budget_CSV_Model', 'WBCSV');
		/*---------------------------------------------*/
		$this->model = $this->WBCSV;
		$this->page = $this->WBCSV->page;
		$this->view = $this->WBCSV->view;
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		/*---------------------------------------------*/
		$this->model->ini_custom_models();
		/*---------------------------------------------*/
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		/*---------------------------------------------*/
		$this->load->library('RecordsFilter',array('model'=>$this->model));
		
		$params = array(
			'model'=>$this->model,
			'model_user'=>$this->User,
			'model_log'=>$this->BLog,
		);
		$this->load->library('ModuleUploads',$params);
		
		return $data;
	}
	
	public function view(){	
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('Files'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$data['form_action'] = site_url('c='.$this->page.'&m=save');
		/*----------------------------------------------------*/
		
		$data = $this->moduleuploads->get_data_edit( $data, FALSE );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->model->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	public function edit(){	
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('files'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/		
		$data['form_action'] = site_url('c='.$this->page.'&m=save');
		/*---------------------------------------------*/
		
		$data = $this->moduleuploads->get_data_edit( $data );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->model->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	 
	public function upload(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$result = $this->moduleuploads->get_upload();  
		$this->model->read_csv($result);
	}
	 
	public function save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('ModuleLogs',$params);
		$this->modulelogs->create();
		
		$this->moduleuploads->save();
	}
	
	public function delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->moduleuploads->delete();
	}
	
	public function download(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*---------------------------------------------*/
				
		$this->moduleuploads->download();
	}
}