<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FModNameProtectedArea extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;	
	private $login_user_type;
	private $dashboard_page;

	public function index(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/

		$data = $this->module->get_data_records( $data );

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	private function set_controller( $params ){
		$this->backend->set();
		$this->frontend->set();
		$this->customfrontend->set();
		/*---------------------------------------------*/
		$this->load->model('modules/Name_Protected_Area_Model', 'NPA');
		/*---------------------------------------------*/		
		$this->model = $this->NPA;
		$this->page = $this->NPA->fpage;
		$this->view = $this->NPA->view;		
		$this->location = $this->FLog->location;
		$this->login_user_type = $this->FLog->login_user_type;
		$this->dashboard_page = $this->FLog->dashboard_page;
		/*set end---------------------------------------------*/
		$this->model->ini_custom_models();
		/*---------------------------------------------*/
		$this->load->library('module');
		$this->module->model = $this->model;
		$this->module->model_files = $this->TransF;
		$this->module->model_user = $this->Mem;
		$this->module->logged_user_only = $this->logged_user_only;
		$this->module->location = $this->location;
		/*---------------------------------------------*/
		$this->frontend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		/*---------------------------------------------*/
		$this->load->library('recordsfilter',array('model'=>$this->model));
		
		return $data;
	}

	public function view(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/

		$data = $this->module->get_data_view( $data );
		
		$latitude =  get_value( $fields, $this->PA->tblpref.'latitude' );
		$longitude =  get_value( $fields, $this->PA->tblpref.'longitude' );
		$data['map'] = $this->googlemap->generate_map( $latitude, $longitude );

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function edit(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/

		$data = $this->module->get_data_edit( $data );

		$data['select_member'] = $this->NPA->get_select( $this->model, $data['fields'] );
		$data['select_province'] = $this->Prov->get_select( $this->model, $data['fields'] );

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->model->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->save( $this->model );
	}
	
	public function delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->delete( $this->model );
	}

	public function trashed(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/

		$data = $this->module->records( $this->model, $data, array($this->model->tblpref.'trashed'=>1) );

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function trash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/

		$this->module->trash();
	}

	public function untrash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/

		$this->module->untrash();
	}

	public function json_search_protected_area(){
		$data = $this->set_controller();
		/*---------------------------------------------*/

		$request = $this->input->post('request');
		$id = $this->input->post('id');
        $query = $this->input->post('query');


		if ($request == 'json_search_protected_area' && strlen($query) > 1) {

            $npa_details = $this->db->query('SELECT * FROM ' . $this->db->dbprefix.$this->model->tblname . ' WHERE ' . $this->model->tblpref."title LIKE '%{$query}%';");

            foreach ($npa_details->result_array() as $row) {

                $id = get_value($row, $this->model->tblid);
                $title = get_value($row, $this->model->tblpref.'title');
                echo '<li id="' . $id . '">' . $title . '</li>';
            }

            exit();
        }

        if( $request == 'json_search_protected_area' && $id != ''  ){
			$result = array();
			$array = array();
			// $city_details = array();
			$npa_details = $this->Data->get_record( $this->model, array('where_params' => array( $this->model->tblid => $id )));
			$prov_id = get_value( $npa_details, $this->Prov->tblid );
			$city_details = $this->Data->get_records( $this->City, array('where_params' => array( $this->Prov->tblid => $prov_id )));
			$i=0;
			$array[] = array( 'id' => '0','name' => '--Select Municipality--' );
			foreach( $city_details as $city_col ){
				$ct_id = get_value( $city_col, $this->City->tblid );
				$ct_name = get_value( $city_col, $this->City->tblpref.'title' );
				$array[] = array( 'id' => $ct_id,'name' => $ct_name );
				$i++;
			}
			$result['result'] = $array;
			// $result['npa_id'] = $id;

			echo json_encode( $result );
			exit();
		}
	}
}
