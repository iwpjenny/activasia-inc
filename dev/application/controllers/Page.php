<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function index(){ 
		/*standard init in every function---------------------*/
		$data = $this->set_controller();
		/*set end---------------------------------------------*/
		
		$header = $this->pages->html_header();
		
		$this->load->view('head',$data);
		$this->load->view($header,$data);
		$this->load->view('home/home',$data);
		$this->load->view('footer',$data);
		$this->load->view('foot',$data);
	 }

	private function set_controller( $params=array() ){
		$this->backend->set();
		$this->frontend->set();
		
		$login_user_type = $this->logged->get_login_user_type();
		
		if( $login_user_type == $this->BLog->login_user_type ){
			$this->backend->set_libaries();
		} elseif( $login_user_type == $this->FLog->login_user_type ){
			$this->frontend->set_libaries();
		} else {
			$this->load->library('logged');
			$this->load->library('method');
		}
		$this->load->library('pages');
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		
		return $data;
	}
	
	public function about_us(){
		/*standard init in every function---------------------*/
		$data = $this->set_controller();
		/*set end---------------------------------------------*/
		
		$header = $this->pages->html_header();
		
		$this->load->view('head',$data);
		$this->load->view($header,$data);
		$this->load->view('page/about-us',$data);
		$this->load->view('footer',$data);
		$this->load->view('foot',$data);
	}	 
}
