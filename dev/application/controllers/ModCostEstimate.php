<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModCostEstimate extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;
	private $login_user_type;
	private $dashboard_page;

	public function index(){		
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->module->get_data_records( $data ); 
		$select_ce = $this->input->post('select_ce');    
		if( $select_ce ){  
			foreach( $select_ce as $selected_id ){  
				$update_ca = $this->CA->unpublished_ca_data( $selected_id );
				$update_ce = $this->Data->update_data( $this->CE, array( $this->CE->tblpref.'published' => '0' ), array( $this->CE->tblid => $selected_id ) );  
				$update_wb = $this->Data->update_data( $this->WB, array( $this->WB->tblpref.'published' => '0' ), array( $this->CE->tblid => $selected_id ) ); 
			}
		} 
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	private function set_controller( $params ){ 
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		/*---------------------------------------------*/
		$this->model = $this->CE;
		$this->page = $this->CE->page;
		$this->view = $this->CE->view;
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		/*---------------------------------------------*/
		$this->model->ini_custom_models();
		/*---------------------------------------------*/
		/*Files*/
		/*---------------------------------------------*/
		$this->load->library('Module');
		$this->module->model = $this->model; 
		$this->module->model_user = $this->User;
		$this->module->location = $this->location;	
		/*---------------------------------------------*/
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		/*---------------------------------------------*/
		$this->load->library('RecordsFilter',array('model'=>$this->model));
		
		return $data;
	}

	public function view(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->module->get_data_view( $data );
		$fields = $data['fields'];
		
		/*--------------custom---------------*/
		$working_amount = $this->CE->get_total_limit( $data['id'] );
		$total_ca_released = $this->CE->get_total_released_ca( $data['id'] ); 
	  
		$data['working_amount'] =  number_format($working_amount,2,'.',','); 
		$data['total_ca_released'] = number_format($total_ca_released,2,'.',',');  
		$working_amount_balance = 0;
		if($working_amount){
			$working_amount_balance = $total_ca_released / $working_amount ;
		}
		$working_amount_balance = $working_amount_balance * 100 ;
		$data['working_amount_balance'] = number_format($working_amount_balance,2,'.',',');
		$data['published'] = get_value( $fields, $this->model->tblpref.'cancelled'); 

		/*--------------------------------------*/
		$data['items'] = $this->CEI->get_items( $data['id']  );
		// $this->userslogs->save( 'view', 'Cost Estimate ('.$id.')' );  
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function edit(){
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->module->get_data_edit( $data );

		$fields = $data['fields'];    
		$id = $data['id'];    

		/*------data with calculations------*/
		$ce_date = get_value( $fields, $this->CE->tblpref.'date',current_date());
		$data['qtr'] = $this->CE->get_quarter( $ce_date ); 
		$working_amount = $this->CE->get_total_limit( $id );
		$data['working_amount'] =  number_format($working_amount,2,'.',','); 
		$total_ca_released = $this->CE->get_total_released_ca( $id ); 
		$data['total_ca_released'] = number_format($total_ca_released,2,'.',','); 
		$po_number = get_value( $fields, $this->model->tblpref.'po_number'); 
		
		// $working_amount_balance = $working_amount - $total_ca_released;
				$working_amount_balance = $total_ca_released / $working_amount ;
		$working_amount_balance = $working_amount_balance * 100 ;
		$data['working_amount_balance'] = number_format($working_amount_balance,2,'.',',');
		/*------------------------------------*/ 
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'add');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		//$this->modulelogs->create();
		/* $params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('modulelogs',$params);
		$this->modulelogs->create(); */
		$this->module->save();
	}

	public function delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		//$this->modulelogs->create();
		/* $params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('modulelogs',$params);
		$this->modulelogs->create(); */
		$this->module->delete();
	}

	public function trashed(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/			
		
		$data['form_action'] = site_url('c='.$this->page.'&m='.__FUNCTION__);
		/*---------------------------------------------*/

		$data = $this->module->records( $this->model, $data, array($this->model->tblpref.'trashed'=>1) );

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function trash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,'method'=>array('library'=>array('TrashItems')));
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/ 
		/* $id = $this->input->get('id'); 
		$this->trashitems->trash($this->model,$id);  */
	 	$params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('ModuleLogs',$params);
		$this->modulelogs->create(); 

		$id = $this->input->get( 'id' ); 
		$params = array('model'=>$this->CEI,'id'=>$id,'type'=>__FUNCTION__); 
		$this->CECSV->create_logs_items($params);    

		$this->module->trash();
	}

	public function untrash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		/*   */
		$this->module->untrash();
	}

	public function export(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'export','method'=>array('library'=>array('excel'))
		);  
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		//$filename = "Cost-Estimate-Records-".current_date().".csv";
	
		$date_now = date('Y-m-d-H-i');
		$filename='Cost Estimate List -'.$date_now; 
		$ordertype = $this->input->get('sort'); 
		$quarter = $this->input->post('quarter');
		$year = $this->input->post('year'); 
		$result = $this->model->get_report($quarter, $year); 
		$columns = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 20,'L' => 20,'M' => 20
		);
		if( $result ){
			$this->excel->export_report( $result, $filename, $columns );
		} else {
			$notify_params = array('text'=>'Sorry, there is no data for that selection.','action'=>'export','type'=>'error','log'=>FALSE); 
			$this->notify->set( $notify_params );
			redirect( site_url( 'c='.$this->page ) );
		}
	}
	
	public function export_individual(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'export','method'=>array('library'=>array('excel'))
		);  
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		//$filename = "Cost-Estimate-Records-".current_date().".csv";
	
		$date_now = date('Y-m-d-H-i');
		$filename='Cost Estimate List -'.$date_now; 
		$ordertype = $this->input->get('sort'); 
		$id = $this->input->get('id'); 
		$result = $this->model->get_report_individual($id); 
		$columns = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 30,'L' => 30,'M' => 30
		);
		if( $result ){
			$this->excel->export_report_individual( $result, $filename, $columns );
		} 
	}

	public function cancel(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		 
	 	/*---------------------------------------------*/
	 	$params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('ModuleLogs',$params);
		$this->modulelogs->create();
	 	/*---------------------------------------------*/
		$id = $this->input->get('id'); 
		$data = array(  
			$this->CE->tblpref.'cancelled'=>1  
		);   
		$update = $this->Data->update_data( $this->model, $data, array( $this->CE->tblid => $id ) );  
		if( $update ){ 
			redirect( site_url('c='.$this->page.'&m=view&id='.$id) );	
		} else {
			set_alerts('warning', 'Cancelling cost estimate unsuccessful.'); 
			redirect_current_page();		
		}
		/*---------------------------------------------*/ 
		
	}
	 
}
