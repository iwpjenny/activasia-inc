<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;	
	private $dashboard_page;
	
	public function index(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->users->get_data_records_except_logged_user( $data );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	private function set_controller( $params ){
		$this->backend->set(); 
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->model = $this->User;
		$this->page = $this->User->page;
		$this->view = $this->User->view;
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		/*---------------------------------------------*/
		$this->model->ini_custom_models();
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		/*---------------------------------------------*/
		$this->load->library('RecordsFilter',array('model'=>$this->model));
		/*---------------------------------------------*/
		$params = array(
			'model'=>$this->model,
			'model_profile'=>$this->UP,
			'model_role'=>$this->UR,
			'model_logs'=>$this->UL
		);
		$this->load->library('users',$params);
		
		return $data;	
	}
	 
	public function edit(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/		
		$data['form_action'] = site_url('c='.$this->page.'&m=save');
		/*---------------------------------------------*/
		
		$data = $this->users->get_data_edit_except_logged_user( $data );
		
		/*---------------------------------------------*/		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/'.$this->view.'/edit', $data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	public function view(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->users->get_data_view_except_logged_user( $data );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/'.$this->view.'/view',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	public function save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->users->save();
	}
	
	public function delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->users->delete();
	}
	
	public function save_profile(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->users->session_user_id = TRUE;
		$this->users->save();
	}
	
	public function profile(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/		
		$data['form_action'] = site_url('c='.$this->page.'&m=save_profile');
		/*---------------------------------------------*/
		
		$data = $this->users->get_data_edit_profile( $data );		
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/'.$this->view.'/profile',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	} 	
} 