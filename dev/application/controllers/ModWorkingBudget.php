<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModWorkingBudget extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;
	private $login_user_type;
	private $dashboard_page;

	public function index(){		
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>'records');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		// $data = $this->module->records( $this->model, $data, array($this->model->tblpref.'trashed'=>0) );		 
		$data = $this->module->get_data_records( $data ); 
	
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	private function set_controller( $params ){
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->load->model('modules/Working_Budget_Model', 'WB');
		/*---------------------------------------------*/
		
		$this->model = $this->WB;
		$this->page = $this->WB->page;
		$this->view = $this->WB->view;
		
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;

		$this->model->ini_custom_models();
		/*---------------------------------------------*/
		$this->load->library('module');
		$this->module->model = $this->model;
		$this->module->location = $this->location;
		/*---------------------------------------------*/
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/	
		$data = $this->method->set_controller( $params );
		/*---------------------------------------------*/
		$this->load->library('RecordsFilter',array('model'=>$this->model));
		
		return $data;
	}

	public function view(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->module->get_data_view( $data ); 
		$data['items'] = $this->WBI->get_items_list( $data['id'] );  
		

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function edit(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('member_models','helper'=>array('fields'),'library'=>array('files'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$data['form_action'] = site_url('c='.$this->page.'&m=save');
		/*---------------------------------------------*/
		$id = $this->input->get('id'); 
		$data['id'] = $id;
		/*---------------------------------------------*/

		$fields = $this->module->record( $this->model, $id );
		$this->moduletag->model = $this->model;
		$this->moduletag->data = $fields;
		
		$data['fields'] = $fields;   
		$data['select_ce'] = $this->CE->get_select( $this->model, $fields );
		
		$ca_limit = get_value($fields, $this->WB->tblpref.'bcs');
		$wb_id = get_value( $fields, $this->WB->tblid);
		$ca_balance = $this->CA->get_current_ca_balance($id, $ca_limit);
		$data['ca_balance'] = $ca_balance;  
		
		//working budget items
		$data['sub_section_1'] = $this->module->modal_sub_section( $this->WBI, $this->model, $id );
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'add');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('modulelogs',$params);
		$this->modulelogs->create();
		$this->module->save( $this->model );
	}

	public function delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('modulelogs',$params);
		$this->modulelogs->create();
		$this->module->delete( $this->model );
	}

	public function trashed(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/			
		
		$data['form_action'] = site_url('c='.$this->page.'&m='.__FUNCTION__);
		/*---------------------------------------------*/

		$data = $this->module->records( $this->model, $data, array($this->model->tblpref.'trashed'=>1) );

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function trash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/ 
		$this->module->trash();
	}

	public function untrash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
	 
		$this->module->untrash();
	}

	public function export(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'export','method'=>array('library'=>array('excel'))
		);  
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		//$filename = "Cost-Estimate-Records-".current_date().".csv";
	
		$date_now = date('Y-m-d-H-i');
		$filename='Budget Control System -'.$date_now; 
		$ordertype = $this->input->get('sort'); 
		$quarter = $this->input->post('quarter');
		$year = $this->input->post('year'); 
		$result = $this->model->get_report($quarter, $year);  
		$columns = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 20,'L' => 20,
		);
		if( $result ){
			$this->excel->export_report( $result, $filename, $columns );
		}  else {
			$notify_params = array('text'=>'Sorry, there is no data for that selection.','action'=>'export','type'=>'error','log'=>FALSE); 
			$this->notify->set( $notify_params );
			redirect( site_url( 'c='.$this->page ) );
		}
	}
	
	public function export_individual(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'export','method'=>array('library'=>array('excel'))
		);  
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		//$filename = "Cost-Estimate-Records-".current_date().".csv";
		$id = $this->input->get('id'); 
		$date_now = date('Y-m-d-H-i');
		$filename='Budget Control System -'.$date_now; 
		$ordertype = $this->input->get('sort'); 
		$result = $this->model->get_report_individual($id); 
		$columns = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 20,'L' => 20,
		);
		if( $result ){
			$this->excel->export_report_individual( $result, $filename, $columns );
		}  
	} 
}