<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BHome extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;	
	private $dashboard_page;

	public function index(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','method'=>array('library'=>array('ModUser')));
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$data['form_action'] = site_url('c='.$this->dashboard_page);
		
		/* $data = $this->User->get_data_section_records( $data );
		$data = $this->UL->get_data_section_records( $data );
		$data = $this->Trans->get_data_section_records( $data ); 
		$data = $this->UL->get_data_section_records( $data );
		$data = $this->BU->get_data_section_records( $data );
		$data = $this->CE->get_data_section_records( $data ); 
		$data = $this->WB->get_data_section_records( $data );
		$data = $this->CA->get_data_section_ca_release( $data );
		$data = $this->CA->get_data_section_ca_pending( $data );
		  
		if( in_array($data['logged_user_position_name'],$this->users->exempted_role) ){
			$data = $this->User->get_data_section_records( $data );
			$data = $this->Mem->get_data_section_records( $data );
			$data = $this->ML->get_data_section_records( $data );
		}  */
		/* $data = $this->CA->get_data_section_ca_release( $data );
		$data = $this->CA->get_data_section_ca_pending( $data );
		 */
		$data['show_status']['show_status_approve_button'] = $this->usersrole->check( $this->CA->urc_name, 'approve');
		$data['show_status']['show_status_decline_button'] = $this->usersrole->check( $this->CA->urc_name, 'decline');
		$data['show_status']['show_status_pending_button'] = $this->usersrole->check( $this->CA->urc_name, 'pending');
		$data['show_status']['show_status_release_button'] = $this->usersrole->check( $this->CA->urc_name, 'release');
		// $data['to_approve'] = $this->CA->get_data_section_ca_pending( $data );
		// printx($data);
		$this->load->library('Models');
		$data = $this->models->set_data_standard_dashboard_sections( $data );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/dashboard',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	 }

	 private function set_controller( $params ){
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/		
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		/*---------------------------------------------*/
		$this->backend->set_libaries();
		
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		/*---------------------------------------------*/
		/* Initialize Custom Model here */
		$this->load->model('modules/Transactions_Model', 'Trans'); 
		$this->load->model('modules/Cost_Estimate_Model', 'CE'); 
		$this->load->model('modules/Region_Model', 'Reg'); 
		$this->load->model('modules/Working_Budget_Model', 'WB'); 
		$this->load->model('modules/Working_Budget_Items_Model', 'WBI'); 
		$this->load->model('modules/Cash_Advance_Model', 'CA'); 
		$this->load->model('modules/Cash_Advance_Items_Model', 'CAI'); 
		$this->load->model('modules/Working_Budget_CSV_Model', 'WBCSV');  
		
		$this->load->model('modules/Area_Model', 'Area'); 
		$this->load->model('modules/Cost_Estimate_CSV_Model', 'CECSV');
		/* End of initialize custom model here */
		/*---------------------------------------------*/
		$params = array(
			'model'=>$this->User,
			'model_profile'=>$this->UP,
			'model_role'=>$this->UR,
			'model_logs'=>$this->UL
		);
		$this->load->library('users',$params);
		$this->load->library('module',$params);
		 
		$params = array(
			'model'=>$this->CECSV,
			'model_user'=>$this->User,
			'model_log'=>$this->BLog,
		);
		$this->load->library('ModuleUploads',$params);
		
		return $data;
	}
}