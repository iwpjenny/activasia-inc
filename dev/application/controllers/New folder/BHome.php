<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BHome extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;	
	private $dashboard_page;

	public function index(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$data['form_action'] = site_url('c='.$this->dashboard_page);
		
		$role_name = $this->logged->get_login_session('role_name');
		 
		$data = $this->User->get_data_section_records( $data );
		$data = $this->UL->get_data_section_records( $data );
		$data = $this->Trans->get_data_section_records( $data );
		$data = $this->UL->get_data_section_records( $data );
		$data = $this->BU->get_data_section_records( $data );
		$data = $this->CE->get_data_section_records( $data );
		$data = $this->WB->get_data_section_records( $data );
		
		if( in_array($role_name,array('developer','admin')) ){
			$data = $this->Mem->get_data_section_records( $data );
			$data = $this->ML->get_data_section_records( $data );
		}
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/dashboard',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	 }

	 private function set_controller( $params ){
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/		
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		
		$this->backend->set_libaries();
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		
		/* Initialize Custom Model here */
		$this->load->model('modules/Transactions_Model', 'Trans'); 
		$this->load->model('modules/Cost_Estimate_Model', 'CE'); 
		$this->load->model('modules/Region_Model', 'Reg'); 
		$this->load->model('modules/Working_Budget_Model', 'WB'); 
		$this->load->model('modules/Area_Model', 'Area'); 
		$this->load->model('modules/Cash_Advance_Model', 'CA'); 
		/* End of initialize custom model here */
		/*---------------------------------------------*/
		
		return $data;
	}
}