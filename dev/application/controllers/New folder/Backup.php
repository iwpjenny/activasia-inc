<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BackUp extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;
	private $dashboard_page;
	
	public function index(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
						
		$data['records'] = $this->model->records();
		
		//settings title
		$module_terms = $this->Set->get_full_settings('module-terms');  		
		$data['backup_description'] = get_value( $module_terms, 'backup_description');
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/'.$this->model->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	private function set_controller( $params ){
		$this->backend->set(); 
		$this->frontend->set(); 
		/*---------------------------------------------*/
		$this->model = $this->BU;
		$this->page = $this->BU->page;
		$this->view = $this->BU->view;
		
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		
		$this->model->ini_custom_models();
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		
		return $data;
	}
	
	public function database(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$backup = $this->model->backup_database();
		
		if( $backup ){
			$notify_params = array('action'=>'backup','type'=>'success','log'=>TRUE);
		} else {
			$notify_params = array('action'=>'backup','type'=>'error','log'=>TRUE);
		}
		$CI->notify->set( $notify_params );
		
		redirect( site_url('c='.$this->page) );
	}
	
	public function application(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$backup = $this->model->backup_database_and_application_folder();
		
		if( $backup ){
			$notify_params = array('action'=>'backup','type'=>'success','log'=>TRUE);
		} else {
			$notify_params = array('action'=>'backup','type'=>'error','log'=>TRUE);
		}
		$CI->notify->set( $notify_params );
		
		redirect( site_url('c='.$this->page) );
	}
	
	public function all(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$backup = $this->model->backup_all();
		
		if( $backup ){
			$notify_params = array('action'=>'backup','type'=>'success','log'=>TRUE);
		} else {
			$notify_params = array('action'=>'backup','type'=>'error','log'=>TRUE);
		}
		$CI->notify->set( $notify_params );
		
		redirect( site_url('c='.$this->page) );
	}

    function download(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->load->helper('download');
		
		$basename = $this->input->get('basename');
		
		$base_path = str_replace('\\','/',FCPATH);
		$backup_file = $base_path.'backup/'.$basename;
		$data = file_get_contents($backup_file);
		
		$this->userslogs->save( 'download', 'Downloaded module Backup Record ('.$basename.').' ); 
		
		force_download($basename, $data); 
    }

    function delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$basename = $this->input->get('basename');
		
		$base_path = str_replace('\\','/',FCPATH);
		$backup_file = $base_path.'backup/'.$basename;
		
		$delete = FALSE;
		if( is_writable($backup_file) ){
			$delete = @unlink($backup_file);
			if( $delete ){
				$CI->notify->set('Delete of backup file ('.$basename.') successful.', 'danger', 'delete', TRUE);
			} else {
				$CI->notify->set('Delete of backup file ('.$basename.') unsuccessful.', 'danger', 'delete', TRUE);
			}
		} else {
			$CI->notify->set('File ('.$basename.') is not writable.', 'warning', 'delete', TRUE);
		}
		
		redirect( site_url('c='.$this->page) );
    }
	
	function trunc_module_records(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'trancate_module_records',
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$result = $this->model->truncate_transactions();
		set_alerts('success', 'Transaction Tables have been truncated.');
		
		redirect( site_url('c='.$this->page) );
    }
	
	function trunc_logs(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'trancate_logs',
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
	
		$result = $this->model->truncate_logs();
		set_alerts('success', 'Logs Tables have been truncated.');
		
		redirect( site_url('c='.$this->page) );
    }
}
