<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class FMember extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;
	private $dashboard_page;
	
	public function index(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/		
		$data['form_action'] = site_url('c='.$this->page.'&m=save');
		/*---------------------------------------------*/
		
		$this->users->model_role = $this->MR;
		$this->users->model_profile = $this->MP;
		$data = $this->users->get_edit_profile( $this->model, $data );		
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/'.$this->view.'/profile',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	private function set_controller( $params ){
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		
		$this->model = $this->Mem;
		$this->page = $this->Mem->fpage;
		$this->view = $this->Mem->view;
		
		$this->location = $this->FLog->location;
		$this->dashboard_page = $this->FLog->dashboard_page;
		
		$this->model->ini_custom_models();
		$this->frontend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		
		return $data;
	}
	
	public function save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->users->session_user_id = TRUE;
		$this->users->Logs = $this->ML;
		$this->users->Role = $this->MR;
		$this->users->save( $this->model, $this->MP, 'member' );
	}
} 