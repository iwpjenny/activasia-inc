<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FLog extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;
	private $dashboard_page;
	
	public function index(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'logout','form'=>TRUE);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$data['form_name'] = 'Member User Log-in Form';
		
		$data = $this->logged->set_data_login_form( $data, $this->model );
		
		$this->load->view('head',$data);
		$this->load->view('header',$data);
		$this->load->view($this->location.'/'.$this->model->view.'/login',$data);
		$this->load->view('foot',$data);
	}
	 
	private function set_controller( $params ){
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->model = $this->FLog;
		
		$this->page = $this->model->page;
		$this->location = $this->FLog->location;
		$this->dashboard_page = $this->FLog->dashboard_page;
		
		$this->frontend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		
		return $data;			
	}
	
	public function login(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'logout');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->logged->login();		
	}

	public function logout(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login');
		$data = $this->set_controller( $controller_params );
		/*logged settings-------------------------------------*/
		
		$this->logged->logout();
	}

	public function forgotpassword(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'logout','form'=>TRUE);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$data['form_name'] = 'Password Recovery Form';
		
		$data = $this->logged->set_data_forgot_password( $data );
		  
		$this->load->view('head',$data);
		$this->load->view('header',$data); 
		$this->load->view('head',$data);
		$this->load->view($this->location.'/'.$this->model->view.'/forgot-password',$data);
		$this->load->view('foot',$data);
	}

	public function retrievepassword(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'logout');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->logged->send_new_password();
	}
}
