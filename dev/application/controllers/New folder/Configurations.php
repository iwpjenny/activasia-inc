<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configurations extends CI_Controller {
	
	private $model;
	private $page;
	private $view;
	private $location;
	private $dashboard_page;
	
	public function index(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		//get configurations record
		$parent = "`".$this->model->tblpref."parent_id` AS `parentid`, (SELECT `".$this->model->tblpref."title` FROM `".$this->db->dbprefix.$this->model->tblname."` WHERE `".$this->model->tblid."`=`parentid`) AS `parent_title`";
		$field = $this->model->tblid.',';
		$field .= $this->model->tblpref.'title,';
		$field .= $this->model->tblpref.'name,';
		$field .= $this->model->tblpref.'created,';
		$field .= $this->model->tblpref.'published,';
		$field .= $parent;
		
		$search_fields = $this->model->set_search_fields(); 
		$records = $this->Data->get_records( $this->model, array(
			'search_fields'=>$search_fields,
			'field'=>$field,
			'compound_select'=>FALSE,
			'paging'=>TRUE
		)); 
		$data['record_rows'] = $records;
		$data['pagination'] =  $this->Data->pagination_link; 
		$data['per_page'] = $this->Data->per_page;
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/'.$this->model->page.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data); 
	}
	
	
	private function set_controller( $params ){ 
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->load->model('Configurations_Model', 'Config');
		/*---------------------------------------------*/
		
		$this->model = $this->Config;
		$this->page = $this->Config->page;
		$this->view = $this->Config->view;
		
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		
		$this->model->ini_custom_models();
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		
		return $data;	
	}
	
	public function edit(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data['form_action'] = site_url('c='.$this->model->page.'&m=save');
		$data['form_action_format_config'] = site_url('c='.$this->model->page.'&m=save_format_config');  
		//setting details
		$id = $this->input->get('set_id');
		$data['id'] = $id;
		$details = $this->Data->get_record( $this->Set, array('where_params'=>array( $this->Set->tblid => $id ) ) );
		$name = get_value($details,$this->Set->tblpref.'name');
		$parent_id = get_value($details,$this->Set->tblpref.'parent_id');
		$prefix_config = $this->model->tblpref; 
		$data[$prefix_config.'id'] = $id;
		$data[$prefix_config.'name'] = $name; 
		$data[$prefix_config.'title'] = get_value($details,$prefix_config.'title');   
		$data[$prefix_config.'parent_id'] = $parent_id;
		$data[$prefix_config.'description'] = get_value($details,$prefix_config.'description'); 
		$data[$prefix_config.'created'] = get_value($details,$prefix_config.'created');
		$data[$prefix_config.'modified'] = get_value($details,$prefix_config.'modified');
		$data[$prefix_config.'published'] = get_value($details,$prefix_config.'published'); 
		$where_params_parents = array( 	
			'where_params'  => array( $this->Set->tblpref.'parent_id' => 0, $this->Set->tblpref.'published'=>'1' )
			
		); 
		$data[$prefix_config.'parent_id'] = $this->Set->dropdown($this->Set,$parent_id,$where_params_parents ,$prefix_config.'parent_id'); //get parents
		$data['prefix_config'] = $prefix_config;
		
		//format settings details 
		$prefix_config_format = $this->SFM->tblpref; 
		$setf_id = $this->input->get('setf_id'); 
		$format_settings_details = $this->Data->get_record( $this->SFM, array('where_params'=>array( $this->SFM->tblid => $setf_id ) ) );  
		$setf_type = get_value($format_settings_details,$this->SFM->tblpref.'type'); 
		$data[$prefix_config_format.'id'] = $setf_id; 
		$data[$prefix_config_format.'title'] = get_value($format_settings_details,$this->SFM->tblpref.'title');
		$data[$prefix_config_format.'name'] = get_value($format_settings_details,$this->SFM->tblpref.'name');  
		$data[$prefix_config_format.'description'] = get_value($format_settings_details,$this->SFM->tblpref.'description'); 
		$data[$prefix_config_format.'type'] = $setf_type;  
		$data[$prefix_config_format.'values'] = get_value($format_settings_details,$this->SFM->tblpref.'values');  
		$data[$prefix_config_format.'default_value'] = get_value($format_settings_details,$this->SFM->tblpref.'default_value');   
		$data[$prefix_config_format.'max_chars'] = get_value($format_settings_details,$this->SFM->tblpref.'max_chars'); 
		$data[$prefix_config_format.'multiple_records'] = get_value($format_settings_details,$this->SFM->tblpref.'multiple_records'); 
		$data[$prefix_config_format.'created'] = get_value($format_settings_details,$this->SFM->tblpref.'created');
		$data[$prefix_config_format.'modified'] = get_value($format_settings_details,$this->SFM->tblpref.'modified');
		$data[$prefix_config_format.'published'] = get_value($format_settings_details,$this->SFM->tblpref.'published');  
		$data['prefix_config_format'] = $prefix_config_format;
		$format_type_choices = array( 
			   'value' => array
				( '0' => 'text', '1' => 'textarea', '2' => 'checkbox', '3' => 'radio',  '4' => 'select', '5' => 'email', '6' => 'password', '7' => 'multiselect', '8' => 'date', '9' => 'datetime' ),
			   'text' => array
				( '0' => 'text', '1' => 'textarea', '2' => 'checkbox', '3' => 'radio',  '4' => 'select', '5' => 'email', '6' => 'password', '7' => 'multiselect', '8' => 'date', '9' => 'datetime' )
			);  
		$data[$prefix_config_format.'type_choices'] = $this->Set->select_dropdown($format_type_choices, $setf_type,$prefix_config_format.'type'); 
		$data['format_settings_table'] = $this->Data->get_records( $this->SFM, array('where_params'=>array( $this->Set->tblid => $id ) ) );
		
		if( $setf_id ){
			/* $data['add_format_settings'] = NULL; */
			$data['update_format_settings'] = 1;
		} else {
			/* $data['add_format_settings'] = 1; */
			$data['update_format_settings'] = NULL;
		} 
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/'.$this->model->page.'/edit',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);  
	}
	
	public function delete(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$id = $this->input->get('id');
		$settings_details = $this->Data->get_record( $this->model, array( 'where_params' => array( $this->model->tblid => $id ) ) ); 
		$title = get_value( $settings_details, $this->model->tblpref.'title');
		$setting = $this->Data->get_record( $this->model, array( 'where_params' => array( $this->model->tblid => $id ) ) );
		
		if( $setting ){
			$name = get_value( $setting, $this->model->tblpref.'name');
			
			if( $name != 'general' ){
				$delete = $this->Data->delete_data( $this->model, array( $this->model->tblid => $id ) );
				if( $delete ){ 
					$notify = 'Deleting Configuration successful. (ID: '.$id.' - '.$title.')';
					$this->notify->set($notify, 'success', 'delete', TRUE);
				} else { 
					$notify = 'Deleting Configuration unsuccessful. (ID: '.$id.' - '.$title.')';
					$this->notify->set($notify, 'warning', 'delete', TRUE);
				}
			}
		}
		
		redirect( site_url('c='.$this->model->page) ); 
	}

	public function save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		 
		$prefix_config = $this->model->tblpref; 
		$id = $this->input->post($prefix_config.'id');
		$title = $this->input->post($prefix_config.'title'); 
		$name = $this->input->post($prefix_config.'name');
		$parent_settings_id = $this->input->post($prefix_config.'parent_id');
		$description = $this->input->post($prefix_config.'description'); 
		$created = $this->input->post($prefix_config.'created');
		$modified = $this->input->post($prefix_config.'modified');
		$published = $this->input->post($prefix_config.'published'); 
		$name = app_convert_to_name($name,'_');
	
		$data = array(
			$this->Set->tblpref.'title' => $title,
			$this->Set->tblpref.'name' => $name,
			$this->Set->tblpref.'parent_id' => $parent_settings_id,
			$this->Set->tblpref.'description' => $description,
			$this->Set->tblpref.'published' => $published
		);
		
		if( $id ){
			// $this->URC->check_user_role_capabality( $this->Config->tblname, 'update_data', 'yes' );
			$update = $this->Data->update_data( $this->model, $data, array( $this->Set->tblid => $id ) );  
			if( $update ){
				$notify = 'Update Configuration successful. (ID: '.$id.' - '.$title.')';
				$this->notify->set($notify, 'success', 'update', TRUE);
			} else {
				$notify = 'Update Configuration unsuccessful. (ID: '.$id.' - '.$title.')';
				$this->notify->set($notify, 'warning', 'update', TRUE);
			} 
			
			redirect_current_page();
		} else {
			// $this->URC->check_user_role_capabality( $this->Config->tblname, 'add_data', 'yes' );
			$array_published = array(
				$this->model->tblpref.'published' => 1
				);
			$data  = array_merge($data, $array_published);
			$id = $this->Data->insert_data( $this->model, $data );
			$record = $this->Data->get_record( $this->model, array( 'where_params' => array($this->model->tblid => $id)) );
			$new_title = get_value($record, $this->model->tblpref.'title');
			if( $id ){
				$notify = 'Adding Configuration successful. (ID: '.$id.' - '.$new_title.')';
				$this->notify->set($notify, 'success', 'add', TRUE);
			} else { 
				$notify = 'Adding Configuration unsuccessful.';
				$this->notify->set($notify, 'warning', 'add', TRUE);
			} 
			redirect( site_url('c='.$this->model->page.'&m=edit&set_id='.$id) );
		}
		 
	}
	
	public function save_format_config(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'add');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$prefix_config_format = $this->SFM->tblpref; 
		$set_id = $this->input->post($this->Set->tblid); 
		$setf_id = $this->input->post($prefix_config_format.'id');
		$setf_title = $this->input->post($prefix_config_format.'title');
		$setf_name = $this->input->post($prefix_config_format.'name'); 
		$setf_description = $this->input->post($prefix_config_format.'description'); 
		$setf_type = $this->input->post($prefix_config_format.'type'); 
		$setf_max_chars = $this->input->post($prefix_config_format.'max_chars'); 
		$setf_default_value = $this->input->post($prefix_config_format.'default_value'); 
		$setf_values = $this->input->post($prefix_config_format.'values'); 
		$setf_multiple_records = $this->input->post($prefix_config_format.'multiple_records');  
		$setf_created = $this->input->post($prefix_config_format.'created');
		$setf_modified = $this->input->post($prefix_config_format.'modified');
		$setf_published = $this->input->post($prefix_config_format.'published'); 
		$setf_name = app_convert_to_name($setf_name,'_');
		
		$data = array(
			$this->Set->tblid => $set_id,
			$this->SFM->tblpref.'title' => $setf_title,
			$this->SFM->tblpref.'description' => $setf_description, 
			$this->SFM->tblpref.'type' => $setf_type, 
			$this->SFM->tblpref.'name' => $setf_name,
			$this->SFM->tblpref.'max_chars' => $setf_max_chars,
			$this->SFM->tblpref.'default_value' => $setf_default_value,
			$this->SFM->tblpref.'values' => $setf_values,
			$this->SFM->tblpref.'multiple_records' => $setf_multiple_records,
			$this->SFM->tblpref.'published' => $setf_published
		);
		
		if( $setf_id ){
			$update = $this->Data->update_data( $this->SFM, $data, array( $this->SFM->tblid => $setf_id ) );
			 
			if( $update ){
				$notify = 'Updating Format Configuration successful. (ID: '.$setf_id.' - '.$setf_title.')';
				$this->notify->set($notify, 'success', 'update', TRUE);
				
			} else {
				$notify = 'Updating Format Configuration unsuccessful. (ID: '.$setf_id.' - '.$setf_title.')';
				$this->notify->set($notify, 'warning', 'update', TRUE);
			} 
			redirect_current_page();
		} else {
			$array_published = array(
				$this->SFM->tblpref.'published' => 1
				);
			$data  = array_merge($data, $array_published);
			$setf_id = $this->Data->insert_data( $this->SFM, $data );
			
			$record = $this->Data->get_record( $this->SFM, array( 'where_params' => array($this->SFM->tblid => $setf_id)) );
			$new_title = get_value($record, $this->SFM->tblpref.'title');
	
			if( $setf_id ){
				
				$notify = 'Adding Format Configuration successful. (ID: '.$setf_id.' - '.$new_title.')';
				$this->notify->set($notify, 'success', 'add', TRUE);
				
			} else {
				$notify = 'Adding Format Configuration unsuccessful.';
				$this->notify->set($notify, 'warning', 'add', TRUE);
			}
			
			redirect( site_url('c='.$this->model->page.'&m=edit&set_id='.$set_id.'&setf_id='.$setf_id) );
		}
		
	}
	
	public function delete_format_config(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'delete');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
	
		$id = $this->input->get('setf_id');
		$set_id = $this->input->get('set_id');
		$setting_details = $this->Data->get_record( $this->SFM, array( 'where_params' => array( $this->SFM->tblid => $id ) ) ); 
		$title = get_value( $setting_details, $this->SFM->tblpref.'title');
		$setting = $this->Data->get_record( $this->SFM, array( 'where_params' => array( $this->SFM->tblid => $id ) ) );
		
		if( $setting ){
			$name = get_value( $setting, $this->SFM->tblpref.'name');
			
			if( $name != 'general' ){
				$delete = $this->Data->delete_data( $this->SFM, array( $this->SFM->tblid => $id ) );
				if( $delete ){ 
					$notify = 'Deleting Format Configuration successful. (ID: '.$id.' - '.$title.')';
					$this->notify->set($notify, 'success', 'delete', TRUE);
				} else {
					set_alerts('warning', 'Deleting data unsuccessful.');
					
					$notify = 'Deleting Format Configuration unsuccessful. (ID: '.$id.' - '.$title.')';
					$this->notify->set($notify, 'warning', 'delete', TRUE);
				}
			}
		}
		redirect( site_url('c='.$this->model->page.'&m=edit&set_id='.$set_id) ); 
	}	
}