<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FMemberLogs extends CI_Controller {
	
	private $model;
	private $page;
	private $view;
	private $location;
	private $dashboard_page; 
	
	public function index(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->userslogs->get_data_records_logged_users( $data );
			
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	private function set_controller( $params ){
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		
		$this->model = $this->ML;
		$this->page = $this->ML->fpage;
		$this->view = $this->ML->view;
		
		$this->location = $this->FLog->location;
		$this->login_user_type = $this->FLog->login_user_type;
		$this->dashboard_page = $this->FLog->dashboard_page;
		
		$this->model->ini_custom_models();
		$this->frontend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		
		return $data;	
	}
	
	public function view(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->userslogs->get_data_view_logged_user( $data );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/'.$this->view.'/view',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

}
