<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {
	
	private $model;
	private $page;
	private $view;
	private $location;
	private $dashboard_page;
	
	public function index(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data['form_action'] = site_url('c='.$this->page);
		
		$search_fields = $this->model->set_search_fields(); 
		$records = $this->Data->get_records( $this->model, array(
			'search_fields'=>$search_fields,
			'where_params'=>array($this->model->tblpref.'parent_id' => 0 ),
			'paging'=>TRUE
		)); 
		$data['record_rows'] = $records;
		$data['pagination'] =  $this->Data->pagination_link; 
		$data['per_page'] = $this->Data->per_page;
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/'.$this->page.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data); 
	}
	
	
	private function set_controller( $params ){ 
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->load->model('Settings_Model', 'Set');
		/*---------------------------------------------*/		
		$this->model = $this->Set;
		$this->page = $this->Set->page;
		$this->view = $this->Set->view;
		
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		
		$this->model->ini_custom_models();
		$this->backend->set_libaries( $this->model );		
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		
		return $data;	
	}
	
	public function edit(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data['form_action'] = site_url('c='.$this->page.'&m=save_parent_settings');
		$data['form_action_format_settings'] = site_url('c='.$this->model->page.'&m=save_sub_settings');  
		
		//config details
		$id = $this->input->get('set_id');
		$data['id'] = $id;
		$data['setr_id'] = $this->input->get('setr_id');
		
		$details = $this->Data->get_record( $this->model, array('where_params'=>array( $this->model->tblid => $id ) ) );
		$name = get_value($details,$this->model->tblpref.'name');
		$parent_id = get_value($details,$this->model->tblpref.'parent_id');
		$prefix_config = $this->Config->tblpref; 
		
		$data['fields'] = $details;
		$data[$prefix_config.'id'] = $id;

		$data[$prefix_config.'name'] = $name; 
		$data[$prefix_config.'title'] = get_value($details,$prefix_config.'title');   
		$data[$prefix_config.'parent_id'] = $parent_id;
		$data[$prefix_config.'description'] = get_value($details,$prefix_config.'description'); 
		$data[$prefix_config.'created'] = get_value($details,$prefix_config.'created');
		$data[$prefix_config.'modified'] = get_value($details,$prefix_config.'modified');
		$data[$prefix_config.'published'] = get_value($details,$prefix_config.'published'); 
		$where_params_parents = array( 	
			'where_params'  => array( $this->model->tblpref.'parent_id' => 0, $this->model->tblpref.'published'=>'1' )
		);  
		$data[$prefix_config.'parent_id'] = $this->model->dropdown($this->model,$parent_id,$where_params_parents ,$prefix_config.'parent_id'); //get parents
		$data['prefix_config'] = $prefix_config;
		$data['prefix_format'] = $this->SFM->tblpref; 
				
		//gets the child config
		$data['records_child_config'] = $this->Data->get_records( $this->model, array( 'where_params' => array($this->model->tblpref.'parent_id' => $id), 'title','ASC' ) );   
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/'.$this->page.'/edit',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);  
	} 

	public function save_sub_settings(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'add');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$set_id = $this->input->post('set_id'); 
		$record = $this->Data->get_records( $this->model, array( 'where_params' => array($this->model->tblpref.'parent_id' => $set_id), 'title','ASC' ) );
		
		if( $record ){
			foreach($record as $item ){
				$set_name = $item->set_name;
				$input_values = $this->input->post($set_name); 
				foreach($input_values as $setf_id=>$value){   
					$existing_record = $this->Data->get_record( $this->SRM, array( 'where_params' => array($this->SFM->tblid => $setf_id) ) );  
					$sfm = $this->Data->get_record($this->SFM, array( 'where_params' => array($this->SFM->tblid =>$setf_id) ));
					$format_type = get_value($sfm,$this->SFM->tblpref.'type');  
					
					if( $format_type == 'multiselect' || $format_type == 'checkbox'){ 
						if($existing_record){
							$delete = $this->Data->delete_data( $this->SRM, array( $this->SFM->tblid => $setf_id ) );
							
						}
						$array_setf_id = array(
							'setf_id' => $setf_id,
						);
						foreach($value as $item){
							$data = array( 
								$this->SRM->tblpref.'value' => $item,
								$this->SRM->tblpref.'published' => 1
							);    
							$data  = array_merge($data, $array_setf_id);
							$this->Data->insert_data($this->SRM, $data );
							
						}  
					} 
					else {
						
						$data = array( 
							$this->SRM->tblpref.'value' => $value,
							$this->SRM->tblpref.'published' => 1
						);    
					 
						if( $existing_record ){
							$update = $this->Data->update_data( $this->SRM, $data, array( $this->SFM->tblid => $setf_id ) ); 
						}
						else{
							$array_setf_id = array(
								'setf_id' => $setf_id,
							);
							$data  = array_merge($data, $array_setf_id);
							$this->Data->insert_data($this->SRM, $data );
						}
					}
				}
			} 
			redirect_current_page();
		}		
	}
	
	public function save_parent_settings(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'add');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/ 
		
		$set_id = $this->input->post('set_id');
		$record = $this->Data->get_record( $this->model, array( 'where_params' => array($this->model->tblid => $set_id), 'title','ASC' ) );
		if( $record ){ 
			$set_name = $record->set_name; 
			$input_values = $this->input->post($set_name);    
			if($input_values){
				foreach($input_values as $setf_id=>$value){  
					$existing_record = $this->Data->get_record( $this->SRM, array( 'where_params' => array($this->SFM->tblid => $setf_id) ) ); 
					$sfm = $this->Data->get_record($this->SFM, array( 'where_params' => array($this->SFM->tblid =>$setf_id) ));
					$format_type = get_value($sfm,$this->SFM->tblpref.'type');  
					
					if( $format_type == 'multiselect' || $format_type == 'checkbox'){ 
						if($existing_record){
							$delete = $this->Data->delete_data( $this->SRM, array( $this->SFM->tblid => $setf_id ) ); 
						}
						$array_setf_id = array(
							'setf_id' => $setf_id,
						);
						foreach($value as $item){
							$data = array( 
								$this->SRM->tblpref.'value' => $item,
								$this->SRM->tblpref.'published' => 1
							);    
							$data  = array_merge($data, $array_setf_id);
							$add = $this->Data->insert_data($this->SRM, $data ); 
						}  
					} 
					else { 
						$data = array( 
							$this->SRM->tblpref.'value' => $value,
							$this->SRM->tblpref.'published' => 1
						);    
					 
						if( $existing_record ){
							$add = $this->Data->update_data( $this->SRM, $data, array( $this->SFM->tblid => $setf_id ) ); 
						}
						else{
							$array_setf_id = array(
								'setf_id' => $setf_id,
							);
							$data  = array_merge($data, $array_setf_id);
							$add = $this->Data->insert_data($this->SRM, $data );
						}
					}  
				}
				if($add){
					$notify = 'Update Settings successful. (ID: '.$set_id.' - '.ucfirst($set_name).')';
					$this->notify->set($notify, 'success', 'update', TRUE);
				}
				else{
					$notify = 'Update Settings unsuccessful. (ID: '.$set_id.' - '.ucfirst($set_name).')';
					$this->notify->set($notify, 'warning', 'update', TRUE);
				}
			}
			redirect_current_page(); 
		} 
		
	}  
	
	public function save_multiple_settings(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'add');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$set_id = $this->input->post('set_id');  
		$setr_id = $this->input->post('setr_id');  
		$setf_id = $this->input->post('setf_id');  
		$sub_set_id = $this->input->post('sub_set_id'); 
		if($sub_set_id){
			$record = $this->Data->get_record( $this->model, array( 'where_params' => array($this->model->tblid => $sub_set_id), 'title','ASC' ) );
		}
		else{
			$record = $this->Data->get_record( $this->model, array( 'where_params' => array($this->model->tblid => $set_id), 'title','ASC' ) );	
		} 
		$existing_record = $this->Data->get_record( $this->SRM, array( 'where_params' => array($this->SRM->tblid => $setr_id, $this->SFM->tblid => $setf_id), 'title','ASC' ) ); 
		 
		if($existing_record){ 
			if( $record ){ 
				$set_name = $record->set_name; 
				$input_values = $this->input->post($set_name);   
				foreach($input_values as $setf_id=>$value){ 
					$data = array( 
						$this->SFM->tblid => $setf_id,
						$this->SRM->tblpref.'value' => $value,
						$this->SRM->tblpref.'published' => 1
					);
					$update = $this->Data->update_data($this->SRM, $data, array( $this->SRM->tblid => $existing_record->setr_id ) ); 
				}
					
				redirect_current_page();
			}
		}
		else{ 
			if( $record ){  
				$set_name = $record->set_name;  
				$input_values = $this->input->post($set_name); 			
				foreach($input_values as $setf_id=>$value){ 
					$data = array( 
						$this->SFM->tblid => $setf_id,
						$this->SRM->tblpref.'value' => $value,
						$this->SRM->tblpref.'published' => 1
					);
					$id = $this->Data->insert_data($this->SRM, $data );
				} 
				redirect( site_url('c='.$this->page.'&m=edit&set_id='.$set_id.'&setr_id='.$id) );  
			} 
		}  
	}
	
	public function delete_multiple_records(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'delete');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
	
		$id = $this->input->get('setr');
		$set_id = $this->input->get('set_id'); 
		//printr($id);
		$record = $this->Data->get_record(  $this->SRM, array( 'where_params' => array( $this->SRM->tblid => $id)) );
		$title = get_value($record, $this->SRM->tblpref.'title');
		$delete = $this->Data->delete_data( $this->SRM, array( $this->SRM->tblid => $id ) );
		//printx($this->db->last_query());
		if( $delete ){ 
			$notify = 'Deleting Multiple Records successful. (ID: '.$id.' - '.$title.')';
			$this->notify->set($notify, 'success', 'delete', TRUE);
			
		} else {
			$notify = 'Deleting Multiple Records unsuccessful. (ID: '.$id.' - '.$title.')';
			$this->notify->set($notify, 'warning', 'delete', TRUE); 
		}
		 
		redirect( site_url('c='.$this->page.'&m=edit&set_id='.$set_id) ); 
	}
	
	
}
