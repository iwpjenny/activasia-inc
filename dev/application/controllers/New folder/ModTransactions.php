<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ModTransactions extends CI_Controller {
	
	private $model;
	private $page;
	private $view;
	private $location;
	private $dashboard_page;
			
	public function index(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('files'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->files->model_files = $this->TransF;
		$data = $this->module->records( $this->model, $data, array($this->model->tblpref.'trashed'=>0) );
				
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	private function set_controller( $params ){
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->load->model('modules/Transactions_Model', 'Trans');
		/*---------------------------------------------*/
		$this->model = $this->Trans;
		$this->page = $this->Trans->page;
		$this->view = $this->Trans->view;
		
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		$this->module->location = $this->location;
		
		$this->model->ini_custom_models();
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		
		return $data;
	}
	
	public function view(){	
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('files'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/		
		$data['form_action'] = site_url('c='.$this->page.'&m=save');
		/*---------------------------------------------*/
		$id = $this->input->get('id');
		$data['id'] = $id;
		/*---------------------------------------------*/ 
		
		$fields = $this->module->record( $this->model, $id );
		$data['fields'] = $fields;
		
		$this->files->id = $id;
		$this->files->model_main = $this->model;
		$this->files->model_files = $this->TransF;
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->model->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	public function edit(){	
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('files'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/		
		$data['form_action'] = site_url('c='.$this->page.'&m=save');
		/*---------------------------------------------*/
		$id = $this->input->get('id');
		$data['id'] = $id;
		/*---------------------------------------------*/ 
		
		$data['sub_section_1'] = $this->module->modal_sub_section( $this->TransSub, $this->model, $id );
		
		$fields = $this->module->record( $this->model, $id );
		$this->moduletag->model = $this->model;
		$this->moduletag->data = $fields;
		$data['fields'] = $fields;
		
		$data['select_member'] = $this->Mem->get_moduletag_select( $this->model, $fields );
		
		$this->files->id = $id;
		$this->files->model_main = $this->model;
		$this->files->model_files = $this->TransF;
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->model->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	 
	public function save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->save( $this->model, $this->TransF );
	}
	
	public function delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->delete( $this->model, $this->TransF );
	}
			
	public function trashed(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('helper'=>array('fields'),'library'=>array('files'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/		
		$data['form_action'] = site_url('c='.$this->page.'&m='.__FUNCTION__);
		/*---------------------------------------------*/
		
		$this->files->model_files = $this->TransF;
		$data = $this->module->records( $this->model, $data, array($this->model->tblpref.'trashed'=>1) );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	public function trash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->trash( $this->model );
	}
	
	public function untrash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->trash( $this->model, 0 );
	}
	
	public function delfile(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*---------------------------------------------*/
		
		$this->module->delete_file( $this->model, $this->TransF );
	}
}