<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModCashAdvanceStatus extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;
	private $login_user_type;
	private $dashboard_page;

	public function index(){		
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>'records','method'=>array('library'=>array('ModUser')));
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/ 
		
		$data = $this->module->records( $this->model, $data, array() );	 
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	private function set_controller( $params ){  
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->load->model('modules/Cash_Advance_Status_Model', 'CAS');
		/*---------------------------------------------*/
		$this->model = $this->CAS;
		$this->page = $this->CAS->page;
		$this->view = $this->CAS->view;
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		/*---------------------------------------------*/
		$this->model->ini_custom_models();
		/*---------------------------------------------*/
		/*Files*/
		/*---------------------------------------------*/
		$this->load->library('Module');
		$this->module->model = $this->model; 
		$this->module->model_user = $this->User;
		$this->module->location = $this->location;	
		/*---------------------------------------------*/
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		/*---------------------------------------------*/
		$this->load->library('RecordsFilter',array('model'=>$this->model));
		
		return $data; 
	}

	public function view(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('helper'=>array('fields'),'library'=>array('pic'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data['form_action'] = site_url('c='.$this->page.'&m=save');
		/*---------------------------------------------*/
		$id = $this->input->get('id');
		$data['id'] = $id;
		/*---------------------------------------------*/

		$fields = $this->module->record( $this->model, $id );
		$data['fields'] = $fields;

		$this->userslogs->save( 'view', 'Viewed Processed Cash Advance Status ('.$id.')' );

		$latitude =  get_value( $fields, $this->model->tblpref.'latitude' );
		$longitude =  get_value( $fields, $this->model->tblpref.'longitude' );
		//$data['map'] = $this->googlemap->generate_map( $latitude, $longitude );

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function edit(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('member_models','helper'=>array('fields'),'library'=>array('files'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$data['form_action'] = site_url('c='.$this->page.'&m=save');
		/*---------------------------------------------*/
		$id = $this->input->get('id'); 
		$wb_id = $this->input->get('wb_id'); 
		$data['id'] = $id;
		$data['wb_id'] = $wb_id;
		/*---------------------------------------------*/
		   
		$fields = $this->module->record( $this->model, $id );
		$this->moduletag->model = $this->model;
		$this->moduletag->data = $fields;
		$data['fields'] = $fields;  
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'add');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$this->module->save( $this->model );
	}

	public function delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->delete( $this->model );
	}

	public function trashed(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/			
		
		$data['form_action'] = site_url('c='.$this->page.'&m='.__FUNCTION__);
		/*---------------------------------------------*/

		$data = $this->module->records( $this->model, $data, array($this->model->tblpref.'trashed'=>1) );

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function trash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->trash( $this->model );
	}

	public function untrash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->trash( $this->model, 0 );
	}

	public function export(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'export','method'=>array('library'=>array('excel'))
		);  
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		//$filename = "Cost-Estimate-Records-".current_date().".csv";
	
		$date_now = date('Y-m-d-H-i');
		$filename='Cash Advance List -'.$date_now; 
		$ordertype = $this->input->get('sort'); 
		$result = $this->model->get_report(); 
		$columns = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 20,'L' => 20,'M' => 20,'N' => 20,'O' => 20,'P' => 20,'Q' => 20,'R' => 20,'S' => 20,'T' => 20,
		);
		if( $result ){
			$this->excel->export_report( $result, $filename, $columns );
		}  
	}
}
