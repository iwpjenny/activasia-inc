<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ModTesting extends CI_Controller {
	
	private $model;
	private $page;
	private $view;
	private $location;
	private $dashboard_page;
			
	public function index(){ 
	
		
		// $this->send_ce();
		// $this->send_bcs();
		// $this->send_ca();
		$this->send_multiple_ca();
		 
	}
	
	private function set_controller( $params ){
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		/*---------------------------------------------*/
		$this->model = $this->CE;
		$this->page = $this->CE->page;
		$this->view = $this->CE->view;
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		/*---------------------------------------------*/
		$this->model->ini_custom_models();
		/*---------------------------------------------*/
		/*Files*/
		/*---------------------------------------------*/
		$this->load->library('Module');
		$this->module->model = $this->model; 
		$this->module->model_user = $this->User;
		$this->module->location = $this->location;	
		/*---------------------------------------------*/
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		/*---------------------------------------------*/
		$this->load->library('RecordsFilter',array('model'=>$this->model));
		
		return $data;
	}
	 
	public function send_ce( $ce_id = 1, $ce_log_id = 1){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'index','method'=>array('library'=>array('excel')));
		$data = $this->set_controller( $controller_params );
		/*---------------------------------------------*/
		 
		$date_now = date('Y-m-d-H-i');
		/* START of Updated Information */
		
		if (!is_dir('uploads/emails/'.$this->CE->view.'/')) {
			mkdir('./uploads/emails/'.$this->CE->view.'/', 0777, TRUE);

		}  
		$filename='Cost Estimate List -'.$date_now; 
		$result_main = $this->CE->get_report_individual( $ce_id); 
		$file_path = 'uploads/emails/'.$this->CE->view.'/'.$filename.'.xls'; 
		 $columns = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 30,'L' => 30,'M' => 30
		);
		if( $result_main ){
			//forattachment 
			$this->excel->export_report_individual_nodownload( $result_main, $filename, $file_path, $columns ); 
		}   
		/* END of Updated Information */
		
		/* START of Log Information */
		if (!is_dir('uploads/emails/'.$this->CEL->view.'/')) {
			mkdir('./uploads/emails/'.$this->CEL->view.'/', 0777, TRUE);

		}  
		$file_path_logs = 'uploads/emails/'.$this->CEL->view.'/'.$filename.'.xls'; 
		$filename_logs ='Log For Cost Estimate List -'.$date_now; 
		$result_logs = $this->CEL->get_report_individual( $ce_log_id ); 
		$columns_logs = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 30,'L' => 30,'M' => 30
		); 
		if( $result_logs ){
			//forattachment 
			$this->excel->export_report_individual_nodownload( $result_logs, $filename_logs, $file_path_logs, $columns_logs );
		}  
		/* END of Log Information */
		 
		$ce_detail = $this->module->record( $this->CE, $ce_id);  
		$created_by_user_id = get_value($ce_detail, $this->CE->tblpref.'created_by_user_id');
		$creator_detail = $this->User->get_by_id( $created_by_user_id );  		 
		$data['firstname'] = get_value($creator_detail, $this->User->tblpref.'firstname');
		$data['lastname'] = get_value($creator_detail, $this->User->tblpref.'lastname'); 
		$receiver_email = get_value($creator_detail, $this->User->tblpref.'email'); 
		$user_detail = $this->User->get_logged_user(); 
		$data['username'] = get_value($user_detail, $this->User->tblpref.'username');
		$data['date_now'] = date("jS F, Y", strtotime($date_now));
		
		$htmlContent = $this->load->view('backend/modules/cost-estimate/email-template',$data, TRUE );
		  
		$this->load->library('email'); 
		$config['mailtype'] = 'html';
		$this->email->initialize($config); 
		$subject = 'CE Overwrite:'; 
		$this->email->from('your@example.com', 'Your Name');
		$this->email->to('rubin@iwebprovider.com');  
		$this->email->subject($subject);
		$this->email->message($htmlContent);
		
		$this->email->attach($file_path);
		$this->email->attach($file_path_logs);
		  
		if (!$this->email->send()) {
			show_error($this->email->print_debugger()); }
		else {
			echo 'Your e-mail has been sent!';
		} 
	}
	
	public function send_bcs( $bcs_id = 1, $bcs_log_id = 1){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'index','method'=>array('library'=>array('excel')));
		$data = $this->set_controller( $controller_params );
		/*---------------------------------------------*/
		$this->load->model('modules/Working_Budget_Model', 'WB'); 
		$this->load->model('modules/Working_Budget_Logs_Model', 'WBL'); 
		$this->load->model('modules/Working_Budget_Items_Model', 'WBI');
		$this->load->model('modules/Working_Budget_Items_Logs_Model', 'WBIL');
		/*---------------------------------------------*/
		 
		$date_now = date('Y-m-d-H-i');
		/* START of Updated Information */
		
		if (!is_dir('uploads/emails/'.$this->WB->view.'/')) {
			mkdir('./uploads/emails/'.$this->WB->view.'/', 0777, TRUE);

		}   
		
		$filename='Budget Control System -'.$date_now; 
		$ordertype = $this->input->get('sort'); 
		$result_main = $this->WB->get_report_individual($bcs_id ); 
		$file_path = 'uploads/emails/'.$this->WB->view.'/'.$filename.'.xls'; 
		$columns = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 20,'L' => 20,
		);
		 
		if( $result_main['head']['BCS Code'] ){ 
			//forattachment 
			$this->excel->export_report_individual_nodownload( $result_main, $filename, $file_path, $columns ); 
		}   
		/* END of Updated Information */
		
		/* START of Log Information */
		if (!is_dir('uploads/emails/'.$this->WBL->view.'/')) {
			mkdir('./uploads/emails/'.$this->WBL->view.'/', 0777, TRUE);

		}  
		$file_path_logs = 'uploads/emails/'.$this->WBL->view.'/'.$filename.'.xls'; 
		$filename_logs ='Log For Budget Control List -'.$date_now; 
		$result_logs = $this->WBL->get_report_individual( $bcs_log_id ); 
		$columns_logs = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 30,'L' => 30,'M' => 30
		); 
		if( $result_logs['head']['BCS Code'] ){ 
			//forattachment 
			$this->excel->export_report_individual_nodownload( $result_logs, $filename_logs, $file_path_logs, $columns_logs );
		}  
		/* END of Log Information */
		 
		$ce_detail = $this->module->record( $this->WB, $bcs_id);  
		$created_by_user_id = get_value($ce_detail, $this->WB->tblpref.'created_by_user_id');
		$creator_detail = $this->User->get_by_id( $created_by_user_id );  		 
		$data['firstname'] = get_value($creator_detail, $this->User->tblpref.'firstname');
		$data['lastname'] = get_value($creator_detail, $this->User->tblpref.'lastname'); 
		$receiver_email = get_value($creator_detail, $this->User->tblpref.'email'); 
		$user_detail = $this->User->get_logged_user(); 
		$data['username'] = get_value($user_detail, $this->User->tblpref.'username');
		$data['date_now'] = date("jS F, Y", strtotime($date_now));
		
		$htmlContent = $this->load->view('backend/modules/cost-estimate/email-template',$data, TRUE );
		
		$htmlContent = $this->load->view('backend/modules/'.$this->WB->view.'/email-template',$data, TRUE ); 
		  
		$this->load->library('email'); 
		$config['mailtype'] = 'html';
		$this->email->initialize($config); 
		$subject = 'WB Overwrite:'; 
		$this->email->from('your@example.com', 'Your Name');
		$this->email->to('rubin@iwebprovider.com');  
		$this->email->subject($subject);
		$this->email->message($htmlContent);
		
		$this->email->attach($file_path);
		$this->email->attach($file_path_logs);
		  
		if (!$this->email->send()) {
			show_error($this->email->print_debugger()); }
		else {
			echo 'Your e-mail has been sent!';
		} 
	}
	public function send_ca( $ca_id = 1, $ca_log_id = 1){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'index','method'=>array('library'=>array('excel')));
		$data = $this->set_controller( $controller_params );
		/*---------------------------------------------*/ 
		$this->load->model('modules/Cash_Advance_Status_Model', 'CAS');
		$this->load->model('modules/Cash_Advance_Model', 'CA');
		$this->load->model('modules/Cash_Advance_Logs_Model', 'CAL');
		$this->load->model('modules/Cash_Advance_Items_Model', 'CAI');
		$this->load->model('modules/Cash_Advance_Items_Logs_Model', 'CAIL');
		/*---------------------------------------------*/
		 
		$date_now = date('Y-m-d-H-i');
		/* START of Updated Information */
		
		if (!is_dir('uploads/emails/'.$this->CA->view.'/')) {
			mkdir('./uploads/emails/'.$this->CA->view.'/', 0777, TRUE); 
		}   
		
		$filename='Cash Advance System -'.$date_now; 
		$ordertype = $this->input->get('sort'); 
		$result_main = $this->CA->get_report_individual($ca_id ); 
		$file_path = 'uploads/emails/'.$this->CA->view.'/'.$filename.'.xls'; 
		$columns = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 20,'L' => 20,
		);
		if( $result_main['head']['CA #'] ){ 
			// forattachment  
			$this->excel->export_report_individual_nodownload( $result_main, $filename, $file_path, $columns ); 
		}   
		/* END of Updated Information */
		
		/* START of Log Information */
		if (!is_dir('uploads/emails/'.$this->CAL->view.'/')) {
			mkdir('./uploads/emails/'.$this->CAL->view.'/', 0777, TRUE); 
		}  
		$file_path_logs = 'uploads/emails/'.$this->CAL->view.'/'.$filename.'.xls'; 
		$filename_logs ='Log For Cash Advance List -'.$date_now; 
		$result_logs = $this->CAL->get_report_individual( $ca_log_id );  
		$columns_logs = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 30,'L' => 30,'M' => 30
		);  
		if( $result_logs['head']['CA #'] ){ 
			// forattachment 
			$this->excel->export_report_individual_nodownload( $result_logs, $filename_logs, $file_path_logs, $columns_logs );
		}  
		/* END of Log Information */
		 
		$ce_detail = $this->module->record( $this->CA, $ca_id );  
		$created_by_user_id = get_value($ce_detail, $this->CA->tblpref.'created_by_user_id');
		$creator_detail = $this->User->get_by_id( $created_by_user_id );  		 
		$data['firstname'] = get_value($creator_detail, $this->User->tblpref.'firstname');
		$data['lastname'] = get_value($creator_detail, $this->User->tblpref.'lastname'); 
		$receiver_email = get_value($creator_detail, $this->User->tblpref.'email'); 
		$user_detail = $this->User->get_logged_user(); 
		$data['username'] = get_value($user_detail, $this->User->tblpref.'username');
		$data['date_now'] = date("jS F, Y", strtotime($date_now));
		
		$htmlContent = $this->load->view('backend/modules/'.$this->CA->view.'/email-template',$data, TRUE ); 
		  
		$this->load->library('email'); 
		$config['mailtype'] = 'html';
		$this->email->initialize($config); 
		$subject = 'CA Overwrite:';  
		$this->email->from('Admin@activasia.iwp.ph', 'ActivAsia Admin');
		$this->email->to('rubin@iwebprovider.com');  
		$this->email->subject($subject);
		$this->email->message($htmlContent);
		
		$this->email->attach($file_path);
		$this->email->attach($file_path_logs);
		  
		if (!$this->email->send()) {
			show_error($this->email->print_debugger()); }
		else {
			echo 'Your e-mail has been sent!';
		} 
	}

	public function send_multiple_ca(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'index','method'=>array('library'=>array('email')));
		$data = $this->set_controller( $controller_params ); 
		/*---------------------------------------------*/ 
		$this->load->model('modules/Cash_Advance_Status_Model', 'CAS');
		$this->load->model('modules/Cash_Advance_Model', 'CA');
		$this->load->model('modules/Cash_Advance_Logs_Model', 'CAL');
		$this->load->model('modules/Cash_Advance_Items_Model', 'CAI');
		$this->load->model('modules/Cash_Advance_Items_Logs_Model', 'CAIL');
		/*---------------------------------------------*/ 
		
		$result = $this->Data->get_record($this->UR, array('where_params'=>array( $this->UR->tblpref.'title like '=> '%Manager%' )));
		$ur_id = get_value($result, $this->UR->tblid );
		
		$recepients = $this->Data->get_records($this->User, array('where_params'=>array( $this->UR->tblid => $ur_id )));
		
		$join_params = $this->CA->set_join_params(); 
		$query_params_release = array(
			'limit'=>5,
			'join_params' => $join_params,
			'orderby' => $this->CA->tblpref.'created',
			'where_params' => array ( 
				$this->CA->tblpref.'published' => 1, 
				$this->CA->tblpref.'status' => 'Ready', 
				$this->CA->tblpref.'trashed' => 0 
		));
	
		$records = $this->Data->get_records( $this->CA, $query_params_release ); 
		$result_count = count($records);
		 
		foreach($recepients as $recepient){ 
			$email = get_value($recepient, $this->User->tblpref.'email');  
			 
			if( $result_count ){ 
			
				$subject = 'CARF Approval';   
				$message = '<table border="0" cellpadding="0" cellspacing="0">';
				$message .= '<tr><td><p>You have '.$result_count.' cash advance requests for approval.</b></p></td></tr></table>';			
					 
					  
				$config['protocol']    = 'smtp';
				$config['smtp_host']    = 'ssl://smtp.gmail.com';
				$config['smtp_port']    = '465';
				$config['smtp_timeout'] = '7';
				$config['smtp_user']    = 'marktestingemail@gmail.com';
				$config['smtp_pass']    = 'iWPEC@16';
				$config['charset']    = 'utf-8';
				$config['newline']    = "\r\n";
				$config['mailtype'] = 'html'; // or html
				$config['validation'] = TRUE; // bool whether to validate email or not      

				$this->email->initialize($config);
				$this->email->clear(TRUE);

				$this->email->from('rubin@iwebprovider.com'); 
				$this->email->to($email );  
				$this->email->subject($subject);
				$this->email->message($message);
				  
				if (!$this->email->send()){
					show_error($this->email->print_debugger()); 
				} else {
					echo 'Your e-mail has been sent!';
				}    
			
			}
		}  
		
		
	}
}