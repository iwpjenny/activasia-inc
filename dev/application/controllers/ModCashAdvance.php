<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModCashAdvance extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;
	private $login_user_type;
	private $dashboard_page;

	public function index(){		
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('ModUser'))
		);
		$data = $this->set_controller( $controller_params );  
		/*set end---------------------------------------------*/ 

		/*-------------CUSTOM CODE-------------*/
		$data['show_status']['show_status_approve_button'] = $this->usersrole->check( $this->model->urc_name, 'approve');
		$data['show_status']['show_status_decline_button'] = $this->usersrole->check( $this->model->urc_name, 'decline');
		$data['show_status']['show_status_pending_button'] = $this->usersrole->check( $this->model->urc_name, 'pending');
		$data['show_status']['show_status_release_button'] = $this->usersrole->check( $this->model->urc_name, 'release');
		$data['show_status']['show_status_ready_button'] = $this->usersrole->check( $this->model->urc_name, 'ready');
		/*---------END----CUSTOM CODE-------------*/
 
		$data = $this->module->get_data_records( $data );  

		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	private function set_controller( $params ){ 
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->load->model('modules/Cash_Advance_Model', 'CA');
		/*---------------------------------------------*/
		$this->model = $this->CA;
		$this->page = $this->CA->page;
		$this->view = $this->CA->view;
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		/*---------------------------------------------*/
		$this->model->ini_custom_models();
		/*---------------------------------------------*/
		/*Files*/
		/*---------------------------------------------*/
		$this->load->library('Module');
		$this->module->model = $this->model; 
		$this->module->model_user = $this->User;
		$this->module->location = $this->location;	
		/*---------------------------------------------*/
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		/*---------------------------------------------*/
		$this->load->library('RecordsFilter',array('model'=>$this->model));
		/*---------------------------------------------*/
		$params = array('model_parent'=>$this->model,'location'=>$this->location);
		$this->load->library('ModuleSubItems',$params);
		
		return $data;
	}

	public function view(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('ModUser'))
		); 
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->module->get_data_view( $data );

		$fields = $data['fields']; 
		$id = $data['id']; 
		if( $fields ){
			$wb_id = get_value( $fields, $this->WB->tblid );
			$bcs = get_value( $fields, $this->WB->tblpref.'bcs'); 
			$ca_amount = $this->WB->get_total_amount($wb_id, $id ) ;
			 
		} 
		// $balance = $this->model->get_balance($bcs, $wb_id);  
		$balance = $this->WB->get_total_balance_from_wb($wb_id);
		$data['balance'] = $balance;  
		$data['ca_amount'] = $ca_amount; 
		$data['balance'] = $balance; 
		$wb_id =  get_value( $fields, $this->WB->tblid ); 
		$user_approved_id = get_value( $fields, $this->model->tblpref.'user_approved'); 
		$operator_id = get_value( $fields, $this->model->tblpref.'created_by_user_id');
		$ca_status = get_value( $fields, $this->model->tblpref.'status');
		$data['operator_id'] = $operator_id;  
		$data['operator_fullname'] = $this->moduser->get_user_fullname_by_id( $operator_id, '{lastname}, {firstname}');
		$data['user_approved_id'] = $user_approved_id;
		$data['user_approved'] = $this->moduser->get_user_fullname_by_id( $user_approved_id, '{lastname}, {firstname}');

	 	$ca_details = $this->Data->get_record( $this->CAS, array( 'where_params' => array( $this->model->tblid => $id ),'orderby'=>$this->CAS->tblpref.'created', 'ordertype'=>'DESC' ) ); 
		
		$user_complete_name = '';
		if ( $ca_details ){ 
			$ca_status_user_id = get_value( $ca_details, $this->User->tblid );
			$user_complete_name = $this->moduser->get_user_fullname_by_id( $ca_status_user_id, '{lastname}, {firstname}'); 
		} 
		 
		$ca_latest_status_user_activity = get_value( $ca_details, $this->CAS->tblpref.'status'); 
		$ca_latest_status_created = get_value( $fields, $this->model->tblpref.'modified');
		$data['date_released'] = get_value( $fields, $this->model->tblpref.'date_released'); 
		if( $ca_latest_status_user_activity ){
			$ca_latest_status_user_activity = $ca_latest_status_user_activity;
		} else {
			$ca_latest_status_user_activity = $ca_status;
		} 
		$user_pproved = get_value( $fields, $this->model->tblpref.'user_approved');
		$data['user_pproved'] = $user_pproved;
		$data['ca_status_user_id'] = isset($ca_status_user_id)?$ca_status_user_id:'';  
		$data['ca_latest_status_created'] = isset($ca_latest_status_created)?$ca_latest_status_created:'';  
		$data['ca_latest_status_user'] = isset($user_complete_name)?$user_complete_name:'';  
		$data['ca_latest_status_user_activity'] = isset($ca_latest_status_user_activity)?$ca_latest_status_user_activity:'';
		$data['user_approved'] =  $this->moduser->get_user_fullname_by_id( $user_pproved, '{lastname},	 {firstname}');
		$user_id = get_value( $fields, $this->model->tblpref.'created_by_user_id');   
		$status = get_value( $fields, $this->model->tblpref.'status','Pending'); 
		$data['status'] = $status;
		$data['name_of_employee'] = $this->moduser->get_user_fullname_by_id( $user_id, '{lastname}, {firstname}' );
		$data['user_id1'] = $user_id;    
	    $user_details = $this->Data->get_record( $this->User, array(
				'where_params' => array( $this->User->tblid => $user_id ) 
		));  
		$data['user_details'] = $user_details;
		
		//items 
		$data['sub_section_1'] = $this->module->modal_sub_section_view( $this->CAI, $data['id'] ); 
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function edit(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('ModUser'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$data = $this->module->get_data_edit( $data );
	
		$wb_id = $this->input->get('wb_id'); 
		$fields = $data['fields'];    
		$id = $data['id']; 
		$data['wb_id'] = $wb_id;
		/*---------------------------------------------*/ 
		/*-------------CUSTOM CODE-------------*/
		$data['show_status']['show_status_approve_button'] = $this->usersrole->check( $this->model->urc_name, 'approve');
		$data['show_status']['show_status_decline_button'] = $this->usersrole->check( $this->model->urc_name, 'decline');
		$data['show_status']['show_status_pending_button'] = $this->usersrole->check( $this->model->urc_name, 'pending');
		$data['show_status']['show_status_release_button'] = $this->usersrole->check( $this->model->urc_name, 'release');
		$data['show_status']['show_status_ready_button'] = $this->usersrole->check( $this->model->urc_name, 'ready');

		$join_params_ca = $this->model->set_join_params();
		$join_params = $this->WB->set_join_params();
		
		if( $id ){ 
			$details = $this->Data->get_record( $this->model, array(
				'where_params' => array( $this->model->tblid => $id ),
				'join_params' => $join_params_ca
			)); 
		
		} 
		if( $wb_id ){ ;
			$details = $this->Data->get_record( $this->WB, array(
				'where_params' => array( $this->WB->tblid => $wb_id ),
				'join_params' => $join_params
			));
				
		}
		
		/* Get Remaining Balance */
		if( empty( $details ) ){
			$details = '';
			$remained_balance = 0;
			$data['employee'] = '';
		} 
		if( $details ){
			$wb_id = get_value( $details, $this->WB->tblid );
			$bcs = get_value( $details, $this->WB->tblpref.'bcs');
			$ca_amount = $this->WB->get_total_amount($wb_id, $id ) ;
			$data['ca_amount'] = $ca_amount; 
		}
		
		if( $id ){
			$user_id = get_value( $details, $this->model->tblpref.'created_by_user_id'); 
			$user_login_details = $this->User->get_logged_user();
			$login_user_id = get_value( $user_login_details, $this->User->tblid );
			$data['login_user_id'] = $login_user_id;

		} else {
			$user_details = $this->User->get_logged_user();
			$user_id = get_value( $user_details, $this->User->tblid );
			$data['login_user_id'] = $user_id;
		} 
		// $balance = $this->CA->get_balance($bcs, $wb_id); 
		$balance = $this->WB->get_total_balance_from_wb($wb_id);
		// printx($balance);
		// $new_balance = $bcs - $ca_amount;
		$data['balance'] = $balance;  
		$data['wk_number'] = get_value( $details, $this->model->tblpref.'wk_number','WK '.date('W'));
		$data['date_requested'] = get_value( $details, $this->model->tblpref.'date_requested',current_date());
		$data['date_needed'] = get_value( $details, $this->model->tblpref.'date_needed', date("Y-m-d", strtotime("+ 1 day")));
		$data['date_released'] = get_value( $details, $this->model->tblpref.'date_released',current_date());
		$ca_number= get_value( $details, $this->model->tblpref.'ca_number');
		$data['ca_number'] = get_value( $details, $this->model->tblpref.'ca_number');
		if( $wb_id ){
			$data['wb_id'] = get_value( $details, $this->WB->tblid);
		} 
		if( empty($ca_number) ){
			$data['ca_number'] = 'Not Yet Generated';
		}
		if( empty($id)){
			$data['new_ca_number'] = 'CA'.date('ymdHis');
		} elseif( $id ){
			$data['new_ca_number'] = $ca_number;
		}
		$ca_number = get_value( $details, $this->model->tblpref.'ca_number');
		$status = get_value( $details, $this->model->tblpref.'status','Pending');
		$data['status'] = $status;
		$data['payment_request'] = get_value( $details, $this->model->tblpref.'payment_request');
		$data['payroll_request'] = get_value( $details, $this->model->tblpref.'payroll_request');
		$data['activity_name'] = get_value( $details, $this->CE->tblpref.'activity');
		$ce_date = get_value( $details, $this->CE->tblpref.'date');
		$ce_date_format = 'F\-y ';
		$data['month_of_activity'] = datetime($ce_date,$ce_date_format);  
		$data['remarks'] = get_value( $details, $this->model->tblpref.'remarks');
		$data['ca_status'] = get_value( $details, $this->model->tblpref.'status');
		$data['position'] = get_value( $details, $this->UR->tblpref.'position'); 
		$data['item_name'] = get_value( $details, $this->WB->tblpref.'item' ); 
		$data['area_name'] = get_value( $details, $this->Area->tblpref.'title' ); 
		$approved_by = get_value( $details, $this->model->tblpref.'user_approved');
		$data['approved_by_id'] = $approved_by;
		$data['approved_by'] = $this->moduser->get_user_fullname_by_id( $approved_by, '{lastname}, {firstname}' );
		$ce_number = get_value( $details, $this->CE->tblpref.'number');
		$ca_modified_date = get_value( $details, $this->model->tblpref.'modified');
		$ca_status = get_value( $details, $this->model->tblpref.'status');
		$data['ce_number'] = $ce_number;
		if( $id || $wb_id ){
			
			$user_id = get_value( $details, $this->User->tblid);
			$created_by = get_value( $details, $this->model->tblpref.'created_by_user_id');
			$bank_account = get_value( $details, $this->User->tblpref.'bank_account');
			
			if( $wb_id ){ 
				$return_user_details = $this->User->get_logged_user();
				$created_by = get_value( $return_user_details, $this->User->tblid);
			}
			$user_details = $this->Data->get_record( $this->User, array(
				'where_params' => array( $this->User->tblid => $created_by ) 
			));  
			$ur_id = get_value( $user_details, $this->UR->tblid  ); 
			$data['employee'] = $this->moduser->get_user_fullname_by_id( $created_by, '{lastname}, {firstname}' );
			
			if( empty( $bank_account ) ){
				$bank_account = get_value( $user_details, $this->User->tblpref.'bank_account');
				
			}
			$data['bank_account'] = $bank_account;
		}
		if( $id ){
			$data['bank_account'] = $bank_account;
			$edit_employee = TRUE;
			if( $edit_employee ){ 
				$data['name_of_employee'] = $this->moduser->get_login_user_fullname( '{lastname}, {firstname}' );
			} else {
				$data['name_of_employee'] = $this->moduser->get_user_fullname_by_id( $user_id, '{lastname}, {firstname}' );
			}
		} else {
			$data['name_of_employee'] = $this->moduser->get_login_user_fullname( '{lastname}, {firstname}' );
		}	 

		//$ca_details = $this->Data->get_record( $this->CAS, array( 'where_params' => array( $this->model->tblid => $id ), 'orderby' => 'created', 'ordertype' => 'DESC' ) ); 
		$ca_details = $this->Data->get_record( $this->CAS, array(
			'where_params' => array( $this->model->tblid => $id ),
		 	'orderby' =>  $this->CAS->tblpref.'created', 
		 	'ordertype' => 'DESC'  
		)); 
		$user_complete_name = ''; 
		if( $ca_details ){ 
			$ca_status_user_id = get_value( $ca_details, $this->User->tblid );
			$user_complete_name = $this->moduser->get_user_fullname_by_id( $ca_status_user_id, '{lastname} {firstname}');
		} 
		
		$ca_latest_status_created = get_value( $ca_details, $this->CAS->tblpref.'created');
		$ca_latest_status_user_activity = get_value( $ca_details, $this->CAS->tblpref.'status','Pending');
		$ca_latest_status_created = $ca_modified_date;
		
		if( $ca_latest_status_user_activity ){
			$ca_latest_status_user_activity = $ca_latest_status_user_activity;
		} else {
			$ca_latest_status_user_activity = $ca_status;
		}

		$data['ca_latest_status_created'] = isset($ca_latest_status_created)?$ca_latest_status_created:'';  
		$data['ca_latest_status_user'] = isset($user_complete_name)?$user_complete_name:'';  
		$data['ca_latest_status_user_activity'] = isset($ca_latest_status_user_activity)?$ca_latest_status_user_activity:'';
		$data['ca_status_user_id'] = isset($ca_status_user_id)?$ca_status_user_id:'';
		/*-------END---CUSTOM CODE-------------*/
		
		/*----Items-----------*/ 
		$data['sub_section_1'] = $this->module->modal_sub_section( $this->CAI, $id );
		// $data['sub_section_1'] = $this->modulesubitems->modal( $this->CAI, $this->CAI, $data['id'] );
		
		$data['items_table'] = $this->CAI->get_checked_items($id);
		$fields = $this->module->record( $this->model, $id );
		if($fields == NULL){
			$fields = $this->module->record( $this->WB, $wb_id );
		}
		else{
			if($status == 'Released'){ 
				$notify_params = array('action'=>'update','type'=>'error','text'=>'You cannot edit released CA.'); 
				$this->notify->set( $notify_params );
				redirect( site_url('c='.$this->model->page) ); 
			}
		}
		// printx($fields);
		$data['fields'] = $fields;
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}

	public function save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->save();
	}
	
	public function delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('ModuleLogs',$params);
		$this->modulelogs->create();
		
		$this->module->delete();
	}
			
	public function trashed(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,'method'=>array('library'=>array('ModUser')));
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$data = $this->module->get_data_records( $data, 1 );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/'.__FUNCTION__,$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	public function trash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->trash();
	}
	
	public function untrash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,'library'=>'ModUser');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$id = $this->input->get('id');


		$total_working_budget = $this->model->get_total_working_budget( $id );
	 
		if( $total_working_budget == TRUE ){
			$this->module->untrash();
		} else { 
			$page = $this->model->page;
			redirect( site_url('c='.$page.'&m=trashed') ); 
		} 
		
	}
	
	public function delfile(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*---------------------------------------------*/
		
		$this->module->delete_file();
	}
	
	public function export(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'export','method'=>array('library'=>array('excel','ModUser'))
		);  
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		//$filename = "Cost-Estimate-Records-".current_date().".csv";
	
		$date_now = date('Y-m-d-H-i');
		$filename='Cash Advance List -'.$date_now; 
		$ordertype = $this->input->get('sort'); 
		$quarter = $this->input->post('quarter');
		$year = $this->input->post('year'); 
		$result = $this->model->get_report($quarter, $year); 
		$columns = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 20,'L' => 20
		);
		if( $result ){
			$this->excel->export_report( $result, $filename, $columns );
		}  else {
			$notify_params = array('text'=>'Sorry, there is no data for that selection.','action'=>'export','type'=>'error','log'=>FALSE); 
			$this->notify->set( $notify_params );
			redirect( site_url( 'c='.$this->page ) );
		}
	}
	
	public function export_individual(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>'export','method'=>array('library'=>array('excel'))
		);  
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		//$filename = "Cost-Estimate-Records-".current_date().".csv";
	
		$date_now = date('Y-m-d-H-i');
		$filename='Cash Advance -'.$date_now; 
		$ordertype = $this->input->get('sort'); 
		$id = $this->input->get('id'); 
		$result = $this->model->get_report_individual($id); 
		$columns = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30
		);
		if( $result ){
			$this->excel->export_report_individual_ca( $result, $filename, $columns );
		}  
	}

	public function approve(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$user_login_details = $this->User->get_logged_user();
		$login_user_id = get_value( $user_login_details, $this->User->tblid ); 
		$id = $this->input->post('id');
		$remarks = $this->input->post('remarks');
		$log_params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('ModuleLogs',$log_params);
		$parent_id = $this->modulelogs->create();

		$log_items_params = array('model'=>$this->CAI,'id'=>$id,'parent_id'=>$parent_id,'type'=>__FUNCTION__); 
		$this->CAIL->create_logs_items($log_items_params);    
		
		$data = array( 
			$this->model->tblid => $id,
			$this->CAS->tblpref.'status'=>'Approved',
			$this->User->tblid => $login_user_id,
		);
		
		$update_data = array(
			$this->model->tblpref.'user_approved' => $login_user_id,
			$this->model->tblpref.'approve_remark' => $remarks 
		);
		
		$insert_data = $this->Data->insert_data( $this->CAS, $data ); 
		$updating_data = $this->Data->update_data( $this->model, $update_data, array( $this->model->tblid => $id ) );
		
		/*---------------------------------------------*/
		$params = array(
			$this->model->tblpref.'status'=>'Approved',
			
		); 
		// $id = $this->input->get('id');
		$approve = $this->model->set_cash_advance_status( $id, $params ); 
		
		if( $approve ){    
			// $this->model->notify_requester($id);
			$notify_params = array('action'=>'update','type'=>'success','text'=>'Approving cash advance request successful.');
			$this->notify->set( $notify_params );   
			$this->model->send_email_confirmation( $id, $parent_id, 'approve', $remarks );
			
		} else { 
			$notify_params = array('action'=>'update','type'=>'error','text'=>'Approving cash advance request unsuccessful.');
			$this->notify->set( $notify_params ); 
		}  
		
		redirect_current_page();
	}

	public function check_for_approval(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$recipients = $this->UR->get_email_recepients('approve'); 
		$user_login_details = $this->User->get_logged_user();
		$login_user_id = get_value( $user_login_details, $this->User->tblid ); 
		// $id = $this->input->get('id');
		$id = $this->input->post('id');
		$remarks = $this->input->post('remarks');
		$log_params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('ModuleLogs',$log_params);
		$parent_id = $this->modulelogs->create();

		$log_items_params = array('model'=>$this->CAI,'id'=>$id,'parent_id'=>$parent_id,'type'=>__FUNCTION__); 
		$this->CAIL->create_logs_items($log_items_params);    
		
		
		$data = array( 
			$this->model->tblid => $id,
			$this->CAS->tblpref.'status'=>'Ready',
			$this->User->tblid => $login_user_id,
		); 
		$insert_data = $this->Data->insert_data( $this->CAS, $data );   
		/*---------------------------------------------*/
		$update_data = array( 
			$this->model->tblpref.'request_remark' => $remarks 
		); 
		$updating_data = $this->Data->update_data( $this->model, $update_data, array( $this->model->tblid => $id ) );
		/*---------------------------------------------*/
		$params = array(
			$this->model->tblpref.'status'=>'Ready'   
		); 
		// $id = $this->input->get('id');
		$check = $this->model->set_cash_advance_status( $id, $params );
		
		if( $check ){    
			// $this->model->notify_requester($id);
			$notify_params = array('action'=>'update','type'=>'success','text'=>'Setting Cash Advance request to Ready for Approval status, successful.');
			$this->notify->set( $notify_params );  
			$this->model->send_email_confirmation_request( $id, $parent_id, 'request', $recipients, $remarks );
			
		} else { 
			$notify_params = array('action'=>'update','type'=>'error','text'=>'Setting Cash Advance request to Ready for Approval status, unsuccessful.');
			$this->notify->set( $notify_params ); 
		}   
		redirect_current_page();
	}
	
	public function decline(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$user_login_details = $this->User->get_logged_user();
		$login_user_id = get_value( $user_login_details, $this->User->tblid ); 
		// $id = $this->input->get('id');
		$id = $this->input->post('id'); 
		
		$remarks = $this->input->post('remarks');
		$log_params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('ModuleLogs',$log_params);
		$parent_id = $this->modulelogs->create();

		$log_items_params = array('model'=>$this->CAI,'id'=>$id,'parent_id'=>$parent_id,'type'=>__FUNCTION__); 
		$this->CAIL->create_logs_items($log_items_params);    

		$data = array( 
			$this->User->tblid => $login_user_id,
			$this->model->tblid => $id,
			$this->CAS->tblpref.'status' => 'Declined'  
		);
			  
		/*---------------------------------------------*/
		$update_data = array(
			$this->model->tblpref.'user_approved' => null,
			$this->model->tblpref.'status' => 'Declined',
			$this->model->tblpref.'decline_remark' => $remarks 
		);
		
		$insert_data = $this->Data->insert_data( $this->CAS, $data );  
		$updating_data = $this->Data->update_data( $this->model, $update_data, array( $this->model->tblid => $id ) );
		/*---------------------------------------------*/
		$params = array(
			$this->model->tblpref.'status'=>'Declined'
		);
		
		// $id = $this->input->get('id');
		$decline = $this->model->set_cash_advance_status( $id, $params ); 
		
		if( $decline ){    
			// $this->model->notify_requester($id);
			$notify_params = array('action'=>'update','type'=>'success','text'=>'Declining cash advance request successful.');
			$this->notify->set( $notify_params );  
			$this->model->send_email_confirmation( $id, $parent_id, 'decline', $remarks );
		} else { 
			$notify_params = array('action'=>'update','type'=>'error','text'=>'Declining cash advance request unsuccessful.');
			$this->notify->set( $notify_params ); 
		} 
		
		redirect_current_page();		
	}

	public function release(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$user_login_details = $this->User->get_logged_user();
		$login_user_id = get_value( $user_login_details, $this->User->tblid ); 
		// $id = $this->input->get('id');
		$id = $this->input->post('id');
		$remarks = $this->input->post('remarks');
		$log_params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('ModuleLogs',$log_params);
		$parent_id = $this->modulelogs->create();

		$log_items_params = array('model'=>$this->CAI,'id'=>$id,'parent_id'=>$parent_id,'type'=>__FUNCTION__); 
		$this->CAIL->create_logs_items($log_items_params);    

		$date_released = current_date();  
		$data = array( 
			$this->User->tblid => $login_user_id,
			$this->model->tblid => $id,
			$this->CAS->tblpref.'status'=>'Released'  
		); 
		 
		$insert_data = $this->Data->insert_data( $this->CAS, $data );  
		/*---------------------------------------------*/
		
		$data_for_date_released = array(   
			$this->model->tblpref.'date_released'=> $date_released, 
			$this->model->tblpref.'release_remark'=> $remarks 
		); 
		$update = $this->Data->update_data( $this->model, $data_for_date_released, array( $this->model->tblid => $id ) );
		 
		/*---------------------------------------------*/
		$date_due = date('Y-m-d', strtotime($date_released. ' + 12 days'));
		
		$params = array(
			$this->model->tblpref.'status'=>'Released'
		);
		
		// $id = $this->input->get('id');
		$ca_details  = $this->Data->get_record( $this->model, array( 'where_params' => array( $this->model ->tblid  => $id )));
		$ca_number = get_value( $ca_details, $this->model->tblpref.'ca_number'); 
		$release = $this->model->set_cash_advance_status( $id , $params );  
		if( $release ){ 
			// $this->model->notify_requester($id);		
			$notify_params = array('action'=>'update','type'=>'success','text'=>'Releasing cash advance request successful.');
			$this->notify->set( $notify_params );  
			$this->model->send_email_confirmation( $id, $parent_id, 'release', $remarks );
			redirect( site_url('c='.$this->model->page.'&m=view&id='.$id) );	
		} else {
			$notify_params = array('action'=>'update','type'=>'error','text'=>'Releasing cash advance request unsuccessful.'); 
			$this->notify->set( $notify_params );  
			redirect_current_page();		
		}
		  
	}

	public function pending(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$user_login_details = $this->User->get_logged_user();
		$login_user_id = get_value( $user_login_details, $this->User->tblid ); 
		// $id = $this->input->get('id');
		$id = $this->input->post('id');
		$remarks = $this->input->post('remarks');
		 
		$log_params = array('model'=>$this->model,'type'=>__FUNCTION__);
		$this->load->library('ModuleLogs',$log_params);
		$parent_id = $this->modulelogs->create();

		$log_items_params = array('model'=>$this->CAI,'id'=>$id,'parent_id'=>$parent_id,'type'=>__FUNCTION__); 
		$this->CAIL->create_logs_items($log_items_params);    
 
		$data = array( 
			$this->User->tblid => $login_user_id,
			$this->CA->tblid => $id,
			$this->CAS->tblpref.'status'=>'Pending'  
		); 
			
		$update_data = array(
			$this->CA->tblpref.'user_approved' => null, 
			$this->model->tblpref.'pending_remark'=> $remarks 
		);
		
		$insert_data = $this->Data->insert_data( $this->CAS, $data );  
		$updating_data = $this->Data->update_data( $this->CA, $update_data, array( $this->CA->tblid => $id ) );
		
		/*---------------------------------------------*/ 
		$params = array(
			$this->CA->tblpref.'status'=>'Pending'
		);

		// $id = $this->input->get('id');
		$pending = $this->CA->set_cash_advance_status( $id , $params );
		
		if( $pending ){  
			// $this->CA->notify_requester($id);	
			$notify_params = array('action'=>'update','type'=>'success','text'=>'Pending cash advance request successful.');
			$this->notify->set( $notify_params );  
			// $this->CA->send_email_confirmation( $id, $parent_id );
			
		} else { 
			$notify_params = array('action'=>'update','type'=>'error','text'=>'Pending cash advance request unsuccessful.');
			$this->notify->set( $notify_params );  
		}
		
		redirect_current_page();		
	}	

	public function json_check_balance(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/

		$id = $this->input->post('id');
		$wb_id = $this->input->post('wb_id');
		$ca_limit = $this->input->post('ca_limit');
		$ce_number = $this->input->post('ce_number'); 
		$ca_amount = $this->input->post('ca_amount'); 
		$user_login_details = $this->User->get_logged_user();
		$login_user_id = get_value( $user_login_details, $this->User->tblid ); 
		$return  = $this->model->json_check_balance($id, $wb_id, $ca_limit, $ce_number, $ca_amount, $login_user_id ); 
		echo json_encode( $return );  
		exit;
	}
	
	public function json_check_balance_total(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$join_params = $this->WB->set_join_params();
		$wb_id = $this->input->post('cash_advance[wb_id]');
		$ca_id = $this->input->post('id');
		$details = $this->Data->get_record( $this->WB, array(
			'where_params' => array( $this->WB->tblid => $wb_id ),
			'join_params' => $join_params
		));
		$bcs = get_value( $details, $this->WB->tblpref.'bcs');
		$total = $this->WB->get_total_amount($wb_id, $ca_id ) ;
		
		$balance = $this->model->get_balance($bcs, $wb_id); 
		
		/*$return['balance'] = number_format($balance,'2','.',','); */
		$return['balance'] = $balance;
		if($total){
			$return['total'] = number_format($total,'2','.',','); ;
		}
		else{
			$return['total']  = '0.00';
		} 
		echo json_encode( $return );  
		exit;
	}

	public function record_pending(){ 
		/*standard init in every function---------------------*/ 
		$controller_params = array('log'=>'login','role'=>'records',
			'method'=>array('library'=>array('ModUser'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$data['show_status']['show_status_approve_button'] = $this->usersrole->check( $this->model->urc_name, 'approve');
		$data['show_status']['show_status_decline_button'] = $this->usersrole->check( $this->model->urc_name, 'decline');
		$data['show_status']['show_status_pending_button'] = $this->usersrole->check( $this->model->urc_name, 'pending');
		$data['show_status']['show_status_release_button'] = $this->usersrole->check( $this->model->urc_name, 'release');
		$data['show_status']['show_status_ready_button'] = $this->usersrole->check( $this->model->urc_name, 'ready');

		$where_params = array( 
		  	$this->model->tblpref.'status' => 'Pending',
		  	$this->model->tblpref.'trashed' => 0,
		  	$this->model->tblpref.'published' => 1
		 ); 
		$data = $this->module->records( $this->model, $data, $where_params );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record_pending',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	public function record_release(){ 
		/*standard init in every function---------------------*/ 
		$controller_params = array('log'=>'login','role'=>'records',
			'method'=>array('library'=>array('ModUser'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$data['show_status']['show_status_approve_button'] = $this->usersrole->check( $this->model->urc_name, 'approve');
		$data['show_status']['show_status_decline_button'] = $this->usersrole->check( $this->model->urc_name, 'decline');
		$data['show_status']['show_status_pending_button'] = $this->usersrole->check( $this->model->urc_name, 'pending');
		$data['show_status']['show_status_release_button'] = $this->usersrole->check( $this->model->urc_name, 'release');
		 
		$where_params = array( 
		  	$this->model->tblpref.'status' => 'Approved',
		  	$this->model->tblpref.'trashed' => 0,
		  	$this->model->tblpref.'published' => 1
		 ); 
		$data = $this->module->records( $this->model, $data, $where_params );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record_release',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	public function send_cron_for_manager(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process', 'method'=>array('library'=>array('email')));
		$data = $this->set_controller( $controller_params ); 
		/*---------------------------------------------*/   
		// cron: 00 16 * * * curl http://devactivasia.iwp.ph/index.php?c=ModCashAdvance&m=send_cron_for_manager
		/*---------------------------------------------*/   
		
		$result = $this->Data->get_record($this->UR, array('where_params'=>array( $this->UR->tblpref.'title like '=> '%Manager%' )));
		$ur_id = get_value($result, $this->UR->tblid );
		
		$recepients = $this->Data->get_records($this->User, array('where_params'=>array( $this->UR->tblid => $ur_id )));
		
		$join_params = $this->model->set_join_params(); 
		$query_params_release = array(
			'limit'=>5,
			'join_params' => $join_params,
			'orderby' => $this->model->tblpref.'created',
			'where_params' => array ( 
				$this->model->tblpref.'published' => 1, 
				$this->model->tblpref.'status' => 'Ready', 
				$this->model->tblpref.'trashed' => 0 
		));
	
		$records = $this->Data->get_records( $this->model, $query_params_release ); 
		$result_count = count($records);
		 
		foreach($recepients as $recepient){ 
			$email = get_value($recepient, $this->User->tblpref.'email');  
			 
			if( $result_count ){ 
			
				$subject = 'CARF Approval';   
				$message = '<table border="0" cellpadding="0" cellspacing="0">';
				$message .= '<tr><td><p>You have '.$result_count.' cash advance requests for approval.</b></p></td></tr></table>';			
					 
					  
				$config['protocol']    = 'smtp';
				$config['smtp_host']    = 'ssl://smtp.gmail.com';
				$config['smtp_port']    = '465';
				$config['smtp_timeout'] = '7';
				$config['smtp_user']    = 'marktestingemail@gmail.com';
				$config['smtp_pass']    = 'iWPEC@16';
				$config['charset']    = 'utf-8';
				$config['newline']    = "\r\n";
				$config['mailtype'] = 'html'; // or html
				$config['validation'] = TRUE; // bool whether to validate email or not      

				$this->email->initialize($config);
				$this->email->clear(TRUE);

				$this->email->from('accounts@iwebprovider.com');  
				$this->email->to($email ); 
				$this->email->cc('rubin@iwebprovider.com'); 				
				$this->email->subject($subject);
				$this->email->message($message);
				  
				if (!$this->email->send()){
					show_error($this->email->print_debugger()); 
				} else {
					echo 'Your e-mail has been sent!';
				}     
			}
		}   
	}
	
}
