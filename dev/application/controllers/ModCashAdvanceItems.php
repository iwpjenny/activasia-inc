
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ModCashAdvanceItems extends CI_Controller {
	
	private $model;
	private $page;
	private $view;
	private $location;	
	private $login_user_type;
	private $dashboard_page;
			
	public function index(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('files'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$data = $this->module->records( $this->model, $data, array($this->model->tblpref.'trashed'=>0) );
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/modules/'.$this->view.'/record',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	}
	
	private function set_controller( $params ){		
		
		$this->backend->set();
		$this->frontend->set();
		/*---------------------------------------------*/
		$this->load->model('modules/Cash_Advance_Items_Model', 'CAI');
		/*---------------------------------------------*/
		$this->model = $this->CAI;
		$this->page = $this->CAI->page;
		$this->view = $this->CAI->view;
		$this->location = $this->BLog->location;
		$this->dashboard_page = $this->BLog->dashboard_page;
		/*---------------------------------------------*/
		$this->model->ini_custom_models();
		/*---------------------------------------------*/
		/*Files*/
		/*---------------------------------------------*/
		$this->load->library('Module');
		$this->module->model = $this->model; 
		$this->module->model_user = $this->User;
		$this->module->model_parent = $this->CA;
		$this->module->location = $this->location;	
		/*---------------------------------------------*/
		$this->backend->set_libaries( $this->model );
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		/*---------------------------------------------*/
		$this->load->library('RecordsFilter',array('model'=>$this->model));
		
		return $data;
		
		
	}
	   
	public function json_save(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		); 
		$data = $this->set_controller( $controller_params ); 
		/*set end---------------------------------------------*/ 
		$this->CAI->save();
	}
	 
	public function json_display_record(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/ 
		$this->module->json_display_record( $this->model, $this->WB );
	}
	 
	public function json_delete(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$this->CAI->delete();
		// $this->module->json_delete();
	}
	 
	public function json_trash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->json_trash( $this->model, $this->WB );
	}
	 
	public function json_untrash(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>__FUNCTION__,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		
		$this->module->json_trash( $this->model, $this->WB, 0 );
	}
	 
	public function json_update_popup_content(){
		/*standard init in every function---------------------*/
		$controller_params = array('type'=>'process','log'=>'login','role'=>TRUE,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/ 
		$parent_id = $this->input->post('parent_id');
		$popup_content = $this->WBI->get_items_checkbox( $parent_id ); 
		$json = array();
		$json['popup_content'] = $popup_content;
		$json['page'] = $this->model->page;
		$json['notification'] = '';
		$json['result'] = TRUE;
		$json['redirect_url'] = '';
		
		echo json_encode($json);
		exit();
	}
	
	 
	
	public function json_check_items_total(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>TRUE,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/ 
		$this->CAI->check_items_total();
	}
	
	public function json_calculate_total(){
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login','role'=>TRUE,
			'method'=>array('library'=>array('users'))
		);
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/ 
		$this->CAI->json_mod_calculate_total();
	}
}