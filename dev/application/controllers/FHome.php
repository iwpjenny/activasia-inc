<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FHome extends CI_Controller {

	private $model;
	private $page;
	private $view;
	private $location;
	private $dashboard_page;
	private $logged_user_only = TRUE;

	public function index(){ 
		/*standard init in every function---------------------*/
		$controller_params = array('log'=>'login');
		$data = $this->set_controller( $controller_params );
		/*set end---------------------------------------------*/
		$data['form_action'] = site_url('c='.$this->dashboard_page);
		
		$data = $this->NPA->get_data_section_records( $data );
		$this->Trans->logged_user_only = $this->logged_user_only;
		$data = $this->Trans->get_data_section_records( $data );
		$this->ML->logged_user_only =  $this->logged_user_only;
		$data = $this->ML->get_data_section_records( $data );
		
		$this->load->view('head',$data);
		$this->load->view($this->location.'/header',$data);
		$this->load->view($this->location.'/dashboard',$data);
		$this->load->view($this->location.'/footer',$data);
		$this->load->view('foot',$data);
	 }

	 private function set_controller( $params ){
		$this->backend->set();
		$this->frontend->set();
		$this->customfrontend->set();
		/*---------------------------------------------*/
		$this->location = $this->FLog->location;
		$this->dashboard_page = $this->FLog->dashboard_page;
		/*---------------------------------------------*/
		$this->frontend->set_libaries();
		/*---------------------------------------------*/
		$data = $this->method->set_controller( $params );
		/*---------------------------------------------*/
		/* Initialize Custom Model here */
		$this->load->model('modules/Transactions_Model', 'Trans');
		$this->load->model('modules/Name_Protected_Area_Model', 'NPA');
		$this->load->model('modules/Tribe_Model', 'Tribe');
		/* End of initialize custom model here */
		/*---------------------------------------------*/
		
		return $data;
	}
}
