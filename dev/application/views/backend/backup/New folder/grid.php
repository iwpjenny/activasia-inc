<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$view_date_time_format = app_get_val($settings['date_and_time'],'view_date_time_format');
$view_paragraph_limit = app_get_val($settings['backend_appearance'],'view_paragraph_limit'); 
$grid_paragraph_limit = app_get_val($settings['backend_appearance'],'grid_paragraph_limit');
?>
<div class="table-responsive">
	<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
		<thead>
			<tr>	
				<th>#</th> 
				<th>Name</th>
				<th>Size (KB)</th>
				<th>Modified</th>
				<th>Option</th>
			</tr>
		</thead> 
		<tbody>
			<?php
			if( $records ){
				$n=1;
				foreach($records as $col){
					$id = get_value($col,'id');
					$basename = get_value($col,'basename');
					$extension = get_value($col,'extension');
					$dir_path = get_value($col,'dir_path');
					$filename = get_value($col,'filename');
					$size = get_value($col,'size');
					$size = ($size/1000);
					$size = number_format($size,1,'.',',');
					
					$datetime = get_value($col,'datetime');
					$datetime_display = datetime($datetime,$grid_date_time_format);
					
					$datetime_changed = get_value($col,'datetime_changed');
					?>
					<tr>
						<td><?php echo $n; ?></td>
						<td><?php echo $basename; ?></td>
						<td align="right"><?php echo $size; ?></td>
						<td width="21%" title="<?php echo $datetime; ?>" align="right"><?php echo $datetime_display; ?></td>
						<td width="24%">
							<?php
							if( $this->usersrole->check( $this->BU->urc_name, 'download' ) ){
								?>
								<a class="btn btn-success btn-xs" href="<?php echo site_url('c='.$this->BU->page.'&m=download&basename='.urlencode($basename)); ?>">Download</a>
								<?php
							}
							if( $this->usersrole->check( $this->BU->urc_name, 'view' ) ){
								?>
								<a class="btn btn-default btn-xs fancybox-view" href="#view-<?php echo $id; ?>">View</a>
								<?php
							}
							?>
							<div id="view-<?php echo $id; ?>" style="display:none;">
								<div class="row">
									<div class="col-sm-4">Basename:</div>
									<div class="col-sm-8"><?php echo $basename; ?></div>
								</div>
								<div class="row">
									<div class="col-sm-4">Filename:</div>
									<div class="col-sm-8"><?php echo $filename; ?></div>
								</div>
								<div class="row">
									<div class="col-sm-4">Extension:</div>
									<div class="col-sm-8"><?php echo $extension; ?></div>
								</div>
								<div class="row">
									<div class="col-sm-4">Size:</div>
									<div class="col-sm-8"><?php echo $size; ?>KB</div>
								</div>
								<div class="row">
									<div class="col-sm-4">Created:</div>
									<div class="col-sm-8"><?php echo datetime($datetime,$view_date_time_format); ?></div>
								</div>
								<div class="row">
									<div class="col-sm-4">Changed:</div>
									<div class="col-sm-8"><?php echo datetime($datetime_changed,$view_date_time_format); ?></div>
								</div>
								<div class="row">
									<div class="col-sm-4">Directory:</div>
									<div class="col-sm-8"><?php echo $dir_path; ?></div>
								</div>
							</div>
							<?php
							if( $this->usersrole->check( $this->BU->urc_name, 'delete' ) ){
								?>
								<a class="btn btn-danger btn-xs confirmation" href="#delete-confirmation-<?php echo $id; ?>">Delete</a>
								<div id="delete-confirmation-<?php echo $id; ?>" style="display:none;">
									<p>Do you want to delete this File? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$this->BU->page.'&m=delete&basename='.urlencode($basename)); ?>">Yes Delete</a></p>
								</div>
								<?php
							}
							?>
						</td>
					</tr>
					<?php
					$n++;
				}
			}
			?>
		</tbody> <!-- end of tbody -->
	</table> <!-- end of table -->
	<?php $this->notify->records( $records ); ?>
</div> <!-- end of .table-responsive --> 