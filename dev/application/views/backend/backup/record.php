<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$view_date_time_format = app_get_val($settings['date_and_time'],'view_date_time_format');
$view_paragraph_limit = app_get_val($settings['backend_appearance'],'view_paragraph_limit'); 
$grid_paragraph_limit = app_get_val($settings['backend_appearance'],'grid_paragraph_limit');
?>
<div class="container-fluid">
		<aside class="col-sm-2 sidebar">
			<?php $this->load->view($location.'/settings/sidebar'); ?>
		</aside>
		<div class="col-sm-10">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="title"><?php echo $model->name; ?> Records</h1>
				</div>
				<div class="col-sm-6 text-right">
					<?php
					if( app_get_val( $user_role_model_capabilities, 'backup_database' ) ){
						?>
						<a class="btn btn-default btn-sm" href="<?php echo site_url('c='.$page.'&m=backup_database'); ?>">
						<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>BackUp Database</a>
						<?php
					}
					if( app_get_val( $user_role_model_capabilities, 'backup_application' ) ){
					?>
						<a class="btn btn-default btn-sm" href="<?php echo site_url('c='.$page.'&m=backup_application'); ?>">
						<span class="glyphicon glyphicon-save-file" aria-hidden="true"></span>BackUp Files</a>
						<?php
					}
					if( app_get_val( $user_role_model_capabilities, 'backup_all' ) ){
					?>
						<a class="btn btn-default btn-sm" href="<?php echo site_url('c='.$page.'&m=backup_all'); ?>">
						<span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span>BackUp All</a> 
						<?php
					}
					?>
				</div> <!-- end of .col-sm-6 text-right -->
			</div> <!-- end of row -->
			<div class="row">
				<div class="col-md-12">
					<?php
					if( app_get_val( $user_role_model_capabilities, 'trancate_module_records' ) ){
					?>
						<a class="btn btn-default btn-sm" href="<?php echo site_url('c='.$page.'&m=trancate_module_records'); ?>">
						<span class="glyphicon glyphicon-floppy-remove" aria-hidden="true"></span>Delete All Custom Module Records</a> 
					<?php
					}
					if( app_get_val( $user_role_model_capabilities, 'trancate_logs' ) ){
						?>
						<a class="btn btn-default btn-sm" href="<?php echo site_url('c='.$page.'&m=trancate_logs'); ?>">
						<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>Delete All Logs</a> 
						<?php
					}
					?>
				</div> <!-- end of .col-sm-6 text-right -->
			</div> <!-- end of row -->
			<hr />
			<?php $this->notify->show(); ?>
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>	
								<th>#</th> 
								<th>Name</th>
								<th>Size (KB)</th>
								<th>Modified</th>
								<th>Option</th>
							</tr>
						</thead> 
						<tbody>
							<?php
							if( $records ){
								$n=1;
								foreach($records as $col){
									$id = get_value($col,'id');
									$basename = get_value($col,'basename');
									$extension = get_value($col,'extension');
									$dir_path = get_value($col,'dir_path');
									$filename = get_value($col,'filename');
									$size = get_value($col,'size');
									$size = ($size/1000);
									$size = number_format($size,1,'.',',');
									$datetime = get_value($col,'datetime');
									$datetime_display = datetime($datetime,$grid_date_time_format);
									$datetime_changed = get_value($col,'datetime_changed');
									$datetime_changed_display = datetime($datetime_changed,$view_date_time_format);
									
									?>
									<tr>
										<td><?php echo $n; ?></td>
										<td><?php echo $basename; ?></td>
										<td align="right"><?php echo $size; ?></td>
										<td title="<?php echo $datetime; ?>" align="right"><?php echo $datetime_display; ?></td>
										<td>
											<?php
											if( $this->usersrole->check( $model->urc_name, 'download' ) ){
												?>
												<a class="btn btn-success btn-xs" href="<?php echo site_url('c='.$page.'&m=download&basename='.urlencode($basename)); ?>">Download</a>
												<?php
											}
											if( $this->usersrole->check( $model->urc_name, 'view' ) ){
												?>
												<a class="btn btn-default btn-xs fancybox-view" href="#view-<?php echo $id; ?>">View</a>
												<?php
											}
											?>
											<div id="view-<?php echo $id; ?>" style="display:none;">
												<div class="row">
													<div class="col-sm-4">Basename:</div>
													<div class="col-sm-8"><?php echo $basename; ?></div>
												</div>
												<div class="row">
													<div class="col-sm-4">Filename:</div>
													<div class="col-sm-8"><?php echo $filename; ?></div>
												</div>
												<div class="row">
													<div class="col-sm-4">Extension:</div>
													<div class="col-sm-8"><?php echo $extension; ?></div>
												</div>
												<div class="row">
													<div class="col-sm-4">Size:</div>
													<div class="col-sm-8"><?php echo $size; ?>KB</div>
												</div>
												<div class="row">
													<div class="col-sm-4">Created:</div>
													<div class="col-sm-8"><?php echo $datetime; ?></div>
												</div>
												<div class="row">
													<div class="col-sm-4">Changed:</div>
													<div class="col-sm-8"><?php echo $datetime_changed_display; ?></div>
												</div>
												<div class="row">
													<div class="col-sm-4">Directory:</div>
													<div class="col-sm-8"><?php echo $dir_path; ?></div>
												</div>
											</div>
											<?php
											if( $this->usersrole->check( $model->urc_name, 'delete' ) ){
												?>
												<a class="btn btn-danger btn-xs confirmation" href="#delete-confirmation-<?php echo $id; ?>">Delete</a>
												<div id="delete-confirmation-<?php echo $id; ?>" style="display:none;">
													<p>Do you want to delete this File? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&basename='.urlencode($basename)); ?>">Yes Delete</a></p>
												</div>
												<?php
											}
											if( $this->usersrole->check( $model->urc_name, 'send_email' ) ){
												if (strpos($basename, 'db') !== false) {
													?>
													<a class="btn btn-info btn-xs" href="<?php echo site_url('c='.$page.'&m=send_email&basename='.urlencode($basename)); ?>" >Send to Email</a>
													<?php
												}
											}
											?>
										</td>
									</tr>
									<?php
									$n++;
								}
							}
							?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $records ); ?>
			</form>
		</div>
</div>  <!-- end of .container-fluid -->