<?php 
if( $this->usersrole->check( $this->BU->urc_name, 'view' ) ){
?>
<div class="list-group" style="margin-top:10px;"> 
	<li class="list-group-item"><h4 class="list-group-item-heading">Backup Menu</h4></li>
	<a href="<?php echo site_url('c='.$this->BU->page); ?>" class="list-group-item default">
		<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
		<strong class="list-group-item-heading">Backup</strong>
		<p class="list-group-item-text"></p>
	</a>
	<?php
	if( $this->usersrole->check( $this->BU->urc_name, 'add' ) ){
	?>	
	<a href="<?php echo site_url('c='.$this->BU->page.'&m=database'); ?>" class="list-group-item default">
		<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
		<strong class="list-group-item-heading">Backup Database</strong>
		<p class="list-group-item-text"></p>
	</a>
	<a href="<?php echo site_url('c='.$this->BU->page.'&m=application'); ?>" class="list-group-item default">
		<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
		<strong class="list-group-item-heading">Backup Files</strong>
		<p class="list-group-item-text"></p>
	</a>
	<a href="<?php echo site_url('c='.$this->BU->page.'&m=all'); ?>" class="list-group-item default">
		<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
		<strong class="list-group-item-heading">Backup All</strong>
		<p class="list-group-item-text"></p>
	</a>
	<?php 
	}
	?>
</div>
<?php 
} 
?>