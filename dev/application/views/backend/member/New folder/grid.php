<div class="table-responsive">
	<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Full Name</th>
				<th>Username</th>
				<th>Role</th>
				<th>Created</th>
				<th>Published</th>
				<th>Option</th> 
			</tr> 
		</thead> <!-- end of thead -->
		<tbody>
		 <?php
			if( $records ){
				$n=1;
				foreach($records as $col){ 
					$id = get_value($col,$this->Mem->tblid);
					$firstname = get_value($col,$this->Mem->tblpref.'firstname');
					$midname = get_value($col,$this->Mem->tblpref.'midname');
					$lastname = get_value($col,$this->Mem->tblpref.'lastname');
					$username = get_value($col,$this->Mem->tblpref.'username'); 
					$published = get_value($col,$this->Mem->tblpref.'published');
					$created = get_value($col,$this->Mem->tblpref.'created');
					
					$role = get_value($col,$this->MR->tblpref.'title');
					$role_id = get_value($col,$this->MR->tblid ); 
												
					$created_display = datetime($created,$grid_date_time_format);			
					?>
					<tr>
						<td><?php echo $n; ?></td>
						<td>
						<?php app_fullname_format( $firstname, $midname, $lastname, '{lastname}, {firstname} {middlename} ' ); ?>
						</td>
						<td><?php echo $username; ?></td>
						<td>
							<?php 
							if( $this->usersrole->check( $this->MR->urc_name, 'records' ) || $this->usersrole->check( $this->MR->urc_name, 'view' ) ){
								?>
								<a href="<?php echo site_url('c='.$this->MR->page.'&m=edit&id='.$role_id); ?>" target="_blank"><?php echo $role; ?></a>	
								<?php
							} else {
								 echo $role; 
							}
							?>
						</td>
						<td align="right" title="<?php echo $created; ?>">
							<small><?php echo $created_display; ?><small>
						</td>
						<td><?php published( $published ); ?></td> 
						<td class="text-right">
							<?php	 
							btn_optn_records($this->Mem->page, array(
								'capabilities'=>$capabilities,
								'ur_name'=>$this->Mem->urc_name,
								'id'=>$id,
								'display'=>array('view','edit','delete')
							));
							?>
						</td> 
					</tr>
					<?php
					$n++;
				}
			} 
			?>
		</tbody> 
	</table> 
<?php $this->notify->records( $records ); ?>
</div>  