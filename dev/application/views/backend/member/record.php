<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
?>
<div class="container-fluid">
	<div class="row">
		<aside class="col-md-2 sidebar">
			<?php $this->load->view($location.'/user/sidebar'); ?>
		</aside>
		<div class="col-md-10 main"> 
			<div class="row" id="header-title-buttons">
				<div class="col-sm-6"> 
					<h1 class="title"><?php echo $this->Mem->name; ?> Records</h1>
				</div>
				<div class="col-sm-6 text-right"> 
					<?php btn_records( $page, array('capabilities'=>$capabilities,'ur_name'=>$this->Mem->urc_name,'display'=>array('add','trash')) ); ?>
				</div> 
			</div> <!-- end of .row -->
			<?php $this->notify->show(); ?>
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th>
								<?php 
								if( $this->usersrole->check( $this->User->urc_name, 'picture' ) ){
									?>
									<th>Picture</th>
									<?php 
								}
								?>
								<th>Full Name</th>
								<th>Username</th>
								<th>Role</th>
								<th>Created</th>
								<th>Published</th>
								<th>Option</th> 
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
							<?php
							if( $record_rows ){
								$n=1;
								foreach($record_rows as $col){ 
									$id = get_value($col,$this->Mem->tblid);
									$firstname = get_value($col,$this->Mem->tblpref.'firstname');
									$midname = get_value($col,$this->Mem->tblpref.'midname');
									$lastname = get_value($col,$this->Mem->tblpref.'lastname');
									$username = get_value($col,$this->Mem->tblpref.'username'); 
									$email = get_value($col,$this->User->tblpref.'email'); 
									$published = get_value($col,$this->Mem->tblpref.'published');
									$created = get_value($col,$this->Mem->tblpref.'created');
									$role = get_value($col,$this->MR->tblpref.'title');
									$role_id = get_value($col,$this->MR->tblid );
									$picture_src = get_value($col,$this->MR->tblpref.'picture');
									$picture = $this->pic->get_img_by_src( $picture_src );
									 							
									$created_display = datetime($created,$grid_date_time_format);
									?>
									<tr>
										<td><?php echo $n; ?></td>
										<?php 
										if( $this->usersrole->check( $this->Mem->urc_name, 'picture' ) ){
											?>
											<td><?php echo $picture; ?></td>
											<?php 
										}
										?>
										<td><?php app_fullname_format( $firstname, $midname, $lastname ); ?></td>
										<td><?php echo $username; ?></td>
										<td>
											<?php 
											if( $this->usersrole->check( $this->MR->urc_name, 'records' ) || $this->usersrole->check( $this->MR->urc_name, 'view' ) ){
												?>
												<a href="<?php echo site_url('c='.$this->MR->page.'&m=edit&id='.$role_id); ?>" target="_blank"><?php echo $role; ?></a>	
												<?php
											} else {
												 echo $role; 
											}
											?>
										</td>
										<td align="right" title="<?php echo $created; ?>">
											<small><?php echo $created_display; ?><small>
										</td>
										<td><?php published( $published ); ?></td> 
										<td class="text-right">
											<?php	 
											btn_optn_records($page, array(
												'capabilities'=>$capabilities,
												'ur_name'=>$this->Mem->urc_name,
												'id'=>$id,
												'display'=>array('view','edit','delete')
											));
											?>
										</td> 
									</tr>
									<?php
									$n++;
								}
							} 
							?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
				<nav>
					<?php echo $pagination; ?>
					<div class="showing-result"><?php app_pagination_details($per_page, $n); ?></div>
				</nav>
			</form> <!-- end of form --> 
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div>  <!-- end of .container-fluid -->