<?php    
$display_latest_members = app_get_val($settings['admin_dashboard'],'display_latest_members'); 
if( $this->usersrole->get_capability($capabilities, $model->urc_name, 'records' ) && $display_latest_members == 'yes' ){
	$urc_name = $model->urc_name;
	$page = $model->page;
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Member Menu</h3>
	</div>
	<!-- <div class="panel-body">
		System Front-end Users.
	</div> -->
	<div class="list-group">	
		<a href="<?php echo site_url('c='.$model->page); ?>" class="list-group-item">
			<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Members</strong>
		</a>
		<?php
		if( $this->usersrole->get_capability($capabilities, $urc_name, 'add' ) ){
			?>
			<a href="<?php echo site_url('c='.$model->page.'&m=edit'); ?>" class="list-group-item">
				<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				<strong class="list-group-item-heading">Add New</strong>
			</a>
			<?php
		}
		if( $this->usersrole->get_capability($capabilities, $urc_name, 'records' ) ){
			?>
			<a href="<?php echo site_url('c='.$this->ML->page.''); ?>" class="list-group-item">
				<span class="glyphicon glyphicon-list" aria-hidden="true"></span>
				<strong class="list-group-item-heading">Logs</strong>
			</a>
			<?php
		}
		?>
	</div>
</div>
<?php  
}
?>