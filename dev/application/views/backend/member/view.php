<?php
$email = app_get_val($fields,$this->Mem->tblpref.'email');
?>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2"> 
			<?php 
			if( $this->usersrole->check( $this->Mem->urc_name, 'picture' ) ){
				?>
				<div class="list-group">
					<li class="list-group-item"><?php echo $picture; ?></li>
					<li class="list-group-item">
						<strong>
							<?php app_val($fields,$this->Mem->tblpref.'firstname'); ?> 
							<?php app_val($fields,$this->Mem->tblpref.'lastname'); ?>
						</strong>
					</li>
				</div>
				<?php
			}
			?>
			<?php $this->load->view($location.'/user/sidebar'); ?>
		</aside>
		<div class="col-sm-10">
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="row">
					<div class="col-sm-6"> 
						<h1 class="title"><?php echo $this->Mem->name; ?> Profile</h1>
					</div>
					<div class="col-sm-6 text-right">
						<?php btn_view( $page, array('capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$this->Mem->urc_name) ); ?> 
					</div>
				</div>
				<?php $this->notify->show(); ?>
				<div class="row">
					<div class="col-md-12"> 
						<div class="form-group">
							<div class="col-xs-6 col-sm-2">Full Name:</div>
							<div class="col-xs-6 col-sm-2">
								<b>
								<?php app_val($fields,$this->Mem->tblpref.'firstname'); ?>
								<?php app_val($fields,$this->Mem->tblpref.'midname'); ?>
								<?php app_val($fields,$this->Mem->tblpref.'lastname'); ?>
								</b>
							</div> 
							<div class="col-xs-6 col-sm-2">User Role:</div>
							<div class="col-xs-6 col-sm-2">
								<b>
								<?php 
								if( $this->usersrole->check( $this->Mem->urc_name, 'view_user_role' ) ){
									?>
									<a href="<?php echo site_url('c='.$this->MR->page.'&m=edit&id='.$ur_id); ?>" target="_blank">
									<?php app_val($fields,$this->MR->tblpref.'title'); ?></a>	
									<?php
								} else {
									app_val($fields,$this->MR->tblpref.'title');
								}
								?>
								</b>	
							</div> 
						</div>
						<div class="form-group"> 
							<div class="col-xs-6 col-sm-2">Username:</div>
							<div class="col-xs-6 col-sm-2">
								<b><?php app_val($fields,$this->Mem->tblpref.'username'); ?></b>
							</div> 
							<div class="col-xs-6 col-sm-2">Email Address:</div>
							<div class="col-xs-6 col-sm-2">
								<b><a href="mailto:<?php echo $email; ?>" target="_blank">
								<?php echo $email; ?></a></b>
							</div> 
						</div>
						<div class="form-group">
							<div class="col-xs-6 col-sm-2">Address:</div>
							<div class="col-xs-6 col-sm-2">
								<b><?php app_val($fields,$this->MP->tblpref.'address'); ?></b>
							</div>  
							<div class="col-xs-6 col-sm-2">Contact No.:</div>
							<div class="col-xs-6 col-sm-2">
								<b><?php app_val($fields,$this->MP->tblpref.'contact_no'); ?></b>
							</div> 
						</div>
						<hr />
						<?php $this->load->view($location.'/view-footer-fields'); ?>
					</div>
				</div>
				<?php form_hidden('id',$id); ?>
			</form> <!-- end of .form -->
		</div>
	</div>
</div>