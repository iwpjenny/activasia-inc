<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2">
			<?php
			if( $this->usersrole->check( $this->Mem->urc_name, 'picture' ) ){
				?>
				<div class="list-group">
					<li class="list-group-item"><?php echo $picture; ?></li>
					<li class="list-group-item">
						<strong>
							<?php app_value($fields,$this->Mem->tblpref.'firstname'); ?>
							<?php app_value($fields,$this->Mem->tblpref.'lastname'); ?>
						</strong>
					</li>
				</div>
				<?php
			}
			?>
			<?php $this->load->view($location.'/user/sidebar'); ?>
		</aside>
		<div class="col-sm-10">
			<?php echo form_open_multipart($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $this->Mem->name; ?> Edit</h1>
					</div>
					<div class="col-sm-6 text-right"> 
						<?php btn_edit( $page, array('capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$this->Mem->urc_name) ); ?> 
					</div>
				</div>
				<?php $this->notify->show(); ?>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (*) are required fields and must not be blank.</h6>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">User Role *:</label>
					<div class="col-sm-4">
						<?php echo $select_role; ?>
					</div>
					<label class="col-sm-2">Email *:</label>
					<div class="col-sm-4">
						<?php app_field('email',$this->Mem,$fields,array('type'=>'email','holder'=>'Email Address [sample@email.com]','attr'=>'pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"')); ?> 
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Username *:</label>
					<div class="col-sm-4">
						<?php app_field('username',$this->Mem,$fields,array('holder'=>'Username')); ?> 
					</div>
					<label class="col-sm-2">First Name *: </label>
					<div class="col-sm-4">
						<?php app_field('firstname',$this->Mem,$fields,array('class'=>'alphaonly','holder'=>'First Name')); ?> 
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Middle Name:</label>
					<div class="col-sm-4">
						<?php app_field('midname',$this->Mem,$fields,array('class'=>'alphaonly','holder'=>'Middle Name')); ?> 
					</div>
					<label class="col-sm-2">Last Name *:</label>
					<div class="col-sm-4">
						<?php app_field('lastname',$this->Mem,$fields,array('class'=>'alphaonly','holder'=>'Last Name')); ?> 
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Address:</label>
					<div class="col-sm-4">
						<?php app_field('address',$this->MP,$fields,array('holder'=>'Full Address','req'=>FALSE)); ?> 
					</div>
					<label class="col-sm-2">Contact No:</label>
					<div class="col-sm-4">
						<?php app_field('contact_no',$this->MP,$fields,array('class'=>'numericOnly','holder'=>'Contact Number','req'=>FALSE)); ?> 
					</div>
				</div>
				<?php if( $this->usersrole->check( $this->Mem->urc_name, 'picture' ) ){ ?>
				<div class="form-group">
					<label class="col-sm-2">Picture:</label>
					<div class="col-sm-4">
						<?php $this->pic->field(); ?>
					</div>
				</div>
				<?php } ?>
				<hr />
				<div class="form-group">
					<h4 class="col-sm-12">Password Update</h4>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Password:</label>
					<div class="col-sm-4">
						<?php app_field('password',$this->Mem->tblpref,$fields,array('type'=>'password','id'=>'password','holder'=>'Password','has_value'=>FALSE,'req'=>FALSE,'min'=>6,'max'=>15)); ?> 
					</div>
					<label class="col-sm-2">Confirm:</label>
					<div class="col-sm-4">
						<?php app_field('confirm_password',$this->Mem->tblpref,$fields,array('type'=>'password','id'=>'confirm_password','holder'=>'Confirm Password','has_value'=>FALSE,'req'=>FALSE,'min'=>6,'max'=>15)); ?> 
					</div>
				</div>
				<hr />
				<?php $this->load->view($location.'/edit-form-footer-fields'); ?>
				<?php echo form_hidden('id',$id); ?>
			</form> <!-- end of .form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->
<div id="delete-confirmation" style="display:none;">
	<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&id='.$id); ?>">Yes Delete</a></p>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $(".alphaonly").keypress(function(event){
        var inputValue = event.which;
        if((inputValue > 47 && inputValue < 58) && (inputValue != 32)){
            event.preventDefault();
        }
    });
});

var holder_id = "<?php echo $id; ?>";

if( holder_id != ' ' || holder_id != false ){
	$("input#password").removeAttr( "required" );
	$("input#confirm_password").removeAttr( "required" );
}

function readURL(input) {
	if( input.files && input.files[0] ){
		var reader = new FileReader();
		reader.onload = function(e){
			$('#display_image').attr( 'src', e.target.result );
		}
		reader.readAsDataURL(input.files[0]);
	}
}

$( '#user_form' ).submit(function( event ) {
	var password = $("input#password").val();
	var confirm_password = $("input#confirm_password").val();
	var message = '<div class="alert alert-warning" role="alert">Your password length must reach up to 5 characters.</div>';

	if( password ){
		if( password.length <= 5  ){
			$('div.msg_box').html(message);
			return false;
		} else if( confirm_password  <= 5 ){
			$('div.msg_box').html(message);
			return false;
		}
	}
});
</script>
