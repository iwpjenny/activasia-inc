<?php
if( $ur_name == 'developer' || (get_urc($urc_array_list,'settings','view_records') && $settings_records) ){
?>
<div class="list-group">
	<li class="list-group-item"><h4 class="list-group-item-heading">Settings Menu</h4></li>
	<?php
	foreach($settings_records as $col){
		$record_id = get_value($col,$settings_prfx.'id');
		$title = get_value($col,$settings_prfx.'title');
		$description = get_value($col,$settings_prfx.'description');
		?>
		<a href="<?php echo site_url('c='.$settings_page.'&m=edit&id='.$record_id); ?>" class="list-group-item <?php active($record_id,$id); ?>">
		<strong class="list-group-item-heading"><?php echo $title; ?></strong>
		<!--	<p class="list-group-item-text"><?php echo $description; ?></p> -->
		</a>
		<?php
	}
	?>
</div>
<?php
}
?>