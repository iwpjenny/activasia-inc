<?php  
if( $this->usersrole->get_capability($capabilities, $model->urc_name, 'records' ) ){
	$urc_name = $model->urc_name;
	$page = $model->page;
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Manager Menu</h3>
	</div>
	<!-- <div class="panel-body">
		System entire Settings and Configurations.
	</div> -->
	<div class="list-group">	
		<?php
		if( $this->usersrole->get_capability($capabilities, $this->Conf->urc_name, 'view' ) ){
			?> 
			<a href="<?php echo site_url('c='.$this->Conf->page); ?>" class="list-group-item">
			<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
				<strong class="list-group-item-heading"> Configurations</strong>
			</a>
			<?php  
		}
		if( $this->usersrole->get_capability($capabilities, $urc_name, 'view' ) ){
			?> 
			<a href="<?php echo site_url('c='.$page); ?>" class="list-group-item">
			<span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
				<strong class="list-group-item-heading"> Settings</strong>
			</a>
			<?php  
		}
		?>
	</div>
</div>
<?php  
}
?>