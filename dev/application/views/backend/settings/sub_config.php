<?php
if( $set_id ){
	//FOR SINGLE RECORD
	echo form_open($form_action_format_settings, array('class'=>'form-horizontal','id' =>$page.'_format_form','name' =>$page.'_format_form'));  
	?> 
		<?php
			if($records_child_config){
		?>
			<div class="form-group" id="header-title-buttons-format">
				<div class="col-sm-6">
					<h3 class="title">Sub Custom Configuration</h3>
				</div> 
				<div class="col-sm-6 text-right"> 
					<?php 
					btn_edit( $page, 'format_form', array('add_data'=>'','update_data'=>1,'delete_data'=>1) ); 
					?>
				</div> 
			</div>   
		<?php  
			foreach($records_child_config as $col){ 
				$sub_set_id = get_value($col,$prefix_config.'id');
				$set_title = get_value($col,$prefix_config.'title');
				$set_name = get_value($col,$prefix_config.'name');
				$set_created = get_value($col,$prefix_config.'created');
				$set_published = get_value($col,$prefix_config.'published'); 
				$config_format = $this->SFM->get_config_format($sub_set_id, $multiple_records = 0); 	 
			?>  
				<div class="form-group">
					 
					<div class="col-sm-4">
						<h4><?php echo $set_title; ?></h4>
					</div>  
				</div> <!-- end of .form-group -->  
				<?php 
				foreach($config_format as $format){
					$format_data = $this->Set->show_config_format( $format, $prefix_format, $set_name); 
				?>
					<div class="form-group">
						<label class="col-sm-2"><?php echo $format_data['setf_title']; ?></label>
						<div class="col-sm-10">
							<?php echo $format_data['format_type']; ?>
						</div>  
					</div> 
				<?php	
				} 	
				?>	
				<hr />	
			<?php  
			}
		}
		?>
			<input type="hidden" name="set_id" value="<?php echo $set_id; ?>" /> 	 
	</form> <!-- end of .form -->    
	<!--END OF SINGLE RECORD-->
	<!--FOR MULTIPLE RECORD-->
	<?php
	if($records_child_config){
	?>
	<div class="form-group" id="header-title-buttons-format"> 
		<h3 class="title">Sub Custom Configuration for Multiple Records</h3> 	 
	</div>   
	<?php  
		foreach($records_child_config as $col){ 
			$sub_set_id = get_value($col,$prefix_config.'id');
			$set_title = get_value($col,$prefix_config.'title');
			$set_name = get_value($col,$prefix_config.'name');
			$set_created = get_value($col,$prefix_config.'created');
			$set_published = get_value($col,$prefix_config.'published');  
			$config_format = $this->SFM->get_config_format($sub_set_id, $multiple_records = 1); 
			if($config_format){
				foreach($config_format as $format){  
					$format_data['format'] = $format;
			?>
				<h4><?php echo $set_title; ?></h4>
			<?php 
					$this->load->view('settings/multiple_records', $format_data);
				} 
			}	 
		}
	} 
}
?>  
