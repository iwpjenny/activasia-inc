<?php  
$format_data = $this->Set->show_config_format( $format, $prefix_format, $set_name, $setr_id );   
?>
<?php		
echo form_open(site_url('c='.$this->Set->page.'&m=save_multiple_settings'), array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); 
?>      
	<div class="form-group" id="header-title-buttons-format">
		<div class="col-sm-6">
			Settings <?php echo $set_name; ?>
		</div> 
		<div class="col-sm-6 text-right">
		<?php  
		$existing = $this->Data->get_record( $this->SRM, array( 'where_params' => array($this->SRM->tblid => $setr_id, $this->SFM->tblid => $format->setf_id)) );
		$update_data_form = 0; 
		if($existing && $format->setf_type != 'select'){ 
			$update_data_form = 1; 
		} else {
			?> 
			<button class="btn btn-success btn-sm required-form" type="submit">
				<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				Add Data
			</button>  
			<?php
		}  
		btn_edit_settings( $page, $id, array('ur_name'=>$this->Set->urc_name),'Reset' );
		?> 
		</div>
	</div> 
	<div class="form-group">
		<label class="col-sm-2"><?php echo $format_data['setf_title']; ?></label>
		<div class="col-sm-10">
			<?php echo $format_data['format_type']; ?>
		</div>  
	</div> 
	<input type="hidden" name="setf_id" value="<?php echo $format->setf_id; ?>" /> 	 
	<input type="hidden" name="set_id" value="<?php echo $set_id; ?>" /> 	  
	<?php if( $format->setf_type != 'select' ){ ?>			
		<input type="hidden" name="setr_id" value="<?php echo $setr_id; ?>" /> 	  
	<?php } ?>	
</form>
<?php  
$format_records = $this->Data->get_records($this->SRM, array( 'where_params' => array($this->SFM->tblid => $format->setf_id), 'title','ASC' ) );

$data_table = array(
	'format_records' => $format_records,
	'set_id' => $set_id,
	'setf_type' => $format->setf_type
);
$this->load->view($location.'/settings/multiple_records_table',$data_table); 
