<div id="delete-confirmation" style="display:none;">
	<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&id='.$id); ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Yes Delete</a></p>
</div>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2 sidebar">	
			<?php $this->load->view($location.'/leftmenu-settings'); ?>
			<?php $this->load->view($location.'/leftmain-sidebar'); ?>
		</aside>
		<div class="col-sm-10 main"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title">Module Terms</h1>
					</div>
					<div class="col-sm-6 text-right">
						<?php btn_edit( $page, $id, array('add_data'=>$add_data,'update_data'=>$update_data,'delete_data'=>$delete_data) ); ?>
					</div> 
				</div> <!-- end of .form-group -->
				<div class="form-group msg_box">
					<?php echo show_alerts(); ?>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (*) are required fields and must not be blank. This settings is for Developers only.</h6>
					</div>
				</div> <!-- end of .form-group -->
				<div class="form-group">
					<label class="col-sm-2">Title <span class="red">*</span>:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="title" value="<?php echo $title; ?>"  placeholder="Title" required="required"/>
					</div> 
					<label class="col-sm-2">Description:</label>
					<div class="col-sm-4">
						<textarea class="form-control" name="description" row="2" placeholder="Description" /><?php echo $description; ?></textarea>
					</div> 
				</div> <!-- end of .form-group -->
				<hr />
				<div class="form-group">
					<label class="col-sm-2">Members</label>
				</div> 	 
				<div class="form-group">  
					<label class="col-sm-2">Title <span class="red">*</span>: </label>
					<div class="col-sm-4"> 
						<input type="text" class="form-control" name="custom[members_title]" value="<?php echo element('members_title', $custom); ?>"  placeholder="Title" />
					</div> 
					<label class="col-sm-2">Description <span class="red">*</span>: </label>
					<div class="col-sm-4"> 
						<textarea class="form-control" name="custom[members_description]" row="2" placeholder="Description" /><?php echo element('members_description', $custom); ?></textarea>
					</div> 
				</div> 
				<div class="form-group">  
					<label class="col-sm-2">Transactions Title <span class="red">*</span>: </label>
					<div class="col-sm-4"> 
						<input type="text" class="form-control" name="custom[transactions_title]" value="<?php echo element('transactions_title', $custom); ?>"  placeholder="Title" />
					</div> 
					<label class="col-sm-2">Transactions Description <span class="red">*</span>: </label>
					<div class="col-sm-4"> 
						<textarea class="form-control" name="custom[transactions_description]" row="2" placeholder="Description" /><?php echo element('transactions_description', $custom); ?></textarea>
					</div> 
				</div> 
				<hr />
				<div class="form-group">
					<label class="col-sm-2">Users</label>
				</div> 	 
				<div class="form-group">  
					<label class="col-sm-2">Title <span class="red">*</span>: </label>
					<div class="col-sm-4"> 
						<input type="text" class="form-control" name="custom[users_title]" value="<?php echo element('users_title', $custom); ?>"  placeholder="Title" />
					</div> 
					<label class="col-sm-2">Description <span class="red">*</span>: </label>
					<div class="col-sm-4"> 
						<textarea class="form-control" name="custom[users_description]" row="2" placeholder="Description" /><?php echo element('users_description', $custom); ?></textarea>
					</div> 
				</div> 
				<hr />
				<div class="form-group">
					<label class="col-sm-2">Manager</label>
				</div> 	 
				<div class="form-group">  
					<label class="col-sm-2">Settings Title <span class="red">*</span>: </label>
					<div class="col-sm-4"> 
						<input type="text" class="form-control" name="custom[users_title]" value="<?php echo element('users_title', $custom); ?>"  placeholder="Title" />
					</div> 
					<label class="col-sm-2">Settings Description <span class="red">*</span>: </label>
					<div class="col-sm-4"> 
						<textarea class="form-control" name="custom[users_description]" row="2" placeholder="Description" /><?php echo element('users_description', $custom); ?></textarea>
					</div> 
				</div> 
				<div class="form-group">  
					<label class="col-sm-2">Backup Title <span class="red">*</span>: </label>
					<div class="col-sm-4"> 
						<input type="text" class="form-control" name="custom[transactions_title]" value="<?php echo element('transactions_title', $custom); ?>"  placeholder="Title" />
					</div> 
					<label class="col-sm-2">Backup Description <span class="red">*</span>: </label>
					<div class="col-sm-4"> 
						<textarea class="form-control" name="custom[transactions_description]" row="2" placeholder="Description" /><?php echo element('transactions_description', $custom); ?></textarea>
					</div> 
				</div> 
				<hr />
				<?php form_stadard_footer_edit_fields( $id, $published, $created, $modified,'1'); ?>
				<input type="hidden" name="<?php echo $tblid; ?>" value="<?php echo $id; ?>" />
				<input type="hidden" class="form-control" name="name" value="<?php echo $name; ?>" />
			</form> <!-- end of .form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->