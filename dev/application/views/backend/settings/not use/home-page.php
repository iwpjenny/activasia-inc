<div id="delete-confirmation" style="display:none;">
	<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&id='.$id); ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Yes Delete</a></p>
</div>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2 sidebar">
			<?php $this->load->view($location.'/leftmenu-settings'); ?>			
			<?php $this->load->view($location.'/leftmain-sidebar'); ?>
		</aside>
		<div class="col-sm-10 main"> 
			<?php echo form_open_multipart($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?> 
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title">Home Page Settings</h1>
					</div>
					<div class="col-sm-6 text-right">
						<?php btn_edit( $page, $id, array('add_data'=>$add_data,'update_data'=>$update_data,'delete_data'=>$delete_data) ); ?>
					</div>
				</div>  
				<div class="form-group msg_box">
						<?php echo show_alerts(); ?>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (*) are required fields and must not be blank.</h6>
					</div>
				</div> <!-- end of .form-group -->
				<div class="form-group">
					<label class="col-sm-2">Title *:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="title" value="<?php echo $title; ?>"  placeholder="Title" required="required"/>
					</div> 
					<label class="col-sm-2">Description:</label>
					<div class="col-sm-4">
						<textarea class="form-control" name="description" row="2" placeholder="Description" /><?php echo $description; ?></textarea>
					</div> 
				</div> <!-- end of .form-group -->
				<?php //form_stadard_footer_edit_fields( $id, $published, $created, $modified, $view_published ); ?>
				
				<div class="form-group">
					<label class="col-sm-2">Custom Settings</label>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Page Title:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="custom[page-title]" value="<?php echo $custom['page-title']; ?>"/>
					</div> 
					<label class="col-sm-2">Page Content:</label>
					<div class="col-sm-4">
						<textarea class="form-control" name="custom[page-content]" row="4"><?php echo $custom['page-content']; ?>
						</textarea>
					</div> 
				</div> <!-- end of .form-group -->  
				<div class="form-group"> 
					<label class="col-sm-2">Image Slider 1:</label>
					<div class="col-sm-2">
						<input class="input-xs custom_image" type="file" name="custom_image_1" property="image_1"  />
						<?php 
						if( isset( $custom['image_1'] ) && !empty( $custom['image_1'] )){ 
							?> 
							<button class="btn btn-danger btn-xs" type="button" onclick="remove_image(this)" property="image_1"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Delete</button> 
							<?php
						}
						?>
						
					</div> 
					<div class="col-sm-2">   
						<?php   
						if( isset( $custom['image_1'] ) && !empty( $custom['image_1'] )){ 
							$image = 'uploads/homepage/'.$custom['image_1'].'?timestamp='.time();  
							?> 
							<input type="hidden" id="image_1" name="custom[image_1]" value="<?php echo $custom['image_1']; ?>" /> 
							<img id="src_image_1" src="<?php echo $image; ?>" alt=""  style="height:100px;width:100px;" /> 
							<?php
						} else {
							?>   
							<img id="src_image_1"  alt=""  style="height:100px;width:100px;"/>
							<?php
						}
						?> 
					</div>
					<label class="col-sm-2">Image Slider 2:</label>
					<div class="col-sm-2">
						<input class="input-xs custom_image" type="file" name="custom_image_2"  property="image_2" />
						<?php 
						if( isset( $custom['image_2'] ) && !empty( $custom['image_2'] )){ 
							?>  
							<button class="btn btn-danger btn-xs" type="button" onclick="remove_image(this)" property="image_2"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Delete</button> 
							<?php
						} 
						?>
						
					</div> 
					<div class="col-sm-2">
						<?php 
						if( isset( $custom['image_2'] ) && !empty( $custom['image_2'] )){
							$image = 'uploads/homepage/'.$custom['image_2'].'?timestamp='.time();  
							?>  
							<input type="hidden" id="image_2" name="custom[image_2]" value="<?php echo $custom['image_2']; ?>" />
							<img id="src_image_2" src="<?php echo $image; ?>" alt=""  style="height:100px;width:100px;"/>
							<?php
						} else {
							?>   
							<img id="src_image_2"  alt=""  style="height:100px;width:100px;"/>
							<?php
						}
						?> 
					</div>
				</div> <!-- end of .form-group -->
				<div class="form-group"> 
					<label class="col-sm-2">Image Slider 1 Caption:</label>
					<div class="col-sm-4">
						<?php 
							$image_slider_one_caption = isset( $custom['image_caption_1'] )?$custom['image_caption_1']:'';
							$image_slider_two_caption = isset( $custom['image_caption_2'] )?$custom['image_caption_2']:'';
						?>
						<textarea class="form-control" name="custom[image_caption_1]"><?php echo $image_slider_one_caption; ?>
						</textarea>
					</div>
					<label class="col-sm-2">Image Slider 2 Caption:</label>
					<div class="col-sm-4">
						<textarea class="form-control" name="custom[image_caption_2]"><?php echo $image_slider_two_caption; ?>
						</textarea>
					</div> 
				</div> <!-- end of .form-group -->
				<div class="form-group"> 
					<label class="col-sm-2">Image Slider 3:</label>
					<div class="col-sm-2">
						<input class="input-xs custom_image" type="file" name="custom_image_3" property="image_3" />
						<?php 
						if( isset( $custom['image_3'] ) && !empty( $custom['image_3'] )){ 
							?>  
							<button class="btn btn-danger btn-xs" type="button" onclick="remove_image(this)" property="image_3"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Delete</button> 
							<?php
						}
						?>
					</div> 
					<div class="col-sm-2">
						<?php 
						if( isset( $custom['image_3'] ) && !empty( $custom['image_3'] )){ 
							$image = 'uploads/homepage/'.$custom['image_3'].'?timestamp='.time();  
							?> 
							<input type="hidden" id="image_3" name="custom[image_3]" value="<?php echo $custom['image_3']; ?>" />
							<img id="src_image_3" src="<?php echo $image; ?>" alt=""  style="height:100px;width:100px;"/>
							<?php
						} else {
							?>   
							<img id="src_image_3"  alt=""  style="height:100px;width:100px;"/>
							<?php
						}
						?> 
					</div>
					<label class="col-sm-2">Image Slider 4:</label>
					<div class="col-sm-2">
						<input class="input-xs custom_image" type="file" name="custom_image_4" property="image_4" /> 
						<?php 
						if( isset( $custom['image_4'] ) && !empty( $custom['image_4'] )){ 
							?> 
							<button class="btn btn-danger btn-xs" type="button" onclick="remove_image(this)" property="image_4"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Delete</button> 
							<?php
						}
						?>
					</div> 
					<div class="col-sm-2">
						<?php 
						if( isset( $custom['image_4'] ) &&!empty( $custom['image_4'] )){
							$image = 'uploads/homepage/'.$custom['image_4'].'?timestamp='.time();  
							?> 
							<input type="hidden" id="image_4" name="custom[image_4]" value="<?php echo $custom['image_4']; ?>" />
							<img id="src_image_4" src="<?php echo $image; ?>" alt=""  style="height:100px;width:100px;"/>
							<?php
						} else {
							?>   
							<img id="src_image_4"  alt=""  style="height:100px;width:100px;"/>
							<?php
						}
						?> 
					</div>
				</div> <!-- end of .form-group -->
				<div class="form-group"> 
					<label class="col-sm-2">Image Slider 3 Caption:</label>
					<div class="col-sm-4">
						<?php 
							$image_slider_three_caption = isset( $custom['image_caption_3'] )?$custom['image_caption_3']:'';
							$image_slider_four_caption = isset( $custom['image_caption_4'] )?$custom['image_caption_4']:'';
						?>
						<textarea class="form-control" name="custom[image_caption_3]"><?php echo $image_slider_three_caption; ?>
						</textarea>
					</div>
					<label class="col-sm-2">Image Slider 4 Caption:</label>
					<div class="col-sm-4">
						<textarea class="form-control" name="custom[image_caption_4]"><?php echo $image_slider_four_caption; ?>
						</textarea>
					</div> 
				</div> <!-- end of .form-group -->
				<div class="form-group"> 
					<label class="col-sm-2">Image Slider 5:</label>
					<div class="col-sm-2">
						<input class="input-xs custom_image" type="file" name="custom_image_5" property="image_5"/> 
						<?php 
						if( isset( $custom['image_5'] ) && !empty( $custom['image_5'] )){ 
							?> 
							<button class="btn btn-danger btn-xs" type="button" onclick="remove_image(this)" property="image_5"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Delete</button> 
							<?php
						}
						?>
					</div> 
					<div class="col-sm-2">
						<?php 
						if( isset( $custom['image_5'] ) && !empty( $custom['image_5'] )){
							$image = 'uploads/homepage/'.$custom['image_5'].'?timestamp='.time();  
							?> 
							<input type="hidden" id="image_5" name="custom[image_5]" value="<?php echo $custom['image_5']; ?>" />
							<img id="src_image_5" src="<?php echo $image; ?>" alt=""  style="height:100px;width:100px;"/>
							<?php
						} else {
							?>   
							<img id="src_image_5"  alt=""  style="height:100px;width:100px;"/>
							<?php
						}
						?> 
					</div>
					<label class="col-sm-2">Image Slider 6:</label>
					<div class="col-sm-2">
						<input class="input-xs custom_image" type="file" name="custom_image_6" property="image_6" /> 
						<?php 
						if( isset( $custom['image_6'] ) && !empty( $custom['image_6'] )){ 
							?> 
							<button class="btn btn-danger btn-xs" type="button" onclick="remove_image(this)" property="image_6"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Delete</button>
							<?php
						}
						?>
					</div> 
					<div class="col-sm-2">
						<?php 
						if( isset( $custom['image_6'] ) && !empty( $custom['image_6'] )){ 
							$image = 'uploads/homepage/'.$custom['image_6'].'?timestamp='.time(); 
							?>  
							<input type="hidden" id="image_6" name="custom[image_6]" value="<?php echo $custom['image_6']; ?>" />
							<img id="src_image_6" src="<?php echo $image; ?>" alt=""  style="height:100px;width:100px;"/>
							<?php
						} else {
							?>   
							<img id="src_image_6"  alt=""  style="height:100px;width:100px;"/>
							<?php
						}
						?> 
					</div>
				</div> <!-- end of .form-group -->
				<div class="form-group"> 
					<label class="col-sm-2">Image Slider 5 Caption:</label>
					<div class="col-sm-4">
						<?php 
							$image_slider_five_caption = isset( $custom['image_caption_5'] )?$custom['image_caption_5']:'';
							$image_slider_six_caption = isset( $custom['image_caption_6'] )?$custom['image_caption_6']:'';
						?>
						<textarea class="form-control" name="custom[image_caption_5]"><?php echo $image_slider_five_caption; ?>
						</textarea>
					</div>
					<label class="col-sm-2">Image Slider 6 Caption:</label>
					<div class="col-sm-4">
						<textarea class="form-control" name="custom[image_caption_6]"><?php echo $image_slider_six_caption; ?>
						</textarea>
					</div> 
				</div> <!-- end of .form-group -->
				<?php form_stadard_footer_edit_fields( $id, $published, $created, $modified,'1'); ?>
				<input type="hidden" name="<?php echo $tblid; ?>" value="<?php echo $id; ?>" />
				<input type="hidden" class="form-control" name="name" value="<?php echo $name; ?>" />
			</form> <!-- end of .form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->
<script type="text/javascript">
function remove_image( obj ){
	var property = $( obj ).attr( 'property' );  
	$('input#'+property ).val('');
	$('img#src_'+property).attr('src', ''); 
	 
} 
/* UPLOAD IMAGE */
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		var property = $( input ).attr( 'property' );  
		reader.onload = function (e) {
			$('img#src_'+property).attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
} 
$('input.custom_image').change(function(){
	readURL(this);
}); 
</script>