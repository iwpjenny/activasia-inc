<div id="delete-confirmation" style="display:none;">
	<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&id='.$id); ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Yes Delete</a></p>
</div>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2 sidebar">
			<?php $this->load->view($location.'/leftmenu-settings'); ?>			
			<?php $this->load->view($location.'/leftmain-sidebar'); ?>
		</aside>
		<div class="col-sm-10 main"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title">General Settings</h1>
					</div>
					<div class="col-sm-6 text-right"> 
						<?php btn_edit( $page, $id, array('add_data'=>$add_data,'update_data'=>$update_data,'delete_data'=>$delete_data) ); ?>
					</div>  
				</div> <!-- end of .form-group -->
				<div class="form-group msg_box">
						<?php echo show_alerts(); ?>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (*) are required fields and must not be blank.</h6>
					</div>
				</div> <!-- end of .form-group -->
				<div class="form-group">
					<label class="col-sm-2">Title *:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="title" value="<?php echo $title; ?>"  placeholder="Title" required="required"/>
					</div> <!-- end of .col-sm-4 -->
					<label class="col-sm-2">Description:</label>
					<div class="col-sm-4">
						<textarea class="form-control" name="description" row="2" placeholder="Description" /><?php echo $description; ?></textarea>
					</div> <!-- end of .col-sm-4 -->
				</div> <!-- end of .form-group -->
				
				<div class="form-group">
					<label class="col-sm-2">Custom Settings</label>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Email *:</label>
					<div class="col-sm-4">
						<input type="email" class="form-control" name="custom[email]" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="<?php echo element('email', $custom); ?>" placeholder="Email Address" required="required" />
					</div> <!-- end of .col-sm-4 -->
					<label class="col-sm-2">Name *:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="custom[company_name]" value="<?php echo element('company_name', $custom); ?>" placeholder="Company Name"  required="required"/>
					</div> <!-- end of .col-sm-4 -->
				</div> <!-- end of .form-group -->
				<div class="form-group">
					<label class="col-sm-2">Description:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="custom[system_description]" value="<?php echo element('system_description', $custom); ?>" placeholder="System Description" />
					</div> <!-- end of .col-sm-4 -->
					<label class="col-sm-2">Environment:</label>
					<div class="col-sm-4">
						<label><input type="radio" name="custom[environment]" value="live" <?php checked( element('environment', $custom),'live'); ?>   /> Live</label>
						<label><input type="radio" name="custom[environment]" value="local" <?php checked( element('environment', $custom),'local'); ?>  /> Local</label>
						<label><input type="radio" name="custom[environment]" value="development" <?php checked( element('environment', $custom),'development'); ?>  /> Development</label>
						<br />
						<small>Environment is for Developers only.</small>
					</div> <!-- end of .col-sm-4 -->
				</div> <!-- end of .form-group --> 
				<div class="form-group">
					<label class="col-sm-2">Website *:</label>
					<div class="col-sm-4">
						<input type="url" class="form-control" name="custom[website]" value="<?php echo element('website', $custom); ?>" placeholder="Website" required="required"/>
					</div> <!-- end of .col-sm-4 -->
					<label class="col-sm-2">Live Site *:</label>
					<div class="col-sm-4">
						<input type="url" class="form-control" name="custom[live_site]" value="<?php echo element('live_site', $custom); ?>" placeholder="Live Site" required="required"/>
					</div> <!-- end of .col-sm-4 -->
				</div> <!-- end of .form-group -->   
				<?php form_stadard_footer_edit_fields( $id, $published, $created, $modified,'1'); ?>
				<input type="hidden" name="<?php echo $tblid; ?>" value="<?php echo $id; ?>" />
				<input type="hidden" name="name" value="<?php echo $name; ?>" />
			</form> <!-- end of .form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->
