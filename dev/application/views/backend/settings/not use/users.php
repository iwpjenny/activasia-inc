<div id="delete-confirmation" style="display:none;">
	<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&id='.$id); ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Yes Delete</a></p>
</div>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2 sidebar">
			<?php $this->load->view($location.'/leftmenu-settings'); ?>			
			<?php $this->load->view($location.'/leftmain-sidebar'); ?>
		</aside>
		<div class="col-sm-10 main"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title">Users Settings</h1>
					</div>
					<div class="col-sm-6 text-right"> 
						<?php btn_edit( $page, $id, array('add_data'=>$add_data,'update_data'=>$update_data,'delete_data'=>$delete_data) ); ?>
					</div>  
				</div> <!-- end of .form-group -->
				<div class="form-group msg_box">
						<?php echo show_alerts(); ?>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (*) are required fields and must not be blank.</h6>
					</div>
				</div> <!-- end of .form-group -->
				<div class="form-group">
					<label class="col-sm-2">Title *:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="title" value="<?php echo $title; ?>"  placeholder="Title" required="required"/>
					</div> <!-- end of .col-sm-4 -->
					<label class="col-sm-2">Description:</label>
					<div class="col-sm-4">
						<textarea class="form-control" name="description" row="2" placeholder="Description" /><?php echo $description; ?></textarea>
					</div> <!-- end of .col-sm-4 -->
				</div> <!-- end of .form-group -->
				<hr />
				<div class="form-group">
					<label class="col-sm-2">Custom Settings</label>
				</div> 
				<div class="form-group">  
					<label class="col-sm-3">Login Using Email *: </label>
					<div class="col-sm-3"> 
						<label><input type="radio" name="custom[email_login]" value="1" required="required" <?php checked( element('email_login', $custom),1); ?> /> Yes</label> &nbsp;&nbsp;
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						<label><input type="radio" name="custom[email_login]" value="0" required="required" <?php checked( element('email_login', $custom),0); ?>/> No</label>
					</div> 
					<label class="col-sm-3">Enable Admin's Profile Picture *: </label>
					<div class="col-sm-3"> 
						<label><input type="radio" name="custom[prof_pic_admin]" value="1" required="required" <?php checked( element('prof_pic_admin', $custom),1); ?> /> Yes</label> &nbsp;&nbsp;
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						<label><input type="radio" name="custom[prof_pic_admin]" value="0" required="required" <?php checked( element('prof_pic_admin', $custom),0); ?>/> No</label>
					</div>
				</div>
				<div class="form-group">    
					<label class="col-sm-3">Admin's Failed Log-in Attempts Limit: </label>
					<div class="col-sm-3"> 
						<input type="number" class="form-control" name="custom[failed_attempts_admin]" min="0" value="<?php echo element('failed_attempts_admin', $custom); ?>"  placeholder="0" required="required"/> 
					</div>
					<label class="col-sm-3">Member's Failed Log-in Attempts Limit: </label>
					<div class="col-sm-3"> 
						<input type="number" class="form-control" name="custom[failed_attempts_member]" min="0" value="<?php echo element('failed_attempts_member', $custom); ?>"  placeholder="0" required="required"/> 
					</div>
				</div>
				<div class="form-group">   
					<label class="col-sm-3">Enable Member's Profile Picture *: </label>
					<div class="col-sm-3"> 
						<label><input type="radio" name="custom[prof_pic_member]" value="1" required="required" <?php checked( element('prof_pic_member', $custom),1); ?> /> Yes</label> &nbsp;&nbsp;
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						<label><input type="radio" name="custom[prof_pic_member]" value="0" required="required" <?php checked( element('prof_pic_member', $custom),0); ?>/> No</label>
					</div>
					
				</div>  
				<?php form_stadard_footer_edit_fields( $id, $published, $created, $modified,'1'); ?>
				<input type="hidden" name="<?php echo $tblid; ?>" value="<?php echo $id; ?>" />
				<input type="hidden" name="name" value="<?php echo $name; ?>" />
			</form> <!-- end of .form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->
