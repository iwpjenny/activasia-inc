<div id="delete-confirmation" style="display:none;">
	<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&id='.$id); ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Yes Delete</a></p>
</div>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2 sidebar">
			<?php $this->load->view($location.'/leftmenu-settings'); ?>			
			<?php $this->load->view($location.'/leftmain-sidebar'); ?>
		</aside>
		<div class="col-sm-10 main"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title">Date and Time Settings</h1>
					</div>
					<div class="col-sm-6 text-right"> 
						<?php btn_edit( $page, $id, array('add_data'=>$add_data,'update_data'=>$update_data,'delete_data'=>$delete_data) ); ?>
					</div> 
				</div> <!-- end of .form-group -->
				<div class="form-group msg_box">
						<?php echo show_alerts(); ?>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (*) are required fields and must not be blank.</h6>
					</div>
				</div> <!-- end of .form-group -->
				<div class="form-group">
					<label class="col-sm-2">Title *:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="title" value="<?php echo $title; ?>"  placeholder="Title" required="required"/>
					</div> 
					<label class="col-sm-2">Description:</label>
					<div class="col-sm-4">
						<textarea class="form-control" name="description" row="2" placeholder="Description" /><?php echo $description; ?></textarea>
					</div> 
					<label class="col-sm-2 control-label"></label>
					<div class="col-sm-4">
						
					</div> 
				</div> <!-- end of .form-group -->
				
				<div class="form-group">
					<label class="col-sm-2">Custom Settings</label>
				</div>
				<?php //printx(timezone_identifiers_list()); ?>
				<div class="form-group">
					<label class="col-sm-3">Record View Date and Time:</label>
					<div class="col-sm-4">
						<?php //echo // $timezone; ?>
					</div> 
				</div> <!-- end of .form-group -->
				<div class="form-group">
					<label class="col-sm-2">Date Format:</label>
					<div class="col-sm-4">
						<input type="radio" name="custom[date_format]" value="Y/m/d" <?php checked( $custom['date_format'],'Y/m/d'); ?> /> <?php echo "YYYY/MM/DD"; ?><br />
						<input type="radio" name="custom[date_format]" value="m/d/Y" <?php checked( $custom['date_format'],'m/d/Y'); ?> /> <?php echo "MM/DD/YYYY"; ?><br />
						<input type="radio" name="custom[date_format]" value="d/m/Y" <?php checked( $custom['date_format'],'d/m/Y'); ?> /> <?php echo "DD/MM/YYYY"; ?><br /> 
						<input type="radio" name="custom[date_format]" value="Y-m-d" <?php checked( $custom['date_format'],'Y-m-d'); ?> /> <?php echo "YYYY-MM-DD"; ?><br />
						<input type="radio" name="custom[date_format]" value="m-d-Y" <?php checked( $custom['date_format'],'m-d-Y'); ?> /> <?php echo "MM-DD-YYYY"; ?><br />
						<input type="radio" name="custom[date_format]" value="F j, Y" <?php checked( $custom['date_format'],'F j, Y'); ?> /> <?php echo "Full Month Date, Year"; ?><br /> 
						<input type="radio" name="custom[date_format]" value="M j, Y" <?php checked( $custom['date_format'],'M j, Y'); ?> /> <?php echo "Abrv Month Date, Year"; ?><br /> 
					</div> 
					
					<label class="col-sm-2">Time Format:</label>
					<div class="col-sm-4">
						<input type="radio" name="custom[time_format]" value="h:ia" <?php checked( $custom['time_format'],'h:ia'); ?> /> <?php echo date("h:ia"); ?><br />
						<input type="radio" name="custom[time_format]" value="h:iA" <?php checked( $custom['time_format'],'h:iA'); ?> /> <?php echo date("h:iA");?><br />
						<input type="radio" name="custom[time_format]" value="h:i" <?php checked( $custom['time_format'],'h:i'); ?> /> <?php echo date("h:i");?> (24 hrs format)<br /> 
					</div> 
				</div> <!-- end of .form-group --> 
				<hr>
				<div class="form-group">
					<label class="col-sm-3">Record Grid Date and Time:</label>
					<div class="col-sm-4">
						<?php //echo // $timezone; ?>
					</div> 
				</div> <!-- end of .form-group -->
				<div class="form-group">
					<label class="col-sm-2">Date Format:</label>
					<div class="col-sm-4">
						<input type="radio" name="custom[grid_date_format]" value="Y/m/d" <?php checked( $custom['grid_date_format'],'Y/m/d'); ?> /> <?php echo "YYYY/MM/DD"; ?><br />
						<input type="radio" name="custom[grid_date_format]" value="m/d/Y" <?php checked( $custom['grid_date_format'],'m/d/Y'); ?> /> <?php echo "MM/DD/YYYY"; ?><br />
						<input type="radio" name="custom[grid_date_format]" value="d/m/Y" <?php checked( $custom['grid_date_format'],'d/m/Y'); ?> /> <?php echo "DD/MM/YYYY"; ?><br /> 
						<input type="radio" name="custom[grid_date_format]" value="Y-m-d" <?php checked( $custom['grid_date_format'],'Y-m-d'); ?> /> <?php echo "YYYY-MM-DD"; ?><br />
						<input type="radio" name="custom[grid_date_format]" value="m-d-Y" <?php checked( $custom['grid_date_format'],'m-d-Y'); ?> /> <?php echo "MM-DD-YYYY"; ?><br />
						<input type="radio" name="custom[grid_date_format]" value="F j, Y" <?php checked( $custom['grid_date_format'],'F j, Y'); ?> /> <?php echo "Full Month Date, Year"; ?><br /> 
						<input type="radio" name="custom[grid_date_format]" value="M j, Y" <?php checked( $custom['grid_date_format'],'M j, Y'); ?> /> <?php echo "Abrv Month Date, Year"; ?><br /> 
					</div> 
					
					<label class="col-sm-2">Time Format:</label>
					<div class="col-sm-4">
						<input type="radio" name="custom[grid_time_format]" value="h:ia" <?php checked( $custom['grid_time_format'],'h:ia'); ?> /> <?php echo date("h:ia"); ?><br />
						<input type="radio" name="custom[grid_time_format]" value="h:iA" <?php checked( $custom['grid_time_format'],'h:iA'); ?> /> <?php echo date("h:iA");?><br />
						<input type="radio" name="custom[grid_time_format]" value="h:i" <?php checked( $custom['grid_time_format'],'h:i'); ?> /> <?php echo date("h:i");?> (24 hrs format)<br /> 
					</div> 
				</div> <!-- end of .form-group --> 
				<?php form_stadard_footer_edit_fields( $id, $published, $created, $modified,'1'); ?>
				<input type="hidden" name="<?php echo $tblid; ?>" value="<?php echo $id; ?>" />
				<input type="hidden" class="form-control" name="name" value="<?php echo $name; ?>"/>
			</form> <!-- end of .form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->
