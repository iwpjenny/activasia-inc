<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2">
			<?php //$this->load->view('leftmenu-settings'); ?>
			<?php //$this->load->view('leftmenu-list'); ?>
			<!--?php $this->load->view('settings/sidebar'); ?>
			<!?php $this->load->view('leftmain-sidebar'); ?-->
		</aside>

		<div class="col-sm-10 main">
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title">Edit Settings</h1>
					</div>
				</div>
				<?php $this->notify->show(); ?>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (*) are required fields and must not be blank.</h6>
					</div>
				</div> <!-- end of .form-group -->
				<div class="form-group">
					<label class="col-sm-2">Title *:</label>
					<div class="col-sm-4">
						<strong><?php app_value($fields,$this->Set->tblpref.'title'); ?></strong>
					</div>
					<label class="col-sm-2">Name/Slug *:</label>
					<div class="col-sm-4">
						<strong><?php app_value($fields,$this->Set->tblpref.'name'); ?></strong>
					</div>
				</div> <!-- end of .form-group -->
				<div class="form-group">
					<label class="col-sm-2">Description:</label>
					<div class="col-sm-4">
						<?php app_value($fields,$this->Set->tblpref.'description'); ?>
					</div>
				</div> <!-- end of .form-group -->
				<?php
				if( $set_id ){
				?>
					<div class="form-group">
						<div class="col-xs-6 col-sm-2">Created:</div>
						<div class="col-xs-6 col-sm-2">
							<small><?php app_value($fields,$this->Set->tblpref.'created'); ?></small>
						</div>
						<div class="col-xs-6 col-sm-2">Modified:</div>
						<div class="col-xs-6 col-sm-2">
							<small><?php app_value($fields,$this->Set->tblpref.'modified'); ?></small>
						</div>
					</div> <!-- end of .form-group -->
					<input type="hidden" name="set_id" value="<?php echo $set_id; ?>" />
				<!--FOR SINGLE RECORD -->
				<?php
			 	}

				$config_format = $this->SFM->get_config_format($set_id, $multiple_records = 0);

				if( $config_format ){
					?>
						<div class="form-group" id="header-title-buttons-format">
							<div class="col-sm-6">
								<h3 class="title">Custom Configuration</h3>
							</div>
							<div class="col-sm-6 text-right">
								<?php btn_edit( $page, array('capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$this->Set->urc_name,'id_field'=>'set_id','display'=>array('edit','delete')) ); ?>
							</div>
						</div>
					<?php
					foreach($config_format as $format){
						$format = $this->Set->show_config_format( $format, $prefix_format, $set_name );
						?>
						<div class="form-group">
							<label class="col-sm-2"><?php echo $format['setf_title']; ?></label>
							<div class="col-sm-10">
								<?php echo $format['format_type']; ?>
							</div>
						</div>
						<?php
					}
				}
				?>
			</form> <!-- end of .form -->
			<hr />
			<!--END OF SINGLE RECORD -->
			<!--FOR MULTIPLE RECORD -->
			<?php
			$config_format = $this->SFM->get_config_format($set_id, $multiple_records = 1);
			 if( $config_format ){
				foreach($config_format as $format){
					$format_data['format'] = $format;
					$this->load->view($location.'/settings/multiple_records', $format_data);
				}
				?>
				<hr />
				<!--END OF MULTIPLE RECORD-->
				<!--FOR SUB CONFIG-->
				<?php
			}
			//$this->load->view('settings/sub_config', $set_id);
			?>
			<!--END OF SUB CONFIG -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->
<div id="delete-confirmation" style="display:none;">
	<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&id='.$set_id); ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Yes Delete</a></p>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$( "#settings_form" ).submit(function( event ) {
		var lowerCase= new RegExp('[a-z]');
		var slug = $("input[name='name']").val();

		if(slug.indexOf(' ') > -1 || slug.replace(/[^A-Z]/g, "").length > 0 )
		{
		  alert('Name contains illegal characters or space.');
		  event.preventDefault();
		}

	});
});
</script>
