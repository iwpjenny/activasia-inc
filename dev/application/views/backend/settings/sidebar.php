<div class="list-group" style="margin-top:10px;">
	<?php
	if( $this->usersrole->check( $this->Set->urc_name, 'view' ) ){
		?>
		<li class="list-group-item"><h4 class="list-group-item-heading">Settings Menu</h4></li>
		<a href="<?php echo site_url('c=settings'); ?>" class="list-group-item default">
		<span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Settings</strong>
			<p class="list-group-item-text"></p>
		</a>
		<?php 
	}
	?>
</div> 
<?php $this->load->view($location.'/backup/sidebar'); ?>  