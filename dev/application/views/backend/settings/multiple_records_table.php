<?php
$grid_date_time_format = app_val($settings,'grid_date_time_format'); 
$character_limit = app_val($settings,'character_limit');
?>
<div class="table-responsive">  
	<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>Date Added</th>
				<th width="4%"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></th>
				<th>Option</th>
			</tr>
		</thead>
		<tbody>
		<?php
		if( $format_records ){
			$n=1;
			foreach($format_records as $col){
				$setr_id_val = get_value($col,'setr_id');
				$setr_value = get_value($col,'setr_value');
				$setf_id = get_value($col,'setf_id'); 
				$created = get_value($col,'setr_created');
							
				$created_display = datetime($created,$grid_date_time_format);	
				?>
				<tr>
					<td><?php echo $n; ?></td> 
					<td><?php echo $setr_value; ?></td> 
					<td align="right" title="<?php echo $created; ?>">
						<small><?php echo $created_display; ?><small>
					</td>
					<td><?php published( $published ); ?></td>
					<?php
					if( $this->usersrole->check( $this->Set->urc_name, 'view' ) ){
						?>
						<td class="text-right">
						<?php
						if( $this->usersrole->check( $this->Set->urc_name, 'edit' ) && $setf_type != 'select' ){ 
							?>
							<a class="btn btn-primary btn-xs" href="<?php echo site_url('c='.$page.'&m=edit&set_id='.$set_id.'&setr_id='.$setr_id_val); ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>Edit</a>
							<?php
						}
						if( $this->usersrole->check( $this->Set->urc_name, 'delete' ) ){
							?>
							<a class="btn btn-danger btn-xs confirmation" href="#delete-confirmation-<?php echo $set_id; ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Delete</a>
							<div id="delete-confirmation-<?php echo $set_id; ?>" style="display:none;">
								<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete_multiple_records&set_id='.$set_id.'&setr='.$setr_id_val); ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Yes Delete</a></p>
							</div>
							<?php
						}
						?>
						</td>
						<?php
					}
					?>
				</tr>
				<?php
				$n++;
			}
		} else {
			?>
			<tr>
				<td colspan="5">
					<div class="alert alert-warning">No Results Found.</div>
				</td>
			</tr>
			<?php
		}
		?>
		</tbody>
	</table>
</div>