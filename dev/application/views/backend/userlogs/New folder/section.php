<?php
if( $this->usersrole->check( $this->UL->urc_name, 'records' ) && $display_latest_user_logs ){
?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel-group basic_reg_form">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="<?php echo site_url('c='.$this->UL->page); ?>">Latest <?php echo $this->UL->name; ?></a>
				</div>
				<div class="panel-body"> 
				<?php $this->load->view($location.'/'.$this->UL->view.'/grid'); ?>
				</div>
			</div>
		</div>
	</div> 
</div>  
<?php 
}
?>