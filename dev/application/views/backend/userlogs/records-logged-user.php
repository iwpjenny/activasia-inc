<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
?>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2 sidebar">
			<?php $this->load->view($location.'/leftmenu'); ?>
			<?php $this->load->view($location.'/user/sidebar'); ?>
		</aside>
		<div class="col-sm-10 main"> 
			<?php $this->recordsfilter->show(); ?>
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-6">
						<h1>My <?php echo $model->name; ?> Records</h1>
					</div> 
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th>
								<th>Action</th>
								<th>Description</th>
								<th>Created</th> 
								<th>Option</th> 
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						if( $record_rows ){
							$n=1;
							foreach( $record_rows as $col ){
								$id = get_value($col,$model->tblid);
								$action = get_value($col,$model->tblpref.'action'); 
								$description = get_value($col,$model->tblpref.'description');
								$created = get_value($col,$model->tblpref.'created');				
								$created_display = datetime($created,$grid_date_time_format);
								$description_display = character_limiter($description,$grid_text_limit);
								?>
								<tr>
									<td><?php echo $n; ?></td>
									<td><?php echo $action; ?></td>
									<td title="<?php echo $description; ?>"><?php echo $description_display; ?></td>
									<td title="<?php echo $created; ?>" align="right"><small><?php echo $created_display; ?></small></td>
									<td>
									<?php	 
									btn_optn_records($page, array(
										'model'=>$model,'fields'=>$col,
										'capabilities'=>$capabilities,
										'id'=>$id,
										'display'=>array('view')
									));
									?>
									</td>
								</tr>
								<?php
								$n++;
							} /*  end of foreach */
						}
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
				<?php $this->load->view($location.'/modules/records-footer-pagination'); ?> 
			</form> <!-- end of form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->