 <div class="table-responsive">
	<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>User</th>
				<th>Action</th>
				<th>Description</th>
				<th>Created</th> 
				<th>Option</th> 
			</tr>
		</thead> <!-- end of thead -->
		<tbody>
		<?php
		if( $records ){
			$n=1;
			foreach( $records as $col ){
				$id = get_value($col, $this->ML->tblpref.'id');
				$action = get_value($col, $this->ML->tblpref.'action'); 
				$description = get_value($col, $this->ML->tblpref.'description');
				$created = get_value($col, $this->ML->tblpref.'created');
				$firstname = get_value($col, $this->Mem->tblpref.'firstname'); 
				$lastname = get_value($col, $this->Mem->tblpref.'lastname');
				$created_display = datetime($created,$grid_date_time_format); 
				$description_display = character_limiter($description,$character_limit);
				?>
				<tr>
					<td><?php echo $n; ?></td>
					<td><?php echo $lastname.', '.$firstname ; ?></td>
					<td><?php echo $action; ?></td>
					<td title="<?php echo $description; ?>"><?php echo $description_display; ?></td>
					<td title="<?php echo $created; ?>" align="right"><small><?php echo $created_display; ?></small></td>
					<td class="text-right">
						<?php 
						btn_optn_records($this->ML->page, array(
							'capabilities'=>$capabilities,
							'ur_name'=>$this->ML->urc_name,
							'id'=>$id,
							'display'=>array('view')
						));
						?>
					</td>
				</tr>
				<?php
				$n++;
			} /*  end of foreach */
		}
		?>
		</tbody> <!-- end of tbody -->
	</table> <!-- end of table -->
	<?php $this->notify->records( $records ); ?>
</div> <!-- end of .table-responsive --> 