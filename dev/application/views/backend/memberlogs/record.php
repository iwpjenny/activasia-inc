<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
?>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2 sidebar">
			<?php $this->load->view($location.'/leftmenu'); ?>
			<?php $this->load->view($location.'/user/sidebar'); ?>
		</aside>
		<div class="col-sm-10 main"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $this->ML->name; ?> Records</h1>
					</div> 
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th>
								<th>User</th>
								<th>Action</th>
								<th>Description</th>
								<th>Created</th> 
								<th>Option</th> 
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						if( $record_rows ){
							$n=1;
							foreach( $record_rows as $col ){
								$id = get_value($col,$this->ML->tblid);
								$action = get_value($col,$this->ML->tblpref.'action'); 
								$description = get_value($col,$this->ML->tblpref.'description');
								$created = get_value($col,$this->ML->tblpref.'created');
								$firstname = get_value($col,$this->ML->tblpref.'firstname'); 
								$lastname = get_value($col,$this->ML->tblpref.'lastname');
				
								$created_display = datetime($created,$grid_date_time_format);
								$description_display = character_limiter($description,$grid_text_limit);
								?>
								<tr>
									<td><?php echo $n; ?></td>
									<td><?php echo $lastname.', '.$firstname ; ?></td>
									<td><?php echo $action; ?></td>
									<td title="<?php echo $description; ?>"><?php echo $description_display; ?></td>
									<td title="<?php echo $created; ?>" align="right"><small><?php echo $created_display; ?></small></td>
									<td>
									<?php	 
									btn_optn_records($page, array(
										'capabilities'=>$capabilities,
										'ur_name'=>$this->ML->urc_name,
										'id'=>$id,
										'display'=>array('view')
									));
									?>
									</td>
								</tr>
								<?php
								$n++;
							} /*  end of foreach */
						}
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
				<nav>
					<?php echo $pagination; ?>
					<div class="showing-result"><?php app_pagination_details($per_page, $n); ?></div>
				</nav>
			</form> <!-- end of form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->