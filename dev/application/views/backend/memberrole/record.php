<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
?>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2 sidebar">
			<?php $this->load->view($location.'/user/sidebar'); ?>
		</aside>
		<div class="col-sm-10 main"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6"> 
						<h1 class="title"><?php echo $model->name; ?> Records</h1>
					</div>
						<div class="col-sm-6 text-right">
						<?php btn_records( $page, array('capabilities'=>$capabilities,'ur_name'=>$model->urc_name,'display'=>array('add','trash')) ); ?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th>
								<th>Title</th>
								<th>Position</th>
								<th>Description</th>
								<th>Created</th>
								<th><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></th>
								<th>Option</th>
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						if( $record_rows ){
							$n=1;
							foreach($record_rows as $col){
								$id = get_value($col,$model->tblid);
								$title = get_value($col,$model->tblpref.'title');
								$position = get_value($col,$model->tblpref.'position');
								$description = get_value($col,$model->tblpref.'description');
								$created = get_value($col,$model->tblpref.'created');
								$published = get_value($col,$model->tblpref.'published');
								$created_display = datetime($created,$grid_date_time_format);
								$description_display = character_limiter($description,$grid_text_limit);
								
								$created_display = datetime($created,$grid_date_time_format);
								$title_display = character_limiter($title,$grid_text_limit);
								?>
								<tr>
									<td><?php echo $n; ?></td>
									<td title="<?php echo $title; ?>"><?php echo $title_display; ?></td>
									<td><?php echo $position; ?></td>
									<td title="<?php echo $description; ?>"><?php echo $description_display; ?></td>
									<td title="<?php echo $created; ?>" align="right">
									<small><?php echo $created_display; ?></small>
									</td>
									<td><?php published( $published ); ?></td>
									<td>
									<?php	 
									btn_optn_records($page, array(
										'capabilities'=>$capabilities,
										'ur_name'=>$model->urc_name,
										'id'=>$id,
										'display'=>array('edit','delete')
									));
									?>
									</td>
								</tr>
								<?php
								$n++;
							}
						}
						?>
						</tbody>
					</table>
				</div>
				<?php $this->notify->records( $record_rows ); ?>
				<nav>
					<?php echo $pagination; ?>
					<div class="showing-result"><?php app_pagination_details($per_page, $n); ?></div>
				</nav>
			</form> <!-- end of form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->