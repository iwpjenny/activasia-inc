<?php
$current_value_access = $this->usersrole->get_capability($selected_capabilities,'access','full');
?>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2 sidebar">
			<?php $this->load->view($location.'/user/sidebar'); ?>
		</aside>
		<div class="col-sm-10 main">
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $this->MR->name; ?> Edit</h1>
					</div>
					<div class="col-sm-6 text-right">
						<?php btn_edit( $page, array('capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$this->MR->urc_name) ); ?>
					</div>
				</div>
				<?php $this->notify->show(); ?>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (*) are required fields and must not be blank.</h6>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Title *:</label>
					<div class="col-sm-4">
						<?php app_field('title',$this->MR,$fields,array('holder'=>'Title')); ?>
					</div>
					<label class="col-sm-2">Name *:</label>
					<div class="col-sm-4">
						<?php app_field('name',$this->MR,$fields,array('holder'=>'Name')); ?>
						<small>Value format: small letters only, no space, space must be dash -.</small>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Position:</label>
					<div class="col-sm-4">
						<?php app_field('position',$this->MR,$fields,array('holder'=>'Position')); ?>
					</div>
					<label class="col-sm-2">Description:</label>
					<div class="col-sm-4">
						<?php app_field('description',$this->MR,$fields,array('holder'=>'Description')); ?>
					</div>
				</div>
				<hr />
				<?php $this->load->view($location.'/edit-form-footer-fields'); ?>
				<hr />
				<div class="form-group">
					<label class="col-sm-2">Administrator:</label>
					<div class="col-sm-4">
						<input type="checkbox" name="capabilities_selected[access][full]" id="select-all" value="yes" <?php checked($current_value_access,'yes'); ?> /> Full Access
						<small>(Super user only)</small>
					</div>
				</div>
				<hr />
				<?php $this->MR->urc_records( $urc_standard_records, $selected_capabilities ); ?>
				<?php $this->MR->urc_records( $urc_modules_records, $selected_capabilities ); ?>
				<input type="hidden" name="<?php echo $tblid; ?>" value="<?php echo $id; ?>" />
			</form> <!-- end of .form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->
<div id="delete-confirmation" style="display:none;">
	<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&id='.$id); ?>">Yes Delete</a></p>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $("input#select-all").click(function(event) {
        if( this.checked ) {
            $(':checkbox').each(function() {
                this.checked = true;
            });
        } else {
            $(':checkbox').each(function() {
                this.checked = false;
            });
        }
    });
});
</script>
