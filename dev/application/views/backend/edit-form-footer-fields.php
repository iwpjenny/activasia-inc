<?php
$view_date_time_format = app_get_val($settings,'view_date_time_format');

$created = app_get_val( $fields, $model->tblpref.'created');
$modified = app_get_val( $fields, $model->tblpref.'modified');

$created_display = datetime($created, $view_date_time_format);
$modified_display = datetime($modified, $view_date_time_format);

$view_published = TRUE;
$default_status = $this->setconf->get('records','default_status'); 
if( $default_status ){
	$status_value = 1;
} else {
	$status_value = 0;  
}

if( $id ){
	if( $view_published ){ 
	$publish_name = get_published_name();
	?>
	<div class="form-group">
		<label class="col-xs-4 col-sm-2"><?php echo $publish_name; ?>:</label>
		<div class="col-xs-4 col-sm-2">
			<label>
				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				<?php app_field('published',$model,$fields,array('type'=>'radio','value'=>1,'compare_values'=>array(1,''))); ?> Yes
			</label>
		</div>
		<div class="col-xs-4 col-sm-2">
			<label>
				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				<?php app_field('published',$model,$fields,array('type'=>'radio','value'=>0,'compare_values'=>0)); ?> No
			</label>
		</div>
		<label class="col-xs-6 col-sm-2"></label>
		<div class="col-xs-6 col-sm-4">
		</div>
	</div> <!-- end of .form-group -->
	<?php 
	}
	?>
	<div class="form-group">
		<div class="col-xs-6 col-sm-2">Created:</div>
		<div class="col-xs-6 col-sm-4">
			<small title="<?php echo $created; ?>"><?php echo $created_display; ?></small>
		</div>
		<div class="col-xs-6 col-sm-2">Modified:</div>
		<div class="col-xs-6 col-sm-4">
			<small title="<?php echo $modified; ?>"><?php echo $modified_display; ?></small>
		</div>
	</div> <!-- end of .form-group -->
	<?php
} else { 
	app_field('published',$model,$fields,array('type'=>'hidden','value'=>$status_value));
}
?>