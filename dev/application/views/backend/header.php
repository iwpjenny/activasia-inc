<?php
$active = 'active';
$meta_title = app_get_val($settings,'meta_title'); 
$client_acronym = app_get_val($config_client,'client_acronym'); 
$meta_title = $meta_title?$meta_title:$client_acronym;

 $ca_pending_count = $this->custombackend->get_ca_pending_count();
 $ca_release_count = $this->custombackend->get_ca_release_count();
  // $ca_pending_count = ''
 
$page_transactions = 'ModTransactions';
$page_transactionssub = 'ModTransactionsSub';
$cost_estimate = 'ModCostEstimate';
$cost_estimate_csv = 'ModCostEstimateCSV';
$cost_estimate_logs = 'ModCostEstimateLogs';
$working_budget = 'ModWorkingBudget';
$working_budget_logs = 'ModWorkingBudgetLogs';
$working_budget_csv = 'ModWorkingBudgetCSV';
$cash_advance = 'ModCashAdvance';
$cash_advance_status = 'ModCashAdvanceStatus';
$cash_advance_logs = 'ModCashAdvanceLogs'; 
$region = 'ModRegion';
$position_approved = 'ModPositionApproved';
$area = 'ModArea';
$backup = 'BackUp';
$settings = 'Settings';

$display_ca_action = $this->custombackend->display_ca_action();
$show_status_notification = app_get_val($display_ca_action,'show_status_notification');  
$show_status_approve_button = app_get_val($display_ca_action,'show_status_approve_button');  
$show_status_decline_button = app_get_val($display_ca_action,'show_status_decline_button');  
$show_status_release_button = app_get_val($display_ca_action,'show_status_release_button');  
 
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#profile-menu').removeClass('active');
	var ckeyword_header = '';
	// ckeyword_header = $('input#ckeyword').val();
	ckeyword_header = "<?php if( isset( $keyword ) && $keyword ){ echo $keyword; } else { echo '';}  ?>";
	ckeyword_header = String(ckeyword_header);
	if( ckeyword_header != false ){
		if( ckeyword_header.length > 0  ){
			$('div#check_sort').show();
		} else {
			$('div#check_sort').hide();
		}
	}

});

$('form.search-header').submit(function( event ){
	var ckeyword_header_get = "<?php if( isset( $keyword ) && $keyword ){ echo $keyword; } else { echo '';}  ?>";
	var ckeyword_header = $('input#ckeyword').val();
	var ckeyword_header_trim = ckeyword_header.trim();
	var ckeyword_num_verify = isNaN( ckeyword_header );
	var ckeyword_date_verify = Date.parse(ckeyword_header);
	$('input#ckeyword').val( ckeyword_header_trim );
	if( ckeyword_header_get == false ){
		if( ckeyword_num_verify == false ){
			$('div#check_sort select[name=sort]').val('DESC');
		} else if( ckeyword_date_verify > 0  ){
			$('div#check_sort select[name=sort]').val('DESC');
		} else {
			$('div#check_sort select[name=sort]').val('ASC');
		}

	}
});

$('div#check_sort select[name=sort]').change(function( event ){
	var ckeyword_header = $('input#ckeyword').val();
	var ckeyword_header_trim = ckeyword_header.trim();
	ckeyword_header = String(ckeyword_header);
	if( ckeyword_header.length > 0  ){
		$('input#ckeyword').val( ckeyword_header_trim );
		$('form.search-header').submit();

	}

});
</script>
<!-- start: header -->
<header class="container-fluid">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span> 
				</button>
				<?php echo $meta_title; ?>
				<a class="navbar-brand" href="<?php echo site_url(); ?>"><?php echo $meta_title; ?></a>
			</div> <!-- end of .navbar-header -->
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="<?php active($page,$this->BLog->dashboard_page); ?>"><a href="<?=site_url('c='.$this->BLog->dashboard_page);?>">Dashboard</a></li>
					<li class="dropdown <?php active($page,array($cost_estimate,$cash_advance,$working_budget)); ?>">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Records <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li class="<?php active($page,$cost_estimate); ?>"><a href="<?php echo site_url('c='.$cost_estimate); ?>"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> Cost Estimate</a></li> 
							<li class="<?php active($page,$cost_estimate_csv); ?>"><a href="<?php echo site_url('c='.$cost_estimate_csv); ?>"><span class="" aria-hidden="true"></span>CE Import CSV</a></li>  
							<li class="<?php active($page,$cost_estimate_logs); ?>"><a href="<?php echo site_url('c='.$cost_estimate_logs); ?>"><span class="" aria-hidden="true"></span>CE Logs</a></li> 
							<li role="separator" class="divider"></li>
							<li class="<?php active($page,$working_budget); ?>"><a href="<?php echo site_url('c='.$working_budget); ?>"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> Budget Control Sheet</a></li>
							<li role="separator"></li>
							<li class="<?php active($page,$working_budget_csv); ?>"><a href="<?php echo site_url('c='.$working_budget_csv); ?>"><span class="" aria-hidden="true"></span>Import Budget</a></li>    
							<li class="<?php active($page,$working_budget_logs); ?>"><a href="<?php echo site_url('c='.$working_budget_logs); ?>"><span class="" aria-hidden="true"></span>BCS Logs</a></li> 
							<li role="separator" class="divider"></li>	
							<li class="<?php active($page,$cash_advance); ?>"><a href="<?php echo site_url('c='.$cash_advance); ?>"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> Cash Advance</a></li>
							<li role="separator"></li>   
							<li class="<?php active($page,$cash_advance_status); ?>"><a href="<?php echo site_url('c='.$cash_advance_status); ?>"><span class="" aria-hidden="true"></span>Status Logs</a></li> 
							<li class="<?php active($page,$cash_advance_logs); ?>"><a href="<?php echo site_url('c='.$cash_advance_logs); ?>"><span class="" aria-hidden="true"></span>CA Logs</a></li>  
							<?php 
							if( $show_status_notification == 'yes' ){ 
								if( $show_status_release_button == 'yes' ){ 
									?>
									<li><a href="<?php echo site_url('c='.$cash_advance.'&m=record_release'); ?>"><span class="" aria-hidden="true"></span>CA Release <span class="badge"><?php echo $ca_release_count; ?></span></a></li> 
									<?php
								}
								if( $show_status_approve_button == 'yes' ){
									?>
									<li><a href="<?php echo site_url('c='.$cash_advance.'&m=record_pending'); ?>"><span class="" aria-hidden="true"></span>CA Pending <span class="badge"><?php echo $ca_pending_count; ?></span></a></li> 
									<?php
								}
							}
							?> 
						</ul>
					</li> 
					<li class="dropdown <?php active($page,array($this->Set->page,$this->BU->page,$region,$area,$position_approved)); ?>">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manager <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<?php if( $this->usersrole->check( $this->Set->urc_name, 'records' ) ){ ?>
							<li class="<?php active($page,'configurations'); ?>"><a href="<?php echo site_url('c=configurations'); ?>"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Configurations</a></li>
							<li class="<?php active($page,$settings); ?>"><a href="<?php echo site_url('c='.$settings); ?>"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span> Settings</a></li>
							<?php } ?>
							<?php if( $this->usersrole->check( $this->BU->urc_name, 'records' ) ){ ?>
							<li role="separator" class="divider"></li>
							<li class="<?php active($page,$backup); ?>"><a href="<?php echo site_url('c='.$backup); ?>"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> BackUp</a></li>
							<?php } ?> 
							<!--li class="<?php active($page,$region); ?>"><a href="<?php echo site_url('c='.$region); ?>"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>Region</a></li>
							<li class="<?php active($page,$area); ?>"><a href="<?php echo site_url('c='.$area); ?>"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>Area</a></li>
							<li class="<?php active($page,$position_approved); ?>"><a href="<?php echo site_url('c='.$position_approved); ?>"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>Position Approved</a></li--> 
						</ul>  <!-- end of .dropdown-menu -->
					</li>
					<li id="users-menu" class="dropdown <?php active($page,array($this->User->page,$this->UR->page,$this->UL->page)); ?>">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" >Users <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<?php if( $this->usersrole->check( $this->User->urc_name, 'records' ) ){ ?>
							<li id="users-sub" class="<?php active($page,$this->User->page); ?>"><a style="float: left; width: 100%;" href="<?php echo site_url('c='.$this->User->page); ?>"><span style="float: left; "
							> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Users</span><span class="badge" style="float: right;"><?php echo $user_total; ?></span></a></li>
								<?php if( $this->usersrole->check( $this->User->urc_name, 'add' ) ){ ?>
								<li><a href="<?php echo site_url('c='.$this->User->page.'&m=edit'); ?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New User</a></li>
								<?php } ?>
							<?php } ?>
							<?php if( $this->usersrole->check( $this->UR->urc_name, 'records' ) ){ ?>
							<li role="separator" class="divider"></li>
							<li class="<?php active($page,$this->UR->page); ?>"><a href="<?php echo site_url('c=UserRole'); ?>"><span class="glyphicon glyphicon-education" aria-hidden="true"></span> User Role</a></li>
								<?php if( $this->usersrole->check( $this->UR->urc_name, 'add' ) ){ ?>
								<li><a href="<?php echo site_url('c='.$this->UR->page.'&m=edit'); ?>"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Role</a></li>
								<?php } ?>
							<?php } ?>
							<?php if( $this->usersrole->check( $this->UL->urc_name, 'records' ) ){ ?>
							<li role="separator" class="divider"></li>
							<li class="<?php active($page,$this->UL->page); ?>"><a style="float: left; width: 100%" href="<?php echo site_url('c='.$this->UL->page); ?>"><span style="float: left"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> User Logs</span><span style="float: right" class="badge"><?php echo $user_logs_total; ?></span></a></li>
							<?php } ?>
						</ul> <!-- end of .dropdown-menu -->
					</li>
					<li class="<?php active($page,'profile'); ?> dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome <?php echo ucwords($logged_firstname); ?>! <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><?php echo $header_menu_logged_user_picture; ?></li>
							<li style="padding: 10px 0 0px 20px;"> <span class="glyphicon glyphicon-education" aria-hidden="true"></span> Role: <?php echo $logged_user_position_title; ?><li>
							<!-- removed style use class -->
							<li role="separator" class="divider"></li>
							<li id="profile-menu" class="<?php active($page,$this->User->page); ?>"><a href="<?php echo site_url('c='.$this->User->page.'&m=profile'); ?>"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> My Profile</a></li>
							<li class="<?php active($page,$this->UL->page); ?>"><a href="<?php echo site_url('c='.$this->UL->page.'&m=my_logs'); ?>"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> My Logs</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="<?php echo site_url('c='.$this->BLog->page.'&m=logout'); ?>"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Logout</a></li>
						</ul> <!-- end of .dropdown-menu -->
					</li>
				</ul> <!-- end of .nav navbar-nav navbar-right -->
			</div> <!-- end of .navbar-collapse collapse -->
		</div> <!-- end of .container-fluid -->
	</nav> <!-- end of .navbar navbar-inverse navbar-fixed-top -->
</header><!-- end of .container-fluid -->
<!-- end: header --> 
 

 