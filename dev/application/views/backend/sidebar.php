<ul class="list-group">
	<?php
	if( $this->usersrole->check( $this->User->urc_name, 'view' ) ){
	?>
		<li class="list-group-item"><h4 class="list-group-item-heading">User Menu</h4></li>
	<?php 
	}
	if( $this->usersrole->check( $this->User->urc_name, 'view' ) ){
		?> 
		<a href="<?php echo site_url('c='.$this->User->page); ?>" class="list-group-item default">
		<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Users</strong>
			<p class="list-group-item-text"><!--List of User Records--></p>
		</a>
		<?php  
	}
	if( $this->usersrole->check( $this->UR->urc_name, 'view' ) ){
		?> 
		<a href="<?php echo site_url('c='.$this->UR->page); ?>" class="list-group-item default">
			<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
			<strong class="list-group-item-heading">User Roles</strong>
			<p class="list-group-item-text"><!-- User Role Records --></p>
		</a>
	<?php }
	if( $this->usersrole->check( $this->UL->urc_name, 'view' ) ){
		?> 
		<a href="<?php echo site_url('c='.$this->UL->page); ?>" class="list-group-item default">
			<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Logs</strong>
			<p class="list-group-item-text"><!-- User Role Records --></p>
		</a>
	<?php 
	} 
	?>
</ul>
 
<ul class="list-group">
	<?php
	if( $this->usersrole->check( $this->Set->urc_name, 'view' )  || $this->usersrole->check( $this->BU->urc_name, 'view' )){
	?>
		<li class="list-group-item"><h4 class="list-group-item-heading">Manager Menu</h4></li>
	<?php 
	} 
	if( $this->usersrole->check( $this->Set->urc_name, 'view' ) ){
		?> 
		<a href="<?php echo site_url('c='.$this->Set->page); ?>" class="list-group-item default">
		<span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Settings</strong>
			<p class="list-group-item-text"><!--List of User Records--></p>
		</a>
		<?php  
	}
	if( $this->usersrole->check( $this->BU->urc_name, 'view' ) ){
		?> 
		<a href="<?php echo site_url('c='.$this->BU->page); ?>" class="list-group-item default">
			<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Backup</strong>
			<p class="list-group-item-text"><!-- User Role Records --></p>
		</a>
	<?php } ?>
</ul>