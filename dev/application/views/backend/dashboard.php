 
<div class="container-fluid">
	<h1 class="title">Dashboard</h1>
	<div class="row">
		<aside class="col-md-2"> 
			<?php echo $menu_sidebar_left; ?>
		</aside>
		<div class="col-md-10">
			<?php $this->notify->show(); ?> 
			<?php echo $dashboard_sections_standard; ?>
		</div>		 
	</div>
</div> 
