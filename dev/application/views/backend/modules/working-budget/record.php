<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
$import_bcs = $this->usersrole->get_capability($capabilities, $this->WB->urc_name, 'import'); 
$form_action_bcs_csv = site_url('c='.$this->WBCSV->page.'&m=upload');
?>
<div class="container-fluid">
	<div class="row">
		<!--aside class="col-sm-2">
			<!--?php $this->load->view($location.'/modules/barangay/sidebar'); ?>  
		</aside-->
		<div class="col-sm-12"> 
			<?php $this->recordsfilter->show(); ?>
			<hr />
			<div class="row">
				<div class="col-sm-6">
					<h1 class="title"><?php echo $model->name; ?> Records</h1> 
				</div> 
				<div class="col-sm-6">  
					<div class="row">
						<div class="col-sm-12 text-right">
							<?php btn_records( $page, array('capabilities'=>$capabilities,'ur_name'=>$this->WB->urc_name,'display'=>array('trash')) ); 
							get_custom_button( $page, array('capabilities'=>$capabilities,'ur_name'=>$this->CE->urc_name,'display'=>array('export','import'),'import_name'=>'View Uploaded BCS Files', 'export'=>'modal') ); 
							?> 
						</div>  
					</div>   
				</div>
			</div>  
			<?php
			if( $import_bcs ){
				?>
				<hr />
				<?php echo form_open_multipart($form_action_bcs_csv, array('class'=>'form-horizontal margin-top-md','id' =>$page.'_form','name' =>$page.'_form')); ?>
					<div class="form-group">
						<label class="col-md-8 col-sm-6 col-xs-12 text-right"> 
							Upload CSV file here:
						</label>  
						<div class="col-md-2 col-sm-3 col-xs-6">
							 <input name="files[]" multiple="multiple" accept="file_extension|audio/*|video/*|image/*|media_type" type="file">
						</div>
						<div class="col-md-2 col-sm-3 col-xs-6 text-right">
							<button type="submit" class="btn btn-success btn-sm js-required-fields">
							<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Upload
							</button>
						</div>
					</div>
				</form>
				<?php
			}
			?>
			<hr />			
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th> 
								<th>BCS No</th>
								<th>CE No</th>     
								<th width="15%">Client</th>  
								<th>Cost Estimate</th>  
								<th>ASF</th>  
								<th>BCS</th>      
								<th>Total Savings</th>      
								<th>Balance</th>      
								<th>Option</th> 
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php  
						if( $record_rows ){   
							$n=1;
							$total_cost_estimate=0; 
							$total_asf=0; 
							$total_bcs=0; 
							$total_savings=0; 
							$total_balance=0; 
							foreach( $record_rows as $col ){ 
								$id = get_value($col, $this->WB->tblid );
								$ce_id = get_value($col, $this->CE->tblid );
								$ce_number = get_value($col, $this->CE->tblpref.'number');
								$number = get_value($col, $this->WB->tblpref.'number');
								$client_name = get_value($col, $this->CE->tblpref.'client_name');
								$cost_estimate = get_value($col, $this->WB->tblpref.'cost_estimate');
								$asf = get_value($col, $this->WB->tblpref.'asf');
								$savings_total = get_value($col, $this->WB->tblpref.'savings_total'); 
								$bcs = get_value($col, $this->WB->tblpref.'bcs'); 
								$region = get_value( $col, $this->Reg->tblpref.'title');
								$cash_advance_limit = get_value($col, $this->WB->tblpref.'bcs');					
								$balance_ca_amount = $this->CA->get_current_ca_balance($id, $cash_advance_limit);
								$balance = $this->WB->get_total_balance_from_wb($id );
								 
								// $balance = $balance_ca_amount;		

								$total_cost_estimate+=$cost_estimate; 
								$total_asf+=$asf; 
								$total_bcs+=$bcs; 
								$total_savings+=$savings_total; 
								$total_balance+=$balance;  	
								?>
								<tr> 
									<td><?php echo $n; ?></td> 
									<td><?php echo $number; ?></td> 
									<td><?php $this->CE->get_data_link( $ce_id, $ce_number); ?></td>    
									<td><?php echo ucwords($client_name); ?></td>  
									<td align="right"><?php echo number_format($cost_estimate,2,'.',',');?></td>
									<td align="right"><?php echo number_format( $asf,2,'.',',');?></td>
									<td align="right"><?php echo number_format( $bcs,2,'.',',');?></td>
									<td align="right"><?php echo number_format( $savings_total,2,'.',',');?></td>
									<td align="right"><?php echo number_format( $balance,2,'.',',');?></td>
									<td class="text-right">
									<?php	 
									if( $cash_advance_limit > 0  ){
										if( $balance > 0 ){
											?>
											<a href="<?php echo site_url('c='.$this->CA->page.'&m=edit&wb_id='.$id); ?>" class="btn btn-success btn-xs"  target="_blank">Add New CA</a>
											<?php
										}
									}
									btn_optn_records($page, array(
											'model'=>$model,'fields'=>$col,
											'capabilities'=>$capabilities,
											'id'=>$id,
											'display'=>array('view','trash')
										));									
									?> 
									</td>  
								</tr>
								<?php
								$n++;
							}
							?>
							<tr> 	
								<td colspan="4" align="right"><strong>Total:</strong></td>
								<td align="right"><strong><?php echo number_format($total_cost_estimate,2,'.',',');?></strong></td>  
								<td align="right"><strong><?php echo number_format($total_asf,2,'.',',');?></strong></td>  
								<td align="right"><strong><?php echo number_format($total_bcs,2,'.',',');?></strong></td>  
								<td align="right"><strong><?php echo number_format($total_savings,2,'.',',');?></strong></td>  
								<td align="right"><strong><?php echo number_format($total_balance,2,'.',',');?></strong></td>  
							</tr>
							<?php
						} 
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
			<?php $this->load->view($location.'/modules/records-footer-pagination'); ?> 
			</form> <!-- end of form -->
		</div> <!-- end of .col-bm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->
<div class="modal fade" id="export_filter_modal" tabindex="-1" role="dialog" aria-labelledby="modal_export_filter">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Export Filter</h3>
			</div>
			<form method="POST" id="export_filter_modal_form"  class ="form-horizontal" action="">
				<div class="modal-body">  
					<div class="form-group margin-bottom-md">
						<label class="col-sm-2">Year:</label>
						<div class="col-sm-4">
							<?php echo app_select_year( null ); ?>
						</div>
						<label class="col-sm-2">Quarter:</label>
						<div class="col-sm-4"> 
							<select class="form-control" name="quarter">
								<option>---Select---</option>
								<option value="q1">Quarter 1</option>
								<option value="q2">Quarter 2</option>
								<option value="q3">Quarter 3</option>
								<option value="q4">Quarter 4</option>
							</select>
						</div> 
					</div> 
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-sm btn-info">Export</button>
				</div>
			</form>
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .modal -->
<script type="text/javascript">
$('.display-filter-modal').click(function() { 
	var action = $(this).attr('data-action'); 
    $('#export_filter_modal').modal('toggle'); 
	var action =$('#export_filter_modal_form').attr('action', action );
});
$('#export_filter_modal_form').submit(function() { 
    $('#export_filter_modal').modal('hide'); 
});
</script>