<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
$import_ce = $this->usersrole->get_capability($capabilities, $this->WB->urc_name, 'import'); 
$form_action_bcs_csv = site_url('c='.$this->WBCSV->page.'&m=upload');
?>
<div class="row margin-bottom-xs">
	<div class="col-sm-12 text-right">
		<?php
		if( $import_ce ){
			?>
			<?php echo form_open_multipart($form_action_bcs_csv, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-offset-6 col-sm-3">
						<input name="files[]" multiple="multiple" accept="file_extension|audio/*|video/*|image/*|media_type" type="file">
					</div>
					<div class="col-sm-3">
						<button type="submit" class="btn btn-success btn-sm js-required-fields">
						<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Upload BCS .CSV File
						</button>
					</div>
				</div>
			</form>
			<?php
		}
		?>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="table-responsive">
			<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
				<thead>
					<tr>
						<th>#</th> 
						<th>BCS No</th>
						<th>CE No</th>     
						<th>Client</th>  
						<th>Cost Estimate</th>  
						<th>BCS</th>      
						<th>Total Savings</th>      
						<th>Option</th> 
					</tr>
				</thead> <!-- end of thead -->
				<tbody>
				<?php
				if( $records ){
					$total_ce_amount = 0;
					$total_ca_limit = 0;
					$n=1;
					foreach( $records as $col ){
						$id = get_value($col, $this->WB->tblid );
						$ce_id = get_value($col, $this->CE->tblid );
						$ce_number = get_value($col, $this->CE->tblpref.'number');
						$number = get_value($col, $this->WB->tblpref.'number');
						$client_name = get_value($col, $this->CE->tblpref.'client_name');
						$cost_estimate = get_value($col, $this->WB->tblpref.'cost_estimate');
						$asf = get_value($col, $this->WB->tblpref.'asf');
						$savings_total = get_value($col, $this->WB->tblpref.'savings_total'); 
						$cash_advance_limit = get_value($col, $this->WB->tblpref.'bcs');
						$bcs = get_value($col, $this->WB->tblpref.'bcs'); 
						$region = get_value( $col, $this->Reg->tblpref.'title');  
						$balance_ca_amount = $this->CA->get_current_ca_balance($id, $cash_advance_limit);
						$balance = $balance_ca_amount;
						?>
						<tr> 
							<td><?php echo $n; ?></td>  
							<td><?php $this->WB->get_data_link( $id, $number); ?></td>   
							<td><?php $this->CE->get_data_link( $ce_id, $ce_number); ?></td>   
							<td><?php echo ucwords($client_name); ?></td>  
							<td align='right'><?php echo number_format($cost_estimate,2,'.',',');?></td> 
							<td align='right'><?php echo number_format( $bcs,2,'.',',');?></td>
							<td align='right'><?php echo number_format( $savings_total,2,'.',',');?></td>
							<td class="text-right">
							<?php	 
							btn_optn_records($this->WB->page, array(
								'capabilities'=>$capabilities,
								'ur_name'=>$this->WB->urc_name,
								'id'=>$id,
								'display'=>array('view','edit','delete','trash')
							));
							if( $cash_advance_limit > 0  ){
								if( $balance > 0 ){
									?>
									<a href="<?php echo site_url('c='.$this->CA->page.'&m=edit&wb_id='.$id); ?>" class="btn btn-success btn-xs"  target="_blank">Add New CA</a>
									<?php
								}
							}
							?> 
							</td>  
						</tr>
						<?php
						$n++;
					}
				}
				
				?>
				</tbody>
			</table>
			<?php $this->notify->records( $records ); ?>
		</div>
	</div>
</div>