<?php
if( $this->usersrole->check( $this->Trans->urc_name, 'records' ) && $display_latest_transactions ){
?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel-group basic_reg_form">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="<?php echo site_url('c='.$this->WB->page); ?>">Latest <?php echo $this->WB->name; ?></a>
				</div>
				<div class="panel-body"> 
				<?php $this->load->view($location.'/modules/'.$this->WB->view.'/grid'); ?>
				</div>
			</div>
		</div>
	</div> 
</div>  
<?php 
}
?>