<?php
//$current_value_access = $this->usersrole->get_capability($selected_capabilities,'access','full');
?>
<div class="container-fluid">
	<div class="row">    
		<!--aside class="col-sm-2">
			<!--?php $this->load->view($location.'/leftmenu'); ?>
			<!?php $this->load->view($location.'/modules/barangay/sidebar'); ?> 	
		</aside-->
		<div class="col-sm-12"> 
			<?php echo form_open_multipart($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form','enctype'=>'multipart/form-data')); ?> 
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6"> 
						<h1 class="title"><?php echo $this->WB->name; ?> Edit</h1>
					</div>
					<div class="col-sm-6 text-right"> 
						<?php btn_edit( $page, array('capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$this->WB->urc_name) ); ?> 
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (<span class="red">*</span>) are required fields and must not be blank.</h6>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">CE Number <span class="red">*</span>:</label>
					<div class="col-sm-4"> 
						<?php echo $select_ce; ?>
					</div>
					<label class="col-sm-2">Area <span class="red">*</span>:</label>
					<div class="col-sm-4"> 
						<?php 
							if($id){ 
								$area_select = $this->Area->get_select( $this->WB, $fields );  
								echo $area_select;
							}
							else{
								echo 'Choose CE # first and save.';
							}
						?>
					</div>
				</div> 
				<div class="form-group">
					<label class="col-sm-2">Item <span class="red">*</span>:</label>
					<div class="col-sm-4"> 
						<?php $this->moduletag->s(array('name'=>'item','holder'=>'Item','req'=>TRUE)); ?>
					</div>
					<label class="col-sm-2">CE Amount per Item <span class="red">*</span>:</label>
					<div class="col-sm-4"> 
						<?php $this->moduletag->s(array('name'=>'amount_per_item','holder'=>'Amount per Item','req'=>TRUE)); ?>
					</div>
				</div> 
				<div class="form-group">
					<label class="col-sm-2">Activity Name:</label>
					<div class="col-sm-4"> 
						<?php echo get_value($fields, $this->CE->tblpref.'activity'); ?>
					</div>
					<label class="col-sm-2">Month of Activity:</label>
					<div class="col-sm-4"> 
						<?php 
							$date_activity = get_value( $fields, $this->CE->tblpref.'date', current_date()); 
							echo datetime($date_activity,'F\-y');
						?>
					</div>
				</div>  
				<div class="form-group">
					<label class="col-sm-2">CA Limit:</label>
					<div class="col-sm-4"> 
						<?php $this->moduletag->s(array('name'=>'ca_limit',
						'holder'=>'CA Limit')); ?>
					</div>
					<label class="col-sm-2">Balance:</label>
					<div class="col-sm-4"> 
						 <?php echo $ca_balance; ?>
					</div>
				</div> 
				<hr />
				<?php $this->load->view($location.'/edit-form-footer-fields'); ?>
				<?php echo form_hidden('id',$id); ?>
			</form>
			<hr />
			<?php echo $sub_section_1; ?>
		</div>
	</div>
</div>