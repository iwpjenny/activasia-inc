<?php
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
?>
<?php
if(isset($selected_capabilities)){
	$current_value_access = $this->usersrole->get_capability($selected_capabilities,'access','full'); 
}
?>
<div class="container-fluid">
	<div class="row">
		<!--aside class="col-sm-2">
			<!--?php $this->load->view($location.'/modules/barangay/sidebar'); ?> 
		</aside-->
		<div class="col-sm-12"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-6">
						<h1><?php echo $this->WB->name; ?> Trashed Records</h1>
					</div> 
					<div class="col-sm-6 text-right">
						<?php btn_records( $page, array('capabilities'=>$capabilities,'ur_name'=>$this->WB->urc_name,'display'=>array('records')) ); ?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th> 
								<th>WB No</th>  
								<th>CE No</th>   
								<th>Client</th>  
								<th>Cost Estimate</th>     
								<th>BCS</th>      
								<th>Option</th> 
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						$n=1;
						if( $record_rows ){  
							foreach( $record_rows as $col ){
								$id = get_value($col, $this->WB->tblid );
								$ce_id = get_value($col, $this->CE->tblid );
								$ce_number = get_value($col, $this->CE->tblpref.'number');
								$client_name = get_value($col, $this->CE->tblpref.'client_name');
								$number = get_value($col, $this->WB->tblpref.'number');
								$cost_estimate = get_value($col, $this->WB->tblpref.'cost_estimate');
								$asf = get_value($col, $this->WB->tblpref.'asf');
								$bcs = get_value($col, $this->WB->tblpref.'bcs'); 
								$region = get_value( $col, $this->Reg->tblpref.'title');  
								?>
								<tr>
									<td><?php echo $n; ?></td> 
									<td ><?php echo $number;?></td>
									<td>
										<a href="<?php echo site_url('c='.$this->CE->page.'&m=view&id='.$ce_id); ?>" target="_blank"><?php echo $ce_number; ?></a> 
									</td>   
									<td><?php echo ucwords($client_name); ?></td>  
									<td align='right'><?php echo number_format($cost_estimate,2,'.',',');?></td> 
									<td align='right'><?php echo number_format( $bcs,2,'.',',');?></td>
									<td class="text-right">
									<?php	 
									btn_optn_records($page, array(
										'model'=>$model,'fields'=>$col,
										'capabilities'=>$capabilities,
										'id'=>$id,
										'display'=>array('view','untrash')
									));
									
									?>
									</td>  
								</tr>
								<?php
								$n++;
							}
						}
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
				<nav>
					<?php echo $pagination; ?>
					<!--div class="showing-result"><?php app_pagination_details($per_page, $n); ?></div-->
				</nav>
			</form> <!-- end of form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->