<hr />
<?php echo form_open($form_action,array('class'=>'form-horizontal','name'=>'grid_'.$page)); ?>
	<div class="form-group"> 
		<label class="col-sm-3"><h4><?php echo $name; ?> Records</h4></label>
		<div class="col-sm-6 notification_<?php echo $page; ?>"></div> 
		<div class="col-sm-3 text-right"> 
			<?php
			if( $views_editable === TRUE ){
			?>
			<button type="button" class="btn btn-success btn-sm" onclick="javascript:json_new_record(this);">Add New Sub Record</button>
			<?php
			}
			?>
		</div> 
	</div>
	<div class="table-responsive">
		<table id="grid_<?php echo $page; ?>" border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
			<thead>
			<?php $this->load->view($location.'/modules/'.$model->view.'/grid-thead'); ?>
			</thead>
			<tbody>
			<?php $this->load->view($location.'/modules/'.$model->view.'/grid-tbody'); ?>
			</tbody>
		</table>
	</div>
	<?php echo form_hidden('id',''); ?>
	<?php echo form_hidden('title',''); ?>
	<?php echo form_hidden('parent_id',$parent_id); ?>
	<?php echo form_hidden('page',$page); ?>
</form>
<!-- Start Modal <?php echo $page; ?> -->
<?php
if( $views_editable === TRUE ){
?>
<div class="modal fade" id="modal_<?php echo $page; ?>" tabindex="-1" role="dialog" aria-labelledby="modal_<?php echo $page; ?>">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title"><?php echo $name; ?> Record Details</h3>
			</div>
			<div class="modal-body">
				<?php echo form_open_multipart(site_url('c='.$page),array('class'=>'form-horizontal','name'=>'field_'.$page)); ?>
					<?php $this->load->view($location.'/modules/'.$model->view.'/fields'); ?>
					<?php $this->load->view($location.'/edit-form-footer-fields'); ?>
					<?php echo form_hidden('id',$id); ?>
					<?php echo form_hidden('parent_id',$parent_id); ?>
					<?php echo form_hidden('page',$page); ?>
					<?php echo form_hidden('redirect_url',url_current()); ?>
					<?php echo form_hidden($tblname.'['.$model_parent->tblid.']',$parent_id); ?>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="javascript:json_save_record(this);">Save Data</button>
			</div>
		</div>
	</div>
</div><!-- .modal -->

<div class="modal fade" id="modal_delete_<?php echo $page; ?>" tabindex="-1" role="dialog" aria-labelledby="label_delete_<?php echo $page; ?>">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="label_delete_<?php echo $name; ?>">Are you sure you want to delete this record?</h4>
			</div>
			<div class="modal-body">
				<p>Record ID: <strong id="delete-id"></strong></p>
				<p>Title: <strong id="delete-title"></strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger btn-sm" onclick="javascript:json_delete_confirmed_record(this);">Delete Now</button>
			</div>
			<?php echo form_hidden('page',$page); ?>
		</div>
	</div>
</div><!-- .modal -->

<div class="modal fade" id="modal_trash_<?php echo $page; ?>" tabindex="-1" role="dialog" aria-labelledby="label_trash_<?php echo $page; ?>">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="label_trash_<?php echo $name; ?>">Are you sure you want to trash this record?</h4>
			</div>
			<div class="modal-body">
				<p>Record ID: <strong id="delete-id"></strong></p>
				<p>Title: <strong id="delete-title"></strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-warning btn-sm" onclick="javascript:json_trash_confirmed_record(this);">Trash Now</button>
			</div>
			<?php echo form_hidden('page',$page); ?>
		</div>
	</div>
</div><!-- .modal -->

<script type="text/javascript">
function json_new_record( obj ){
	var parent_form_obj = $(obj).parents("form");	
	var page = $("input[name='page']",parent_form_obj).val();
	
	var form_obj = $("form[name='field_"+page+"']");	
	$("input[name='id']",form_obj).val("");
	$("input.fields",form_obj).val("");
	
	if( typeof json_new_record_custom == "function" ){
		json_new_record_custom( page );
	}
		
	$("div#modal_"+page+"").modal();
}

function json_save_record( obj ){
	var modal_obj = $(obj).parents("div.modal");	
	var form_obj = $("form",modal_obj);
	var page = $("input[name='page']",form_obj).val();
	
	var url = form_obj.attr("action");
	url = url+"&m=json_save";
	var data = form_obj.serialize();

	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: json_update_grid
	});
	
	$("div#modal_"+page+"").modal('hide');
}

function json_update_grid( data ){	  
	json_before_update_grid_custom( data );
 
	if( data.result == true ){
		$("table#grid_"+data.page+" tbody").html(data.tbody);
	}
	$("div.notification_"+data.page).html(data.notification);
}

function json_edit_record( obj, id ){
	var form_obj_grid = $(obj).parents("form");	
	var page = $("input[name='page']",form_obj_grid).val();
	
	var form_obj = $("form[name='field_"+page+"']");
	$("input[name='id']",form_obj).val( id );
	var url = form_obj.attr("action");
	url = url+"&m=json_display_record";
	var data = form_obj.serialize();
 	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: json_display_field_data
	});
	
	$("div#modal_"+page+"").modal();
}

function json_display_field_data( data ){
	
	var form_obj = $("form[name='field_"+data.page+"']");
	var fields = data.field;
	
	$.each(fields, function(key, value){
		if( fields.hasOwnProperty(key) ){
			$("#"+key,form_obj).val( value );
		}
	});
	
	if( typeof json_display_field_data_custom == "function" ){
		json_display_field_data_custom( data );
	}
}

function json_delete_record( obj, id ){
	var modal_obj = $(obj).parents("div.modal");	
	var page = $("input[name='page']",modal_obj).val();
	
	var form_obj = $("form[name='field_"+page+"']");
	var parent_id = $("input[name='parent_id']",form_obj).val();
	var url = form_obj.attr("action");
	url = url+"&m=json_delete";
	
	var data = {id:id,parent_id:parent_id,page:page};
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: json_update_grid
	});
	
	$("input[name='id']",form_obj).val("");
	$(modal_obj).modal('hide');
}

function json_delete_confirmation_record( obj, id, title ){
	var form_obj = $(obj).parents("form");	
	var page = $("input[name='page']",form_obj).val();
	
	$("input[name='id']",form_obj).val(id);
	$("strong#delete-id").html(id);
	$("strong#delete-title").html(title);		
	$("div#modal_delete_"+page).modal();
}

function json_delete_confirmed_record( obj ){
	var modal_obj = $(obj).parents("div.modal");
	var page = $("input[name='page']",modal_obj).val();
	
	var form_obj1 = $("form[name='field_"+page+"']");
	var url = form_obj1.attr("action");
	console.log(url);
	url = url+"&m=json_delete";
	
	var form_obj2 = $("form[name='grid_"+page+"']");
	var id = $("input[name='id']",form_obj2).val();
	var parent_id = $("input[name='parent_id']",form_obj2).val();
	
	var data = {id:id,parent_id:parent_id,page:page};
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: json_update_grid
	});
	
	$("input[name='id']",form_obj2).val("");
	$(modal_obj).modal('hide');
}

function json_trash_confirmation_record( obj, id, title ){
	var form_obj = $(obj).parents("form");	
	var page = $("input[name='page']",form_obj).val();
	
	$("input[name='id']",form_obj).val(id);
	$("strong#delete-id").html(id);
	$("strong#delete-title").html(title);		
	$("div#modal_trash_"+page).modal();
}

function json_trash_confirmed_record( obj ){
	var modal_obj = $(obj).parents("div.modal");
	var page = $("input[name='page']",modal_obj).val();
	
	var form_obj1 = $("form[name='field_"+page+"']");
	var url = form_obj1.attr("action");
	console.log(url);
	url = url+"&m=json_trash";
	
	var form_obj2 = $("form[name='grid_"+page+"']");
	var id = $("input[name='id']",form_obj2).val();
	var parent_id = $("input[name='parent_id']",form_obj2).val();
	
	var data = {id:id,parent_id:parent_id,page:page};
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: json_update_grid
	});
	
	$("input[name='id']",form_obj2).val("");
	$(modal_obj).modal('hide');
}

function json_trash_record( page, id ){
	var form_obj = $("form[name='field_"+page+"']");
	var parent_id = $("input[name='parent_id']",form_obj).val();
	var url = form_obj.attr("action");
	url = url+"&m=json_trash";
	
	var data = {id:id,parent_id:parent_id,page:page};
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: json_update_grid
	});
}

function untrash_record( page, id ){
	var form_obj = $("form[name='field_"+page+"']");
	var parent_id = $("input[name='parent_id']",form_obj).val();
	var url = form_obj.attr("action");
	url = url+"&m=json_untrash";
	
	var data = {id:id,parent_id:parent_id,page:page};
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: json_update_grid
	});
}
</script>
<?php
}
?>
<!-- End Modal <?php echo $page; ?> -->