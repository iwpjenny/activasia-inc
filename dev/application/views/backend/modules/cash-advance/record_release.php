<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_date_format = app_get_val($settings['date_and_time'],'grid_date_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit');  
$show_status_approve_button = app_get_val($show_status,'show_status_approve_button');  
$show_status_decline_button = app_get_val($show_status,'show_status_decline_button');  
$show_status_pending_button = app_get_val($show_status,'show_status_pending_button');  
$show_status_release_button = app_get_val($show_status,'show_status_release_button');  
?>
<div class="container-fluid">
	<div class="row">
		<!--aside class="col-sm-2">
			<!--?php $this->load->view($location.'/modules/barangay/sidebar'); ?-->  
		<!--/aside!-->
		<div class="col-sm-12"> 
			<?php $this->recordsfilter->show(); ?>
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-6">
						<h1><?php echo $model->name; ?> to be Released Records</h1>
					</div> 
					<div class="col-sm-6 text-right">
						<?php btn_records( $page, array('capabilities'=>$capabilities,'ur_name'=>$model->urc_name,'display'=>array('add','trash')) ); ?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th> 
								<th>Requested</th>
								<th>CA #</th>
								<th>Employee</th> 
								<th width="20%">Project Name</th> 
								<th>CA Amount</th>
								<th width="5%">Date Release</th>
								<th width="5%">Status</th>
								<?php 
								if( $show_status_approve_button || $show_status_decline_button || $show_status_pending_button || $show_status_release_button  ){
									echo '<th width="10%">Action</th>';
								}
								?>
								<th>Option</th> 
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						if( $record_rows ){ 
							$n=1;
							$total_ca_amount = 0;
							foreach( $record_rows as $col ){ 
								$id = get_value($col,$this->CA->tblpref.'id'); 
								$date_requested = get_value($col, $this->CA->tblpref.'date_requested');
								$ca_number = get_value($col, $this->CA->tblpref.'ca_number'); 
								$user_created = get_value($col, $this->CA->tblpref.'created_by_user_id');
								$fullname = $this->moduser->get_user_fullname_by_id( $user_created, '{lastname}, {firstname}'); 
								$area_id = get_value($col, $this->Area->tblid);
								$area_title = get_value($col, $this->Area->tblpref.'title');  
								// $activity = get_value($col, $this->CE->tblpref.'activity');
								$project_name = get_value($col, $this->CE->tblpref.'project_name');
								// $ca_amount = get_value($col, $this->CA->tblpref.'ca_amount');  
								$wb_id = get_value( $col, $this->WB->tblid );  
								$ca_amount = $this->WB->get_total_amount($wb_id, $id ) ; 
								$date_released = get_value($col, $this->CA->tblpref.'date_released');
								$created = get_value($col, $this->CA->tblpref.'created');  
								$status = get_value($col, $this->CA->tblpref.'status');
								$item = get_value( $col, $this->WB->tblpref.'item' ); 
								$activity_display = character_limiter($project_name,$grid_text_limit); 
								$created_display = datetime($created,$grid_date_time_format); 
								$date_requested_display = datetime($date_requested,$grid_date_format); 
								if( $date_released <> '0000-00-00' ){
									$date_released_display = datetime($date_released,$grid_date_format);
								} else {
									$date_released_display = 'Unreleased';
								}
								
								$approve_only_position = TRUE;
								$can_approve_all = TRUE;
								$can_approve_self = TRUE;
								$total_ca_amount += $ca_amount;
								?>
								<tr>
									<td><?php echo $n; ?></td> 
									<td ><?php echo $date_requested; ?></td> 
									<td ><?php $this->CA->get_data_link( $id, $ca_number); ?></td> 
									<td >
										<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$user_created); ?>" target="_blank"><?php echo ucwords($fullname); ?></a> 
									</td>      
									<td><?php echo $activity_display; ?></td> 	  	
									<td align="right"><?php echo number_format($ca_amount,2,'.',','); ?></td> 	
									<td><?php echo $date_released_display; ?></td>
									<td><?php echo get_ca_status_button( $status ); ?></td>
									<?php 
									if( $show_status_approve_button || $show_status_decline_button || $show_status_pending_button || $show_status_release_button  ){
										 echo '<td class="text-right">';	
											option_status( $status, $page, $id,  $show_status ); 
										 echo '</td>';	 
									}
									?> 
									<td class="text-right">
									<?php	 
									btn_optn_records($page, array(
										'capabilities'=>$capabilities,
										'ur_name'=>$this->CA->urc_name,
										'id'=>$id,
										'display'=>array('view','edit','delete','trash')
									));
									?>
									</td>  
								</tr>
								<?php
								$n++;
							}
							?>
							<tr>
								<td colspan="5" align='right'><b>Total:</b></td>
								<td align='right'><b><?php echo number_format($total_ca_amount,2,'.',',');?></b></td>   
							</tr>
							<?php
						} 
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
			<?php $this->load->view($location.'/modules/records-footer-pagination'); ?> 
			</form> <!-- end of form -->
		</div> <!-- end of .col-bm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->