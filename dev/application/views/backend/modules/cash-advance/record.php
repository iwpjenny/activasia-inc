<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_date_format = app_get_val($settings['date_and_time'],'grid_date_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit');  
$show_status_approve_button = app_get_val($show_status,'show_status_approve_button');  
$show_status_decline_button = app_get_val($show_status,'show_status_decline_button');  
$show_status_pending_button = app_get_val($show_status,'show_status_pending_button');  
$show_status_release_button = app_get_val($show_status,'show_status_release_button');  
$show_status_ready_button = app_get_val($show_status,'show_status_ready_button');   
/* printx($show_status); */
?>
<div class="container-fluid">
	<div class="row">
		<!--aside class="col-sm-2">
			<!--?php $this->load->view($location.'/modules/barangay/sidebar'); ?-->  
		<!--/aside!-->
		<div class="col-sm-12"> 
			<?php $this->recordsfilter->show(); ?>
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-6">
						<h1><?php echo $model->name; ?> Records</h1>
					</div> 
					<div class="col-sm-6 text-right">
						<?php btn_records( $page, array('capabilities'=>$capabilities,'ur_name'=>$model->urc_name,'display'=>array('trash')) );
						get_custom_button( $page, array('capabilities'=>$capabilities,'ur_name'=>$this->CE->urc_name,'display'=>array('export'), 'export'=>'modal') ); 
						?> 
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th>  
								<th>CA #</th>
								<th>Requested</th>
								<th>Employee</th>
								<th>BCS No.</th>
								<th width="20%">Project Name</th> 
								<th align="right">CA Total</th>
								<th width="5%">Date Released</th>
								<th width="5%">Status</th>
								<?php 
								if( $show_status_approve_button || $show_status_decline_button || $show_status_pending_button || $show_status_release_button || $show_status_ready_button ){
									echo '<th width="10%">Action</th>';
								}
								?>
								<th>Option</th> 
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						if( $record_rows ){ 
							$n=1;
							$total_ca_amount = 0;
							foreach( $record_rows as $col ){ 
								$id = get_value($col,$this->CA->tblpref.'id'); 
								$date_requested = get_value($col, $this->CA->tblpref.'date_requested');
								$ca_number = get_value($col, $this->CA->tblpref.'ca_number'); 
								$user_created = get_value($col, $this->CA->tblpref.'created_by_user_id');
								$fullname = $this->moduser->get_user_fullname_by_id( $user_created, '{lastname}, {firstname}'); 
								// $area_id = get_value($col, $this->Area->tblid);
								// $area_title = get_value($col, $this->Area->tblpref.'title');  
								$project_name = get_value($col, $this->CE->tblpref.'project_name');
								$wb_id = get_value($col, $this->WB->tblpref.'id');
								$wb_no = get_value($col, $this->WB->tblpref.'number');
								// $ca_amount = $this->CAI->get_total_amount($id) ; 
								$wb_id = get_value( $col, $this->WB->tblid );  
								$ca_amount = $this->WB->get_total_amount($wb_id, $id ) ; 
								$date_released = get_value($col, $this->CA->tblpref.'date_released');
								$created = get_value($col, $this->CA->tblpref.'created');  
								$status = get_value($col, $this->CA->tblpref.'status');
								$item = get_value( $col, $this->WB->tblpref.'item' ); 
								$activity_display = character_limiter($project_name,$grid_text_limit); 
								$created_display = datetime($created,$grid_date_time_format); 
								$date_requested_display = datetime($date_requested,$grid_date_format); 
								if( $date_released <> '0000-00-00' ){
									$date_released_display = datetime($date_released,$grid_date_format);
								} else {
									$date_released_display = 'Unreleased';
								}
								$total_ca_amount += $ca_amount;
								$approve_only_position = TRUE;
								$can_approve_all = TRUE;
								$can_approve_self = TRUE;
								?>
								<tr>
									<td><?php echo $n; ?></td>  
									<td><?php echo $ca_number; ?></td>   
									<td><?php echo $date_requested; ?></td> 
									<td >
										<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$user_created); ?>" target="_blank"><?php echo ucwords($fullname); ?></a> 
									</td>      		
									<td><?php $this->WB->get_data_link( $wb_id, $wb_no); ?></td>   			 
									<td title="<?php echo $project_name; ?>"><?php echo $activity_display; ?></td>  
									<td align="right"><?php echo number_format($ca_amount,2,'.',','); ?></td> 	
									<td><?php echo $date_released_display; ?></td> 
									<td><?php echo get_ca_status_button( $status ); ?></td>
									<?php 
									if( $show_status_approve_button || $show_status_decline_button || $show_status_pending_button || $show_status_release_button || $show_status_ready_button ){ 
										 echo '<td class="text-right">';	
											option_status( $status, $page, $id,  $show_status ); 
										 echo '</td>';	 
									}
									?> 
									<td class="text-right">
									<?php 
									$display_cap = array('view','delete','trash');
									if($status != 'Released'){
										$display_cap[] = 'edit';
									}
									btn_optn_records($page, array(
										'model'=>$model,'fields'=>$col,
										'capabilities'=>$capabilities, 
										'capabilities'=>$capabilities,
										'ur_name'=>$this->CA->urc_name,
										'id'=>$id,
										'display'=>$display_cap
									));
									?>
									</td>  
								</tr>
								<?php
								$n++;
							}
							?>
							<tr>
								<td colspan="6" align="right"><b>Total:</b></td>
								<td align="right"><b><?php echo number_format($total_ca_amount,2,'.',',');?></b></td>   
							</tr>
							<?php
						} 
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
			<?php $this->load->view($location.'/modules/records-footer-pagination'); ?> 
			</form> <!-- end of form -->
		</div> <!-- end of .col-bm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->
<div class="modal fade" id="export_filter_modal" tabindex="-1" role="dialog" aria-labelledby="modal_export_filter">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Export Filter</h3>
			</div>
			<form method="POST" id="export_filter_modal_form"  class ="form-horizontal" action="">
				<div class="modal-body">  
					<div class="form-group margin-bottom-md">
						<label class="col-sm-2">Year:</label>
						<div class="col-sm-4">
							<?php echo app_select_year( null ); ?>
						</div>
						<label class="col-sm-2">Quarter:</label>
						<div class="col-sm-4"> 
							<select class="form-control" name="quarter">
								<option>---Select---</option>
								<option value="q1">Quarter 1</option>
								<option value="q2">Quarter 2</option>
								<option value="q3">Quarter 3</option>
								<option value="q4">Quarter 4</option>
							</select>
						</div> 
					</div> 
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-sm btn-info">Export</button>
				</div>
			</form>
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .modal -->
<script type="text/javascript">
$('.display-filter-modal').click(function() { 
	var action = $(this).attr('data-action'); 
    $('#export_filter_modal').modal('toggle'); 
	var action =$('#export_filter_modal_form').attr('action', action );
});
$('#export_filter_modal_form').submit(function() { 
    $('#export_filter_modal').modal('hide'); 
});
</script>