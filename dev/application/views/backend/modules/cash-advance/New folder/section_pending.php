<?php
if( $this->usersrole->check( $this->CA->urc_name, 'records' ) &&  
	$this->usersrole->check( $this->CA->urc_name, 'release' )  ){
?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel-group basic_reg_form">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="<?php echo site_url('c='.$this->CA->page); ?>">Latest <?php echo $this->CA->name; ?> to Approve</a>
				</div>
				<div class="panel-body"> 
				<?php $this->load->view($location.'/modules/'.$this->CA->view.'/grid_pending'); ?>
				</div>
			</div>
		</div>
	</div> 
</div>  
<?php 
}
?>