<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_date_format = app_get_val($settings['date_and_time'],'grid_date_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit');  
?> 
<div class="table-responsive">
	<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th>#</th> 
				<th>Requested</th>
				<th>CA #</th>
				<th>Employee</th> 
				<th>Project Name</th> 
				<th>CA Total</th>
				<th>Date Released</th> 
				<th width='10%'>Action</th> 	 
			</tr>
		</thead> <!-- end of thead -->
		<tbody>
		<?php
		if( $records ){
			$n=1;
			foreach( $records as $col ){ 
				$id = get_value($col,$this->CA->tblpref.'id'); 
				$date_requested = get_value($col, $this->CA->tblpref.'date_requested');
				$ca_number = get_value($col, $this->CA->tblpref.'ca_number'); 
				$user_created = get_value($col, $this->CA->tblpref.'created_by_user_id');
				$fullname = $this->moduser->get_user_fullname_by_id( $user_created, '{lastname}, {firstname}');  
				$project_name = get_value($col, $this->CE->tblpref.'project_name');
				// $ca_amount = $this->CAI->get_total_amount($id) ; 
				$wb_id = get_value( $col, $this->WB->tblid );  
				$ca_amount = $this->WB->get_total_amount($wb_id, $id ) ; 
				$date_released = get_value($col, $this->CA->tblpref.'date_released');
				$created = get_value($col, $this->CA->tblpref.'created');  
				$status = get_value($col, $this->CA->tblpref.'status'); 
				$activity_display = character_limiter($project_name,$grid_text_limit); 
				$created_display = datetime($created,$grid_date_time_format); 
				$date_requested_display = datetime($date_requested,$grid_date_format); 
				if( $date_released <> '0000-00-00' ){
					$date_released_display = datetime($date_released,$grid_date_format);
				} else {
					$date_released_display = 'Unreleased';
				}
				
				$approve_only_position = TRUE;
				$can_approve_all = TRUE;
				$can_approve_self = TRUE;
				?>
				<tr>
					<td><?php echo $n; ?></td> 
					<td ><?php echo $date_requested; ?></td>  
					<td ><?php $this->CA->get_data_link( $id, $ca_number); ?></td>        

					<td >
						<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$user_created); ?>" target="_blank"><?php echo ucwords($fullname); ?></a> 
					</td>     
					<td><?php echo $activity_display; ?></td> 	 
					<td><?php echo number_format( $ca_amount,2,'.',','); ?></td> 	
					<td><?php echo $date_released_display; ?></td> 
					<td class="text-right">
					<?php	 
						option_status( $status, $this->CA->page, $id,  $show_status );  
					?>
					</td>   
				</tr>
				<?php
				$n++;
			}
		} 
		else{
		?>
				<tr>
					<td colspan="8" class="text-center"><strong class="empty-record-table-label"><?php echo "No Cash Advance to release."; ?><strong></td> 
					 
				</tr>
		<?php
		}
		?>
		</tbody> <!-- end of tbody -->
	</table> <!-- end of table -->
	<!--?php $this->notify->records( $records ); ?-->
</div> <!-- end of .table-responsive --> 