<?php
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_date_format = app_get_val($settings['date_and_time'],'grid_date_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit');  
?>
<div class="container-fluid">
	<div class="row">    
		<!--aside class="col-sm-2">
			<!--?php $this->load->view($location.'/leftmenu'); ?>
			<!?php $this->load->view($location.'/modules/barangay/sidebar'); ?--> 	
		<!--/aside!-->
		<div class="col-sm-12"> 
			<?php echo form_open_multipart($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form','enctype'=>'multipart/form-data')); ?> 
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6"> 
						<h1 class="title"><?php echo $this->CA->name; ?> Edit</h1>
					</div>
					<div class="col-sm-6 text-right"> 
						<?php btn_edit( $page, array('capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$this->CA->urc_name,'display'=>array('view','delete','trash') ));   
						if($id){ 
							export_individual_button( $page, array('capabilities'=>$capabilities,'id'=>$id, 'ur_name'=>$this->CE->urc_name,'display'=>array('export')) );		
						} 
						if($id == NULL){ 
						?>  
							<button class="btn btn-success btn-sm js-required-fields" type="submit">
								<span class="glyphicon glyphicon-save" aria-hidden="true"></span>
								Save Cash Advance
							</button>
						<?php }?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="form-group">
					<label class="col-sm-2">CA Number:</label>
					<div class="col-sm-4"> 
						<?php echo $ca_number; ?>
					</div> 
					<label class="col-sm-2">BCS Number:</label>
					<div class="col-sm-4">  
						<?php 
					 	$wb_id = app_get_value($fields, $this->WB->tblpref.'id');
					  	$wb_number = app_get_value($fields, $this->WB->tblpref.'number');
						$this->WB->get_data_link( $wb_id, $wb_number);  
					 	 ?>
					</div> 
				</div>  
				<div class="form-group">
					<label class="col-sm-2">Project Name:</label>
					<div class="col-sm-4"> 
						<?php echo get_value($fields, $this->CE->tblpref.'project_name'); ?>
					</div>
					<label class="col-sm-2">Budget Avail. Balance:</label>
					<div class="col-sm-4" id="balance"> 
						<?php echo number_format($balance,2,'.',','); ?>
					</div>
				</div>  
				<div class="form-group">
					<label class="col-sm-2">Employee:</label>
					<div class="col-sm-4"> 
						<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$user_id); ?>" target="_blank"><?php echo ucwords($name_of_employee); ?></a> 
					</div>
					<label class="col-sm-2">Bank Account:</label>
					<div class="col-sm-4"> 
						<?php echo $bank_account; ?>
					</div>
				</div>   
				<div class="form-group">
					<label class="col-sm-2">Date Requested:</label>
					<div class="col-sm-4"> 
						<?php echo datetime($date_requested,$grid_date_format); ?>
					</div>
					<label class="col-sm-2">Date Released:</label>
					<div class="col-sm-4"> 
						<?php 
						if( $status == 'Released' ){
							echo datetime($date_released,$grid_date_format); 
						} else {
							echo 'Unreleased';
						}
						?>
					</div>
				</div>  
				<div class="form-group">
					<label class="col-sm-2">Status:</label>
					<div class="col-sm-4"> 
						<?php echo $status; ?> 
					</div>
				 
				</div>  
				<div class="form-group">
					<label class="col-sm-2">CA Total:</label>
					<div class="col-sm-4" id="ca_total">  
						<?php
							echo number_format($ca_amount,'2','.',',');
						?>
					</div>
					<label class="col-sm-2">CA Limit:</label>
					<div class="col-sm-4"> 
						<?php 
						$ca_limit = get_value($fields,$this->WB->tblpref.'bcs');
						echo number_format($ca_limit,'2','.',','); 
						?>
					</div>
				</div>  
				<?php
				if( $id ){ 
					?>
					<div class="form-group">
						<label class="col-sm-2">Options Status:</label>
						<div class="col-sm-4"> 
							<?php 
							option_status( $status, $page, $id,  $show_status, $capabilities );  
							?>
						</div>
						<label class="col-sm-2">User Approved:</label>
						<div class="col-sm-4"> 
							<?php 
								if($approved_by_id){
							?>
								<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$approved_by_id); ?>" target="_blank" ><?php echo ucwords($approved_by); ?></a>
							<?php
								}
								else{
									echo "Not Yet Approved";
								}
							?>
							
						</div> 
					</div>   
					<div class="form-group">
						<label class="col-sm-2">Log Updated By:</label>
						<div class="col-sm-4"> 
							<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$ca_status_user_id); ?>" target="_blank" ><?php echo $ca_latest_status_user;?></a> (<?php echo $ca_latest_status_user_activity;?>) 
						</div> 
						<label class="col-sm-2">Log Updated:</label>
						<div class="col-sm-4"> 
							<?php echo datetime($ca_latest_status_created,'F j, Y \a\t H:ia'); ?>
						</div>
					</div>  
					<div class="form-group">
						<label class="col-sm-2">Log Status:</label>
						<div class="col-sm-4"> 
							<?php echo $ca_latest_status_user_activity; ?>
						</div> 
					</div>   	
					<?php
				}
				?>
				<hr /> 
				<?php $this->moduletag->field($model,array('name'=>'date_requested','value'=>current_date(),'type'=>'hidden')); ?> 
				<?php $this->moduletag->field($model,array('name'=>'created_by_user_id','value'=>$user_id,'type'=>'hidden')); ?>  
				<?php $this->moduletag->field($model,array('name'=>'status','value'=>$status,'type'=>'hidden')); ?>   
				<?php $this->moduletag->field($model,array('name'=>'ca_number','value'=>$new_ca_number,'type'=>'hidden')); ?>   
				<input type="hidden" name='cash_advance[wb_id]' value="<?php echo $wb_id; ?>"  /> 
				<?php $this->load->view($location.'/edit-form-footer-fields'); ?>
				<?php echo form_hidden('id',$id); ?>
				<?php echo form_hidden('available_balance',$balance); ?>
			</form>
			<?php if($id){ ?> 
			<?php } ?>
			<?php echo $sub_section_1; 
			// $items['record_rows'] = $items_table;
			// $this->load->view($location.'/modules/'.$this->CAI->view.'/record',$items);
			?> 
		</div>
	</div>
</div>


<!--script type="text/javascript">
$( "#ModCashAdvance_form" ).submit(function( event ) { 
	  event.preventDefault();
    var url = "<?php echo site_url('c=ModCashAdvance&m=json_check_balance'); ?>";
    var id = "<?php echo $id; ?>";
    var wb_id = "<?php echo $wb_id; ?>";
    var ca_limit = "<?php echo $ca_limit; ?>";
    var ce_number = "<?php echo $ce_number; ?>";
    var ca_amount = $( "input[name='cash_advance[car_ca_amount]']" ).val();
    
	var form_fields = { 
		'id': id,
		'wb_id': wb_id, 
		'ca_limit': ca_limit,
		'ca_amount': ca_amount,
		'ce_number': ce_number 
	}; 
	data = form_fields
	callback = function( json ){

	 	if( json == false ){
	 		alert('Sorry the entered Amount is larger than the allocated budget.');  
	 		 
 		  	return false;
	 	} else {
	 		$('form#ModCashAdvance_form').unbind('submit').submit();
	   
	 	}
	};

	jQuery.post( url, data, callback, "json" );  


});
</script-->