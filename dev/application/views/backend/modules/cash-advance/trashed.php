<?php
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
?>
<?php
if(isset($selected_capabilities)){
	$current_value_access = $this->usersrole->get_capability($selected_capabilities,'access','full'); 
}
?>
<div class="container-fluid">
	<div class="row">
		<!--aside class="col-sm-2">
			<!--?php $this->load->view($location.'/modules/barangay/sidebar'); ?--> 
		<!--/aside!-->
		<div class="col-sm-12"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-6">
						<h1><?php echo $this->CA->name; ?> Trashed Records</h1>
					</div> 
					<div class="col-sm-6 text-right">
						<?php btn_records( $page, array('capabilities'=>$capabilities,'ur_name'=>$this->CA->urc_name,'display'=>array('records')) ); ?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr> 
								<th>#</th>  
								<th width="10%">CA #</th>
								<th>Requested</th>
								<th>Employee</th>
								<th width="10%">BCS No.</th>
								<th width="20%">Project Name</th> 
								<th align="right">CA Total</th> 
								<th width="10%">Status</th>
								
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						$n=1;
						if( $record_rows ){
							
							foreach( $record_rows as $col ){
								$id = get_value($col,$this->CA->tblpref.'id'); 
								$ca_number = get_value($col, $this->CA->tblpref.'ca_number'); 
								$ca_amount = get_value($col, $this->CA->tblpref.'ca_amount');  
								$created = get_value($col, $this->CA->tblpref.'created');  
								$date_requested = get_value($col, $this->CA->tblpref.'date_requested');
								$created_display = datetime($created,$grid_date_time_format);  
								$user_created = get_value($col, $this->CA->tblpref.'created_by_user_id');
								$fullname = $this->moduser->get_user_fullname_by_id( $user_created, '{lastname}, {firstname}'); 
								$wb_id = get_value($col, $this->WB->tblpref.'id');
								$wb_no = get_value($col, $this->WB->tblpref.'number');
								$project_name = get_value($col, $this->CE->tblpref.'project_name');
								$activity_display = character_limiter($project_name,$grid_text_limit); 
								$ca_amount = $this->WB->get_total_amount($wb_id, $id ) ; 
								?>
								<tr>
									<td><?php echo $n; ?></td> 
									<td ><?php echo $ca_number; ?></td>   
									<td><?php echo $date_requested; ?></td> 	
									<td >
										<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$user_created); ?>" target="_blank"><?php echo ucwords($fullname); ?></a> 
									</td>    
									<td><?php $this->WB->get_data_link( $wb_id, $wb_no); ?></td>
									<td title="<?php echo $project_name; ?>"><?php echo $activity_display; ?></td> 
									<td align="right"><?php echo number_format($ca_amount,2,'.',',');; ?></td> 	 
									<td class="text-right">
									<?php	 
									btn_optn_records($page, array(
										'model'=>$model,'fields'=>$col,
										'capabilities'=>$capabilities, 
										'ur_name'=>$this->CA->urc_name,
										'id'=>$id,
										'display'=>array('view','edit','delete','untrash')
									));
									?>
									</td>  
								</tr>
								<?php
								$n++;
							}
						}
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
				<nav>
					<?php echo $pagination; ?>
					<!--div class="showing-result"><?php app_pagination_details($per_page, $n); ?></div-->
				</nav>
			</form> <!-- end of form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->