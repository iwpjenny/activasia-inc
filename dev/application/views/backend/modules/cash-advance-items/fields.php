<div class="loader"></div>
<div class="form-group"> 
	<div class="col-sm-12" id="<?php echo $page; ?>-popup-content"> 
		 
	</div> 
</div>
 
<script type="text/javascript">  
function show_loader(){
	$(".loader").show();
}

function hide_loader(){
	$(".loader").fadeOut("slow");
	// $( "#ca_date_needed" ).datepicker();
	
	$('input#ca_date_needed').datepicker({
		dateFormat : 'yy-mm-dd'
	}
);
}
function display_field_details( data ){ 
	var form_obj = $("form[name='field_"+data.page+"']"); 
	$("input#name",form_obj).val( data.field.name );  
}

function json_update_popup_content( page ){
	 show_loader();    
	var form_obj = $("form[name='field_"+page+"']");
	var url = form_obj.attr("action");
	url = url+"&m=json_update_popup_content";
	var data = form_obj.serialize();
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: update_popup_content, 
		complete: hide_loader
	}); 
}

function update_popup_content( data ){	
	
	if( data.result == true ){
		$("div#"+data.page+"-popup-content").html(data.popup_content);    
		var available_balance = $('input[name="available_balance"]').val();  
		thousand_balance = available_balance.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
		thousand_balance = thousand_balance + '.00';
		$("#total-balance").html(thousand_balance);    
	} 
} 

function json_new_record_custom( data ){	
 
	json_update_popup_content( data );
	 
}
function check_option( val, page ){   
	var atLeastOneIsChecked = $('input[name="items[]"]:checked').length;
	
	if( atLeastOneIsChecked > 0 ){ 
		 enable_inputs( val );
		 check_if_disabled_or_not(val);
		 compute_items();    
	} else {
		disable_inputs(val);
		$("strong#total-items").html('0.00'); 
		var balance = $('input[name="available_balance"]').val();
		balance = parseFloat(balance);
		balance = balance.toFixed(2);
		var balance = balance.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		$("strong#total-balance").html(balance);
		$('input[name="available_balance"]').val(thousand_existing_total_balance);
	}
	
	$("input.header_input").change(function() { 
		compute_items();  
	});
	 
}
function check_if_disabled_or_not(val){  
	var result = $('input[name="items[]"][value="'+val+'"]').is(":checked");  
	if(result == true){
		enable_inputs( val );
	}
	else{
		disable_inputs(val);
	}
}
function compute_items(){ 
	var checked_items = $('input[name="items[]"]:checked');
	var checked_items_length = $('input[name="items[]"]:checked').length;
	var total_items=0, item_id, item_asf, item_unit_cost, multiplier_product=0, total=0, total_items_alert=0;
	var header1_val, header2_val, header3_val, header4_val;
	var available_balance = $('input[name="available_balance"]').val();
	
	for(var x=0; x < checked_items_length; x++){
		item_id = $(checked_items[x]).val();
		item_asf = $("input[name='item_asf["+item_id+"]']").val();
		item_unit_cost = $("input[name='item_unit_cost["+item_id+"]']").val();
		bcs = $("input[name='bcs["+item_id+"]']").val();
		item_asf = eval(item_asf);
		item_unit_cost = eval(item_unit_cost);
		
		header1_val = $('input[name="header_1['+item_id+']"]').val();	
		header2_val = $('input[name="header_2['+item_id+']"]').val();	
		header3_val = $('input[name="header_3['+item_id+']"]').val();	
		header4_val = $('input[name="header_4['+item_id+']"]').val();
		
		multiplier_product = (((header1_val*header2_val)*header3_val)*header4_val);  
		// total = (item_asf+(multiplier_product*item_unit_cost));
		total =  multiplier_product*item_unit_cost;
		balance = bcs - total;
		total_items += total;
		
		$("input[name='total["+item_id+"]']").val(total.toFixed(2));
		$("input[name='balance["+item_id+"]']").val(balance.toFixed(2));
		if(balance < 0){
			alert("Insufficient item balance. Item Amount is "+total.toFixed(2)+", please try again.");  
			disable_inputs( item_id ); 
		}
		if( total_items > available_balance ){
			total_items_alert = (total_items.toFixed(2).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
			alert("Insufficient balance. Total Item Amount is "+total_items_alert+", please try again."); 
			disable_inputs( item_id );
			break;
		}
	} 
	
	var existing_total_balance = available_balance - total_items;
	var thousand_existing_total_balance = existing_total_balance.toFixed(2);
	var thousand_total_items = total_items.toFixed(2);
	var thousand_total_items = thousand_total_items.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var thousand_existing_total_balance = thousand_existing_total_balance.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	 
	
	$("strong#total-items").html(thousand_total_items);
	$("strong#total-balance").html(thousand_existing_total_balance);
	
}

function alert_and_disable(val){
	$('input[name="items[]"][value="' + val.toString() + '"]').prop("checked", false);
	alert("Insufficient balance."); 
	disable_inputs(val);
}
 
 function disable_inputs( value ){   
	$('input[name="header_1['+value+']"]').prop("disabled", true);
	$('input[name="header_2['+value+']"]').prop("disabled", true);
	$('input[name="header_3['+value+']"]').prop("disabled", true);
	$('input[name="header_4['+value+']"]').prop("disabled", true);
	$('input[name="header_1['+value+']"]').val(0);	
	$('input[name="header_2['+value+']"]').val(0);	
	$('input[name="header_3['+value+']"]').val(0);	
	$('input[name="header_4['+value+']"]').val(0);	
	$("input[name='total["+value+"]']").val(0);
	$('input[name="items[]"][value="'+value+'"]').prop("checked", false);
	var bcs = $("input[name='bcs["+value+"]']").val();
	$("input[name='balance["+value+"]']").val(bcs);
}
function enable_inputs( value ){ 
	$('input[name="header_1['+value+']"]').prop("disabled", false);
	$('input[name="header_2['+value+']"]').prop("disabled", false);
	$('input[name="header_3['+value+']"]').prop("disabled", false);
	$('input[name="header_4['+value+']"]').prop("disabled", false);	 
	$('input[name="header_1['+value+']"]').val(1);	
	$('input[name="header_2['+value+']"]').val(1);	
	$('input[name="header_3['+value+']"]').val(1);	
	$('input[name="header_4['+value+']"]').val(1);	

	$(function() { 
		$(".numericOnly").numericInput({ allowFloat: true, allowNegative: false });
	});   
}

function compute_and_check(val,page){ 
	var form_obj = $("form[name='field_"+page+"']");
	var url = form_obj.attr("action");
	url = url+"&m=json_calculate_total";
	var form_fields = form_obj.serialize();
	var data = form_fields;
		
	callback = function( json ){ 
		var x = 2;
		var total = json.total;   
		var arr = [];
		$.each(total, function(key, value) {
			 $('input[name="total['+ key.toString() +']"').val(value);
		}); 
		if( json.response < 1 ){ 
			 $('input[name="items[]"][value="' + val.toString() + '"]').prop("checked", false);
			 alert("Insufficient balance."); 
			 disable_inputs(val); 
		}
		else { 
			enable_inputs( val,page );
		}	
	};
	
	jQuery.post( url, data, callback, "json" ); 
	
}   
</script>