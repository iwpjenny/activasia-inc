<?php
$view_date_time_format = $this->setconf->get('date_and_time','view_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit');


if( $records ){
	$n=1; 
	$total_unit_cost = 0; 
	$total_sum = 0;
	$total_header_1 = 0;
	$total_header_2 = 0;
	$total_header_3 = 0;
	$total_header_4 = 0; 

	foreach( $records as $col ){ 
		$id = get_value($col,$model->tblid); 
		$wbi_id = get_value($col,$this->WBI->tblid);
		$this->load->library('Module'); 
		$item_details = $this->module->record( $this->WBI, $wbi_id ); 
		$ce_id = get_value($item_details, $this->CE->tblid); 
		$code = get_value($item_details, $this->WBI->tblpref.'code');
		$name = get_value($item_details, $this->WBI->tblpref.'name');
		$unit_cost = get_value($item_details, $this->WBI->tblpref.'unit_cost');		
		$unit = get_value($item_details, $this->WBI->tblpref.'unit_measure');   
		$header_1 = get_value($col, $this->CAI->tblpref.'header_1');   
		$header_2 = get_value($col, $this->CAI->tblpref.'header_2');   
		$header_3 = get_value($col, $this->CAI->tblpref.'header_3');   
		$header_4 = get_value($col, $this->CAI->tblpref.'header_4');   
		// $asf_amount = $this->CEI->get_asf_value($ce_id, $code);  
		$multiplier_product =  $header_1 * $header_2 * $header_3 * $header_4;
		$total = $this->CAI->get_individual_total($unit_cost, $multiplier_product); 
	
		$total_unit_cost += $unit_cost; 
		$total_sum += $total;
		$total_header_1 += $header_1;
		$total_header_2 += $header_2;
		$total_header_3 += $header_3;
		$total_header_4 += $header_4;
		 
		?>
		<tr>  
			<td><?php echo $n; ?></td>
			<td><?php echo $code; ?></td>
			<td><?php echo $name; ?></td>	
			<td align='right'><?php echo number_format($unit_cost,2,'.',','); ?></td>
			<td><?php echo $unit; ?></td>
			<td align='right'><?php echo number_format($header_1,2,'.',','); ?></td>
			<td align='right'><?php echo number_format($header_2,2,'.',','); ?></td>
			<td align='right'><?php echo number_format($header_3,2,'.',','); ?></td>
			<td align='right'><?php echo number_format($header_4,2,'.',','); ?></td> 
			<td align='right'><?php echo number_format($total,2,'.',','); ?></td>  
			<?php
			if( $views_editable === TRUE ){
			?>
			<td class="text-right">
				<?php 
				$params = array(
					'id'=>$id,'model'=>$model,'capabilities'=>$capabilities,
					'confirmaiton'=>TRUE,'title'=>$name,'fields'=>$col,'delete_only'=>TRUE
				);
				modal_btn_optn_records( $page, $params );  
				?>
			</td>
			<?php 
			}
			?> 
		</tr>
		<?php
		$n++;
	}
	?>
	<tr>
		<td colspan="3" align='right'><b>Total:</b></td>
		<td align='right'><b><?php echo number_format($total_unit_cost,2,'.',',');?></b></td>  
		<td></td>
		<td align='right'><b><?php echo number_format($total_header_1,2,'.',',');?></b></td>     
		<td align='right'><b><?php echo number_format($total_header_2,2,'.',',');?></b></td>     
		<td align='right'><b><?php echo number_format($total_header_3,2,'.',',');?></b></td>     
		<td align='right'><b><?php echo number_format($total_header_4,2,'.',',');?></b></td>     
		<td align='right'><b><?php echo number_format($total_sum,2,'.',',');?></b></td>     
	</tr>
	<?php
}
else{
?>
	<tr>  
		<td colspan="12" class="text-center"><strong class="empty-record-table-label"><?php echo "No items."; ?><strong></td>
	</tr>
<?php
}
?>
 
<script>
function json_before_update_grid_custom( data ){  
	var balance = data.balance;
	var total = data.ca_total;
	balance = json_convert_to_thousand( balance );
	total = json_convert_to_thousand( total );
	$('#balance').html(balance);	 
	$('#ca_total').html(total);	   
	$('input[name="available_balance"]').val(data.balance)
}

function json_convert_to_thousand( digit ){
	var thousand_total_items = digit.toFixed(2);
	thousand_total_items = thousand_total_items.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	return thousand_total_items;
}
  
</script>