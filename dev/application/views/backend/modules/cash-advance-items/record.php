<div class="table-responsive">
	<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th>No.</th>
				<th>Code</th>
				<th>Particulars</th>
				<th>Qty</th>
				<th>Unit</th>
				<th>Freq</th>
				<th>Man Days</th>
				<th>Unit Cost</th>
				<th>Total</th>
				<th>BCS w/ EWT</th>
				<th>Savings</th>
				<th>Option</th>
			</tr>
		</thead> <!-- end of thead -->
		<tbody>
		<?php
		if( $record_rows ){
			$n=1;
			foreach( $record_rows as $col ){ 
				$wbi_id = get_value($col,$this->WBI->tblid);
				$this->load->library('Module');
				$item_details = $this->module->record( $this->WBI, $wbi_id );
				$id = get_value($item_details, $this->WBI->tblpref.'no');
				$no = get_value($item_details, $this->WBI->tblpref.'no');
				$code = get_value($item_details, $this->WBI->tblpref.'code');
				$name = get_value($item_details, $this->WBI->tblpref.'name');
				$qty = get_value($item_details, $this->WBI->tblpref.'qty');
				$unit = get_value($item_details, $this->WBI->tblpref.'unit');
				$freq = get_value($item_details, $this->WBI->tblpref.'freq');
				$man_days = get_value($item_details, $this->WBI->tblpref.'man_days');
				$unit_cost = get_value($item_details, $this->WBI->tblpref.'unit_cost');
				$total = get_value($item_details, $this->WBI->tblpref.'total');
				$bcs_ewt = get_value($item_details, $this->WBI->tblpref.'bcs_ewt');
				$savings = get_value($item_details, $this->WBI->tblpref.'savings');
				?>
				<tr>  
					<td><?php echo $no; ?></td>
					<td><?php echo $code; ?></td>
					<td><?php echo $name; ?></td>	
					<td><?php echo $qty; ?></td>
					<td><?php echo $unit; ?></td>
					<td><?php echo $freq; ?></td>
					<td><?php echo $man_days; ?></td>
					<td><?php echo $unit_cost; ?></td>
					<td><?php echo $total; ?></td>
					<td><?php echo $bcs_ewt; ?></td>
					<td><?php echo $savings; ?></td>
					<td class="text-right">
						<button class="btn btn-danger btn-xs" type="button" onclick="remove()">Remove</button>
					</td>  
				</tr>
				<?php
				$n++;
			}
		}
		?>
		</tbody> <!-- end of tbody -->
	</table> <!-- end of table -->
</div> <!-- end of .table-responsive -->  