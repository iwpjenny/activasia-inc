<?php
$parent_id = app_get_val($fields,$this->Trans->tblid);
?>
<div class="container-fluid">
	<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?> 
		<div class="row">
			<aside class="col-sm-2 sidebar">    
				<?php $this->load->view($location.'/leftmenu'); ?>
				<?php $this->load->view($location.'/member/sidebar'); ?>
			</aside>
			<div class="col-md-10 main">
				<div class="row" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $this->TransSub->name; ?> View</h1>
					</div>
					<div class="col-sm-6 text-right">
						<?php btn_view( $page, array('capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$this->TransSub->urc_name) ); ?>
					</div>
				</div> 
				<?php $this->notify->show(); ?>	
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Transaction:</div>
					<div class="col-xs-6 col-sm-4">
						<b><a href="<?php echo site_url('c='.$this->Trans->page.'&m=edit&id='.$parent_id); ?>" target="_blank">
						<?php app_value($fields,$this->Trans->tblpref.'title'); ?></a></b>
					</div>  
					<div class="col-xs-6 col-sm-2"></div>
					<div class="col-xs-6 col-sm-4">
					</div>  
				</div>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Title:</div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->TransSub->tblpref.'title'); ?></b>
					</div>  
					<div class="col-xs-6 col-sm-2">Description:</div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->TransSub->tblpref.'description'); ?></b>
					</div>  
				</div>
				<hr />
				<?php $this->load->view($location.'/view-footer-fields'); ?>
			</div> <!-- end of .col-sm-9 col-md-10 main -->
		</div> <!-- end of .row -->
		<?php echo form_hidden('id',$id); ?>
	</form> <!-- end of .form -->
</div> <!-- end of .container-fluid -->