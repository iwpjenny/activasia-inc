<?php
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
?>
<?php
if(isset($selected_capabilities)){
	$current_value_access = $this->usersrole->get_capability($selected_capabilities,'access','full'); 
}
?>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2">
			<!--?php $this->load->view($location.'/modules/barangay/sidebar'); ?--> 
		</aside>
		<div class="col-sm-10"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-6">
						<h1><?php echo $model->name; ?> Trashed Records</h1>
					</div> 
					<div class="col-sm-6 text-right">
						<?php btn_records( $page, array('capabilities'=>$capabilities,'ur_name'=>$model->urc_name,'display'=>array('add','records')) ); ?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th> 
								<th>CA Number</th>  
								<th>CA Amount</th>  
								<th>Created</th> 	
								<th>Option</th> 
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						if( $record_rows ){
							$n=1;
							foreach( $record_rows as $col ){
								$id = get_value($col,$this->CA->tblpref.'id'); 
								$ca_number = get_value($col, $this->CA->tblpref.'ca_number'); 
								$ca_amount = get_value($col, $this->CA->tblpref.'ca_amount');  
								$created = get_value($col, $this->CA->tblpref.'created');  
								$created_display = datetime($created,$grid_date_time_format); 
								?>
								<tr>
									<td><?php echo $n; ?></td> 
									<td ><?php echo $ca_number; ?></td>   
									<td><?php echo $ca_amount; ?></td> 	
									<td title="<?php echo $created; ?>" align="right">
									<small><?php echo $created_display; ?></small>
									</td>
									<td class="text-right">
									<?php	 
									btn_optn_records($page, array(
										'capabilities'=>$capabilities,
										'ur_name'=>$this->CA->urc_name,
										'id'=>$id,
										'display'=>array('view','edit','delete','untrash')
									));
									?>
									</td>  
								</tr>
								<?php
								$n++;
							}
						}
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
				<nav>
					<?php echo $pagination; ?>
					<div class="showing-result"><?php app_pagination_details($per_page, $n); ?></div>
				</nav>
			</form> <!-- end of form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->