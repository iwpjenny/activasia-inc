<?php
$user_id = app_get_value($fields,$this->User->tblid);
?>

<div class="container-fluid">
	<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?> 
		<div class="row">
			<aside class="col-sm-2 sidebar">    
				<?php $this->load->view($location.'/leftmenu'); ?>
			
			</aside>
			<div class="col-md-10 main">
				<div class="row" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $model->name; ?> View</h1>
					</div>
					<div class="col-sm-6 text-right">
						<?php btn_view( $page, array('model'=>$model,'fields'=>$fields,'capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$model->urc_name) ); ?>
					</div>
				</div>
				<?php $this->notify->show(); ?>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Title:</div>
					<div class="col-xs-6 col-sm-4"> 
						<b><?php app_value($fields,$this->PA->tblpref.'title'); ?></b>
					</div>  
					<div class="col-xs-6 col-sm-2">Name:</div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->PA->tblpref.'name'); ?></b>
					</div>  
				</div>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Description:</div>
					<div class="col-xs-6 col-sm-4"> 
						<b><?php app_value($fields,$this->PA->tblpref.'description'); ?></b>
					</div>  
				</div>
	
				<hr />
				<?php $this->load->view($location.'/view-footer-fields'); ?>
			</div> <!-- end of .col-sm-9 col-md-10 main -->
		</div> <!-- end of .row -->
		<?php echo form_hidden('id',$id); ?>
	</form> <!-- end of .form -->
</div> <!-- end of .container-fluid -->