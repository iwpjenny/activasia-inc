<?php
//$current_value_access = $this->usersrole->get_capability($selected_capabilities,'access','full');
?>
<div class="container-fluid">
	<div class="row">    
		<aside class="col-sm-2">
			<!--?php $this->load->view($location.'/leftmenu'); ?>
			<!?php $this->load->view($location.'/modules/barangay/sidebar'); ?--> 	
		</aside>
		<div class="col-sm-10"> 
			<?php echo form_open_multipart($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form','enctype'=>'multipart/form-data')); ?> 
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $model->name; ?> Edit</h1>
					</div>
					<div class="col-sm-6 text-right">
						<?php btn_edit( $page, array('model'=>$model,'fields'=>$fields,'capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$model->urc_name) ); ?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (<span class="red">*</span>) are required fields and must not be blank.</h6>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Title <span class="red">*</span>:</label>
					<div class="col-sm-4"> 
						<?php $this->moduletag->field($model,array('name'=>'title','holder'=>'Title','req'=>TRUE)); ?>
					</div>
					<label class="col-sm-2">Name<span class="red">*</span>:</label>
					<div class="col-sm-4"> 
						<?php $this->moduletag->field($model,array('name'=>'name','holder'=>'Name','req'=>TRUE)); ?>
						<small>Value format: small letters only, no space, space must be dash -.</small>
					</div>
				</div>  
				<div class="form-group">
					<label class="col-sm-2">Description</span>:</label>
					<div class="col-sm-4"> 
						<?php $this->moduletag->field($model,array('name'=>'description','holder'=>'Description','type'=>'textarea')); ?>
					</div> 
				</div>  
				<hr />
				<?php $this->load->view($location.'/edit-form-footer-fields'); ?>
				<?php echo form_hidden('id',$id); ?>
			</form>
		</div>
	</div>
</div>