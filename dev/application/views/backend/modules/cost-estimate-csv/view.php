<?php 
$filepath = app_get_val($fields,$model->tblpref.'filepath'); 
$file_pathinfo = pathinfo($filepath);
$user_id = app_get_value($fields,$this->User->tblid);

$download = $this->usersrole->get_capability($capabilities, $model->urc_name, 'download');
$download_url = site_url('c='.$page.'&m=download&id='.$id);
?>
<div class="container-fluid">
	<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?> 
		<div class="row">
			<!--aside class="col-sm-2 sidebar">    
				<?php //$this->load->view($location.'/sidebar'); ?>
			</aside-->
			<div class="col-md-12 main">
					<div class="row" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $model->name; ?> View</h1>
					</div>
					<div class="col-sm-6 text-right">
						<?php btn_view( $page, array('model'=>$model,'fields'=>$fields,'capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$model->urc_name, 'display'=>array('back','delete')) ); ?>
						<?php
						if( $download ){
						?>
						<a class="btn btn-success btn-sm" href="<?php echo $download_url; ?>"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> Download</a>
						<?php
						}
						?>
					</div>
				</div>
				<?php $this->notify->show(); ?>
				<div class="form-group">
					<label class="col-sm-2">User:</label>
					<div class="col-sm-4">
						<?php 
						$lastname = app_get_val($fields,$this->User->tblpref.'lastname');  
						$firstname = app_get_val($fields,$this->User->tblpref.'firstname');  
						$user_id = app_get_val($fields,$this->User->tblid);   
						?>
						
						<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$user_id); ?>" target="_blank"><?php echo  $lastname.', '.$firstname; ?></a> 
					</div> 
					<label class="col-sm-2">Module Type:</label>
					<div class="col-sm-4">
						<?php app_value($fields,$model->tblpref.'module_type'); ?>
					</div> 
				</div>
				<div class="form-group">
					<label class="col-sm-2">Type:</label>
					<div class="col-sm-4">
						<?php app_value($fields,$model->tblpref.'type'); ?>
					</div> 
					<label class="col-sm-2">File Name:</label>
					<div class="col-sm-4">
						<?php app_value($file_pathinfo,'basename'); ?>
					</div> 
				</div>
				<div class="form-group">
					<label class="col-sm-2">File Path <span class="red">*</span>:</label>
					<div class="col-sm-10">
						<?php app_value($fields,$model->tblpref.'filepath'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Title <span class="red">*</span>:</label>
					<div class="col-sm-4">
						<?php app_value($fields,$model->tblpref.'title'); ?>
					</div>
					<label class="col-sm-2">Description :</label>
					<div class="col-sm-4">
						<?php app_value($fields,$model->tblpref.'description'); ?>
					</div>
				</div>
				<hr /> 
			</div> <!-- end of .col-sm-9 col-md-10 main -->
		</div> <!-- end of .row -->
		<?php echo form_hidden('id',$id); ?>
	</form> <!-- end of .form -->
</div> <!-- end of .container-fluid -->