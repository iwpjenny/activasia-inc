<?php
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit');
?>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2">
			<?php $this->load->view($location.'/member/sidebar'); ?> 
		</aside>
		<div class="col-sm-10"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-6">
						<h1><?php echo $model->name; ?> Trashed Records</h1>
					</div> 
					<div class="col-sm-6 text-right">
						<?php btn_records( $page, array('capabilities'=>$capabilities,'ur_name'=>$model->urc_name,'display'=>array('add','records')) ); ?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th>
								<th>User</th> 
								<th>Image</th> 
								<th>Title</th> 
								<th>Created</th>  
								<th>Option</th>  
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						if( $record_rows ){
							$n=1;
							foreach( $record_rows as $col ){
								$id = get_value($col,$model->tblid); 
								$title = get_value($col, $model->tblpref.'title');
								$created = get_value($col, $model->tblpref.'created');
								$firstname = get_value($col, $this->Mem->tblpref.'firstname'); 
								$lastname = get_value($col, $this->Mem->tblpref.'lastname');  
								$image = $this->modulefiles->record_grid_thumbnail( $id );
								$title_display = character_limiter($title,$grid_text_limit); 	
								$created_display = datetime($created,$grid_date_time_format);
								?>
								<tr>
									<td><?php echo $n; ?></td>
									<td><?php echo $lastname.', '.$firstname; ?></td> 
									<td><?php echo $image; ?></td> 
									<td title="<?php echo $title; ?>"><?php echo $title_display; ?></td> 
									<td title="<?php echo $created; ?>" align="right">
										<small><?php echo $created_display; ?></small>
									</td>
									<td class="text-right">
									<?php	 
									btn_optn_records($page, array(
										'model'=>$model,'fields'=>$col,
										'capabilities'=>$capabilities,
										'id'=>$id,
										'display'=>array('view','edit','delete','untrash')
									));
									
									?>
									</td>  
								</tr>
								<?php
								$n++;
							}
						}
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
				<nav>
					<?php echo $pagination; ?>
					<div class="showing-result">
						<?php app_pagination_details( $total_rows ); ?>
					</div>
				</nav>
			</form> <!-- end of form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->