<?php  
$display_latest_transactions = app_get_val($settings['admin_dashboard'],'display_latest_transactions'); 
if( $this->usersrole->get_capability($capabilities, $model->urc_name, 'records' ) && $display_latest_transactions == 'yes'  ){  
?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel-group basic_reg_form">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="<?php echo site_url('c='.$model->page); ?>">Latest <?php echo $model->name; ?></a>
				</div>
				<div class="panel-body"> 
					 <div class="table-responsive">
						<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
							<thead>
								<tr>
									<th>#</th>
									<th>User</th> 
									<th>Description</th>
									<th>Created</th> 
									<?php 
									if( $this->usersrole->get_capability($capabilities, $model->urc_name, 'view' ) ){
										?>
										<th>Option</th> 
										<?php 
									}
									?>
								</tr>
							</thead> <!-- end of thead -->
							<tbody>
							<?php
							if( $records ){
								$n=1;
								foreach( $records as $col ){
									$id = get_value($col, $model->tblpref.'id'); 
									$description = get_value($col, $model->tblpref.'description');
									$created = get_value($col, $model->tblpref.'created');
									$firstname = get_value($col, $this->Mem->tblpref.'firstname'); 
									$lastname = get_value($col, $this->Mem->tblpref.'lastname');
									$created_display = datetime($created,$grid_date_time_format); 
									$description_display = character_limiter($description,$character_limit);
									?>
									<tr>
										<td><?php echo $n; ?></td>
										<td><?php echo $lastname.', '.$firstname ; ?></td> 
										<td title="<?php echo $description; ?>"><?php echo $description_display; ?></td>
										<td title="<?php echo $created; ?>" align="right"><small><?php echo $created_display; ?></small></td>
										<?php 
										if( $this->usersrole->get_capability($capabilities, $model->urc_name, 'view' ) ){
											?>
											<td class="text-right">
												<?php 
												btn_optn_records($model->page, array(
													'model'=>$model,'fields'=>$col,
													'capabilities'=>$capabilities,
													'id'=>$id,
													'display'=>array('view','edit','delete','trash')
												));
												?>
											</td>	
											<?php 
										}
										?>				
									</tr>
									<?php
									$n++;
								}
							}
							?>
							</tbody>
						</table>
						<?php $this->notify->records( $records ); ?>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>  
<?php 
}
?>