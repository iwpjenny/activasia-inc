<?php
//$current_value_access = $this->usersrole->get_capability($selected_capabilities,'access','full');
?>
<div class="container-fluid">
	<div class="row">    
		<!--aside class="col-sm-2">
			<!--?php $this->load->view($location.'/leftmenu'); ?>
			<!?php $this->load->view($location.'/modules/barangay/sidebar'); ?> 	
		</aside-->
		<div class="col-sm-12"> 
			<?php echo form_open_multipart($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form','enctype'=>'multipart/form-data')); ?> 
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6"> 
						<h1 class="title"><?php echo $this->CE->name; ?> Edit</h1>
					</div>
					<div class="col-sm-6 text-right"> 
						<?php btn_edit( $page, array('capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$this->CE->urc_name) );  
						?>  
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (<span class="red">*</span>) are required fields and must not be blank.</h6>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">CE Number <span class="red">*</span>:</label>
					<div class="col-sm-4"> 
						<?php $this->moduletag->s(array('name'=>'number','holder'=>'CE Number','req'=>TRUE, 'max' => '30')); ?>
					</div>
					<label class="col-sm-2">CE Date <span class="red">*</span>:</label>
					<div class="col-sm-4"> 
						<?php $this->moduletag->s(array('name'=>'date','holder'=>'CE Date','req'=>TRUE, 'class'=>'datepicker','attr'=>'readonly')); ?>
					</div>
				</div> 
				<div class="form-group">
					<label class="col-sm-2">Region <span class="red">*</span>:</label>
					<div class="col-sm-4">
					<?php echo $select_region; ?>
				</div> 
					<label class="col-sm-2">PO #:</label>
					<div class="col-sm-4"> 
						<?php $this->moduletag->s(array('name'=>'po_number','holder'=>'PO Number')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Activity <span class="red">*</span>:</label>
					<div class="col-sm-4">
						<?php $this->moduletag->s(array('name'=>'activity','holder'=>'Activity','req'=>TRUE, 'type'=>'textarea')); ?>
					</div> 
					<label class="col-sm-2">CE Amount <span class="red">*</span>:</label>
					<div class="col-sm-4"> 
						<?php $this->moduletag->s(array('name'=>'amount','holder'=>'CE Amount','req'=>TRUE)); ?><small>Value format: No comma on thousand.<br />VAT Exclusive.</small>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Month of Activity:</label>
					<div class="col-sm-4">
						<?php 
							$date_activity = get_value( $fields, $this->CE->tblpref.'date', current_date()); 
							echo datetime($date_activity,'F\-y');
						?>
					</div> 
					<label class="col-sm-2">Quarter<span class="red">*</span>:</label>
					<div class="col-sm-4"> 
						<?php echo $qtr;?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Total CA Limit:</label>
					<div class="col-sm-4">
						<?php echo $working_amount; ?>
					</div> 
					<label class="col-sm-2">Total CA Released:</label>
					<div class="col-sm-4"> 
						<?php echo $total_ca_released; ?> 
					</div>
				</div>
				<div class="form-group"> 
					<label class="col-sm-2">CA Limit vs CA Released:</label>
					<div class="col-sm-4"> 
						<?php echo $working_amount_balance; ?>%
					</div>
					<label class="col-sm-2">Remarks:</label>
					<div class="col-sm-4"> 
						<?php $this->moduletag->s(array('name'=>'remarks','holder'=>'Remarks','type'=>'textarea')); ?>
					</div>
				</div>  
				<hr />
				<?php $this->load->view($location.'/edit-form-footer-fields'); ?>
				<?php echo form_hidden('id',$id); ?>
			</form>
		</div>
	</div>
</div>