<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
$import_ce = $this->usersrole->get_capability($capabilities, $this->CE->urc_name, 'import'); 
$form_action_ce_csv = site_url('c='.$this->CECSV->page.'&m=upload');
?>
<div class="container-fluid">
	<div class="row">
		<!--aside class="col-sm-2">
			<?php //$this->load->view($location.'/modules/barangay/sidebar'); ?>  
		</aside-->
		<div class="col-sm-12"> 
			<?php $this->recordsfilter->show(); ?>
			<hr />
			<div class="row">
				<div class="col-sm-6">
					<h1><?php echo $model->name; ?> Records</h1>
				</div> 
				<div class="col-sm-6"> 
					<div class="row"> 
						<div class="col-sm-12  text-right">  
							<?php btn_records( $page, array('capabilities'=>$capabilities,'ur_name'=>$model->urc_name,'display'=>array('add','trash')) ); 
							get_custom_button( $page, array('capabilities'=>$capabilities,'ur_name'=>$this->CE->urc_name,'display'=>array('export','import'),'import_name'=>'View Uploaded CE Files', 'export'=>'modal') ); 
							?>
						</div>
					</div>
				</div>
			</div>
			<?php
			if( $import_ce ){
				?>
				<hr />
				<?php
				echo form_open_multipart($form_action_ce_csv, array('class'=>'form-horizontal margin-top-md','id' =>$page.'_form','name' =>$page.'_form'));
					?>
					<div class="form-group">
						<label class="col-md-8 col-sm-6 col-xs-12 text-right"> 
							Upload CSV file here:
						</label> 
						<div class="col-md-2 col-sm-3 col-xs-6"> 
							<input name="files[]" multiple="multiple" accept="file_extension|audio/*|video/*|image/*|media_type" type="file" />
						</div> 
						<div class="col-md-2 col-sm-3 col-xs-6 text-right">
							<button type="submit" class="btn btn-success btn-sm js-required-fields">
							<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Upload CSV
							</button>
						</div>
					</div>
				</form>
				<?php
			}
			?>
			<hr />
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>  
				<?php $this->notify->show(); ?> 
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th class='column-space'><input type="checkbox" id="select_all_ce" name="select_all_ce" value="all">&nbsp;#</th>
								<th>CE #</th>  
								<th width="25%">Project Name</th>  
								<th width="15%">Client</th>  
								<th>Project Cost</th>    
								<th>CA Limit</th>  
								<th>CA Released</th>   	
								<th width="3%"><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></th> 
								<th>Option</th> 
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						if( $record_rows ){
							$n=1;
							$total_project_cost = 0;
							$total_ca_limit_amount = 0;
							$total_ca_released_amount = 0;
							foreach( $record_rows as $col ){ 
								$id = get_value($col,$this->CE->tblpref.'id'); 
								$ce_no = get_value($col, $this->CE->tblpref.'number');
								$project_name = get_value($col, $this->CE->tblpref.'project_name'); 
								$client_name = get_value($col, $this->CE->tblpref.'client_name');  
								$grand_total = get_value($col, $this->CE->tblpref.'grand_total');  
								$project_cost = get_value($col, $this->CE->tblpref.'project_cost');  
								$region = get_value($col, $this->Reg->tblpref.'title'); 
								$amount = get_value($col, $this->CE->tblpref.'amount'); 
								$ca_limit_amount = $this->CE->get_total_limit( $id );
								$ca_released_amount =$this->CE->get_total_released_ca( $id );
								$po_number = get_value( $col, $this->CE->tblpref.'po_number');   
								$cancelled = get_value( $col, $this->CE->tblpref.'cancelled');  

								$total_project_cost += $project_cost;
								$total_ca_limit_amount += $ca_limit_amount;
								$total_ca_released_amount += $ca_released_amount;

								?>
								<tr>
									<td><input type="checkbox" class="select_ce" name="select_ce[]" value="<?php echo $id; ?>">&nbsp;<?php echo $n; ?></td> 
								 	<td ><?php echo $ce_no; ?></td>   
									<td title="<?php echo $project_name; ?>"><?php echo character_limiter($project_name,$grid_text_limit); ?></td> 	
									<td title="<?php echo $client_name; ?>"><?php echo character_limiter($client_name,$grid_text_limit); ?></td> 	 
									<td align='right'><?php echo number_format($project_cost, 2, '.', ','); ?></td> 	
									<td align='right'><?php echo number_format($ca_limit_amount, 2, '.', ','); ; ?></td> 	
									<td align='right'><?php echo number_format($ca_released_amount, 2, '.', ','); ?></td> 	 
									<td><?php cancelled($cancelled); ?></td> 	 
									<td class="text-right">
									<?php	 
										btn_optn_records($page, array(
											'model'=>$model,'fields'=>$col,
											'capabilities'=>$capabilities,
											'id'=>$id,
											'display'=>array('view','edit','trash')
										));
										echo $this->CE->get_ce_cancel_button( $id, $cancelled );
										?> 
									</td>  
								</tr>
								<?php
								$n++;
							}
							?>
							<tr>
								<td colspan="4" align="right"><strong>Total:</strong></td>
								<td align="right"><strong><?php echo number_format($total_project_cost,2,'.',',');?></strong></td>  
								<td align="right"><strong><?php echo number_format($total_ca_limit_amount,2,'.',',');?></strong></td>  
								<td align="right"><strong><?php echo number_format($total_ca_released_amount,2,'.',',');?></strong></td>  
							</tr>
							<?php
						} 
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
			<?php $this->load->view($location.'/modules/records-footer-pagination'); ?> 
			</form> <!-- end of form -->
		</div> <!-- end of .col-bm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->
<div class="modal fade" id="export_filter_modal" tabindex="-1" role="dialog" aria-labelledby="modal_export_filter">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Export Filter</h3>
			</div>
			<form method="POST" id="export_filter_modal_form"  class ="form-horizontal" action="">
				<div class="modal-body">  
					<div class="form-group margin-bottom-md">
						<label class="col-sm-2">Year:</label>
						<div class="col-sm-4">
							<?php echo app_select_year( null ); ?>
						</div>
						<label class="col-sm-2">Quarter:</label>
						<div class="col-sm-4"> 
							<select class="form-control" name="quarter">
								<option>---Select---</option>
								<option value="q1">Quarter 1</option>
								<option value="q2">Quarter 2</option>
								<option value="q3">Quarter 3</option>
								<option value="q4">Quarter 4</option>
							</select>
						</div> 
					</div> 
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-sm btn-info">Export</button>
				</div>
			</form>
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .modal -->
<script type="text/javascript">
$('.display-filter-modal').click(function() { 
	var action = $(this).attr('data-action'); 
    $('#export_filter_modal').modal('toggle'); 
	var action =$('#export_filter_modal_form').attr('action', action );
});
$('#export_filter_modal_form').submit(function() { 
    $('#export_filter_modal').modal('hide'); 
}); 
$(document).on("change", "input#select_all_ce", function(){  
	if (this.checked == true){
		$('input.select_ce').prop('checked', true);
	} else {
		$('input.select_ce').prop('checked', false);
	} 
});
function unpublished_ce(){
	var form_name = "<?php echo $page.'_form'; ?>"; 
	console.log(form_name);
	// return false;
	$('form#'+form_name).submit(); 
}
</script>