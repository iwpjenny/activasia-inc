 <div class="table-responsive">
	<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>CE #</th>  
				<th>PO #</th>  
				<th>CE Date</th>  
				<th>Activity</th>  
				<th>Region</th>  
				<th>CE Amount</th>  
				<th>CA Limit</th>  
				<th>CA Released</th>  
				<th>PAR</th>  
				<th>Billing</th>   
				<th>Option</th> 
			</tr>
		</thead> <!-- end of thead -->
		<tbody>
		<?php
		if( $records ){
			$n=1;
			foreach( $records as $col ){
				$id = get_value($col,$this->CE->tblpref.'id'); 
				$ce_no = get_value($col, $this->CE->tblpref.'number');
				$po_no = get_value($col, $this->CE->tblpref.'po_number'); 
				$date = get_value($col, $this->CE->tblpref.'date'); 
				$activity = get_value($col, $this->CE->tblpref.'activity'); 
				$region = get_value($col, $this->Reg->tblpref.'title'); 
				$amount = get_value($col, $this->CE->tblpref.'amount');  
				$ca_limit_amount = $this->CE->get_total_limit( $id );
				$ca_released_amount =	$this->CE->get_total_released_ca( $id ); 
				$po_number = get_value( $col, $this->CE->tblpref.'po_number'); 
				$par = $po_number != ''?'Y':'N'; 
				$billing_status = get_value($col,$this->CE->tblpref.'billing_status');
				$billing_status = $billing_status == 'yes'?'Y':'N';
				?>
				<tr>
					<td><?php echo $n; ?></td> 
					<td ><?php echo $ce_no; ?></td>   
					<td><?php echo $po_no; ?></td> 	
					<td><?php echo $date; ?></td> 	
					<td><?php echo $activity; ?></td> 	
					<td><?php echo $region; ?></td> 	
					<td><?php echo number_format($amount, 2, '.', ','); ?></td> 	
					<td><?php echo $ca_limit_amount; ?></td> 	
					<td><?php echo number_format($ca_released_amount, 2, '.', ','); ?></td> 	
					<td><?php echo $par; ?></td> 	
					<td><?php echo $billing_status; ?></td>  	
					 
					<td class="text-right">
					<?php	 
					btn_optn_records($this->CE->page, array(
						'capabilities'=>$capabilities,
						'ur_name'=>$this->CE->urc_name,
						'id'=>$id,
						'display'=>array('view','edit','delete','trash')
					));
					?>
					</td>  
				</tr>
				<?php
				$n++;
			}
		}
		?>
		</tbody>
	</table>
	<?php $this->notify->records( $records ); ?>
</div>