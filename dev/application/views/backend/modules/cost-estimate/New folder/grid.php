<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
$import_ce = $this->usersrole->get_capability($capabilities, $this->CE->urc_name, 'import'); 
$form_action_ce_csv = site_url('c='.$this->CECSV->page.'&m=upload');
 
?>
<div class="row margin-bottom-xs">
	<div class="col-sm-12 text-right">
		<?php
		if( $import_ce ){
			?>
			<?php echo form_open_multipart($form_action_ce_csv, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-offset-6 col-sm-3">
						 <input name="files[]" multiple="multiple" accept="file_extension|audio/*|video/*|image/*|media_type" type="file">
					</div>
					<div class="col-sm-3">
						<button type="submit" class="btn btn-success btn-sm js-required-fields">
						<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Upload CE .CSV File
						</button>
					</div>
				</div>
			</form>
			<?php
		}
		?>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="table-responsive">
			<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>CE #</th>  
						<th>Project Name</th>  
						<th>Client</th>     
						<th>CA Limit</th>  
						<th>CA Released</th> 
						<th width="3%"><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></th> 				
						<th>Option</th> 
					</tr>
				</thead> <!-- end of thead -->
				<tbody>
				<?php
				if( $records ){
					$n=1;
					foreach( $records as $col ){
						$id = get_value($col,$this->CE->tblpref.'id'); 
						$ce_no = get_value($col, $this->CE->tblpref.'number');
						$project_name = get_value($col, $this->CE->tblpref.'project_name'); 
						$client_name = get_value($col, $this->CE->tblpref.'client_name');  
						$grand_total = get_value($col, $this->CE->tblpref.'grand_total');  
						$region = get_value($col, $this->Reg->tblpref.'title'); 
						$amount = get_value($col, $this->CE->tblpref.'amount'); 
						$ca_limit_amount = $this->CE->get_total_limit( $id );
						$ca_released_amount =	$this->CE->get_total_released_ca( $id );
						$po_number = get_value( $col, $this->CE->tblpref.'po_number');   
						$cancelled = get_value( $col, $this->CE->tblpref.'cancelled');   
						?>
						<tr>
							<td><?php echo $n; ?></td> 
							<td ><?php $this->CE->get_data_link( $id, $ce_no); ?></td>    
							<td title="<?php echo $project_name; ?>"><?php echo character_limiter($project_name,$grid_text_limit); ?></td> 	
							<td title="<?php echo $client_name; ?>"><?php echo character_limiter($client_name,$grid_text_limit); ?></td> 	  
							<td><?php echo number_format( $ca_limit_amount,2,'.',','); ?></td> 	
							<td><?php echo number_format($ca_released_amount, 2, '.', ','); ?></td> 	 
							<td><?php cancelled($cancelled); ?></td> 	
							<td class="text-right">
							<?php	 
							btn_optn_records($this->CE->page, array(
								'capabilities'=>$capabilities,
								'ur_name'=>$this->CE->urc_name,
								'id'=>$id,
								'display'=>array('view','edit','delete','trash')
							));
							?>
							</td>  
						</tr>
						<?php
						$n++;
					}
				} 
				?>
				</tbody>
			</table>
			<?php $this->notify->records( $records ); ?>
		</div>
	</div>
</div>