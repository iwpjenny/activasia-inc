<?php
$user_id = app_get_value($fields,$this->User->tblid);
?>

<div class="container-fluid">
	<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?> 
		<div class="row">
			<aside class="col-sm-2 sidebar">    
				<?php $this->load->view($location.'/leftmenu'); ?>
			
			</aside>
			<div class="col-md-10 main">
				<div class="row" id="header-title-buttons">
					<div class="col-sm-6"> 
						<h1 class="title"><?php echo $this->CE->name; ?> View</h1> 
					</div>
					<div class="col-sm-6 text-right"> 
						<?php btn_view( $page, array('capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$this->CE->urc_name) ); ?> 
					</div>
				</div> 
				<?php $this->notify->show(); ?>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">CE Number:</div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->CE->tblpref.'number'); ?></b>
					</div>  
					<div class="col-xs-6 col-sm-2">CE Date:</div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->CE->tblpref.'date'); ?></b>
					</div>  
				</div>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Region: </div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->Reg->tblpref.'title'); ?></b>
					</div>  
					<div class="col-xs-6 col-sm-2">PO #:</div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->CE->tblpref.'po_number'); ?></b>
					</div>  
				</div>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Activity: </div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->CE->tblpref.'activity'); ?></b>
					</div> 
					<div class="col-xs-6 col-sm-2">CE Amount:  </div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->CE->tblpref.'po_number'); ?></b>
					</div> 						
				</div>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Month of Activity: </div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->CE->tblpref.'date'); ?></b>
					</div> 
					<div class="col-xs-6 col-sm-2">Quarter: </div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->CE->tblpref.'published'); ?></b>
					</div> 						
				</div>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Total CA Limit: </div>
					<div class="col-xs-6 col-sm-4">
						<b><?php echo $working_amount; ?></b>
					</div> 	
					<div class="col-xs-6 col-sm-2">Total CA Released: </div>
					<div class="col-sm-4"> 
						<strong><?php echo $total_ca_released; ?></strong>
					</div>			
				</div>
				<div class="form-group">   
					<div class="col-xs-6 col-sm-2">CA Limit vs CA Released: </div>
					<div class="col-sm-4"> 
						<strong><?php echo $working_amount_balance; ?></strong>
					</div>
					<div class="col-xs-6 col-sm-2">Remarks: </div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->CE->tblpref.'remarks'); ?></b>
					</div> 						
				</div> 
				<div class="form-group">
					<div class="col-xs-6 col-sm-2">Cancelled:</div>
					<div class="col-xs-6 col-sm-2">
						<strong><?php echo $published?'Yes':'No'; ?></strong>
					</div>
				</div>
				<hr />
				<?php $this->load->view($location.'/view-footer-fields'); ?>
			</div> <!-- end of .col-sm-9 col-md-10 main -->
		</div> <!-- end of .row -->
		<?php echo form_hidden('id',$id); ?>
	</form> <!-- end of .form -->
</div> <!-- end of .container-fluid -->