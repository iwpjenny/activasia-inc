<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
?>
<div class="container-fluid">
	<div class="row">
		<!--aside class="col-sm-2">
			<!--?php $this->load->view($location.'/modules/barangay/sidebar'); ?-->  
		<!--/aside!-->
		<div class="col-sm-12"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $model->name; ?> Records</h1> 
					</div> 
					<div class="col-sm-6 text-right">  
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th> 
								<th>CE #</th>  
								<th>CA #</th>  
								<th>Approve By</th>  
								<th>Status</th>  
								<th>Created</th>  
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						if( $record_rows ){
							$n=1;
							foreach( $record_rows as $col ){ 
								 
								$id = get_value($col,$this->CAS->tblpref.'id'); 
								$ca_number = get_value($col, $this->CA->tblpref.'ca_number'); 
								$ce_number = get_value($col, $this->CE->tblpref.'number'); 

								$ca_id = get_value($col, $this->CA->tblpref.'id'); 
								$ce_id = get_value($col, $this->CE->tblpref.'id'); 
								
								$status = get_value($col, $this->CAS->tblpref.'status'); 

								$user_details = $this->CAS->get_specific_data( $id );
								$user_id = get_value($user_details, $this->User->tblid);  
								$user_approved = get_value($col, $this->CA->tblpref.'user_approved'); 
								$user_name = $this->moduser->get_user_fullname_by_id( $user_id, '{lastname},	 {firstname}'); 
								$created = get_value($col, $this->CAS->tblpref.'created');   
								$created_display = datetime($created,$grid_date_time_format); 
								?>
								<tr>
									<td><?php echo $n; ?></td> 
									<td><?php $this->CE->get_data_link( $ce_id, $ce_number); ?></td> 
									<td><?php $this->CA->get_data_link( $ca_id, $ca_number); ?></td>   
									<td >
										<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$user_id); ?>" target="_blank"><?php echo  $user_name; ?></a> 
									</td> 
									<td><?php echo get_ca_status_button( $status ); ?></td>
									<td title="<?php echo $created; ?>" align="right">
									<small><?php echo $created_display; ?></small>
									</td>  
								</tr>
								<?php
								$n++;
							}
						} 
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
			<?php $this->load->view($location.'/modules/records-footer-pagination'); ?> 
			</form> <!-- end of form -->
		</div> <!-- end of .col-bm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->