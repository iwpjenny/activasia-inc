<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
?>
<div class="container-fluid">
	<div class="row">
		<!--aside class="col-sm-2">
			<?php //this->load->view($location.'/modules/barangay/sidebar'); ?>  
		</aside-->
		<div class="col-sm-12"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $model->name; ?> Records</h1> 
					</div> 
					<!--div class="col-sm-6 text-right"> 
						<?php btn_records( $page, array('capabilities'=>$capabilities,'ur_name'=>$this->CE->urc_name,'display'=>array('')) );  
				 		?>
					</div-->
				</div>  
				<?php $this->notify->show(); ?>
				<?php $n = 0; ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th>  
								<th>CE #</th>  
								<th width="25%">Project Name</th>  
								<th width="15%">Client</th>  
								<th>Project Cost</th>
								<th>User</th>
								<th>Type</th>
								<th>Created</th>     
								<th>Option</th> 
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php  
						if( $record_rows ){
							$n=1;
							$total_project_cost = 0;
							$total_ca_limit_amount = 0;
							$total_ca_released_amount = 0;
							foreach( $record_rows as $col ){ 
								$id = get_value($col,$this->CE->tblpref.'id'); 
								$ce_no = get_value($col, $this->CE->tblpref.'number');
								$project_name = get_value($col, $this->CE->tblpref.'project_name'); 
								$client_name = get_value($col, $this->CE->tblpref.'client_name');  
								$grand_total = get_value($col, $this->CE->tblpref.'grand_total');  
								$project_cost = get_value($col, $this->CE->tblpref.'project_cost');  
								$region = get_value($col, $this->Reg->tblpref.'title'); 
								$amount = get_value($col, $this->CE->tblpref.'amount'); 
								$ca_limit_amount = $this->CE->get_total_limit( $id );
								$ca_released_amount =$this->CE->get_total_released_ca( $id );
								$po_number = get_value( $col, $this->CE->tblpref.'po_number');   
								$cancelled = get_value( $col, $this->CE->tblpref.'cancelled');  
								$logs_created = get_value( $col, $this->CE->tblpref.'logs_created');
								$logs_type = get_value($col,$this->CE->tblpref.'logs_type');
								$logs_user_id = get_value($col,$this->CE->tblpref.'created_by_user_id');
								$fullname = $this->moduser->get_user_fullname_by_id( $logs_user_id, '{lastname}, {firstname}'); 								

								$total_project_cost += $project_cost;
								$total_ca_limit_amount += $ca_limit_amount;
								$total_ca_released_amount += $ca_released_amount;

								?>
								<tr>
									<td><?php echo $n; ?></td> 
								 	<td><?php echo $ce_no; ?></td>   
									<td title="<?php echo $project_name; ?>"><?php echo character_limiter($project_name,$grid_text_limit); ?></td> 	
									<td title="<?php echo $client_name; ?>"><?php echo character_limiter($client_name,$grid_text_limit); ?></td> 	 
									<td align='right'><?php echo number_format($project_cost, 2, '.', ','); ?></td>
									<td >
										<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$logs_user_id); ?>" target="_blank"><?php echo  $fullname; ?></a> 
									</td>
									<td><?php echo $logs_type;?></td>
									<td align='right'><small><?php echo $logs_created; ?></small></td> 	
									 
								 
									<td class="text-right">
									<?php	 
										btn_optn_records($page, array(
											'model'=>$model,'fields'=>$col,
											'capabilities'=>$capabilities,
											'id'=>$id,
											'display'=>array('view')
										)); 
										?> 
									</td>  
								</tr>
								<?php
								$n++;
							}
						}  
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
			<?php $this->load->view($location.'/modules/records-footer-pagination'); ?> 
			</form> <!-- end of form -->
		</div> <!-- end of .col-bm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->
<script type="text/javascript">
$(document).on("change", "input#select_all_ce", function(){  
	if (this.checked == true){
		$('input.select_ce').prop('checked', true);
	} else {
		$('input.select_ce').prop('checked', false);
	} 
});
function unpublished_ce(){
	var form_name = "<?php echo $page.'_form'; ?>"; 
	console.log(form_name);
	// return false;
	$('form#'+form_name).submit(); 
}
</script>