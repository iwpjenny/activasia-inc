<?php
$user_id = app_get_value($fields,$this->User->tblid);
?>

<div class="container-fluid">
	<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?> 
		<div class="row">
			<!--aside class="col-sm-2 sidebar">    
				<?php //$this->load->view($location.'/leftmenu'); ?>
			
			</aside-->
			<div class="col-md-12 main">
				<div class="row" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $model->name; ?> View</h1>
					</div>
					<div class="col-sm-6 text-right">
						<?php btn_view( $page, array('model'=>$model,'fields'=>$fields,'capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$model->urc_name,'display'=>array('back')) );
						 ?>
						
					</div>
				</div>
				<?php $this->notify->show(); ?>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">CE Number:</div>
					<div class="col-xs-6 col-sm-4">
						<strong><?php app_value($fields,$this->CE->tblpref.'number'); ?></strong>
					</div>  
					<div class="col-xs-6 col-sm-2">Project Name:</div>
					<div class="col-xs-6 col-sm-4">
						<strong><?php app_value($fields,$this->CE->tblpref.'project_name'); ?></strong>
					</div>  
				</div>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Project Duration: </div>
					<div class="col-xs-6 col-sm-4">
						<strong><?php app_value($fields,$this->CE->tblpref.'duration'); ?></strong>
					</div>  
					<div class="col-xs-6 col-sm-2">Client:</div>
					<div class="col-xs-6 col-sm-4">
						<strong><?php app_value($fields,$this->CE->tblpref.'client_name'); ?></strong>
					</div>  
				</div>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Client Address: </div>
					<div class="col-xs-6 col-sm-4">
						<strong><?php app_value($fields,$this->CE->tblpref.'client_address'); ?></strong>
					</div> 
					<div class="col-xs-6 col-sm-2">Attention To:</div>
					<div class="col-xs-6 col-sm-4">
						<strong><?php app_value($fields,$this->CE->tblpref.'attention_to'); ?></strong>
					</div> 						
				</div>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Date Submitted: </div>
					<div class="col-xs-6 col-sm-4">
						<strong><?php app_value($fields,$this->CE->tblpref.'date_submitted'); ?></strong>
					</div>  			
				</div>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Project Cost: </div>
					<div class="col-xs-6 col-sm-4">
						<strong>
						<?php 
						$total =  get_value($fields,$this->CE->tblpref.'project_cost'); 
						echo number_format($total, 2, '.', ',');
						?>
						</strong>
					</div> 
					<div class="col-xs-6 col-sm-2">ASF:</div>
					<div class="col-xs-6 col-sm-4">
						<strong>
							<?php 
							$plus_asf = get_value($fields,$this->CE->tblpref.'total_asf'); 
							echo number_format($plus_asf, 2, '.', ',');
							?>
						</strong>
					</div> 						
				</div> 
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Total CA Limit: </div>
					<div class="col-xs-6 col-sm-4">
						<strong><?php echo $working_amount; ?></strong>
					</div> 	
					<div class="col-xs-6 col-sm-2">Total CA Released: </div>
					<div class="col-sm-4"> 
						<strong><?php echo $total_ca_released; ?></strong>
					</div>			
				</div>
				<div class="form-group">   
					<div class="col-xs-6 col-sm-2">CA Limit vs CA Released: </div>
					<div class="col-sm-4"> 
						<strong><?php echo $working_amount_balance; ?></strong>
					</div>
					<div class="col-xs-6 col-sm-2">Cancelled:</div>
					<div class="col-xs-6 col-sm-2">
						<strong><?php echo $published?'Yes':'No'; ?></strong>
					</div> 				
				</div> 
				<div class="form-group">   
					<div class="col-xs-6 col-sm-2">Created: </div>
					<div class="col-sm-4"> 
						<strong><?php app_value($fields,$this->CE->tblpref.'logs_created'); ?></strong>
					</div> 			
				</div>  
				<hr />  
				<h3>Items</h3> 
				<table class="table items-table table-bordered">
					<thead>	
					<tr>
						<th>No.</th>
						<th>Code</th>
						<th>Item Name</th>
						<th>Description</th>
						<th>Unit Cost</th>
						<th>Unit of Measure</th>
						<th>Header 1</th> 
						<th>Header 2</th> 
						<th>Header 3</th> 
						<th>Header 4</th> 
						<th>Multiplier Product</th> 
						<th>ASF</th> 
						<th>ASF Amount</th> 
						<th>Total</th>  
					<tr>
					</thead> 
					<tbody>	
						<?php  
						if($items){
							$i = 1;
							$total_unit_cost = 0;
							$total_asf_amount = 0;
							$total_sum = 0;
							$total_header_1 = 0;
							$total_header_2 = 0;
							$total_header_3 = 0;
							$total_header_4 = 0;
							$total_multiplier_product = 0;
							$total_asf= 0;
							
							foreach($items as $item){
								$unit_cost = app_get_value($item,$this->CEI->tblpref.'unit_cost'); 
								$asf_amount = app_get_value($item,$this->CEI->tblpref.'asf_amount'); 
								$total = app_get_value($item,$this->CEI->tblpref.'total'); 
								$header_1 = app_get_value($item,$this->CEI->tblpref.'header_1'); 
								$header_2 = app_get_value($item,$this->CEI->tblpref.'header_2'); 
								$header_3 = app_get_value($item,$this->CEI->tblpref.'header_3'); 
								$header_4 = app_get_value($item,$this->CEI->tblpref.'header_4'); 
								$multiplier_product = app_get_value($item,$this->CEI->tblpref.'multiplier_product'); 
								$asf = app_get_value($item,$this->CEI->tblpref.'asf'); 

								$total_sum +=$total; 
								$total_asf_amount +=$asf_amount; 
								$total_unit_cost +=$unit_cost; 
								$total_header_1 +=$header_1; 
								$total_header_2 +=$header_2; 
								$total_header_3 +=$header_3; 
								$total_header_4 +=$header_4; 
								$total_multiplier_product +=$multiplier_product; 
								$total_asf+=$asf; 
								?>	
								<tr>
									<td><?php echo $i; ?></td> 
									<td><?php app_value($item,$this->CEI->tblpref.'code'); ?></td> 
									<td><?php app_value($item,$this->CEI->tblpref.'name'); ?></td> 
									<td><?php app_value($item,$this->CEI->tblpref.'description'); ?></td> 
									<td align="right"><?php  echo number_format($unit_cost,2,'.',','); ?></td> 
									<td><?php app_value($item,$this->CEI->tblpref.'unit_measure'); ?></td> 
									<td align="right"><?php  echo number_format($header_1,2,'.',','); ?></td> 
									<td align="right"><?php  echo number_format($header_2,2,'.',','); ?></td> 
									<td align="right"><?php  echo number_format($header_3,2,'.',','); ?></td> 
									<td align="right"><?php  echo number_format($header_4,2,'.',','); ?></td> 
									<td align="right"><?php  echo number_format($multiplier_product,2,'.',','); ?></td> 
									<td align="right"><?php  echo number_format($asf,2,'.',','); ?></td> 
									<td align="right"><?php  echo number_format($asf_amount,2,'.',','); ?></td> 
									<td align="right"><?php  echo number_format($total,2,'.',','); ?></td>  
								</tr>
								<?php 
							}
							?>
							<tr> 
								<td colspan="4" align="right"><strong>Total:</strong></td>
								<td align="right"><b><?php echo number_format($total_unit_cost,2,'.',',');?></b></td>  
								<td></td>
								<td align="right"><b><?php echo number_format($total_header_1,2,'.',',');?></b></td>   
								<td align="right"><b><?php echo number_format($total_header_2,2,'.',',');?></b></td>   
								<td align="right"><b><?php echo number_format($total_header_3,2,'.',',');?></b></td>   
								<td align="right"><b><?php echo number_format($total_header_4,2,'.',',');?></b></td>   
								<td align="right"><b><?php echo number_format($total_multiplier_product,2,'.',',');?></b></td>   
								<td align="right"><b><?php echo number_format($total_asf,2,'.',',');?></b></td>   
								<td align="right"><b><?php echo number_format($total_asf_amount,2,'.',',');?></b></td>   
								<td align="right"><b><?php echo number_format($total_sum,2,'.',',');?></b></td>   
							</tr>
							<?php
						}
						else{ 
						?>
							<tr>
								<td colspan="14" class="alert alert-warning" style="text-align: center">
									No items or data found.
								</td>
							<tr> 
						<?php
						}						
						?>
					</tbody>
				</table>
				<hr /> 
			</div> <!-- end of .col-sm-9 col-md-10 main -->
		</div> <!-- end of .row -->
		<?php echo form_hidden('id',$id); ?>
	</form> <!-- end of .form -->
</div> <!-- end of .container-fluid -->