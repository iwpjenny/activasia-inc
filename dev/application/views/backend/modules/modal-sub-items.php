<hr />
<?php echo form_open($form_action,array('class'=>'form-horizontal','name'=>'grid_'.$page)); ?>
	<div class="form-group"> 
		<label class="col-sm-3"><h4><?php echo $name; ?> Records</h4></label>
		<div class="col-sm-6 notification_<?php echo $page; ?>"></div> 
		<div class="col-sm-3 text-right"> 
			<?php
			if( $views_editable === TRUE ){
			?>
			<button type="button" class="btn btn-danger btn-sm" onclick="javascript:json_delete_confirmation_multiple_item(this);">Delete Items</button>
			<button type="button" class="btn btn-success btn-sm" onclick="javascript:json_new_item(this);">Add New Sub Item</button>
			<?php
			}
			?>
		</div> 
	</div>
	<div class="table-responsive">
		<table id="grid_<?php echo $page; ?>" border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
			<thead>
			<?php $this->load->view($location.'/modules/'.$model->view.'/items-grid-thead'); ?>
			</thead>
			<tbody>
			<?php $this->load->view($location.'/modules/'.$model->view.'/items-grid-tbody'); ?>
			</tbody>
		</table>
	</div>
	<?php echo form_hidden('id',''); ?>
	<?php echo form_hidden('title',''); ?>
	<?php echo form_hidden('parent_id',$parent_id); ?>
	<?php echo form_hidden('page',$page); ?>
</form>
<!-- Start Modal <?php echo $page; ?> -->
<?php
if( $views_editable === TRUE ){
?>
<div class="modal fade" id="modal_<?php echo $page; ?>" tabindex="-1" role="dialog" aria-labelledby="label_<?php echo $page; ?>">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title" id="label_<?php echo $page; ?>"><?php echo $name; ?></h3>
			</div>
			<div class="modal-body">
				<p>Check the following Checkbox on each records to select an Item to add.</p>
				<?php echo form_open_multipart(site_url('c='.$page),array('class'=>'form-horizontal','name'=>'modal_'.$page)); ?>
					<div class="table-responsive">
						<?php $this->load->view($location.'/modules/'.$model->view.'/items-selection'); ?>
					</div>
					<?php $this->load->view($location.'/edit-form-footer-fields'); ?>
					<?php echo form_hidden('id',''); ?>
					<?php echo form_hidden('parent_id',$parent_id); ?>
					<?php echo form_hidden('page',$page); ?>
					<?php echo form_hidden('redirect_url',url_current()); ?>
					<?php echo form_hidden($tblname.'['.$model_parent->tblid.']',$parent_id); ?>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="javascript:json_add_item(this);">Add Items</button>
			</div>
		</div>
	</div>
</div><!-- .modal -->

<div class="modal fade" id="modal_field_<?php echo $page; ?>" tabindex="-1" role="dialog" aria-labelledby="label_field_<?php echo $page; ?>">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title" id="label_field_<?php echo $page; ?>">Record Details: <?php echo $name; ?></h3>
			</div>
			<div class="modal-body">
				<?php echo form_open_multipart(site_url('c='.$page),array('class'=>'form-horizontal','name'=>'field_'.$page)); ?>
					<?php $this->load->view($location.'/modules/'.$model->view.'/items-fields'); ?>
					<?php $this->load->view($location.'/edit-form-footer-fields'); ?>
					<?php echo form_hidden('id',''); ?>
					<?php echo form_hidden('parent_id',$parent_id); ?>
					<?php echo form_hidden('page',$page); ?>
					<?php echo form_hidden('redirect_url',url_current()); ?>
					<?php echo form_hidden($tblname.'['.$model_parent->tblid.']',$parent_id); ?>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="javascript:json_save_item(this);">Save Data</button>
			</div>
		</div>
	</div>
</div><!-- .modal -->

<div class="modal fade" id="modal_delete_<?php echo $page; ?>" tabindex="-1" role="dialog" aria-labelledby="label_delete_<?php echo $page; ?>">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="label_delete_<?php echo $name; ?>">Are you sure you want to delete this record?</h4>
			</div>
			<div class="modal-body">
				<p>Record ID: <strong id="delete-id"></strong></p>
				<p>Title: <strong id="delete-title"></strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger btn-sm" onclick="javascript:json_delete_confirmed_item(this);">Delete Now</button>
			</div>
			<?php echo form_hidden('page',$page); ?>
		</div>
	</div>
</div><!-- .modal -->

<div class="modal fade" id="modal_delete_multiple_<?php echo $page; ?>" tabindex="-1" role="dialog" aria-labelledby="label_delete_multiple_<?php echo $page; ?>">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="label_delete_multiple_<?php echo $name; ?>">Are you sure you want to delete these records?</h4>
			</div>
			<div class="modal-body">
				<p>Record IDs: <strong id="delete-multiple-id"></strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger btn-sm" onclick="javascript:json_delete_confirmed_multiple_item(this);">Delete Now</button>
			</div>
			<?php echo form_hidden('page',$page); ?>
		</div>
	</div>
</div><!-- .modal -->

<script type="text/javascript">
function json_new_item( obj ){
	var parent_form_obj = $(obj).parents("form");	
	var page = $("input[name='page']",parent_form_obj).val();
	
	if( typeof json_new_item_custom == "function" ){
		json_new_item_custom( page );
	}
	
	$("div#modal_"+page).modal();
}

function json_add_item( obj ){
	var modal_obj = $(obj).parents("div.modal");	
	var form_obj = $("form",modal_obj);
	var page = $("input[name='page']",form_obj).val();
	
	var url = form_obj.attr("action");
	url = url+"&m=json_add";
	var data = form_obj.serialize();

	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: json_update_grid_item
	});
	
	$(modal_obj).modal('hide');
}

function json_update_grid_item( data ){
	if( data.result == true ){
		$("table#grid_"+data.page+" tbody").html(data.modal_selected_content);
		$("div#modal_"+data.page+" div.table-responsive").html(data.modal_selection_content);
	}
	$("div.notification_"+data.page).html(data.notification);
}

function json_delete_item( obj, id ){
	var form_obj = $(obj).parents("form");	
	var page = $("input[name='page']",form_obj).val();
	
	var parent_id = $("input[name='parent_id']",form_obj).val();
	var url = form_obj.attr("action");
	url = url+"&m=json_delete";
	
	var data = {id:id,parent_id:parent_id,page:page};
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: json_update_grid_item
	});
}

function json_delete_confirmation_item( obj, id, title ){
	var form_obj = $(obj).parents("form");	
	var page = $("input[name='page']",form_obj).val();
	
	$("input[name='id']",form_obj).val(id);
	$("strong#delete-id").html(id);
	$("strong#delete-title").html(title);		
	$("div#modal_delete_"+page).modal();
}

function json_delete_confirmed_item( obj ){
	var modal_obj = $(obj).parents("div.modal");
	var page = $("input[name='page']",modal_obj).val();
	
	var form_obj1 = $("form[name='field_"+page+"']");
	var url = form_obj1.attr("action");
	url = url+"&m=json_delete";
	
	var form_obj2 = $("form[name='grid_"+page+"']");
	var id = $("input[name='id']",form_obj2).val();
	var parent_id = $("input[name='parent_id']",form_obj2).val();
	
	var data = {id:id,parent_id:parent_id,page:page};
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: json_update_grid_item
	});
	
	$("input[name='id']",form_obj2).val("");
	$(modal_obj).modal('hide');
}

function json_delete_confirmation_multiple_item( obj ){
	var form_obj = $(obj).parents("form");	
	var page = $("input[name='page']",form_obj).val();
	var checkbox = $("input[name='item_id_array[]']:checked",form_obj);
	var checkbox_length = $("input[name='item_id_array[]']:checked",form_obj).length;
	var item_id, item_id_display = "";
	
	if( checkbox_length > 0 ){
		for(var x=0; x < checkbox_length; x++){
			item_id = $(checkbox[x]).val();
			if( x == 0 ){
				item_id_display += item_id;
			} else {
				item_id_display += ", "+item_id;
			}
		}
		$("strong#delete-multiple-id").html(item_id_display);
		
		$("div#modal_delete_multiple_"+page).modal();
	} else {
		alert("Error: Please select a record to delete.");
	}
}

function json_delete_confirmed_multiple_item( obj ){
	var modal_obj = $(obj).parents("div.modal");
	var page = $("input[name='page']",modal_obj).val();
	var form_obj = $("form[name='grid_"+page+"']");
	
	var url = form_obj.attr("action");
	url = url+"&m=json_delete_multiple";
	var data = form_obj.serialize();
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: json_update_grid_item
	});
	
	$(modal_obj).modal('hide');
}

function json_edit_item( obj, id ){
	var form_obj_grid = $(obj).parents("form");	
	var page = $("input[name='page']",form_obj_grid).val();
	
	var form_obj = $("form[name='field_"+page+"']");
	$("input[name='id']",form_obj).val( id );
	var url = form_obj.attr("action");
	url = url+"&m=json_edit";
	var data = form_obj.serialize();
 	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: json_item_display_field_data
	});
	
	$("div#modal_field_"+page+"").modal();
}

function json_item_display_field_data( data ){
	
	var form_obj = $("form[name='field_"+data.page+"']");
	var item_fields = data.item_fields;
	var main_fields = data.main_fields;
	
	$.each(item_fields, function(key, value){
		if( item_fields.hasOwnProperty(key) ){
			$("input[name='"+key+"']",form_obj).val( value );
		}
	});
	
	$.each(main_fields, function(key, value){
		if( main_fields.hasOwnProperty(key) ){
			$("input[name='fields["+key+"]']",form_obj).val( value );
		}
	});
}

function json_save_item( obj ){
	var modal_obj = $(obj).parents("div.modal");	
	var form_obj = $("form",modal_obj);
	var page = $("input[name='page']",form_obj).val();
	
	var url = form_obj.attr("action");
	url = url+"&m=json_save";
	var data = form_obj.serialize();

	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: json_update_grid_item
	});
	
	$(modal_obj).modal('hide');
}
</script>
<?php
}
?>
<!-- End Modal <?php echo $page; ?> -->