<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
?>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2">
			<!--?php $this->load->view($location.'/modules/barangay/sidebar'); ?-->  
		</aside>
		<div class="col-sm-10"> 
			<?php $this->recordsfilter->show(); ?>
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-6">
						<h1><?php echo $model->name; ?> Records</h1>
					</div> 
					<div class="col-sm-6 text-right">
						<?php btn_records( $page, array('capabilities'=>$capabilities,'ur_name'=>$model->urc_name,'display'=>array('add','trash')) ); ?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th> 
								<th>Title</th>  
								<th>Region</th>  
								<th>Description</th>  
								<th>Created</th>
								<th width="3%"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></th> 
								<th>Option</th> 
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						if( $record_rows ){
							$n=1;
							foreach( $record_rows as $col ){
								$id = get_value($col,$this->Area->tblpref.'id'); 
								$title = get_value($col, $this->Area->tblpref.'title'); 
								$region = get_value($col, $this->Reg->tblpref.'title'); 
								$description = get_value($col, $this->Area->tblpref.'description');  
								$created = get_value($col, $this->Area->tblpref.'created');  
								$published = get_value($col, $this->Area->tblpref.'published');  
								$created_display = datetime($created,$grid_date_time_format); 
								?>
								<tr>
									<td><?php echo $n; ?></td> 
									<td ><?php echo $title; ?></td>   
									<td ><?php echo $region; ?></td>   
									<td><?php echo $description; ?></td> 
									<td title="<?php echo $created; ?>" align="right">
									<small><?php echo $created_display; ?></small>
									</td>
									<td><?php published( $published ); ?></td>
									<td class="text-right">
									<?php	 
									btn_optn_records($page, array(
										'model'=>$model,'fields'=>$col,
										'capabilities'=>$capabilities,
										'id'=>$id,
										'display'=>array('view','edit','trash')
									));
									?>
									</td>  
								</tr>
								<?php
								$n++;
							}
						} 
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
			<?php $this->load->view($location.'/modules/records-footer-pagination'); ?> 
			</form> <!-- end of form -->
		</div> <!-- end of .col-bm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->