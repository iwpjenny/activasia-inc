<div class="list-group">
	<?php 
	if( $this->usersrole->check( $this->User->urc_name, 'view' ) ){
		?>
		<li class="list-group-item"><h4 class="list-group-item-heading">User Menu</h4></li>
		<?php
	}
	if( $this->usersrole->check( $this->User->page, 'view' ) ){
		?>
		<a href="<?php echo site_url('c=user'); ?>" class="list-group-item default">
		<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
			<strong class="list-group-item-heading">User</strong> 
		</a>
		<?php
	}
	if( $this->usersrole->check( $this->User->page, 'add' ) ){
		?>
		<a href="<?php echo site_url('c=user&m=edit'); ?>" class="list-group-item default">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Add New</strong> 
		</a>
		<?php
	}  
	
	if( $this->usersrole->check( $this->UL->urc_name, 'view' ) ){
		?>
		<a href="<?php echo site_url('c=userlogs'); ?>" class="list-group-item default">
			<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
			<strong class="list-group-item-heading">User Logs</strong> 
		</a>
		<?php
	}
	if( $this->usersrole->check( $this->Brgy->urc_name, 'view' ) ){
		?>
		<a href="<?php echo site_url('c=ModBarangay'); ?>" class="list-group-item default">
			<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Barangay</strong> 
		</a>
		<?php
	}		
	?>
</div> 
<!--?php $this->load->view($location.'/memberuserrole/sidebar'); ?--> 