<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_date_format = app_get_val($settings['date_and_time'],'grid_date_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit');  
?>
<div class="container-fluid">
	<div class="row">
		<!--aside class="col-sm-2">
			<!--?php $this->load->view($location.'/modules/barangay/sidebar'); ?-->  
		<!--/aside!-->
		<div class="col-sm-12"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $model->name; ?> Records</h1> 
					</div> 
					<div class="col-sm-6 text-right"> 
						<?php btn_records( $page, array('capabilities'=>$capabilities,'ur_name'=>$model->urc_name,'display'=>array()) );  
						?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th>  
								<th>CA #</th>
								<th>Requested</th>
								<th>Employee</th>
								<th>BCS No.</th>
								<th width="20%">Project Name</th>  
								<th>User</th>  
								<th width="5%">Type</th>  
								<th>Created</th>  
								<th>Option</th> 
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						if( $record_rows ){
							$n=1;
							foreach( $record_rows as $col ){ 
								$id = get_value($col,$this->CAL->tblpref.'id'); 
								$logs_type = get_value($col,$this->CAL->tblpref.'type'); 
								$logs_user_id = get_value($col,$this->CAL->tblpref.'user_id'); 
								$logs_created = get_value($col,$this->CAL->tblpref.'created'); 
								$fullname = $this->moduser->get_user_fullname_by_id( $logs_user_id, '{lastname}, {firstname}');
								$ca_id = get_value($col, $this->CA->tblpref.'id');
								$wb_id = get_value($col, $this->WB->tblpref.'id');
								$wb_no = get_value($col, $this->WB->tblpref.'number');
								$date_requested = get_value($col, $this->CA->tblpref.'date_requested');
								$ca_number = get_value($col, $this->CA->tblpref.'ca_number'); 
								$user_created = get_value($col, $this->CA->tblpref.'created_by_user_id');
								$log_fullname = $this->moduser->get_user_fullname_by_id( $user_created, '{lastname}, {firstname}'); 
								$area_id = get_value($col, $this->Area->tblid);
								$area_title = get_value($col, $this->Area->tblpref.'title');  
								$activity = get_value($col, $this->CE->tblpref.'activity');
								$project_name = get_value($col, $this->CE->tblpref.'project_name');
								$ca_amount = get_value($col, $this->CA->tblpref.'ca_amount');  
								$date_released = get_value($col, $this->CA->tblpref.'date_released');
								$created = get_value($col, $this->CA->tblpref.'created');  
								$status = get_value($col, $this->CA->tblpref.'status');
								$item = get_value( $col, $this->WB->tblpref.'item' ); 
								$activity_display = character_limiter($project_name,$grid_text_limit); 
								$created_display = datetime($created,$grid_date_time_format); 
								$date_requested_display = datetime($date_requested,$grid_date_format); 
								if( $date_released <> '0000-00-00' ){
									$date_released_display = datetime($date_released,$grid_date_format);
								} else {
									$date_released_display = 'Unreleased';
								}
								
								$approve_only_position = TRUE;
								$can_approve_all = TRUE;
								$can_approve_self = TRUE;
								$logs_created_display = datetime($logs_created,$grid_date_time_format);	

								?>
								<tr>
									<td><?php echo $n; ?></td> 
									<td><?php $this->CA->get_data_link( $ca_id, $ca_number); ?></td> 
									<td><?php echo $date_requested_display; ?></td>
									<td >
										<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$logs_user_id); ?>" target="_blank"><?php echo ucwords($fullname); ?></a> 
									</td>  								    
									<td><?php $this->WB->get_data_link( $wb_id, $wb_no); ?></td>
									<td title="project_name"><?php echo $activity_display; ?></td>  
									<td >
										<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$user_created); ?>" target="_blank"><?php echo ucwords($log_fullname); ?></a> 
									</td>  	
									<td><?php echo $logs_type; ?></td>
									<td><?php echo $logs_created_display; ?></td>
									<td class="text-right">
									<?php	 
									btn_optn_records($page, array(
										'capabilities'=>$capabilities,
										'ur_name'=>$this->CA->urc_name,
										'id'=>$id,
										'display'=>array('view' )
									));
									?>
									</td>  
								</tr>
								<?php
								$n++;
							}
						} 
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
			<?php $this->load->view($location.'/modules/records-footer-pagination'); ?> 
			</form> <!-- end of form -->
		</div> <!-- end of .col-bm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->