<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_date_format = app_get_val($settings['date_and_time'],'grid_date_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
$user_id = app_get_value($fields,$this->User->tblid);
?>

<div class="container-fluid">
	<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?> 
		<div class="row">
			<!--aside class="col-sm-2 sidebar">    
				<?php $this->load->view($location.'/leftmenu'); ?>
			<!--/aside!-->
			<div class="col-md-12 main">
				<div class="row" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $model->name; ?> View</h1>
					</div>
					<div class="col-sm-6 text-right">
						<?php btn_view( $page, array('model'=>$model,'fields'=>$fields,'capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$model->urc_name, 'display'=>array('back')) ); ?>
					</div>
				</div>
				<?php $this->notify->show(); ?>
			<div class="form-group">
					<label class="col-sm-2">CA Number:</label>
					<div class="col-sm-4"> 
						<?php echo app_value($fields, $this->CA->tblpref.'ca_number'); ?>
					</div> 
					<label class="col-sm-2">CE Number:</label>
					<div class="col-sm-4"> 
						<?php echo app_value($fields, $this->CE->tblpref.'number'); ?>
					</div> 
				</div>  
				<div class="form-group">
					<label class="col-sm-2">Project Name:</label>
					<div class="col-sm-4"> 
						<?php echo app_value($fields, $this->CE->tblpref.'project_name'); ?>
					</div>
					<label class="col-sm-2">Budget Avail. Balance:</label>
					<div class="col-sm-4" id="balance"> 
						<?php echo number_format($balance,2,'.',','); ?>
					</div>
				</div>  
				<div class="form-group">
					<label class="col-sm-2">Employee:</label>
					<div class="col-sm-4">   
						<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$user_id1); ?>" target="_blank"><?php echo ucwords($name_of_employee); ?></a> 
					</div>
					<label class="col-sm-2">Bank Account:</label>
					<div class="col-sm-4"> 
						<?php echo app_value($user_details, $this->User->tblpref.'bank_account'); ?>
					</div>
				</div>   
				<div class="form-group">
					<label class="col-sm-2">Date Requested:</label>
					<div class="col-sm-4"> 
						<?php echo app_value($fields, $this->CA->tblpref.'date_requested'); ?>
					</div>
					<label class="col-sm-2">Date Released:</label>
					<div class="col-sm-4"> 
						<?php 
						if( $status == 'Released' ){
							echo datetime($date_released,$grid_date_format); 
						} else {
							echo 'Unreleased';
						}
						?>
					</div>
				</div>  
				<div class="form-group">
					<label class="col-sm-2">Status:</label>
					<div class="col-sm-4"> 
						<?php echo $status; ?> 
					</div>
				 
				</div>  
				<div class="form-group">
					<label class="col-sm-2">CA Total:</label>
					<div class="col-sm-4" id="ca_total"> 
						<?php echo number_format($ca_amount,2,'.',','); ?>
					</div>
					<label class="col-sm-2">CA Limit:</label>
					<div class="col-sm-4"> 
						<?php 
						$ca_limit = get_value($fields,$this->WB->tblpref.'bcs');
						echo number_format($ca_limit,'2','.',','); 
						?>
					</div>
				</div>  
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Log Updated: </div>
					<div class="col-xs-6 col-sm-4"> 
						<b><?php echo datetime($ca_latest_status_created,'M j, Y \a\t H:ia'); ?></b>
					</div> 
					<div class="col-xs-6 col-sm-2">Log Updated By:</div>
					<div class="col-xs-6 col-sm-4"> 
						<b>
							<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$ca_status_user_id );?>" target="_blank" ><?php echo $ca_latest_status_user.'</a> ('.$ca_latest_status_user_activity.')'; ?>
						</b>
					</div> 
				</div>  
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Log Status: </div>
					<div class="col-xs-6 col-sm-4"> 
						<b><?php echo $ca_latest_status_user_activity; ?></b>
					</div> 
					<div class="col-xs-6 col-sm-2">Created By:</div>
					<div class="col-xs-6 col-sm-4"> 
						<b>
							<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$operator_id );?>" target="_blank" ><?php echo ucwords($operator_fullname);?></a>
						</b>
					</div> 
				</div>  
				<hr />
				<?php echo $sub_section_1; ?> 
			</div> <!-- end of .col-sm-9 col-md-10 main --> 
		</div> <!-- end of .row -->
		<?php echo form_hidden('id',$id); ?> 
	</form> <!-- end of .form -->
</div> <!-- end of .container-fluid -->