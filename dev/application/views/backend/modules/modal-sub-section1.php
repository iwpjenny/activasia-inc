<?php echo form_open($form_action,array('class'=>'form-horizontal','name'=>'grid_'.$page)); ?>
	<div class="form-group"> 
		<label class="col-sm-2"><?php echo $name; ?> Records</label>
		<div class="col-sm-4"> 
		<?php
			if( $views_editable === TRUE ){
		?>
			<button type="button" class="btn btn-success btn-sm" onclick="javascript:new_record('<?php echo $page; ?>');">Add New</button>
		<?php
			}
		?>
		</div> 
	</div>
	<div class="table-responsive">
		<table id="grid_<?php echo $page; ?>" border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
			<thead>
			<?php echo $thead; ?>
			</thead>
			<tbody>
			<?php echo $tbody; ?>
			</tbody>
		</table>
	</div>
</form>
<!-- Start Modal <?php echo $page; ?> -->
<div class="modal fade" id="modal_<?php echo $page; ?>" tabindex="-1" role="dialog" aria-labelledby="modal_<?php echo $page; ?>">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title"><?php echo $name; ?> Records</h3>
			</div>
			<div class="modal-body">
				<?php echo form_open_multipart(site_url('c='.$page),array('class'=>'form-horizontal','name'=>'field_'.$page)); ?>
					<?php $this->load->view($location.'/modules/'.$model->view.'/fields'); ?>
					<?php $this->load->view($location.'/edit-form-footer-fields'); ?>
					<?php echo form_hidden('parent_id',$parent_id); ?>
					<?php echo form_hidden('id',$id); ?>
					<?php echo form_hidden('redirect_url',url_current()); ?>
					<?php echo form_hidden($tblname.'['.$model_parent->tblid.']',$parent_id); ?>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="javascript:save_record('<?php echo $page; ?>');">Save Data</button>
			</div>
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .modal -->
<script type="text/javascript">
function new_record( page ){
	var form_obj = $("form[name='field_"+page+"']");	
	$("input[name='id']",form_obj).val("");
	$("input.fields",form_obj).val("");
	if( typeof json_update_popup_content == "function" ){
		json_update_popup_content( page );
	}
		
	$("div#modal_"+page+"").modal();
}

function save_record( page ){
	var form_obj = $("form[name='field_"+page+"']");
	var url = form_obj.attr("action");
	url = url+"&m=json_save";
	var data = form_obj.serialize();

	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: update_grid
	});
	
	$("div#modal_"+page+"").modal('hide');
}

function update_grid( data ){	
	if( data.result == true ){
		$("table#grid_"+data.page+" tbody").html(data.tbody); 
	}
	$("div#notify").html(data.notification);
	$("#ModCashAdvance_form .form-group div#balance").html(data.balance);
	$("#ModCashAdvance_form .form-group div#ca_total").html(data.ca_total);
}

function edit_record( page, id ){
	var form_obj = $("form[name='field_"+page+"']");
	$("input[name='id']",form_obj).val( id );
	var url = form_obj.attr("action");
	url = url+"&m=json_display_record";
	var data = form_obj.serialize();
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: display_field_details
	});
	
	$("div#modal_"+page+"").modal();
}

function display_field_data( data ){
	
	var form_obj = $("form[name='field_"+data.page+"']");
	var fields = data.field;
	
	if( fields.length > 0 ){
		for(var key in fields){
			$("#"+key,form_obj).val( fields[key] );
		}
	}
}

function delete_record( page, id ){
	var form_obj = $("form[name='field_"+page+"']");
	var parent_id = $("input[name='parent_id']",form_obj).val();
	var url = form_obj.attr("action");
	url = url+"&m=json_delete";
	
	var data = {id:id,parent_id:parent_id,page:page};
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: update_grid
	});
}

function trash_record( page, id ){
	var form_obj = $("form[name='field_"+page+"']");
	var parent_id = $("input[name='parent_id']",form_obj).val();
	var url = form_obj.attr("action");
	url = url+"&m=json_trash";
	
	var data = {id:id,parent_id:parent_id,page:page};
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: update_grid
	});
}

function untrash_record( page, id ){
	var form_obj = $("form[name='field_"+page+"']");
	var parent_id = $("input[name='parent_id']",form_obj).val();
	var url = form_obj.attr("action");
	url = url+"&m=json_untrash";
	
	var data = {id:id,parent_id:parent_id,page:page};
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url,
		data: data,
		success: update_grid
	});
}
</script>
<!-- End Modal <?php echo $page; ?> -->