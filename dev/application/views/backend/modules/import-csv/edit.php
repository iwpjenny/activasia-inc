<?php 
$filepath = app_get_val($fields,$model->tblpref.'filepath'); 
$file_pathinfo = pathinfo($filepath);

$download = $this->usersrole->get_capability($capabilities, $model->urc_name, 'download');
$download_url = site_url('c='.$page.'&m=download&id='.$id);
?>
<div class="container-fluid">
	<div class="row">    
		<aside class="col-sm-2">
			<?php //$this->load->view($location.'/sidebar'); ?>		
		</aside>
		<div class="col-sm-10"> 
			<?php echo form_open_multipart($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form','enctype'=>'multipart/form-data')); ?> 
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $model->name; ?> Edit</h1>
					</div>
					<div class="col-sm-6 text-right">
						<?php btn_edit( $page, array('capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$model->urc_name, 'display'=>array('back','view','edit','delete')) ); ?>
						<?php
						if( $download ){
						?>
						<a class="btn btn-success btn-sm" href="<?php echo $download_url; ?>"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> Download</a>
						<?php
						}
						?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (<span class="red">*</span>) are required fields and must not be blank.</h6>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">User:</label>
					<div class="col-sm-4">
						<?php app_value($fields,$this->User->tblpref.'lastname'); ?>, 
						<?php app_value($fields,$this->User->tblpref.'firstname'); ?>
					</div> 
					<label class="col-sm-2">Module Type:</label>
					<div class="col-sm-4">
						<?php app_value($fields,$model->tblpref.'module_type'); ?>
					</div> 
				</div>
				<div class="form-group">
					<label class="col-sm-2">Type:</label>
					<div class="col-sm-4">
						<?php app_value($fields,$model->tblpref.'type'); ?>
					</div> 
					<label class="col-sm-2">File Name:</label>
					<div class="col-sm-4">
						<?php app_value($file_pathinfo,'basename'); ?>
					</div> 
				</div>
				<div class="form-group">
					<label class="col-sm-2">File Path <span class="red">*</span>:</label>
					<div class="col-sm-10">
						<?php app_value($fields,$model->tblpref.'filepath'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Title <span class="red">*</span>:</label>
					<div class="col-sm-4">
						<?php $this->moduletag->s(array('name'=>'title','holder'=>'Title','req'=>TRUE)); ?>
					</div>
					<label class="col-sm-2">Description :</label>
					<div class="col-sm-4">
						<?php $this->moduletag->s(array('name'=>'description','holder'=>'Description')); ?>
					</div>
				</div>
				<hr />
				<?php $this->load->view($location.'/edit-form-footer-fields'); ?>
				<?php echo form_hidden('id',$id); ?>
			</form>
		</div>
	</div>
</div>