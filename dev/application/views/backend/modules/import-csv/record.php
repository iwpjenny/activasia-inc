<?php
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit');

$upload = $this->usersrole->get_capability($capabilities, $model->urc_name, 'upload');
?>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2">
			<?php //$this->load->view($location.'/sidebar'); ?>	
		</aside>
		<div class="col-sm-10">
			<?php $this->recordsfilter->show(); ?>
			<?php echo form_open_multipart($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-6">
						<h1><?php echo $model->name; ?> Records</h1>
					</div> 
					<?php
					if( $upload ){
					?>
					<div class="col-sm-3">
						<?php $this->files->field(); ?>
					</div>
					<div class="col-sm-3">
						<button type="submit" class="btn btn-success btn-sm js-required-fields">
						<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Upload File
						</button>
					</div>
					<?php
					}
					?>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th> 
								<th>Title</th> 
								<th>File Ext.</th>
								<th>User</th> 
								<th>Created</th>  
								<th>Option</th>  
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						if( $record_rows ){
							$n=1;
							foreach( $record_rows as $col ){
								$id = get_value($col,$model->tblid); 
								$filepath = get_value($col, $model->tblpref.'filepath');
								$title = get_value($col, $model->tblpref.'title');
								$created = get_value($col, $model->tblpref.'created');
								$firstname = get_value($col, $this->User->tblpref.'firstname'); 
								$lastname = get_value($col, $this->User->tblpref.'lastname');
								$created_display = datetime($created,$grid_date_time_format);	
								$title_display = character_limiter($title,$grid_text_limit);
								$file_pathinfo = pathinfo($filepath);
								$basename = app_get_val($file_pathinfo,'basename');
								$extension = strtoupper(app_get_val($file_pathinfo,'extension'));
								
								$download_url = site_url('c='.$page.'&m=download&id='.$id);
								?>
								<tr>
									<td><?php echo $n; ?></td>
									<td title="<?php echo $title; ?>"><?php echo $title_display; ?></td> 
									<td title="<?php echo $basename; ?>"><?php echo $extension; ?></td>
									<td><?php echo $lastname.', '.$firstname; ?></td> 
									<td title="<?php echo $created; ?>" align="right">
										<small><?php echo $created_display; ?></small>
									</td>
									<td class="text-right">
										<?php	 
										btn_optn_records($page, array(
											'capabilities'=>$capabilities,
											'ur_name'=>$model->urc_name,
											'id'=>$id,
											'display'=>array('download','view','edit','delete'),
											'download_url'=>$download_url
										));
										?>
									</td>  
								</tr>
								<?php
								$n++;
							}
						}
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
			<?php $this->load->view($location.'/modules/records-footer-pagination'); ?> 
			</form> <!-- end of form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->