<?php
$user_id = app_get_value($fields,$this->User->tblid);
?>

<div class="container-fluid">
	<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?> 
		<div class="row">
			<!--aside class="col-sm-2 sidebar">    
				<!--?php $this->load->view($location.'/leftmenu'); ?>
			
			</aside!-->
			<div class="col-md-12 main">
				<div class="row" id="header-title-buttons">
					<div class="col-sm-6"> 
						<h1 class="title"><?php echo $this->WBL->name; ?> View</h1> 
					</div>
					<div class="col-sm-6 text-right"> 
						<?php  
						btn_view( $page, array('model'=>$model,'fields'=>$fields,'capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$model->urc_name, 'display'=>array('back')) );
					 	?> 
						
					</div>
				</div> 
				<?php $this->notify->show(); ?>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">CE Number:</div>
					<div class="col-xs-6 col-sm-4"> 
						<b><?php app_value($fields,$this->CE->tblpref.'number'); ?></b>
					</div>  
					<div class="col-xs-6 col-sm-2">BCS Number:</div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->WB->tblpref.'number'); ?></b>
					</div>  
				</div>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Client:</div>
					<div class="col-xs-6 col-sm-4"> 
						<b><?php app_value($fields,$this->CE->tblpref.'client_name'); ?></b>
					</div>  
					<div class="col-xs-6 col-sm-2">Address:</div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->CE->tblpref.'client_address'); ?></b>
					</div>  
				</div>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Total Cost Estimate:</div>
					<div class="col-xs-6 col-sm-4"> 
						<b><?php app_value($fields,$this->WB->tblpref.'cost_estimate'); ?></b>
					</div>  
					<div class="col-xs-6 col-sm-2">ASF:</div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->WB->tblpref.'asf'); ?></b>
					</div>  
				</div>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">BCS:</div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->WB->tblpref.'bcs'); ?></b>
					</div> 
					<div class="col-xs-6 col-sm-2">Gross Profit:</div>
					<div class="col-sm-4"> 
						<b><?php app_value($fields,$this->WB->tblpref.'gross_profit'); ?></b>
					</div> 					
				</div>
				<div class="form-group">  
					<div class="col-xs-6 col-sm-2">Gross Profit%:</div>
					<div class="col-xs-6 col-sm-4">
						<b><?php app_value($fields,$this->WB->tblpref.'gp_percentage'); ?>%</b>
					</div> 
					<div class="col-xs-6 col-sm-2">Total Savings:</div>
					<div class="col-sm-4"> 
						<b><?php app_value($fields,$this->WB->tblpref.'savings_total'); ?></b>
					</div> 					
				</div>
				<div class="form-group">   
					<div class="col-xs-6 col-sm-2">Created: </div>
					<div class="col-sm-4"> 
						<strong><?php app_value($fields,$this->WB->tblpref.'logs_created'); ?></strong>
					</div>					
				</div>
				<hr />  
				<h3>Items</h3> 
				<table class="table items-table table-bordered">
					<thead>	
						<tr>
							<th>No.</th>
							<th>Code</th>
							<th>Particulars</th> 
							<th>Qty</th>
							<th>Unit</th>
							<th>Freq</th> 
							<th>Man Days</th>
							<th>Unit Cost</th>
							<th>Total</th> 
							<th>BCS W/ EWT</th> 
							<th>Savings</th> 
						<tr>
					</thead> 
					<tbody> 
						<?php 
						if($items){
							$total_unit_cost = 0;
							$total_header_1 = 0;
							$total_header_2 = 0;
							$total_header_3 = 0;
							$total_header_4 = 0;
							$total_multiplier_product = 0;
							$total_bcs_ewt = 0;
							$i = 1;
							foreach($items as $item){
								$unit_cost =get_value($item,$this->WBI->tblpref.'unit_cost');
								$header_1 =get_value($item,$this->WBI->tblpref.'header_1');
								$header_2 =get_value($item,$this->WBI->tblpref.'header_2');
								$header_3 =get_value($item,$this->WBI->tblpref.'header_3');
								$header_4 =get_value($item,$this->WBI->tblpref.'header_4');
								$multiplier_product =get_value($item,$this->WBI->tblpref.'multiplier_product');
								$bcs_ewt =get_value($item,$this->WBI->tblpref.'bcs_ewt');
								
								$total_unit_cost +=$unit_cost;
								$total_header_1 +=$header_1;
								$total_header_2 +=$header_2;
								$total_header_3 +=$header_3;
								$total_header_4 +=$header_4;
								$total_multiplier_product +=$multiplier_product;
								$total_bcs_ewt +=$bcs_ewt;
								?>	
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php app_value($item,$this->WBI->tblpref.'code'); ?></td>
									<td><?php app_value($item,$this->WBI->tblpref.'name'); ?></td>
									<td><?php app_value($item,$this->WBI->tblpref.'description'); ?></td>
									<td align="right"><?php echo number_format($unit_cost,2,'.',','); ?></td>
									<td><?php app_value($item,$this->WBI->tblpref.'unit_measure'); ?></td> 
									<td align="right"><?php echo number_format($header_1,2,'.',','); ?></td>
									<td align="right"><?php echo number_format($header_2,2,'.',','); ?></td>
									<td align="right"><?php echo number_format($header_3,2,'.',','); ?></td>
									<td align="right"><?php echo number_format($header_4,2,'.',','); ?></td> 
									<td align="right"><?php echo number_format($multiplier_product,2,'.',','); ?></td> 
									<td align="right"><?php echo number_format($bcs_ewt,2,'.',','); ?></td>  
								</tr>
								<?php
								$i++;
							}
							?>
							<tr>
								<td colspan="4" align="right"><b>Total:</b></td>
								<td align="right"><b><?php echo number_format($total_unit_cost,2,'.',',');?></b></td>  
								<td></td>
								<td align="right"><b><?php echo number_format($total_header_1,2,'.',',');?></b></td>  
								<td align="right"><b><?php echo number_format($total_header_2,2,'.',',');?></b></td>  
								<td align="right"><b><?php echo number_format($total_header_3,2,'.',',');?></b></td>  
								<td align="right"><b><?php echo number_format($total_header_4,2,'.',',');?></b></td>  
								<td align="right"><b><?php echo number_format($total_multiplier_product,2,'.',',');?></b></td>  
								<td align="right"><b><?php echo number_format($total_bcs_ewt,2,'.',',');?></b></td>  
							</tr>
							<?php
						}
						else{ 
						?>
							<tr>
								<td colspan="12" class="alert alert-warning" style="text-align: center">
									No items or data found.
								</td>
							<tr> 
						<?php
						}						
						?>
					</tbody> 
				</table>
				<hr /> 
			</div> <!-- end of .col-sm-9 col-md-10 main -->
		</div> <!-- end of .row -->
		<?php echo form_hidden('id',$id); ?>
	</form> <!-- end of .form -->
</div> <!-- end of .container-fluid -->