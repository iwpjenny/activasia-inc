<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
?>
<div class="container-fluid">
	<div class="row">
		<!--aside class="col-sm-2">
			<!--?php $this->load->view($location.'/modules/barangay/sidebar'); ?-->  
		<!--/aside!-->
		<div class="col-sm-12"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $model->name; ?> Records</h1> 
					</div> 
					<div class="col-sm-6 text-right"> 
						<?php btn_records( $page, array('capabilities'=>$capabilities,'ur_name'=>$this->WBL->urc_name,'display'=>array()) ); 
						 
						?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th> 
								<th>BCS No</th>
								<th>CE No</th>     
								<th width="15%">Client</th>  
								<th>Cost Estimate</th>   
								<th>Total Savings</th>      
								<th>Balance</th>      
								<th>User</th>  
								<th>Type</th>  
								<th>Created</th>  
								<th>Option</th> 
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php 
						if( $record_rows ){ //printx($record_rows);
							$total_ce_amount = 0;
							$total_ca_limit = 0;
							$n=1;
							foreach( $record_rows as $col ){ 
								$id = get_value($col, $this->WBL->tblid );
								$logs_type = get_value($col,$this->WBL->tblpref.'type'); 
								$logs_user_id = get_value($col,$this->WBL->tblpref.'user_id'); 
								$logs_created = get_value($col,$this->WBL->tblpref.'created'); 
								$fullname = $this->moduser->get_user_fullname_by_id( $logs_user_id, '{lastname}, {firstname}');  
								$ce_id = get_value($col, $this->CE->tblid );
								$ce_number = get_value($col, $this->CE->tblpref.'number');
								$wb_id = get_value($col, $this->WB->tblpref.'id');
								$number = get_value($col, $this->WB->tblpref.'number');
								$client_name = get_value($col, $this->CE->tblpref.'client_name');
								$cost_estimate = get_value($col, $this->WB->tblpref.'cost_estimate');
								$savings_total = get_value($col, $this->WB->tblpref.'savings_total'); 
								$cash_advance_limit = get_value($col, $this->WB->tblpref.'bcs');					
								$balance_ca_amount = $this->CA->get_current_ca_balance($id, $cash_advance_limit);

								 
								$logs_created_display = datetime($logs_created,$grid_date_time_format);	
								?>
								<tr>
								
									<td><?php echo $n; ?></td>  
									<td><?php $this->WB->get_data_link( $wb_id, $number); ?></td>    
									<td><?php $this->CE->get_data_link( $ce_id, $ce_number); ?></td>  
									<td><?php echo $client_name; ?></td>  
									<td align="right"><?php echo number_format($cost_estimate,2,'.',',');?></td>
									<td align="right"><?php echo number_format($savings_total,2,'.',',');?></td>
									<td align="right"><?php echo number_format($balance_ca_amount,2,'.',',');?></td>  
									<td >
										<a href="<?php echo site_url('c='.$this->User->page.'&m=view&id='.$logs_user_id); ?>" target="_blank"><?php echo  $fullname; ?></a> 
									</td> 
									<td><?php echo $logs_type;?></td>
									<td><?php echo $logs_created_display;?></td>
									<td class="text-right">
									<?php	 
									btn_optn_records($page, array(
										'capabilities'=>$capabilities,
										'ur_name'=>$this->WB->urc_name,
										'id'=>$id,
										'display'=>array('view')
									)); 
									?> 
									</td>  
								</tr>
								<?php
								$n++;
							}
						} 
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
			<?php $this->load->view($location.'/modules/records-footer-pagination'); ?> 
			</form> <!-- end of form -->
		</div> <!-- end of .col-bm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->