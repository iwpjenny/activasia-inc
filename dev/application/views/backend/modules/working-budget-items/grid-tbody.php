<?php
$view_date_time_format = $this->setconf->get('date_and_time','view_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit');

if( $records ){
	$n=1;
	foreach( $records as $col ){
		$id = get_value($col,$model->tblid);
		$name = get_value($col,$model->tblpref.'name');
		$amount = get_value($col,$model->tblpref.'amount');
		$created = get_value($col,$model->tblpref.'created');

		$title_display = character_limiter($name,$grid_text_limit);						
		$created_display = datetime($created,$view_date_time_format);	
		?>
		<tr>
			<td><?php echo $n; ?></td>
			<td title="<?php echo $name; ?>">
				<?php echo $title_display; ?>
			</td>
			<td> <?php echo $amount; ?> </td>
			<td title="<?php echo $created; ?>" align="right">
				<small><?php echo $created_display; ?></small>
			</td>
			<td class="text-right">
				<?php 
				$params = array('id'=>$id,'capabilities'=>$capabilities,'ur_name'=>$model->urc_name);
				modal_btn_optn_records( $page, $params ); 
				?>
			</td>  
		</tr>
		<?php
		$n++;
	}
}
?>