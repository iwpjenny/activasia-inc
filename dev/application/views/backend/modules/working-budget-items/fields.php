<div class="form-group">
	<div class="col-md-12">
		<h6>The following with (<span class="red">*</span>) are required fields and must not be blank.</h6>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2">Item Name <span class="red">*</span>:</label>
	<div class="col-sm-4">
		<?php $this->moduletag->s(array('name'=>'name','id'=>'name','class'=>'fields','holder'=>'Name','req'=>TRUE)); ?>
	</div>
	<label class="col-sm-2">Amount <span class="red">*</span>:</label>
	<div class="col-sm-4">
		<?php $this->moduletag->s(array('name'=>'amount','id'=>'amount','class'=>'fields','holder'=>'0.00')); ?>
	</div>
</div>
 
<script type="text/javascript">
function display_field_details( data ){
	
	var form_obj = $("form[name='field_"+data.page+"']");
 
	$("input#name",form_obj).val( data.field.name );
	$("input#amount",form_obj).val( data.field.amount ); 
}
</script>