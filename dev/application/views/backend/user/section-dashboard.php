<?php  
$display_latest_users = app_get_val($settings['admin_dashboard'],'display_latest_users'); 
if( $this->usersrole->get_capability($capabilities, $model->urc_name, 'records' ) && (isset($display_latest_users) && $display_latest_users) && $display_latest_users == 'yes' ){
?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel-group basic_reg_form">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="<?php echo site_url('c='.$model->page); ?>">Latest <?php echo $model->name; ?></a>
				</div>
				<div class="panel-body"> 
					<div class="table-responsive">
						<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
							<thead>
								<tr>
									<th>#</th>
									<th>Full Name</th>
									<th>Username</th>
									<th>Role</th>
									<th>Created</th>
									<th>Published</th>
									<th>Option</th> 
								</tr> 
							</thead> <!-- end of thead -->
							<tbody>
							 <?php
								if( $records ){
									$n=1;
									foreach($records as $col){ 
										$id = get_value($col,$model->tblid);
										$firstname = get_value($col,$model->tblpref.'firstname');
										$midname = get_value($col,$model->tblpref.'midname');
										$lastname = get_value($col,$model->tblpref.'lastname');
										$username = get_value($col,$model->tblpref.'username'); 
										$published = get_value($col,$model->tblpref.'published');
										$created = get_value($col,$model->tblpref.'created');
										
										$role = get_value($col,$this->UR->tblpref.'title');
										$role_id = get_value($col,$this->UR->tblid ); 
																	
										$created_display = datetime($created,$grid_date_time_format);			
										?>
										<tr>
											<td><?php echo $n; ?></td>
											<td>
											<?php app_fullname_format( $firstname, $midname, $lastname, '{lastname}, {firstname} {middlename} ' ); ?>
											</td>
											<td><?php echo $username; ?></td>
											<td>
												<?php 
												if( $this->usersrole->get_capability($capabilities, $this->UR->urc_name, 'records' ) || $this->usersrole->get_capability($capabilities, $this->UR->urc_name, 'view' ) ){
													?>
													<a href="<?php echo site_url('c='.$this->UR->page.'&m=edit&id='.$role_id); ?>" target="_blank"><?php echo $role; ?></a>	
													<?php
												} else {
													 echo $role; 
												}
												?>
											</td>
											<td align="right" title="<?php echo $created; ?>">
												<small><?php echo $created_display; ?><small>
											</td>
											<td><?php published( $published ); ?></td> 
											<td class="text-right">
												<?php	 
												btn_optn_records($model->page, array(
													'model'=>$model,'fields'=>$col,
													'capabilities'=>$capabilities,
													'id'=>$id,
													'display'=>array('view','edit','delete')
												));
												?>
											</td> 
										</tr>
										<?php
										$n++;
									}
								} 
								?>
							</tbody> 
						</table> 
					<?php $this->notify->records( $records ); ?>
					</div>  
				</div>
			</div>
		</div>
	</div> 
</div>  
<?php 
}
?>