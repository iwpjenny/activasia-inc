<div class="list-group">
	<?php  
	if( $this->usersrole->check( $model->urc_name, 'records' ) || $this->usersrole->check( $model->urc_name, 'add' ) ){
	?>	
		<li class="list-group-item"><h4 class="list-group-item-heading">User Menu</h4></li>
	<?php 	
	}
	if( $this->usersrole->check( $model->urc_name, 'records' ) ){
	?> 
		<a href="<?php echo site_url('c='.$this->User->page); ?>" class="list-group-item default">
		<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Users</strong>
			<p class="list-group-item-text"><!--List of User Records--></p>
		</a>
	<?php
	}
	if( $this->usersrole->check( $model->urc_name, 'add' ) ){
		?>
		<a href="<?php echo site_url('c='.$this->User->page.'&m=edit'); ?>" class="list-group-item default">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Add New</strong>
			<p class="list-group-item-text"><!-- User Registration Form--> </p>
		</a>
		<?php
	}
	if( $this->usersrole->check( $this->UL->urc_name, 'records' ) ){
		?>
		<a href="<?php echo site_url('c='.$this->UL->page.''); ?>" class="list-group-item default">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Logs</strong>
			<p class="list-group-item-text"><!-- User Registration Form--> </p>
		</a>
		<?php
	}
	?>
</div>
<?php $this->load->view($location.'/userrole/sidebar'); ?> 