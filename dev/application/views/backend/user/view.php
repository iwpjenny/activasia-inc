<?php
$email = app_get_val($fields,$model->tblpref.'email');
?>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2"> 
			<?php 
			if( $this->usersrole->check( $model->urc_name, 'picture' ) ){
				?>
				<div class="list-group">
					<li class="list-group-item"><?php echo $picture; ?></li>
					<li class="list-group-item">
						<strong>
							<?php app_val($fields,$model->tblpref.'firstname'); ?> 
							<?php app_val($fields,$model->tblpref.'lastname'); ?>
						</strong>
					</li>
				</div>
				<?php
			}
			?>
			<?php $this->load->view($location.'/user/sidebar'); ?>
		</aside>
		<div class="col-sm-10">
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
				<div class="row">
					<div class="col-sm-6"> 
						<h1 class="title"><?php echo $model->name; ?> Profile</h1>
					</div>
					<div class="col-sm-6 text-right">
						<?php btn_view( $page, array('model'=>$model,'fields'=>$fields,'capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$model->urc_name) ); ?> 
					</div>
				</div>
				<?php $this->notify->show(); ?>
				<div class="row">
					<div class="col-md-12"> 
						<div class="form-group">
							<div class="col-xs-6 col-sm-2">Full Name:</div>
							<div class="col-xs-6 col-sm-2">
								<b>
								<?php app_val($fields,$model->tblpref.'firstname'); ?>
								<?php app_val($fields,$model->tblpref.'midname'); ?>
								<?php app_val($fields,$model->tblpref.'lastname'); ?>
								</b>
							</div> 
							<div class="col-xs-6 col-sm-2">User Role:</div>
							<div class="col-xs-6 col-sm-2">
								<b>
								<?php 
								if( $this->usersrole->check( $model->urc_name, 'view_user_role' ) ){
									?>
									<a href="<?php echo site_url('c='.$this->UR->page.'&m=edit&id='.$ur_id); ?>" target="_blank">
									<?php app_val($fields,$this->UR->tblpref.'title'); ?></a>	
									<?php
								} else {
									app_val($fields,$this->UR->tblpref.'title');
								}
								?>
								</b>	
							</div> 
						</div>
						<div class="form-group"> 
							<div class="col-xs-6 col-sm-2">Username:</div>
							<div class="col-xs-6 col-sm-2">
								<b><?php app_val($fields,$model->tblpref.'username'); ?></b>
							</div> 
							<div class="col-xs-6 col-sm-2">Email Address:</div>
							<div class="col-xs-6 col-sm-2">
								<b><?php app_set_mailto( $email ); ?></b>
							</div> 
						</div>
						<div class="form-group">
							<div class="col-xs-6 col-sm-2">Address:</div>
							<div class="col-xs-6 col-sm-2">
								<b><?php app_val($fields,$this->UP->tblpref.'address'); ?></b>
							</div>  
							<div class="col-xs-6 col-sm-2">Contact No.:</div>
							<div class="col-xs-6 col-sm-2">
								<b><?php app_val($fields,$this->UP->tblpref.'contact_no'); ?></b>
							</div> 
						</div>
						<?php $this->load->view($location.'/view-footer-fields'); ?>
					</div>
				</div>
				<?php form_hidden('id',$id); ?>
			</form> <!-- end of .form -->
		</div>
	</div>
</div>