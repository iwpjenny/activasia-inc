<?php  
if( $this->usersrole->get_capability($capabilities, $model->urc_name, 'records' ) ){
	$urc_name = $model->urc_name;
	$page = $model->page;
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">User Menu</h3>
	</div>
	<!-- <div class="panel-body">
		System Back-end Users.
	</div> -->
	<div class="list-group">	
		<a href="<?php echo site_url('c='.$page); ?>" class="list-group-item">
			<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Users</strong>
		</a>
		<?php
		if( $this->usersrole->get_capability($capabilities, $urc_name, 'add' ) ){
			?>
			<a href="<?php echo site_url('c='.$page.'&m=edit'); ?>" class="list-group-item">
				<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				<strong class="list-group-item-heading">Add New</strong>
			</a>
			<?php
		}
		if( $this->usersrole->get_capability($capabilities, $urc_name, 'records' ) ){
			?>
			<a href="<?php echo site_url('c='.$this->UL->page.''); ?>" class="list-group-item">
				<span class="glyphicon glyphicon-list" aria-hidden="true"></span>
				<strong class="list-group-item-heading">Logs</strong>
			</a>
			<?php
		}
		?>
	</div>
</div>
<?php  
}
?>