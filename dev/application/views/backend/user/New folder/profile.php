<?php
$view_date_time_format = app_get_val($settings,'view_date_time_format');

$created = app_get_val( $fields, $model->tblpref.'created');
$modified = app_get_val( $fields, $model->tblpref.'modified');

$created_display = datetime($created, $view_date_time_format);
$modified_display = datetime($modified, $view_date_time_format);
?>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2">
			<?php 
			if( $this->usersrole->check( $this->User->urc_name, 'picture' ) ){
				?>
				<div class="list-group">
					<li class="list-group-item"><?php echo $picture; ?></li>
					<li class="list-group-item">
						<strong>
							<?php app_value($fields,$this->User->tblpref.'firstname'); ?> 
							<?php app_value($fields,$this->User->tblpref.'lastname'); ?>
						</strong>
					</li>
				</div>
				<?php
			}
			?> 
			<?php $this->load->view($location.'/user/sidebar'); ?>
		</aside>
		<div class="col-sm-10"> 
			<?php echo form_open_multipart($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?> 
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6"> 
						<h1 class="title"><?php echo $this->User->name; ?> Edit</h1>
					</div>
					<div class="col-sm-6 text-right"> 
						<?php btn_edit( $page, array('capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$this->User->urc_name,'display'=>array('edit')) ); ?> 
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (*) are required fields and must not be blank.</h6>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">User Role *:</label>
					<div class="col-sm-4">
						<?php echo $select_role; ?>
					</div>
					<label class="col-sm-2">Email *:</label>
					<div class="col-sm-4">
						<?php app_field('email',$this->User,$fields,array('type'=>'email','holder'=>'Email Address [sample@email.com]','attr'=>'pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Username *:</label>
					<div class="col-sm-4">
						<?php app_field('username',$this->User,$fields,array('holder'=>'Username')); ?>
					</div>
					<label class="col-sm-2">First Name *: </label>
					<div class="col-sm-4">
						<?php app_field('firstname',$this->User,$fields,array('class'=>'alphaonly','holder'=>'First Name')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Middle Name:</label>
					<div class="col-sm-4">
						<?php app_field('midname',$this->User,$fields,array('class'=>'alphaonly','holder'=>'Middle Name')); ?>
					</div>
					<label class="col-sm-2">Last Name *:</label>
					<div class="col-sm-4">
						<?php app_field('lastname',$this->User,$fields,array('class'=>'alphaonly','holder'=>'Last Name')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Bank Name:</label>
					<div class="col-sm-4">
						<?php app_field('bank_name',$this->User,$fields,array('holder'=>'Bank Name')); ?>
					</div>
					<label class="col-sm-2">Bank Account *:</label>
					<div class="col-sm-4">
						<?php app_field('bank_account',$this->User,$fields,array('holder'=>'Bank Account')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Address:</label>
					<div class="col-sm-4">
						<?php app_field('address',$this->UP,$fields,array('holder'=>'Full Address','req'=>FALSE)); ?>
					</div> 
					<label class="col-sm-2">Contact No:</label>
					<div class="col-sm-4">
						<?php app_field('contact_no',$this->UP,$fields,array('class'=>'numericOnly','holder'=>'Contact Number','req'=>FALSE)); ?>
					</div> 
				</div>
				<?php if( $this->usersrole->check( $this->User->urc_name, 'picture' ) ){ ?>
				<div class="form-group"> 
					<label class="col-sm-2">Picture:</label>
					<div class="col-sm-4"> 
						<?php $this->pic->field(); ?>
					</div> 
				</div>
				<?php } ?>
				<hr />
				<div class="form-group"> 
					<h4 class="col-sm-12">Password Update</h4>
				</div>
				<div class="form-group"> 
					<label class="col-sm-2">Password:</label>
					<div class="col-sm-4">
						<?php app_field('password',$this->User->tblpref,$fields,array('type'=>'password','id'=>'password','holder'=>'Password','has_value'=>FALSE,'req'=>FALSE,'min'=>6,'max'=>15)); ?>
					</div>
					<label class="col-sm-2">Confirm:</label>
					<div class="col-sm-4">
						<?php app_field('confirm_password',$this->User->tblpref,$fields,array('type'=>'password','id'=>'confirm_password','holder'=>'Confirm Password','has_value'=>FALSE,'req'=>FALSE,'min'=>6,'max'=>15)); ?>
					</div>
				</div>
				<hr />	
				<div class="form-group">
					<div class="col-sm-2">Created:</div>
					<div class="col-sm-4">
						<small title="<?php echo $created; ?>"><?php echo $created_display; ?></small>
					</div>
					<div class="col-sm-2">Modified:</div>
					<div class="col-sm-4">
						<small title="<?php echo $modified; ?>"><?php echo $modified_display; ?></small>
					</div>
				</div>
				<hr />
				<!--div class="form-group">
					<div class="col-sm-2">Area:</div>
					<div class="col-sm-8">
						<?php $this->Reg->get_checkbox($id); ?> 
					</div> 
				</div-->
				<?php echo form_hidden('id',$id); ?>
			</form> <!-- end of .form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->
<script type="text/javascript">
var holder_id = "<?php echo $id; ?>";
console.log( holder_id );
if( holder_id != ' ' || holder_id != false ){
	$("input[name=password]").removeAttr( "required" );
	$("input[name=confirm_password]").removeAttr( "required" ); 
}
function readURL(input) {
	if( input.files && input.files[0] ){
		var reader = new FileReader(); 
		reader.onload = function(e){
			$('#display_image').attr( 'src', e.target.result );
			$('#display_image').attr( 'class',"profile-picture" );
		} 
		reader.readAsDataURL(input.files[0]);
	}
}
$('#image').change(function(){
	readURL(this);
	var fileExtension = ['jpg','jpeg','png'];
	if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {	
		alert("Invalid file type");
		$('.btn').attr('disabled','disabled');
	}
	else{ 
		$('.btn').removeAttr('disabled');
	}
}); 

$( "#user_form" ).submit(function( event ) {
  var password = $("input[name='password']").val(); 
  var passwordconfirm = $("input[name='confirm_password']").val();
 
  if(passwordconfirm != password && passwordconfirm.length ){
	  alert("New Password and Password confirmation do not match.");
	  event.preventDefault();
  } 
  else if(password.length && passwordconfirm.length == 0){
	  alert("Input password confirmation.");
	  event.preventDefault();
  }
  
}); 
$(document).ready(function(){ 
	$('#users-menu').removeClass('active');
	$('#users-sub').removeClass('active');
	$('#profile-menu').addClass('active');
});
/* $( ".region-list li" ).click(function() {
  if($(this).find('input').is(':checked') == true){
	  $(this).find('.area-list li input').prop("disabled",false);
	  $(this).find('.area-list li input').prop("checked",true);
  }
  else{
	  $(this).find('.area-list li input').removeAttr("checked");
	  $(this).find('.area-list li input').prop("disabled",true);
	 
  }  
}); */
 $(document).on("change", '.select_area_parent', function(){ 
	var id = $(this).val(); 
	
	 if( $('#check_box_parent_'+id).is(':checked') == true ){
		$('.area_child_'+id ).prop('checked',true);
		 $('.area_child_'+id).removeAttr("disabled");
	 } else {
		$('.area_child_'+id ).prop('checked',false);
		 $('.area_child_'+id).attr("disabled", true);
	 }
});
</script>  
