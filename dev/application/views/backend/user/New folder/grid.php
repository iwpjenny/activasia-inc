<div class="table-responsive">
	<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Full Name</th>
				<th>Username</th>
				<th>Role</th>
				<th>Created</th>
				<th>Published</th>
				<th>Option</th> 
			</tr> 
		</thead> <!-- end of thead -->
		<tbody>
		 <?php
			if( $records ){
				$n=1;
				foreach($records as $col){ 
					$id = get_value($col,$this->User->tblid);
					$firstname = get_value($col,$this->User->tblpref.'firstname');
					$midname = get_value($col,$this->User->tblpref.'midname');
					$lastname = get_value($col,$this->User->tblpref.'lastname');
					$username = get_value($col,$this->User->tblpref.'username'); 
					$published = get_value($col,$this->User->tblpref.'published');
					$created = get_value($col,$this->User->tblpref.'created');
					
					$role = get_value($col,$this->UR->tblpref.'title');
					$role_id = get_value($col,$this->UR->tblid ); 
												
					$created_display = datetime($created,$grid_date_time_format);			
					?>
					<tr>
						<td><?php echo $n; ?></td>
						<td>
						<?php app_fullname_format( $firstname, $midname, $lastname, '{lastname}, {firstname} {middlename} ' ); ?>
						</td>
						<td><?php echo $username; ?></td>
						<td>
							<?php 
							if( $this->usersrole->check( $this->UR->urc_name, 'records' ) || $this->usersrole->check( $this->UR->urc_name, 'view' ) ){
								?>
								<a href="<?php echo site_url('c='.$this->UR->page.'&m=edit&id='.$role_id); ?>" target="_blank"><?php echo $role; ?></a>	
								<?php
							} else {
								 echo $role; 
							}
							?>
						</td>
						<td align="right" title="<?php echo $created; ?>">
							<small><?php echo $created_display; ?><small>
						</td>
						<td><?php published( $published ); ?></td> 
						<td class="text-right">
							<?php	 
							btn_optn_records($this->User->page, array(
								'model'=>$this->User,'fields'=>$col,
								'capabilities'=>$capabilities,
								'id'=>$id,
								'display'=>array('view','edit','delete')
							));
							?>
						</td> 
					</tr>
					<?php
					$n++;
				}
			} 
			?>
		</tbody> 
	</table> 
<?php $this->notify->records( $records ); ?>
</div>  