<?php
if( $this->usersrole->check( $this->User->urc_name, 'records' ) && $display_latest_users ){
?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel-group basic_reg_form">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="<?php echo site_url('c='.$this->User->page); ?>">Latest <?php echo $this->User->name; ?></a>
				</div>
				<div class="panel-body"> 
				<?php $this->load->view($location.'/'.$this->User->view.'/grid'); ?>
				</div>
			</div>
		</div>
	</div> 
</div>  
<?php 
}
?>