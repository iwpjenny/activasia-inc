<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
?>
<div class="container-fluid">
	<?php $this->recordsfilter->show(); ?>
	<div class="row">
		<div class="col-sm-6"> 
			<h1><?php echo $model->name; ?> Records</h1>
		</div>
		<div class="col-sm-6 text-right"> 
			<?php btn_records( $page, array('model'=>$model,'capabilities'=>$capabilities,'display'=>array('add','trash')) ); ?>
		</div> 
	</div> <!-- end of .row -->
	<?php $this->notify->show(); ?>
	<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
		<div class="table-responsive">
			<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<?php 
						if( $this->usersrole->check( $model->urc_name, 'picture' ) ){
							?>
							<th>Picture</th>
							<?php 
						}
						?>
						<th>Full Name</th>
						<th>Username</th>
						<th>Email</th>
						<th>Role</th>
						<th>Created</th>
						<th>Published</th>
						<th>Option</th> 
					</tr>
				</thead> <!-- end of thead -->
				<tbody>
					<?php
					if( $record_rows ){
						$n=1;
						foreach($record_rows as $col){ 
							$id = get_value($col,$model->tblid);
							$firstname = get_value($col,$model->tblpref.'firstname');
							$midname = get_value($col,$model->tblpref.'midname');
							$lastname = get_value($col,$model->tblpref.'lastname');
							$username = get_value($col,$model->tblpref.'username'); 
							$email = get_value($col,$model->tblpref.'email'); 
							$published = get_value($col,$model->tblpref.'published');
							$created = get_value($col,$model->tblpref.'created');
							$role = get_value($col,$this->UR->tblpref.'title');
							$role_id = get_value($col,$this->UR->tblid );
							$picture_src = get_value($col,$this->UP->tblpref.'picture');
							$picture = $this->pic->get_img_by_src( $picture_src );	
														
							$created_display = datetime($created,$grid_date_time_format);
							?>
							<tr>
								<td><?php echo $n; ?></td>
								<?php 
								if( $this->usersrole->check( $model->urc_name, 'picture' ) ){
									?>
									<td><?php echo $picture; ?></td>
									<?php 
								}
								?>
								<td><?php app_fullname_format( $firstname, $midname, $lastname ); ?></td>
								<td><?php echo $username; ?></td>
								<td><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
								<td>
									<?php 
									if( $this->usersrole->check( $this->UR->urc_name, 'records' ) || $this->usersrole->check( $this->UR->urc_name, 'view' ) ){
										?>
										<a href="<?php echo site_url('c='.$this->UR->page.'&m=edit&id='.$role_id); ?>" target="_blank"><?php echo $role; ?></a>	
										<?php
									} else {
										 echo $role; 
									}
									?>
								</td>
								<td align="right" title="<?php echo $created; ?>">
									<small><?php echo $created_display; ?><small>
								</td>
								<td><?php published( $published ); ?></td> 
								<td class="text-right">
									<?php	 
									btn_optn_records($page, array(
										'model'=>$model,'fields'=>$col,
										'capabilities'=>$capabilities,
										'id'=>$id,
										'display'=>array('view','edit','delete')
									));
									?>
								</td> 
							</tr>
							<?php
							$n++;
						}
					} 
					?>
				</tbody> <!-- end of tbody -->
			</table> <!-- end of table -->
		</div> <!-- end of .table-responsive -->
		<?php $this->notify->records( $record_rows ); ?>
		<?php $this->load->view($location.'/modules/records-footer-pagination'); ?> 
	</form> <!-- end of form --> 
</div>  <!-- end of .container-fluid -->