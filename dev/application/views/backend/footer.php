<?php
$dev_copyright = app_get_val($config_developer,'dev_copyright'); 
$dev_developed = app_get_val($config_developer,'dev_developed');    
?>
<hr />
<!-- start: footer -->
<footer class="container-fluid">
	<div class="row">
		<div class="col-sm-6 text-muted">
			<?php echo $dev_copyright; ?>
		</div>
		<div class="col-sm-6 text-right text-muted">
			<?php echo $dev_developed; ?>
		</div>
	</div>
</footer> 
<!-- Modal -->
<div id="modal-cash-advance-status" class="modal fade" role="dialog">
	<div class="modal-dialog"> 
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title text-left" id="modal-title"></h4>
			</div> 
			<form id="ca_modal_status_form" class="form-horizontal" action="" name="_form" method="post" accept-charset="utf-8">
				<div class="modal-body">   
					<div class="form-group"> 
						<label class="col-sm-2">Remarks:</label>
						<div class="col-sm-10"> 
							<textarea type="text" name="remarks" class="form-control" placeholder="Enter Remarks"></textarea>  
							<input type="hidden" name="id" class="form-control"/>  
						</div> <!-- end of .col-sm-4 -->  
					</div>
				</div> 
				<div class="modal-footer">
					<div class="form-group"> 
						<button type="submit" class="btn btn-success">Submit</a>
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
					</div>
				</div>
			</form>
		</div> 
	</div>
</div>
 
<script type="text/javascript">
$('.modal_ca_status').click(function() { 
	var id = $(this).attr('data-id'); 
	var action = $(this).attr('data-url'); 
	var title = $(this).attr('data-title'); 
	 
	$('#ca_modal_status_form').attr('action', action );
	$('#ca_modal_status_form input[name="id"]').val( id );
	$('#modal-cash-advance-status h4#modal-title').html( title );
	
    $('#modal-cash-advance-status').modal('toggle'); 
});
</script>