Hi <strong><?php echo $firstname; ?></strong>,
<br /><br />
You have requested for your password recovery. Here are your new password information:<br />
<strong>Email:</strong> <?php echo $email; ?><br />
<strong>Full Name:</strong> <?php echo $fullname; ?><br /> 
<strong>Username:</strong> <?php echo $username; ?><br />
<strong>New Password:</strong> <?php echo $new_password; ?>
<br /><br /><br />
Regards,<br /><br />
<?php echo $from_name; ?><br />