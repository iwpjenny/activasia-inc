<?php
$current_value_access = $this->usersrole->get_capability($selected_capabilities,'access','full');
?>
<div class="container-fluid">
	<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
		<div class="form-group">
			<div class="col-sm-6">
				<h1 class="title"><?php echo $model->name; ?> Edit</h1>
			</div>
			<div class="col-sm-6 text-right">
				<?php btn_edit( $page, array('model'=>$model,'fields'=>$fields,'capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$model->urc_name, 'display'=>array('back','edit','add','delete')) ); ?>
			</div>
		</div>
		<?php $this->notify->show(); ?>
		<div class="form-group">
			<div class="col-md-12">
				<h6>The following with (*) are required fields and must not be blank.</h6>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2">Title <span class="red">*</span>:</label>
			<div class="col-sm-4">
				<?php app_field('title',$model,$fields,array('holder'=>'Title','req'=>TRUE)); ?>
			</div>
			<label class="col-sm-2">Name <span class="red">*</span>:</label>
			<div class="col-sm-4">
				<?php app_field('name',$model,$fields,array('holder'=>'Name','req'=>TRUE)); ?>
				<small>Value format: small letters only, no space, space must be dash -.</small>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2">Position:</label>
			<div class="col-sm-4">
				<?php app_field('position',$model,$fields,array('holder'=>'Position')); ?>
			</div>
			<label class="col-sm-2">Description:</label>
			<div class="col-sm-4">
				<?php app_field('description',$model,$fields,array('holder'=>'Description')); ?>
			</div>
		</div>
		<?php $this->load->view($location.'/edit-form-footer-fields'); ?>
		<hr />
		<div class="form-group">
			<label class="col-sm-2">Administrator:</label>
			<div class="col-sm-4">
				<input type="checkbox"id="select-all" name="capabilities_selected[access][full]" class="check-all" value="yes" <?php checked($current_value_access,'yes'); ?> /> Full Access
				<small>(Super user only)</small>
			</div>
		</div>
		<hr />
		<?php $model->urc_records( $urc_standard_records, $selected_capabilities ); ?>
		<?php $model->urc_records( $urc_modules_records, $selected_capabilities ); ?>
		<input type="hidden" name="<?php echo $tblid; ?>" value="<?php echo $id; ?>" />
	</form> <!-- end of .form -->
</div> <!-- end of .container-fluid -->
<div id="delete-confirmation" style="display:none;">
	<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&id='.$id); ?>">Yes Delete</a></p>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $("input#select-all").click(function(event) {
        if( this.checked ) {
            $(':checkbox').each(function() {
                this.checked = true;
            });
        } else {
            $(':checkbox').each(function() {
                this.checked = false;
            });
        }
    });
});
</script>

