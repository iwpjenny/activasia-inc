<?php
if( $this->usersrole->check( $model->page, 'records' ) ){
?> 
<div class="list-group"> 
	<li class="list-group-item"><h4 class="list-group-item-heading">User Role Menu</h4></li>
	<a href="<?php echo site_url('c='.$model->page); ?>" class="list-group-item default">
		<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
		<strong class="list-group-item-heading">User Roles</strong>
		<p class="list-group-item-text"><!-- User Role Records --></p>
	</a>
	<?php
	if( $this->usersrole->check( $model->page, 'add' ) ){
		?>
		<a href="<?php echo site_url('c='.$model->page.'&m=edit'); ?>" class="list-group-item default">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Add New</strong>
			<p class="list-group-item-text"><!-- Add New User Role --></p>
		</a>
		<?php
	}
	?>
</div>
<?php
}
?>