<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
?>
<div class="container-fluid">
	<?php $this->recordsfilter->show(); ?>
	<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?>
		<div class="form-group">
			<div class="col-sm-6"> 
				<h1><?php echo $model->name; ?> Records</h1>
			</div>
				<div class="col-sm-6 text-right">
				<?php btn_records( $page, array('model'=>$model,'capabilities'=>$capabilities,'display'=>array('add','trash')) ); ?>
			</div>
		</div>  
		<?php $this->notify->show(); ?>
		<div class="table-responsive">
			<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Position</th>
						<th>Description</th>
						<th>Created</th>
						<th><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></th>
						<th>Option</th>
					</tr>
				</thead> <!-- end of thead -->
				<tbody>
				<?php
				if( $record_rows ){
					$n=1;
					foreach($record_rows as $col){
						$id = get_value($col,$model->tblid);
						$title = get_value($col,$model->tblpref.'title');
						$position = get_value($col,$model->tblpref.'position');
						$description = get_value($col,$model->tblpref.'description');
						$created = get_value($col,$model->tblpref.'created');
						$published = get_value($col,$model->tblpref.'published');
						$created_display = datetime($created,$grid_date_time_format);
						$description_display = character_limiter($description,$grid_text_limit);
						
						$created_display = datetime($created,$grid_date_time_format);
						$title_display = character_limiter($title,$grid_text_limit);
						?>
						<tr>
							<td><?php echo $n; ?></td>
							<td title="<?php echo $title; ?>"><?php echo $title_display; ?></td>
							<td><?php echo $position; ?></td>
							<td title="<?php echo $description; ?>"><?php echo $description_display; ?></td>
							<td title="<?php echo $created; ?>" align="right">
							<small><?php echo $created_display; ?></small>
							</td>
							<td><?php published( $published ); ?></td>
							<td>
							<?php	 
							btn_optn_records($page, array(
								'model'=>$model,'fields'=>$col,
								'capabilities'=>$capabilities,
								'id'=>$id,
								'display'=>array('edit','delete')
							));
							?>
							</td>
						</tr>
						<?php
						$n++;
					}
				}
				?>
				</tbody>
			</table>
		</div>
		<?php $this->notify->records( $record_rows ); ?>
		<?php $this->load->view($location.'/modules/records-footer-pagination'); ?> 
	</form> <!-- end of form -->
</div> <!-- end of .container-fluid -->