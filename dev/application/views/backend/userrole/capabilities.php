<?php
if( $records ){
	$c=1;
	foreach($records as $col1){
		$mod_name = get_value($col1,'name');
		$urc_name = get_value($col1,'urc_name');
		$description = get_value($col1,'description');
		$user_role = get_value($col1,'user_role');
		
		if( $user_role ){
			$n=1; 
			foreach($user_role as $role){
				?>
				<div class="form-group">
					<label class="col-sm-2">
					<?php
					if( $n == 1 ){
						echo $c.'. '.$mod_name.':'; 
					}
					?>
					</label>		
					<?php
					if( $role ){
						foreach($role as $col2){ 
							$field = get_value($col2,'field');
							$name = get_value($col2,'name');
							$value = get_value($col2,'value');
							
							$current_value = $this->usersrole->get_capability($capabilities, $urc_name, $field);
							?>			
							<div class="col-sm-2">
								<label><small>
								<input type="checkbox" name="capabilities_selected[<?php echo $urc_name; ?>][<?php echo $field; ?>]" value="<?php echo $value; ?>" <?php checked($current_value,$value); ?> class="urc <?php echo $urc_name; ?>" /> <?php echo $name; ?>
								</small></label>
								<input type="hidden" name="urc_name" value="<?php echo $urc_name; ?>" />
							</div>
							<?php
						}
					}
					?>
				</div>
				<?php
				$n++;
			}
		}
		?>
		<hr />
		<?php
		$c++;
	}
}
?>