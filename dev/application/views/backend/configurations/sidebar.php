<div class="list-group" style="margin-top:10px;">
	<?php if( $view_settings || $add_settings ){
	?>
	<li class="list-group-item"><h4 class="list-group-item-heading">Configurations Menu</h4></li>
	<?php }  
	if( $view_config ){
	?>
		<a href="<?php echo site_url('c=configurations'); ?>" class="list-group-item default">
		<span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Configurations</strong>
			<p class="list-group-item-text"></p>
		</a>
	<?php } 
	if( $add_config ){
	?>
		<a href="<?php echo site_url('c=configurations&m=edit'); ?>" class="list-group-item default">
		<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Add New</strong>
			<p class="list-group-item-text"></p>
		</a> 
	<?php }  ?>
</div> 
 