<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2">
			
		</aside>
		<div class="col-sm-10"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?> 
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title">Edit Configuration</h1>
					</div>
					<div class="col-sm-6 text-right"> 
						<?php btn_edit( $page, array('capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$this->Config->urc_name,'id_field'=>'set_id', 'display' => array('edit','delete','add')) ); ?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (*) are required fields and must not be blank.</h6>
					</div>
				</div> <!-- end of .form-group -->
				<div class="form-group">
					<label class="col-sm-2">Title *:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="set_title" value="<?php echo $set_title; ?>"  placeholder="Title" required="required" />
					</div> 
					<label class="col-sm-2">Name/Slug *:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="set_name" value="<?php echo $set_name; ?>" placeholder="Name"required="required" />
						<small>Value format: small letters only, no space, space must be dash -.</small>
					</div> 
				</div> <!-- end of .form-group -->
				<div class="form-group">
					<label class="col-sm-2">Parent Settings:</label>
					<div class="col-sm-4">
						<?php echo $set_parent_id; ?>
					</div>
					<label class="col-sm-2">Description:</label>
					<div class="col-sm-4">
						<textarea class="form-control" name="set_description" row="2" placeholder="Description" /><?php echo $set_description; ?></textarea>
					</div> 
				</div> <!-- end of .form-group -->
				<?php
				if( $set_id ){
				?>
					<div class="form-group">
						<div class="col-sm-2">Published</div>
						<div class="col-sm-3">
							<input type="radio" name="set_published" value="1" <?php checked($set_published,1); ?> required="required" /> Yes
							<input type="radio" name="set_published" value="0" <?php checked($set_published,0); ?> required="required" /> No
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2">Created:</div>
						<div class="col-sm-4">
							<small><?php echo $set_created; ?></small>
						</div>
						<div class="col-sm-2">Modified:</div>
						<div class="col-sm-4">
							<small><?php echo $set_modified; ?></small>
						</div>
					</div>
					<input type="hidden" name="set_id" value="<?php echo $set_id; ?>" /> 	 
				<?php
				}
				?>   
			</form> <!-- end of .form --> 
			<?php $this->load->view($location.'/configurations/settings_format'); ?> 
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->  
<script type="text/javascript">
$(document).ready(function(){   
	$( "#settings_form" ).submit(function( event ) {
		var lowerCase= new RegExp('[a-z]');
		var slug = $("input[name='name']").val();  
		 
		if(slug.indexOf(' ') > -1 || slug.replace(/[^A-Z]/g, "").length > 0 )
		{
		  alert('Name contains illegal characters or space.');
		  event.preventDefault();
		}		
	}); 
});
</script> 