<?php 
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit'); 
?>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2 sidebar">
			<?php $this->load->view($location.'/settings/sidebar'); ?>
		</aside>
		<div class="col-sm-10 main">
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'_form','name' =>$page.'_form')); ?> 
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title">Configurations Record</h1>
					</div>
					<div class="col-sm-6 text-right">
						<?php
						if( $this->usersrole->check( $this->Set->urc_name, 'add' ) ){
							?>
							<a class="btn btn-success btn-sm" href="<?php echo site_url('c='.$page.'&m=edit'); ?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New</a>
							<?php
						}
						?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th>
								<th>Title</th>
								<th>Parent</th>
								<th>Name</th>
								<th>Created</th>
								<th width="4%"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></th> 
								<th>Options</th> 
							</tr>
						</thead>
						<tbody>
						<?php
						if( $record_rows ){ //printx($record_rows);
							$n=1;
							foreach($record_rows as $col){
								$id = get_value($col,$this->Set->tblid );
								$parent_title = get_value($col,$this->Set->tblpref.'parent_title');
								$title = get_value($col,$this->Set->tblpref.'title');
								$name = get_value($col,$this->Set->tblpref.'name');
								$created = get_value($col,$this->Set->tblpref.'created');
								$published = get_value($col,$this->Set->tblpref.'published');
								$parent_title_display = character_limiter($parent_title,$grid_text_limit);
								$created_display = datetime($created,$grid_date_time_format);
								$title_display = character_limiter($title,$grid_text_limit);
								?>
								<tr>
									<td><?php echo $n; ?></td>
									<td title="<?php echo $title; ?>"><?php echo $title_display; ?></td>
									<td title="<?php echo $parent_title; ?>"><?php echo $parent_title_display; ?></td>
									<td><?php echo $name; ?></td>
									<td title="<?php echo $created; ?>" align="right">
										<small><?php echo $created_display; ?></small>
									</td>
									<td><?php published( $published ); ?></td> 
									<td class="text-right">
									<?php	 
									btn_optn_records($page, array(
										'capabilities'=>$capabilities,
										'ur_name'=>$this->Config->urc_name,
										'id'=>$id,
										'id_field'=>'set_id',
										'display'=>array('edit','delete')
									));
									?>
									</td> 
								</tr>
								<?php
								$n++;
							}
						}
						?>
						</tbody>
					</table>
				</div>
				<?php $this->notify->records( $record_rows ); ?>
				<nav>
					<?php echo $pagination; ?>
					<div class="showing-result"><?php app_pagination_details($per_page, $n); ?></div>
				</nav>
			</form>
		</div>
	</div>
</div>