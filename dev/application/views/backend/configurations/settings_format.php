<?php
$grid_date_time_format = app_get_val($settings,'grid_date_time_format'); 
$character_limit = app_get_val($settings,'character_limit');

if( $set_id ){
echo form_open($form_action_format_config, array('class'=>'form-horizontal','id' =>$page.'_format_form','name' =>$page.'_format_form')); 
?>
	<div class="form-group" id="header-title-buttons-format">
		<div class="col-sm-6">
			<h1 class="title">Edit Configuration Format</h1>
		</div> 
		<div class="col-sm-6 text-right">
			<?php btn_edit( $page.'&m=edit&set_id='.$set_id, array('capabilities'=>$capabilities,'id'=>$setf_id,'ur_name'=>$this->Config->urc_name,'id_field'=>'set_id','display'=>array('add','edit','delete')) ); ?>
			<?php //btn_edit( $page.'&m=edit&set_id='.$set_id, $setf_id.'_format_form', array('ur_name'=>$this->Config->urc_name) ); ?>
		</div>
	</div>   
	<div class="form-group">
		<div class="col-md-12">
			<h6>The following with (*) are required fields and must not be blank.</h6>
		</div>
	</div> <!-- end of .form-group -->
	<div class="form-group">
		<label class="col-sm-2">Title *:</label>
		<div class="col-sm-4">
			<input type="text" class="form-control" name="setf_title" value="<?php echo $setf_title; ?>"  placeholder="Title" required="required" />
		</div> 
		<label class="col-sm-2">Name/Slug *:</label>
		<div class="col-sm-4">
			<input type="text" class="form-control" name="setf_name" value="<?php echo $setf_name; ?>" placeholder="Name"required="required" /> 
			<small>Value format: small letters only, no space, space must be dash -.</small>
		</div> 
	</div> <!-- end of .form-group -->
	<div class="form-group">
		<label class="col-sm-2">Description:</label>
		<div class="col-sm-4">
			<input type="text" class="form-control" name="setf_description" value="<?php echo $setf_description; ?>"  placeholder="Description"  />
		</div> 
		<label class="col-sm-2">Type *:</label>
		<div class="col-sm-4"> 
			<?php echo $setf_type_choices; ?>
		</div> 
	</div> <!-- end of .form-group --> 
	<div class="form-group"> 
		<div class="col-sm-6" id ="setf_values">
			<div class="row">
				<label class="col-sm-4">Values:</label>
				<div class="col-sm-8">
					<textarea class="form-control" name="setf_values" row="2" placeholder="Values" /><?php echo $setf_values; ?></textarea>
				</div> 
			</div> 
		</div> 
		<div class="col-sm-6" id ="setf_max_char">
			<div class="row">
				<label class="col-sm-4">Max Characters:</label>
				<div class="col-sm-8">
					<input type="number" min='1' class="form-control" name="setf_max_chars" value="<?php echo $setf_max_chars; ?>"  placeholder="Maximum Characters" />
				</div> 
			</div> 
		</div> 
	</div> <!-- end of .form-group --> 
	<div class="form-group"> 
		<div class="col-md-6" id="multiple-records">
			<div class="row">
				<label class="col-sm-4">Multiple Records *:</label>
				<div class="col-sm-8">
					<input type="radio" name="setf_multiple_records" value="1" <?php checked($setf_multiple_records,1); ?> required="required" /> Yes
					<input type="radio" name="setf_multiple_records" value="0" <?php checked($setf_multiple_records,0); ?> required="required" /> No
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<label class="col-sm-4">Default Value:</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="setf_default_value" value="<?php echo $setf_default_value; ?>" placeholder="Default Value"/> 
				</div> 
			</div> 
		</div> 
	</div> <!-- end of .form-group -->
	<input type="hidden" name="set_id" value="<?php echo $set_id; ?>" /> 
	<input type="hidden" name="setf_id" value="<?php echo $setf_id; ?>" />  
	<div class="form-group" id="header-title-buttons">
		<div class="col-sm-6">
			<h1 class="title">Configuration Formats Record</h1>
			<h5><!--?php echo $settings_description; ?--></h5>
		</div>
		<div class="col-sm-6 text-right"> 
			<a class="btn btn-success btn-sm" href="<?php echo site_url('c=configurations&m=edit&set_id='.$set_id); ?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Format</a> 
		</div>
	</div> 
</form> <!-- end of .form -->  
<div class="table-responsive">  
	<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>Name</th>
				<th>Type</th>
				<th>Created</th>
				<th width="4%"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></th>
				 <th>Option</th> 
			</tr>
		</thead> <!-- end of thead -->
		<tbody>
		<?php
		if( $format_settings_table ){
			$n=1;
			foreach($format_settings_table as $col){
				$setf_id = get_value($col,$prefix_config_format.'id');
				$title = get_value($col,$prefix_config_format.'title');
				$name = get_value($col,$prefix_config_format.'name');
				$type = get_value($col,$prefix_config_format.'type');
				$created = get_value($col,$prefix_config_format.'created');
				$published = get_value($col,$prefix_config_format.'published');		
				
				$title_display = character_limiter($title,$character_limit);
				$created_display = datetime($created,$grid_date_time_format);	
				?>
				<tr>
					<td><?php echo $n; ?></td>
					<td title="<?php echo $title; ?>"><?php echo $title_display; ?></td>
					<td><?php echo $name; ?></td>
					<td><?php echo $type; ?></td>
					<td title="<?php echo $created; ?>" align="right">
						<small><?php echo $created_display; ?></small>
					</td>
					<td><?php published( $published ); ?></td> 
					<td class="text-right">
					<?php
					if( $this->usersrole->check( $this->Config->urc_name, 'edit' ) ){
						?>
						<a class="btn btn-primary btn-xs" href="<?php echo site_url('c='.$page.'&m=edit&set_id='.$set_id.'&setf_id='.$setf_id); ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>Edit</a>
						<?php
					}
					if( $this->usersrole->check( $this->Config->urc_name, 'delete' ) ){
						?>
						<a class="btn btn-danger btn-xs confirmation" href="#delete-confirmation-<?php echo $set_id; ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Delete</a>
						<div id="delete-confirmation-<?php echo $set_id; ?>" style="display:none;">
							<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete_format_config&setf_id='.$setf_id.'&set_id='.$set_id); ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Yes Delete</a></p>
						</div>
						<?php
					}
					?>
					</td> 
				</tr>
				<?php
				$n++;
			} /*  end of foreach */
		} else {
			?>
			<tr>
				<td colspan="6">
					<div class="alert alert-warning">No Results Found.</div>
				</td>
			</tr>
			<?php
		}
		?>
		</tbody> <!-- end of tbody -->
	</table> <!-- end of table -->
</div> <!-- end of .table-responsive -->

<?php
}
?> 

<script type="text/javascript">
$(document).ready(function(){     
	compare_type();
	$("select[name='setf_type']").change(function(){
		compare_type();
    });
});
function compare_type(){
	var setf_type = $("select[name='setf_type']").val();
	if(setf_type == 'date' || setf_type == 'datetime'){
		$("#setf_max_char").css("display","none"); 
	}
	if(setf_type == 'text' || setf_type == 'password' || setf_type == 'textarea' || setf_type == 'email'){
		$("#setf_max_char").css("display","block"); 
	}
	if(setf_type == 'checkbox' || setf_type == 'radio' || setf_type == 'select' || setf_type == 'multiselect'){
		$("#setf_values").css("display","block"); 
		$("#setf_max_char").css("display","none"); 
	}
	
	else{
		$("#setf_values").css("display","none");
	}
	if(setf_type == 'checkbox' || setf_type == 'radio' || setf_type == 'multiselect' ){
		$("#multiple-records").css("display","none"); 
	}
	else{
		$("#multiple-records").css("display","block");
	}
	
	
} 
</script> 
