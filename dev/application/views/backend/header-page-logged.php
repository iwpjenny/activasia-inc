<?php 
$meta_title = app_get_val($settings,'meta_title'); 
$client_acronym = app_get_val($config_client,'client_acronym'); 
$meta_title = $meta_title?$meta_title:$client_acronym;
?>
<header class="container-fluid">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span> 
				</button>
				<?php echo $meta_title;?>
				<a class="navbar-brand" href="<?php echo site_url(); ?>"><?php echo $meta_title; ?></a>
			</div> <!-- end of .navbar-header -->
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?php echo site_url(); ?>">Home</a></li>
					<li><a href="<?php echo site_url('c=page&m=about_us'); ?>">About Us</a></li>
					<li class="<?php active($page,$this->BLog->dashboard_page); ?>">						
						<a href="<?php echo site_url('c='.$this->BLog->dashboard_page); ?>">
						<span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span> Dashboard</a>
					</li>
					<li class="<?php active($page,$this->User->page); ?>">						
						<a href="<?php echo site_url('c='.$this->User->page.'&m=profile'); ?>">
						<span class="glyphicon glyphicon-user" aria-hidden="true"></span> My Profile</a>
					</li>
					<li>
						<a href="<?php echo site_url('c='.$this->BLog->page.'&m=logout'); ?>">
						<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Logout</a>
					</li>
				</ul> <!-- end of .nav navbar-nav navbar-right -->
			</div> <!-- end of .navbar-collapse collapse -->
		</div> <!-- end of .container-fluid -->
	</nav> <!-- end of .navbar navbar-inverse navbar-fixed-top -->
</header><!-- end of .container-fluid -->
<!-- end: header -->