<?php
$view_date_time_format = app_get_val($settings,'view_date_time_format');

$created = app_get_val( $fields, $model->tblpref.'created');
$modified = app_get_val( $fields, $model->tblpref.'modified');
$published = app_get_val( $fields, $model->tblpref.'published');

$created_display = datetime($created, $view_date_time_format);
$modified_display = datetime($modified, $view_date_time_format);

$publish_name = get_published_name(); 
?>
<div class="form-group">
	<div class="col-xs-6 col-sm-2"><?php echo $publish_name; ?>:</div>
	<div class="col-xs-6 col-sm-2">
		<strong><?php echo $published?'Yes':'No'; ?></strong>
	</div>
	<div class="col-xs-6 col-sm-2">Created:</div>
	<div class="col-xs-6 col-sm-2">
		<small title="<?php echo $created; ?>"><?php echo $created_display; ?></small>
	</div>
	<div class="col-xs-6 col-sm-2">Modified:</div>
	<div class="col-xs-6 col-sm-2">
		<small title="<?php echo $modified; ?>"><?php echo $modified_display; ?></small>
	</div>
</div>