<?php$client_name = app_get_val($config_client,'client_name'); $client_acronym = app_get_val($config_client,'client_acronym');$dev_copyright = app_get_val($config_developer,'dev_copyright'); $dev_developed = app_get_val($config_developer,'dev_developed'); $dev_app_name = app_get_val($config_developer,'dev_app_name'); $dev_app_acronym = app_get_val($config_developer,'dev_app_acronym'); ?><div class="container">
	<div class="row">
		<h1 class="text-center"><strong><?php echo $dev_app_name; ?> (<?php echo $dev_app_acronym; ?>)</strong></h1>
		<h3 class="text-center"><?php echo $client_name; ?> (<?php echo $client_acronym; ?>)</h3>
	</div>
	<div class="row">
		<div class="col-md-6 text-justify">
			<h3><?php echo $dev_app_acronym; ?> Terms and Conditions of usage:</h3>
			<p>This <?php echo $form_name; ?> is intented for <?php echo $client_acronym; ?> System User only.</p>
			<p>All terms and conditions of <?php echo $dev_app_acronym; ?> Service Agreement apply to this System and any unauthorize access of this System is punishable by the Cyber Crime Law of the Philippine Republic.</p>
			<p>The contents/records displayed are only for <?php echo $client_acronym; ?> Users internal use only.</p>
			<p><?php echo $client_acronym; ?> does not guarantee the 100% performance stability of this System, but <?php echo $client_acronym; ?> will try its best to keep the performance stable.</p>
		</div>
		<div class="col-md-6">
			<h3 class="text-right"><?php echo $form_name; ?></h3>			<?php $this->notify->show(); ?>
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'-form','name' =>$page.'-form')); ?>
				<!--?php app_alerts( array('start'=>'<div class="form-group">','end'=>'</div>') ); ?-->
				<div class="form-group">
					<label>Username:</label>
					<input type="text" class="form-control" name="username" value="<?php echo $username; ?>" placeholder="Enter your Username/Email" required="required" maxlength="50" />
				</div>
				<div class="form-group">
					<label>Password:</label>
					<input type="password" class="form-control" name="password" value="<?php echo $password; ?>" placeholder="Enter your Password" required="required" maxlength="15" />
				</div>
				<div class="form-group">
					<div class="col-md-6">
						<label><input type="checkbox" name="remember_me" value="yes" <?php checked($remember_me,'yes'); ?> /> Remember me?</label>
					</div>
					<div class="col-md-6 text-right">
						<a href="<?php echo site_url('c='.$this->BLog->page.'&m=forgotpassword'); ?>">Retrieve your Password click here</a>
					</div>
				</div> 				<div class="form-group"> 					<div class="col-sm-offset-2 col-sm-10"> 						<?php echo $getWidget;?>					</div>				</div>
				<div class="form-group">
					<button type="submit" id="g-recaptcha" class="btn btn-lg btn-primary btn-block">Login Now</button>					
				</div> <!--end of form-group -->
				<input type="hidden" name="url_current" value="<?php echo $url_current; ?>" />
				<input type="hidden" name="security_key" value="<?php echo $security_key; ?>" />			
			</form>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="col-md-6 text-left">
			<?php echo $dev_copyright; ?>
		</div>
		<div class="col-md-6 text-right">
			<?php echo $dev_developed; ?>
		</div>
	</div>
</div> <!--end of container --> 