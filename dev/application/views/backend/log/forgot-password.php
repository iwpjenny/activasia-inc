<?php
$client_name = app_get_val($config_client,'client_name'); 
$client_acronym = app_get_val($config_client,'client_acronym');
$dev_copyright = app_get_val($config_developer,'dev_copyright'); 
$dev_developed = app_get_val($config_developer,'dev_developed'); 
$dev_app_name = app_get_val($config_developer,'dev_app_name'); 
$dev_app_acronym = app_get_val($config_developer,'dev_app_acronym'); 
?>
<div class="container">
	<div class="row">
		<h1 class="text-center"><strong><?php echo $form_name; ?></strong></h1>
		<h3 class="text-center"><?php echo $client_acronym; ?></h3>
	</div>
	<div class="row">
		<div class="col-md-6 text-justify">
			<h3><?php echo $dev_app_acronym; ?> Terms and Conditions of usage:</h3>
			<p>This <?php echo $form_name; ?> is intented for <?php echo $client_acronym; ?> System User only.</p>
			<p>All terms and conditions of <?php echo $dev_app_acronym; ?> Service Agreement apply to this System and any unauthorize access of this System is punishable by the Cyber Crime Law of the Philippine Republic.</p>
			<p>The contents/records displayed are only for <?php echo $client_acronym; ?> Users internal use only.</p>
			<p><?php echo $client_acronym; ?> does not guarantee the 100% performance stability of this System, but <?php echo $client_acronym; ?> will try its best to keep the performance stable.</p>
		</div>
		<div class="col-md-6">
			<h3 class="text-right"><?php echo $form_name; ?></h3>
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$page.'-form','name' =>$page.'-form')); ?>
				<!--?php app_alerts( array('start'=>'<div class="form-group">','end'=>'</div>') ); ?-->
				<div class="form-group">
					<label>Email Address:</label>
					<input type="email" class="form-control" name="email" value="" placeholder="Enter your Email" required="required" maxlength="50" />
				</div>
				<div class="form-group">
					<div class="col-md-6">
						
					</div>
					<div class="col-md-6 text-right">
						<a href="<?php echo site_url('c='.$this->BLog->page);?>">Click here to go back to the Login Form</a>
					</div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-lg btn-primary btn-block">Submit Email</button>					
				</div>
				<input type="hidden" name="security_key" value="<?php echo $security_key; ?>" />			
			</form>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="col-md-6 text-left">
			<?php echo $dev_copyright; ?>
		</div>
		<div class="col-md-6 text-right">
			<?php echo $dev_developed; ?>
		</div>
	</div>
</div> <!--end of container -->
<script type="text/javascript">  
$(document).ready(function(){
	
});
</script>


