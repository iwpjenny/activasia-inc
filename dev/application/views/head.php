<?php
$meta_title = app_get_val($settings,'meta_title');  
$client_acronym = app_get_val($config_client,'client_acronym'); 
$meta_title = $meta_title?$meta_title:$client_acronym; 
$meta_author = app_get_val($config_developer,'dev_name'); 
$meta_description = app_get_val($config_developer,'dev_description'); 

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="<?php echo $meta_description; ?>" />
	<meta name="author" content="<?php echo $meta_author; ?>" />
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url('includes/images/logo.png'); ?>" />
	<title><?php echo $meta_title; ?></title>
	
	<!-- Bootstrap core CSS -->   
	<link rel="stylesheet" href="<?php echo base_url('includes/bootstrap3.3.6/css/bootstrap.css'); ?>" />  
	<link rel="stylesheet" href="<?php echo base_url('includes/bootstrap3.3.6/css/bootstrap-theme.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('includes/css/generic.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('includes/css/normalize.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('includes/css/glyphicons-halflings.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('includes/css/font-awesome.min.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('includes/css/style.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('includes/css/normalize.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('includes/css/jquery-ui.css'); ?>" />
	
	<!-- MonthPicker -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('includes/js/jquery-ui-month-picker-master/css/MonthPicker.css'); ?>" /> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('includes/js/jquery-timepicker/jquery-ui.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('includes/js/jquery-timepicker/jquery-ui-timepicker-addon.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('includes/js/fancybox/source/jquery.fancybox.css'); ?>" />

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('includes/js/fancybox/source/helpers/jquery.fancybox-buttons.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('includes/js/fancybox/source/helpers/jquery.fancybox-thumbs.css'); ?>" />
		
	<script src="<?php echo base_url('includes/js/jquery-1.11.3.min.js'); ?>"></script>
	<script src="<?php echo base_url('includes/js/numericInput.js'); ?>"></script>
	<script src="<?php echo base_url('includes/js/tel.js'); ?>"></script>
	
	<!-- Start of Custom CSS and JScript -->
	<script type="text/javascript" src="<?php echo base_url('includes/js/jquery-ui.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo base_url('includes/js/jquery.maskedinput.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('includes/js/jquery-ui-month-picker-master/MonthPicker.js'); ?>"></script>
	
	<!-- End MonthPicker -->
	<!--http://api.jqueryui.com/datepicker/-->
	<script type="text/javascript" src="<?php echo base_url('includes/js/jquery-timepicker/jquery-ui-timepicker-addon.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('includes/js/jquery-timepicker/jquery-ui-sliderAccess.js'); ?>"></script>
	<!-- End Datepicker -->
	<script type="text/javascript">
		$(document).ready(function(){
			$( "input.monthpicker" ).MonthPicker({ ShowIcon: false,_isMonthInputType: true });
			
			$( "input.datepicker" ).datepicker({
				dateFormat: "yy-mm-dd"
			});
			
			$( "input.datetimepicker" ).datetimepicker({
				dateFormat: "yy-mm-dd",
				timeFormat: 'HH:mm:ss'
			});
			
			$( "input.timepicker" ).timepicker({
				timeFormat: 'HH:mm:ss'
			});
		});
	</script>
	
	<!-- Fancybox -->
	<script type="text/javascript" src="<?php echo base_url('includes/js/fancybox/lib/jquery.mousewheel-3.0.6.pack.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('includes/js/fancybox/source/jquery.fancybox.pack.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('includes/js/fancybox/source/helpers/jquery.fancybox-buttons.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('includes/js/fancybox/source/helpers/jquery.fancybox-media.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('includes/js/fancybox/source/helpers/jquery.fancybox-thumbs.js'); ?>"></script>
	
	<!--Custom--> 
	<script type="text/javascript" src="<?php echo base_url('includes/js/custom-script.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('includes/js/numericInput.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('includes/js/tel.js'); ?>"></script> 
	
	<script type="text/javascript">
		$(document).ready(function(){
			$(".fancybox").fancybox();
			
			$(".confirmation").fancybox({
				maxWidth	: 360,
				maxHeight	: 120,
				fitToView	: false,
				width		: '50%',
				height		: '40%',
				autoSize	: false,
				closeClick	: false,
				openEffect	: 'none',
				closeEffect	: 'none'
			});
			
			$(".fancybox-view").fancybox({
				maxWidth	: 500,
				maxHeight	: 200,
				fitToView	: false,
				width		: '50%',
				height		: '45%',
				autoSize	: false,
				closeClick	: false,
				openEffect	: 'none',
				closeEffect	: 'none'
			});
		}); 
	</script> 
</head>
<body>  