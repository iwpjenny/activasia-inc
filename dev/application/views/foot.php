<script type="text/javascript" src="<?php echo base_url('includes/bootstrap3.3.6/js/bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('includes/js/modernizr-custom.js'); ?>"></script>
<script src="//www.google.com/recaptcha/api.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    // if (!Modernizr.inputtypes.date) {
        $('input[type=date]').datepicker({
                dateFormat : 'yy-mm-dd'
            }
        );
    // }
});
$(document).ready(function() {
    $('#g-recaptcha').prop('disabled', true);
});
function togglerecaptcha() {
    $('#g-recaptcha').prop('disabled', false);
}
</script>
</body>
</html>
