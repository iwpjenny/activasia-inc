<div class="list-group"> 
	<li class="list-group-item"><h4 class="list-group-item-heading">Coop Menu</h4></li>
	<a href="<?php echo site_url('c='.$this->COOP->page); ?>" class="list-group-item default">
		<span class="badge"><?php  ?></span>
		<span class="glyphicon glyphicon-book" aria-hidden="true"></span> 
		<strong class="list-group-item-heading">Coop Profiles</strong>
	</a> 
	<a href="<?php echo site_url('c='.$this->CUM->page); ?>" class="list-group-item default">
		<span class="badge"><?php  ?></span>
		<span class="glyphicon glyphicon-file" aria-hidden="true"></span> 
		<strong class="list-group-item-heading">Coop Users</strong>
	</a>  
	<a href="<?php echo site_url('c='.$this->CUR->page); ?>" class="list-group-item default">
		<span class="badge"><?php  ?></span>
		<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> 
		<strong class="list-group-item-heading">Coop User Role</strong>
	</a>  
	<a href="<?php echo site_url('c='.$this->TM->page); ?>" class="list-group-item default">
		<span class="badge"><?php  ?></span>
		<span class="glyphicon glyphicon-link" aria-hidden="true"></span> 
		<strong class="list-group-item-heading">Terminals</strong>
	</a> 
	<a href="<?php echo site_url('c='.$this->CUST->page); ?>" class="list-group-item default">
		<span class="badge"><?php  ?></span>
		<span class="glyphicon glyphicon-user" aria-hidden="true"></span> 
		<strong class="list-group-item-heading">Customers</strong>
	</a> 
	<a href="<?php echo site_url('c='.$this->TRM->page); ?>" class="list-group-item default">
		<span class="badge"><?php  ?></span>
		<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> 
		<strong class="list-group-item-heading">Transactions</strong>
	</a>  
</div>
<div class="list-group"> 
	<li class="list-group-item"><h4 class="list-group-item-heading">Uploads Menu</h4></li>
	<a href="<?php echo site_url('c='.$this->UPL->page); ?>" class="list-group-item default">
		<span class="badge"><?php  ?></span>
		<span class="glyphicon glyphicon-transfer" aria-hidden="true"></span> 
		<strong class="list-group-item-heading">XML Upload</strong>
	</a> 
	<a href="<?php echo site_url('c='.$this->UPLD->page); ?>" class="list-group-item default">
		<span class="badge"><?php  ?></span>
		<span class="glyphicon glyphicon-hdd" aria-hidden="true"></span> 
		<strong class="list-group-item-heading">Uploaded XML</strong>
	</a> 
	<a href="<?php echo site_url('c='.$this->PRM->page); ?>" class="list-group-item default">
		<span class="badge"><?php  ?></span>
		<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> 
		<strong class="list-group-item-heading">Processing XML</strong>
	</a>
	<a href="<?php echo site_url('c='.$this->PM->page); ?>" class="list-group-item default">
		<span class="badge"><?php  ?></span>
		<span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> 
		<strong class="list-group-item-heading">Processed XML</strong>
	</a>  
</div>  
<div class="list-group"> 
	<li class="list-group-item"><h4 class="list-group-item-heading">Credit Menu</h4></li>
	<a href="<?php echo site_url('c='.$this->REP->page); ?>" class="list-group-item default">
		<span class="badge"><?php  ?></span>
		<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> 
		<strong class="list-group-item-heading">Credit Report</strong>
	</a> 
 
</div>  