<?php
if( $this->usersrole->check( $this->Trans->urc_name, 'records' ) && $display_latest_transactions ){
?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel-group basic_reg_form">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="<?php echo site_url('c='.$this->Trans->fpage); ?>">Latest <?php echo $this->Trans->name; ?></a>
				</div>
				<div class="panel-body"> 
				<?php $this->load->view($location.'/modules/'.$this->Trans->view.'/grid'); ?>
				</div>
			</div>
		</div>
	</div> 
</div>  
<?php 
}
?>