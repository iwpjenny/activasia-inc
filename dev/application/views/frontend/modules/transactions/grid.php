 <div class="table-responsive">
	<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>Description</th>
				<th>Created</th> 
				<?php 
				if( $this->usersrole->check( $this->Trans->urc_name, 'view' ) ){
					?>
					<th>Option</th> 
					<?php 
				}
				?>
			</tr>
		</thead> <!-- end of thead -->
		<tbody>
		<?php
		if( $records ){
			$n=1;
			foreach( $records as $col ){
				$id = get_value($col, $this->Trans->tblpref.'id'); 
				$title = get_value($col, $this->Trans->tblpref.'title');
				$description = get_value($col, $this->Trans->tblpref.'description');
				$created = get_value($col, $this->Trans->tblpref.'created');
				$created_display = datetime($created,$grid_date_time_format); 
				$title_display = character_limiter($title,$character_limit);
				$description_display = character_limiter($description,$character_limit);
				?>
				<tr>
					<td><?php echo $n; ?></td>
					<td title="<?php echo $title; ?>"><?php echo $title_display; ?></td>
					<td title="<?php echo $description; ?>"><?php echo $description_display; ?></td>
					<td title="<?php echo $created; ?>" align="right"><small><?php echo $created_display; ?></small></td>
					<td class="text-right">
						<?php 
						btn_optn_records($this->Trans->fpage, array(
							'capabilities'=>$capabilities,
							'ur_name'=>$this->Trans->urc_name,
							'id'=>$id,
							'display'=>array('view','edit','delete','trash')
						));
						?>
					</td>					
				</tr>
				<?php
				$n++;
			} /*  end of foreach */
		}
		?>
		</tbody>
	</table>
	<?php $this->notify->records( $records ); ?>
</div>