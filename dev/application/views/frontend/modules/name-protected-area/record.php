<?php
$grid_date_time_format = app_get_val($settings['date_and_time'],'grid_date_time_format');
$grid_text_limit = app_get_val($settings['backend_appearance'],'grid_text_limit');
?>
<div class="container-fluid">
	<div class="row">
		<aside class="col-sm-2">
			<?php $this->load->view($location.'/modules/name-protected-area/sidebar'); ?>  
		</aside>
		<div class="col-sm-10"> 
			<?php echo form_open($form_action, array('class'=>'form-horizontal','id' =>$fpage.'_form','name' =>$fpage.'_form')); ?>
				<div class="form-group">
					<div class="col-sm-6"> 
						<h1 class="title"><?php echo $model->name; ?> Records</h1>
					</div> 
					<div class="col-sm-6 text-right"> 
						<?php btn_records( $fpage, array('capabilities'=>$capabilities,'ur_name'=>$this->NPA->urc_name,'display'=>array('add','trash')) ); ?>
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="table-responsive">
					<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>#</th> 
								<th>Title</th>  
								<th>Description</th>  
								<th>Province</th>  
								<th>Created</th>   
								<th>Option</th> 
							</tr>
						</thead> <!-- end of thead -->
						<tbody>
						<?php
						if( $record_rows ){
							$n=1;
							foreach( $record_rows as $col ){ 
								$id = get_value($col,$this->NPA->tblpref.'id'); 
								$title = get_value($col, $this->NPA->tblpref.'title');
								$description = get_value($col, $this->NPA->tblpref.'description');
								$longitude = get_value($col, $this->NPA->tblpref.'longitude'); 
								$latitude = get_value($col, $this->NPA->tblpref.'latitude');  
								$province = get_value($col, $this->Prov->tblpref.'title');
								$created = get_value($col, $this->Prov->tblpref.'created');
								$created_display = datetime($created,$grid_date_time_format);	
								$title_display = character_limiter($title,$grid_text_limit); 				
							
								?>
								<tr>
									<td><?php echo $n; ?></td> 
									<td title="<?php echo $title; ?>"><?php echo $title_display; ?></td>   
									<td><?php echo $description; ?></td>  
									<td><?php echo $province; ?></td>  
									<td title="<?php echo $created; ?>" align="right">
									<small><?php echo $created_display; ?></small>
									</td>
									<td class="text-right">
									<?php	 
									btn_optn_records($fpage, array(
										'capabilities'=>$capabilities,
										'ur_name'=>$this->NPA->urc_name,
										'id'=>$id,
										'display'=>array('view','edit','delete','trash')
									));
									?>
									</td>  
								</tr>
								<?php
								$n++;
							}
						}
						?>
						</tbody> <!-- end of tbody -->
					</table> <!-- end of table -->
				</div> <!-- end of .table-responsive -->
				<?php $this->notify->records( $record_rows ); ?>
				<nav>
					<?php echo $pagination; ?>
					<div class="showing-result"><?php app_pagination_details($per_page, $n); ?></div>
				</nav> 
			</form> <!-- end of form -->
		</div> <!-- end of .col-sm-9 col-md-10 main -->
	</div> <!-- end of .row -->
</div> <!-- end of .container-fluid -->