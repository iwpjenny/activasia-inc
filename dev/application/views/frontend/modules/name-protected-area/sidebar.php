<div class="list-group">
	<?php 
	if( $this->usersrole->check( $this->NPA->urc_name, 'view' ) ){
		?>
		<li class="list-group-item"><h4 class="list-group-item-heading">User Menu</h4></li>
		<?php
	}
	if( $this->usersrole->check( $this->NPA->urc_name, 'view' ) ){
		?>
		<a href="<?php echo site_url('c='.$this->NPA->fpage); ?>" class="list-group-item default">
			<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
			<strong class="list-group-item-heading"><?php echo $this->NPA->name; ?></strong> 
		</a>
		<?php
	}		
	?>
</div>