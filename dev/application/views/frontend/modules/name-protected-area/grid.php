<div class="table-responsive">
	<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Title</th>  
				<th>Description</th>  
				<th>Province</th>  
				<th>Created</th>   
				<th>Option</th>  
			</tr>
		</thead> <!-- end of thead -->
		<tbody>
		<?php
		if( $records ){
			$n=1;
			foreach( $records as $col ){   
				$id = get_value($col,$this->NPA->tblpref.'id'); 
				$title = get_value($col, $this->NPA->tblpref.'title');
				$description = get_value($col, $this->NPA->tblpref.'description');
				$longitude = get_value($col, $this->NPA->tblpref.'longitude'); 
				$latitude = get_value($col, $this->NPA->tblpref.'latitude');  
				$province = get_value($col, $this->Prov->tblpref.'title');
				$created = get_value($col, $this->NPA->tblpref.'created');
				$created_display = datetime($created,$grid_date_time_format);	
				?>
				<tr>
					<td><?php echo $n; ?></td> 
					<td title="<?php echo $title; ?>"><?php echo character_limiter($title,$character_limit); ?></td>   
					<td><?php echo $description; ?></td>  
					<td><?php echo $province; ?></td>  
					<td title="<?php echo $created; ?>" align="right">
					<small><?php echo $created_display; ?></small>
					</td>
					<td class="text-right">
					<?php	 
					btn_optn_records($this->NPA->fpage, array(
						'capabilities'=>$capabilities,
						'ur_name'=>$this->NPA->urc_name,
						'id'=>$id,
						'display'=>array('view','edit','delete','trash')
					));
					?>
					</td>  
				</tr>
				<?php
				$n++;
			}
		}
		?>
		</tbody> <!-- end of tbody -->
	</table> <!-- end of table -->
	<?php $this->notify->records( $records ); ?>
</div> <!-- end of .table-responsive --> 