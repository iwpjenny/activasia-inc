<?php
if( $this->usersrole->check( $this->NPA->urc_name, 'records' ) && $display_protected_area ){
?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel-group basic_reg_form">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="<?php echo site_url('c='.$this->NPA->fpage); ?>">Latest <?php echo $this->NPA->name; ?></a>
				</div>
				<div class="panel-body"> 
				<?php $this->load->view($location.'/modules/'.$this->NPA->view.'/grid'); ?>
				</div>
			</div>
		</div>
	</div> 
</div>  
<?php 
}
?>