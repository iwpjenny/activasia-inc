<div class="container-fluid">
	<div class="row">    
		<aside class="col-sm-2">
			<?php $this->load->view($location.'/leftmenu'); ?>
			<?php $this->load->view($location.'/modules/'.$view.'/sidebar'); ?> 	
		</aside>
		<div class="col-sm-10"> 
			<?php echo form_open_multipart($form_action, array('class'=>'form-horizontal','id' =>$fpage.'_form','name' =>$fpage.'_form','enctype'=>'multipart/form-data')); ?> 
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6"> 
						<h1 class="title"><?php echo $this->NPA->name; ?> Edit</h1>
					</div>
					<div class="col-sm-6 text-right"> 
						<?php btn_edit( $fpage, array('capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$this->NPA->urc_name) ); ?> 
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<div class="form-group">
					<div class="col-md-12">
						<h6>The following with (<span class="red">*</span>) are required fields and must not be blank.</h6>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Title <span class="red">*</span>:</label>
					<div class="col-sm-4"> 
						<?php $this->moduletag->s(array('name'=>'title','holder'=>'Title','req'=>TRUE )); ?>
					</div>
					<label class="col-sm-2">Description :</label>
					<div class="col-sm-4"> 
						<?php $this->moduletag->s(array('name'=>'description','holder'=>'Description','req'=>FALSE )); ?>
					</div>
				</div>   
				<div class="form-group">
					<label class="col-sm-2">Province <span class="red">*</span>:</label> 
					<div class="col-sm-4">
						<?php echo $select_province; ?>
					</div>   
				</div>   
				<hr />
				<?php $this->load->view($location.'/edit-form-footer-fields'); ?>
				<?php echo form_hidden('id',$id); ?>
			</form>
		</div>
	</div>
</div>