<div class="form-group">
	<div class="col-md-12">
		<h6>The following with (<span class="red">*</span>) are required fields and must not be blank.</h6>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2">Title <span class="red">*</span>:</label>
	<div class="col-sm-4">
		<?php $this->moduletag->s(array('name'=>'title','id'=>'title','class'=>'fields','holder'=>'Title','req'=>TRUE)); ?>
	</div>
	<label class="col-sm-2">Description <span class="red">*</span>:</label>
	<div class="col-sm-4">
		<?php $this->moduletag->s(array('name'=>'description','id'=>'description','class'=>'fields','holder'=>'Description','req'=>TRUE)); ?>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2">Content:</label>
	<div class="col-sm-4">
		<?php $this->moduletag->s(array('name'=>'content','id'=>'content','class'=>'fields','holder'=>'Content')); ?>
	</div>
	<?php
	if( isset($select_transactions) ){
	?>
	<label class="col-sm-2">Parent *:</label>
	<div class="col-sm-4">
		<?php echo $select_transactions; ?>
	</div>
	<?php
	}
	?>
</div>
<div class="form-group">
	<label class="col-sm-2">Question 1 <span class="red">*</span>:</label>
	<div class="col-sm-4">
		<label>
		<?php $this->moduletag->s(array('name'=>'question1','type'=>'radio','value'=>'yes')); ?> Yes
		</label>
		<label>
		<?php $this->moduletag->s(array('name'=>'question1','type'=>'radio','value'=>'no','default'=>TRUE)); ?> No
		</label>
	</div>
	<label class="col-sm-2">Question 2 <span class="red">*</span>:</label>
	<div class="col-sm-4">
		<label>
		<?php $this->moduletag->s(array('name'=>'question2','type'=>'checkbox','value'=>'option-1','default'=>TRUE)); ?> Option 1
		</label>
		<label>
		<?php $this->moduletag->s(array('name'=>'question2','type'=>'checkbox','value'=>'option-2')); ?> Option 2
		</label>
		<label>
		<?php $this->moduletag->s(array('name'=>'question2','type'=>'checkbox','value'=>'option-3')); ?> Option 3
		</label>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2">Question 3 <span class="red">*</span>:</label>
	<div class="col-sm-4">
		<label>
		<?php $this->moduletag->s(array('name'=>'question3','type'=>'radio','value'=>'yes','default'=>TRUE)); ?> Yes
		</label>
		<label>
		<?php $this->moduletag->s(array('name'=>'question3','type'=>'radio','value'=>'no')); ?> No
		</label>
	</div>
	<label class="col-sm-2">Question 4 <span class="red">*</span>:</label>
	<div class="col-sm-4">
		<label>
		<?php $this->moduletag->s(array('name'=>'question4','type'=>'checkbox','value'=>'option-1','default'=>TRUE)); ?> Option 1
		</label>
		<label>
		<?php $this->moduletag->s(array('name'=>'question4','type'=>'checkbox','value'=>'option-2')); ?> Option 2
		</label>
		<label>
		<?php $this->moduletag->s(array('name'=>'question4','type'=>'checkbox','value'=>'option-3')); ?> Option 3
		</label>
	</div>
</div>
<script type="text/javascript">
function display_field_details( data ){
	
	var form_obj = $("form[name='field_"+data.page+"']");
	
	/* for(var key in data.field){
		$("#"+key,form_obj).val( data.field[key] );
	} */
	$("input#title",form_obj).val( data.field.title );
	$("input#description",form_obj).val( data.field.description );
	$("input#content",form_obj).val( data.field.content );
	$("input#question1",form_obj).val( data.field.question1 );
	$("input#question2",form_obj).val( data.field.question2 );
	$("input#question3",form_obj).val( data.field.question3 );
	$("input#question4",form_obj).val( data.field.question4 );
}
</script>