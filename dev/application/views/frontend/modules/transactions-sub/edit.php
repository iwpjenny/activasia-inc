<div class="container-fluid">
	<div class="row">    
		<aside class="col-sm-2">
			<?php $this->load->view($location.'/leftmenu'); ?>
			<?php $this->load->view($location.'/member/sidebar'); ?>			
		</aside>
		<div class="col-sm-10"> 
			<?php echo form_open_multipart($form_action, array('class'=>'form-horizontal','id' =>$fpage.'_form','name' =>$fpage.'_form','enctype'=>'multipart/form-data')); ?> 
				<div class="form-group" id="header-title-buttons">
					<div class="col-sm-6">
						<h1 class="title"><?php echo $this->TransSub->name; ?> Edit</h1>
					</div>
					<div class="col-sm-6 text-right">
						<?php btn_edit( $fpage, array('capabilities'=>$capabilities,'id'=>$id,'ur_name'=>$this->TransSub->urc_name) ); ?> 
					</div>
				</div>  
				<?php $this->notify->show(); ?>
				<?php $this->load->view($location.'/modules/'.$this->TransSub->view.'/fields'); ?>
				<hr />
				<?php $this->load->view($location.'/edit-form-footer-fields'); ?>
				<?php echo form_hidden('id',$id); ?>
			</form>
		</div>
	</div>
</div>