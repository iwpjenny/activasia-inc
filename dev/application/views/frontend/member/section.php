<?php
if( $this->usersrole->check( $this->Mem->urc_name, 'records' ) && $display_latest_users ){
?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel-group basic_reg_form">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="<?php echo site_url('c='.$this->Mem->fpage); ?>">Latest <?php echo $this->Mem->name; ?></a>
				</div>
				<div class="panel-body"> 
				<?php $this->load->view($location.'/'.$this->Mem->view.'/grid'); ?>
				</div>
			</div>
		</div>
	</div> 
</div>  
<?php 
}
?>