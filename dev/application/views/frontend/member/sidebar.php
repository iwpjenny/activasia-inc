<div class="list-group">
	<?php  
	if( $this->usersrole->check( $this->Trans->urc_name, 'records' ) ){
	?>	
		<li class="list-group-item"><h4 class="list-group-item-heading">My Menu</h4></li>
	<?php 	
	}
	if( $this->usersrole->check( $this->Trans->urc_name, 'records' ) ){
	?> 
		<a href="<?php echo site_url('c='.$this->Trans->fpage); ?>" class="list-group-item default">
		<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Transactions</strong>
			<p class="list-group-item-text"><!--List of User Records--></p>
		</a>
	<?php
	}
	if( $this->usersrole->check( $this->TransSub->urc_name, 'records' ) ){
	?> 
		<a href="<?php echo site_url('c='.$this->TransSub->fpage); ?>" class="list-group-item default">
		<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Transactions Sub</strong>
			<p class="list-group-item-text"><!--List of User Records--></p>
		</a>
	<?php
	}
	if( $this->usersrole->check( $this->ML->urc_name, 'records' ) ){
		?>
		<a href="<?php echo site_url('c='.$this->ML->fpage.''); ?>" class="list-group-item default">
			<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Logs</strong>
			<p class="list-group-item-text"><!-- User Registration Form--> </p>
		</a>
		<?php
	}
	?>
</div>