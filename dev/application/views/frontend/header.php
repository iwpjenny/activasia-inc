<?php
$active = 'active';
$meta_title = app_get_val($settings,'meta_title'); 
$client_acronym = app_get_val($config_client,'client_acronym'); 
$meta_title = $meta_title?$meta_title:$client_acronym;

$page_population = 'ModPopulationSurvey';
$page_demographic = 'ModDemographic';
$page_livelihood = 'ModLivelihood';
$page_housing = 'ModHousingHealthSanitation';
$page_datacollection = 'ModDataCollection'; 
$page_protected= 'ModProtectedArea'; 
$page_cave = 'ModCave'; 
$page_island = 'ModIslandWetlands'; 
$page_waterbird = 'ModWaterbirdSpecies'; 
$page_wildlife = 'ModWildlifeViolations'; 
$page_threaten = 'ModThreatenSpecies';

$page_province = 'ModProvince'; 
$page_city = 'ModCity'; 
$page_barangay = 'ModBarangay'; 
$page_sitio = 'ModSitio'; 
$page_name = 'ModNameProtectedArea'; 
$page_tribe = 'ModTribe'; 
$page_penro = 'ModPenro'; 
$page_conservation = 'ModConservationStatus'; 
$page_wetland = 'ModWetlandLocation';

$page_transactions = 'FModTransactions';
$page_transactionssub = 'FModTransactionsSub';
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#profile-menu').removeClass('active');
	var ckeyword_header = '';
	// ckeyword_header = $('input#ckeyword').val();
	ckeyword_header = "<?php if( isset( $keyword ) && $keyword ){ echo $keyword; } else { echo '';}  ?>";
	ckeyword_header = String(ckeyword_header);
	if( ckeyword_header != false ){
		if( ckeyword_header.length > 0  ){
			$('div#check_sort').show();
		} else {
			$('div#check_sort').hide();
		}
	}

});

$('form.search-header').submit(function( event ){
	var ckeyword_header_get = "<?php if( isset( $keyword ) && $keyword ){ echo $keyword; } else { echo '';}  ?>";
	var ckeyword_header = $('input#ckeyword').val();
	var ckeyword_header_trim = ckeyword_header.trim();
	var ckeyword_num_verify = isNaN( ckeyword_header );
	var ckeyword_date_verify = Date.parse(ckeyword_header);
	$('input#ckeyword').val( ckeyword_header_trim );
	if( ckeyword_header_get == false ){
		if( ckeyword_num_verify == false ){
			$('div#check_sort select[name=sort]').val('DESC');
		} else if( ckeyword_date_verify > 0  ){
			$('div#check_sort select[name=sort]').val('DESC');
		} else {
			$('div#check_sort select[name=sort]').val('ASC');
		}

	}
});

$('div#check_sort select[name=sort]').change(function( event ){
	var ckeyword_header = $('input#ckeyword').val();
	var ckeyword_header_trim = ckeyword_header.trim();
	ckeyword_header = String(ckeyword_header);
	if( ckeyword_header.length > 0  ){
		$('input#ckeyword').val( ckeyword_header_trim );
		$('form.search-header').submit();

	}

});
</script>
<!-- start: header -->
<header class="container-fluid">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span> 
				</button>
				<?php echo $meta_title;?>
				<a class="navbar-brand" href="<?php echo site_url(); ?>"><?php echo $meta_title; ?></a>
			</div> <!-- end of .navbar-header -->
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="<?php active($page,$this->FLog->dashboard_page); ?>">
						<a href="<?php site_url('c='.$this->FLog->dashboard_page);?>">
						<span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span> Dashboard</a>
					</li>					
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Modules <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li class="<?php active($page,$page_population); ?>"><a href="<?php echo site_url('c='.$page_population); ?>"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span>Population Survey and Monitoring</a></li> 
							<li style="padding: 10px 0 0px 20px;">Sub Records</li> 
							<li class="<?php active($page,$page_demographic); ?> margin-left-30"><a href="<?php echo site_url('c='.$page_demographic); ?>"><span class="glyphicon glyphicon-th" aria-hidden="true"></span>Demographic Information</a></li>
							<li class="<?php active($page,$page_livelihood); ?> margin-left-30"><a href="<?php echo site_url('c='.$page_livelihood); ?>"><span class="glyphicon glyphicon-th" aria-hidden="true"></span>Livelihood Activities</a></li>
							<li class="<?php active($page,$page_housing); ?> margin-left-30"><a href="<?php echo site_url('c='.$page_housing); ?>"><span class="glyphicon glyphicon-th" aria-hidden="true"></span>Housing Health & Sanitation</a></li>
							<li style="padding: 10px 0 0px 20px;">Reports</li>
							<li class="<?php active($page, $page_population.'&m=summary_barangay'); ?> margin-left-30"><a href="<?php echo site_url('c='.$page_population.'&m=summary_barangay'); ?>"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>Summary per Barangay (Form 2)</a></li>
							<li class="<?php active($page,$page_population.'&m=summary_sheet'); ?> margin-left-30"><a href="<?php echo site_url('c='.$page_population.'&m=summary_sheet'); ?>"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>Summary Sheet (Form 3)</a></li>
							<li class="<?php active($page,$page_population.'&m=tenured_migrants'); ?> margin-left-30"><a href="<?php echo site_url('c='.$page_population.'&m=tenured_migrants'); ?>"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>Official List of Tenured Migrants (Form 5)</a></li>
							<li class="<?php active($page,$page_population.'&m=indigenous_people'); ?> margin-left-30"><a href="<?php echo site_url('c=ModIndigenousPeople'); ?>"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>Questionnaire for Indigenous People (Form 6)</a></li>
							<li role="separator" class="divider"></li>
							<li class="<?php active($page,$page_datacollection); ?>"><a href="<?php echo site_url('c='.$page_datacollection); ?>"><span class="glyphicon glyphicon-save-file" aria-hidden="true"></span>Data Collection</a></li>
							<li role="separator" class="divider"></li>
							<li class="<?php active($page,$page_protected); ?>"><a href="<?php echo site_url('c='.$page_protected); ?>"><span class="glyphicon glyphicon-open" aria-hidden="true"></span>Updating Protected Area Profile and Map</a></li>
							<li role="separator" class="divider"></li>
							<li class="<?php active($page,$page_cave); ?>"><a href="<?php echo site_url('c='.$page_cave); ?>"><span class="glyphicon glyphicon-tent" aria-hidden="true"></span>Classified Caves</a></li>
							<li role="separator" class="divider"></li> 
							<li class="<?php active($page, $page_island); ?>"><a href="<?php echo site_url('c='.$page_island); ?>"><span class="glyphicon glyphicon-tree-deciduous" aria-hidden="true"></span>Priority Island Wetlands</a></li>
							<li role="separator" class="divider"></li>
							<li class="<?php active($page,$page_waterbird); ?>"><a href="<?php echo site_url('c='.$page_waterbird); ?>"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span>Waterbird Population per Species</a></li>
							<li role="separator" class="divider"></li>
							<li class="<?php active($page,$page_wildlife); ?>"><a href="<?php echo site_url('c='.$page_wildlife); ?>"><span class="glyphicon glyphicon-tree-conifer" aria-hidden="true"></span>Wildlife Violations</a></li>
							<li role="separator" class="divider"></li>
							<li class="<?php active($page,$page_threaten ); ?>"><a href="<?php echo site_url('c='.$page_threaten ); ?>"><span class="glyphicon glyphicon-grain" aria-hidden="true"></span>Threaten Species</a></li>
						</ul>
					</li>
					<li class="dropdown <?php if( in_array($page,array($page_transactions,$page_transactionssub,$this->ML->fpage)) ) { echo $active; };  ?>">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Records <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li class="<?php active($page,$this->ML->fpage); ?>"><a href="<?php echo site_url('c='.$this->ML->fpage); ?>"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>My Logs </a></li>
							<li role="separator" class="divider"></li>
							<li class="<?php active($page,$page_transactions); ?>"><a href="<?php echo site_url('c='.$page_transactions); ?>"> <span class="glyphicon glyphicon-list" aria-hidden="true"></span>Transactions</a></li>
							<li class="<?php active($page,$page_transactionssub); ?>"><a href="<?php echo site_url('c='.$page_transactionssub); ?>"> <span class="glyphicon glyphicon-list" aria-hidden="true"></span>Transactions Sub</a></li>
						</ul>
					</li>
					<li class="<?php active($page,'profile'); ?> dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome <?php echo ucwords($logged_firstname); ?>! <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><?php echo $header_menu_logged_user_picture; ?></li>
							<li style="padding: 10px 0 0px 20px;"> <span class="glyphicon glyphicon-education" aria-hidden="true"></span>Role: <?php echo $logged_user_position_title; ?><li>
							<!-- removed style use class -->
							<li role="separator" class="divider"></li>
							<li id="profile-menu" class="<?php active($page,$this->Mem->page); ?>"><a href="<?php echo site_url('c='.$this->Mem->fpage); ?>"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span>My Profile</a></li>
							<li><a href="<?php echo site_url('c='.$this->FLog->page.'&m=logout'); ?>"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>Logout</a></li>
						</ul> <!-- end of .dropdown-menu -->
					</li>
				</ul> <!-- end of .nav navbar-nav navbar-right -->
				<?php
				$check_controller = $this->input->get('c');
				$check_sort = $this->input->get('sort');
				$check_sortby = $this->input->get('sortby');
				$check_sort = $check_sort?$check_sort:'ASC';
				?>
				<?php if( $check_controller && $check_controller != 'bhome' ){
					$check_sort = select_sort( $check_sort, FALSE );
					?>
					<form action="<?php echo $form_action; ?>" method="get" name="search" class="navbar-form navbar-right search-header">
						<div id="check_sort" style="display: none; float: left; padding-right: 5px;">
							<?php echo $check_sort; ?>
						</div>
						<input type="text" id="ckeyword" name="keyword" class="form-control input-sm" placeholder="Type to Search..." value="<?php echo isset($keyword)?$keyword:''; ?>"  />
						<?php
						if( isset($keyword) && $keyword ){
							?>
							<a href="<?php echo site_url('c='.$page); ?>" class="btn btn-default btn-sm" style="float: right;">Reset</a>
							<?php
						}
						?>
						<input type="hidden" name="c" value="<?php echo $page; ?>" />
					</form> <!-- end of form -->
					<?php
				}
				?>
			</div> <!-- end of .navbar-collapse collapse -->
		</div> <!-- end of .container-fluid -->
	</nav> <!-- end of .navbar navbar-inverse navbar-fixed-top -->
</header><!-- end of .container-fluid -->
<!-- end: header -->
