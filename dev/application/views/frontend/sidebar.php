<ul class="list-group">
	<?php
	if( $this->usersrole->check( $this->Mem->urc_name, 'view' ) ){
	?>
		<li class="list-group-item"><h4 class="list-group-item-heading">Member Menu</h4></li>
	<?php 
	}
	if( $this->usersrole->check( $this->ML->urc_name, 'view' ) ){
		?> 
		<a href="<?php echo site_url('c='.$this->ML->page); ?>" class="list-group-item default">
			<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Logs</strong>
			<p class="list-group-item-text"><!-- User Role Records --></p>
		</a>
	<?php 
	}
	if( $this->usersrole->check( $this->Trans->urc_name, 'view' ) ){
		?> 
		<a href="<?php echo site_url('c=modtransactions'); ?>" class="list-group-item default">
			<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
			<strong class="list-group-item-heading">Transactions</strong>
			<p class="list-group-item-text"><!-- User Role Records --></p>
		</a>
	<?php 
	} 
	?>
</ul>