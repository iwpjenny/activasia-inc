<?php
if( $this->usersrole->check( $this->ML->urc_name, 'records' ) && $display_latest_member_logs ){
?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel-group basic_reg_form">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="<?php echo site_url('c='.$this->ML->fpage); ?>">Latest <?php echo $this->ML->name; ?></a>
				</div>
				<div class="panel-body"> 
				<?php $this->load->view($location.'/'.$this->ML->view.'/grid'); ?>
				</div>
			</div>
		</div>
	</div> 
</div>  
<?php 
}
?>