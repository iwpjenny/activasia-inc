<div class="container-fluid">
	<h1 class="title">Dashboard</h1>
	<div class="row">
		<aside class="col-md-2 col-sm-2 col-xs-12"> 
			<?php $this->load->view($location.'/sidebar'); ?>
		</aside>
		<div class="col-md-10">
			<?php $this->notify->show(); ?>
			
			<?php echo $section_npa; ?>
			<?php echo $section_transactions; ?>
			<?php echo $section_member_logs; ?>
		</div>		
	</div>
</div> 