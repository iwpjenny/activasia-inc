<div class="container-fluid" > 
	<div class="row"> 
		<div class="banner-container">
			<div id="myCarousel" class="carousel slide" data-ride="carousel" >
			<div class='background-opacity'></div>
				<ol class="carousel-indicators"> 
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li> 
				</ol>
				<div class="carousel-inner" role="listbox"> 
				
					<div class="item active">
						<img src="<?php echo base_url('includes/images/banner/banner1.jpg'); ?>" alt="" width="460" height="100">
						<div class="carousel-caption">
							<h2> </h2>
						</div>
					</div>
					<div class="item ">
						<img src="<?php echo base_url('includes/images/banner/banner1.jpg'); ?>" alt="" width="460" height="100">
						<div class="carousel-caption">
							<h2> </h2>
						</div>
					</div>
					<div class="item">
						<img src="<?php echo base_url('includes/images/banner/banner1.jpg'); ?>" alt="" width="460" height="100">
						<div class="carousel-caption">
							<h2> </h2>
						</div>
					</div>  
				</div>
				<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
    </div>
	<div class="row">
		<?php 
		$title = $this->setconf->get('home_page','title');
		$sub_title = $this->setconf->get('home_page','sub_title');
		$left_content_text = $this->setconf->get('home_page','left_content_text');
		$left_content_text_2 = $this->setconf->get('home_page','left_content_text_2');
		$left_content_text_3 = $this->setconf->get('home_page','left_content_text_3');
		$left_content_text_4 = $this->setconf->get('home_page','left_content_text_4');
		$right_content_text = $this->setconf->get('home_page','right_content_text');   
		$right_content_text_2 = $this->setconf->get('home_page','right_content_text_2');
		$right_content_text_3 = $this->setconf->get('home_page','right_content_text_3');
		$right_content_text_4 = $this->setconf->get('home_page','right_content_text_4');
		?>
		<div class="container">
			<h1 class="text-center"><strong><?php echo $title;?></strong></h1>
			<h2 class="text-center"><?php echo $sub_title;?></h2>
			<div class="col-sm-6">
				<p class="text-justify"><?php echo $left_content_text;?></p> 
				<p class="text-justify"><?php echo $left_content_text_2;?></p> 
				<p class="text-justify"><?php echo $left_content_text_3;?></p> 
				<p class="text-justify"><?php echo $left_content_text_4;?></p> 
			</div>
			<div class="col-sm-6">
				<p class="text-justify"><?php echo $right_content_text;?></p> 
				<p class="text-justify"><?php echo $right_content_text_2;?></p> 
				<p class="text-justify"><?php echo $right_content_text_3;?></p> 
				<p class="text-justify"><?php echo $right_content_text_4;?></p> 
			</div>
		</div>
	</div>
</div>
