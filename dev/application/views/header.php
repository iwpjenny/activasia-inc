<?php
$meta_title = app_get_val($settings,'meta_title'); 
$client_acronym = app_get_val($config_client,'client_acronym'); 
$meta_title = $meta_title?$meta_title:$client_acronym;
?>
<!-- start: header -->
<header class="container-fluid">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo site_url(); ?>"><?php echo $meta_title; ?></a>
			</div> <!-- end of .navbar-header -->
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?php echo site_url(); ?>">Home</a></li>
					<li><a href="<?php echo site_url('c=page&m=about_us'); ?>">About Us</a></li>
					<li><a href="<?php echo site_url('c='.$this->BLog->page); ?>">Log-in</a></li>
				</ul> <!-- end of .nav navbar-nav navbar-right -->
			</div> <!-- end of .navbar-collapse collapse -->
		</div>
	</nav> <!-- end of .navbar navbar-inverse navbar-fixed-top -->
</header><!-- end of .container-fluid -->
<!-- end: header -->
