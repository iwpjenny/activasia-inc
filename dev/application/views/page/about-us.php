<div class="container-fluid">
	<h1 class="title">Company Overview</h1>
	<div class="row"> 
		<div class="col-md-10"> 
			<h4><strong>ActivAsia Inc.</strong></h4>
			<p>Activasia Inc. is a Brand Activation (Advertising) company located in Pasig City with different branches all over the Philippines, which caters to big established companies nationally. OUR PURPOSE: "SPREADING HAPPINESS to our clients, our colleagues, our community and our country." OUR VALUES: RESPECT FOR THE INDIVIDUAL We respect each Individual's values, abilities and uniqueness PROFESSIONALISM We uphold leading edge thinking and doing in all aspects of our organization INTEGRITY We treat each of our colleagues, our clients, our projects and stakeholders with honesty, integrity and the highest standards of business ethics DOING WELL BY DOING GOOD We are a committed social business enterprise, through our support and participation in various advocacies FUN We value work-life balance and foster a rewarding and fun work environment that continually cultivates teamwork, creativity and learning.</p>
			<hr />
			<h4><strong>OUR PURPOSE:</strong></h4> 
			<p>"SPREADING HAPPINESS to our clients, our colleagues, our community and our country"</p> 
			<hr />
			<h4><strong>OUR VALUE PROPOSITION:</strong></h4> 
			<p>We are a pioneering, point-to-point Multi-Platform Activation agency specializing in integrated marketing across various channels of the brand activation process.</p> 
			<p>We are a committed team of passion-driven, professional dream-weavers, and idea makers who bring our client's brands to life to through the most compelling brand activation experiences.</p> 
			<hr />
			<h4><strong>OUR VALUES:</strong></h4> 
			<p><strong>RESPECT FOR THE INDIVIDUAL</strong><br/>
			We respect each Individual's values, abilities and uniqueness.</p>
			<p><strong>PROFESSIONALISM</strong><br/>
			We uphold leading edge thinking and doing in all aspects of our organization.</p>
			<p><strong>DOING WELL BY DOING GOOD</strong><br/>
			We are a committed social business enterprise, through our support and participation in various advocacies.</p> 
		</div> 
	</div> 
</div>
