<?php
$dev_copyright = app_get_val($config_developer,'dev_copyright'); 
$dev_developed = app_get_val($config_developer,'dev_developed'); 
?>
<hr />
<!-- start: footer -->
<footer class="container-fluid">
	<div class="row">
		<div class="col-sm-6 text-muted">
			<?php echo $dev_copyright; ?>
		</div>
		<div class="col-sm-6 text-right text-muted">
			<?php echo $dev_developed; ?>
		</div>
	</div>
</footer>
<!-- end: footer -->
