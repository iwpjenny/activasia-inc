<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="<?php echo base_url('includes/bootstrap3.3.7/css/bootstrap.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('includes/bootstrap3.3.7/css/bootstrap-theme.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('includes/css/normalize.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('includes/css/glyphicons-halflings.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('includes/css/font-awesome.min.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('includes/css/style.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('includes/css/normalize.css'); ?>" />
	<title>Error - No User Capability Permission</title>
</head>
<body>
	<div class="container">
		<div class="row"> 
			<div class="col-md-8 col-md-offset-2 text-center">
				<br />
				<br />
				<br />
				<div class="alert alert-danger">   
					<h1>You have No User Capability Permission to access this Page.</h1>
				</div>
				<a class="btn btn-default btn-md" href="<?php echo url_current(); ?>">Refresh</a>
				<a class="btn btn-info btn-md" href="<?php echo site_url(); ?>">Go back Home</a>
				<a class="btn btn-danger btn-md" href="<?php echo $url_logout; ?>">Logout</a>
			</div>
		</div>
	</div>
</body>
</html>