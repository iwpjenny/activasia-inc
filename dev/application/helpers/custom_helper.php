<?php
function get_urc( $array, $module, $urc_name ){
	
	$key = 'urc_'.$module.'_'.$urc_name;
	$result = get_value($array, $key);
	
	return $result;
}

function get_published( $published ){
	
	if( $published ){
		$result = '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
	} else {
		$result = '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
	}
	
	return $result;
}

function published( $published ){
	
	$result = get_published( $published );
	
	echo $result;
}

/* Do not use this anymore */
function custom_date_time($created, $modified){
	$CI = get_instance();
    $CI->load->model('Settings_Model', 'Set');	
	
	$date_time_settings = $CI->Set->get_settings( 'date-time' ); 
	$date_time_settings = $date_time_settings?$date_time_settings:'';
	$date_format = get_value($date_time_settings, 'date_format');
	$time_format = get_value($date_time_settings, 'time_format');
	
	$created = datetime($created, $date_format.' \a\t '.$time_format);
	$modified = datetime($modified, $date_format.' \a\t '.$time_format);

	$params_needed = array(
		'created_date' => $created,
		'modified_date' => $modified
	);
			
	return $params_needed;
}

/* Do not use this anymore */
function app_form_datetime($created, $modified){
	$CI = get_instance();
    $CI->load->model('Settings_Model', 'Set');	
	 
	$settings = $CI->setconf->all();  
	$view_date_format = app_val($settings,'view-date-format'); 
	$view_time_format = app_val($settings,'view-time-format');  
	
	$created = datetime($created, $view_date_format.' \a\t '.$view_time_format);
	$modified = datetime($modified, $view_date_format.' \a\t '.$view_time_format);

	$params_needed = array(
		'created' => $created,
		'modified' => $modified
	);
			
	return $params_needed;
}

/* Do not use this anymore */
/* function edit_form_stadard_footer_fields( $model, $fields, $id ){
	$CI = get_instance();
    $CI->load->model('Log_Model', 'Log');
	
	$view_published = 1;
	
	$created = get_value( $fields, $model->tblpref.'created');
	$modified = get_value( $fields, $model->tblpref.'modified');
	
	$params = app_form_datetime($created, $modified);
	 
	if( $params ){
		$created = get_value($params,'created');
		$modified = get_value($params,'modified');
	}
	
	ob_start();
	if( $id ){
		if( $view_published ){ 
		$publish_name = get_published_name();
		?>
		<div class="form-group">
			<label class="col-xs-4 col-sm-2"><?php echo $publish_name; ?>:</label>
			<div class="col-xs-4 col-sm-2">
				<label>
					<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					<?php app_field('published',$model,$fields,array('type'=>'radio','value'=>1,'compare_values'=>array(1,''))); ?> Yes
				</label>
			</div>
			<div class="col-xs-4 col-sm-2">
				<label>
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					<?php app_field('published',$model,$fields,array('type'=>'radio','value'=>0,'compare_values'=>0)); ?> No
				</label>
			</div>
			<label class="col-xs-6 col-sm-2"></label>
			<div class="col-xs-6 col-sm-4">
			</div>
		</div> <!-- end of .form-group -->
		<?php 
		}
		?>
		<div class="form-group">
			<div class="col-xs-6 col-sm-2">Created:</div>
			<div class="col-xs-6 col-sm-4">
				<small><?php echo $created; ?></small>
			</div>
			<div class="col-xs-6 col-sm-2">Modified:</div>
			<div class="col-xs-6 col-sm-4">
				<small><?php echo $modified; ?></small>
			</div>
		</div> <!-- end of .form-group -->
		<?php
	} else {
		app_field('published',$model,$fields,array('type'=>'hidden','set_value'=>1));
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	echo $contents;
} */

/* Do not use this anymore */
/* function form_stadard_footer_view_fields( $published, $created, $modified ){
	
	$params = custom_date_time($created, $modified); 
	if( $params ){
		$created_date = get_value( $params, 'created_date');
		$modified_date = get_value( $params, 'modified_date');
	}
	
	$publish_name = get_published_name(); 
	
	ob_start();
	?>
	<div class="form-group">
		<div class="col-xs-6 col-sm-2"><?php echo $publish_name; ?>:</div>
		<div class="col-xs-6 col-sm-2">
			<strong><?php echo $published?'Yes':'No'; ?></strong>
		</div>
		<div class="col-xs-6 col-sm-2">Created:</div>
		<div class="col-xs-6 col-sm-2">
		    <small><?php echo $created_date; ?></small>
		</div>
		<div class="col-xs-6 col-sm-2">Modified:</div>
		<div class="col-xs-6 col-sm-2">
			<small><?php echo $modified_date; ?></small>
		</div>
	</div> <!-- end of .form-group -->
	<?php
	$contents = ob_get_contents();
	ob_end_clean();
	
	echo $contents;
} */

/* Do not use this anymore */
/* function view_form_stadard_footer_fields( $model, $fields ){
	$CI = get_instance();
    $CI->load->model('Log_Model', 'Log');
	
	$created = get_value( $fields, $model->tblpref.'created');
	$modified = get_value( $fields, $model->tblpref.'modified');
	$published = get_value( $fields, $model->tblpref.'published');
	
	$params = app_form_datetime($created, $modified);
	
	if( $params ){
		$created = get_value($params,'created');
		$modified = get_value($params,'modified');
	}
	
	$publish_name = get_published_name(); 
	
	ob_start();
	?>
	<div class="form-group">
		<div class="col-xs-6 col-sm-2"><?php echo $publish_name; ?>:</div>
		<div class="col-xs-6 col-sm-2">
			<strong><?php echo $published?'Yes':'No'; ?></strong>
		</div>
		<div class="col-xs-6 col-sm-2">Created:</div>
		<div class="col-xs-6 col-sm-2">
		    <small><?php echo $created; ?></small>
		</div>
		<div class="col-xs-6 col-sm-2">Modified:</div>
		<div class="col-xs-6 col-sm-2">
			<small><?php echo $modified; ?></small>
		</div>
	</div> <!-- end of .form-group -->
	<?php
	$contents = ob_get_contents();
	ob_end_clean();
	
	echo $contents;
} */

function set_title( $id, $title ){
	
	ob_start();
	if( $id ){
		?>
		<h1 class="title">Edit <?php echo $title; ?></h1>
		<?php
	} else {
		?>
		<h1 class="title">Add <?php echo $title; ?></h1>
		<?php
	}
	
	$contents = ob_get_contents();
	ob_end_clean();
	
	echo $contents;
}

function get_published_name(){
	$CI = get_instance();
	$CI->load->model('Settings_Model', 'Set');
	$field_options = $CI->Set->get_full_settings('field-options');
	
	$publish_name = get_value( $field_options, 'publish_name', 'Published' );  
	
	return $publish_name;
} 

function option_status( $status, $page, $id, $show_status, $capabilities=array()){
	$show_status['status'] = $status;
	status_button( $page, $id, $show_status );
}

function status_button( $page, $id, $params ){
	
	$result = get_status_button( $page, $id, $params );
	
	echo $result;
}

function get_status_button( $page, $id, $params ){
	$CI = get_instance();
	extract($params); 
	ob_start();
	if( $status == 'Declined' && $CI->usersrole->check( $CI->CA->urc_name, 'approve') ){ 
		if( $show_status_ready_button ){ 
			?>
			<button type="button" class="btn btn-info btn-xs modal_ca_status" data-url="<?php echo site_url('c='.$page.'&m=check_for_approval'); ?>" data-id="<?php echo $id;?>" data-title="Cash Advance Ready for Approval Remarks" >Ready for Approval</button> 
			<?php 
		}
	} elseif( $status == 'Approved'  ){
		
		if( $show_status_release_button && $CI->usersrole->check( $CI->CA->urc_name, 'release') ){ 
			?>
			<!--a class="btn btn-success btn-xs" href="<?php echo site_url('c='.$page.'&m=release&id='.$id); ?>">Release</a-->
			<button type="button" class="btn btn-success btn-xs modal_ca_status" data-url="<?php echo site_url('c='.$page.'&m=release'); ?>" data-id="<?php echo $id;?>" data-title="Cash Advance Release Remarks" >Release</button>
			<?php
			}
		if( $show_status_decline_button && $CI->usersrole->check( $CI->CA->urc_name, 'decline') ){ 
			?>
			<!--a class="btn btn-danger btn-xs" href="<?php echo site_url('c='.$page.'&m=decline&id='.$id); ?>">Decline</a -->
			<button type="button" class="btn btn-danger btn-xs modal_ca_status" data-url="<?php echo site_url('c='.$page.'&m=decline'); ?>" data-id="<?php echo $id;?>" data-title="Cash Advance Decline Remarks" >Decline</button>
			<?php 
		}
	} elseif( $status == 'Released' ){ 
		if( $show_status_pending_button == 'yes' && $CI->usersrole->check( $CI->CA->urc_name, 'pending') ){ 
			?>
			<!--a class="btn btn-warning btn-xs" href="<?php echo site_url('c='.$page.'&m=pending&id='.$id); ?>">Pending</a-->
			<button type="button" class="btn btn-warning btn-xs modal_ca_status" data-url="<?php echo site_url('c='.$page.'&m=pending'); ?>" data-id="<?php echo $id;?>" data-title="Cash Advance Pending Remarks" >Pending</button>
			<?php
		}
	} elseif( $status == 'Ready') {
		if( $show_status_approve_button && $CI->usersrole->check( $CI->CA->urc_name, 'approve') ){ 
			?>
			<!--a class="btn btn-primary btn-xs" href="<?php echo site_url('c='.$page.'&m=approve&id='.$id); ?>">Approve</a --> 
			<button type="button" class="btn btn-primary btn-xs modal_ca_status" data-url="<?php echo site_url('c='.$page.'&m=approve'); ?>" data-id="<?php echo $id;?>" data-title="Cash Advance Approve Remarks" >Approve</button>
			<?php
			}
		if( $show_status_decline_button && $CI->usersrole->check( $CI->CA->urc_name, 'decline')  ){ 
			?>
			<!--a class="btn btn-danger btn-xs" href="<?php echo site_url('c='.$page.'&m=decline&id='.$id); ?>">Decline</a-->
			<button type="button" class="btn btn-danger btn-xs modal_ca_status" data-url="<?php echo site_url('c='.$page.'&m=decline'); ?>" data-id="<?php echo $id;?>" data-title="Cash Advance Decline Remarks" >Decline</button>
			<?php 
		}
	}
	else{
		if( $show_status_ready_button && $CI->usersrole->check( $CI->CA->urc_name, 'ready') ){  
			?>
			<!--a class="btn btn-info btn-xs" href="<?php echo site_url('c='.$page.'&m=check_for_approval&id='.$id); ?>">Ready for Approval</a-->
			<button type="button" class="btn btn-info btn-xs modal_ca_status" data-url="<?php echo site_url('c='.$page.'&m=check_for_approval'); ?>" data-id="<?php echo $id;?>" data-title="Cash Advance Ready for Approval Remarks" >Ready for Approval</button> 
			
			<?php
		}
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function get_ca_status_button( $status ){
	
	ob_start();
	if( $status == 'Declined' ){ 
		?>
		<span class="label label-danger"><?php echo $status;?></span>
		<?php 
	} elseif( $status == 'Approved' ){
		?>
		<span class="label label-primary"><?php echo $status;?></span>
		<?php 
	} elseif( $status == 'Released' ){ 
		?>
		<span class="label label-success"><?php echo $status;?></span>
		<?php
	} elseif( $status == 'Refunded' ){ 
		?>
		<span class="label label-Info"><?php echo $status;?></span>
		<?php
	}
	elseif( $status == 'Ready' ){ 
	// printx($status);
		?>
		<span class="label label-info">Ready for Approval</span>
		<?php
	} else {
		?>
		<span class="label label-warning"><?php echo $status;?></span>
		<?php
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	echo $contents;
}

function get_cancelled( $value ){
 
	if( $value ){
		$result = '<span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>';
	} else {
		$result = '<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>';
	}
	
	return $result;
}

function cancelled( $cancelled ){
	
	$result = get_cancelled( $cancelled ); 
	echo $result;
}

function get_custom_button($page, $params){ 
	$CI = get_instance();
	extract($params); 
	
	$import = isset($import_name)?$import_name:'Import';
	$export = isset($export)?$export:null;
	
	foreach($display as $action){ 
		$capability = $CI->usersrole->get_capability($capabilities, $ur_name, $action); 
		if($capability){ 
			if($action == 'export'){
				if( $export == 'modal' ){
					?>
					<a href="#" data-action="<?php echo site_url('c='.$page.'&m=export');?>" class="btn btn-sm btn-info display-filter-modal"  ><span class="glyphicon glyphicon-file"></span> Export</a> 
					<?php	
				} else {
					?>
					<a href="<?php echo site_url('c='.$page.'&m=export');?>" class="btn btn-sm btn-info" target="_blank"><span class="glyphicon glyphicon-file"></span> Export</a>
					<?php	
				} 		
			} else if($action == 'import'){
			?>
				<a href="<?php echo site_url('c='.$page.'CSV');?>" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-file"></span> <?php echo $import; ?></a>
			<?php	 		
			}
		}
	} 
}

function export_individual_button($page, $params){ 
	$CI = get_instance();
	extract($params); 
	
	foreach($display as $action){ 
		$capability = $CI->usersrole->get_capability($capabilities, $ur_name, $action); 
		if($capability){ 
			if($action == 'export'){
			?>
				<a href="<?php echo site_url('c='.$page.'&m=export_individual&id='.$id);?>" class="btn btn-sm btn-info" target="_blank"><span class="glyphicon glyphicon-file"></span> Export</a>
			<?php			
			} 
		}
	} 
}
 
 
?>