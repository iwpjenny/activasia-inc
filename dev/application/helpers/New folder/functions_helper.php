<?php
function printr( $array ){
	echo '<pre>';
	print_r($array);
	echo '</pre>';
}

function printx( $array ){
	echo '<pre>';
	print_r($array);
	echo '</pre>';
	exit();
}

function app_limit_chars( $string, $lenght=50, $dots='...'){
	  
	$string = strip_tags($string);
	$string = html_entity_decode($string,ENT_QUOTES,'UTF-8');
	
	$strlength = strlen($string);
	
	if ( $strlength > $lenght ){       
		$limited = substr($string, 0, $lenght);
		$limited .= $dots;                  
	} else {
		$limited = $string;
	}
	
	$limited = htmlentities($limited,ENT_QUOTES,'UTF-8');
	
	return $limited;
}

function app_tohtml( $text ){
	
	$text = trim($text);
	$text = htmlentities($text,ENT_QUOTES,'UTF-8');
	
	return $text;
}

/* this will be remove later on, do not use this */
function current_date(){
	$date = date('Y-m-d');
	
	return $date;
}

/* this will be remove later on, do not use this */
function current_datetime(){
	
	$date = date('Y-m-d H:i:s');
	
	return $date;
}

/*
 * Return Current Date of the month
 *
 * @return date
 */
function app_cd(){
		
	return date('Y-m-d');
}

/*
 * Return Current Time of the month
 *
 * @return date
 */
function app_ct(){
		
	return date('H:i:s');
}

/*
 * Return Current Date and Time of the month
 *
 * @return date time
 */
function app_cdt(){
	
	return date('Y-m-d H:i:s');
}

/*
 * Format Date and Time Value
 *
 * @param	timestamp	$time_stamp		timestamp value
 * @param	string		$format			date and time desired format 
 * @param 	string		$default		date and time default value 
 * @return	date time
 */
function app_dt( $time_stamp, $format='Y-m-d H:i:s', $default='0000-00-00' ){
	
	$date_time = date($format, strtotime($time_stamp));
	
	if( in_array( $date_time, array('1970-01-01','')) ){
		$date_time = $default;
	}
	
	return $date_time;
}

/* this will be remove later on, do not use this */
function datetime( $time_stamp, $format='Y-m-d H:i:s', $default='0000-00-00' ){
	
	$date_time = date($format, strtotime($time_stamp));
	
	if( in_array( $date_time, array('1970-01-01','')) ){
		$date_time = $default;
	}
	
	return $date_time;
}

/*
 * Format Date and Time Value
 *
 * @param	array/object	$array_object	array or object format
 * @param	string			$name			string format, small caps, without spaces and underscore/dash as space/seperator 
 * @param 	string			$default		any default string value
 * @return	string
 */
function app_val( $array_object, $name, $default=NULL ){
	
	$value = app_get_val( $array_object, $name, $default );
	
	echo $value;
}

function app_get_val( $array_object, $name, $default=NULL ){
	
	if( is_array($array_object) ){
		$value = isset($array_object[$name])?$array_object[$name]:NULL;
	} elseif( is_object($array_object) ){
		$value = isset($array_object->$name)?$array_object->$name:NULL;
	} else {
		$value = $array_object;
	}
	
	if( $value == '' && isset($default) ){
		$value = $default;
	}
	
	return $value;
}

/* this will be remove later on, do not use this */
function get_value( $array_object, $name, $default_value=NULL ){
	
	if( is_array($array_object) ){
		$value = isset($array_object[$name])?$array_object[$name]:NULL;
	} elseif( is_object($array_object) ){
		$value = isset($array_object->$name)?$array_object->$name:NULL;
	} else {
		$value = $array_object;
	}
	
	if( $value == '' && isset($default_value) ){
		$value = $default_value;
	}
	
	return $value;
}

function app_get_request( $name, $default_value=NULL ){
		
	$value = isset($_POST[$name])?$_POST[$name]:(isset($_GET[$name])?$_GET[$name]:NULL);
	
	if( isset($value) && $value == '' && isset($default_value) ){
		$value = $default_value;
	}
	
	if( get_magic_quotes_gpc() == TRUE ){
		$value = stripslashes($value);
	}
	
	return $value;
}

/* this will be remove later on, do not use this */
function get_request( $name, $default_value=NULL ){
		
	$value = isset($_POST[$name])?$_POST[$name]:(isset($_GET[$name])?$_GET[$name]:NULL);
	
	if( isset($value) && $value == '' && isset($default_value) ){
		$value = $default_value;
	}
	
	if( get_magic_quotes_gpc() == TRUE ){
		$value = stripslashes($value);
	}
	
	return $value;
}

function app_checked( $selected_value, $field_value_array ){
	
	$result = app_get_checked( $selected_value, $field_value_array );
	
	echo $result;
}

function app_get_checked( $selected_value, $field_value_array ){
	
	$result = '';	
	if( is_array($field_value_array) && in_array($selected_value, $field_value_array) ){
		$result = ' checked="checked" ';
	} elseif($selected_value == $field_value_array){
		$result = ' checked="checked" ';
	}
	
	return $result;
}

/* do not use this anymore */
function checked( $selected_value, $field_value_array ){
	
	$result = get_checked( $selected_value, $field_value_array );
	
	echo $result;
}

/* do not use this anymore */
function get_checked( $selected_value, $field_value_array ){
	
	$result = '';	
	if( is_array($field_value_array) && in_array($selected_value, $field_value_array) ){
		$result = ' checked="checked" ';
	} elseif($selected_value == $field_value_array){
		$result = ' checked="checked" ';
	}
	
	return $result;
}

/* do not use this anymore */
function is_checked( $selected_value, $field_value_array ){
	
	$result = '';	
	if( is_array($field_value_array) && in_array($selected_value, $field_value_array) ){
		$result = ' checked="checked" ';
	} elseif($selected_value == $field_value_array){
		$result = ' checked="checked" ';
	}
	
	echo $result;
}

function is_serialized( $data, $strict = true ) {
	// if it isn't a string, it isn't serialized.
	if( ! is_string( $data )){
		return false;
	}
	$data = trim( $data );
	if ( 'N;' == $data ) {
		return true;
	}
	if ( strlen( $data ) < 4 ){
		return false;
	}
	if ( ':' !== $data[1] ){
		return false;
	}
	if( $strict ){
		$lastc = substr( $data, -1 );
		if( ';' !== $lastc && '}' !== $lastc ){
			return false;
		}
	} else {
		$semicolon = strpos( $data, ';' );
		$brace     = strpos( $data, '}' );
		// Either ; or } must exist.
		if( false === $semicolon && false === $brace )
			return false;
		// But neither must be in the first X characters.
		if( false !== $semicolon && $semicolon < 3 )
			return false;
		if( false !== $brace && $brace < 4 )
			return false;
	}
	$token = $data[0];
	switch( $token ) {
		case 's' :
			if( $strict ) {
				if ( '"' !== substr( $data, -2, 1 ) ) {
					return false;
				}
			} elseif ( false === strpos( $data, '"' ) ) {
				return false;
			}
			// or else fall through
		case 'a' :
		case 'O' :
			return (bool) preg_match( "/^{$token}:[0-9]+:/s", $data );
		case 'b' :
		case 'i' :
		case 'd' :
			$end = $strict ? '$' : '';
		return (bool) preg_match( "/^{$token}:[0-9.E-]+;$end/", $data );
	}
	return false;
}

function app_selected( $selected_value, $field_value_array='', $attr='selected' ){
	
	$result = app_get_selected( $selected_value, $field_value_array, $attr );
	
	echo $result;
}

function app_get_selected( $selected_value, $field_value_array='', $attr='selected' ){
	
	$result = '';
		
	if( is_array($field_value_array) && in_array($selected_value, $field_value_array) ){
		$result = ' '.$attr.'="selected" ';
	} elseif($selected_value == $field_value_array){
		$result = ' '.$attr.'="selected" ';
	}
	
	return $result;
}

/* do not use this anymore */
function selected( $selected_value, $field_value_array='', $attr='selected' ){
	
	$result = get_selected( $selected_value, $field_value_array, $attr );
	
	echo $result;
}

/* do not use this anymore */
function get_selected( $selected_value, $field_value_array='', $attr='selected' ){
	
	$result = '';
		
	if( is_array($field_value_array) && in_array($selected_value, $field_value_array) ){
		$result = ' '.$attr.'="selected" ';
	} elseif($selected_value == $field_value_array){
		$result = ' '.$attr.'="selected" ';
	}
	
	echo $result;
}

function active( $selected_value, $field_value_array='', $value='active' ){
	
	$result = '';
		
	if( is_array($field_value_array) && in_array($selected_value, $field_value_array) ){
		$result = $value;
	} elseif($selected_value == $field_value_array){
		$result = $value;
	}
	
	echo $result;
}

function is_display( $selected_value, $field_value_array ){
	
	$result = '';
	
	if( is_array($field_value_array) && in_array($selected_value, $field_value_array) ){
		$result = ' display:none; ';
	} elseif($selected_value == $field_value_array){
		$result = ' display:none; ';
	}
	
	echo $result;
}

function app_readonly( $value, $array_value ){	
	$result = in_array($value, $array_value)?'readonly="readonly"':'';
	
	echo $result;
}

function app_disabled( $value, $array_value ){	
	$result = in_array($value, $array_value)?'disabled="disabled"':'';
	
	echo $result;
}

/* do not use this anymore */
function readonly( $value, $array_value ){	
	$result = in_array($value, $array_value)?'readonly="readonly"':'';
	
	echo $result;
}

/* do not use this anymore */
function disabled( $value, $array_value ){	
	$result = in_array($value, $array_value)?'disabled="disabled"':'';
	
	echo $result;
}

function app_create_file($content, $path, $file_name, $ext='txt', $suffix=''){
	
	$suffix = $suffix ? $suffix : date('Ymd_His');
	$full_file_path = $path.$file_name.'-'.$suffix.'.'.$ext;
	
	$handle = @fopen($full_file_path, 'w');
	@fwrite($handle, $content);
	@fclose($handle);
}

/* do not use this anymore */
function redirect_current_page(){

	redirect($_SERVER['HTTP_REFERER']);
}

function redirect_current(){

	redirect($_SERVER['HTTP_REFERER']);
}

function url_current(){

	$url = site_url($_SERVER['QUERY_STRING']);

	return $url;
}

function url_previous(){

	$url = site_url($_SERVER['HTTP_REFERER']);

	return $url;
}

function file_include_once( $include_file_path ){
	$included_files_array = array();
	
	$included_files = get_included_files();
	
	if( $included_files ){
		foreach($included_files as $filename) {
			$included_files_array[] = basename($filename);
		}
	}
	
	if( in_array(basename($include_file_path), $included_files_array) == FALSE ){
		include_once($include_file_path);
	}
}
	
function app_object_to_array( $obj ){
	$array = array();
	
	if( is_object($obj) ){ 
		$array = (array) $obj;
	} else {
		$array = $obj;
	}
	
	if( is_array($array) ) {
		$new = array();
		foreach($array as $key => $val) {
			if( $this->is_serialized( $val ) ){
				$new[$key] = unserialize($val);
			} else {
				$new[$key] = object_to_array($val);
			}
		}
	} else { 
		$new = $array;
	}
	
	return $new;
}

function app_array_to_data( $data, $records ){
	$records = object_to_array( $records );
	
	if( $records ){
		foreach( $records as $key=>$value ){
			$data[$key] = $value;
		}
	}
	
	return $data;
}

function app_array_to_data_select_key( $key, $records ){
	$data = array();
	
	$records = app_object_to_array( $records );
	
	if( $records ){
		foreach( $records as $value ){
			$data[] = $value[$key];
		}
	}
	
	return $data;
}

function app_select_dropdown( $name, $records, $selected_value, $params=array() ){
	
	$params_default = array_merge(array(
		'multiple'=>FALSE,
		'default_value'=>'',
		'class'=>'form-control',
		'elem_id'=>'',
		'attr'=>'',
		'req'=>TRUE,
		'read'=>FALSE,
		), $params
	);		
	extract($params_default);
		
	if( $multiple === TRUE ){
		$name = $name.'[]';
		$selected_value = is_serialized($selected_value)?unserialize($selected_value):'';
	}
	
	$attr = $attr?$attr:'';
	$class = $class?'class="'.$class.'"':'';
	$multiple = $multiple?'multiple="multiple"':'';
	$required = $req?'required="required"':'';
	$readonly = $read?'readonly="readonly"':'';
	$elem_id = $elem_id?'id="'.$elem_id.'"':'';
	
	$attributes = $attr.' '.$required.' '.$readonly.' '.$elem_id.' '.$multiple.' '.$class;
	
	$result = '<select name="'.$name.'" '.$attributes.'>';
	foreach($records as $value=>$text){
		if( is_array($selected_value) ){
			if( in_array($value, $selected_value) ){
				$result .= '<option value="'.$value.'" selected="selected">'.$text.'</option>';
			} else {
				$result .= '<option value="'.$value.'">'.$text.'</option>';
			}
		} elseif( $selected_value == $value ){
			$result .= '<option value="'.$value.'" selected="selected">'.$text.'</option>';
		} else {
			if($default_value == $value){
				$result .= '<option value="'.$value.'" selected="selected">'.$text.'</option>';
			} else {
				$result .= '<option value="'.$value.'">'.$text.'</option>';
			}
		}
	}
	$result .= '</select>';
	
	return $result;
}

function app_show_select_order( $selected_value, $max=10 ){
	
	$result = '<select name="order">';
	for($n=1; $n<=$max; $n++){
		if($selected_value == $n){
			$result .= '<option value="'.$n.'" selected="selected">'.$n.'</option>';
		} else {
			$result .= '<option value="'.$n.'">'.$n.'</option>';
		}
	}
	$result .= '</select>';
	
	return $result;
}

function app_add_lead_zero( $number ){
	
	$number = $number<10?'0'.$number:$number;
	
	return $number;
}

function app_get_file_open_dir( $dir ){
	$filearray = array();

	if (is_dir($dir)) {
		if ($dh = opendir($dir)) {
			while (($file = readdir($dh)) !== FALSE) {
				if($file != '.' && $file != '..' && is_dir($dir.$file) == FALSE){
					$filearray[] = $file;
				}
			}
			closedir($dh);
		}
	}
	
	return $filearray;
}

function app_get_date_of_birth( $sday, $smonth='', $syear='' ){
	  
	if( $smonth == '' && $syear == '' ){
		$sday = datetime($sday,'d');
		$smonth = datetime($smonth,'m');
		$syear = datetime($syear,'Y');
	}
	
	ob_start();
	?>
	<div class="row">
		<div class="col-md-4  col-sm-3 col-xs-4 date_pick">
			<select name="bdaydate" class="form-control input-xs date_pick">
			<?php
			if( $sday == '' ){
				?>
				<option value="" disabled selected>Day</option>
				<?php
			}
			for($day=1; $day<=31; $day++){
				$tday = ($day<10)?('0'.$day):$day;
				if($sday == $day){
					?>
					<option value="<?php echo $day; ?>" selected="selected"><?php echo $tday; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $day; ?>" ><?php echo $tday; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
		<div class="col-md-4 col-sm-3 col-xs-4 date_pick">
			<select name="bdaymonth" class="form-control input-xs date_pick">
			<?php
			if( $smonth == '' ){
				?>
				<option value="">Month</option>
				<?php
			}
			for($month=1; $month<=12; $month++){
				$tmonth = ($month<10)?('0'.$month):$month;
				if($smonth == $month){
					?>
					<option value="<?php echo $month; ?>" selected="selected"><?php echo $tmonth; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $month; ?>" ><?php echo $tmonth; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-4 date_pick">
			<select name="bdayyear" class="form-control input-xs date_pick">
			<?php
			if( $syear == '' ){
				?>
				<option value="">Year</option>
				<?php
			}
			$current_year = date('Y');
			$limit_year = $current_year-100;
			for($year=$current_year; $year>=$limit_year; $year--){
				if($syear == $year){
					?>
					<option value="<?php echo $year; ?>" selected="selected"><?php echo $year; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $year; ?>" ><?php echo $year; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
	</div>

	<?php
	$contents = ob_get_contents();
	ob_end_clean();

	return $contents;
}


/* function get_date_of_birth_spouse( $sday, $smonth='', $syear='' ){
	
	if( $smonth == '' && $syear == '' ){
		$sday = datetime($sday,'d');
		$smonth = datetime($smonth,'m');
		$syear = datetime($syear,'Y');
	}
	
	ob_start();
	?>
	<div class="row">
		<div class="col-md-4  col-sm-3 col-xs-4 date_pick">
			<select name="spouse_bdaydate" class="form-control input-xs date_pick">

			<?php
			if( $sday == '' ){
				?>
				<option value="" disabled selected>Day</option>
				<?php
			}
			for($day=1; $day<=31; $day++){
				$tday = ($day<10)?('0'.$day):$day;
				if($sday == $day){
					?>
					
					<option value="<?php echo $day; ?>" selected="selected"><?php echo $tday; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $day; ?>" ><?php echo $tday; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
		<div class="col-md-4 col-sm-3 col-xs-4 date_pick">
			<select name="spouse_bdaymonth" class="form-control input-xs date_pick">
			<?php
			if( $smonth == '' ){
				?>
				<option value="">Month</option>
				<?php
			}
			for($month=1; $month<=12; $month++){
				$tmonth = ($month<10)?('0'.$month):$month;
				if($smonth == $month){
					?>
					<option value="<?php echo $month; ?>" selected="selected"><?php echo $tmonth; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $month; ?>" ><?php echo $tmonth; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-4 date_pick">
			<select name="spouse_bdayyear" class="form-control input-xs date_pick">
			<?php
			if( $syear == '' ){
				?>
				<option value="">Year</option>
				<?php
			}
			$current_year = date('Y');
			$limit_year = $current_year-100;
			for($year=$current_year; $year>=$limit_year; $year--){
				if($syear == $year){
					?>
					<option value="<?php echo $year; ?>" selected="selected"><?php echo $year; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $year; ?>" ><?php echo $year; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
	</div>

	<?php
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function get_date_of_birth_mch( $sday, $smonth='', $syear='' ){
	
	if( $smonth == '' && $syear == '' ){
		$sday = datetime($sday,'d');
		$smonth = datetime($smonth,'m');
		$syear = datetime($syear,'Y');
	}

	ob_start();
	?>
	<div class="row">
		<div class="col-md-4 col-sm-4 col-xs-4 date_pick">
			<select name="mch_bdaydate" class="form-control input-xs date_pick">

			<?php
			if( $sday == '' ){
				?>
				<option value="" disabled selected>Day</option>
				<?php
			}
			for($day=1; $day<=31; $day++){
				$tday = ($day<10)?('0'.$day):$day;
				if($sday == $day){
					?>
					<option value="<?php echo $day; ?>" selected="selected"><?php echo $tday; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $day; ?>" ><?php echo $tday; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4 date_pick">
			<select name="mch_bdaymonth" class="form-control input-xs date_pick">
			<?php
			if( $smonth == '' ){
				?>
				<option value="">Month</option>
				<?php
			}
			for($month=1; $month<=12; $month++){
				$tmonth = ($month<10)?('0'.$month):$month;
				if($smonth == $month){
					?>
					<option value="<?php echo $month; ?>" selected="selected"><?php echo $tmonth; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $month; ?>" ><?php echo $tmonth; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4 date_pick">
			<select name="mch_bdayyear" class="form-control input-xs date_pick">
			<?php
			if( $syear == '' ){
				?>
				<option value="">Year</option>
				<?php
			}
			$current_year = date('Y');
			$limit_year = $current_year-100;
			for($year=$current_year; $year>=$limit_year; $year--){
				if($syear == $year){
					?>
					<option value="<?php echo $year; ?>" selected="selected"><?php echo $year; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $year; ?>" ><?php echo $year; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
	</div>

	<?php
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function get_date_of_birth_mib( $sday, $smonth='', $syear='' ){
	
	if( $smonth == '' && $syear == '' ){
		$sday = datetime($sday,'d');
		$smonth = datetime($smonth,'m');
		$syear = datetime($syear,'Y');
	}

	ob_start();
	?>
	<div class="row">
		<div class="col-md-4  col-sm-3 col-xs-4 date_pick">
			<select name="mib_bdaydate" class="form-control input-xs date_pick">

			<?php
			if( $sday == '' ){
				?>
				<option value="" disabled selected>Day</option>
				<?php
			}
			for($day=1; $day<=31; $day++){
				$tday = ($day<10)?('0'.$day):$day;
				if($sday == $day){
					?>
					<option value="<?php echo $day; ?>" selected="selected"><?php echo $tday; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $day; ?>" ><?php echo $tday; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
		<div class="col-md-4 col-sm-3 col-xs-4 date_pick">
			<select name="mib_bdaymonth" class="form-control input-xs date_pick">
			<?php
			if( $smonth == '' ){
				?>
				<option value="">Month</option>
				<?php
			}
			for($month=1; $month<=12; $month++){
				$tmonth = ($month<10)?('0'.$month):$month;
				if($smonth == $month){
					?>
					<option value="<?php echo $month; ?>" selected="selected"><?php echo $tmonth; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $month; ?>" ><?php echo $tmonth; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-4 date_pick">
			<select name="mib_bdayyear" class="form-control input-xs date_pick">
			<?php
			if( $syear == '' ){
				?>
				<option value="">Year</option>
				<?php
			}
			$current_year = date('Y');
			$limit_year = $current_year-100;
			for($year=$current_year; $year>=$limit_year; $year--){
				if($syear == $year){
					?>
					<option value="<?php echo $year; ?>" selected="selected"><?php echo $year; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $year; ?>" ><?php echo $year; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
	</div>

	<?php
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
} */


function app_select_month( $selected_month ){
	
	$month_names = array('','January','February','March','April','May','June','July','August','September','October','November','December');
	
	ob_start();
	?>	
	<select name="month" class="form-control input-sm" >
		<?php
		
		if( $selected_month == '' ){
			?>
			<option value="" selected="selected">---Month---</option>
			<?php
		} else {
			?>
			<option value="">---Month---</option>
			<?php
		}
		
		for($x=1; $x<count($month_names); $x++ ){
			$month = $month_names[$x];
			if( $x == $selected_month ){
				?>
				<option value="<?php echo $x; ?>" selected="selected"><?php echo $month; ?></option>
				<?php
			} else {
				?>
				<option value="<?php echo $x; ?>" ><?php echo $month; ?></option>
				<?php
			}
		}
		?>
	</select>	
	<?php
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function app_select_year( $selected_year ){
	
	ob_start();
	?>
	<select name="year" class="form-control input-sm" required="required" >
		<?php
		if($selected_year == ''){
			?>
			<option value="" selected="selected">---Year---</option>
			<?php
		} else {
			?>
			<option value="">---Year---</option>
			<?php
		}
			
		$current_year = date('Y');		
		$limit_year = ($current_year-10);
		for($year=$current_year; $year >= $limit_year; $year--){
			if($selected_year == $year){
				?>
				<option value="<?php echo $year; ?>" selected="selected"><?php echo $year; ?></option>
				<?php
			} else {
				?>
				<option value="<?php echo $year; ?>"><?php echo $year; ?></option>
				<?php
			}
		}
		?>
	</select>
	<?php
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function app_add_leading_zero( $number, $digit=2 ){
	
	if( is_numeric($number) ){
		$count_string = strlen($number);
		if( $digit > 1 ){
			$zeros = $digit-$count_string;
		
			$leading_zero = '';
			for($x=1; $x<=$zeros; $x++){
				$leading_zero .= '0';
			}
			
			$number = $leading_zero.$number;
		}
	} else {
		$number = 0;
	}
	
	return $number;	
}

function app_select_ordertype( $selected_value ){
	
	$options = array('--Sort--'=>'','ASC'=>'ASC','DESC'=>'DESC');
	
	ob_start();
	?>
	<select name="ordertype" class="form-control input-sm" required="required">
		<?php
		foreach($options as $text=>$value){
			if( $value == $selected_value ){
				?>
				<option value="<?php echo $value; ?>" selected="selected"><?php echo $text; ?></option>
				<?php
			} else {
				?>
				<option value="<?php echo $value; ?>"><?php echo $text; ?></option>
				<?php
			}
		}
		?>
	</select>
	<?php
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function app_radio_button_ordertype( $selected_value ){
	
	ob_start();
	?>
	Order Type: <label><input type="radio" name="ordertype" value="ASC" required="required" <?php app_checked($selected_value,'ASC'); ?> /> ASC</label> 
	<label><input type="radio" name="ordertype" value="DESC" required="required" <?php app_checked($selected_value,array('DESC','')); ?> /> DESC</label>
	<?php
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function app_get_month_name( $month_number ){
	if( $month_number ){
		$date_object_return = DateTime::createFromFormat('!m', $month_number );
		$month_name = $date_object_return->format('F'); 
		return $month_name;
	} 
}

function app_get_time_schedule( $upload_schedule_time ){
	
	$time_value = 1;
	for($i = 0; $i < 24; $i++){ 
		if($upload_schedule_time == $time_value ){
		?>
		<?php $i % 12 ? $i % 12 : 12; ?>:00 <?php $i >= 12 ? 'pm' : 'am'; ?> 
		<?php 
		}  
		$time_value++;
	}  
}

function app_pagination_details( $total_rows ){
	
	$result = app_get_pagination_details( $total_rows );
	
	echo $result;
}

function app_get_pagination_details( $total_rows ){
	$CI = get_instance();
	$cur_page = $CI->pagination->cur_page;
	$per_page = $CI->pagination->per_page;
	
	$from = ((($cur_page-1)*$per_page)+1);
	$to = $per_page+($from-1);
	if( $to > $total_rows ){
		$to = $total_rows;
	} elseif( $per_page > $total_rows ){
		$from = 1;
		$to = $total_rows;
	}
	
	$result = 'Showing: <strong>'.$from.' - '.$to.'</strong> out of <strong>'.$total_rows.'</strong>, page <strong>'.$cur_page.'</strong>';
	
	return $result;
}
	
function app_fullname_format( $firstname, $middlename, $lastname, $format='{firstname} {mi}. {lastname}', $text_transform='capitalize' ){
	
	$fullname = app_get_fullname_format( $firstname, $middlename, $lastname, $format, $text_transform );
	
	echo $fullname;
}
	
function app_get_fullname_format( $firstname, $middlename, $lastname, $format='{firstname} {mi}. {lastname}', $text_transform='capitalize' ){
	
	$mi = substr($middlename, 0, 1);
	
	$format = str_replace('{firstname}', $firstname, $format);
	$format = str_replace('{middlename}', $middlename, $format);
	$format = str_replace('{mi}', $mi, $format);
	$fullname = str_replace('{lastname}', $lastname, $format);
	
	if( $text_transform == 'uppsercase' ){
		$fullname = strtoupper($fullname);
	} elseif($text_transform == 'lowercase') {
		$fullname = strtolower($fullname);
	} else {
		$fullname = ucwords($fullname);
	}
	
	return $fullname;
}

function app_convert_to_name( $string, $space='-' ){
	
	$string = strtolower($string);
	$string = str_replace(' ', $space, $string); // Replaces all spaces with hyphens.
	$string = preg_replace('/[^A-Za-z0-9\\'.$space.']/','', $string); // Removes special chars.
	$string = preg_replace('/'.$space.'+/',$space, $string); // Replaces multiple hyphens with single one.

	return $string;
}
?>