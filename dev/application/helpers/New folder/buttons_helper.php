<?php
function get_btn_view( $page, $params ){
	$CI = get_instance();
	
	$params_default = array_merge(array(
		'id_field'=>'id',
		'display'=>array('back','edit','delete','trash')
		), $params
	);
	extract($params_default);
	
	$edit = $CI->usersrole->get_capability($capabilities, $ur_name, 'edit');
	$delete = $CI->usersrole->get_capability($capabilities, $ur_name, 'delete');
	$trash = $CI->usersrole->get_capability($capabilities, $ur_name, 'trash');
	
	ob_start();
	if( in_array('back',$display) ){
		?>
		<a class="btn btn-default btn-sm" href="<?php echo site_url('c='.$page); ?>">Back</a>
		<?php
	}
	if( in_array('edit',$display) && $edit ){
		?>
		<a class="btn btn-primary btn-sm" href="<?php echo site_url('c='.$page.'&m=edit&'.$id_field.'='.$id); ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>
		<?php
	}
	if( in_array('delete',$display) && $delete ){
		?>
		<a class="btn btn-danger btn-sm confirmation" href="#delete-confirmation-<?php echo $id; ?>">
		<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete</a>
		<div id="delete-confirmation-<?php echo $id; ?>" style="display:none;">
			<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&'.$id_field.'='.$id); ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Yes Delete</a></p>
		</div>
		<?php
	}
	if( in_array('trash',$display) && $trash ){
		?>
		<a class="btn btn-warning btn-sm confirmation" href="#trash-confirmation-<?php echo $id; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true" title="Trash Record ID #<?php echo $id; ?>"></span> Trash</a>
		<div id="trash-confirmation-<?php echo $id; ?>" style="display:none;">
			<p>Do you want to trash this record? <a class="btn btn-warning btn-sm" href="<?php echo site_url('c='.$page.'&m=trash&'.$id_field.'='.$id); ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>Yes Trash</a></p>
		</div>
		<?php
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function btn_view( $page, $params ){
	
	$return = get_btn_view( $page, $params );
	
	echo $return;
}

function get_btn_edit( $page, $params ){
	$CI = get_instance();
	
	$params_default = array_merge(array(
		'id_field'=>'id',
		'display'=>array('back','edit','add','view','delete','trash')
		), $params
	);
	extract($params_default);
	
	$edit = $CI->usersrole->get_capability($capabilities, $ur_name, 'edit');
	$add = $CI->usersrole->get_capability($capabilities, $ur_name, 'add');
	$view = $CI->usersrole->get_capability($capabilities, $ur_name, 'view');
	$trash = $CI->usersrole->get_capability($capabilities, $ur_name, 'trash');
	$delete = $CI->usersrole->get_capability($capabilities, $ur_name, 'delete');
	$reset = $CI->usersrole->get_capability($capabilities, $ur_name, 'reset');
	
	ob_start();
	if( in_array('back',$display) ){
		?>
		<a class="btn btn-default btn-sm" href="<?php echo site_url('c='.$page); ?>">Back</a>
		<?php
	}
	if( $id ){
		if( in_array('edit',$display) && $edit ){
			?>
			<button type="submit" class="btn btn-primary btn-sm js-required-fields">
			<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Update Data
			</button>
			<?php
		}
		if( in_array('add',$display) && $add ){
			?>
			<a class="btn btn-success btn-sm" href="<?php echo site_url('c='.$page.'&m=edit'); ?>">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New</a>
			<?php
		}
		if( in_array('view',$display) && $view ){
			?>
			<a class="btn btn-info btn-sm" href="<?php echo site_url('c='.$page.'&m=view&'.$id_field.'='.$id); ?>">
			<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> View</a>
			<?php
		}
		if( in_array('delete',$display) && $delete ){
			?>
			<a class="btn btn-danger btn-sm confirmation" href="#delete-confirmation-<?php echo $id; ?>">
			<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete</a>
			<div id="delete-confirmation-<?php echo $id; ?>" style="display:none;">
				<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&'.$id_field.'='.$id); ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Yes Delete</a></p>
			</div>
			<?php
		}
		if( in_array('trash',$display) && $trash ){
			?>
			<a class="btn btn-warning btn-sm confirmation" href="#trash-confirmation-<?php echo $id; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true" title="Trash Record ID #<?php echo $id; ?>"></span> Trash</a>
			<div id="trash-confirmation-<?php echo $id; ?>" style="display:none;">
				<p>Do you want to trash this record? <a class="btn btn-warning btn-sm" href="<?php echo site_url('c='.$page.'&m=trash&'.$id_field.'='.$id); ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>Yes Trash</a></p>
			</div>
			<?php
		}
	} else {
		if( in_array('add',$display) && $add ){
			?>
			<button type="submit" class="btn btn-success btn-sm js-required-fields">
			<span class="glyphicon glyphicon-save" aria-hidden="true"></span>Save Data</button>
			<?php
		}
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	echo $contents;
}

function btn_edit( $page, $params ){
	
	$result = get_btn_edit( $page, $params );
	
	echo $result;
}

function get_btn_records( $page, $params=array() ){
	$CI = get_instance();
	
	$params_default = array_merge(array(
		'id_field'=>'id',
		'display'=>array('add','trash')
		), $params
	);
	extract($params_default);
	
	$records = $CI->usersrole->get_capability($capabilities, $ur_name, 'records');
	$add = $CI->usersrole->get_capability($capabilities, $ur_name, 'add');
	$trash = $CI->usersrole->get_capability($capabilities, $ur_name, 'trash');
	
	ob_start();
	if( in_array('records',$display) && $records ){
		?>
		<a class="btn btn-default btn-sm" href="<?php echo site_url('c='.$page); ?>">View Records</a>
		<?php
	}
	if( in_array('add',$display) && $add ){
		?>
		<a class="btn btn-success btn-sm" href="<?php echo site_url('c='.$page.'&m=edit'); ?>"><span class="glyphicon glyphicon-save" aria-hidden="true"></span>Add New</a>
		<?php
	}
	if( in_array('trash',$display) && $trash ){
		?>
		<a class="btn btn-warning btn-sm" href="<?php echo site_url('c='.$page.'&m=trashed'); ?>">View Trashed Records</a>
		<?php
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function btn_records( $page, $params ){
	
	$result = get_btn_records( $page, $params );
	
	echo $result;
}

function get_btn_optn_records( $page, $params=array() ){
	$CI = get_instance();
	
	$params_default = array_merge(array(
		'id_field'=>'id',
		'display'=>array('edit','delete')
		), $params
	);
	extract($params_default);
	
	$download = $CI->usersrole->get_capability($capabilities, $ur_name, 'download');
	$view = $CI->usersrole->get_capability($capabilities, $ur_name, 'view');
	$edit = $CI->usersrole->get_capability($capabilities, $ur_name, 'edit');
	$delete = $CI->usersrole->get_capability($capabilities, $ur_name, 'delete');
	$trash = $CI->usersrole->get_capability($capabilities, $ur_name, 'trash');
	
	ob_start();
	if( in_array('download',$display) && $download && isset($download_url) ){
		?>
		<a class="btn btn-success btn-xs" href="<?php echo $download_url; ?>"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span></a>
		<?php
	}
	if( in_array('view',$display) && $view ){
		?>
		<a class="btn btn-info btn-xs" href="<?php echo site_url('c='.$page.'&m=view&'.$id_field.'='.$id); ?>" title="View Record ID #<?php echo $id; ?>"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
		<?php
	}
	if( in_array('edit',$display) && $edit ){
		?>
		<a class="btn btn-primary btn-xs" href="<?php echo site_url('c='.$page.'&m=edit&'.$id_field.'='.$id); ?>" title="Edit Record ID #<?php echo $id; ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
		<?php
	}
	if( in_array('delete',$display) && $delete ){
		?>
		<a class="btn btn-danger btn-xs confirmation" href="#delete-confirmation-<?php echo $id; ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true" title="Delete Record ID #<?php echo $id; ?>"></span></a>
		<div id="delete-confirmation-<?php echo $id; ?>" style="display:none;">
			<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&'.$id_field.'='.$id); ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Yes Delete</a></p>
		</div>
		<?php
	}
	if( in_array('trash',$display) && $trash ){
		?>
		<a class="btn btn-warning btn-xs confirmation" href="#trash-confirmation-<?php echo $id; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true" title="Trash Record ID #<?php echo $id; ?>"></span></a>
		<div id="trash-confirmation-<?php echo $id; ?>" style="display:none;">
			<p>Do you want to trash this record? <a class="btn btn-warning btn-sm" href="<?php echo site_url('c='.$page.'&m=trash&'.$id_field.'='.$id); ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>Yes Trash</a></p>
		</div>
		<?php
	}
	if( in_array('untrash',$display) && $trash ){
		?>
		<a class="btn btn-success btn-xs" href="<?php echo site_url('c='.$page.'&m=untrash&'.$id_field.'='.$id); ?>" title="Untrash Record ID #<?php echo $id; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
		<?php
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function get_modal_btn_optn_records( $page, $params=array() ){
	$CI = get_instance();
	 
	$params_default = array_merge(array(
		'display'=>array('edit','delete','trash'),
		'size'=>'btn-xs'
		), $params
	);
	extract($params_default);
	
	$view = $CI->usersrole->get_capability($capabilities, $ur_name, 'view');
	$edit = $CI->usersrole->get_capability($capabilities, $ur_name, 'edit');
	$delete = $CI->usersrole->get_capability($capabilities, $ur_name, 'delete');
	$trash = $CI->usersrole->get_capability($capabilities, $ur_name, 'trash');
	
	ob_start();
	if( in_array('view',$display) && $view ){
		?> 
		<button type="button" class="btn btn-info <?php echo $size; ?>" onclick="view_record('<?php echo $page; ?>',<?php echo $id; ?>)">View</button>
		<?php
	}
	if( in_array('edit',$display) && $edit ){
		?>
		<button type="button" class="btn btn-success <?php echo $size; ?>" onclick="edit_record('<?php echo $page; ?>',<?php echo $id; ?>)">Edit</button>
		<?php
	}
	if( in_array('delete',$display) && $delete ){
		?>
		<button type="button" class="btn btn-danger <?php echo $size; ?>" onclick="delete_record('<?php echo $page; ?>',<?php echo $id; ?>)">Delete</button>
		<?php
	}
	if( in_array('trash',$display) && $trash ){
		?>
		<button type="button" class="btn btn-warning <?php echo $size; ?>" onclick="trash_record('<?php echo $page; ?>',<?php echo $id; ?>)">Trash</button>
		<?php
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function modal_btn_optn_records( $page, $params ){
	
	$result = get_modal_btn_optn_records( $page, $params );
	
	echo $result;
}


function btn_optn_records( $page, $params ){
	
	$result = get_btn_optn_records( $page, $params );
	
	echo $result;
}

function get_btn_edit_settings( $page, $id, $params ){
	$CI = get_instance();
	
	$params_default = array_merge(array(
		'id_field'=>'id',
		'display'=>array('add')
		), $params
	);
	extract($params_default);
	
	$add = $CI->usersrole->get_capability($capabilities, $ur_name, 'add');
	
	ob_start();
	?>
	<a class="btn btn-default btn-sm" href="<?php echo site_url('c='.$page); ?>">Reset</a>
	<?php
	if( in_array('add',$display) && $add ){
		if( $id ){
			$add_name = 'Save Data';
		} else {
			$add_name = 'Add Data';
		}
		?>
		<button type="submit" class="btn btn-primary btn-sm required-form">
			<span class="glyphicon glyphicon-edit" aria-hidden="true"></span><?php echo $add_name; ?>
		</button>
		<?php
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	echo $contents;
}

function btn_edit_settings( $page, $id, $params ){
	
	$result = get_btn_edit_settings( $page, $id, $params );
	
	echo $result;
}
?>