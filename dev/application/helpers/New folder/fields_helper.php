<?php	
function app_value( $array_object, $name, $default_value=NULL ){
	
	$value = app_get_value( $array_object, $name, $default_value );
	
	echo $value;
}

function app_get_value( $array_object, $name, $default_value=NULL ){
	
	if( is_array($array_object) ){
		$value = isset($array_object[$name])?$array_object[$name]:NULL;
	} elseif( is_object($array_object) ){
		$value = isset($array_object->$name)?$array_object->$name:NULL;
	} else {
		$value = $array_object;
	}
	
	if( $value == '' && isset($default_value) ){
		$value = $default_value;
	}
	
	return $value;
}

function app_field( $field_name, $model, $data, $params ){
	
	$defaults = array_merge(array(
		'type'=>'text',
		'class'=>'',
		'id'=>'',
		'holder'=>'',
		'attr'=>'',
		'max'=>'',
		'min'=>'',
		'req'=>FALSE,
		'read'=>FALSE,
		'has_value'=>TRUE,
		'compare_values'=>1,
		'value'=>'',
		'default'=>''
		), $params
	);
	extract($defaults);
	
	$required = $req?'required="required"':'';
	$readonly = $read?'readonly="readonly"':'';
	$id = $id?'id="'.$id.'"':'';
	$maxlength = $max?'maxlength="'.$max.'"':'';
	$minlength = $min?'maxlength="'.$min.'"':'';
	$placeholder = $holder?'placeholder="'.$holder.'"':'';
	
	$attributes = $attr.' '.$required.' '.$readonly.' '.$id.' '.$maxlength.' '.$minlength.' '.$placeholder;
	
	if( is_object($model) ){
		$prefix = $model->tblpref;
		$tag_name = $model->tblname.'['.$prefix.$field_name.']';
	} else {
		$prefix = $model;
		$tag_name = $prefix.$field_name;
	}
	
	$current_value = '';
	$default_value = FALSE;
	if( $has_value ){		
		$current_value = app_get_value( $data, $prefix.$field_name );
		if( empty($current_value) && $default ){
			$current_value = is_bool($default)?$value:$default;
			$default_value = TRUE;
		}
	}
	if( in_array($type,array('checkbox','radio')) ){
		if( $type == 'checkbox' ){
			if( $default_value == FALSE ){
				$current_value = $current_value?@unserialize($current_value):$current_value;
			}
			$tag_name = $tag_name.'[]';		
		}
		$checked = get_checked($value, $current_value);
		
		$result = '<input type="'.$type.'" class="'.$class.'" name="'.$tag_name.'" value="'.$value.'" '.$attributes.' '.$checked.' />';
	} elseif( $type == 'textarea' ){		
		$value = $value?$value:$current_value;
		$result = '<textarea class="form-control '.$class.'" name="'.$tag_name.'" '.$attributes.'>'.$value.'</textarea>';
	} else {
		$value = $value?$value:$current_value;
		
		$result = '<input type="'.$type.'" class="form-control '.$class.'" name="'.$tag_name.'" value="'.$value.'" '.$attributes.' />';
	}
	
	echo $result;
}
?>