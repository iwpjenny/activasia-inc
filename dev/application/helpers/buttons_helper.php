<?php
function app_get_btn( $params ){
	
	$params_default = array_merge(array(
		'label'=>'View',
		'element'=>'button',
		'url'=>NULL,
		'size'=>'sm',
		'btn_type'=>'default',
		'icon'=>NULL,
		'id'=>NULL,
		'confirmation'=>array(''),	
		), $params
	);
	extract($params_default);
	
	if( $icon ){
		$icon_tag = '<span class="glyphicon glyphicon-'.$icon.'" aria-hidden="true"></span> ';
	}
	
	ob_start();
	if( $element == 'link' ){
		?>
		<a class="btn btn-<?php echo $btn_type; ?> btn-<?php echo $size; ?>" href="<?php echo $url; ?>"><?php echo $icon_tag; ?><?php echo $label; ?></a>
		<?php
	} elseif( $element == 'submit' ){
		?>
		<a class="btn btn-<?php echo $btn_type; ?> btn-<?php echo $size; ?>" href="<?php echo $url; ?>"><?php echo $icon_tag; ?><?php echo $label; ?></a>
		<?php
	} elseif( $element == 'confirmation' ){
		?>
		<a class="btn btn-<?php echo $btn_type; ?> btn-<?php echo $size; ?> confirmation" href="#delete-confirmation-<?php echo $id; ?>">
		<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <?php echo $label; ?></a>
		<div id="delete-confirmation-<?php echo $id; ?>" style="display:none;">
			<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&'.$id_field.'='.$id); ?>">
			<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Yes Delete</a></p>
		</div>
		<?php
	} else {
		
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function get_btn_view( $page, $params ){
	$CI = get_instance();
	
	$params_default = array_merge(array(
		'id_field'=>'id',
		'display'=>array('back','edit','delete','trash'),
		'delete_url'=>NULL,
		'edit_url'=>NULL,
		'back_url'=>NULL
		), $params
	);
	extract($params_default);
	
	if( isset($model) ){
		$tblpref = app_get_val($model,'tblpref');
		$ur_name = $model->urc_name;
		if( isset($fields) ){
			$trashed = app_get_val($fields,$tblpref.'trashed');
		}
	}
	
	$edit = $CI->usersrole->get_capability($capabilities, $ur_name, 'edit');
	$delete = $CI->usersrole->get_capability($capabilities, $ur_name, 'delete');
	$trash = $CI->usersrole->get_capability($capabilities, $ur_name, 'trash');
	
	ob_start();
	if( in_array('back',$display) ){
		if( isset($trashed) && $trashed == 1 ){
			$back_url_default = site_url('c='.$page.'&m=trashed');
		} else {
			$back_url_default = site_url('c='.$page);
		}
		$back_url = $back_url ? $back_url : $back_url_default;
		?>
		<a class="btn btn-default btn-sm" href="<?php echo $back_url; ?>">Back</a>
		<?php
	}
	if( in_array('edit',$display) && $edit ){
		$edit_url = $edit_url ? $edit_url : 'c='.$page.'&m=edit&'.$id_field.'='.$id;
		?>
		<a class="btn btn-primary btn-sm" href="<?php echo site_url($edit_url); ?>">
		<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>
		<?php
	}
	if( in_array('delete',$display) && $delete ){		
		$delete_url = $delete_url ? $delete_url : 'c='.$page.'&m=delete&'.$id_field.'='.$id;
		if( isset($trashed) ){
			if( $trashed == 1 ){
				?>
				<a class="btn btn-danger btn-sm confirmation" href="#delete-confirmation-<?php echo $id; ?>">
				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete</a>
				<div id="delete-confirmation-<?php echo $id; ?>" style="display:none;">
					<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url($delete_url); ?>">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Yes Delete</a></p>
				</div>
				<?php
			}
		} else {
			?>
			<a class="btn btn-danger btn-sm confirmation" href="#delete-confirmation-<?php echo $id; ?>">
			<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete</a>
			<div id="delete-confirmation-<?php echo $id; ?>" style="display:none;">
				<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url('c='.$page.'&m=delete&'.$id_field.'='.$id); ?>">
				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Yes Delete</a></p>
			</div>
			<?php
		}
	}
	if( $trash && isset($trashed) ){
		?>
		<a class="btn btn-default btn-sm" href="<?php echo site_url('c='.$page.'&m=trashed'); ?>">
		<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Trashed Records</a>
		<?php
		if( $trashed == 0 ){
		?>
		<a class="btn btn-warning btn-sm confirmation" href="#trash-confirmation-<?php echo $id; ?>">
		<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Trash Record ID #<?php echo $id; ?>"></span> Trash</a>
		<div id="trash-confirmation-<?php echo $id; ?>" style="display:none;">
			<p>Do you want to trash this record? <a class="btn btn-warning btn-sm" href="<?php echo site_url('c='.$page.'&m=trash&'.$id_field.'='.$id); ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Yes Trash</a></p>
		</div>
		<?php
		} else {
		?>
		<a class="btn btn-success btn-sm" href="<?php echo site_url('c='.$page.'&m=untrash&'.$id_field.'='.$id); ?>">
		<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Untrash</a>
		<?php
		}
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function btn_view( $page, $params ){
	
	$return = get_btn_view( $page, $params );
	
	echo $return;
}

function get_btn_edit( $page, $params ){
	$CI = get_instance();
	
	$params_default = array_merge(array(
		'id_field'=>'id',
		'display'=>array('back','edit','add','view','delete','trash'),
		'add_url'=>NULL,
		'delete_url'=>NULL,
		'view_url'=>NULL,
		'back_url'=>NULL
		), $params
	);
	extract($params_default);
	
	if( isset($model) ){
		$tblpref = app_get_val($model,'tblpref');
		$ur_name = $model->urc_name;
		if( isset($fields) ){
			$trashed = app_get_val($fields,$tblpref.'trashed');
		}
	}
	
	$edit = $CI->usersrole->get_capability($capabilities, $ur_name, 'edit');
	$add = $CI->usersrole->get_capability($capabilities, $ur_name, 'add');
	$view = $CI->usersrole->get_capability($capabilities, $ur_name, 'view');
	$trash = $CI->usersrole->get_capability($capabilities, $ur_name, 'trash');
	$delete = $CI->usersrole->get_capability($capabilities, $ur_name, 'delete');
	$reset = $CI->usersrole->get_capability($capabilities, $ur_name, 'reset');
	
	ob_start();
	if( in_array('back',$display) ){
		if( isset($trashed) && $trashed == 1 ){
			$back_url_default = site_url('c='.$page.'&m=trashed');
		} else {
			$back_url_default = site_url('c='.$page);
		}
		$back_url = $back_url ? $back_url : $back_url_default;
		?>
		<a class="btn btn-default btn-sm" href="<?php echo $back_url; ?>">Back</a>
		<?php
	}
	if( $id ){
		if( in_array('edit',$display) && $edit ){
			?>
			<button type="submit" class="btn btn-primary btn-sm js-required-fields">
				<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Update
			</button>
			<?php
		}
		if( in_array('add',$display) && $add ){
			$add_url = $add_url ? $add_url : 'c='.$page.'&m=edit';
			?>
			<a class="btn btn-success btn-sm" href="<?php echo site_url($add_url); ?>">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New</a>
			<?php
		}
		if( in_array('view',$display) && $view ){
			$view_url = $view_url ? $view_url : 'c='.$page.'&m=view&'.$id_field.'='.$id;
			?>
			<a class="btn btn-info btn-sm" href="<?php echo site_url($view_url); ?>">
			<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> View</a>
			<?php
		}
		if( in_array('delete',$display) && $delete ){			
			$delete_url = $delete_url ? $delete_url : 'c='.$page.'&m=delete&'.$id_field.'='.$id;			
			if( isset($trashed) ){
				if( $trashed == 1 ){
					?>
					<a class="btn btn-danger btn-sm confirmation" href="#delete-confirmation-<?php echo $id; ?>">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete</a>
					<div id="delete-confirmation-<?php echo $id; ?>" style="display:none;">
						<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url($delete_url); ?>">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Yes Delete</a></p>
					</div>
					<?php
				}
			} else {
				?>
				<a class="btn btn-danger btn-sm confirmation" href="#delete-confirmation-<?php echo $id; ?>">
				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete</a>
				<div id="delete-confirmation-<?php echo $id; ?>" style="display:none;">
					<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url($delete_url); ?>">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Yes Delete</a></p>
				</div>
				<?php
			}
		}
		if( $trash && isset($trashed) ){
			?>
			<a class="btn btn-default btn-sm" href="<?php echo site_url('c='.$page.'&m=trashed'); ?>">
			<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Trashed Records</a>
			<?php
			if( $trashed == 0 ){
			?>
			<a class="btn btn-warning btn-sm confirmation" href="#trash-confirmation-<?php echo $id; ?>">
			<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Trash Record ID #<?php echo $id; ?>"></span> Trash</a>
			<div id="trash-confirmation-<?php echo $id; ?>" style="display:none;">
				<p>Do you want to trash this record? <a class="btn btn-warning btn-sm" href="<?php echo site_url('c='.$page.'&m=trash&'.$id_field.'='.$id); ?>">
				<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Yes Trash</a></p>
			</div>
			<?php
			} else {
			?>
			<a class="btn btn-success btn-sm" href="<?php echo site_url('c='.$page.'&m=untrash&'.$id_field.'='.$id); ?>">
			<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Untrash</a>
			<?php
			}
		}
	} else {
		if( in_array('add',$display) && $add ){
			?>
			<button type="submit" class="btn btn-success btn-sm js-required-fields">
			<span class="glyphicon glyphicon-save" aria-hidden="true"></span> Save Now</button>
			<?php
		}
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	echo $contents;
}

function btn_edit( $page, $params ){
	
	$result = get_btn_edit( $page, $params );
	
	echo $result;
}

function get_btn_records( $page, $params=array() ){
	$CI = get_instance();
	
	$action = $CI->input->get('m');
	
	$params_default = array_merge(array(
		'id_field'=>'id',
		'display'=>array('add','trash'),
		'add_url'=>NULL,
		'back_url'=>NULL
		), $params
	);
	extract($params_default);
	
	if( isset($model) ){
		$ur_name = $model->urc_name;
	}
	
	$records = $CI->usersrole->get_capability($capabilities, $ur_name, 'records');
	$add = $CI->usersrole->get_capability($capabilities, $ur_name, 'add');
	$trash = $CI->usersrole->get_capability($capabilities, $ur_name, 'trash');
	
	ob_start();
	if( in_array('records',$display) && $records ){		
		$back_url = $back_url ? $back_url : 'c='.$page;
		?>
		<a class="btn btn-default btn-sm" href="<?php echo site_url($back_url); ?>">
		<span class="glyphicon glyphicon-list" aria-hidden="true"></span> Back to Records</a>
		<?php
	}
	if( in_array('add',$display) && $add ){
		$add_url = $add_url ? $add_url : 'c='.$page.'&m=edit';
		?>
		<a class="btn btn-success btn-sm" href="<?php echo site_url($add_url); ?>">
		<span class="glyphicon glyphicon-save" aria-hidden="true"></span> Add New</a>
		<?php
	}
	if( $trash && $action != 'trashed' ){
		?>
		<a class="btn btn-default btn-sm" href="<?php echo site_url('c='.$page.'&m=trashed'); ?>">
		<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Trashed Records</a>
		<?php
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function btn_records( $page, $params ){
	
	$result = get_btn_records( $page, $params );
	
	echo $result;
}

function get_btn_optn_records( $page, $params=array() ){
	$CI = get_instance();
	
	$params_default = array_merge(array(
		'id_field'=>'id',
		'display'=>array('edit','delete'),
		'delete_url'=>NULL,
		'edit_url'=>NULL,
		'view_url'=>NULL
		), $params
	);
	extract($params_default);
	
	if( isset($model) ){
		$tblpref = app_get_val($model,'tblpref');
		$ur_name = $model->urc_name;
		if( isset($fields) ){
			$trashed = app_get_val($fields,$tblpref.'trashed');
		}
	}
	
	$download = $CI->usersrole->get_capability($capabilities, $ur_name, 'download');
	$view = $CI->usersrole->get_capability($capabilities, $ur_name, 'view');
	$edit = $CI->usersrole->get_capability($capabilities, $ur_name, 'edit');
	$delete = $CI->usersrole->get_capability($capabilities, $ur_name, 'delete');
	$trash = $CI->usersrole->get_capability($capabilities, $ur_name, 'trash');
	
	ob_start();
	if( in_array('download',$display) && $download && isset($download_url) ){
		?>
		<a class="btn btn-success btn-xs" href="<?php echo $download_url; ?>">
		<span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span></a>
		<?php
	}
	if( in_array('view',$display) && $view ){
		$view_url = $view_url ? $view_url : 'c='.$page.'&m=view&'.$id_field.'='.$id;
		?>
		<a class="btn btn-info btn-xs" href="<?php echo site_url($view_url); ?>" title="View Record ID #<?php echo $id; ?>">
		<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
		<?php
	}
	if( in_array('edit',$display) && $edit ){
		$edit_url = $edit_url ? $edit_url : 'c='.$page.'&m=edit&'.$id_field.'='.$id;
		?>
		<a class="btn btn-primary btn-xs" href="<?php echo site_url($edit_url); ?>" title="Edit Record ID #<?php echo $id; ?>">
		<span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
		<?php
	}
	if( in_array('delete',$display) && $delete ){
		$delete_url = $delete_url ? $delete_url : 'c='.$page.'&m=delete&'.$id_field.'='.$id;
		if( isset($trashed) ){
			if( $trashed == 1 ){
				?>
				<a class="btn btn-danger btn-xs confirmation" href="#delete-confirmation-<?php echo $id; ?>">
				<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Delete Record ID #<?php echo $id; ?>"></span></a>
				<div id="delete-confirmation-<?php echo $id; ?>" style="display:none;">
					<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url($delete_url); ?>">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Yes Delete</a></p>
				</div>
				<?php
			}
		} else {
			?>
			<a class="btn btn-danger btn-xs confirmation" href="#delete-confirmation-<?php echo $id; ?>">
			<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Delete Record ID #<?php echo $id; ?>"></span></a>
			<div id="delete-confirmation-<?php echo $id; ?>" style="display:none;">
				<p>Do you want to delete this record? <a class="btn btn-danger btn-sm" href="<?php echo site_url($delete_url); ?>">
				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Yes Delete</a></p>
			</div>
			<?php
		}
	}
	if( $trash && isset($trashed) ){
		if( $trashed == 0 ){
			?>
			<a class="btn btn-warning btn-xs confirmation" href="#trash-confirmation-<?php echo $id; ?>">
			<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Trash Record ID #<?php echo $id; ?>"></span></a>
			<div id="trash-confirmation-<?php echo $id; ?>" style="display:none;">
				<p>Do you want to trash this record? <a class="btn btn-warning btn-sm" href="<?php echo site_url('c='.$page.'&m=trash&'.$id_field.'='.$id); ?>">
				<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Yes Trash</a></p>
			</div>
			<?php
		} else {
			?>
			<a class="btn btn-success btn-xs" href="<?php echo site_url('c='.$page.'&m=untrash&'.$id_field.'='.$id); ?>" title="Untrash Record ID #<?php echo $id; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
			<?php
		}
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function get_modal_btn_optn_records( $page, $params=array() ){
	$CI = get_instance();
	 
	$params_default = array_merge(array(
		'display'=>array('edit','delete','trash'),
		'size'=>'btn-xs',
		'type'=>'record',
		'delete_only'=>FALSE,
		'confirmaiton'=>FALSE
		), $params
	);
	extract($params_default);
	
	if( isset($model) && isset($fields) ){
		$tblpref = app_get_val($model,'tblpref');
		$trashed = app_get_val($fields,$tblpref.'trashed');
		$ur_name = $model->urc_name;
	}
	
	$view = $CI->usersrole->get_capability($capabilities, $ur_name, 'view');
	$edit = $CI->usersrole->get_capability($capabilities, $ur_name, 'edit');
	$delete = $CI->usersrole->get_capability($capabilities, $ur_name, 'delete');
	$trash = $CI->usersrole->get_capability($capabilities, $ur_name, 'trash');
	
	ob_start();
	if( in_array('view',$display) && $view ){
		?> 
		<button type="button" class="btn btn-info <?php echo $size; ?>" onclick="json_view_<?php echo $type; ?>('<?php echo $page; ?>',<?php echo $id; ?>)"> View</button>
		<?php
	}
	if( in_array('edit',$display) && $edit ){
		?>
		<button type="button" class="btn btn-success <?php echo $size; ?>" onclick="json_edit_<?php echo $type; ?>(this,<?php echo $id; ?>)"> Edit</button>
		<?php
	}
	if( in_array('delete',$display) && $delete ){
		if( isset($trashed) && $delete_only === FALSE ){
			if( $trashed == 1 ){
				if( $confirmaiton ){
				?>
				<button type="button" class="btn btn-danger <?php echo $size; ?>" onclick="json_trash_confirmation_<?php echo $type; ?>(this,<?php echo $id; ?>,'<?php echo $title; ?>')"> Trash</button>
				<?php
				} else {
				?>
				<button type="button" class="btn btn-danger <?php echo $size; ?>" onclick="json_trash_<?php echo $type; ?>(this,<?php echo $id; ?>)"> Trash</button>
				<?php
				}
			}
		} else {
			if( $confirmaiton ){
			?>
			<button type="button" class="btn btn-danger <?php echo $size; ?>" onclick="json_delete_confirmation_<?php echo $type; ?>(this,<?php echo $id; ?>,'<?php echo $title; ?>')">Delete</button>
			<?php
			} else {
			?>
			<button type="button" class="btn btn-danger <?php echo $size; ?>" onclick="json_delete_<?php echo $type; ?>(this,<?php echo $id; ?>)"> Delete</button>
			<?php
			}
		}
	}
	if( $trash && isset($trashed) && $delete_only === FALSE ){
		if( $confirmaiton ){
		?>
		<button type="button" class="btn btn-warning <?php echo $size; ?>" onclick="json_trash_confirmation_<?php echo $type; ?>(this,<?php echo $id; ?>,'<?php echo $title; ?>')"> Trash</button>
		<?php
		} else {
		?>
		<button type="button" class="btn btn-warning <?php echo $size; ?>" onclick="json_trash_<?php echo $type; ?>(this,<?php echo $id; ?>)"> Trash</button>
		<?php
		}
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function modal_btn_optn_records( $page, $params ){
	
	$result = get_modal_btn_optn_records( $page, $params );
	
	echo $result;
}


function btn_optn_records( $page, $params ){
	
	$result = get_btn_optn_records( $page, $params );
	
	echo $result;
}

function get_btn_edit_settings( $page, $id, $params ){
	$CI = get_instance();
	
	$params_default = array_merge(array(
		'id_field'=>'id',
		'display'=>array('add')
		), $params
	);
	extract($params_default);
	
	if( isset($model) ){
		$ur_name = $model->urc_name;
	}
	
	$add = $CI->usersrole->get_capability($capabilities, $ur_name, 'add');
	
	ob_start();
	?>
	<a class="btn btn-default btn-sm" href="<?php echo site_url('c='.$page); ?>"> Reset</a>
	<?php
	if( in_array('add',$display) && $add ){
		if( $id ){
			$add_name = 'Save Data';
		} else {
			$add_name = 'Add Data';
		}
		?>
		<button type="submit" class="btn btn-primary btn-sm required-form">
			<span class="glyphicon glyphicon-edit" aria-hidden="true"></span><?php echo $add_name; ?>
		</button>
		<?php
	}
	$contents = ob_get_contents();
	ob_end_clean();
	
	echo $contents;
}

function btn_edit_settings( $page, $id, $params ){
	
	$result = get_btn_edit_settings( $page, $id, $params );
	
	echo $result;
}
?>