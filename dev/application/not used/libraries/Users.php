<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users {
	
	private $settings;
	private $Data;
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	
	/* parameters */
	public $id = NULL;
	public $session_user_id = FALSE;
	public $exempted_role = array('developer','iwpadmin');
	public $model_user;
	public $model_role;
	public $model_profile;

    function __construct(){
        $CI =& get_instance();
		
		$this->Data = $CI->Data;
    }

    function __destruct(){
        $this->exempted_role = NULL;
        $this->Role = NULL;
		
        $this->id = NULL;
        $this->Data = NULL;
    }
	
	public function record( $model, $query_params_or_id=array() ){
				
		if( is_array($query_params_or_id) ){
			$where_params = $query_params_or_id;
		} else {	
			$where_params = array( $model->tblname.'.'.$model->tblid => $query_params_or_id ); 
		}
		
		$join_params = $model->set_join_params(); 
		$query_params = array('where_params'=>$where_params,'join_params'=>$join_params );
		
		$fields = $this->Data->get_record( $model, array(
			'where_params'=>$where_params,
			'join_params'=>$join_params 
		));
		
		return $fields;
	}
	
	public function records( $model, $data, $where_params=array() ){ 
        $CI =& get_instance();
		
		$search_fields = $model->set_search_fields(); 
		$join_params = $model->set_join_params();
		
		$query_params = array(
			'join_params'=>$join_params,
			'search_fields'=> $search_fields,
			'where_params'=> $where_params,
			'paging'=>TRUE
		);  
		$records = $this->Data->get_records( $model, $query_params ); 
		$data['record_rows'] = $records;
		$data['pagination'] =  $this->Data->pagination_link; 
		$data['per_page'] = $this->Data->set_per_page;
		$data['n'] = 0;
		
		return $data;
	}
	
	public function update_password( $Model, $id, $password, $confirm_password ){
		$CI =& get_instance();
		
		$result = FALSE;
		
		if( empty($password) == FALSE ){
			if( $password == $confirm_password ){
				$data = array(
					$Model->tblpref.'password'=>$password
				);
				$update_password = $CI->Data->update_data( $Model, $data, array( $Model->tblid => $id ) ); 
				
				if( $update_password ){
					$result = TRUE;
					
					$details = $CI->Data->get_record( $Model, array('where_params'=>array($Model->tblid=>$id)));
					$lastname = app_get_val($details,$Model->tblpref.'lastname');
					$firstname = app_get_val($details,$Model->tblpref.'firstname');
					
					$notify_params = array('action'=>'password','type'=>'change-success','log'=>TRUE);
				} else {
					$notify_params = array('action'=>'password','type'=>'change-error','log'=>TRUE);
				}
			} else {
				$notify_params = array('action'=>'password','type'=>'did-not-match','log'=>TRUE);
			}
			$CI->notify->set( $notify_params );
		}
		
		return $result;
	}
	
	public function save( $ModUser, $ModProf, $pic_folder ){
		$CI =& get_instance();
		
		$id = $CI->input->post('id');
		
		$field_data = $CI->input->post($ModUser->tblname);

		$username = app_get_val($field_data,$ModUser->tblpref.'username');
		$email = app_get_val($field_data,$ModUser->tblpref.'email');
		
		$password = $CI->input->post($ModUser->tblpref.'password');
		$confirm_password = $CI->input->post($ModUser->tblpref.'confirm_password');
		/*---------------------------------------------*/
		
		$if_username_exist = $this->if_username_exist( $ModUser, $username, $id );
		$if_email_exist = $this->if_email_exist( $ModUser, $email, $id );
		
		if( $if_username_exist ){
			$notify_params = array('action'=>'username','type'=>'taken','log'=>TRUE);
			$CI->notify->set( $notify_params );
		} elseif( $if_email_exist ){
			$notify_params = array('action'=>'email','type'=>'taken','log'=>TRUE);
			$CI->notify->set( $notify_params );
		} else { 
			$field_data_user = $CI->input->post($ModUser->tblname);
			$field_data_profile = $CI->input->post($ModProf->tblname);
		
			if( $id ){
				$CI->usersrole->has_user_has_capability($ModUser->urc_name,'edit');
				
				$update_main = $CI->records->save( $ModUser, $field_data_user, $id );
				
				if( $update_main ){					
					$this->update_password( $ModUser, $id, $password, $confirm_password );
					
					$update_profile = $CI->records->save( $ModProf, $field_data_profile, array($ModUser->tblid=>$id) );
					 
					if( $update_profile ){						
						$user = $CI->Data->get_record($ModUser, array($ModUser->tblid=>$id));
						$lastname = app_get_val($user,$ModUser->tblpref.'lastname');
						$firstname = app_get_val($user,$ModUser->tblpref.'firstname');
						
						$notify_params = array('action'=>'profile','type'=>'update-success','log'=>TRUE);
					} else {
						$notify_params = array('action'=>'profile','type'=>'update-error','log'=>TRUE);
					}			
				} else {
					$notify_params = array('action'=>'update','type'=>'success','log'=>TRUE);
				}
				$CI->notify->set( $notify_params );
				
				$this->upload_picture( $id, $ModUser, $ModProf, $pic_folder );				
			} else {
				$CI->usersrole->has_user_has_capability($ModUser->urc_name,'add');
				
				if( empty($password) ){
					$CI->notify->set('Password must not be empty', 'warning');
				} elseif( $password != $confirm_password ){
					$CI->notify->set('Password and Confirm Password must be equal', 'warning');
				} else {
					$id = $CI->records->save( $ModUser, $field_data_user );
						
					if( $id ){
						$this->update_password( $ModUser, $id, $password, $confirm_password );
						
						$field_data_profile = array_merge($field_data_profile,array($ModUser->tblid=>$id));
						$profile_id = $CI->records->save( $ModProf, $field_data_profile );
						
						if( $profile_id ){
							$this->upload_picture( $id, $ModUser, $ModProf, $pic_folder );
							
							$user = $CI->Data->get_record($ModUser, array($ModUser->tblid=>$id));
							$lastname = app_get_val($user,$ModUser->tblpref.'lastname');
							$firstname = app_get_val($user,$ModUser->tblpref.'firstname');
							
							$notify_params = array('action'=>'profile','type'=>'add-success','log'=>TRUE);
						} else {
							$notify_params = array('action'=>'profile','type'=>'add-error','log'=>TRUE);
						}
						$CI->notify->set( $notify_params );
					
						redirect( site_url( 'c='.$ModUser->page.'&m=edit&id='.$id ) );
					} else {
						$notify_params = array('action'=>'add','type'=>'success','log'=>TRUE);
						$CI->notify->set( $notify_params );
					}
				}
			}
		}
		
		redirect_current_page();
	}
	
	private function upload_picture( $id, $ModUser, $ModProf, $pic_folder ){
		$CI =& get_instance();
		$CI->load->library('pic');
		
		$CI->pic->id = $id;
		$CI->pic->folder = $pic_folder;
		$CI->pic->model_main = $ModUser;
		$CI->pic->model_prof = $ModProf;
		$CI->pic->upload();
	}
	
	public function delete( $ModUser, $ModProf ){
		$CI =& get_instance();
		
		$CI->usersrole->has_user_has_capability($ModUser->urc_name,__FUNCTION__);
		
		$id = $CI->input->get('id');
		$popup = $CI->input->get('popup');
		
		$details = $CI->Data->get_record( $ModUser, array( 'where_params'=>array($ModUser->tblid=>$id) ) ); 
		
		if( $details ){
			$username = app_get_val( $details, $ModUser->tblpref.'username');
			$lastname = app_get_val( $details, $ModUser->tblpref.'lastname'); 
			$firstname = app_get_val( $details, $ModUser->tblpref.'firstname');
			
			if( in_array($username, $exempted_role) == FALSE ){
				$delete1 = $CI->Data->delete_data( $ModUser, array($ModUser->tblid=>$id) );
				$delete2 = $CI->Data->delete_data( $ModProf, array($ModProf->tblid=>$id) );

				if( $delete1 && $delete2 ){
					$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE);
				} else {
					$notify_params = array('action'=>'delete','type'=>'error','log'=>TRUE);
				}
			} else {
				$notify_params = array('action'=>'user','type'=>'can-not-delete','log'=>TRUE);
			}			
			$CI->notify->set( $notify_params );
			
			if( $popup ){
				redirect_current_page();
			}
		} else {
			$notify_params = array('action'=>'record','type'=>'not-exist','log'=>TRUE);
			$CI->notify->set( $notify_params );
		}
		
		redirect( site_url('c='.$ModUser->page) );
	}
	
	public function get_data_records_except_logged_user( $data, $model ){
		$CI =& get_instance();
				
		$logged_user_id = $CI->logged->get_login_session('user_id');
		$where_params = array($model->tblname.'.'.$model->tblid.' <>' => $logged_user_id);
		
		$data = $this->records( $model, $data, $where_params );
		
		return $data;
	}

	public function get_edit( $model, $data ){
		$CI =& get_instance();
		$CI->load->library('pic');
		
		$id = $CI->input->get('id');
		$data['id'] = $id;
		
		$fields = $this->record( $model, $id );
		$data['fields'] = $fields;
		
		$data['select_role'] = $this->model_role->get_select( $model, $fields );
		
		$CI->pic->folder = 'user';
		$CI->pic->model_main = $model;
		$CI->pic->model_prof = $this->model_profile;
		$data['picture'] = $CI->pic->get_img_by_id( $id );
		
		return $data;
	}

	public function get_edit_profile( $model, $data, $update_role=FALSE ){
		$CI =& get_instance();
		$CI->load->library('pic');
		
		$id = $CI->logged->get_login_session('user_id');
		$data['id'] = $id;
		
		$fields = $model->get_by_id( $id );
		$data['fields'] = $fields;
		
		if( $update_role ){
			$data['select_role'] = $this->model_role->get_select( $model, $fields );
		} else {
			$data['select_role'] = app_get_val($fields,$this->model_role->tblpref.'title');
		}
		
		$CI->pic->folder = 'user';
		$CI->pic->model_main = $model;
		$CI->pic->model_prof = $this->model_profile;
		$data['picture'] = $CI->pic->get_img_by_id( $id );
		
		return $data;
	}
	
	public function if_username_exist( $model, $username, $user_id='' ){
		$CI =& get_instance();
		
		if( $user_id ){
			$exist = $CI->Data->get_record( $model, 
				array('where_params'=>array(
					$model->tblpref.'username'=>$username,
					$model->tblid.' <>'=>$user_id
				)
			));
		} else {
			$exist = $CI->Data->get_record( $model, 
				array('where_params'=>array($model->tblpref.'username'=>$username)) 
			);
		}
		
		return $exist;
	}
	
	public function if_email_exist( $model, $email, $user_id='' ){
		$CI =& get_instance();
		
		if( $user_id ){
			$exist = $CI->Data->get_record( $model, 
				array('where_params'=>array(
					$model->tblpref.'email'=>$email,
					$model->tblid.' <>'=>$user_id
				)
			));
		} else {
			$exist = $CI->Data->get_record( $model, 
				array('where_params'=>array($model->tblpref.'email'=>$email)) 
			);
		}
		
		return $exist;
	}
}
?>