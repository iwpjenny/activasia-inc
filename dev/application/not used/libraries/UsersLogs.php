<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UsersLogs {
	
	private $settings;
	private $Data;
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	
	/* parameters */
	public $id;
	public $login_user_type;
	public $model_user;
	public $model_user_role;
	public $model_user_logs;
	public $model_log;
	
    function __construct( $params=array() ){
        $CI =& get_instance();
		
		$params_default = array_merge(array(
			'login_user_type'=>'admin',
			'model_user'=>NULL,
			'model_user_role'=>NULL,
			'model_user_logs'=>NULL,
			'model_log'=>NULL
			), $params
		);
		extract($params_default);
		
		$this->login_user_type = $login_user_type;
		$this->model_user = $model_user;
		$this->model_user_role = $model_user_role;
		$this->model_user_logs = $model_user_logs;
		$this->model_log = $model_log;
		
		$this->Data = $CI->Data;
    }

    function __destruct(){		
        $this->id = NULL;
        $this->Data = NULL;
		
		$this->login_user_type = NULL;
		$this->model_user = NULL;
		$this->model_user_role = NULL;
		$this->model_user_logs = NULL;
		$this->model_log = NULL;
    }
	
	public function record( $model, $query_params_or_id=array() ){
				
		if( is_array($query_params_or_id) ){
			$where_params = $query_params_or_id;
		} else {	
			$where_params = array( $model->tblname.'.'.$model->tblid => $query_params_or_id ); 
		}
		
		$join_params = $model->set_join_params(); 
		$query_params = array('where_params'=>$where_params,'join_params'=>$join_params );
		
		$fields = $this->Data->get_record( $model, array(
			'where_params'=>$where_params,
			'join_params'=>$join_params 
		));
		
		return $fields;
	}
	
	public function records( $model, $data, $where_params=array() ){ 
        $CI =& get_instance();
		
		$search_fields = $model->set_search_fields(); 
		$join_params = $model->set_join_params();
		
		$query_params = array(
			'join_params'=>$join_params,
			'search_fields'=> $search_fields,
			'where_params'=> $where_params,
			'groupby'=> $CI->db->dbprefix.$model->tblname.'.'.$model->tblid,
			'paging'=>TRUE
		);  
		$records = $this->Data->get_records( $model, $query_params ); 
		$data['record_rows'] = $records;
		$data['pagination'] =  $this->Data->pagination_link; 
		$data['per_page'] = $this->Data->set_per_page;
		$data['n'] = 0;
		
		return $data;
	}
	
	function save( $params, $action='view', $user_id='' ){
        $CI =& get_instance();
		
		$logged_user_id = $CI->logged->get_login_session('user_id');
		
		if( is_array($params) ){
			extract($params);
		} else {
			$description = $params;
		}
		
		$user_id = $user_id?$user_id:$logged_user_id;
		
		$data = array(
			$this->model_user->tblid=>$user_id,
			$this->model_user_logs->tblpref.'action'=>$action,
			$this->model_user_logs->tblpref.'description'=>$description
		);
		
		$id = $this->Data->insert_data( $this->model_user_logs, $data );
		
		return $id;
	}
	
	public function get_data_records_logged_users( $data, $model='' ){
		$CI =& get_instance();
		
		$model = $model?$model:$this->model_user_logs;
				
		$logged_user_id = $CI->logged->get_login_session('user_id');
		
		$where_params = array($this->model_user->tblname.'.'.$this->model_user->tblid=>$logged_user_id);
		$data = $this->records($model, $data, $where_params);
		
		return $data;
	}
	
	public function get_data_view_logged_user( $data, $model='' ){
		$CI =& get_instance();
		
		$model = $model?$model:$this->model_user_logs;
		
		$id = $CI->input->get('id');
		$data['id'] = $id;
		
		$logged_user_id = $CI->logged->get_login_session('user_id');
		$where_params = array(
			$model->tblname.'.'.$model->tblid=>$id,
			$this->model_user->tblname.'.'.$this->model_user->tblid=>$logged_user_id
		);
		
		$fields = $this->record($model, $where_params);
		$data['fields'] = $fields;
		
		return $data;
	}
}
?>