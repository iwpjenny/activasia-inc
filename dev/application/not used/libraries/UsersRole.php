<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UsersRole {
	
	private $Data;
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	
	/* parameters */
	public $id;
	public $model_user;
	public $model_user_role;
	public $model_log;
	
    function __construct( $params=array() ){
        $CI =& get_instance();
		
		$params_default = array_merge(array(
			'login_user_type'=>'admin',
			'model_user'=>NULL,
			'model_user_role'=>NULL,
			'model_log'=>NULL
			), $params
		);
		extract($params_default);
		
		$this->login_user_type = $login_user_type;
		$this->model_user = $model_user;
		$this->model_user_role = $model_user_role;
		$this->model_log = $model_log;
		
		$this->Data = $CI->Data;
    }

    function __destruct(){
        $this->exempted_role = NULL;
		
        $this->id = NULL;
        $this->Data = NULL;
        $this->model_log = NULL;
    }
	
	public function record( $model, $query_params_or_id=array() ){
				
		if( is_array($query_params_or_id) ){
			$where_params = $query_params_or_id;
		} else {	
			$where_params = array( $model->tblname.'.'.$model->tblid => $query_params_or_id ); 
		}
		
		$join_params = $model->set_join_params(); 
		$query_params = array('where_params'=>$where_params,'join_params'=>$join_params );
		
		$fields = $this->Data->get_record( $model, array(
			'where_params'=>$where_params,
			'join_params'=>$join_params 
		));
		
		return $fields;
	}
	
	public function records( $model, $data, $where_params=array() ){ 
        $CI =& get_instance();
		
		$search_fields = $model->set_search_fields(); 
		$join_params = $model->set_join_params();
		
		$query_params = array(
			'join_params'=>$join_params,
			'search_fields'=> $search_fields,
			'where_params'=> $where_params,
			'groupby'=> $CI->db->dbprefix.$model->tblname.'.'.$model->tblid,
			'paging'=>TRUE
		);  
		$records = $this->Data->get_records( $model, $query_params ); 
		$data['record_rows'] = $records;
		$data['pagination'] =  $this->Data->pagination_link; 
		$data['per_page'] = $this->Data->set_per_page;
		$data['n'] = 0;
		
		return $data;
	}
	
	public function save( $model ){
		$CI =& get_instance();
		
		$id = $CI->input->post($model->tblid); 
		
		$capabilities_selected = $CI->input->post('capabilities_selected'); 
		$capabilities_serialized = serialize($capabilities_selected);
		
		$array_capabilities = array(
			$model->tblpref.'capabilities_serialized' => $capabilities_serialized
		);
		$field_data = $CI->input->post($model->tblname);
		$field_data = array_merge($field_data, $array_capabilities);
		
		$position = app_get_val($field_data,$model->tblpref.'position');
		
		if( $id ){
			$this->has_user_has_capability($model->urc_name,'edit');
			
			$update = $CI->records->save( $model, $field_data, $id );
			
			if( $update ){
				$notify_params = array('action'=>'update','type'=>'success','log'=>TRUE);
			} else {
				$notify_params = array('action'=>'update','type'=>'error','log'=>TRUE);
			}
			$CI->notify->set( $notify_params );
			
			redirect_current_page();
		} else {
			$this->has_user_has_capability($model->urc_name,'add');
			
			$array_published = array(
				$model->tblpref.'published'=>1
			);
			$field_data = array_merge($field_data, $array_published);
			
			$id = $CI->records->save( $model, $field_data );
			
			if( $id ){
				$notify_params = array('action'=>'add','type'=>'success','log'=>TRUE);
			} else {
				$notify_params = array('action'=>'add','type'=>'error','log'=>TRUE);
			}
			$CI->notify->set( $notify_params );
			
			redirect( site_url('c='.$model->page.'&m=edit&id='.$id) );
		}
	}
	
	public function delete( $model ){
		$CI =& get_instance();
		
		$id = $CI->input->get('id');

		$user_role = $this->record($model, $id);
		$position = app_get_val($user_role,$model->tblpref.'position'); 
		
		if( $user_role ){
			$name = app_get_val($user_role, $model->tblpref.'name');
			
			if( in_array($name, $this->delete_exemption) == FALSE ){
				$delete = $this->Data->delete_data($model, array($model->tblid=>$id));
				
				if( $delete ){					
					$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE);
				} else {
					$notify_params = array('action'=>'delete','type'=>'error','log'=>TRUE);
				}
				$CI->notify->set( $notify_params );
			}
		}
		
		redirect( site_url('c='.$model->page) );
	}

	public function get_edit( $model, $data ){
		$CI =& get_instance();
		
		$id = $CI->input->get('id');
		
		$fields = $this->record( $model, $id );
		$data['fields']= $fields;
		
		$capabilities_serialized = app_get_val($fields, $model->tblpref.'capabilities_serialized');
		$data['selected_capabilities'] = unserialize($capabilities_serialized);
		
		$data['urc_standard_records'] = $model->get_models_urc();
		$data['urc_modules_records'] = $model->get_models_urc('modules/');
		
		return $data;
	}
	
	public function get_capability($capabilities, $urc_name, $field){
		
		$result = isset($capabilities[$urc_name][$field])?$capabilities[$urc_name][$field]:'';
		
		return $result;
	}
	
	public function check( $module, $name ){
		$CI =& get_instance();
		
		$dev_mode_urc = FALSE;
		
		$capabilities = $CI->logged->get_user_capabilities();
		
		$result = $this->get_capability($capabilities, $module, $name);
		
		if( $dev_mode_urc ){
			echo '<br /><strong style="color:blue;">module: '.$module.' | name: '.$name.' | result: '.$result.'</strong>';
		}
		
		return $result;
	}
	
	public function has_user_has_capability( $module, $name, $value='yes' ){
		$CI =& get_instance();
		
		$result = $this->check($module, $name);
		
		if( $result != $value ){
		
			$data = array();
			$data['url_login'] = site_url('c='.$this->model_log->page.'&url_current='.urlencode( url_current() ));
			$data['url_logout'] = site_url('c='.$this->model_log->page.'&m=logout&url_current='.urlencode( url_current() ));
			
			echo $CI->load->view('error-no-user-capability-permission',$data,TRUE);
			exit();
		}
	}
}
?>