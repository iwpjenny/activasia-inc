<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Module {
	
	private $settings;
	private $Data;
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	public $notification;
	
	/* parameters */
	public $id = NULL;
	public $page = NULL;
	public $model = NULL;
	public $model_parent = NULL;
	public $model_sub = NULL;
	public $model_user = NULL;
	public $model_log = NULL;
	public $model_files = NULL;
	public $module = NULL;
	public $location;
	public $logged_user_only = FALSE;
	public $fields;
	
	//public $data = array();
	public $primary_field = 'title';

    function __construct(){
        $CI =& get_instance();
		$CI->load->library('setconf');
		
		$CI->load->model('Data_Model','Data');
		
		$this->Data = $CI->Data;
		
		$this->settings = $CI->setconf->all();
    }

    function __destruct(){
        $this->id = NULL;
        $this->Data = NULL;
        $this->settings = NULL;
		
		$this->dashboard_page = NULL;
		$this->login_user_type = NULL;
		$this->model = NULL;
		$this->model_user = NULL;
		$this->model_log = NULL;
		$this->field_restrictions = NULL;
    }
	
	public function record( $model, $where_params_or_id ){
        $CI =& get_instance();
		
		if( is_array($where_params_or_id) ){
			$where_params = $where_params_or_id; 
		} else {
			$where_params = array( $model->tblname.'.'.$model->tblid => $where_params_or_id ); 
		}
		
		$join_params = $model->set_join_params(); 
		$fields = $this->Data->get_record( $model, array(
			'where_params'=>$where_params,
			'join_params'=>$join_params
		)); 
		//printr($fields);
		/* if( $fields ){
			$CI->load->library('moduletag');
			$CI->moduletag->unset_session_post();
		} */
		
		return $fields;
	}
	
	public function records( $model, $data, $where_params=array() ){ 
        $CI =& get_instance();
		
		$search_fields = $model->set_search_fields(); 
		$join_params = $model->set_join_params();
		
		$query_params = array(
			'join_params'=>$join_params,
			'search_fields'=> $search_fields,
			'where_params'=> $where_params,
			'paging'=>TRUE
		);
		$records = $this->Data->get_records( $model, $query_params ); 
		$data['record_rows'] = $records;
		$data['pagination'] =  $this->Data->pagination_link; 
		$data['per_page'] = $this->Data->set_per_page;
		$data['n'] = 0;
		
		return $data;
	}
	
	public function save( $module='', $model_files='' ){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$module = $module?$module:$this->module;
		$this->model = $module;
		$page = $this->page?$this->page:$module->page;
		$model_files = $model_files?$model_files:$this->model_files;
		/*---------------------------------------------*/
		
		$id = $CI->input->post('id'); 
		$field_data = $CI->input->post($module->tblname);
				
		$field_data = $this->logged_user_only( $field_data );
		$field_data = $this->serialize_array_data( $field_data );
		
		$CI->fields->model = $this->model;
		$error = $CI->fields->validate( $field_data );
		
		if( $error ){
			$CI->fields->set_session( $field_data );
			redirect_current_page();
		}
		
		if( $id ){
			$CI->usersrole->has_user_has_capability($module->urc_name,'edit');
			
			$save = $CI->records->save( $module, $field_data, $id );
			
			if( $save ){
				$details = $this->record($module, $id);
				$name = get_value($details, $module->tblpref.$this->primary_field);
								
				$notify_params = array('action'=>'update','type'=>'success','log'=>TRUE);
				$CI->fields->unset_session();
			} else {
				$notify_params = array('action'=>'update','type'=>'error','log'=>TRUE);
			}
			$CI->notify->set( $notify_params );
			
			if( is_object($model_files) ){
				$CI->load->library('files');
				$CI->files->id = $id;
				$CI->files->model_main = $module;
				$CI->files->model_files = $model_files;
				$CI->files->upload();
			}		
			
			redirect_current_page();
		} else {
			$CI->usersrole->has_user_has_capability($module->urc_name,'add');
			
			$id = $CI->records->save( $module, $field_data );
			
			if( $id ){
				if( is_object($model_files) ){
					$CI->load->library('files');
					$CI->files->id = $id;
					$CI->files->model_main = $module;
					$CI->files->model_files = $model_files;
					$CI->files->upload();
				}
			
				$details = $this->record($module, $id);
				$name = get_value($details,$module->tblpref.$this->primary_field);
				
				$notify_params = array('action'=>'add','type'=>'success','log'=>TRUE);
				$CI->notify->set( $notify_params );
				$CI->fields->unset_session();
				
				redirect( site_url( 'c='.$page.'&m=edit&id='.$id ) );
			} else {
				$notify_params = array('action'=>'add','type'=>'error','log'=>TRUE);
				$CI->notify->set( $notify_params );
				
				redirect_current_page();
			}					
		}
	}
	
	public function delete( $module, $ModuleFiles='' ){
		$CI =& get_instance();
		$CI->load->library('files');
		/*---------------------------------------------*/
		$module = $module?$module:$this->module;
		$page = $this->page?$this->page:$module->page;
		/*---------------------------------------------*/
		
		$CI->usersrole->has_user_has_capability($module->urc_name,__FUNCTION__);
		
		$id = $CI->input->get('id');
		
		$details = $this->Data->get_record( $module, array( 'where_params'=>array($module->tblid=>$id) ) ); 
		
		if( $details ){
			$name = get_value( $details, $module->tblpref.$this->primary_field);
			
			$delete = $this->Data->delete_data( $module, array($module->tblid=>$id) );
			
			if( $delete ){
				$notification = 'Deleted record (ID:'.$id.' - '.$name.')';
				$CI->userslogs->save('delete',$notification);
				$CI->notify->set($notification);
			
				if( is_object($ModuleFiles) ){
					$CI->files->model_main = $module;
					$CI->files->model_files = $ModuleFiles;
					$delete_all_files = $CI->files->delete_all( $id );
					
					if( $delete_all_files ){
						$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE);
						$CI->notify->set( $notify_params );
					} else {
						$notify_params = array('action'=>'file','type'=>'delete-error','log'=>TRUE);
						$CI->notify->set( $notify_params );
					}
				}
			} else {
				$notify_params = array('action'=>'file','type'=>'delete-error','log'=>TRUE);
				$CI->notify->set( $notify_params );
			}
		} else {
			$notify_params = array('action'=>'record','type'=>'not-exist','log'=>TRUE);
			$CI->notify->set( $notify_params );
		}
		
		redirect( site_url('c='.$page) );
	}
	
	public function trash( $module, $value=1 ){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$module = $module?$module:$this->module;
		$page = $this->page?$this->page:$module->page;
		/*---------------------------------------------*/
		
		$CI->usersrole->has_user_has_capability($module->urc_name,__FUNCTION__);
		
		$id = $CI->input->get('id');
		
		$details = $this->Data->get_record( $module, array( 'where_params'=>array($module->tblid=>$id) ) ); 
		
		if( $details ){
			$name = get_value( $details, $module->tblpref.$this->primary_field);
			
			$data = array($module->tblpref.'trashed'=>$value);
			$trash = $this->Data->update_data( $module, $data, array($module->tblid=>$id) );
			
			if( $trash ){
				$notify_params = array('action'=>'trash','type'=>'success','log'=>TRUE);
				$CI->notify->set( $notify_params );	
			} else {
				$notify_params = array('action'=>'trash','type'=>'error','log'=>TRUE);
				$CI->notify->set( $notify_params );	
			}
			
		} else {
			$notify_params = array('action'=>'record','type'=>'not-exist','log'=>TRUE);
			$CI->notify->set( $notify_params );
		}
		
		if( $value === 1 ){
			redirect( site_url('c='.$page.'&m=trashed') );
		} else {
			redirect( site_url('c='.$page) );
		}
	}
	
	public function delete_file( $Module, $ModuleFiles ){
		$CI =& get_instance();
		$CI->load->library('Files');
		/*---------------------------------------------*/
		$module = $module?$module:$this->module;
		$page = $this->page?$this->page:$module->page;
		/*---------------------------------------------*/
		
		$id = $CI->input->get('id'); 
		$fid = $CI->input->get('fid'); 
		
		$CI->files->id = $id;
		$CI->files->model_files = $ModuleFiles;
		$CI->files->delete( $fid );
					
		redirect( site_url( 'c='.$page.'&m=edit&id='.$id ) );
	}
	
	public function grid_tbody( $module, $module_parent, $parent_id ){
		$CI =& get_instance();
				
		$data['settings'] = $this->settings;
		$data['capabilities'] = $CI->logged->get_user_capabilities();
		
		$data['model'] = $module;
		$data['page'] = $module->page;
		$data['fpage'] = $module->fpage;
		$data['parent_id'] = $parent_id;
		$data['records'] = $this->grid_sub_records( $module, $module_parent, $parent_id );
				
		$contents = $CI->load->view($this->location.'/modules/'.$module->view.'/grid-tbody',$data,TRUE);
				
		return $contents;
	}
	
	public function modal_sub_section( $module, $module_parent, $parent_id ){ 
		$CI =& get_instance();
		
		$contents = '';
		
		if( $parent_id ){
			$data['settings'] = $this->settings;
			$data['location'] = $this->location;
			
			$tbody = $this->grid_tbody( $module, $module_parent, $parent_id );
			$thead = $CI->load->view($this->location.'/modules/'.$module->view.'/grid-thead',$data,TRUE);
			
			$data['id'] = '';
			$data['thead'] = $thead;
			$data['tbody'] = $tbody;
			$data['parent_id'] = $parent_id;
			$data['model'] = $module;
			$data['model_parent'] = $module_parent;
			$data['name'] = $module->name;
			$data['page'] = $module->page;
			$data['fpage'] = $module->fpage;
			$data['tblid'] = $module->tblid;
			$data['tblpref'] = $module->tblpref;
			$data['tblname'] = $module->tblname;
			$data['fields'] = array();
			$data['form_action'] = site_url('c='.$module->page);
			$CI->moduletag->model = $module;
			$CI->moduletag->data = array();
			
			$contents = $CI->load->view($this->location.'/modules/modal-sub-section',$data,TRUE);
		}
				
		return $contents;
	}
	
	public function grid_sub_records( $module, $module_parent, $query_params_or_id ){ 
		$CI =& get_instance();
		
		$id = $query_params_or_id;
				
		if( is_array($query_params_or_id) ){
			$query_params = $query_params_or_id;
		} else {
			$query_params = array('where_params'=>array($module_parent->tblid=>$id,$module->tblpref.'trashed'=>0));
		}
			
		$records = $this->Data->get_records( $module, $query_params ); 
		
		return $records;
	}
	
	public function logged_user_only( $field_data ){
		$CI =& get_instance();
		
		if( $this->logged_user_only ){
			$logged_user_id = $CI->logged->get_login_session('user_id');
			$field_data = array_merge($field_data,array($this->model_user->tblid=>$logged_user_id));
		}
		
		return $field_data;
	}
	
	private function serialize_array_data( $field_data ){
		$CI =& get_instance();
		
		$new_field_data = $field_data;
		if( $field_data ){
			foreach($field_data as $key=>$value){
				if( is_array($value) ){
					$value = serialize($value);
				}
				$new_field_data[$key] = $value;
			}
		}
		
		return $new_field_data;
	}
	
	public function json_save( $module, $module_parent, $module_files='' ){
		$CI =& get_instance();
		$CI->load->library('files');
		/*---------------------------------------------*/
		$module = $module?$module:$this->module;
		$page = $this->page?$this->page:$module->page;
		/*---------------------------------------------*/
		
		$parent_id = $CI->input->post('parent_id');
		$id = $CI->input->post('id'); 
		$field_data = $CI->input->post($module->tblname);
		
		$result = FALSE;
		
		if( $id ){
			$CI->usersrole->has_user_has_capability($module->urc_name,'edit');
			
			$save = $CI->records->save( $module, $field_data, $id );
			
			if( $save ){				
				$details = $this->Data->get_record($module, array($module->tblid=>$id));
				$name = get_value($details,$module->tblpref.$this->primary_field);
								
				$notify_params = array('action'=>'update','type'=>'success','log'=>TRUE);
				$result = TRUE;
			} else {
				$notify_params = array('action'=>'update','type'=>'error','log'=>TRUE);
			}
			$CI->notify->set( $notify_params );	
			
			if( is_object($module_files) ){
				$CI->files->id = $id;
				$CI->files->model_main = $module;
				$CI->files->model_files = $module_files;
				$CI->files->upload();
			}			
		} else {
			$CI->usersrole->has_user_has_capability($module->urc_name,'add');
			
			$id = $CI->records->save( $module, $field_data );
			
			if( $id ){
				if( is_object($module_files) ){
					$CI->files->id = $id;
					$CI->files->model_main = $module;
					$CI->files->model_files = $module_files;
					$CI->files->upload();
				}
			
				$details = $this->Data->get_record($module, array($module->tblid=>$id));
				$name = get_value($details,$module->tblpref.$this->primary_field);
				
				$notify_params = array('action'=>'add','type'=>'success','log'=>TRUE);				
				$result = TRUE;
			} else {
				$notify_params = array('action'=>'add','type'=>'error','log'=>TRUE);
			}
			$CI->notify->set( $notify_params );	
		}
		$notification = $CI->notify->notification;
		
		$data = $this->Data->get_record( $module, array( 'where_params'=>array($module->tblid=>$id) ) );
		
		if( $data ){
			foreach($data as $key=>$val){
				$key = str_replace($module->tblpref,'',$key);
				$field_data[$key] = $val;
			}
		}
		
		$tbody = $this->grid_tbody( $module, $module_parent, $parent_id );
		
		$json = array();
		$json['tbody'] = $tbody;
		$json['page'] = $page;
		$json['field_data'] = $field_data;
		$json['notification'] = $notification;
		$json['result'] = $result;
		$json['redirect_url'] = '';
		
		echo json_encode($json);
		exit();
	}
	
	public function json_display_record( $module, $module_parent ){
		$CI =& get_instance();
		
		$id = $CI->input->post('id');
		$details = $this->record( $module, $id );
		
		$result = FALSE;
		$field = array();
		if( $details ){
			foreach($details as $key=>$val){
				$key = str_replace($module->tblpref,'',$key);
				$field[$key] = $val;
			}
			
			$result = TRUE;
		}
		
		$json = array();
		$json['field'] = $field;
		$json['page'] = $module->page;
		$json['notification'] = '';
		$json['result'] = $result;
		$json['redirect_url'] = '';
		
		echo json_encode($json);
		exit();
	}
	
	public function json_delete( $module, $module_parent, $ModuleFiles='' ){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$module = $module?$module:$this->module;
		$page = $this->page?$this->page:$module->page;
		/*---------------------------------------------*/
		
		$CI->usersrole->has_user_has_capability($module->urc_name,'delete');
		
		$parent_id = $CI->input->post('parent_id');
		$id = $CI->input->post('id');
		
		$details = $this->Data->get_record( $module, array( 'where_params'=>array($module->tblid=>$id) ) ); 
		
		$result = FALSE;
		if( $details ){
			$name = get_value( $details, $module->tblpref.$this->primary_field);
			
			$delete = $this->Data->delete_data( $module, array($module->tblid=>$id) );
			
			if( $delete ){
				$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE);
				$result = TRUE;
			} else {
				$notify_params = array('action'=>'delete','type'=>'error','log'=>TRUE);
			}
		} else {
			$notify_params = array('record'=>'delete','type'=>'not-exist','log'=>TRUE);
		}
		$CI->notify->set( $notify_params );
		
		$tbody = $this->grid_tbody( $module, $module_parent, $parent_id );
		
		$json = array();
		$json['tbody'] = $tbody;
		$json['page'] = $page;
		$json['notification'] = $notification;
		$json['result'] = $result;
		$json['redirect_url'] = '';
		
		echo json_encode($json);
		exit();
	}
	
	public function json_trash( $module, $module_parent, $value=1 ){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$module = $module?$module:$this->module;
		$page = $this->page?$this->page:$module->page;
		/*---------------------------------------------*/
		
		$CI->usersrole->has_user_has_capability($module->urc_name,'delete');
		
		$parent_id = $CI->input->post('parent_id');
		$id = $CI->input->post('id');
		
		$details = $this->Data->get_record( $module, array( 'where_params'=>array($module->tblid=>$id) ) ); 
		
		$result = FALSE;
		if( $details ){
			$name = get_value( $details, $module->tblpref.$this->primary_field);
			
			$data = array($module->tblpref.'trashed'=>$value);
			$trash = $this->Data->update_data( $module, $data, array($module->tblid=>$id) );

			if( $trash ){
				$notify_params = array('action'=>'trash','type'=>'success','log'=>TRUE);
				$result = TRUE;
			} else {
				$notify_params = array('action'=>'trash','type'=>'error','log'=>TRUE);
			}			
		} else {
			$notify_params = array('record'=>'delete','type'=>'not-exist','log'=>TRUE);
		}		
		$CI->notify->set( $notify_params );	
		
		$tbody = $this->grid_tbody( $module, $module_parent, $parent_id );
		
		$json = array();
		$json['tbody'] = $tbody;
		$json['page'] = $page;
		$json['notification'] = $notification;
		$json['result'] = $result;
		$json['redirect_url'] = '';
		
		echo json_encode($json);
		exit();
	}
	
	public function get_data_records( $data, $trashed=0 ){
		$CI =& get_instance();
		
		$where_params = array($this->model->tblpref.'trashed'=>$trashed);
		
		if( $this->logged_user_only ){		
			$logged_user_id = $CI->logged->get_login_session('user_id');	
			$where_params = array_merge($where_params,array($this->model_user->tblname.'.'.$this->model_user->tblid=>$logged_user_id));
		}
		
		$CI->files->model_files = $this->model_files;
		$data = $this->records( $this->model, $data, $where_params );
		
		return $data;
	}
	
	public function get_data_edit( $data ){
		$CI =& get_instance();
			
		$id = $CI->input->get('id');
		$data['id'] = $id;
		
		if( $this->logged_user_only ){		
			$logged_user_id = $CI->logged->get_login_session('user_id');
			
			$where_params = array(
				$this->model->tblid=>$id,
				$this->model_user->tblname.'.'.$this->model_user->tblid=>$logged_user_id
			);			
			$where_params_or_id = $where_params;
		} else {
			$where_params_or_id = $id;
		}		
		
		$data['sub_section_1'] = $this->modal_sub_section( $this->model_sub, $this->model, $id );
		
		$fields = $this->record( $this->model, $where_params_or_id );
		$CI->moduletag->model = $this->model;
		$CI->moduletag->data = $fields;
		$data['fields'] = $fields;
		
		$CI->files->id = $id;
		$CI->files->model_main = $this->model;
		$CI->files->model_files = $this->model_files;
		
		return $data;
	}
	
	public function get_data_records_sub( $data, $trashed=0 ){
		$CI =& get_instance();
		
		$where_params = array($this->model->tblpref.'trashed'=>$trashed);
		
		if( $this->logged_user_only ){		
			$logged_user_id = $CI->logged->get_login_session('user_id');	
			$where_params = array_merge($where_params,
				array($this->model_user->tblname.'.'.$this->model_user->tblid=>$logged_user_id)
			);
		}
		
		$CI->files->model_files = $this->model_files;
		$data = $this->records( $this->model, $data, $where_params );
		
		return $data;
	}
	
	public function get_data_edit_sub( $data ){
		$CI =& get_instance();
			
		$id = $CI->input->get('id');
		$data['id'] = $id;
		
		$where_params_select = array();
		if( $this->logged_user_only ){		
			$logged_user_id = $CI->logged->get_login_session('user_id');
			
			$where_params = array(
				$this->model->tblid=>$id,
				$this->model_user->tblname.'.'.$this->model_user->tblid=>$logged_user_id
			);			
			$where_params_or_id = $where_params;			
			
			$where_params_select = array($this->model_user->tblid=>$logged_user_id);
		} else {
			$where_params_or_id = $id;
		}		
		
		$fields = $this->record( $this->model, $where_params_or_id );
		$CI->moduletag->model = $this->model;
		$CI->moduletag->data = $fields;
		$data['fields'] = $fields;		
		
		$data['select_transactions'] = $this->model_parent->get_select( $this->model, $fields, $where_params_select );
		
		return $data;
	}
}
?>