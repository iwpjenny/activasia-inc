<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notify {
	
	public $next_line;
	public $data;
	public $notification;

    function __construct( $params=array() ){
		
		$params_default = array_merge(array(
			'next_line'=>'<br />'
			), $params
		);
		extract($params_default);
		
		$this->next_line = $next_line;
    }

    function __destruct(){
		
    }
	
	public function set($notification, $alert='success', $action='add', $log=FALSE){
		$CI =& get_instance();
				
		if( is_array($notification) ){
			if( is_numeric(key($notification)) ){
				$action = app_get_val($notification,0);
				$type = app_get_val($notification,1);
				$text = app_get_val($notification,2,'');
				$log = app_get_val($notification,3);
			} else {
				$action = app_get_val($notification,'action');
				$type = app_get_val($notification,'type');
				$text = app_get_val($notification,'text','');
				$log = app_get_val($notification,'log');
			}
			
			$alert = key($this->data['config_notifications'][$action][$type]);
			$notification = $this->data['config_notifications'][$action][$type][$alert];
			$message = $text ? $notification.' '.$text : $notification;	
		} else {
			$message = $notification;
		}
		
		$this->notification = $message;
		
		if( $log  ){
			$user_id = $log;
			$action = $type?$action.'-'.$type:$action;
			if( is_bool($log) && $log === TRUE ){
				$user_id = $CI->logged->get_login_session('user_id');				
			}
			$CI->userslogs->save($message, $action, $user_id);
		}
		
		if( $alert == 'success' ){
			$success = $this->get_by_type($alert);
			if( $success ){
				$set_message = $success.$this->next_line.$message;
			} else {
				$set_message = $message;
			}
			$array['alerts'] = array($alert=>$set_message);
		}
		if( $alert == 'warning' ){
			$warning = $this->get_by_type($alert);
			if( $warning ){
				$set_message = $warning.$this->next_line.$message;
			} else {
				$set_message = $message;
			}
			$array['alerts'] = array($alert=>$set_message);
		}
		if( $alert == 'danger' ){
			$danger = $this->get_by_type($alert);
			if( $danger ){
				$set_message = $danger.$this->next_line.$message;
			} else {
				$set_message = $message;
			}
			$array['alerts'] = array($alert=>$set_message);
		}
		
		if( $alert == 'info' ){
			$danger = $this->get_by_type($alert);
			if( $danger ){
				$set_message = $danger.$this->next_line.$message;
			} else {
				$set_message = $message;
			}
			$array['alerts'] = array($alert=>$set_message);
		}
		
		$CI->session->set_userdata($array);
	}

	public function remove(){
		$CI =& get_instance();
		$CI->load->library('session');
		
		$CI->session->unset_userdata('alerts');
	}
		
	public function show( $params=array() ){
		
		$result = $this->get( $params );
		
		echo $result;
	}
		
	public function get( $params=array() ){
		$CI =& get_instance();
		$CI->load->library('session');
		
		$defaults = array_merge(array(
			'tag'=>'div',
			'start'=>'<div id="notify" class="form-group">',
			'end'=>'</div>'
			), $params
		);
		extract($defaults);
		
		$result = '';
		
		$alerts = $CI->session->userdata('alerts');
		
		if( $alerts ){
			$success = $this->get_by_type('success');
			$warning = $this->get_by_type('warning');
			$danger = $this->get_by_type('danger');
			$info = $this->get_by_type('info');
			 
			if( $success ){
				$result .= '<'.$tag.' class="alert alert-success" role="alert">'.$success.'</'.$tag.'>';
			}
			if( $warning ){
				$result .= '<'.$tag.' class="alert alert-warning" role="alert">'.$warning.'</'.$tag.'>';
			}
			if( $danger ){
				$result .= '<'.$tag.' class="alert alert-danger" role="alert">'.$danger.'</'.$tag.'>';
			}
			if( $info ){
				$result .= '<'.$tag.' class="alert alert-info" role="alert">'.$info.'</'.$tag.'>';
			}
		}
		
		$result = $start.$result.$end;
		
		$this->remove();	
		
		echo $result;
	}
		
	public function get_by_type( $type ){
		$CI =& get_instance();
		$CI->load->library('session');
		
		$result = '';
		
		$alerts = $CI->session->userdata('alerts');
		
		if( $alerts ){
			$result = isset($alerts[$type])?$alerts[$type]:'';
		}
		
		return $result;
	}
		
	public function warning( $type=0 ){
		
		if( $type == 1 ){
			$message = 'Please fill-out the required fields.';
		} else {
			$message = 'No records or data found.';
		}
		
		$result = '<div class="alert alert-warning">'.$message.'</div>';
		
		echo $result;
	}
		
	public function records( $records ){
		
		$alert = '';
		if( is_array($records) || is_object($records) ){
			if( count($records) <= 0 ){
				$message = 'No records or data found. Click the <strong>Add New</strong> button to add new data.';		
				$alert = '<div class="alert alert-warning">'.$message.'</div>';				
			}
		}
		
		echo '<div id="notify-record">'.$alert.'</div>';
	}
}
?>