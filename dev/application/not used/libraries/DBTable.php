<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DBTable {
	
	private $Data;
	private $params;
	private $attributes = array('ENGINE'=>'InnoDB');
	private $if_not_exists = TRUE;
	
	/* return */
	public $error;
	public $result;
	public $notification;
	
	/* parameters */

    function __construct( $params=array() ){
        $CI =& get_instance();
		
		$this->params = $params;
		
		$this->Data = $CI->Data;
    }

    function __destruct(){
        $this->Data = NULL;
		
		$this->params = NULL;
		$this->attributes = NULL;
		$this->if_not_exists = NULL;
    }
	
	public function create( $table_name, $fields, $params ){
		$CI =& get_instance();		
		
		$table_exists = $CI->db->table_exists( $table_name );
		
		$params = $params ? $params: $this->params;
		extract($params);
		
		$result = FALSE;
		if( $table_exists == FALSE ){
			$CI->load->dbforge();
			
			$attributes = isset($attributes) ? $attributes : $this->attributes;
			$if_not_exists = isset($if_not_exists) ? $if_not_exists : $this->if_not_exists;
			
			$CI->dbforge->add_field( $fields );
			if( isset($primary_field) ){
				$CI->dbforge->add_key( $primary_field, TRUE );
			}
			$result = $CI->dbforge->create_table($table_name, $if_not_exists, $this->attributes);
		}
		
		return $result;
	}
}
?>