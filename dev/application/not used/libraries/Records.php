<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Records {
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	
	/* parameters */
	public $id = NULL;
	public $data = array();

    function __construct( $params=array() ){
        // Do something with $params
    }

    function __destruct(){
        $this->id = NULL;
        $this->data = NULL;
    }
	
	public function save( $model, $field_data, $id_or_params='' ){
		$CI =& get_instance();
		$CI->load->model('Data_Model','Data');
		
		$data = array_merge($field_data, $this->data);
				
		if( $id_or_params ){
			if( is_array($id_or_params) ){
				$result = $CI->Data->update_data( $model, $data, $id_or_params );
			} else {
				$result = $CI->Data->update_data( $model, $data, array( $model->tblid=>$id_or_params ) );
			}
		} else {
			$result = $CI->Data->insert_data( $model, $data );
		}
		
		$this->data = array();
		
		return $result;
	}
	/* Do not use this anymore */
	function select_basic( $Model, $ModelUsedBy, $current_id='', $params=array() ){	
		$CI =& get_instance();

		$CI->load->database();
		$CI->load->model('Data_Model','Data');
		
		$params_default = array_merge(array(
			'multiple'=>FALSE,
			'name'=>$ModelUsedBy->tblname.'['.$Model->tblid.']',
			'default_value'=>'',
			'default_text'=>'--Select '.$Model->name.'--',
			'class'=>'',
			'id'=>'',
			'attr'=>'',
			'req'=>TRUE,
			'read'=>FALSE,
			'query_params'=>array('where_params'=>array($Model->tblpref.'id <>'=>0),'orderby'=>$Model->tblpref.'name','ordertype'=>'ASC'),
			'fields'=>'title',
			'seperator'=>', '
			), $params
		);
		extract($params_default);
		
		if( $multiple === TRUE ){
			$name = $name.'[]';
		}
		
		$attr = $attr?$attr:'';
		$multiple = $multiple?'multiple="multiple"':'';
		$required = $req?'required="required"':'';
		$readonly = $read?'readonly="readonly"':'';
		$id = $id?'id="'.$id.'"':'';
		
		$attributes = $attr.' '.$required.' '.$readonly.' '.$id.' '.$multiple;
		
		$selected = empty($current_id)?'selected="selected"':'';
		
		$rows = $CI->Data->get_records( $Model, $query_params );
		
		$result = '<select name="'.$name.'" class="form-control '.$class.'" '.$attributes.'>';
		$result .= '<option value="'.$default_value.'" '.$selected.'>'.$default_text.'</option>';
		if( $rows ){
			foreach($rows as $col){
				$id = get_value($col,$Model->tblid);
				
				if( is_array($fields) ){
					$text = '';
					$c=1;
					foreach($fields as $field){
						$value = get_value($col,$Model->tblpref.$field);
						if( $field == 'id' ){
							$value = 'ID-'.$value;
						}
						if( $c > 1 ){
							$text .= $seperator.$value;
						} else {
							$text .= $value;
						}
						$c++;
					}
				} else {
					$text = get_value($col,$Model->tblpref.$fields);
				}
				
				$selected = ($current_id==$id)?'selected="selected"':'';
				$result .= '<option value="'.$id.'" '.$selected.'>'.$text.'</option>';
			}
		}
		
		$result .= '</select>';
		
		return $result;
    }
	
	function select( $Model, $ModelUsedBy, $current_id='', $params=array(), $hierarchy=FALSE, $level=0, $id=0, $parent_counts=0, $parent_level=1 ){	
		$CI =& get_instance();

		$CI->load->database();
		$CI->load->model('Data_Model','Data');
		
		$params_default = array_merge(array(
			'multiple'=>FALSE,
			'name'=>$ModelUsedBy->tblname.'['.$Model->tblid.']',
			'default_value'=>'',
			'default_text'=>'--Select '.$Model->name.'--',
			'class'=>'',
			'elem_id'=>'',
			'attr'=>'',
			'req'=>TRUE,
			'read'=>FALSE,
			'query_params'=>array('where_params'=>array($Model->tblid.'<>'=>0),'orderby'=>$Model->tblpref.'name','ordertype'=>'ASC'),
			'fields'=>'title',
			'seperator'=>', ',
			'prefix'=>'-',
			'debug'=>FALSE
			), $params
		);		
		extract($params_default);
		
		if( $hierarchy === TRUE ){
			$query_params = array_merge($query_params,array('where_params'=>array($Model->tblpref.'parent_id'=>$id)));
		}
		
		if( $multiple === TRUE ){
			$name = $name.'[]';
			$selected_value = is_serialized($selected_value)?unserialize($selected_value):'';
		}
		
		$attr = $attr?$attr:'';
		$multiple = $multiple?'multiple="multiple"':'';
		$required = $req?'required="required"':'';
		$readonly = $read?'readonly="readonly"':'';
		$elem_id = $elem_id?'id="'.$elem_id.'"':'';
		
		$rows = $CI->Data->get_records( $Model, $query_params );
		//printr($CI->Data->query);
		$result = '';
		if( $hierarchy === FALSE || ($hierarchy === TRUE && $level === 0) ){
			$parent_counts = count($rows);
			
			$attributes = $attr.' '.$required.' '.$readonly.' '.$elem_id.' '.$multiple;
			$result .= "\n".'<select name="'.$name.'" class="form-control '.$class.'" '.$attributes.'>'."\n";
			$selected = empty($current_id)?'selected="selected"':'';
			$result .= '<option value="'.$default_value.'" '.$selected.'>'.$default_text.'</option>'."\n";
		}
		
		/* if( $hierarchy === TRUE && $parent_level == $parent_counts && count($rows) == 0 ){
			$result .= '</select>'."\n";
		} */
		
		$prfx = '';
		if( $level ){
			$cp = 1;
			while($cp <= $level){
				$prfx .= $prefix;
				$cp++;
			}
		}
		   
		$level++;	
		
		if( $rows ){
			foreach($rows as $col){
				$id = get_value($col,$Model->tblid);
				
				if( is_array($fields) ){
					$text = '';
					$c=1;
					foreach($fields as $field){
						$value = get_value($col,$Model->tblpref.$field);
						if( $field == 'id' ){
							$value = 'ID-'.$value;
						}
						if( $c > 1 ){
							$text .= $seperator.$value;
						} else {
							$text .= $value;
						}
						$c++;
					}
				} else {
					$text = get_value($col,$Model->tblpref.$fields);
				}
				$text = $prfx.$text;
								
				$selected = ($current_id==$id)?'selected="selected"':'';
				if( $debug === TRUE ){
					$result .= '<option value="'.$id.'" '.$selected.'>'.$text.' '.$level.'-'.$parent_counts.'-'.$parent_level.'</option>'."\n";
				} else {
					$result .= '<option value="'.$id.'" '.$selected.'>'.$text.'</option>'."\n";
				}
				
				if( $hierarchy === TRUE ){
					
					$result2 = $this->select( $Model, $ModelUsedBy, $current_id, $params, $hierarchy, $level, $id, $parent_counts, $parent_level );
					
					if( $result2 ){
						$result .= $result2;
					}
		
					//if( $level == 1 && ($parent_level <= $parent_counts) ){
					if( $level == 1 && ($parent_level < $parent_counts) ){
						$parent_level++;
					}
				}
			}
		} else {
			if( $hierarchy === TRUE && $parent_level == $parent_counts ){
				$result .= '</select>'."\n";
			}
		}

		/* if( $hierarchy === TRUE && $parent_level == $parent_counts && count($rows) == 0 ){
			$result .= '</select>'."\n";
		} else */
		if( $hierarchy === FALSE || (count($rows) <= 0 && $level == 1) ){
			$result .= '</select>'."\n";
		}
		/* if( $hierarchy === FALSE ){
			$result .= '</select>'."\n";
		} */
		
		/* if( $hierarchy === TRUE ){
			if($parent_level == $parent_counts && count($rows) == 0 ){
				$result .= '</select>'."\n";
			}
		} 
		if( $hierarchy === FALSE ){
			$result .= '</select>'."\n";
		} */
		
		return $result;
    }
	
	/* function recursive( $Model, $ModelUsedBy, $id=0, $level=0 ){
		$CI =& get_instance();

		$CI->load->database();
		$CI->load->model('Data_Model','Data');

		$query_params = array('where_params'=>array($Model->tblpref.'parent_id'=>$id),'orderby'=>$Model->tblpref.'name','ordertype'=>'ASC');		
		$rows = $CI->Data->get_records( $Model, $query_params );
		//echo "\n\r".'<br />'.$CI->Data->query;
		
		$level++;
		
		$result = '';
		if( $rows ){
			foreach($rows as $col){
				$id = get_value($col,$Model->tblid);
				$text = get_value($col,$Model->tblpref.'title');
				
				$prefix = '';
				$cp = 1;
				while($cp <= $level){
					$prefix .= '-';
					$cp++;
				}
				
				$result .= '<br />'.$prefix.' '.$id.' '.$text."\n";
				
				$result2 = $this->recursive( $Model, $ModelUsedBy, $id, $level );
				if( $result2 ){
					$result .= $result2;
				}
			}
		}
		
		return $result;
    } */	
}
?>