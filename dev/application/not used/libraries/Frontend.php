<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frontend {
	
	private $model_name = 'Member';

    public function __construct( $params=array() ){
        
    }

	public function set(){
        $CI =& get_instance();
		
		$CI->load->model('Frontend_Log_Model', 'FLog');
		
		$CI->load->model($this->model_name.'_Model', 'Mem');
		$CI->load->model($this->model_name.'_Logs_Model', 'ML');
		$CI->load->model($this->model_name.'_Role_Model', 'MR');
		$CI->load->model($this->model_name.'_Profile_Model', 'MP');
	}

	public function set_libaries( $model=NULL ){
        $CI =& get_instance();
		
		$this->location = $CI->FLog->location;
		$this->login_user_type = $CI->FLog->login_user_type;
		$this->dashboard_page = $CI->FLog->dashboard_page;
		
		$CI->load->library('logged');
		$CI->logged->dashboard_page = $this->dashboard_page;
		$CI->logged->login_user_type = $this->login_user_type;
		$CI->logged->model_user = $CI->Mem;
		$CI->logged->model_user_role = $CI->MR;
		$CI->logged->model_user_logs = $CI->ML;
		$CI->logged->model_user_profile = $CI->MP;
		$CI->logged->model_log = $CI->FLog;
		
		$userlogs_params = array(
			'login_user_type'=>$this->login_user_type,
			'model_user'=>$CI->Mem,
			'model_user_logs'=>$CI->ML
		);
		$CI->load->library('userslogs',$userlogs_params);
		
		$CI->load->library('method');
		$CI->method->model = $model;
		$CI->method->login_user_type = $this->login_user_type;
		$CI->method->location = $CI->FLog->location;
		
		$CI->method->model_user = $CI->Mem;
		$CI->method->model_user_role = $CI->MR;
		$CI->method->model_user_logs = $CI->ML;
		$CI->method->model_user_profile = $CI->MP;
		$CI->method->model_log = $CI->FLog;
		
		$CI->load->library('usersrole',array('model_log'=>$CI->FLog));
	}
}
?>