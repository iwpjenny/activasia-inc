<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'third_party/dompdf/dompdf_config.inc.php';

class PDF {

	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();

	/* return */
	public $error;
	public $result;

	/* parameters */
	public $id;

	public function __construct( $params=array() ){
	require_once APPPATH.'third_party/dompdf/dompdf_config.inc.php';
	// require_once APPPATH.'third_party/fpdf/fpdf.php';
		$params_default = array_merge(array(
			'id'=>NULL,
			'folder'=>'modules',
			'base_path'=>str_replace('\\','/',FCPATH),
			'dir_root'=>'uploads',
			'dir_perms'=>DIR_WRITE_MODE,
			'model_main'=>NULL,
			), $params
		);
		extract($params_default);

		if( $params ){
			$this->id = $id;
			$this->model_main = $model_main;
		}

		$this->folder = $folder;
		$this->base_path = $base_path;
		$this->dir_root = $dir_root;
		$this->dir_perms = $dir_perms;

	}

    // function __construct( $params=array() ){
		// $this->default_image_url_fullpath = base_url($this->dir_root.'/'.$this->default_image);
    // }

    function __destruct(){
		$this->trigger_before = NULL;
		$this->trigger_after = NULL;
		$this->trigger_on_success = NULL;
		$this->trigger_on_error = NULL;
		$this->error = NULL;

        $this->id = NULL;
        $this->folder = NULL;
        $this->model_main = NULL;

        $this->dir_root = NULL;
        $this->dir_perms = NULL;
    }

	private function trigger( $triggers=array() ){

		$result = FALSE;

		if( array_key_exists(0,$triggers) ){
			foreach($triggers as $trigger){
				$result[] = $this->get_trigger( $trigger );
			}
		} else {
			$result = $this->get_trigger( $triggers );
		}

		return $result;
	}

	private function get_trigger( $trigger ){

		$result = FALSE;

		if( is_array($trigger) && $trigger ){
			extract($trigger);

			if( isset($params) ){
				if( is_object($obj) ){
					$result = $obj->$func( $params );
				} else {
					$result = $func( $params );
				}
			} else {
				if( is_object($obj) ){
					$result = $obj->$func();
				} else {
					$result = $func();
				}
			}
		}

		return $result;
	}

	public function upload(){
		$CI =& get_instance();
		$CI->load->model('Data_Model','Data');
	}

	/* --------------------CUSTOM---------------------- */
	/* ------------------------------------------------ */
	protected function ci(){
		return get_instance();
	}

	public function generate_pdf($html, $params = array()){

		$CI =& get_instance();
		$params_default = array_merge(array(
			'paper_size'=>'legal',
			'paper_orientation'=>'landscape',
			'file_name'=>'form.pdf',
			'add_footer'=>FALSE,
			), $params
		);
		extract($params_default);
		// printx($file); //SRPsAO Form 1 2017-01-06 HH-MM-SS
		// $dompdf = new DOMPDF();
		// $dompdf->set_paper( $paper_size,$paper_orientation );
		// $dompdf->load_html( $file );
		// $dompdf->render();
		// $dompdf->stream( $file_name,array('Attachment'=>0));

		$dompdf = new DOMPDF();
		$dompdf->set_paper(  $paper_size,$paper_orientation  );
		$dompdf->load_html($html);
		$dompdf->render();

		$canvas = $dompdf->get_canvas();
		$font = Font_Metrics::get_font("helvetica", "bold");
		if( $add_footer ){
			$canvas->page_text(16, 800, "Page: {PAGE_NUM} of {PAGE_COUNT}", $font, 8, array(0,0,0));
		}

		// $date_added = date('Y-m-d-H-i');
		// $file_name = $lastname.'-Loan-Record-'.$date_added.'.pdf';
		$dompdf->stream( $file_name, array("Attachment" =>0));

	}

}
?>
