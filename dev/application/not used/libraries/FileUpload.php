<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FileUpload {
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	public $full_filepath;
	public $upload_filepath;
	public $filename_new;
	public $filename_old;
	public $file_title;
	public $file_type;
	
	/* parameters */
	public $fieldname;
	public $filename;
	public $dist_dir;
	public $base_path;
	public $dir_perms;
	public $name_case;
	public $allowed;
	public $max_size;
	public $max_width;
	public $max_height;

    function __construct( $params=array() ){
		
		$params_default = array_merge(array(
			'fieldname'=>'file',
			'filename'=>'',
			'dist_dir'=>str_replace('\\','/',FCPATH).'uploads/',
			'base_path'=>str_replace('\\','/',FCPATH),
			'dir_perms'=>DIR_WRITE_MODE,
			'name_case'=>'strtolower',
			'allowed'=>'',
			'max_size'=>16,
			'max_width'=>1024,
			'max_height'=>1024
			), $params
		);
		extract($params_default);
		
		$this->fieldname = $fieldname;
		$this->filename = $filename;
		$this->dist_dir = $dist_dir;
		$this->base_path = $base_path;
		$this->dir_perms = $dir_perms;
		$this->name_case = $name_case;
		$this->allowed = $allowed;
		$this->max_size = $max_size;
		$this->max_width = $max_width;
		$this->max_height = $max_height;
    }

    function __destruct(){
		$this->trigger_before = NULL;
		$this->trigger_after = NULL;
		$this->trigger_on_success = NULL;
		$this->trigger_on_error = NULL;
		$this->error = NULL;
		
		$this->full_filepath = NULL;
		$this->upload_filepath = NULL;
		$this->filename_new = NULL;
		$this->filename_old = NULL;
		$this->file_title = NULL;
		$this->file_type = NULL;
		
		$this->fieldname = NULL;
		$this->filename = NULL;
		$this->dist_dir = NULL;
		$this->base_path = NULL;
		$this->dir_perms = NULL;
		$this->allowed = NULL;
		$this->max_size = NULL;
		$this->max_width = NULL;
		$this->max_height = NULL;
    }
	
	private function trigger( $triggers=array() ){
		
		$result = FALSE;
		
		if( array_key_exists(0,$triggers) ){
			foreach($triggers as $trigger){
				$result[] = $this->get_trigger( $trigger );
			}
		} else {
			$result = $this->get_trigger( $triggers );
		}
		
		return $result;
	}
	
	private function get_trigger( $trigger ){
		
		$result = FALSE;
		
		if( is_array($trigger) && $trigger ){
			extract($trigger);
			
			if( isset($params) ){
				if( is_object($obj) ){
					$result = $obj->$func( $params );
				} else {
					$result = $func( $params );
				}
			} else {
				if( is_object($obj) ){
					$result = $obj->$func();
				} else {
					$result = $func();
				}
			}
		}
		
		return $result;
	}
	
	public function upload(){
		$CI =& get_instance();
		
		$result = FALSE;
		
		if( isset($_FILES[$this->fieldname]['name']) ){
			$_file = $_FILES[$this->fieldname];
			
			$files_name = $_file['name'];
			$files_type = $_file['type'];
			$files_size = $_file['size'];
			$files_error = $_file['error'];
			$files_temp = $_file['tmp_name'];
				
			$this->trigger( $this->trigger_before );
			
			if( is_array($files_name) ){
				if( $files_name[0] ){
					
					$uploaded = array();
					$filename_new_array = array();
					$file_title_array = array();
					$file_type_array = array();
					$filepath_new_array = array();
					$upload_filepath_array = array();
					$count = 1;
					
					foreach($files_temp as $key=>$file_temp){
						$file = $files_name[$key];
						$type = $files_type[$key];
						
						$fileinfo = pathinfo($file);
						
						$dirname = $fileinfo['dirname'];
						$basename = $fileinfo['basename'];
						$extension = strtolower($fileinfo['extension']);
						$filename = $fileinfo['filename'];
						
						$name_case = $this->name_case;
						
						if( $this->filename ){
							$filename_temp = $name_case($this->filename);
						} else {
							$filename_temp = $name_case($filename);
						}
						
						$title = str_replace('_',' ',$filename);
						$title = str_replace('-',' ',$title);
						$title = ucwords($title);
						
						$postfix = date('Ymd-His');
						
						$filename_new = $filename_temp.'-'.$count.'-'.$postfix.'.'.$extension;
						$filename_new_array[] = $filename_new;
						$filename_old_array[] = $basename;
						$file_title_array[] = $title;
						$file_type_array[] = $type;
						$full_filepath_new = $this->dist_dir.$filename_new;
						$filepath_new_array[] = $full_filepath_new;
						
						$upload = move_uploaded_file($file_temp, $full_filepath_new);	

						if( $upload ){
							$upload_filepath = str_replace($this->base_path,'',$full_filepath_new);
							$upload_filepath_array[] = $upload_filepath;
							$uploaded[] = $upload?$upload_filepath:'';	
							
							$this->filename_new = $filename_new_array;
							$this->filename_old = $filename_old_array;
							$this->file_title = $file_title_array;
							$this->file_type = $file_type_array;
							$this->full_filepath = $filepath_new_array;
							$this->upload_filepath = $upload_filepath_array;
							
							$notify_params = array('action'=>'upload','type'=>'success','log'=>TRUE);
							$CI->notify->set( $notify_params );
							$this->trigger( $this->trigger_on_success );
						} else {
							$error[] = error_get_last();
							$this->error = $error;
							
							$notify_params = array('action'=>'upload','type'=>'error','log'=>TRUE);
							$CI->notify->set( $notify_params );
							$this->trigger( $this->trigger_on_error );
						}
						$count++;
					}
					
					$result = $uploaded;
				}
			} else {
				$fileinfo = pathinfo($files_name);
				$dirname = $fileinfo['dirname'];
				$basename = $fileinfo['basename'];
				$extension = strtolower($fileinfo['extension']);
				$filename = $fileinfo['filename'];
				
				$name_case = $this->name_case;
				
				if( $this->filename ){
					$basename = $name_case($this->filename);
				} else {
					$basename = $name_case($filename);
				}
				
				$filename_new = $basename.'.'.$extension;
				$full_filepath_new = $this->dist_dir.$filename_new;
				
				$upload = move_uploaded_file($files_temp, $full_filepath_new);	

				if( $upload ){
					$upload_filepath = str_replace($this->base_path,'',$full_filepath_new);
					
					$this->filename_new = $filename_new;
					$this->full_filepath = $full_filepath_new;
					$this->upload_filepath = $upload_filepath;
					$result = $upload;
					
					$notify_params = array('action'=>'upload','type'=>'success','log'=>TRUE);
					$CI->notify->set( $notify_params );
					$this->trigger( $this->trigger_on_success );
				} else {
					$this->error = error_get_last();
					$notify_params = array('action'=>'upload','type'=>'error','log'=>TRUE,'text'=>$this->error);
					$CI->notify->set( $notify_params );
					$this->trigger( $this->trigger_on_error );
				}
			}
			
			$this->trigger( $this->trigger_after );
		}
		
		return $result;
	}
}
?>