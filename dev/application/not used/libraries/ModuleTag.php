<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModuleTag {
	
	public $model;
	public $data;

    public function __construct( $params=array() ){
        // Do something with $params
    }

    public function __destruct(){
        $this->model = NULL;
        $this->data = NULL;
    }

	public function s( $params ){
		
		$result = $this->g( $params );
		
		echo $result;
	}

	public function g( $params ){
		$CI =& get_instance();
		
		$data = $this->data; 
		$field_data = $CI->fields->get_session();
		
		$params_default = array_merge(array(
			'name'=>'title',
			'type'=>'text',
			'group'=>$this->model->tblname,
			'prefix'=>$this->model->tblpref,
			'field'=>'',
			'class'=>'',
			'id'=>'',
			'holder'=>'',
			'attr'=>'',
			'max'=>'',
			'min'=>'',
			'req'=>FALSE,
			'read'=>FALSE,
			'has_value'=>TRUE,
			'compare_values'=>1,
			'value'=>'',
			'default'=>''
			), $params
		);
		extract($params_default);
		
		$required = $req?'required="required"':'';
		$readonly = $read?'readonly="readonly"':'';
		$id = $id?'id="'.$id.'"':'';
		$maxlength = $max?'maxlength="'.$max.'"':'';
		$minlength = $min?'maxlength="'.$min.'"':'';
		$placeholder = $holder?'placeholder="'.$holder.'"':'';
		
		$attributes = $attr.' '.$required.' '.$readonly.' '.$id.' '.$maxlength.' '.$minlength.' '.$placeholder;
			
		$field_name = $prefix.$name;
		$tag_name = $group.'['.$field_name.']';
		
		$current_value = '';
		$default_value = FALSE;
		if( $has_value ){
			$current_value = app_get_val( $data, $field_name );			
			
			if( empty($current_value) && $default ){
				if( is_bool($default) && $default === TRUE ){
					$current_value = $value;
				} else {
					$current_value = $default;
				}
				$default_value = TRUE;
			} elseif( empty($current_value) && isset($field_data[$field_name]) ) {
				$current_value = $field_data[$field_name];
			}
		}
		
		if( in_array($type,array('checkbox','radio')) ){
			if( $type == 'checkbox' ){
				if( $default_value == FALSE ){
					$current_value = $current_value?@unserialize($current_value):$current_value;
				}
				$tag_name = $tag_name.'[]';		
			}
			
			$checked = app_get_checked($value, $current_value);
			
			$result = '<input type="'.$type.'" class="'.$class.'" name="'.$tag_name.'" value="'.$value.'" '.$attributes.' '.$checked.' />';
		} elseif( $type == 'textarea' ){
			$value = $value?$value:$current_value;
			$result = '<textarea class="form-control '.$class.'" name="'.$tag_name.'" '.$attributes.'>'.$value.'</textarea>';
		} else {
			$value = $value?$value:$current_value;
			
			$result = '<input type="'.$type.'" class="form-control '.$class.'" name="'.$tag_name.'" value="'.$value.'" '.$attributes.' />';
		}
		
		return $result;
	}
	
	public function select( $Model, $ModelUsedBy, $current_id='', $params=array(), $hierarchy=FALSE ){	
		$CI =& get_instance();
		
		$field_data = $CI->fields->get_session();
		
		$current_id = app_get_val($field_data, $Model->tblid, $current_id);
		
		$result = $this->dropdown( $Model, $ModelUsedBy, $current_id, $params, $hierarchy );
		
		return $result;
	}
	
	public function dropdown( $Model, $ModelUsedBy, $current_id='', $params=array(), $hierarchy=FALSE, $level=0, $id=0, $parent_counts=0, $parent_level=1 ){	
		$CI =& get_instance();
		
		$params_default = array_merge(array(
			'multiple'=>FALSE,
			'name'=>$ModelUsedBy->tblname.'['.$Model->tblid.']',
			'default_value'=>'',
			'default_text'=>'--Select '.$Model->name.'--',
			'class'=>'',
			'elem_id'=>'',
			'attr'=>'',
			'req'=>TRUE,
			'read'=>FALSE,
			'query_params'=>array('where_params'=>array($Model->tblid.'<>'=>0),'orderby'=>$Model->tblpref.'name','ordertype'=>'ASC'),
			'fields'=>'title',
			'seperator'=>', ',
			'prefix'=>'-',
			'debug'=>FALSE
			), $params
		);		
		extract($params_default);
		
		if( $hierarchy === TRUE ){
			$query_params = array_merge($query_params,array('where_params'=>array($Model->tblpref.'parent_id'=>$id)));
		}
		
		if( $multiple === TRUE ){
			$name = $name.'[]';
			$selected_value = is_serialized($selected_value)?unserialize($selected_value):'';
		}
		
		$attr = $attr?$attr:'';
		$multiple = $multiple?'multiple="multiple"':'';
		$required = $req?'required="required"':'';
		$readonly = $read?'readonly="readonly"':'';
		$elem_id = $elem_id?'id="'.$elem_id.'"':'';
		
		$rows = $CI->Data->get_records( $Model, $query_params );
		
		$result = '';
		if( $hierarchy === FALSE || ($hierarchy === TRUE && $level === 0) ){
			$parent_counts = count($rows);
			
			$attributes = $attr.' '.$required.' '.$readonly.' '.$elem_id.' '.$multiple;
			$result .= "\n".'<select name="'.$name.'" class="form-control '.$class.'" '.$attributes.'>'."\n";
			$selected = empty($current_id)?'selected="selected"':'';
			$result .= '<option value="'.$default_value.'" '.$selected.'>'.$default_text.'</option>'."\n";
		}
		
		$prfx = '';
		if( $level ){
			$cp = 1;
			while($cp <= $level){
				$prfx .= $prefix;
				$cp++;
			}
		}
		   
		$level++;	
		
		if( $rows ){
			foreach($rows as $col){
				$id = get_value($col,$Model->tblid);
				
				if( is_array($fields) ){
					$text = '';
					$c=1;
					foreach($fields as $field){
						$value = get_value($col,$Model->tblpref.$field);
						if( $field == 'id' ){
							$value = 'ID-'.$value;
						}
						if( $c > 1 ){
							$text .= $seperator.$value;
						} else {
							$text .= $value;
						}
						$c++;
					}
				} else {
					$text = get_value($col,$Model->tblpref.$fields);
				}
				$text = $prfx.$text;
								
				$selected = ($current_id==$id)?'selected="selected"':'';
				if( $debug === TRUE ){
					$result .= '<option value="'.$id.'" '.$selected.'>'.$text.' '.$level.'-'.$parent_counts.'-'.$parent_level.'</option>'."\n";
				} else {
					$result .= '<option value="'.$id.'" '.$selected.'>'.$text.'</option>'."\n";
				}
				
				if( $hierarchy === TRUE ){
					
					$result2 = $this->dropdown( $Model, $ModelUsedBy, $current_id, $params, $hierarchy, $level, $id, $parent_counts, $parent_level );
					
					if( $result2 ){
						$result .= $result2;
					}
		
					if( $level == 1 && ($parent_level < $parent_counts) ){
						$parent_level++;
					}
				}
			}
		} else {
			if( $hierarchy === TRUE && $parent_level == $parent_counts ){
				$result .= '</select>'."\n";
			}
		}
		
		if( $hierarchy === FALSE || (count($rows) <= 0 && $level == 1) ){
			$result .= '</select>'."\n";
		}
		
		return $result;
    }
}
?>