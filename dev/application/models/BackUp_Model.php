<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BackUp_Model extends CI_Model {
	
	public $page = 'BackUp';
	public $view = 'backup';
	public $tblname = 'backup';
	public $tblpref = 'backup_';
	public $tblid = 'backup_id';
	public $per_page = 15;
	
	public $name = 'BackUp';
	public $urc_name = 'backup';
	public $description = 'Manager BackUp';
	public $display_section = array('menu-sidebar-left'=>array('order'=>4),'dashboard'=>array('order'=>6));
	
	public $user_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes'),
			array('field'=>'download','name'=>'Download','value'=>'yes'),
			array('field'=>'delete_session','name'=>'Delete Other Session','value'=>'yes'),
		),
		array(
			array('field'=>'backup_database','name'=>'BackUp Database','value'=>'yes'),
			array('field'=>'backup_application','name'=>'BackUp Application Files Only','value'=>'yes'),
			array('field'=>'backup_all','name'=>'BackUp All','value'=>'yes'),
			array('field'=>'trancate_module_records','name'=>'Trancate Module Records','value'=>'yes'),
			array('field'=>'trancate_logs','name'=>'Trancate Logs','value'=>'yes')
		),
		array(
			array('field'=>'send_email','name'=>'Send Email','value'=>'yes') 
		)
	);
	
	public $format = 'zip';
	public $backup_path;
	public $dir_path;
	private $base_path;

    function __construct(){
		
		$this->base_path = str_replace('\\','/',FCPATH);
		$this->backup_path = 'backup/';
		$this->dir_path = $this->base_path.$this->backup_path;
		
        parent::__construct();
    }

	public function ini_custom_models(){
		
		/* Initialize Custom Model here */
		
		/* End of initialize custom model here */	
	}

    function records( $dir_path='' ){
		
		$records = array();
		$dir_path = $dir_path?$dir_path:$this->dir_path;
		$backup_files = app_get_file_open_dir( $dir_path );
		
		if( $backup_files ){
			$id=1;
			foreach($backup_files as $name){
				if( !in_array($name, array('index.html')) ){
					$pathinfo = pathinfo($dir_path.$name);
					$size = filesize($dir_path.$name);
					$datetime = date('Y-m-d H:i:s',filemtime($dir_path.$name));
					$datetime_changed = date('Y-m-d H:i:s',filectime($dir_path.$name));
					$basename = get_value( $pathinfo, 'basename' );
					$filename = get_value( $pathinfo, 'filename' );
					$extension = get_value( $pathinfo, 'extension' );
					
					$records[] = (object) array(
						'id'=>$id,
						'basename'=>$basename,
						'extension'=>$extension,
						'dir_path'=>$dir_path,
						'filename'=>$filename,
						'size'=>$size,
						'datetime'=>$datetime,
						'datetime_changed'=>$datetime_changed
					);
					$id++;
				}
			}
		}
		
		return $records;
    }

    function backup_database( $dir='./backup/', $name='' ){
		$this->load->dbutil();
		$this->load->helper('file');
		
		
		$datetime = date('Ymd-His');
		$filename = $name?$name:'db-'.$datetime.'.'.$this->format;
		$filepath = $dir.$filename;
		$sql_name = $this->db->database.'-'.$datetime.'.sql';
		
		$params = array(
			'format'      => $this->format,		/* gzip, zip, txt */
			'filename'    => $sql_name,		/* File name - NEEDED ONLY WITH ZIP FILES */
			'add_drop'    => TRUE,			/* Whether to add DROP TABLE statements to backup file */
			'add_insert'  => TRUE,			/* Whether to add INSERT data to backup file */
			'newline'     => "\n"			/* Newline character used in backup file */
		);
		$backup = $this->dbutil->backup($params);
		$result = @write_file($filepath, $backup);
		
		return $result;
    }

    function backup_database_and_application_folder(){
		$this->load->library('zip');
		
		$base_path = str_replace('\\','/',FCPATH);
		
		$datetime = date('Ymd-His');
		$app_path = $this->base_path.'application/';
		$filename = 'application-'.$datetime.'.'.$this->format;
		
		$this->backup_database( $app_path );
		$this->zip->read_dir( $app_path, FALSE ); 
		$result = $this->zip->archive($this->backup_path.$filename);
		
		return $result;
    }

    function backup_all(){
		$this->load->library('zip');
		$this->load->helper('directory');
		
		$environment = $this->Set->get_settings('general','environment','local'); 
			
		$datetime = date('Ymd-His');
		$dirpath_backup_temp = $this->backup_path.$environment.'-backup-'.$datetime.'/';
		
		$dirpath_app = $this->base_path.'application';
		$dirpath_system = $this->base_path.'system';
		$dirpath_includes = $this->base_path.'includes';
		$dirpath_uploads = $this->base_path.'uploads';

		$result = FALSE;		
		
		if( file_exists( $this->backup_path ) ){
			if( mkdir( $dirpath_backup_temp ) ){
				copy( $this->base_path.'index.php', $dirpath_backup_temp.'index.php' );
				copy( $this->base_path.'license.txt', $dirpath_backup_temp.'license.txt' );
				$this->copy_dir( $dirpath_app, $dirpath_backup_temp.'application' );
				$this->copy_dir( $dirpath_system, $dirpath_backup_temp.'system' );
				$this->copy_dir( $dirpath_includes, $dirpath_backup_temp.'includes' );
				if( file_exists( $dirpath_backup_temp.'uploads' ) ){
					$this->copy_dir( $dirpath_uploads, $dirpath_backup_temp.'uploads' );
				}
				$this->backup_database( $dirpath_backup_temp );
			
				$filename = $environment.'-backup-'.$datetime.'.'.$this->format;
				
				$this->zip->read_dir( $dirpath_backup_temp, FALSE ); 
				$result = $this->zip->archive($this->backup_path.$filename);
				
				$this->remove_dir( $dirpath_backup_temp );
			}
		} else {
			mkdir( $this->backup_path );
		}
		
		return $result;
    }
	
	function copy_dir($src, $dst) {
		$dir = opendir($src);
		@mkdir($dst);
		while(FALSE !== ( $file = readdir($dir)) ){
			if (( $file != '.' ) && ( $file != '..' )){
				if ( is_dir($src . '/' . $file) ) {
					$this->copy_dir($src . '/' . $file,$dst . '/' . $file);
				} else {
					copy($src . '/' . $file,$dst . '/' . $file);
				}
			}
		}
		closedir($dir);
	}
	
	function remove_dir( $dir ){
		
		$dir = rtrim($dir,'/');
		
		$result = FALSE;
	
		if( is_dir($dir) ){
			$files = scandir($dir);
			foreach($files as $file){
				if($file != '.' && $file != '..'){
					//if (!is_dir($dir.'/'.$file)){
						//unlink($dir.'/'.$file);
					//} else {
						$this->remove_dir($dir.'/'.$file);
					//}
				}
			}
			$result = rmdir($dir);
		} elseif( file_exists($dir) ){
			$result = unlink($dir);
		}

		return $result;
	}
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/	
	function get_dashboard_section_view( $data ){
			
		$location = app_get_val($data,'location');
		
		$data['display_latest_backups'] = app_get_val($data['settings'],'display_latest_backups');		
		$data['grid_date_time_format'] = app_get_val($data['settings'],'grid_date_time_format'); 
		$data['view_date_time_format'] = app_get_val($data['settings'],'view_date_time_format'); 
		$data['character_limit'] = app_get_val($data['settings'],'grid_text_limit');
		
		$data['records'] = $this->records();
		
		$result = $this->load->view($location.'/'.$this->view.'/section-dashboard', $data, TRUE);
		
		return $result;
	}
	
	function truncate_logs(){ 
		$this->db->truncate($this->db->dbprefix.$this->UL->tblname); 
		$this->db->truncate($this->db->dbprefix.$this->ML->tblname);
    } 
	
	function truncate_transactions(){ 
		$this->load->model('modules/Transactions_Model', 'Trans');
		$this->load->model('modules/Transactions_Sub_Model', 'TransSub');
		$this->load->model('modules/Transactions_Files_Model', 'TransF');
		
		$this->db->truncate($this->db->dbprefix.$this->Trans->tblname); 
		$this->db->truncate($this->db->dbprefix.$this->TransSub->tblname); 
		$this->db->truncate($this->db->dbprefix.$this->TransF->tblname);
    }
	
	function archive_database_user_logs( $dir='./backup/', $name='' ){
		$result = FALSE; 
		$datetime = date('Ymd');
		$dbprefix = $this->db->dbprefix;
		$new_table_name = $this->db->dbprefix.'std_user_logs_'.$datetime;
		$table_name = $this->db->dbprefix.'std_user_logs';
		 
		$create_query = $this->db->query("CREATE TABLE IF NOT EXISTS `$new_table_name` LIKE `$table_name`;");
		$transfer_query = $this->db->query("INSERT `$new_table_name` SELECT * FROM `$table_name`;"); 
		if( $transfer_query ){
			$this->db->empty_table($table_name);
			$result = TRUE;
		}  
		return $result;
    }
	
	function backup_database_log_archive( $dir='./backup/', $name='' ){
		$this->load->dbforge();
		$this->load->dbutil();
		$this->load->helper('file');
		$table_log_array = FALSE;
		$delete_log_array = FALSE;
		$tables = $this->db->list_tables();
		$main_user_logs = $this->db->dbprefix.'std_user_logs';  

		foreach ($tables as $table){ 
			if(strpos($table, 'std_user_logs') !== false && $table != $main_user_logs ){ 
				$table_log_array[] = $table;
				$delete_log_array[] = str_replace( $this->db->dbprefix, '', $table) ;;
			} 
		} 
		
		if( $table_log_array ){
			$datetime = date('Ymd-His');
			$filename = $name?$name:'db-logs-archive-'.$datetime.'.'.$this->format;
			$filepath = $dir.$filename;
			$sql_name = $this->db->database.'-logs-archive-'.$datetime.'.sql';
			
			$params = array(
				'tables' 	  => $table_log_array,   // Array of tables to backup.
				'format'      => $this->format,		/* gzip, zip, txt */
				'filename'    => $sql_name,		/* File name - NEEDED ONLY WITH ZIP FILES */
				'add_drop'    => TRUE,			/* Whether to add DROP TABLE statements to backup file */
				'add_insert'  => TRUE,			/* Whether to add INSERT data to backup file */
				'newline'     => "\n"			/* Newline character used in backup file */
			);
			$backup = $this->dbutil->backup($params);
			$result = @write_file($filepath, $backup);
			if( $result ){
				foreach ($delete_log_array as $delete_table){ 
					$this->dbforge->drop_table($delete_table,TRUE); 
				}
			}
			return $result;
		} else {
			return FALSE;
		} 
    }
	 
	function auto_send_backup_database( $data ){
		$base_path = str_replace('\\','/',FCPATH);
		$path = $base_path.'backup';
		
		$latest_ctime = 0;
		$latest_filename = '';    

		$d = dir($path);
		while (false !== ($entry = $d->read())) {
		  $filepath = "{$path}/{$entry}"; 
		  if (is_file($filepath) && filectime($filepath) > $latest_ctime &&  strpos($entry, 'db') !== false &&  strpos($entry, 'archive') === false) {
			$latest_ctime = filectime($filepath);
			$latest_filename = $entry;
		  }
		}  
		$backup_file = $path.'/'.$latest_filename; 
		if( file_exists( $backup_file )){
			
			$general_email = app_get_val( $data['settings'] ,'email');
			$system_name = app_get_val( $data['settings'] ,'system_name');
			$website = app_get_val( $data['settings'] ,'website'); 
			
			$file_name_date = date('F j, Y',filemtime($backup_file));
			$file_name_time = date('g:i a',filemtime($backup_file)); 
			$file_name_size =  (filesize($backup_file) * .0009765625) * .0009765625; // bytes to MB 
			
			$message = '<table border="0" cellpadding="0" cellspacing="0">';
			$message .= '<tr><td><p><b>Dear Admin, </b></p></td></tr></table>';	
			$message .= '<p>The system database has been successfully backup.'; 
			  
			$message .= '<br /><br /><p>System: <b>'.$system_name.'</b></p>';
			$message .= '<p>URL: '.$website.'</p>';
			$message .= '<p>Date: <b>'.$file_name_date.' at '.$file_name_time.'</b></p>';
			$message .= '<p>Filename: <b>'.$latest_filename.'</b></p>';
			$message .= '<p>File size: <b>'.number_format($file_name_size,2,'.',',').'mb</b></p>'; 
			
			$message .= '<br /><p>You can download the attached Zip file to view Backup.</p>'; 
			$message .= '<p>Do not reply on this email.</p>'; 
			  
			$send = $this->send_backup_as_email( $general_email, $message, $backup_file, $latest_filename ); 
			if( $send ){
				echo 'Success auto email complete';
			} else {
				echo 'Error auto email complete';
			}
		}
	}
	
	function send_backup_as_email( $general_email, $message, $backup_file, $file_name ){
	
		$this->load->library('email'); 
		$file_name_date = date('F j, Y',filemtime($backup_file));
		$file_name_time = date('g:i a',filemtime($backup_file));   
	  
		$config['mailtype'] = 'html'; // or html
		$this->email->initialize($config);
		$this->email->clear(TRUE); 
		$this->email->from( 'accounts@iwebprovider.com', 'Admin' );
		$this->email->to( 'accounts@iwebprovider.com', 'Admin' );
		// $this->email->to( $general_email );   
		$this->email->cc( 'rubin@iwebprovider.com', 'Rubin' );   

		$this->email->subject('Database BackUp for ActivAsia '.$file_name_date.' at '.$file_name_time);
		$this->email->message($message);
		$this->email->attach($backup_file); 
		
		$send = $this->email->send();  
		
		if( $send ){
			return TRUE;
		} else {
			return FALSE;
		}
	}
 
}
