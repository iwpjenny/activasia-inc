<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_Logs_Model extends CI_Model {
	 
	public $page = 'MemberLogs';
	public $view = 'memberlogs';
	public $fpage = 'fmemberlogs';
	public $tblname = 'std_member_logs';
	public $tblpref = 'mul_';
	public $tblid = 'mul_id';
	public $per_page = 15;
	public $name = 'Member User Logs';
	public $urc_name = 'member_logs';
	public $description = 'Member User Logs';
	public $logged_user_only = FALSE;
	public $display_section = array('dashboard'=>array('order'=>2));
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),  
		) 
	);
	
	public $member_user_role = array(
		array(
			array('field'=>'view_records','name'=>'View Records','value'=>'yes'),
			array('field'=>'view_details','name'=>'View Details','value'=>'yes')
		)	 
	);
	
    function __construct(){
		
        parent::__construct();
    }

	public function create_table(){
		$charset = $this->db->char_set;
		$collate = $this->db->dbcollat;
		$table_name = $this->db->dbprefix.$this->tblname;
		$prefix = $this->tblpref;
		
		$table_query_structure = "CREATE TABLE IF NOT EXISTS `".$table_name."` (
		  `".$this->tblid."` int(10) NOT NULL AUTO_INCREMENT COMMENT 'User Logs ID',
		  `".$this->Mem->tblid."` int(10) DEFAULT NULL COMMENT 'User ID',
		  `".$prefix."action` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL DEFAULT 'login',
		  `".$prefix."description` varchar(250) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."created` datetime NOT NULL,
		  `".$prefix."modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY (`".$this->tblid."`)
		) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=".$charset." COLLATE=".$collate." ROW_FORMAT=DYNAMIC;";
		  
		$this->load->library('DBTable');
		$this->dbtable->execute( $table_name, $table_query_structure );
	}

	public function ini_custom_models(){
		$this->create_table();
		
		/* Initialize Custom Model here */
		
		/* End of initialize custom model here */	
	}
		
	public function set_join_params(){
		
		$data = array( 
			array($this->Mem->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->Mem->tblid.'='.$this->db->dbprefix.$this->Mem->tblname.'.'.$this->Mem->tblid,'LEFT') 		 
		);
		
		return $data;
	} 
	
	public function set_search_fields(){

		$data = array(
			$this->tblpref.'description',
			$this->tblpref.'created',
			$this->User->tblpref.'firstname', 
			$this->User->tblpref.'lastname'
		);

		return $data;
	}
	
	public function get_total_records(){
		
		$this->db->from($this->tblname);
		$total = $this->db->count_all_results();
		
		return $total;
	}
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
}