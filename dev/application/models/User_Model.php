<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Model extends CI_Model {
	
	public $page = 'User';
	public $view = 'user';
	public $tblname = 'std_user';
	public $tblpref = 'user_';
	public $tblid = 'user_id';
	public $per_page = 15;
	public $name = 'User';
	public $urc_name = 'user';
	public $description = 'Manager User';
	public $folder = 'user';
	public $display_section = array('menu-sidebar-left'=>array('order'=>1),'dashboard'=>array('order'=>3));
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array( 
			array('field'=>'profile','name'=>'Update Profile','value'=>'yes'), 
			array('field'=>'update_password','name'=>'Update Password','value'=>'yes'),
			array('field'=>'picture','name'=>'Picture Upload','value'=>'yes')
		) 
	); 
	
    function __construct(){
		
        parent::__construct();
    }

	public function create_table(){
		$charset = $this->db->char_set;
		$collate = $this->db->dbcollat;
		$table_name = $this->db->dbprefix.$this->tblname;
		$prefix = $this->tblpref;
		$datetime = app_cdt();
		
		$table_query_structure = "CREATE TABLE IF NOT EXISTS `".$table_name."` (
		  `".$this->tblid."` int(10) NOT NULL AUTO_INCREMENT COMMENT 'User ID',
		  `".$this->UR->tblid."` int(10) NOT NULL COMMENT 'User Role ID',
		  `".$prefix."username` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."password` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."email` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."firstname` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."midname` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."lastname` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."created` datetime NOT NULL,
		  `".$prefix."modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		  `".$prefix."published` tinyint(1) NOT NULL DEFAULT '1',
		  PRIMARY KEY (`".$this->tblid."`)
		) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=".$charset." COLLATE=".$collate." ROW_FORMAT=DYNAMIC;
		INSERT INTO ".$table_name." VALUES ('1', '17', 'iwpadmin', 'Cm5@App!5', 'account@iwebprovider.com', 'iWP', 'A', 'Admin', '".$datetime."', '".$datetime."', '1');";
		  
		$this->load->library('DBTable');
		$this->dbtable->execute( $table_name, $table_query_structure );
	}

	public function ini_custom_models(){
		$this->create_table();
		$this->UP->create_table();
		$this->UR->create_table();
		$this->UL->create_table();
		
		/* Initialize Custom Model here */
		
		/* End of initialize custom model here */	
	}
	
	public function set_search_fields(){
		
		$data = array(
			$this->tblpref.'firstname',
			$this->tblpref.'midname',
			$this->tblpref.'lastname',
			$this->tblpref.'username',
			$this->UR->tblpref.'title'
		);
		
		return $data;
	}
	
	public function set_join_params(){
				
		$data = array( 
			array($this->UP->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->tblid.'='.$this->db->dbprefix.$this->UP->tblname.'.'.$this->tblid,'LEFT'), 
			array($this->UR->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->UR->tblid.'='.$this->db->dbprefix.$this->UR->tblname.'.'.$this->UR->tblid,'LEFT')
		);
		
		return $data;
	}
	
	function get_by_id( $id ){
		
		$join_params = $this->set_join_params(); 
		$where_params = array( $this->tblname.'.'.$this->tblid=>$id, $this->tblpref.'published'=>1 );
		
		$query_params = array(
			'join_params'=>$join_params,
			'where_params'=> $where_params
		);
		
		$result = $this->Data->get_record( $this, $query_params );
		
		return $result;
	}
	
	function get_by_username_email( $username, $by_username_email=FALSE ){
		
		if( $by_username_email ){   
			$where_params = "(".$this->tblpref."username='".$username."' OR ".$this->tblpref."email='".$username."')";
			$where_params .= " AND ".$this->tblpref."published='1'"; 
		} else {
			$where_params = array(
				$this->tblpref.'username'=>$username,
				$this->tblpref.'published'=>1
			);
		}
		
		$join_params = $this->set_join_params(); 
		
		$query_params = array(
			'join_params'=>$join_params,
			'where_params'=> $where_params
		);
		
		$result = $this->Data->get_record( $this, $query_params );
		
		return $result;
	}
	
	function get_logged_user(){
		
		$id = $this->logged->get_login_session('user_id');
				
		$result = $this->get_by_id( $id );
		
		return $result;
	}
	
	function get_by_username( $username ){
		$this->load->model('Data_Model', 'Data');
				
		$result = $this->Data->get_record($this, 
			array( 'where_params' => array(
					$this->tblpref.'username'=>$username,
					$this->tblpref.'published'=>1
				)
		));
		
		return $result;
	}
	
	function select( $selected_value='', $class='', $attr='' ){	
		$this->load->model('Data_Model', 'Data');
		/*---------------------------------------------*/
		
		$rows = $this->Data->get_records( $this, array('where_params'=>array($this->tblpref.'published'=>'1'), 'lastname', 'ASC' ) );
		
		$result = '<select name="'.$this->tblid.'" class="form-control '.$class.'" '.$attr.'>';
		if( $selected_value == '' || $selected_value == '0' ){
			$result .= '<option value="" selected="selected">---select---</option>';
		} else {
			$result .= '<option value="">---select---</option>';
		}
		if( $rows ){
			foreach($rows as $col){
				$idvar = $this->tblid;
				$firstname = $this->tblpref.'firstname';
				$lastname = $this->tblpref.'lastname';

				$id = $col->$idvar;
				$firstname = get_value($col,$this->tblpref.'firstname');
				$middlename = get_value($col,$this->tblpref.'midname');
				$lastname = get_value($col,$this->tblpref.'lastname');
				
				$fullname = $this->Gen->set_fullname_format($firstname, $middlename, $lastname, '{lastname}, {firstname} {mi}.');

				if( $selected_value == $id ){
					$result .= '<option value="'.$id.'" selected="selected">'.$fullname.'</option>';
				} else {
					$result .= '<option value="'.$id.'">'.$fullname.'</option>';
				}
			}
		}
		$result .= '</select>';
		
		return $result;

	}
	
	public function get_total_records(){
		
		$this->db->from($this->tblname);
		$this->db->where(array($this->tblpref.'published' =>'1'));
		$total = $this->db->count_all_results();
		
		return $total;
	}
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
	public function get_recepients( $records ){
		if(is_array($records )){
			$query_params = array(   
				'where_in_key'=>$this->UR->tblid,
				'where_in_value'=>$records,
			);  
		}
		else{
			$query_params = array(   
				'where_params'=> array($this->UR->tblid => $records)
			);  
		}    
		$result = $this->Data->get_records( $this, $query_params );   
		return $result;
	}
}