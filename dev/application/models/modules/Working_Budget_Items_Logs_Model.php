<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Working_Budget_Items_Logs_Model extends CI_Model {
	
	public $page = 'ModWorkingBudgetItems';
	public $view = 'working-budget-items';
	public $fpage = '';
	public $tblname = 'working_budget_items_logs';
	public $tblpref = 'wbi_';
	public $tblid = 'wbi_id';
	public $urc_name = 'working_budget_items';
	public $name = 'Budget Control Sheet Items';
	public $description = 'Budget Control Sheet Items';
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'export','name'=>'Export Excel','value'=>'yes'),
			array('field'=>'import','name'=>'Import Excel','value'=>'yes'),
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array( 
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->helper('functions');
		$this->load->database();
		
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */ 
		$this->load->model('modules/Working_Budget_Model', 'WB'); 
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
		 $data = array(
			array($this->WB->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->WB->tblid.'='.$this->WB->tblname.'.'.$this->WB->tblid,'LEFT'), 
		);
		
		return $data;
	}
	
	public function set_search_fields(){ 
		$this->load->model('modules/CE_Records_Model', 'CE'); 
		$this->load->model('modules/Region_Model', 'Reg');
		$this->load->model('modules/Area_Model', 'Area');
		/*---------------------------------------------*/
		$data = array( 
		);	
		 
		
		return $data;
	} 
	 
	function get_select( $ModelUsedBy, $record_fields ){
		
		$id = get_value($record_fields, $this->tblid);
		
		$select = $this->records->select($this, $ModelUsedBy, $id, array(
			'query_params'=>array('orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));
		
		return $select;
	}
	 
	/*---------------------------------------------*/
	
	/*Custom functions here------------------------*/ 
	function get_items_list( $wb_id ){  
		$query_params = array( 
			'where_params' => array( $this->WB->tblpref.'logs_id' => $wb_id)
		);    
		$return = $this->Data->get_records( $this, $query_params );   
		return $return; 
	}
	 
}
