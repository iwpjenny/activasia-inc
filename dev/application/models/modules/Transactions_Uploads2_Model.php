<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transactions_Uploads2_Model extends CI_Model {

	public $page = 'modtransactionsuploads2';
	public $view = 'transactions-uploads2';
	public $tblname = 'std_transactions_uploads';
	public $tblpref = 'tu_';
	public $tblid = 'tu_id';
	public $urc_name = 'transactions_uploads';
	public $name = 'Transactions Uploads 2';
	public $description = 'Transactions Upload Files 2';
	public $folder = 'uploads';
	public $logged_user_only = FALSE;

	public $user_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes'),
			array('field'=>'upload','name'=>'Upload','value'=>'yes')
		),
		array(
			array('field'=>'download','name'=>'Download','value'=>'yes')
		)
	);

    function __construct(){

        parent::__construct();
    }

	public function ini_custom_models(){
		$this->load->model('modules/Transactions_Model', 'Trans');
		
		/* Initialize Custom Model here */
		
		/* End of initialize custom model here */	
	}

	public function set_join_params(){

		$data = array(
			array($this->User->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->User->tblid.'='.$this->db->dbprefix.$this->User->tblname.'.'.$this->User->tblid,'LEFT')
		);

		return $data;
	}

	public function set_search_fields(){

		$data = array(
			$this->tblpref.'filepath',
			$this->tblpref.'title',
			$this->User->tblpref.'firstname',
			$this->User->tblpref.'lastname'
		);

		return $data;
	}

	public function set_order_by(){

		$field = $this->tblpref.'created';
		
		return $field;
	}

	function get_select( $ModelUsedBy, $record_fields, $where_params=array() ){

		$id = get_value($record_fields, $this->tblid);
		$select = $this->fields->select($this, $ModelUsedBy, $id, array(			
			'query_params'=>array('where_params'=>$where_params,'orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));

		return $select;
	}
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
}
