<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transactions_Sub_Model extends CI_Model {
	
	public $page = 'modtransactionssub';
	public $view = 'transactions-sub';
	public $fpage = 'fmodtransactionssub';
	public $tblname = 'transactions_sub';
	public $tblpref = 'trs_';
	public $tblid = 'trs_id';
	public $urc_name = 'transactions_sub';
	public $name = 'Transactions Sub';
	public $description = 'Transactions Sub';
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){
		
        parent::__construct();
    }

	public function ini_custom_models(){
		$this->load->model('modules/Transactions_Model', 'Trans');
		$this->load->model('modules/Transactions_Files_Model', 'TransF');
		
		/* Initialize Custom Model here */
		
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
		
		$data = array( 
			array($this->Trans->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->Trans->tblid.'='.$this->db->dbprefix.$this->Trans->tblname.'.'.$this->Trans->tblid,'LEFT'),
			array($this->Mem->tblname,$this->db->dbprefix.$this->Trans->tblname.'.'.$this->Mem->tblid.'='.$this->db->dbprefix.$this->Mem->tblname.'.'.$this->Mem->tblid,'LEFT')
		);
		
		return $data;
	}
	
	public function set_search_fields(){
		$this->load->model('Transactions_Model', 'Trans'); 
		
		$data = array(
			$this->tblname.'.'.$this->tblid,
			$this->tblpref.'title',
			$this->tblpref.'description',
			$this->tblpref.'created'
		);
		
		return $data;
	}

	function get_select( $ModelUsedBy, $record_fields ){
		
		$id = get_value($record_fields, $this->tblid);		
		$select = $this->records->select($this, $ModelUsedBy, $id, array(
			'query_params'=>array('orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));
		
		return $select;
	}
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
}
