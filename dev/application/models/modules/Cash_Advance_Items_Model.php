<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cash_Advance_Items_Model extends CI_Model {
	
	public $page = 'ModCashAdvanceItems';
	public $view = 'cash-advance-items';
	public $fpage = '';
	public $tblname = 'cash_advance_items';
	public $tblpref = 'cai_';
	public $tblid = 'cai_id';
	public $urc_name = 'cash_advance_items';
	public $name = 'Cash Advance Items';
	public $description = 'Cash Advance Items';
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'), 
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'trash','name'=>'Trash','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'export','name'=>'Export Excel','value'=>'yes'),
			array('field'=>'import','name'=>'Import Excel','value'=>'yes'),
			
		)
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){ 
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */ 
		$this->load->model('modules/Cash_Advance_Model', 'CA');
		$this->load->model('modules/Working_Budget_Items_Model', 'WBI');
		$this->load->model('modules/Working_Budget_Model', 'WB');
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		$this->load->model('modules/Cost_Estimate_Items_Model', 'CEI');
		$this->load->model('modules/Region_Model', 'Reg');
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){ 
		
		$data = array( 
			array($this->CA->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->CA->tblid.'='.$this->CA->tblname.'.'.$this->CA->tblid,'LEFT'),
			// array($this->WBI->tblname,$this->db->dbprefix.$this->WBI->tblname.'.'.$this->WBI->tblid.'='.$this->WBI->tblname.'.'.$this->WBI->tblid,'LEFT')
		);
		
		return $data;		 
	}
	
	public function set_search_fields(){ 
		 
		$data = array(
			 
		);
		
		return $data;
	} 
	 
	function get_select( $ModelUsedBy, $record_fields ){
		
		$id = get_value($record_fields, $this->tblid);
		
		$select = $this->records->select($this, $ModelUsedBy, $id, array(
			'query_params'=>array('orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));
		
		return $select;
	} 

	function get_checked_items( $ca_id ){
		if( $ca_id ){ 
			$checked = $this->Data->get_records( $this, array( 'where_params' => array( $this->CA->tblid => $ca_id, $this->tblpref.'trashed' => 0 ) ) ); 
			return $checked;
		}
	} 
	
	function get_total_amount( $ca_id ){
		// printx($ca_id );
		$this->load->model('modules/Working_Budget_Items_Model', 'WBI');
		$this->load->library('Module');
		if( $ca_id ){  
			$checked_items = $this->Data->get_records( $this, array( 
				'where_in_key'=>$this->CA->tblid,
				'where_in_value'=>$ca_id,
			) ); 
			$total = 0;
			foreach($checked_items as $col){
				$wb_id = get_value($col,$this->WBI->tblid);
				$item_details = $this->module->record( $this->WBI, $wb_id );
				$bcs_ewt = get_value($item_details, $this->WBI->tblpref.'bcs_ewt');
				$total += $bcs_ewt;
			}
			return $total;
			
		}
	}  
	
	function save(){
		$items = $this->input->post('items'); 
		$ca_id = $this->input->post('parent_id'); 
		$header_1_items = $this->input->post('header_1'); 
		$header_2_items = $this->input->post('header_2'); 
		$header_3_items = $this->input->post('header_3'); 
		$header_4_items = $this->input->post('header_4'); 
		$ca_date_needed = $this->input->post('ca_date_needed');
		$bcs = $this->input->post('bcs');  
		$balance = $this->input->post('balance');  
	 
		$ca_data = array( 
			$this->CA->tblpref.'date_needed' => $ca_date_needed,
		);
		$result = $this->Data->update_data( $this->CA, $ca_data, array( $this->CA->tblid => $ca_id ) );	
		if($items){
			foreach($items as $item){
				$item_details = $this->module->record( $this->WBI, $item ); 
				$name = get_value($item_details,$this->WBI->tblpref.'name');  
				$code = get_value($item_details,$this->WBI->tblpref.'code');  
				$description = get_value($item_details,$this->WBI->tblpref.'description');  
				$unit_cost = get_value($item_details,$this->WBI->tblpref.'unit_cost');  
				$unit_measure = get_value($item_details,$this->WBI->tblpref.'unit_measure');  
				$multiplier_product = get_value($item_details,$this->WBI->tblpref.'multiplier_product');  
				$bcs_ewt = get_value($item_details,$this->WBI->tblpref.'bcs_ewt');  
				$header_1 = $header_1_items[$item];  
				$header_2 = $header_2_items[$item];  
				$header_3 = $header_3_items[$item];  
				$header_4 = $header_4_items[$item];   
				$bcs = $bcs[$item];   
				$ca_details = $this->get_ca_total_balance($ca_id);
				$ca_details_balance = $ca_details['balance'];
				$ca_details_balance = number_format((float)$ca_details_balance,2,'.','');
				$amount = number_format((float)$bcs_ewt,2,'.','');
				$item_balance = $this->get_balance_per_item($item, $bcs_ewt); 
				$multiplier_product = $header_1 * $header_2 * $header_3 * $header_4; 
				$total =  $multiplier_product * $unit_cost; 
				$balance = $item_balance - $total;
				/* printr("hello");
				printr($amount);
				printr($ca_details_balance); */
				
				// if($amount <= $ca_details_balance && $balance >= 0){ i forgot why!
				if($balance >= 0){
					// printr("gasulod ka diri?");
					$check = array(
						$this->CA->tblid => $ca_id,
						$this->WBI->tblid => $item, 
						$this->CAI->tblpref.'code' => $code,
						$this->CAI->tblpref.'name' => $name,
						$this->CAI->tblpref.'description' => $description,
						$this->CAI->tblpref.'unit_cost' => $unit_cost,
						$this->CAI->tblpref.'unit_measure' => $unit_measure,
						$this->CAI->tblpref.'multiplier_product' => $multiplier_product,
						$this->CAI->tblpref.'bcs_ewt' => $bcs_ewt,
						$this->CAI->tblpref.'header_1' => $header_1,
						$this->CAI->tblpref.'header_2' => $header_2,
						$this->CAI->tblpref.'header_3' => $header_3,
						$this->CAI->tblpref.'header_4' => $header_4,
					);
					$this->records->save( $this, $check);    
				}
				// printx("hello again");
			}
				
		} 
		$this->module->sub_section_join_params = $this->CAI->set_join_params();
		$tbody = $this->module->grid_tbody( $this, $this->CA, $ca_id );
		
		$ca_details = $this->get_ca_total_balance($ca_id);
		
		$page = $this->page;
		$json = array();
		$json['tbody'] = $tbody;
		$json['page'] = $page;
		$json['field_data'] = ''; 
		$json['notification'] = '';
		$json['result'] = TRUE;
		$json['redirect_url'] = '';
		
		$balance = $ca_details['balance'];
		$ca_total = $ca_details['ca_total'];
		$json['balance'] = $balance;
		$json['ca_total'] = $ca_total;
		
		echo json_encode($json);
		exit();		
	}
	
	function get_ca_total_balance($ca_id){
		 if($ca_id){
			 
			$limit = 0;
			$ca_details = $this->module->record( $this->CA, $ca_id );
			$limit = get_value( $ca_details, $this->WB->tblpref.'bcs');
			$wb_id = get_value( $ca_details, $this->WB->tblid);

			$ca_total= $this->WB->get_total_amount( $wb_id, $ca_id );
			$result['ca_total'] = $ca_total;
			$result_balance = $this->CA->get_balance($limit, $wb_id);    
			$result['balance'] = $result_balance;  
			 
			return $result;
		 }
	}
	
	function check_items_total($total_of_item){
		$total = 0;
		$ca_id = $this->input->post('parent_id');  
		$ca_details = $this->get_ca_total_balance($ca_id);
		foreach($total_of_item as $total_item){
			$total += $total_item;
		}
		$ca_details_balance = $ca_details['balance']; 
		$total = number_format((float)$total,2,'.',''); 
		 
		if($total <= $ca_details_balance){
			 $response = 1;
		}
		else{
			$response = 0;
		} 
		
		return $response;
	}
	
	
	function calculate_total(){
		$this->load->model('modules/Cost_Estimate_Model', 'CE'); 
		$this->load->model('modules/Cost_Estimate_Items_Model', 'CEI');
		$header_1 = $this->input->post('header_1');
		$header_2 = $this->input->post('header_2');
		$header_3 = $this->input->post('header_3');
		$header_4 = $this->input->post('header_4');

		$total = array();
		$length = count($header_1);
		foreach($header_1 as $key => $value){ 
			$wb_details = $this->module->record( $this->WBI, $key );
			
			$wbi_code = get_value($wb_details, $this->WBI->tblpref.'code');
			$unit_cost = get_value($wb_details, $this->WBI->tblpref.'unit_cost'); 
			$ce_id = get_value($wb_details, $this->CE->tblid); 
			$asf_amount = $this->CEI->get_asf_value($ce_id, $wbi_code);  
			$multiplier_product =  $header_1[$key] * $header_2[$key] * $header_3[$key] * $header_4[$key];
			$total[$key] = $this->get_individual_total($asf_amount, $unit_cost, $multiplier_product); 
		
		}
		$response = $this->check_items_total($total);
		$result['response'] = $response;
		$result['total'] = $total;
		
		return $result;
		
	}
	
	function json_mod_calculate_total(){
		
		$result = $this->calculate_total();
		
		$json['total'] = $result['total'];
		$json['response'] = $result['response'];
		echo json_encode($json);
		exit();	
	}
	
	 
	function get_individual_total( $unit_cost, $multiplier_product){
		
		$total = $unit_cost * $multiplier_product;
		return $total;
		
	}
	function delete(){
		$id = $this->input->post('id'); 
		$ca_id = $this->input->post('parent_id');  
		
		$delete = $this->Data->delete_data( $this, array($this->tblid=>$id) ); 
		 
		$tbody = $this->module->grid_tbody( $this, $this->CA, $ca_id );
		$ca_details = $this->get_ca_total_balance($ca_id); 
		
		$page = $this->page;
		$json = array();
		$json['tbody'] = $tbody;
		$json['page'] = $page;
		$json['field_data'] = '';
		$json['notification'] = '';
		$json['result'] = TRUE;
		$json['redirect_url'] = '';
		$json['balance'] = $ca_details['balance'];
		if($ca_details['ca_total']){
			$json['ca_total'] = $ca_details['ca_total']; 
		}
		else{
			$json['ca_total'] = 0;
		} 
		echo json_encode($json);
		exit();		
	}
	
	public function grid_tbody( $model, $module_parent, $parent_id ){
		$data['settings'] = $this->settings;
		$data['capabilities'] = $CI->logged->get_user_capabilities();
		
		$data['model'] = $model;
		$data['page'] = $model->page;
		$data['fpage'] = $model->fpage;
		$data['parent_id'] = $parent_id;
		$data['records'] = $this->grid_sub_records( $model, $module_parent, $parent_id );
		
		 
		
		$contents = $CI->load->view($this->location.'/modules/'.$model->view.'/grid-tbody',$data,TRUE);
		
		return $contents;
	}
	
	public function modal_sub_section( $model, $module_parent, $parent_id ){
		
		$contents = '';
		
		if( $parent_id ){
			$data['settings'] = $this->settings;
			$data['location'] = $this->location;
			
			$tbody = $this->grid_tbody( $model, $module_parent, $parent_id );
			$thead = $CI->load->view($this->location.'/modules/'.$model->view.'/grid-thead',$data,TRUE);
			
			$data['id'] = '';
			$data['thead'] = $thead;
			$data['tbody'] = $tbody;
			$data['parent_id'] = $parent_id;
			$data['model'] = $model;
			$data['model_parent'] = $module_parent;
			$data['name'] = $model->name;
			$data['page'] = $model->page;
			$data['fpage'] = $model->fpage;
			$data['tblid'] = $model->tblid;
			$data['tblpref'] = $model->tblpref;
			$data['tblname'] = $model->tblname;
			$data['fields'] = array();
			$data['form_action'] = site_url('c='.$model->page);
			$CI->moduletag->model = $model;
			$CI->moduletag->data = array();
			
			$contents = $CI->load->view($this->location.'/modules/modal-sub-section',$data,TRUE);
		}
				
		return $contents;
	}
	
	public function grid_sub_records( $model, $module_parent, $query_params_or_id ){
		
		$id = $query_params_or_id;
				
		if( is_array($query_params_or_id) ){
			$query_params = $query_params_or_id;
		} else {
			$query_params = array('where_params'=>array($module_parent->tblid=>$id,$model->tblpref.'trashed'=>0));
		}
			
		$records = $this->Data->get_records( $model, $query_params ); 
		
		return $records;
	} 
	
	function get_balance_per_item( $wbi_id, $bcs_ewt ){
		$join_params = $this->CAI->set_join_params();
		$query_params = array( 
			'where_params' => array( 
				$this->WBI->tblid => $wbi_id,
				$this->CA->tblpref.'trashed' => 0 
			),
			'join_params' => $join_params 
		); 
		$records = $this->Data->get_records( $this, $query_params );   
		if($records){
			$total = 0;
			foreach($records as $record){
				$h1 = get_value($record, $this->tblpref.'header_1');
				$h2 = get_value($record, $this->tblpref.'header_2');
				$h3 = get_value($record, $this->tblpref.'header_3');
				$h4 = get_value($record, $this->tblpref.'header_4');
				$h_total = $h1 * $h2 * $h3 * $h4;
				$unit_cost = get_value($record, $this->tblpref.'unit_cost');
				$item_total = $unit_cost * $h_total;
				$total += $item_total;
			}
			$bcs_ewt = $bcs_ewt - $total;
		}
		return $bcs_ewt;
	}
	
	
}