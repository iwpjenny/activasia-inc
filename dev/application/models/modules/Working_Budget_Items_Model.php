<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Working_Budget_Items_Model extends CI_Model {
	
	public $page = 'ModWorkingBudgetItems';
	public $view = 'working-budget-items';
	public $fpage = '';
	public $tblname = 'working_budget_items';
	public $tblpref = 'wbi_';
	public $tblid = 'wbi_id';
	public $urc_name = 'working_budget_items';
	public $name = 'Budget Control Sheet Items';
	public $description = 'Budget Control Sheet Items';
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'export','name'=>'Export Excel','value'=>'yes'),
			array('field'=>'import','name'=>'Import Excel','value'=>'yes'),
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array( 
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->helper('functions');
		$this->load->database();
		
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */ 
		$this->load->model('modules/Working_Budget_Model', 'WB'); 
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
		 $data = array(
			array($this->WB->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->WB->tblid.'='.$this->WB->tblname.'.'.$this->WB->tblid,'LEFT'), 
		);
		
		return $data;
	}
	
	public function set_search_fields(){ 
		$this->load->model('modules/CE_Records_Model', 'CE'); 
		$this->load->model('modules/Region_Model', 'Reg');
		$this->load->model('modules/Area_Model', 'Area');
		/*---------------------------------------------*/
		$data = array( 
		);	
		 
		
		return $data;
	} 
	 
	function get_select( $ModelUsedBy, $record_fields ){
		
		$id = get_value($record_fields, $this->tblid);
		
		$select = $this->records->select($this, $ModelUsedBy, $id, array(
			'query_params'=>array('orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));
		
		return $select;
	}
	 
	/*---------------------------------------------*/
	
	/*Custom functions here------------------------*/ 
	function get_items( $checked_items, $wb_id ){ 
		
		$query_params = array( 
			'where_params' => array( $this->WB->tblid => $wb_id,  $this->tblpref.'trashed' => 0)
		);    
		$select = $this->Data->get_records( $this, $query_params );  
			
		if($checked_items){
			foreach($select as $key => $row){ 
				$id = get_value($row, $this->tblid);
				if($checked_items){
					foreach($checked_items as $checked){ 
						$checked_id = get_value($checked, $this->tblid);
						if($checked_id == $id){
							 unset($select[$key]);
						}  
					} 
				} 
			}
		}
		return $select;
		
	}
	
	function get_items_checkbox( $ca_id ){ 
		$this->load->model('modules/Cash_Advance_Items_Model', 'CAI'); 
		
		if($ca_id){
			//get CA details
			$ca_details = $this->module->record( $this->CA, $ca_id );
			$wb_id = get_value($ca_details, $this->WB->tblid);
			$ce_id = get_value($ca_details, $this->CE->tblid); 
			$date_needed = get_value($ca_details, $this->CA->tblpref.'date_needed'); 
			
			$checked_items = $this->CAI->get_checked_items($ca_id);
			$items = $this->get_items( $checked_items, $wb_id); 

			ob_start();
			?>
		 	<table class="item-table table table-bordered"> 
		 		<thead>
		 			<tr>
		 				<th>Code</th>  
		 				<th>Item Name</th> 
		 				<th>Unit Cost</th>
		 				<th>Unit of Measure</th>
		 				<th>BCS w/ EWT</th>
		 				<th>Header 1</th>
		 				<th>Header 2</th>
		 				<th>Header 3</th>
		 				<th>Header 4</th>
		 				<th>Total</th> 
		 				<th width="5%">Balance</th> 
		 			</tr>
	 			</thead>
	 			<tbody>
 					<?php
					if($items){
						foreach($items as $col){
							$id = get_value($col, $this->tblid);  
							$code = get_value($col, $this->tblpref.'code');
							$name = get_value($col, $this->tblpref.'name');
							$description = get_value($col, $this->tblpref.'description');
							$unit_cost = get_value($col, $this->tblpref.'unit_cost');
							$unit_measure = get_value($col, $this->tblpref.'unit_measure');
							$unit = get_value($col, $this->tblpref.'unit');
							$freq = get_value($col, $this->tblpref.'freq');
							$man_days = get_value($col, $this->tblpref.'man_days');
							$unit_cost = get_value($col, $this->tblpref.'unit_cost');
							$total = get_value($col, $this->tblpref.'total');
							$bcs_ewt = get_value($col, $this->tblpref.'bcs_ewt'); 
							$item_balance = $this->CAI->get_balance_per_item($id, $bcs_ewt);
							$page_response =  $this->CAI->page;
							$asf_amount = $this->CEI->get_asf_value($ce_id, $code);
							?>
							<tr>
								<td>
									<input type="hidden" name="item_asf[<?php echo $id; ?>]" value="<?php echo $asf_amount; ?>" />
									<input type="hidden" name="item_unit_cost[<?php echo $id; ?>]" value="<?php echo $unit_cost; ?>" /> 
									<input type="hidden" name="bcs[<?php echo $id; ?>]" value="<?php echo $item_balance; ?>" /> 
									<input type="checkbox" class="item_checkbox" name="items[]" value="<?php echo $id; ?>" onclick="javascript:check_option(<?php echo $id; ?>,'<?php echo $page_response; ?>')" />
									<?php echo $code; ?>
								</td>  
								<td> 
									<span href="#" data-toggle="tooltip" title="<?php echo $description; ?>"><?php echo $name; ?></span>
									<br />
									<small><?php echo $description; ?></small>
								</td> 
								<td><?php echo $unit_cost; ?></td>
								<td><?php echo $unit_measure; ?></td>
								<td><?php echo $bcs_ewt; ?> </td>
								<td><input type="number" class="narrow-font header_input numericOnly form-control input-sm" value="0" name = "header_1[<?php echo $id; ?>]" disabled /></td>  
								<td><input type="number" class="narrow-font header_input numericOnly form-control input-sm" value="0" name = "header_2[<?php echo $id; ?>]" disabled /></td>  
								<td><input type="number" class="narrow-font header_input numericOnly form-control input-sm" value="0" name = "header_3[<?php echo $id; ?>]" disabled /></td>  
								<td><input type="number" class="narrow-font header_input numericOnly form-control input-sm" value="0" name = "header_4[<?php echo $id; ?>]" disabled /></td>  
								<td><input type="text" class="narrow-font form-control input-sm" value="0" name="total[<?php echo $id; ?>]" readonly /></td> 
								<td><input type="text" class="narrow-font form-control input-sm" value="<?php echo number_format((float)$item_balance,2,'.',''); ?>" name="balance[<?php echo $id; ?>]" readonly /></td>  
								<?php /* ?>
								<td><input type="text" class="narrow-font form-control input-sm" value="<?php echo ($unit_cost + $asf_amount); ?>" name="total[<?php echo $id; ?>]" readonly /></td> 
								<?php */ ?>
							</tr> 
							<?php
						}
					}
 					?>
					<tr>
						<td colspan="10" class="text-right">Total:</td> 
						<td><strong id="total-items">0.00</strong></td>
					</tr>
					<tr>
						<td colspan="10" class="text-right">Balance:</td> 
						<td><strong id="total-balance">0.00</strong></td>
					</tr>
					<tr>
						<td colspan="9" class="text-right">Date Needed:</td> 
						<td colspan="2"><input type="text" class="narrow-font form-control datepicker" id="ca_date_needed" value="<?php echo $date_needed; ?>" name="ca_date_needed" readonly/></td> 
					</tr>
	 			</tbody> 
	 		</table> 
	 		<?php	

			$result = ob_get_contents();
			ob_end_clean();
			
			return $result;
		}
	}	

	function get_items_list( $wb_id ){  
		$query_params = array( 
			'where_params' => array( $this->WB->tblid => $wb_id)
		);    
		$return = $this->Data->get_records( $this, $query_params );   
		return $return; 
	}
	
	function duplicate_item_code( $code ){
		if($code){
			$result = FALSE;
			$query_params = array( 
				'where_params' =>array( $this->tblpref.'code like' => $code ) 
			);    
			$record = $this->Data->get_records( $this, $query_params );   
			if($record){
				$result = TRUE;
			} 
			return $result;
		}
	} 
	
	
}
