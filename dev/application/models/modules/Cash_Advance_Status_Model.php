<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cash_Advance_Status_Model extends CI_Model {
	
	public $page = 'ModCashAdvanceStatus';
	public $view = 'cash-advance-status';
	public $fpage = '';
	public $tblname = 'cash_advance_status';
	public $tblpref = 'cas_';
	public $tblid = 'cas_id';
	public $urc_name = 'cash_advance_status';
	public $name = 'Cash Advance Status';
	public $description = 'Cash Advance Status';
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'export','name'=>'Export Excel','value'=>'yes'),
			array('field'=>'import','name'=>'Import Excel','value'=>'yes'),
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->helper('functions');
		$this->load->database();
		
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */ 
		$this->load->model('modules/Working_Budget_Model', 'WB'); 
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		$this->load->model('modules/Cash_Advance_Model', 'CA');  
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
		$this->load->model('modules/Working_Budget_Model', 'WB'); 
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		$this->load->model('modules/Cash_Advance_Model', 'CA');  
		// /*---------------------------------------------*/ 
		$data = array(

			array($this->CA->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->CA->tblid.'='.$this->CA->tblname.'.'.$this->CA->tblid,'LEFT'),
			array($this->WB->tblname,$this->db->dbprefix.$this->CA->tblname.'.'.$this->WB->tblid.'='.$this->WB->tblname.'.'.$this->WB->tblid,'LEFT'),
			array($this->CE->tblname,$this->db->dbprefix.$this->CA->tblname.'.'.$this->WB->tblid.'='.$this->CE->tblname.'.'.$this->CE->tblid,'LEFT')
		);
		
		return $data;
		 
	}
	
	public function set_search_fields(){ 
		 
		$data = array(  
			
		);
		
		return $data;
	} 
	 
	function get_select( $ModelUsedBy, $record_fields ){
		
		$id = get_value($record_fields, $this->tblid);
		
		$select = $this->records->select($this, $ModelUsedBy, $id, array(
			'query_params'=>array('orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));
		
		return $select;
	} 
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/ 
	function get_specific_data( $id ){
		$user_details = $this->Data->get_record( $this, array(
			'where_params'=> array($this->tblid => $id)
		));     
		return $user_details;
	}
}
