<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Working_Budget_Model extends CI_Model {
	
	public $page = 'ModWorkingBudget';
	public $view = 'working-budget';
	public $fpage = '';
	public $tblname = 'working_budget';
	public $tblpref = 'wb_';
	public $tblid = 'wb_id';
	public $urc_name = 'working_budget';
	public $name = 'Budget Control Sheet';
	public $description = 'Budget Control Sheet';
	public $display_section = array('dashboard'=>array('order'=>8));
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			// array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			// array('field'=>'add','name'=>'Add','value'=>'yes'),
			// array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'export','name'=>'Export Excel','value'=>'yes'),
			array('field'=>'import','name'=>'Import Excel','value'=>'yes'),
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array( 
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->helper('functions');
		$this->load->database();
		
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		$this->load->model('modules/Cost_Estimate_Items_Model', 'CEI'); 
		$this->load->model('modules/Cash_Advance_Model', 'CA');
		$this->load->model('modules/Working_Budget_Items_Model', 'WBI');
		$this->load->model('modules/Working_Budget_CSV_Model', 'WBCSV'); 
		$this->load->model('modules/Working_Budget_Logs_Model', 'WBL');  
		$this->load->model('modules/Working_Budget_Items_Logs_Model', 'WBIL');
		$this->load->model('modules/Area_Model', 'Area');
		$this->load->model('modules/Region_Model', 'Reg');
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
		 $data = array(
			array($this->CE->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->CE->tblid.'='.$this->CE->tblname.'.'.$this->CE->tblid,'LEFT'),
			array($this->Reg->tblname,$this->db->dbprefix.$this->CE->tblname.'.'.$this->Reg->tblid.'='.$this->Reg->tblname.'.'.$this->Reg->tblid,'LEFT'),
			// array($this->Area->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->Area->tblid.'='.$this->Area->tblname.'.'.$this->Area->tblid,'LEFT')
		);
		
		return $data;
	}
	
	public function set_search_fields(){ 
		$this->load->model('modules/CE_Records_Model', 'CE'); 
		$this->load->model('modules/Region_Model', 'Reg');
		$this->load->model('modules/Area_Model', 'Area');
		/*---------------------------------------------*/
		$data = array(
			$this->tblpref.'number',
			$this->tblpref.'cost_estimate',
			$this->tblpref.'asf',
			$this->tblpref.'bcs',
			$this->tblpref.'gross_profit',
			$this->tblpref.'gp_percentage',
			$this->tblpref.'savings_total',
			$this->CE->tblpref.'number',
			// $this->CE->tblname.'.'.$this->CE->tblpref.'activity',
			$this->Reg->tblpref.'title'
		);	 
		
		return $data;
	} 
	 
	function get_select( $ModelUsedBy, $record_fields ){
		
		$id = get_value($record_fields, $this->tblid);
		
		$select = $this->records->select($this, $ModelUsedBy, $id, array(
			'query_params'=>array('orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));
		
		return $select;
	}
	 
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
	
	public function get_report($quarter='', $year=''){ 
		if( $year ){
			$where_params = array('YEAR('.$this->tblpref.'created)' => $year );
		} else {
			$where_params = '';
		}
		
		$where_in_key = ' MONTH('.$this->tblpref.'created)';
		if( $quarter == 'q1' ){
			$where_in_value = array('01','02','03');
		} elseif( $quarter == 'q2' ){
			$where_in_value = array('04','05','06'); 
		} elseif( $quarter == 'q3' ){
			$where_in_value = array('07','08','09'); 
		} elseif( $quarter == 'q4' ){
			$where_in_value = array('10','11','12'); 
		} else {
			$where_in_key = '';
			$where_in_value = '';
		} 
		$join_params = $this->set_join_params(); 
		$result = $this->Data->get_records( $this,
			array(
			'join_params'=> $join_params, 
			'where_params'=> $where_params, 
			'where_in_key'=> $where_in_key, 
			'where_in_value'=> $where_in_value
		));   		 
		$report = $this->report_data($result);
		
		return $report;
	}
	
	public function get_report_individual($id){  
		
		$result['wb'] = $this->module->record( $this, $id);  
		$result['wb_items'] = $this->Data->get_records( $this->WBI,array( 'where_params' =>array( $this->WB->tblid => $id)));    
		$report = $this->report_data_individual($result); 
		return $report;
	}
	
	public function report_data($items){
		$result = array(); 
		if($items){
			foreach($items as $item){  
				$ce_number = get_value( $item, $this->CE->tblpref.'number');  
				$date_submitted = get_value( $item, $this->CE->tblpref.'date_submitted');  
				$project_name = get_value( $item, $this->CE->tblpref.'project_name');  
				$duration = get_value( $item, $this->CE->tblpref.'duration');  
				$client_name = get_value( $item, $this->CE->tblpref.'client_name');  
				$client_address = get_value( $item, $this->CE->tblpref.'client_address');  
				$attention_to = get_value( $item, $this->CE->tblpref.'attention_to');  
				$project_cost = get_value( $item, $this->CE->tblpref.'project_cost');  
				$total_asf = get_value( $item, $this->CE->tblpref.'total_asf');  
				$total_ex_vat = get_value( $item, $this->CE->tblpref.'total_ex_vat');  
				$total_inc_vat = get_value( $item, $this->CE->tblpref.'total_inc_vat');    
				$bcs = get_value( $item, $this->tblpref.'bcs');  
				$bcs_number = get_value( $item, $this->tblpref.'number');  
				   
				
				$result[] = array(
					'BCS Code:' => $bcs_number,
					'CE Code:' => $ce_number,
					'Project Name:' => $project_name,
					'Project Duration:' => $duration, 
					'Client:' => $client_name,
					'Address:' => $client_address,
					'Attention To:' => $attention_to,
					'Date Submitted:' => $date_submitted,
					'Project Cost:' => $project_cost, 
					'BCS (with EWT)' => $bcs, 
				);
			}   
			return $result;
		}
	}
	
	public function report_data_individual($result){ 
		if($result){
			$wb_result = $result['wb'];
			$wb_items_result = $result['wb_items']; 	
			$id = get_value( $wb_result, $this->tblid); 
			$ce_number = get_value( $wb_result, $this->CE->tblpref.'number');  
			$wb_number = get_value( $wb_result, $this->tblpref.'number');  
			$bcs = get_value( $wb_result, $this->tblpref.'bcs');  
			$data_wb['head'] = array(
				'BCS Code' => $wb_number, 
				'CE Code' => $ce_number, 
				'BCS (with EWT)' => $bcs
			); 
			foreach($wb_items_result as $item){
				$id = get_value( $item, $this->WBI->tblid);   
				$code = get_value( $item, $this->WBI->tblpref.'code');   
				$name = get_value( $item, $this->WBI->tblpref.'name');   
				$description = get_value( $item, $this->WBI->tblpref.'description');   
				$unit_cost = get_value( $item, $this->WBI->tblpref.'unit_cost');   
				$unit_measure = get_value( $item, $this->WBI->tblpref.'unit_measure');   
				$header_1 = get_value( $item, $this->WBI->tblpref.'header_1');   
				$header_2 = get_value( $item, $this->WBI->tblpref.'header_2');
				$header_3 = get_value( $item, $this->WBI->tblpref.'header_3');   
				$header_4 = get_value( $item, $this->WBI->tblpref.'header_4');   
				$multiplier_product = get_value( $item, $this->WBI->tblpref.'multiplier_product');   
				$bcs_ewt = get_value( $item, $this->WBI->tblpref.'bcs_ewt');   
				  
				$data_wb['items'][] = array(
					'ITEM CODE' => (string)$code,
					'ITEM NAME' => $name,
					'DESCRIPTION' => $description, 
					'UNIT COST' => $unit_cost,
					'UOM' => $unit_measure,
					'HEADER 1' => $header_1,
					'HEADER 2' => $header_2,
					'HEADER 3' => $header_3,
					'HEADER 4' => $header_4,
					'MULTIPLIER PRODUCT' => $multiplier_product,					
					'BCS W/ EWT' => $bcs_ewt, 
				); 
			} 			
			return $data_wb;
		}
	} 
	
	// function get_data_section_records( $data ){
		
		// $join_params = $this->set_join_params();
		// $ca_limit_amount = 0;
		
		// $location = app_get_val($data,'location');
		
		// $data['display_wb'] = app_get_val($data['settings'],'display_wb');  
		// $data['grid_date_time_format'] = app_get_val($data['settings'],'grid_date_time_format'); 
		// $data['character_limit'] = app_get_val($data['settings'],'grid_text_limit');
		
		// /* $join_params = $this->set_join_params();  */
		// $query_params = array(
			// 'limit'=>5,
			// 'join_params' => $join_params,
			// 'orderby' => $this->tblpref.'created',
			// 'where_params' => array($this->tblpref.'trashed' => 0)	
		// ); 
		// $data['records'] = $this->Data->get_records( $this, $query_params ); 
		// $data['section_wb'] = $this->load->view($location.'/modules/'.$this->view.'/section', $data, TRUE); 
		// return $data; 
	// }
	
	function get_data_section_records( $data ){
		
		$table_name = $this->db->dbprefix.$this->tblname;
		$table_exists = $this->db->table_exists( $table_name );
		
		if( $table_exists ){		
			$location = app_get_val($data,'location');
			
			$data['display_latest_transactions'] = app_get_val($data['settings'],'display_latest_transactions'); 
			$data['grid_date_time_format'] = app_get_val($data['settings'],'grid_date_time_format'); 
			$data['grid_text_limit'] = app_get_val($data['settings'],'grid_text_limit');
		 
			$join_params = $this->set_join_params(); 
			$query_params = array(
				'limit'=>5,
				'join_params' => $join_params,
				'orderby' => $this->tblpref.'created',
				'where_params' => array($this->tblpref.'trashed' => 0)	
			);
			if( $this->logged_user_only ){
				$logged_user_id = $this->logged->get_login_session('user_id');
				$query_params = array_merge($query_params,array(
					'where_params'=>array($this->Mem->tblname.'.'.$this->Mem->tblid=>$logged_user_id)
				));
			}
			$data['records'] = $this->Data->get_records( $this, $query_params );
			
			$data['section_wb'] = $this->load->view($location.'/modules/'.$this->view.'/section', $data, TRUE);
		}
		
		return $data;
	}
	
	function get_data_link( $wb_id, $wb_number){
 
		$url = site_url('c='.$this->page.'&m=view&id='.$wb_id); 
		$return = '<a href="'.$url.'" target="_blank">'.$wb_number.'</a>';

		echo $return;
	}

	function get_total_amount( $wb_id, $ca_id ){
		$this->load->model('modules/Cost_Estimate_Items_Model', 'CEI');
		$this->load->library('Module');
		
		$wb_detail =  $this->module->record( $this, $wb_id);
		$ce_id = get_value($wb_detail,$this->CE->tblid); 
 	 	$total = 0; 
 	 	$ca_query_params = array( 
	 	 	'where_params' => array( $this->CA->tblid => $ca_id )
		);    
	 	$ca_item_records = $this->Data->get_records( $this->CAI, $ca_query_params ); 
	 	if( $ca_item_records ){ 
		 	foreach ($ca_item_records as $ca_item_array ) {
 	   			$ca_id = get_value($ca_item_array,$this->CA->tblid);
	 	   		$cai_code = get_value($ca_item_array,$this->CAI->tblpref.'code');
	 	   		$cai_header_1 = get_value($ca_item_array,$this->CAI->tblpref.'header_1');
	 	   		$cai_header_1 = get_value($ca_item_array,$this->CAI->tblpref.'header_1');
	 	   		$cai_header_2 = get_value($ca_item_array,$this->CAI->tblpref.'header_2');
	 	   		$cai_header_3 = get_value($ca_item_array,$this->CAI->tblpref.'header_3');
	 	   		$cai_header_4 = get_value($ca_item_array,$this->CAI->tblpref.'header_4');
	 	
	 	   		$counter_header = $cai_header_1 * $cai_header_2 * $cai_header_3 * $cai_header_4;

	 	   		$wbi_query_params = array( 
		 	 		'where_params' => array( $this->WBI->tblpref.'code' => $cai_code, $this->WB->tblid => $wb_id )
				);    
			 	$wbi_item_record = $this->Data->get_record( $this->WBI, $wbi_query_params );
			 	$unit_cost = get_value($wbi_item_record,$this->WBI->tblpref.'unit_cost');
			 	 
			 	$cei_query_params = array( 
		 	 		'where_params' => array( $this->CEI->tblpref.'code' => $cai_code, $this->CE->tblid => $ce_id )
				);    
			 	$cei_item_record = $this->Data->get_record( $this->CEI, $cei_query_params );
			 	$asf_amount = get_value($cei_item_record,$this->CEI->tblpref.'asf_amount');

			 	$sub_total = $counter_header * $unit_cost;
			 	// $new_sub_total = $sub_total + $asf_amount;
			 	$total += $sub_total;  
	 	   	} 
			return $total; 
 	   	}  
		
 	   
	}  


	function get_total_balance_from_wb($wb_id){

		$wb_detail =  $this->module->record( $this, $wb_id);
		$bcs = get_value($wb_detail,$this->tblpref.'bcs'); 
 		$total_sub = 0;  
		$query_params = array( 
	 	 	'where_params' => array( $this->WB->tblid => $wb_id,  $this->CA->tblpref.'trashed' => 0)
		);    
	 	$ca_records = $this->Data->get_records( $this->CA, $query_params );
		/* printx($this->db->last_query()); */
	 	if($ca_records){
	 		foreach ($ca_records as $ca_col ) {
	 			$ca_id = get_value($ca_col,$this->CA->tblid); 

	 			$ca_amount = $this->get_total_amount( $wb_id, $ca_id );
	 			$total_sub += $ca_amount ; 
		 		 
		 	}
	 	}
	 	$total = $bcs - $total_sub;
	 	return  $total;  
	}

	
}
