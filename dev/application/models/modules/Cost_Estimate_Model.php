<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cost_Estimate_Model extends CI_Model {
	
	public $page = 'ModCostEstimate';
	public $view = 'cost-estimate';
	public $fpage = '';
	public $tblname = 'cost_estimate';
	public $tblpref = 'ce_';
	public $tblid = 'ce_id';
	public $urc_name = 'cost_estimate';
	public $name = 'Cost Estimate';
	public $description = 'Cost Estimate';
	public $display_section = array('dashboard'=>array('order'=>7));
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),  
			// array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'export','name'=>'Export Excel','value'=>'yes'),
			array('field'=>'import','name'=>'Import Excel','value'=>'yes'),
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->helper('functions');
		$this->load->database();
		
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */
		$this->load->model('modules/Region_Model', 'Reg');
		$this->load->model('modules/Cash_Advance_Model', 'CA');   
		$this->load->model('modules/Working_Budget_Model', 'WB');
		$this->load->model('modules/Area_Model', 'Area');
		$this->load->model('modules/Cost_Estimate_Items_Model', 'CEI');
		$this->load->model('modules/Cost_Estimate_Logs_Model', 'CEL');
		$this->load->model('modules/Cost_Estimate_CSV_Model', 'CECSV');
		$this->load->model('modules/Working_Budget_Items_Model', 'WBI');
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
		 $data = array(
			array($this->Reg->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->Reg->tblid.'='.$this->Reg->tblname.'.'.$this->Reg->tblid,'LEFT'), 
		);
		
		return $data;
	}
	
	public function set_search_fields(){ 
		 
		$data = array(
			$this->tblpref.'number',   
			$this->tblpref.'project_name',
			$this->tblpref.'duration',
			$this->tblpref.'client_name',
			$this->tblpref.'client_address', 
			$this->tblpref.'attention_to' 
		);
		
		return $data;
	} 

	function get_select( $ModelUsedBy, $record_fields, $where_params=array() ){

		$id = get_value($record_fields, $this->tblid);
		$select = $this->records->select($this, $ModelUsedBy, $id, array(			
			'query_params'=>array('where_params'=>$where_params,'orderby'=>$this->tblpref.'number','ordertype'=>'ASC'),
			'fields'=>array('number')
		));

		return $select;
	}
	 
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
	public function get_quarter( $date ){
		// yyyy-mm-dd
		
		if( $date ){
			
			$date =  date("m",strtotime( $date ) );	
		
			if( $date >= 01 && $date <= 03 ){
				$quarter = 1;
			}elseif( $date >= 04 && $date <= 06){
				$quarter = 2;
			}elseif( $date >= 07 && $date <= 09 ){
				$quarter = 3;
			}elseif( $date >= 10 && $date <= 12 ){
				$quarter = 4;
			} else {
				$quarter = '';
			}
			return $quarter;
			
		} 
	}
	
	public function get_total_limit( $id ){  
		$this->load->library('module');
		$this->load->model('modules/Working_Budget_Items_Model', 'WBI'); 
		$wb_details = $this->module->record( $this->WB, array($this->WB->tblname.'.'.$this->CE->tblid => $id) );	 
		$ca_limit_amount = get_value($wb_details, $this->WB->tblpref.'bcs');
		return $ca_limit_amount;
		
	}  
	
	public function get_total_released_ca( $id ){ 
		$this->load->model('modules/Cash_Advance_Items_Model', 'CAI');   
		$this->load->model('modules/Cash_Advance_Model', 'CA');   
		$this->load->model('modules/Working_Budget_Model', 'WB');   
		$ca_release_amount = 0;
		$ca_amount = 0;
		
		$wb_details = $this->Data->get_records( $this->WB, array( 'where_params' =>array( $this->tblid => $id, $this->WB->tblpref.'published' => 1, $this->WB->tblpref.'trashed' => 0 ) ) ); 
		// printx($wb_details);
		// printx($this->db->last_query());
		if( $wb_details ){ 
			foreach( $wb_details as $col_wb ){
				$wb_id = get_value( $col_wb, $this->WB->tblid ); 
				
				$record = $this->Data->get_records( $this->CA, array( 'where_params' =>array( $this->WB->tblid => $id, $this->CA->tblpref.'published' => 1, $this->CA->tblpref.'trashed' => 0, $this->CA->tblpref.'status' => 'Released'  ) ) );  
				foreach($record as $item){
					$ca_id = get_value( $item, $this->CA->tblid ); 
					$ca_amount = $this->WB->get_total_amount($wb_id, $ca_id ) ;
					// $ca_amount = $this->CAI->get_total_amount($ca_id) ; 
					$ca_release_amount += $ca_amount; 
				}  
			}  
		} 
		return $ca_release_amount;
	}
	
	public function get_report($quarter='', $year=''){ 
		
		if( $year ){
			$where_params = array('YEAR('.$this->tblpref.'created)' => $year );
		} else {
			$where_params = '';
		}
		
		$where_in_key = ' MONTH('.$this->tblpref.'created)';
		if( $quarter == 'q1' ){
			$where_in_value = array('01','02','03');
		} elseif( $quarter == 'q2' ){
			$where_in_value = array('04','05','06'); 
		} elseif( $quarter == 'q3' ){
			$where_in_value = array('07','08','09'); 
		} elseif( $quarter == 'q4' ){
			$where_in_value = array('10','11','12'); 
		} else {
			$where_in_key = '';
			$where_in_value = '';
		} 
			
		$join_params = $this->set_join_params(); 
		$result = $this->Data->get_records( $this,
			array(
			'join_params'=> $join_params, 
			'where_params'=> $where_params, 
			'where_in_key'=> $where_in_key, 
			'where_in_value'=> $where_in_value
		)); 
		$report = $this->report_data($result); 
		return $report;
	}
	public function get_report_individual($id){  
		
		$result['ce'] = $this->module->record( $this, $id);  
		$result['ce_items'] = $this->Data->get_records( $this->CEI,array( 'where_params' =>array( $this->CE->tblid => $id)));   
		$report = $this->report_data_individual($result);
		
		return $report;
	}
	
	public function report_data($items){ 
		if($items){
			// printx($items);
			foreach($items as $item){
				$number = get_value( $item, $this->tblpref.'number');  
				$date_submitted = get_value( $item, $this->tblpref.'date_submitted');  
				$project_name = get_value( $item, $this->tblpref.'project_name');  
				$duration = get_value( $item, $this->tblpref.'duration');  
				$client_name = get_value( $item, $this->tblpref.'client_name');  
				$client_address = get_value( $item, $this->tblpref.'client_address');  
				$attention_to = get_value( $item, $this->tblpref.'attention_to');  
				$project_cost = get_value( $item, $this->tblpref.'project_cost');  
				$total_asf = get_value( $item, $this->tblpref.'total_asf');  
				$total_ex_vat = get_value( $item, $this->tblpref.'total_ex_vat');  
				$total_inc_vat = get_value( $item, $this->tblpref.'total_inc_vat');   
				
				// $project_cost = number_format( $project_cost,2,'.',',');    
				// $total_asf = number_format( $total_asf,2,'.',',');    
				// $total_ex_vat = number_format( $total_ex_vat,2,'.',',');    
				// $total_inc_vat = number_format( $total_inc_vat,2,'.',',');    
						
				$result[] = array(
					'Project Name:' => $project_name,
					'Project Duration:' => $duration,
					'CE Code:' => $number,
					'Client:' => $client_name,
					'Address:' => $client_address,
					'Attention To:' => $attention_to,
					'Date Submitted:' => $date_submitted,
					'Project Cost:' => $project_cost,
					'ASF:' => $total_asf,
					'Total CE (EX VAT)' => $total_ex_vat,
					'Total CE (VAT INC)' => $total_inc_vat, 
				);
			} 
			return $result;
		}
	}
	
	public function report_data_individual($result){ 
		if($result){
			$ce_result = $result['ce'];
			$ce_items_result = $result['ce_items']; 
			$id = get_value( $ce_result, $this->tblid); 
			$number = get_value( $ce_result, $this->tblpref.'number');  
			$date_submitted = get_value( $ce_result, $this->tblpref.'date_submitted');  
			$project_name = get_value( $ce_result, $this->tblpref.'project_name');  
			$duration = get_value( $ce_result, $this->tblpref.'duration');  
			$client_name = get_value( $ce_result, $this->tblpref.'client_name');  
			$client_address = get_value( $ce_result, $this->tblpref.'client_address');  
			$attention_to = get_value( $ce_result, $this->tblpref.'attention_to');  
			$project_cost = get_value( $ce_result, $this->tblpref.'project_cost');  
			$total_asf = get_value( $ce_result, $this->tblpref.'total_asf');  
			$total_ex_vat = get_value( $ce_result, $this->tblpref.'total_ex_vat');  
			$total_inc_vat = get_value( $ce_result, $this->tblpref.'total_inc_vat');   
			$ca_limit = $this->get_total_limit( $id ); 
			$ca_released = 	$this->get_total_released_ca( $id );
			$ca_limit_released = $ca_limit - $ca_released;    
			$data_ce['head'] = array(
				'Project Name:' => $project_name,
				'Project Duration:' => $duration,
				'CE Code:' => $number,
				'Client:' => $client_name,
				'Address:' => $client_address,
				'Attention To:' => $attention_to,
				'Date Submitted:' => $date_submitted,
				'Project Cost:' => $project_cost,
				'ASF:' => $total_asf,
				'Total CE (EX VAT)' => $total_ex_vat,
				'Total CE (VAT INC)' => $total_inc_vat, 
			); 
			foreach($ce_items_result as $item){
				$id = get_value( $item, $this->CEI->tblid);   
				$code = get_value( $item, $this->CEI->tblpref.'code');  
				$name = get_value( $item, $this->CEI->tblpref.'name');  
				$description = get_value( $item, $this->CEI->tblpref.'description');  
				$unit_cost = get_value( $item, $this->CEI->tblpref.'unit_cost');  
				$unit_measure = get_value( $item, $this->CEI->tblpref.'unit_measure');  
				$header_1 = get_value( $item, $this->CEI->tblpref.'header_1');  
				$header_2 = get_value( $item, $this->CEI->tblpref.'header_2');  
				$header_3 = get_value( $item, $this->CEI->tblpref.'header_3');  
				$header_4 = get_value( $item, $this->CEI->tblpref.'header_4');  
				$multiplier_product = get_value( $item, $this->CEI->tblpref.'multiplier_product');  
				$asf = get_value( $item, $this->CEI->tblpref.'asf');  
				$asf_amount = get_value( $item, $this->CEI->tblpref.'asf_amount');  
				$total = get_value( $item, $this->CEI->tblpref.'total');  
				
				// $unit_cost = number_format( $unit_cost,2,'.',',');    
				// $header_1 = number_format( $header_1,2,'.',',');    
				// $header_2 = number_format( $header_2,2,'.',',');    
				// $header_3 = number_format( $header_3,2,'.',',');  
				// $header_4 = number_format( $header_4,2,'.',',');  
				// $multiplier_product = number_format( $multiplier_product,2,'.',',');  
				// $asf = number_format( $asf,2,'.',',');  
				// $asf_amount = number_format( $asf_amount,2,'.',',');  
				// $total = number_format( $total,2,'.',',');  
				
				$data_ce['items'][] = array( 
					'ITEM CODE' => (string)$code,
					'ITEM NAME' => $name,
					'DESCRIPTION' => $description,
					'UNIT COST' => $unit_cost,
					'UOM' => $unit_measure,
					'HEADER 1' => $header_1,
					'HEADER 2' => $header_2,
					'HEADER 3' => $header_3,
					'HEADER 4' => $header_4,
					'MULTIPLIER PRODUCT' => $multiplier_product,
					'ASF' => $asf,
					'ASF AMOUNT' => $asf_amount,
					'TOTAL' => $total,
				); 
			}  
			return $data_ce;
		}
	}
	
	// function get_data_section_records( $data ){
		
		// $join_params = $this->set_join_params();
		// $ca_limit_amount = 0;
		
		// $location = app_get_val($data,'location');
		
		// $data['display_ce'] = app_get_val($data['settings'],'display_ce');  
		// $data['grid_date_time_format'] = app_get_val($data['settings'],'grid_date_time_format'); 
		// $data['character_limit'] = app_get_val($data['settings'],'grid_text_limit');
		
		// /* $join_params = $this->set_join_params();  */
		// $query_params = array(
			// 'limit'=>5,
			// 'join_params' => $join_params,
			// 'orderby' => $this->tblpref.'created',
			// 'where_params' => array($this->tblpref.'trashed' => 0),
		// ); 
		// $data['records'] = $this->Data->get_records( $this, $query_params );
		// $data['section_ce'] = $this->load->view($location.'/modules/'.$this->view.'/section', $data, TRUE);
		
		// return $data;
	// }
	
	function get_ce_details($id =''){ 
		if($id){
			$item = $this->module->record( $this, $id );
			return $item;
		} 
	}

	function get_ce_cancel_button( $id, $cancelled ){ 
		if($id && $cancelled == false || $cancelled == 0 ){ 
			ob_start();
			?>
			<a class="btn btn-danger btn-xs confirmation" href="#cancelled-confirmation-<?php echo $id;?>"><span class="glyphicon glyphicon-ban-circle" aria-hidden="true" title="Cancelled Record ID #<?php echo $id;?>"></span></a>
			<div id="cancelled-confirmation-<?php echo $id;?>" style="display:none;">
			<p>Do you want to cancel this record? <a class="btn btn-danger btn-sm" href="<?php echo base_url('index.php?c='.$this->page.'&m=cancel&id='.$id)?>"><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>Yes Cancelled</a></p>
			</div>
			<?php
			$contents = ob_get_contents();
			ob_end_clean();
			
			return $contents;
		}
	}

	function get_ce_details_by_number($number){ 
		if($number){ 
			$ce_details = $this->Data->get_record( $this , array(
				'where_params' =>array( $this->tblpref.'number like' => $number, $this->tblpref.'trashed' => 0, $this->tblpref.'cancelled' => 0 ) 
			));   
			return $ce_details;
		} 
	}

	function get_data_link( $ce_id, $ce_number){
 
		$url = site_url('c='.$this->page.'&m=view&id='.$ce_id); 
		$return = '<a href="'.$url.'" target="_blank">'.$ce_number.'</a>';

		echo $return;
	}
	
	function get_data_section_records( $data ){
		
		$table_name = $this->db->dbprefix.$this->tblname;
		$table_exists = $this->db->table_exists( $table_name );
		
		if( $table_exists ){		
			$location = app_get_val($data,'location');
			
			$data['display_latest_transactions'] = app_get_val($data['settings'],'display_latest_transactions'); 
			$data['grid_date_time_format'] = app_get_val($data['settings'],'grid_date_time_format'); 
			$data['grid_text_limit'] = app_get_val($data['settings'],'grid_text_limit');
		 
			$join_params = $this->set_join_params(); 
			$query_params = array(
				'limit'=>5,
				'join_params' => $join_params,
				'orderby' => $this->tblpref.'created',
				'where_params' => array($this->tblpref.'trashed' => 0),
			);
			if( $this->logged_user_only ){
				$logged_user_id = $this->logged->get_login_session('user_id');
				$query_params = array_merge($query_params,array(
					'where_params'=>array($this->Mem->tblname.'.'.$this->Mem->tblid=>$logged_user_id)
				));
			}
			$data['records'] = $this->Data->get_records( $this, $query_params );
			
			$data['section_ce'] = $this->load->view($location.'/modules/'.$this->view.'/section', $data, TRUE);
		}
		
		return $data;
	}
	
	
	
}
