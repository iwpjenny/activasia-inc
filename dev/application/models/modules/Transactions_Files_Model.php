<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transactions_Files_Model extends CI_Model {
	
	public $tblname = 'std_transactions_files';
	public $tblpref = 'trf_';
	public $tblid = 'trf_id';
	public $name = 'Transactions Files';
	public $description = 'Transactions File Attachments';
	public $folder = 'transactions';
}