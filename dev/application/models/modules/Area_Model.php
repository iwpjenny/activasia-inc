<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Area_Model extends CI_Model {
	
	public $page = 'ModArea';
	public $view = 'area';
	public $fpage = '';
	public $tblname = 'area';
	public $tblpref = 'area_';
	public $tblid = 'area_id';
	public $urc_name = 'area';
	public $name = 'Area';
	public $description = 'Area';
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		) 
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){ 
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */
		$this->load->model('modules/Region_Model','Reg');
		$this->load->model('modules/User_Area_Model','UA');
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
		
		 $data = array(
			array($this->Reg->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->Reg->tblid.'='.$this->Reg->tblname.'.'.$this->Reg->tblid,'LEFT')		 
		);
		
		return $data;
	}
	
	public function set_search_fields(){ 
		 
		$data = array(
			$this->tblpref.'title', 
			$this->tblpref.'description',
			$this->tblpref.'name'
		);
		
		return $data;
	} 


	public function set_order_by(){

		$field = $this->tblpref.'created';

		return $field;
	}
  
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/   
	function get_select( $ModelUsedBy, $record_fields ){
		$this->load->model('modules/User_Area_Model','UA'); 
		$this->load->model('modules/Cost_Estimate_Model','CE');  
		
		$logged_user_id = $this->logged->get_login_session('user_id');
		$ce_id = get_value($record_fields, $this->CE->tblid);
		$ce_details = $this->CE->get_ce_details($ce_id);
		$reg_id = get_value($ce_details, $this->Reg->tblid);    
		$user_areas = $this->UA->get_checked_items( $logged_user_id, $reg_id ); 
		$id = get_value($record_fields, $this->tblid); 
		
		$user_areas_array = array();
		if( $user_areas ){
			foreach($user_areas as $col){
				$user_areas_array[] = app_get_val($col,$this->tblid);
			}
		}  
		if($user_areas_array){
			$select = $this->records->select($this, $ModelUsedBy, $id, array(
				'query_params'=>array(
					'orderby'=>$this->tblpref.'title',
					'ordertype'=>'ASC',
					'where_in_key'=>$this->tblid,
					'where_in_value'=>$user_areas_array,
				),
				'fields'=>array('title')
			));
		}
		else{
			$select = 'No Areas Available';
		} 
		
		
		return $select;
	}

	function get_items($id = ''){   
		$query_params = array( 
			'where_params' => array( $this->Reg->tblid => $id )
			);    
		$rows = $this->Data->get_records($this, $query_params);
		return $rows;
	} 
	
	function get_checkbox($reg_id = '', $input='', $user_id){   
		$record = $this->get_items($reg_id);
		if($reg_id){
			$checked_items = $this->UA->get_checked_items($user_id); 
		}  
		if($record){
			$input .= '<ul class="area-list">';
			foreach($record as $item){
				$id = get_value($item, $this->tblid);
				$title = get_value($item, $this->tblpref.'title'); 
				$input .=  '<li><input type="checkbox" name="area[]" value="'.$id.'" class="area_child_'.$reg_id.'"'; 
				if($checked_items){
					foreach($checked_items as $checked){ 
						$checked_id = get_value($checked, $this->tblid);
						if($checked_id == $id){
							$input .=  'checked';
						} 
					}
				}
				$input .=  '>&nbsp;&nbsp;'.$title.'</li>';
			}
			$input .= '</ul>';
		}
		return $input;
	} 
}
