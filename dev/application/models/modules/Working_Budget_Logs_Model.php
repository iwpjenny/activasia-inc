<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Working_Budget_Logs_Model extends CI_Model {
	
	public $page = 'ModWorkingBudgetLogs';
	public $view = 'working-budget-logs';
	public $fpage = '';
	public $tblname = 'working_budget_logs';
	public $tblpref = 'wb_logs_';
	public $tblid = 'wb_logs_id';
	public $urc_name = 'working_budget_logs';
	public $name = 'Working Budget Logs';
	public $description = 'Working Budget Logs';
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		) 
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->helper('functions');
		$this->load->database();
		
        parent::__construct();
    }
	
	public function ini_custom_models(){ 
		/* Initialize Custom Model here */ 
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		$this->load->model('modules/Area_Model', 'Area');
		$this->load->model('modules/Region_Model', 'Reg');
		$this->load->model('modules/Cash_Advance_Model', 'CA');
		$this->load->model('modules/Working_Budget_Items_Model', 'WBI');
		$this->load->model('modules/Working_Budget_Items_Logs_Model', 'WBIL');
		$this->load->model('modules/Working_Budget_Model', 'WB'); 

		/* End of initialize custom model here */	
	}

	public function set_join_params(){

		$data = array( 
			array($this->CE->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->CE->tblid.'='.$this->CE->tblname.'.'.$this->CE->tblid,'LEFT'),
			array($this->Reg->tblname,$this->db->dbprefix.$this->CE->tblname.'.'.$this->Reg->tblid.'='.$this->Reg->tblname.'.'.$this->Reg->tblid,'LEFT') 
		);

		return $data;
	}

	public function set_search_fields(){
		$this->load->model('Member_Model', 'Mem');

		$data = array( 
			$this->tblpref.'title', 
		);

		return $data;
	}

	function get_select( $ModelUsedBy, $record_fields, $where_params=array() ){

		$id = get_value($record_fields, $this->tblid);
		$select = $this->records->select($this, $ModelUsedBy, $id, array(			
			'query_params'=>array('where_params'=>$where_params,'orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));

		return $select;
	}
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/ 
	public function get_report_individual($id){  
		
		$result['wb'] = $this->module->record( $this, $id);  
		$result['wb_items'] = $this->Data->get_records( $this->WBIL,array( 'where_params' =>array( $this->tblid => $id)));   
		$report = $this->WB->report_data_individual($result);
		
		return $report;
	}
}
