<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transactions_Model extends CI_Model {

	public $page = 'modtransactions';
	public $view = 'transactions';
	public $fpage = 'fmodtransactions';
	public $tblname = 'std_transactions';
	public $tblpref = 'tr_';
	public $tblid = 'tr_id';
	public $urc_name = 'transactions';
	public $name = 'Transactions';
	public $description = 'Transactions';
	public $logged_user_only = FALSE;
	public $display_section = array('dashboard'=>array('order'=>5));

	public $user_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);

	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);

    function __construct(){

        parent::__construct();
    }

	public function ini_custom_models(){
		$this->load->model('modules/Transactions_Sub_Model', 'TransSub');
		// $this->load->model('modules/Transactions_Sub2_Model', 'TransSub2');
		$this->load->model('modules/Transactions_Files_Model', 'TransF');
		$this->load->library('modulefiles');
		$this->modulefiles->create_table( $this, $this->TransF );
		
		/* Initialize Custom Model here */
		
		/* End of initialize custom model here */	
	}

	public function set_join_params(){

		$data = array(
			array($this->Mem->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->Mem->tblid.'='.$this->db->dbprefix.$this->Mem->tblname.'.'.$this->Mem->tblid,'LEFT')
		);

		return $data;
	}

	public function set_search_fields(){
		$this->load->model('Member_Model', 'Mem');

		$data = array(
			$this->tblpref.'title',
			$this->tblpref.'description',
			$this->tblpref.'created',
			$this->Mem->tblpref.'firstname',
			$this->Mem->tblpref.'lastname'
		);

		return $data;
	}

	public function set_order_by(){

		$field = $this->tblpref.'created';

		return $field;
	}

	function get_select( $ModelUsedBy, $record_fields, $where_params=array() ){

		$id = get_value($record_fields, $this->tblid);
		$select = $this->fields->select($this, $ModelUsedBy, $id, array(			
			'query_params'=>array('where_params'=>$where_params,'orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));

		return $select;
	}
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
	function get_data_section_records( $data ){
		
		$table_name = $this->db->dbprefix.$this->tblname;
		$table_exists = $this->db->table_exists( $table_name );
		
		if( $table_exists ){		
			$location = app_get_val($data,'location');
			
			$data['display_latest_transactions'] = app_get_val($data['settings'],'display_latest_transactions'); 
			$data['grid_date_time_format'] = app_get_val($data['settings'],'grid_date_time_format'); 
			$data['character_limit'] = app_get_val($data['settings'],'grid_text_limit');
			
			$join_params = $this->Trans->set_join_params(); 
			$query_params = array(
				'limit'=> 5,
				'join_params'=>$join_params
			);
			if( $this->logged_user_only ){
				$logged_user_id = $this->logged->get_login_session('user_id');
				$query_params = array_merge($query_params,array(
					'where_params'=>array($this->Mem->tblname.'.'.$this->Mem->tblid=>$logged_user_id)
				));
			}
			$data['records'] = $this->Data->get_records( $this, $query_params );
			
			$data['section_transactions'] = $this->load->view($location.'/modules/'.$this->view.'/section', $data, TRUE);
		}
		
		return $data;
	}
}
