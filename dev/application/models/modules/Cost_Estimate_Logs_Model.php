<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cost_Estimate_Logs_Model extends CI_Model {
	
	public $page = 'ModCostEstimateLogs';
	public $view = 'cost-estimate-logs';
	public $fpage = '';
	public $tblname = 'cost_estimate_logs';
	public $tblpref = 'ce_logs_';
	public $tblid = 'ce_logs_id';
	public $urc_name = 'cost_estimate_logs';
	public $name = 'Cost Estimate Logs';
	public $description = 'Cost Estimate Logs';
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		) 
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->helper('functions');
		$this->load->database();
		
        parent::__construct();
    }
	
	public function ini_custom_models(){ 
		/* Initialize Custom Model here */
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		$this->load->model('modules/Region_Model', 'Reg');
		$this->load->model('modules/Cash_Advance_Model', 'CA');   
		$this->load->model('modules/Working_Budget_Model', 'WB');
		$this->load->model('modules/Area_Model', 'Area'); 
		$this->load->model('modules/Cost_Estimate_Items_Model', 'CEI');
		$this->load->model('modules/Cost_Estimate_Item_Logs_Model', 'CEIL');
		/* End of initialize custom model here */	
	}

	public function set_join_params(){

		$data = array(
			 array($this->Reg->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->Reg->tblid.'='.$this->Reg->tblname.'.'.$this->Reg->tblid,'LEFT')
		);

		return $data;
	}

	public function set_search_fields(){
		$this->load->model('Member_Model', 'Mem');

		$data = array( 
			$this->tblpref.'title', 
		);

		return $data;
	}

	function get_select( $ModelUsedBy, $record_fields, $where_params=array() ){

		$id = get_value($record_fields, $this->tblid);
		$select = $this->records->select($this, $ModelUsedBy, $id, array(			
			'query_params'=>array('where_params'=>$where_params,'orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));

		return $select;
	}
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/ 
	public function get_report_individual($id){  
		$this->load->model('modules/Cost_Estimate_Item_Logs_Model', 'CEIL');
		
		$result['ce'] = $this->module->record( $this, $id);  
		$result['ce_items'] = $this->Data->get_records( $this->CEIL,array( 'where_params' =>array( $this->tblid => $id)));   
		$report = $this->CE->report_data_individual($result);
		
		return $report;
	}
	 
}
