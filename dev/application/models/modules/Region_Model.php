<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Region_Model extends CI_Model {
	
	public $page = 'ModRegion';
	public $view = 'region';
	public $fpage = '';
	public $tblname = 'region';
	public $tblpref = 'reg_';
	public $tblid = 'reg_id';
	public $urc_name = 'region';
	public $name = 'Region';
	public $description = 'Region';
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		) 
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){ 
		
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */
		$this->load->model('modules/Area_Model','Area');
		$this->load->model('modules/User_Region_Model','UReg');
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
		 
	}
	
	public function set_search_fields(){ 
		 
		$data = array( 
			$this->tblpref.'title', 
			$this->tblpref.'name',
			$this->tblpref.'description'
		);
		
		return $data;
	}  
	 
	/**Custom functions here--------------------------*/
	function get_select( $ModelUsedBy, $record_fields ){
		$this->load->model('modules/User_Region_Model','UReg');
		
		$logged_user_id = $this->logged->get_login_session('user_id');		
		$user_regions = $this->UReg->get_checked_items( $logged_user_id );
		
		$user_regions_array = array();
		if( $user_regions ){
			foreach($user_regions as $col){
				$user_regions_array[] = app_get_val($col,$this->tblid);
			}
		} 
		$id = get_value($record_fields, $this->tblid); 
		$select = $this->records->select($this, $ModelUsedBy, $id, array(
			'query_params'=>array('orderby'=>$this->tblpref.'title','ordertype'=>'ASC',
				'where_in_key'=>$this->tblid,
				'where_in_value'=>$user_regions_array
 			),
			'fields'=>array('title')
		));  
		 
		return $select;
	}

	function get_items(){      
		$rows = $this->Data->get_records($this);
		 
		return $rows;
	}  
	
	function get_checkbox($user_id=''){     
		$record = $this->get_items();
		$input = '';
		$checked_items = array();
		if($user_id){
			$checked_items = $this->UReg->get_checked_items($user_id); 
		}
		if($record){
			$input = '<ul class="region-list">';
			foreach($record as $item){
				$id = get_value($item, $this->tblid);
				$title = get_value($item, $this->tblpref.'title');
				/* $input = '<input type="checkbox" name="region[]" value="'.$id.'"';  */
				$input .=  '<li><input id="check_box_parent_'.$id.'" type="checkbox" name="region[]" class="select_area_parent" value="'.$id.'"'; 
				if($checked_items){
					foreach($checked_items as $checked){ 
						$checked_id = get_value($checked, $this->tblid);
						if($checked_id == $id){
							$input .=  'checked';
						} 
					}
				}
				$input .=  '>&nbsp;&nbsp;'.$title;
				$input = $this->Area->get_checkbox($id, $input, $user_id); 
				$input .= '</li>';
			}
			$input .= '</ul>';
			echo $input;
		} 
	}  
 
}
