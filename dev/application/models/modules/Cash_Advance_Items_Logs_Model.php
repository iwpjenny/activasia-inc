<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cash_Advance_Items_Logs_Model extends CI_Model {
	
	public $page = 'ModCashAdvanceItems';
	public $view = 'cash-advance-items';
	public $fpage = '';
	public $tblname = 'cash_advance_items_logs';
	public $tblpref = 'cai_';
	public $tblid = 'cai_logs_id';
	public $urc_name = 'cash_advance_items_logs';
	public $name = 'Cash Advance Items Logs';
	public $description = 'Cash Advance Items Logs';
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'), 
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'trash','name'=>'Trash','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'export','name'=>'Export Excel','value'=>'yes'),
			array('field'=>'import','name'=>'Import Excel','value'=>'yes'),
			
		)
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){ 
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */ 
		$this->load->model('modules/Cash_Advance_Model', 'CA');
		$this->load->model('modules/Working_Budget_Items_Model', 'WBI');
		$this->load->model('modules/Working_Budget_Model', 'WB');
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		$this->load->model('modules/Region_Model', 'Reg');
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){ 
		
		$data = array( 
			array($this->CA->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->CA->tblid.'='.$this->CA->tblname.'.'.$this->CA->tblid,'LEFT'),
			// array($this->WBI->tblname,$this->db->dbprefix.$this->WBI->tblname.'.'.$this->WBI->tblid.'='.$this->WBI->tblname.'.'.$this->WBI->tblid,'LEFT')
		);
		
		return $data;		 
	}
	
	public function set_search_fields(){ 
		 
		$data = array(
			 
		);
		
		return $data;
	} 
	 
	function get_select( $ModelUsedBy, $record_fields ){
		
		$id = get_value($record_fields, $this->tblid);
		
		$select = $this->records->select($this, $ModelUsedBy, $id, array(
			'query_params'=>array('orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));
		
		return $select;
	} 

	public function create_logs_items( $params=array() ){
		$result = FALSE;
		$params = $params ? $params: $this->params;
		extract($params);
 	 	$data = array();
		$logged_user_id = $this->logged->get_login_session('user_id');
		$table_name = $this->CAI->tblname.'_logs';
		  
		$table_exists = $this->db->table_exists( $table_name );
		
		$result = FALSE;
		if( $table_exists ){
			if( isset($id) == FALSE ){
				$post_id = $this->input->post('id');
				$get_id = $this->input->get('id');
				$id = $post_id ? $post_id : $get_id;
			}
			
			$main_data_array = (array) $this->Data->get_records( $this->CAI, array('where_params'=>array($this->CA->tblid=>$id)) );
			
			
			if( $main_data_array ){
				$log_data = array(
					$this->CA->tblpref.'logs_id'=>$parent_id,
					$this->CAI->tblpref.'logs_user_id'=>$logged_user_id,
					$this->CAI->tblpref.'logs_type'=>$type,
					$this->CAI->tblpref.'logs_created'=>app_cdt()
				);			
			
				foreach ($main_data_array as $main_data ) { 
					$main_array = json_decode(json_encode($main_data), True);
					$data[] = array_merge($main_array, $log_data);  
					
				} 
			}
			  
			if($data){ 
				$this->db->insert_batch($table_name, $data); 
				$result = $this->db->insert_id();
			}
		}
			
		return $result;
	}
 
}