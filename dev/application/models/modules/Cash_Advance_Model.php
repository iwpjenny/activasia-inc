<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cash_Advance_Model extends CI_Model {
	
	public $page = 'ModCashAdvance';
	public $view = 'cash-advance';
	public $fpage = '';
	public $tblname = 'cash_advance';
	public $tblpref = 'car_';
	public $tblid = 'car_id';
	public $urc_name = 'cash_advance';
	public $name = 'Cash Advance';
	public $description = 'Cash Advance';
	public $display_section = array('dashboard'=>array('order'=>9));
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'trash','name'=>'Trash','value'=>'yes'), 
			// array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'export','name'=>'Export Excel','value'=>'yes'),
			array('field'=>'import','name'=>'Import Excel','value'=>'yes'),
			
		),
		array( 
			array('field'=>'approve','name'=>'Approve','value'=>'yes'),
			array('field'=>'decline','name'=>'Decline','value'=>'yes'),
			array('field'=>'release','name'=>'Release','value'=>'yes'),
			array('field'=>'pending','name'=>'Pending','value'=>'yes'),
			array('field'=>'ready','name'=>'Ready for Approval','value'=>'yes'),
		),
		array(  
			array('field'=>'notify','name'=>'Notify','value'=>'yes'),
		)
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){ 
		
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */
		$this->load->model('modules/Area_Model', 'Area');
		$this->load->model('modules/Working_Budget_Model', 'WB');
		$this->load->model('modules/Working_Budget_Items_Model', 'WBI');
		$this->load->model('modules/Region_Model', 'Reg');
		$this->load->model('modules/Cash_Advance_Status_Model', 'CAS');
		$this->load->model('modules/Cash_Advance_Items_Model', 'CAI');
		$this->load->model('modules/Cash_Advance_Items_Logs_Model', 'CAIL');

		$this->load->model('modules/Cost_Estimate_Model', 'CE');  
		$this->load->model('modules/Cost_Estimate_Items_Model', 'CEI');  
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){ 
		/*---------------------------------------------*/
		
		$data = array(
			array($this->WB->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->WB->tblid.'='.$this->WB->tblname.'.'.$this->WB->tblid,'LEFT'), 
			// array($this->Area->tblname,$this->db->dbprefix.$this->WB->tblname.'.'.$this->Area->tblid.'='.$this->Area->tblname.'.'.$this->Area->tblid,'LEFT'),
			array($this->CE->tblname,$this->db->dbprefix.$this->WB->tblname.'.'.$this->CE->tblid.'='.$this->CE->tblname.'.'.$this->CE->tblid,'LEFT'),
			array($this->Reg->tblname,$this->db->dbprefix.$this->CE->tblname.'.'.$this->Reg->tblid.'='.$this->Reg->tblname.'.'.$this->Reg->tblid,'LEFT')
			 
		);
		
		return $data;
		 
	}
	
	public function set_search_fields(){ 
		 
		$data = array( 
			$this->tblpref.'ca_number', 
			$this->tblpref.'date_released',
			$this->tblpref.'ca_amount',
			$this->tblpref.'status',
			$this->tblpref.'payment_request',
			$this->tblpref.'payroll_request',
			$this->tblpref.'remarks'
		);
		
		return $data;
	} 
	 
	function get_select( $ModelUsedBy, $record_fields ){
		
		$id = get_value($record_fields, $this->tblid);
		
		$select = $this->records->select($this, $ModelUsedBy, $id, array(
			'query_params'=>array('orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));
		
		return $select;
	}
	
	public function get_current_ca_balance( $wb_id, $ca_limit, $ca_id='' ){  
		$this->load->model('modules/Cash_Advance_Items_Model', 'CAI');
		$balance_ca_amount = 0;
		$remained_balance = 0;
		$ca_balance = 0; 
		
		if( $ca_id ){ 
			$record = $this->Data->get_record( $this, array(
				'field'=>'SUM('.$this->tblpref.'ca_amount) as sum_ca',
				'where_params' => array( $this->WB->tblid => $wb_id, $this->tblpref.'published' => 1, $this->tblpref.'trashed' => 0, $this->tblid.' <>' => $ca_id  )
			));   
			$balance_ca_amount = get_value($record, 'sum_ca');  
		} else {
			
			$ca_details = $this->Data->get_records( $this->CA, array( 'where_params' => array( $this->WB->tblid => $wb_id, $this->tblpref.'published' => 1, $this->tblpref.'trashed' => 0 ) ) );
			$ca_id_array = array();
			if( $ca_details ){
				foreach($ca_details as $col){
					$ca_id_array[] = app_get_val($col,$this->tblid);
				}
			}   
			$balance_ca_amount = $this->CAI->get_total_amount($ca_id_array);
		}
		if( $balance_ca_amount ){ 
			$ca_balance = $ca_limit - $balance_ca_amount;
			
		} else {
			$ca_balance = $ca_limit;
		} 
		return $ca_balance;
	}

	 
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/ 
	public function get_report( $quarter='', $year='' ){
		if( $year ){
			$where_params = array(	
				'YEAR('.$this->tblpref.'created)' => $year,
				$this->tblpref.'published' => 1, 
				$this->tblpref.'trashed' => 0  
			);
		} else {
			$where_params = array ( 
				$this->tblpref.'published' => 1, 
				$this->tblpref.'trashed' => 0 
			);
		}
		
		$where_in_key = ' MONTH('.$this->tblpref.'created)';
		if( $quarter == 'q1' ){
			$where_in_value = array('01','02','03');
		} elseif( $quarter == 'q2' ){
			$where_in_value = array('04','05','06'); 
		} elseif( $quarter == 'q3' ){
			$where_in_value = array('07','08','09'); 
		} elseif( $quarter == 'q4' ){
			$where_in_value = array('10','11','12'); 
		} else {
			$where_in_key = '';
			$where_in_value = '';
		} 
		
		$join_params = $this->set_join_params();
		$result = $this->Data->get_records( $this,
			array( 
				'where_params' => $where_params,
				'join_params' => $join_params,
				'where_in_key'=> $where_in_key, 
				'where_in_value'=> $where_in_value				
			));   		

		$report = $this->report_data($result); 
		return $report;
	}
	
	public function report_data($record){
		$result = array(); 
		
		if($record){
			foreach($record as $item){ 
				$id = get_value( $item, $this->tblid);
				$ca_no = get_value( $item, $this->tblpref.'ca_number');  
				$created_by_user_id = get_value( $item, $this->tblpref.'created_by_user_id'); 
				$name_of_employee = $this->moduser->get_user_fullname_by_id( $created_by_user_id, '{lastname}, {firstname}' );; 
				//user details_ca
				 $user_details = $this->Data->get_record( $this->User, array(
						'where_params' => array( $this->User->tblid => $created_by_user_id ) 
				));  
			
				$bank_account = get_value( $user_details, $this->User->tblpref.'bank_account');  
				$ce_number = get_value( $item, $this->CE->tblpref.'number');  
				$project_name = get_value( $item, $this->CE->tblpref.'project_name');  
				$client_name = get_value( $item, $this->CE->tblpref.'client_name');  
				$project_cost = get_value( $item, $this->CE->tblpref.'project_cost');  
				//balance
				$wb_id = get_value( $item, $this->WB->tblid );
				$wb_number = get_value( $item, $this->WB->tblpref.'number' );
				$balance = get_value( $item, $this->WB->tblpref.'bcs');
				$balance = $this->CA->get_current_ca_balance( $wb_id, $balance) ;
				$date_requested = get_value( $item, $this->tblpref.'date_requested');   
				$status = get_value( $item, $this->tblpref.'status','Pending');
				if( $status == 'Released' ){
					$date_released = datetime($date_released,$grid_date_format); 
				} else {
					$date_released = 'Unreleased';
				}  
				$ca_amount = $this->CAI->get_total_amount($id) ; 
				$ca_limit = get_value($item,$this->WB->tblpref.'bcs'); 
				$result[] = array(
					'CA Number:' => $ca_no,
					'BCS Code:' => $wb_number,
					'CE Code:' => $ce_number,
					'Project Name:' => $project_name, 
					'Client:' => $client_name, 
					'Project Cost:' => $project_cost,  
					'CA Total' => $ca_amount, 
					'Status' => $status,
					'CA Limit' => $ca_limit
				); 
			}    
			
			return $result;
		}
	}
	
	public function get_report_individual($id){  
		
		$result['ca'] = $this->module->record( $this, $id);  
		$result['ca_items'] = $this->Data->get_records( $this->CAI,array( 'where_params' =>array( $this->tblid => $id)));   
		$report = $this->report_data_individual($result);
		
		return $report;
	}
	
	public function report_data_individual($result){ 
		$this->load->model('modules/Cash_Advance_Items_Model', 'CAI');
		if($result){
			$total = 0; 
			$ca_result = $result['ca'];
			$ca_items_result = $result['ca_items']; 	
			$id = get_value( $ca_result, $this->tblid); 
			$ca_number = get_value( $ca_result, $this->CA->tblpref.'ca_number');  
			$status = get_value( $ca_result, $this->CA->tblpref.'status');  
			$ce_number = get_value( $ca_result, $this->CE->tblpref.'number');  
			$project_name = get_value( $ca_result, $this->CE->tblpref.'project_name');  
			$date_needed = get_value( $ca_result, $this->CA->tblpref.'date_needed');  
			$ce_id = get_value( $ca_result, $this->CE->tblid);  
			$ca_amount = $this->CAI->get_total_amount($id) ; 
			$data_ca['head'] = array();
			$data_ca['items'] = array();
			$data_ca['head'] = array(
				'CA #' => $ca_number,
				'CE #' => $ce_number, 
				'Project Name' => $project_name, 
				'Date Needed' => $date_needed, 
				// 'total' =>$ca_amount,
				'Status' =>$status
			);  
			foreach($ca_items_result as $item){ 
				$id = get_value( $item, $this->tblid);  
				$wbi_id = get_value($item,$this->WBI->tblid);
				$item_details = $this->module->record( $this->WBI, $wbi_id );	 
				$code = get_value( $item_details, $this->WBI->tblpref.'code');   
				$name = get_value( $item_details, $this->WBI->tblpref.'name');     
				$unit_cost = get_value( $item_details, $this->WBI->tblpref.'unit_cost');     
				$uom = get_value( $item_details, $this->WBI->tblpref.'unit_measure');     
				$description = get_value( $item_details, $this->WBI->tblpref.'description');     
				$header_1 = get_value( $item, $this->CAI->tblpref.'header_1');      
				$header_2 = get_value( $item, $this->CAI->tblpref.'header_2');     
				$header_3 = get_value( $item, $this->CAI->tblpref.'header_3');     
				$header_4 = get_value( $item, $this->CAI->tblpref.'header_4');      
				$asf_amount = $this->CEI->get_asf_value($ce_id, $code);   
				$multiplier_product =  $header_1 * $header_2 * $header_3 * $header_4;
				$total = $this->CAI->get_individual_total($unit_cost, $multiplier_product); 
				$data_ca['items'][] = array( 
					'Code' => $code,
					'Particulars' => $name, 
					'Description' => $description, 
					'Unit Cost' => $unit_cost, 
					'Unit of Measure' => $uom,  
					'Header 1' => $header_1, 
					'Header 2' => $header_2, 
					'Header 3' => $header_3, 
					'Header 4' => $header_4, 
					'Total' => $total, 
				); 
			}    
			return $data_ca;
			
		}
	} 
	
	
	
	public function unpublished_ca_data( $ce_id ){ 
		$this->load->model('modules/Working_Budget_Model', 'WB');
		/*---------------------------------------------*/
		
		$wb_details = $this->Data->get_records( $this->WB, array( 'where_params' =>array( $this->CE->tblid => $ce_id ) ) );
		$unpublished_ca = 0;
		if( $wb_details ){ 
			foreach( $wb_details as $col_wb ){
				$wb_id = get_value( $col_wb, $this->WB->tblid );
				$unpublished_ca = $this->Data->update_data( $this->CA, array( $this->CA->tblpref.'published' => '0' ), array( $this->WB->tblid => $wb_id ) );  
			}  
			
			return $unpublished_ca;
		} 
	}
	
	public function published_ca_data( $ce_id ){ 
		$this->load->model('modules/Working_Budget_Model', 'WB');
		/*---------------------------------------------*/
		
		$wb_details = $this->Data->get_records( $this->WB, array( 'where_params' =>array( $this->CE->tblid => $ce_id ) ) );
		$published_ca = 0;
		if( $wb_details ){ 
			foreach( $wb_details as $col_wb ){
				$wb_id = get_value( $col_wb, $this->WB->tblid );
				$published_ca = $this->Data->update_data( $this->CA, array( $this->CA->tblpref.'published' => '1' ), array( $this->WB->tblid => $wb_id ) );  
			}  
			
			return $published_ca;
		}
	}

	function set_cash_advance_status( $id, $params = array() ){
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		/*---------------------------------------------*/
		
		if( $params ){
			$status = get_value( $params, $this->tblpref.'status' );
			if( $status == 'Approved'){ 
				$details_ca = $this->Data->get_record( $this, array('where_params'=>array( $this->tblid => $id )) );
		
				$ce_id = get_value($details_ca,$this->CE->tblid);
				$ca_amount = get_value($details_ca,$this->CE->tblpref.'ca_amount');
		
				$check = $this->check_if_sufficient_working_amount_balance( $ce_id, $ca_amount );
		
				if( $check ){
					$data = array(
						$this->tblpref.'status'=>'Approved',
						/* $this->tblpref.'date_released'=>current_datetime() */
					);
					$result = $this->Data->update_data( $this, $data, array( $this->tblid => $id ) );
				} else {
					return FALSE;
				}
				
			} else {
				$result = $this->Data->update_data( $this, $params, array( $this->tblid => $id ) );	
			}
			
		} else {
			return FALSE;
		} 
		
		return $result;
	}

	function get_remaining_working_amount( $ce_id ){
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		/*---------------------------------------------*/
		
		$details_dor = $this->Data->get_record( $this->CE, array('where_params' => array( $this->CE->tblid => $ce_id, $this->CE->tblpref.'published' => 1, $this->CE->tblpref.'trashed' => 0 ) ) );
		$working_amount = get_value($details_dor,$this->CE->tblpref.'working_amount');
	
		if( $working_amount ){
			$total_ca_amount = $this->get_total_ca_amount_by_ce_id( $ce_id );
			$working_amount = ($working_amount - $total_ca_amount);
		}
		
		return $working_amount;
	}
	
	function check_if_sufficient_working_amount_balance( $ce_id, $ca_amount ){
		
		$working_amount = $this->get_remaining_working_amount( $ce_id );
		
		$result = FALSE;
		if( $ca_amount <= $working_amount  ){
			$result = TRUE;
		}
		
		return $result;
	}
	
	function json_check_balance( $id, $wb_id, $ca_limit, $ce_number, $ca_amount, $user_id ){
		
		$result = TRUE;
		$result_array = array();
		$total_balance = 0;
		if( $id ){
			$balance_ca_amount = $this->get_current_ca_balance( $wb_id, $ca_limit );	
			$total_balance = $balance_ca_amount;
		} else {
			$balance_ca_amount = $this->get_current_ca_balance( $wb_id, $ca_limit, $id );	
			// $total_balance = $balance_ca_amount - $ca_amount;
			$total_balance = $balance_ca_amount;

		}

		if( $ca_amount > $total_balance ){
			$result = FALSE;
		}
 
		if( $total_balance < 0 ){
			$ce_number = isset( $ce_number )?$ce_number:'';
			// set_alerts( 'warning', 'Not enough budget on '.$ce_number );
		 	$result = FALSE;
		}
		if( $id ){
			$urc_total_ca_amount_limit = $this->get_pending_and_approve_ca_limit_total( $user_id, $id );	
			$total_ca_amount = $urc_total_ca_amount_limit;
			$urc_total_ca_amount_limit += $ca_amount;
		} else {
			$urc_total_ca_amount_limit = $this->get_pending_and_approve_ca_limit_total( $user_id );	
			$total_ca_amount = $urc_total_ca_amount_limit;
			$urc_total_ca_amount_limit += $ca_amount;
		} 
		$urc_ca_amount_limit = $this->get_urc_ca_limit( $user_id ); 
  
		if( ( $urc_total_ca_amount_limit > $urc_ca_amount_limit ) && ( $urc_ca_amount_limit != '0.00' ) ){
			//set_alerts('warning', 'User only allowed to input equal or above CA Limit:'.$urc_ca_amount_limit );
			$urc_ca_amount_limit = number_format( $urc_ca_amount_limit,'2','.',',');
			$total_ca_amount = number_format( $total_ca_amount,'2','.',',');
			$balance_ca_limit = $urc_ca_amount_limit - $urc_total_ca_amount_limit;
			$result = FALSE;
			// set_alerts('warning', 'User only allowed CA Limit not more than: PHP '.$urc_ca_amount_limit.' .Current total amount you CA is PHP '.$total_ca_amount);
			//redirect( site_url('c='.$this->WB->page) );
			// redirect_current_page();
		}
		return $result;
	}

	public function get_pending_and_approve_ca_limit_total( $user_id='', $id='' ){ 
		
		$join_params = $this->set_join_params();
		$ca_total = 0;
		
		if( $id ){
			$records = $this->Data->get_records( $this, 
				array(
				 	'where_params' => array ( 
				 		$this->tblpref.'published' => 1, 
				 		$this->tblpref.'trashed' => 0, 
				 		$this->tblpref.'created_by_user_id' => $user_id, 
				 		$this->tblid.' <>' => $id ),
			 		'join_params ' => $join_params
	 		));
		} else { 
			$records = $this->Data->get_records( $this, 
				array( 
					'where_params' => array ( 
						$this->tblpref.'published' => 1, 
						$this->tblpref.'trashed' => 0, 
						$this->tblpref.'created_by_user_id' => $user_id ),
					'join_params' => $join_params 
			));
		}
		
		if( $records ){
			foreach( $records as $col ){
				$status = get_value( $col, $this->tblpref.'status');
				if( $status == 'Approved' || $status == 'Pending' ){
					$ca_amount = get_value( $col, $this->tblpref.'ca_amount');
					$ca_total += $ca_amount;	
				}
			}
		}
		//printx( $ca_total );
		return $ca_total;
	} 

	function get_urc_ca_limit( $operator ){
		// $this->load->model('Data_Model', 'Data');
		// $this->load->model('User_Role_Capability_Model', 'URC'); 
		// /* ---------------------------------------------------------- */
		// 	if($operator){
		// 	$user_params = array( 'where_params' => array( $this->User->tblid => $operator ) );
		// 	$user_details = $this->Data->get_record( $this->User, $user_params );
		// 	$ur_id = get_value( $user_details, $this->UR->tblid );
			
		// 	$params = array( 'where_params' => array( $this->UR->tblid => $ur_id ) );
		// 	$urc_details = $this->Data->get_record( $this->UR, $params );
		// 	$ur_ca_amount_limit = get_value( $urc_details, $this->UR->tblpref.'cash_advance_limit');
			
		// } else {
		// 	$ur_ca_amount_limit = 0;
		// } 
		$ur_ca_amount_limit = 0; 
		return $ur_ca_amount_limit;
	}

	function get_data_section_ca_pending( $data ){
		
		$location = app_get_val($data,'location');
		 
		$data['grid_date_time_format'] = app_get_val($data['settings'],'grid_date_time_format'); 
		$data['character_limit'] = app_get_val($data['settings'],'grid_text_limit');
		$data['show_status']['show_status_approve_button'] = $this->usersrole->check( $this->CA->urc_name, 'approve');
		$data['show_status']['show_status_decline_button'] = $this->usersrole->check( $this->CA->urc_name, 'decline');
		$data['show_status']['show_status_pending_button'] = $this->usersrole->check( $this->CA->urc_name, 'pending');
		$data['show_status']['show_status_release_button'] = $this->usersrole->check( $this->CA->urc_name, 'release');
		
		$join_params = $this->set_join_params();
		$query_params = array( 
			'where_params' => array ( 
				$this->tblpref.'published' => 1, 
				$this->tblpref.'status' => 'Ready', 
				$this->tblpref.'trashed' => 0 ),
			'join_params' => $join_params 
		);   		  
		$data['records'] = $this->Data->get_records( $this, $query_params );
		
		$data['section_ca_pending'] = $this->load->view($location.'/modules/'.$this->view.'/section_pending', $data, TRUE);
		
		return $data;
	}
 
	function get_data_section_ca_release( $data ){
		
		$location = app_get_val($data,'location');
		 
		$data['grid_date_time_format'] = app_get_val($data['settings'],'grid_date_time_format'); 
		$data['character_limit'] = app_get_val($data['settings'],'grid_text_limit');
		$data['show_status']['show_status_approve_button'] = $this->usersrole->check( $this->CA->urc_name, 'approve');
		$data['show_status']['show_status_decline_button'] = $this->usersrole->check( $this->CA->urc_name, 'decline');
		$data['show_status']['show_status_pending_button'] = $this->usersrole->check( $this->CA->urc_name, 'pending');
		$data['show_status']['show_status_release_button'] = $this->usersrole->check( $this->CA->urc_name, 'release');
		
		$join_params = $this->set_join_params();
		$query_params = array( 
			'where_params' => array ( 
				$this->tblpref.'published' => 1, 
				$this->tblpref.'status' => 'Approved', 
				$this->tblpref.'trashed' => 0 ),
			'join_params' => $join_params 
		);   		
		$data['records'] = $this->Data->get_records( $this, $query_params );
		
		$data['section_ca_release'] = $this->load->view($location.'/modules/'.$this->view.'/section_release', $data, TRUE);
		
		return $data;
	}
	
	
	function get_data_section_records( $data ){
		
		$table_name = $this->db->dbprefix.$this->tblname;
		$table_exists = $this->db->table_exists( $table_name );
		 
		if( $table_exists ){		
			$location = app_get_val($data,'location');
			 
			$join_params = $this->set_join_params(); 
			$query_params_release = array(
				'limit'=>5,
				'join_params' => $join_params,
				'orderby' => $this->tblpref.'created',
				'where_params' => array ( 
					$this->tblpref.'published' => 1, 
					$this->tblpref.'status' => 'Approved', 
					$this->tblpref.'trashed' => 0 
			));
		
			$data['records'] = $this->Data->get_records( $this, $query_params_release ); 
			
			$data['section_ca_release'] = $this->load->view($location.'/modules/'.$this->view.'/section', $data, TRUE); 
			
		}
		
		return $data;
	}

	public function get_pending_count( ){ 
		 
		 $where_params = array( 
		  $this->tblpref.'status' => 'Pending',
		  $this->tblpref.'trashed' => 0,
		  $this->tblpref.'published' => 1
		 ); 
		 
		 $this->db->select('Count('.$this->tblid.') as count');
		 $this->db->from($this->tblname);
		 $this->db->where($where_params); 
		 $records = $this->db->get()->row();

		 $count = get_value( $records, 'count' );
		 
		  
		 return $count;
	   
	}


	public function get_release_count( ){ 
		 
		 $where_params = array( 
		  $this->tblpref.'status' => 'Approved',
		  $this->tblpref.'trashed' => 0,
		  $this->tblpref.'published' => 1
		 ); 
		 
		 $this->db->select('Count('.$this->tblid.') as count');
		 $this->db->from($this->tblname);
		 $this->db->where($where_params); 
		 $records = $this->db->get()->row();

		 $count = get_value( $records, 'count' );
		 
		  
		 return $count;
	   
	}

	public function get_total_working_budget( $id ){ 
 
		$join_params = $this->set_join_params();
		$where_params = array( $this->tblname.'.'.$this->tblid => $id ); 
		$amount = 0;
		$query_params = array(
			'join_params'=>$join_params, 
			'where_params'=> $where_params  
		);
		$records = $this->Data->get_record( $this, $query_params ); 
		  
		$wb_id = get_value( $records, $this->WB->tblid );
		$old_balance = get_value( $records, $this->WB->tblpref.'bcs');
		$balance = $this->get_current_ca_balance( $wb_id, $old_balance); 
		$ca_amount = $this->CAI->get_total_amount( $id ); 
		  
	 	$query_params2 = array( 
			'where_params'=> array( $this->WB->tblid => $wb_id, $this->tblpref.'trashed' => '0' )
		);
	 	$existing_records = $this->Data->get_records( $this, $query_params2 ); 
	 	if( $existing_records ){
	 		foreach ( $existing_records as $col ) {
	 			$ca_id = get_value( $col, $this->tblid );
	 			$other_ca_amount = $this->CAI->get_total_amount( $id ); 
	 			$amount += $other_ca_amount; 
	 		}
	 	}

	 	$new_amount = $ca_amount + $amount;
	 	 
 	 	if( $balance > $new_amount ){
 			$return = TRUE;
 	 	} else {
 	 		$return = FALSE;  
			$notify_params = array('action'=>'update','type'=>'error','text'=>'Sorry, the amount of this CA overlap the Working Budget!','log'=>TRUE);
			$this->notify->set( $notify_params );	
 	 	}  
		return $return; 
	} 

	function get_data_link( $ca_id, $ca_number){
 
		$url = site_url('c='.$this->page.'&m=view&id='.$ca_id); 
		$return = '<a href="'.$url.'" target="_blank">'.$ca_number.'</a>';

		echo $return;
	}
	
	function get_balance( $bcs, $wb_id ){
		$grand_total = 0;
		$ca_entries_params = array( 
	 	 	'where_params' => array( $this->WB->tblid => $wb_id,  $this->CA->tblpref.'trashed' => 0)
		);     
	 	$ca_entries = $this->Data->get_records( $this->CA, $ca_entries_params ); 
		foreach($ca_entries as $entry){
			$ca_id = get_value($entry,$this->CA->tblid);  
			$amount = $this->WB->get_total_amount( $wb_id, $ca_id ); 
			$grand_total += $amount;
		}
	
		$balance = $bcs - $grand_total; 
		return $balance;
	}
	
	function get_dashboard_section_view( $data ){
		$location = app_get_val($data,'location');
		$data['grid_date_time_format'] = app_get_val($data['settings'],'grid_date_time_format'); 
		$data['character_limit'] = app_get_val($data['settings'],'grid_text_limit');
		$data['show_status']['show_status_approve_button'] = $this->usersrole->check( $this->CA->urc_name, 'approve');
		$data['show_status']['show_status_decline_button'] = $this->usersrole->check( $this->CA->urc_name, 'decline');
		$data['show_status']['show_status_pending_button'] = $this->usersrole->check( $this->CA->urc_name, 'pending');
		$data['show_status']['show_status_release_button'] = $this->usersrole->check( $this->CA->urc_name, 'release');
		$data['show_status']['show_status_ready_button'] = $this->usersrole->check( $this->CA->urc_name, 'ready');
		$join_params = $this->set_join_params();
		$query_params_pending = array( 
			'where_params' => array ( 
				$this->tblpref.'published' => 1, 
				$this->tblpref.'status' => 'Ready', 
				$this->tblpref.'trashed' => 0 ),
			'join_params' => $join_params 
		);   		
		
		$query_params_release = array(
			'limit'=>5,
			'join_params' => $join_params,
			'orderby' => $this->tblpref.'created',
			'where_params' => array ( 
				$this->tblpref.'published' => 1, 
				$this->tblpref.'status' => 'Approved', 
				$this->tblpref.'trashed' => 0 
		));
		
		$data['records_release'] = $this->Data->get_records( $this, $query_params_release );   
		$data['records_pending'] = $this->Data->get_records( $this, $query_params_pending );
		$result = $this->load->view($location.'/modules/'.$this->view.'/section-dashboard', $data, TRUE);
		
		return $result;  
		
	} 
	
	function set_email_attachment( $id ){
		$this->load->library('Excel');
		$ca = $this->module->record( $this, $id);  
		$status = get_value($ca, $this->tblpref.'status');
		$status = strtolower($status); 
		$content = $this->get_report_individual($id); 
		$date_now = date('Y-m-d-H-i');
		$filename= $this->name.'-'.$date_now; 
		$file_path = 'uploads/emails/'.$this->view.'/'.$status.'/'.$filename.'.xls';   
		$columns = array(
			'A' => 15, 'B' => 20 
		); 
		if( $content ){
			$this->excel->export_report_individual_nodownload( $content, $filename, $file_path, $columns );
		} 
		
		return $filename;
	}
	
	function send_email_confirmation_request( $ca_id, $ca_log_id, $type='', $recepients='', $remarks='' ){ 
		/*---------------------------------------------*/ 
		$this->load->library('excel'); 
		$this->load->library('email');  
		$this->load->model('modules/Cash_Advance_Status_Model', 'CAS'); 
		$this->load->model('modules/Cash_Advance_Logs_Model', 'CAL');
		$this->load->model('modules/Cash_Advance_Items_Model', 'CAI');
		$this->load->model('modules/Cash_Advance_Items_Logs_Model', 'CAIL');
		/*---------------------------------------------*/
		 
		$attach = FALSE;
		$attach1 = FALSE;
		$date_now = date('Y-m-d-H-i');
		/* START of Updated Information */ 
		if (!is_dir('uploads/emails/'.$this->CA->view.'/')) {
			mkdir('./uploads/emails/'.$this->CA->view.'/', 0777, TRUE); 
		}    
		$filename='Cash Advance System -'.$date_now; 
		$ordertype = $this->input->get('sort'); 
		$result_main = $this->get_report_individual($ca_id ); 
		$file_path = 'uploads/emails/'.$this->CA->view.'/ready/'.$filename.'.xls'; 
		$columns = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 20,'L' => 20,
		);
		if( $result_main['head']['CA #'] ){ 
			$this->excel->export_report_individual_ca_nodownload( $result_main, $filename, $file_path, $columns );   
			$attach = TRUE;
		}   
		/* END of Updated Information */
	
		 
		$ce_detail = $this->module->record( $this->CA, $ca_id );  
		$project_name = get_value($ce_detail, $this->CE->tblpref.'project_name');
		$created_by_user_id = get_value($ce_detail, $this->CA->tblpref.'created_by_user_id');
		$ca_number = get_value($ce_detail, $this->CA->tblpref.'ca_number');
		$creator_detail = $this->User->get_by_id( $created_by_user_id );  		 
		$firstname = get_value($creator_detail, $this->User->tblpref.'firstname');
		$lastname = get_value($creator_detail, $this->User->tblpref.'lastname'); 
		$receiver_email = get_value($creator_detail, $this->User->tblpref.'email'); 
		$user_detail = $this->User->get_logged_user(); 
		$username  = get_value($user_detail, $this->User->tblpref.'username');
		$date_now = date("jS F, Y", strtotime($date_now));
		  
		$config = $this->email_config();
		$data_message = array(
			'type' => $type,
			'project_name' => $project_name, 
			'ca_number' => $ca_number,
			'project_name' => $project_name, 
			'remarks' => $remarks, 
		);  
		$data_content = array( 
			'config' => $config, 
			'file_path' => $file_path, 
		); 
		foreach($recepients as $recepient){   
			$data_message['firstname'] = get_value($recepient, $this->User->tblpref.'firstname');  
			$content = $this->get_message($data_message); 
			extract($content); 
			$data_content['email'] = get_value($recepient, $this->User->tblpref.'email');   
			$data_content['subject'] = $subject;  
			$data_content['message'] = $message;    
			$this->send_email_with_config($data_content);   
		}    
	} 
	
	function send_email_with_config($data_content){
		extract($data_content);
		$this->email->initialize($config);
		$this->email->clear(TRUE); 
		$this->email->from('accounts@iwebprovider.com'); 
		$this->email->to($email);  
		$this->email->cc('rubin@iwebprovider.com'); 
		$this->email->subject($subject);
		$this->email->message($message);
		$attach = TRUE;
		if( $attach ){
			$this->email->attach($file_path); 
		} 
		if (!$this->email->send()){
			show_error($this->email->print_debugger()); 
		} else {
			echo 'Your e-mail has been sent!';
		}  
	}
	
	function email_config(){
		$config['protocol']    = 'smtp';
		$config['smtp_host']    = 'ssl://smtp.gmail.com';
		$config['smtp_port']    = '465';
		$config['smtp_timeout'] = '7';
		$config['smtp_user']    = 'marktestingemail@gmail.com';
		$config['smtp_pass']    = 'iWPEC@16';
		$config['charset']    = 'utf-8';
		$config['newline']    = "\r\n";
		$config['mailtype'] = 'html'; // or html
		$config['validation'] = TRUE; // bool whether to validate email or not 
		
		return $config;
	}
	
	function send_email_confirmation( $ca_id, $ca_log_id, $type='', $remarks='' ){ 
		/*---------------------------------------------*/ 
		$this->load->library('excel'); 
		$this->load->library('email');  
		$this->load->model('modules/Cash_Advance_Status_Model', 'CAS'); 
		$this->load->model('modules/Cash_Advance_Logs_Model', 'CAL');
		$this->load->model('modules/Cash_Advance_Items_Model', 'CAI');
		$this->load->model('modules/Cash_Advance_Items_Logs_Model', 'CAIL');
		/*---------------------------------------------*/
		 
		$attach = FALSE;
		$attach1 = FALSE;
		$date_now = date('Y-m-d-H-i');
		/* START of Updated Information */ 
		if (!is_dir('uploads/emails/'.$this->CA->view.'/')) {
			mkdir('./uploads/emails/'.$this->CA->view.'/', 0777, TRUE); 
		}    
		$filename='Cash Advance System -'.$date_now; 
		$ordertype = $this->input->get('sort'); 
		$result_main = $this->get_report_individual($ca_id ); 
		$file_path = 'uploads/emails/'.$this->CA->view.'/'.$filename.'.xls'; 
		$columns = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 20,'L' => 20,
		);
		if( $result_main['head']['CA #'] ){ 
			// forattachment  
			// $this->excel->export_report_individual_nodownload( $result_main, $filename, $file_path, $columns ); 
			$this->excel->export_report_individual_ca_nodownload( $result_main, $filename, $file_path, $columns );  
			$attach = TRUE;
		}   
		/* END of Updated Information */
		
		/* START of Log Information */
		if (!is_dir('uploads/emails/'.$this->CAL->view.'/')) {
			mkdir('./uploads/emails/'.$this->CAL->view.'/', 0777, TRUE); 
		}   
		/* END of Log Information */ 
		$ce_detail = $this->module->record( $this->CA, $ca_id );  
		$project_name = get_value($ce_detail, $this->CE->tblpref.'project_name');
		$ca_number = get_value($ce_detail, $this->CA->tblpref.'ca_number');
		$created_by_user_id = get_value($ce_detail, $this->CA->tblpref.'created_by_user_id'); 
		$creator_detail = $this->User->get_by_id( $created_by_user_id );  		 
		$firstname = get_value($creator_detail, $this->User->tblpref.'firstname');
		$lastname = get_value($creator_detail, $this->User->tblpref.'lastname'); 
		$receiver_email = get_value($creator_detail, $this->User->tblpref.'email'); 
		$user_detail = $this->User->get_logged_user(); 
		$username  = get_value($user_detail, $this->User->tblpref.'username');
		$approver_email  = get_value($user_detail, $this->User->tblpref.'email');
		$approver_firstname  = get_value($user_detail, $this->User->tblpref.'firstname');
		
		$date_now = date("jS F, Y", strtotime($date_now)); 
		$config = $this->email_config();
		$data_message = array(
			'project_name' => $project_name, 
			'ca_number' => $ca_number, 
			'remarks' => $remarks, 
		); 
		$data_content = array( 
			'config' => $config, 
			'file_path' => $file_path, 
		);
		if($type == 'approve'){
			$recipients = array(
				'requester' => array(
					'email' => $receiver_email,
					'firstname' => $firstname
				),
				'approver' => array(
					'email' => $approver_email,
					'firstname' => $approver_firstname
				)
			);
			foreach($recipients as $key => $recipient){  
				$data_message['type'] = $type; 
				$data_message['firstname'] = $recipient['firstname'];  
				$data_message['receiver_type'] = $key;
				$content = $this->get_message($data_message);   
				extract($content); 
				$data_content['email'] = $recipient['email'];   
				$data_content['subject'] = $subject;  
				$data_content['message'] = $message;    
				$this->send_email_with_config($data_content);   
			}
			
			$managers = $this->UR->get_email_recepients_role('manager'); 
			foreach($managers as $manager){  
				$data_message['type'] = $type; 
				$data_message['firstname'] =  get_value($manager, $this->User->tblpref.'firstname');
				$data_message['receiver_type'] = 'approver';
				$content = $this->get_message($data_message);   
				extract($content); 
				$data_content['email'] =  get_value($manager, $this->User->tblpref.'email');  
				$data_content['subject'] = $subject;  
				$data_content['message'] = $message;    
				$this->send_email_with_config($data_content);    
			}  
			
		} 
		if($type == 'decline'){
			$recipients = array(
				'requester' => array(
					'email' => $receiver_email,
					'firstname' => $firstname
				), 
			);
			foreach($recipients as $key => $recipient){  
				$data_message['type'] = $type; 
				$data_message['firstname'] = $recipient['firstname'];  
				$data_message['receiver_type'] = $key;
				$content = $this->get_message($data_message);   
				extract($content); 
				$data_content['email'] = $recipient['email'];   
				$data_content['subject'] = $subject;  
				$data_content['message'] = $message;    
				$this->send_email_with_config($data_content);   
			}
			 
		}
		
		if($type == 'release'){
			$recipients = array(
				'requester' => array(
					'email' => $receiver_email,
					'firstname' => $firstname
				), 
			);
			$managers = $this->UR->get_email_recepients_role('manager');
		  
			foreach($recipients as $key => $recipient){  
				$data_message['type'] = $type; 
				$data_message['firstname'] = $recipient['firstname'];  
				$data_message['receiver_type'] = $key;
				$content = $this->get_message($data_message);   
				extract($content); 
				$data_content['email'] = $recipient['email'];   
				$data_content['subject'] = $subject;  
				$data_content['message'] = $message;    
				$this->send_email_with_config($data_content);  
			}
			
			foreach($managers as $manager){  
				$data_message['type'] = $type; 
				$data_message['firstname'] =  get_value($manager, $this->User->tblpref.'firstname');
				$data_message['receiver_type'] = 'manager';
				$content = $this->get_message($data_message);   
				extract($content); 
				$data_content['email'] =  get_value($manager, $this->User->tblpref.'email');  
				$data_content['subject'] = $subject;  
				$data_content['message'] = $message;    
				$this->send_email_with_config($data_content);    
			} 
		} 
	}  
	
	function get_message( $data_message ){
		extract($data_message);
		$message = '<html><body>';
		$message .= '<p>Dear '.$firstname.',</p>';  
		 
		if( $type == 'request'){
			$subject = 'CARF Approval CA#: '.$ca_number;   
			$message .= '<table border="0" cellpadding="0" cellspacing="0">';
			$message .= '<tr><td><p>You have a cash advance request for approval with the CA# <b>'.$ca_number.'</b>. Please see the attached file for more information.</p></td></tr></table>';		

			if( $remarks ){
				$message .='<br /><p>Remarks: '.$remarks.'</p>';;
			}
			
		} elseif( $type == 'approve'){
			$subject = 'CA Request Approved: '.$project_name; 
			$message .= '<table border="0" cellpadding="0" cellspacing="0">'; 
			if($receiver_type == 'requester'){
				$message .= '<tr><td><p>Your ';
			}
			else{
				$message .= '<tr><td><p>The ';
			}
			$message .='cash advance request for the project: <b>'.$project_name.'</b> has been approved. Please see the attached file for more information.</p></td></tr></table>';	
			
			if( $remarks ){
				$message .='<br /><p>Remarks: '.$remarks.'</p>';;
			}
					
		} elseif( $type == 'decline'){
			$subject = 'CA Request Declined: '.$project_name;  
			$message .= '<table border="0" cellpadding="0" cellspacing="0">';
			$message .= '<tr><td><p>Your cash advance request for the project: <b>'.$project_name.'</b> has been declined. Please speak to your manager for further details.</p></td></tr></table>';

			if( $remarks ){
				$message .='<br /><p>Remarks: '.$remarks.'</p>';;
			}		
		} elseif( $type == 'release'){
			$subject = 'CA Request Released: '.$project_name; 
			$message .= '<table border="0" cellpadding="0" cellspacing="0">'; 
			if($receiver_type == 'requester'){
				$message .= '<tr><td><p>Your ';
			}
			else{
				$message .= '<tr><td><p>The ';
			}
			$message .='cash advance request for the project: <b>'.$project_name.'</b> has been released. Please see the attached file for more information.</p></td></tr></table>';

			if( $remarks ){
				$message .='<br /><p>Remarks: '.$remarks.'</p>';;
			}
			 
		} else {
			$subject = 'CA Overwrite CA#: '.$ca_number;
			$message .= '<table border="0" cellpadding="0" cellspacing="0">';
			$message .= '<tr><td><p>Please be notified that your CA with CA #: <b>'.$ca_number.'</b> was overwritten by <b>'.$username.'</b> on '.$date_now.'. Please see the attached file for more information.</p></td></tr></table>';
			
			if( $remarks ){
				$message .='<br /><p>Remarks: '.$remarks.'</p>';;
			}			
		}
		$message .= '</body></html>'; 
		
		$result['subject'] = $subject;
		$result['message'] = $message; 
		return $result;
	}
	
}
