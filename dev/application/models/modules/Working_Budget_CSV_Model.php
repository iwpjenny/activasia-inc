<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Working_Budget_CSV_Model extends CI_Model {

	public $page = 'ModWorkingBudgetCSV';
	public $view = 'working-budget-csv';
	public $tblname = 'import_csv';
	public $tblpref = 'imp_';
	public $tblid = 'imp_id';
	public $urc_name = 'ce_csv';
	public $name = 'Budget Control Sheet Uploads';
	public $description = 'Working Budget CSV Files';
	public $folder = 'working-budget-csv';
	public $logged_user_only = FALSE;  

	public $user_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes'),
			array('field'=>'upload','name'=>'Dpload','value'=>'yes')
		),
		array(
			array('field'=>'download','name'=>'Download','value'=>'yes')
		)
	);

    function __construct(){

        parent::__construct();
    }

	public function ini_custom_models(){  
		/* Initialize Custom Model here */ 
		$this->load->model('modules/Transactions_Model', 'Trans'); 
		$this->load->model('modules/Region_Model', 'Reg');
		$this->load->model('modules/Area_Model', 'Area');
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		$this->load->model('modules/Working_Budget_Model', 'WB');
		$this->load->model('modules/Working_Budget_Items_Model', 'WBI'); 
		/* End of initialize custom model here */	
	}

	public function set_join_params(){

		$data = array(
			array($this->User->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->User->tblid.'='.$this->db->dbprefix.$this->User->tblname.'.'.$this->User->tblid,'LEFT')
		);

		return $data;
	}

	public function set_search_fields(){

		$data = array(
			$this->tblpref.'filepath',
			$this->tblpref.'title',
			$this->User->tblpref.'firstname',
			$this->User->tblpref.'lastname'
		);

		return $data;
	}

	public function set_order_by(){

		$field = $this->tblpref.'created';
		
		return $field;
	}

	function get_select( $ModelUsedBy, $record_fields, $where_params=array() ){

		$id = get_value($record_fields, $this->tblid);
		$select = $this->fields->select($this, $ModelUsedBy, $id, array(			
			'query_params'=>array('where_params'=>$where_params,'orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));

		return $select;
	}
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
	 
	function read_csv( $csv_details ){  
		if($csv_details){  
			foreach($csv_details as $csv){
				$user_detail = $this->User->get_logged_user();
				$user_id = get_value($user_detail, $this->User->tblid);
				$imp_id = get_value($csv, 'id'); 
				$filepath = get_value($csv, 'filepath'); 
				$fp = fopen($filepath,'r') or die( "can't open file" ); 		
				$result = $this->save_wb($fp, $imp_id, $user_id); 
				$wb_id = $result['wb_id'];
				$ce_id = $result['ce_id'];
				// printx($wb_id);
				if($wb_id){
					$insert_wb_items = $this->save_wb_items($wb_id, $ce_id, $fp); 
					if($insert_wb_items){
						$notify_params = array('action'=>'update','type'=>'success');
						$this->notify->set( $notify_params );
						redirect( site_url('c='.$this->WB->page.'&m=view&id='.$wb_id) );
					}  
					else{
						redirect( site_url('c='.$this->WB->page.'&m=view&id='.$wb_id) );
					}
				} else {
					redirect( site_url('c='.$this->WB->page) );
				} 
			}
		}
		else{
			$notify_params = array('action'=>'add','type'=>'error','text'=>'No file to upload.');
			$this->notify->set( $notify_params );
			redirect( site_url('c='.$this->WB->page) );
		}

	}
	
	function save_wb( $file, $imp_id, $user_id ){  
		$this->load->library('Module'); 
		$loop = 1;  
		while( $csv_line = fgetcsv( $file,1024 ) ){   
			if($loop > 12){
				break;
			} 
			if($loop == 2 ) {
				$wb_number = isset($csv_line[1])?trim($csv_line[1]):'';  
			}elseif( $loop == 3){
				$ce_number = isset($csv_line[1])?trim($csv_line[1]):''; 
				$ce_detail = $this->CE->get_ce_details_by_number($ce_number);
				if($ce_detail){
					$ce_id = get_value($ce_detail, $this->CE->tblid);
				}
				else{
					$ce_id = '';
				} 
			} elseif($loop == 6 ) {
				$total_cost_estimate = isset($csv_line[1])?$csv_line[1]:'';  
			} elseif($loop == 7) {
				$asf = isset($csv_line[1])?$csv_line[1]:'';  
			} elseif($loop == 8) {
				$bcs = isset($csv_line[1])?$csv_line[1]:'';  
			} elseif($loop == 9 ) {
				$gross_profit = isset($csv_line[1])?$csv_line[1]:'';  
			} elseif($loop == 10) {
				$gp_percentage = isset($csv_line[1])?$csv_line[1]:'';  
			} elseif($loop == 11 ) {
				$savings_total = isset($csv_line[1])?$csv_line[1]:'';  
			}
			$loop++;
		} 
		$data = array( 
			$this->CE->tblid=>$ce_id, 
			$this->tblid=>$imp_id,  
			$this->WB->tblpref.'number'=>$wb_number,  
			$this->WB->tblpref.'asf'=>$asf, 
			$this->WB->tblpref.'bcs'=>$bcs, 
			$this->WB->tblpref.'gross_profit'=>$gross_profit,
			$this->WB->tblpref.'gp_percentage'=>$gp_percentage,
			$this->WB->tblpref.'savings_total'=>$savings_total,
			$this->WB->tblpref.'published'=>1,
			$this->WB->tblpref.'trashed'=>0
		);  
		$check = $this->check_wb($data);    
		if($check == TRUE){	
			$ce_detail = $this->CE->get_ce_details($ce_id);
			$ce_project_cost = get_value($ce_detail, $this->CE->tblpref.'project_cost');
			$data[$this->WB->tblpref.'cost_estimate'] = $ce_project_cost;
			$existing_wb = $this->module->record( $this->WB, array($this->WB->tblpref.'number' => $wb_number) );	  
			if($existing_wb){
				
				$id = get_value($existing_wb, $this->WB->tblid);  
				
				$params = array('model'=>$this->WB,'id'=>$id,'type'=>'Update WB Records');
				 
				//CREATE LOGS
				$this->load->library('ModuleLogs',$params);
				$parent_id = $this->modulelogs->create();     
				
				$params = array('model'=>$this->WBI,'id'=>$id,'parent_id'=>$parent_id,'type'=>'Update WB Items'); 
				if( $params ){
					$this->create_logs_items($params, $parent_id );  
				}
				$update = $this->Data->update_data( $this->WB, $data, array( $this->WB->tblid => $id ) );//email original uploader
				$this->send_email_confirmation( $id, $parent_id ); 
			 
			}
			else{  
				$data[$this->WB->tblpref.'created_by_user_id'] = $user_id; 
				$id = $this->Data->insert_data( $this->WB, $data );	 
			}  
			$return['wb_id'] = $id;
			$return['ce_id'] = $ce_id;
			return $return;
		}
	} 
	
	function save_wb_items($wb_id, $ce_id, $file){     
		$this->load->model('modules/Cost_Estimate_Items_Model', 'CEI');	
		if($file){  
			$codes_array = array();
			$result_count = 0;
			$x = 14;
			while( $csv_line = fgetcsv( $file,1024 ) ){   
				if($x >= 15){
					$code = isset($csv_line[0])?$csv_line[0]:'';  
					$name = isset($csv_line[1])?$csv_line[1]:'';  
					$description = isset($csv_line[2])?$csv_line[2]:''; 	
					$unit_cost = isset($csv_line[3])?$csv_line[3]:'';   
					$unit_measure = isset($csv_line[4])?$csv_line[4]:''; 	
					$header_1 = isset($csv_line[5])?$csv_line[5]:''; 	
					$header_2 = isset($csv_line[6])?$csv_line[6]:''; 	
					$header_3 = isset($csv_line[7])?$csv_line[7]:''; 	
					$header_4 = isset($csv_line[8])?$csv_line[8]:'';  	
					$multiplier_product = isset($csv_line[9])?$csv_line[9]:'';   
					$bcs_ewt = isset($csv_line[10])?$csv_line[10]:'';   
					$existing_code = $this->CEI->check_item_code($code, $ce_id); 
					if($existing_code){ 
							$data_wbi = array( 
								$this->WB->tblid=>$wb_id,  
								$this->WBI->tblpref.'code'=>$code, 
								$this->WBI->tblpref.'name'=>$name, 
								$this->WBI->tblpref.'description'=>$description,
								$this->WBI->tblpref.'unit_cost'=>$unit_cost,
								$this->WBI->tblpref.'unit_measure'=>$unit_measure,
								$this->WBI->tblpref.'header_1'=>$header_1,
								$this->WBI->tblpref.'header_2'=>$header_2,
								$this->WBI->tblpref.'header_3'=>$header_3,
								$this->WBI->tblpref.'header_4'=>$header_4,
								$this->WBI->tblpref.'multiplier_product'=>$multiplier_product,
								$this->WBI->tblpref.'bcs_ewt'=>$bcs_ewt,
								$this->WBI->tblpref.'published'=>1,
								$this->WBI->tblpref.'trashed'=>0
							);  
							
							$existing_wbi_id = $this->check_item_code_wbi($code, $wb_id);
							if($existing_wbi_id){
								$result = $this->Data->update_data( $this->WBI, $data_wbi, array( $this->WBI->tblid => $existing_wbi_id ) );
							}
							else{
								$result  = $this->Data->insert_data( $this->WBI, $data_wbi ); 	
							}   
							$result_count++;
							$codes_array[] = $code; 
					}
					else{
						$notify_params = array('action'=>'update','type'=>'error','text'=>'Item: '.$name.' not added. Item code: '.$code.' does not exist');
						$this->notify->set( $notify_params ); 
					}
					
				}
				$x++;
			}
			$codes_array = implode (", ", $codes_array); 
			if($codes_array){
				$this->delete_excess_items($codes_array,$wb_id);	
			}
			
			return $result_count;
		}
	}
	
	function check_wb( $data ){  
	
		if($data){   
			$x = 0; 
			$bcs = $data[$this->WB->tblpref.'bcs'];
			$num_bcs = floatval($bcs); 
			$ce_detail = $this->CE->get_ce_details($data[$this->CE->tblid]);
			$ce_project_cost = get_value($ce_detail, $this->CE->tblpref.'project_cost');  
			$ce_project_cost = floatval($ce_project_cost); 
			if( $data[$this->WB->tblpref.'number'] == '' ){
				$x++;
				$notify_params = array('action'=>'add','type'=>'error','text'=>'In Row #: 2, the BCS Code field must not be blank.'); 
			} 
			
			else{
				$code_length = strlen($data[$this->WB->tblpref.'number']);
				if($code_length < 10 ){
					$x++; 
					$notify_params = array('action'=>'add','type'=>'error','text'=>'In Row #: 2, the length of BCS Code must be greater than or equal to 10.'); 
				} 
			}  
			if($data[$this->CE->tblid] == ''){ 
				$x++;
				$notify_params = array('action'=>'add','type'=>'error','text'=>'In Row #: 3, CE code does not exist.'); 
			}
			
			else if($data[$this->WB->tblpref.'cost_estimate']=''){
				
				$x++;
				$notify_params = array('action'=>'add','type'=>'error','text'=>'In Row #: 6, Cost Estimate must not be blank.'); 
			}  
			
			else if($data[$this->WB->tblpref.'bcs'] =''){ 
				$x++;
				$notify_params = array('action'=>'add','type'=>'error','text'=>'In Row #: 6, Cost Estimate must not be blank.'); 
			}    
			else{ 
				if($num_bcs <= 0){
					$x++;
					$notify_params = array('action'=>'add','type'=>'error','text'=>'In Row #: 8, BCS (w/ EWT) must not be 0 or less than 0.'); 
				}
				else if($num_bcs > $ce_project_cost){
					$x++;
					$notify_params = array('action'=>'add','type'=>'error','text'=>'In Row #: 8, BCS (w/ EWT) must not be greater than CE Project Cost.'); 
				}
				 
			}
			if($x > 0){
				$this->notify->set( $notify_params ); 
				return false;
			}
			else{
				return true;
			}
		} 
	}
	function check_item_code_wbi( $code, $wb_id ){   
		$existing_code = $this->module->record( $this->WBI, array($this->WBI->tblname.'.'.$this->WBI->tblpref.'code' => $code, $this->WB->tblname.'.'.$this->WB->tblid => $wb_id ) );	
		$item_id = get_value($existing_code, $this->WBI->tblid); 
		return $item_id;
		
	}
	public function delete_excess_items($items,$wb_id){
		$table = $this->db->dbprefix($this->WBI->tblname);
		$id_name = $this->WB->tblid;
		$item_code_name = $this->WBI->tblpref.'code';
		$query = $this->db->query("
			DELETE 
			FROM $table
			WHERE $id_name = $wb_id
			AND $item_code_name NOT IN ($items)
		"); 
		 
	} 
	public function create_logs_items( $params=array(), $parent_id  ){
		 
		$params = $params ? $params: $this->params;
		extract($params);
 	 	$data = array();
		$logged_user_id = $this->logged->get_login_session('user_id');
		$table_name = $this->WBI->tblname.'_logs';
		 
		
		$table_exists = $this->db->table_exists( $table_name );
		
		$result = FALSE;
		if( $table_exists ){
			if( isset($id) == FALSE ){
				$post_id = $this->input->post('id');
				$get_id = $this->input->get('id');
				$id = $post_id ? $post_id : $get_id;
			}
			
			$main_data_array = (array) $this->Data->get_records( $this->WBI, array('where_params'=>array($this->WB->tblid=>$id)) );
			
		
			
			if( $main_data_array && $parent_id ){
				$log_data = array(
					$this->WB->tblpref.'logs_id'=>$parent_id,
					$this->WBI->tblpref.'logs_user_id'=>$logged_user_id,
					$this->WBI->tblpref.'logs_type'=>$type,
					$this->WBI->tblpref.'logs_created'=>app_cdt()
				);			
			
				foreach ($main_data_array as $main_data ) {
					 
					$main_array = json_decode(json_encode($main_data), True);
					$data[] = array_merge($main_array, $log_data); 
					
					
				}
				
				$this->db->insert_batch($table_name, $data); 
				$result = $this->db->insert_id();
			}
			 
			
			
		}
		
			
		return $result;
	}
	
	public function send_email_confirmation( $bcs_id = 1, $bcs_log_id = 1){ 
		/*---------------------------------------------*/ 
		$this->load->library('email');  
		$this->load->library('Excel');
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		$this->load->model('modules/Working_Budget_Model', 'WB'); 
		$this->load->model('modules/Working_Budget_Logs_Model', 'WBL'); 
		$this->load->model('modules/Working_Budget_Items_Model', 'WBI');
		$this->load->model('modules/Working_Budget_Items_Logs_Model', 'WBIL'); 
		/*---------------------------------------------*/
		
		$date_now = date('Y-m-d-H-i');
		$attach = FALSE;
		$attach1 = FALSE;
		/* START of Updated Information */
		if (!is_dir('uploads/emails/'.$this->WB->view.'/')) {
			mkdir('./uploads/emails/'.$this->WB->view.'/', 0777, TRUE); 
		}   
		
		$filename=$this->name.'-'.$date_now;  
		$result_main = $this->WB->get_report_individual($bcs_id ); 
		$file_path = 'uploads/emails/'.$this->WB->view.'/'.$filename.'.xls'; 
		$columns = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 20,'L' => 20,
		);
		 
		if( $result_main['head']['BCS Code'] ){ 
			//forattachment 
			$this->excel->export_report_individual_nodownload( $result_main, $filename, $file_path, $columns );
			$attach = TRUE;			
		}   
		/* END of Updated Information */
		
		/* START of Log Information */
		if (!is_dir('uploads/emails/'.$this->WBL->view.'/')) {
			mkdir('./uploads/emails/'.$this->WBL->view.'/', 0777, TRUE);
		}   
		$filename_logs ='Logs For Budget Control Sheet -'.$date_now; 
		$file_path_logs = 'uploads/emails/'.$this->WBL->view.'/'.$filename_logs.'.xls'; 
		$result_logs = $this->WBL->get_report_individual( $bcs_log_id ); 
		$columns_logs = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 30,'L' => 30,'M' => 30
		); 
		if( $result_logs['head']['BCS Code'] ){ 
			//forattachment 
			$this->excel->export_report_individual_nodownload( $result_logs, $filename_logs, $file_path_logs, $columns_logs );
			$attach1 = TRUE;
		}  
		/* END of Log Information */
		 
		$ce_detail = $this->module->record( $this->WB, $bcs_id);  
		$project_name = get_value($ce_detail, $this->CE->tblpref.'project_name');
		$created_by_user_id = get_value($ce_detail, $this->WB->tblpref.'created_by_user_id');
		$modified = get_value($ce_detail, $this->WB->tblpref.'modified');
		$creator_detail = $this->User->get_by_id( $created_by_user_id );  		 		
		$firstname = get_value($creator_detail, $this->User->tblpref.'firstname');
		$lastname = get_value($creator_detail, $this->User->tblpref.'lastname'); 
		$receiver_email = get_value($creator_detail, $this->User->tblpref.'email'); 
		$user_detail = $this->User->get_logged_user(); 
		$username = get_value($user_detail, $this->User->tblpref.'username');
		$date_now = date("jS F, Y", strtotime($date_now));
		
		// $htmlContent = $this->load->view('backend/modules/cost-estimate/email-template',$data, TRUE ); 
		// $htmlContent = $this->load->view('backend/modules/'.$this->WB->view.'/email-template',$data, TRUE );  
		
		$message = '<p>Dear '.$firstname.',</p>'; 
		$message .= '<table border="0" cellpadding="0" cellspacing="0">';
		$message .= '<tr><td><p>Please be notified that your BCS for the project <b>'.$project_name.'</b> was overwritten by <b>'.$username.'</b> on '.$modified.'. Please see your original BCS attached for your reference.</p></td></tr></table>';
		 
		$subject = 'WB Overwrite: '.$project_name;  
		$this->email->clear();
		$config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'marktestingemail@gmail.com';
        $config['smtp_pass']    = 'iWPEC@16';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);
		  
		$this->email->from('rubin@iwebprovider.com'); 
		$this->email->to($receiver_email);  
		$this->email->subject($subject);
		$this->email->message($message);
		
		/* if($attach){
			$this->email->attach($file_path);
		} */
		if($attach1){
			$this->email->attach($file_path_logs);
		}  
		if (!$this->email->send()) {
			show_error($this->email->print_debugger()); }
		else {
			echo 'Your e-mail has been sent!';
		} 
	}
}
