<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Position_Approved_Model extends CI_Model {
	
	public $page = 'ModPositionApproved';
	public $view = 'position-approved';
	public $fpage = '';
	public $tblname = 'position_approved';
	public $tblpref = 'pa_';
	public $tblid = 'pa_id';
	public $urc_name = 'position_approved';
	public $name = 'Position Approved';
	public $description = 'Position Approved';
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		) 
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){ 
		
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */

		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
		 
	}
	
	public function set_search_fields(){ 
		 
		$data = array( 
			$this->tblpref.'title', 
			$this->tblpref.'name',
			$this->tblpref.'description'
		);
		
		return $data;
	} 
	 
	function get_select( $ModelUsedBy, $record_fields ){
		
		$id = get_value($record_fields, $this->tblid);
		
		$select = $this->records->select($this, $ModelUsedBy, $id, array(
			'query_params'=>array('orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));
		
		return $select;
	}
	 
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
	
}
