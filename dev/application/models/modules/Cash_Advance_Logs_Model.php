<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cash_Advance_Logs_Model extends CI_Model {
	
	public $page = 'ModCashAdvanceLogs';
	public $view = 'cash-advance-logs';
	public $fpage = '';
	public $tblname = 'cash_advance_logs';
	public $tblpref = 'car_logs_';
	public $tblid = 'car_logs_id';
	public $urc_name = 'cash_advance_logs';
	public $name = 'Cash Advance Logs';
	public $description = 'Cash Advance Logs';
	public $display_section = array('dashboard'=>array('order'=>10));
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		) 
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array(
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->helper('functions');
		$this->load->database();
		
        parent::__construct();
    }
	
	public function ini_custom_models(){ 
		/* Initialize Custom Model here */ 
		$this->load->model('modules/Area_Model', 'Area');
		$this->load->model('modules/Working_Budget_Model', 'WB');
		$this->load->model('modules/Region_Model', 'Reg');
		$this->load->model('modules/Cash_Advance_Status_Model', 'CAS');
		$this->load->model('modules/Cash_Advance_Model', 'CA');
		$this->load->model('modules/Cash_Advance_Items_Model', 'CAI');
		$this->load->model('modules/Cash_Advance_Items_Logs_Model', 'CAIL');
		$this->load->model('modules/Cost_Estimate_Items_Model', 'CEI'); 
		$this->load->model('modules/Working_Budget_Items_Model', 'WBI'); 
		/* End of initialize custom model here */	
	}

	public function set_join_params(){
		$this->load->model('modules/Working_Budget_Model', 'WB');
		$this->load->model('modules/Area_Model', 'Area');
		$this->load->model('modules/Cost_Estimate_Model', 'CE'); 
		$this->load->model('modules/Region_Model', 'Reg');
		/*---------------------------------------------*/
		
		$data = array(
			array($this->WB->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->WB->tblid.'='.$this->WB->tblname.'.'.$this->WB->tblid,'LEFT'), 
		 	array($this->CE->tblname,$this->db->dbprefix.$this->WB->tblname.'.'.$this->CE->tblid.'='.$this->CE->tblname.'.'.$this->CE->tblid,'LEFT'),
			array($this->Reg->tblname,$this->db->dbprefix.$this->CE->tblname.'.'.$this->Reg->tblid.'='.$this->Reg->tblname.'.'.$this->Reg->tblid,'LEFT')
			 
		);
		
		return $data;
	}

	public function set_search_fields(){
		$this->load->model('Member_Model', 'Mem');

		$data = array( 
			$this->tblpref.'title', 
		);

		return $data;
	}

	function get_select( $ModelUsedBy, $record_fields, $where_params=array() ){

		$id = get_value($record_fields, $this->tblid);
		$select = $this->records->select($this, $ModelUsedBy, $id, array(			
			'query_params'=>array('where_params'=>$where_params,'orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));

		return $select;
	}
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
	 function get_data_section_records( $data ){
		
		$table_name = $this->db->dbprefix.$this->CA->tblname;
		$table_exists = $this->db->table_exists( $table_name );
		 
		if( $table_exists ){		
			$location = app_get_val($data,'location');
			 
			$join_params = $this->CA->set_join_params(); 
			$query_params_pending = array(
				'limit'=>5,
				'join_params' => $join_params,
				'orderby' => $this->tblpref.'created',
				'where_params' => array ( 
					$this->CA->tblpref.'published' => 1, 
					$this->CA->tblpref.'status' => 'Pending', 
					$this->CA->tblpref.'trashed' => 0 
			));
			
			$data['records'] = $this->Data->get_records( $this->CA, $query_params_pending ); 
			
			$data['section_ca_pending'] = $this->load->view($location.'/modules/'.$this->view.'/section', $data, TRUE); 
		}
		
		return $data;
	}
	
	public function get_report_individual($id){  
		
		$result['ca'] = $this->module->record( $this, $id);  
		$result['ca_items'] = $this->Data->get_records( $this->CAIL,array( 'where_params' =>array( $this->tblid => $id)));   
		$report = $this->CA->report_data_individual($result);
		
		return $report;
	}
}
