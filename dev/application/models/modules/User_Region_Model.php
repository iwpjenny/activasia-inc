<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Region_Model extends CI_Model {
	
	public $page = 'ModRegion';
	public $view = 'region';
	public $fpage = '';
	public $tblname = 'user_region';
	public $tblpref = 'ureg_';
	public $tblid = 'ureg_id'; 
	public $name = 'Region';
	public $description = 'Region'; 
	
    function __construct(){
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->helper('functions');
		$this->load->database();
		
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */
		$this->load->model('User_Model','User');
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
		 
	}
	
	public function set_search_fields(){ 
		 
		$data = array(
			$this->tblname.'.'.$this->tblid, 
			$this->tblpref.'title', 
			$this->tblpref.'description',
			$this->tblpref.'latitude',
			$this->tblpref.'longitude',
			$this->tblpref.'created',
			$this->User->tblpref.'firstname', 
			$this->User->tblpref.'lastname' 
		);
		
		return $data;
	} 
	 
	/*---------------------------------------------*/ 
	/*Custom functions here------------------------*/
	function get_checked_items($id){
		
		$query_params = array( 
			'where_params' => array( $this->User->tblid => $id )
			);    
		$rows = $this->Data->get_records($this, $query_params);
		return $rows;
	} 
	
	function save_checked_items($regions, $user_id){   
		if($regions){ 
			$delete = $this->Data->delete_data( $this, array($this->User->tblid=>$user_id) ); 
			foreach($regions as $region){ 
				$id = get_value($region, $this->Reg->tblid);  
				$entry = array(
					$this->User->tblid => $user_id, 
					$this->Reg->tblid => $id 
				); 
				$this->records->save( $this, $entry);    
			}  
		}
	}
	
}
