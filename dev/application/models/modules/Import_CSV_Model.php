<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Import_CSV_Model extends CI_Model {
	
	public $page = 'ModImportCSV';
	public $view = 'import-csv';
	public $fpage = '';
	public $tblname = 'import_csv';
	public $tblpref = 'imp_';
	public $tblid = 'imp_id';
	public $urc_name = 'import_csv';	
	public $name = 'Import CSV';
	public $description = 'Import CSV';
	 
	public $user_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes'),
			array('field'=>'upload','name'=>'Dpload','value'=>'yes')
		),
		array(
			array('field'=>'download','name'=>'Download','value'=>'yes')
		)
	);
	
    function __construct(){
		
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		$this->load->model('modules/Working_Budget_Model', 'WB');
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
		 $data = array( 
		 
		);
		
		return $data;
	}
	
	public function set_search_fields(){ 
		 
		$data = array(
			$this->tblpref.'filepath',
			$this->tblpref.'title', 
			$this->tblpref.'description' 
		);
		
		return $data;
	} 

	function get_select( $ModelUsedBy, $record_fields, $where_params=array() ){

		$id = get_value($record_fields, $this->tblid);
		$select = $this->records->select($this, $ModelUsedBy, $id, array(			
			'query_params'=>array('where_params'=>$where_params,'orderby'=>$this->tblpref.'number','ordertype'=>'ASC'),
			'fields'=>array('number')
		));

		return $select;
	}
	
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
	
	function upload_csv(){		
		$this->load->model('modules/Region_Model', 'Reg');   
		/*---------------------------------------------*/	
		  
		$config['upload_path'] = './uploads/user';
        $config['allowed_types'] = 'png';
        $config['max_size'] = '1000';
		
		$this->load->library('file', $config);
        $this->load->library('upload', $config);
        
 
		$upload = $this->upload->do_upload(); 
		if($upload){
			printx($upload);
		}
		else{
			/* printx('hi'); */
			printx($this->upload->display_errors());
		}
		printx("heheh");
		
	}
	
}
