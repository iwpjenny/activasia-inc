<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cost_Estimate_CSV_Model extends CI_Model {

	public $page = 'ModCostEstimateCSV';
	public $view = 'cost-estimate-csv';
	public $tblname = 'import_csv';
	public $tblpref = 'imp_';
	public $tblid = 'imp_id';
	public $urc_name = 'ce_csv';
	public $name = 'Cost Estimate Uploads';
	public $description = 'Cost Estimate Uploads Files';
	public $folder = 'cost-estimate-csv';
	public $logged_user_only = FALSE;

	public $user_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes'),
			array('field'=>'upload','name'=>'Dpload','value'=>'yes')
		),
		array(
			array('field'=>'download','name'=>'Download','value'=>'yes')
		)
	);

    function __construct(){

        parent::__construct();
    }

	public function ini_custom_models(){
		$this->load->model('modules/Cost_Estimate_Model', 'CE');
		$this->load->model('modules/Region_Model', 'Reg'); 
		$this->load->model('modules/Transactions_Model', 'Trans'); 
	}

	public function set_join_params(){

		$data = array(
			array($this->User->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->User->tblid.'='.$this->db->dbprefix.$this->User->tblname.'.'.$this->User->tblid,'LEFT')
		);

		return $data;
	}

	public function set_search_fields(){

		$data = array(
			$this->tblpref.'filepath',
			$this->tblpref.'title',
			$this->User->tblpref.'firstname',
			$this->User->tblpref.'lastname'
		);

		return $data;
	}

	public function set_order_by(){

		$field = $this->tblpref.'created';
		
		return $field;
	}

	function get_select( $ModelUsedBy, $record_fields, $where_params=array() ){

		$id = get_value($record_fields, $this->tblid);
		$select = $this->fields->select($this, $ModelUsedBy, $id, array(			
			'query_params'=>array('where_params'=>$where_params,'orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));

		return $select;
	}  
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
	function read_csv( $csv_details ){ 
		$this->load->model('modules/Cost_Estimate_Items_Model', 'CEI');
		if($csv_details){ 
			foreach($csv_details as $csv){
				$user_detail = $this->User->get_logged_user();
				$user_id = get_value($user_detail, $this->User->tblid);
				$imp_id = get_value($csv, 'id'); 
				$filepath = get_value($csv, 'filepath'); 
				$fp = fopen($filepath,'r') or die( "can't open file" ); 		
				$ce_id = $this->save_ce($fp, $imp_id, $user_id);  
				if($ce_id){
					$insert_ce_items = $this->save_ce_items($ce_id, $fp); 
					if($insert_ce_items){
						$notify_params = array('action'=>'update','type'=>'success');
						$this->notify->set( $notify_params );
						redirect( site_url('c='.$this->CE->page.'&m=view&id='.$ce_id) );
					} 
					else{
						redirect( site_url('c='.$this->CE->page.'&m=view&id='.$ce_id) );
					}
				} else {
					redirect( site_url('c='.$this->CE->page) );
				}
			}
		}
		else{
			$notify_params = array('action'=>'add','type'=>'error','text'=>'No file to upload.');
			$this->notify->set( $notify_params );
			redirect( site_url('c='.$this->CE->page) );
		}

	}
	function save_ce( $file, $imp_id, $user_id ){  
		$this->load->library('Module'); 
		$project_name = ''; 
		$project_duration = ''; 
		$ce_code = ''; 
		$client = ''; 
		$address = ''; 
		$attention_to = ''; 
		$date_submitted = ''; 
		$project_cost = ''; 
		$total_asf = ''; 
		$total_ex_vat = ''; 
		$total_inc_vat = ''; 
		
		$x = 1;
		while(  $csv_line = fgetcsv( $file,1024) ){ 
			if($x > 15){
				break;
			} 
			$project_name = ($x == 2 ? trim($csv_line[1]) : $project_name); 
			$project_duration = ($x == 3 ? $csv_line[1] : $project_duration);  
			$ce_code = ($x == 4 ? trim($csv_line[1]) : $ce_code); 			
			$client = ($x == 5 ? trim($csv_line[1]) : $client); 
			$address = ($x == 6 ? $csv_line[1] : $address); 
			$attention_to = ($x == 7 ? $csv_line[1] : $attention_to); 
			$date_submitted = ($x == 8 ? $csv_line[1] : $date_submitted); 
			$project_cost = ($x == 11 ? $csv_line[1] : $project_cost); 
			$total_asf = ($x == 12 ? $csv_line[1] : $total_asf); 
			$total_ex_vat = ($x == 14 ? $csv_line[1] : $total_ex_vat); 
			$total_inc_vat = ($x == 15 ? $csv_line[1] : $total_inc_vat);   
			  
			$x++;
		}  
		if($date_submitted == ''){
			$date_submitted = date("Y-m-d");
		}
		$data = array( 
			$this->tblid=> $imp_id, 
			$this->CE->tblpref.'project_name'=>$project_name,
			$this->CE->tblpref.'duration'=>$project_duration,
			$this->CE->tblpref.'number'=>$ce_code, 
			$this->CE->tblpref.'client_name'=>$client, 
			$this->CE->tblpref.'client_address'=>$address,
			$this->CE->tblpref.'attention_to'=>$attention_to,
			$this->CE->tblpref.'date_submitted'=>$date_submitted,
			$this->CE->tblpref.'project_cost'=>$project_cost,
			$this->CE->tblpref.'total_asf'=>$total_asf,
			$this->CE->tblpref.'total_ex_vat'=>$total_ex_vat,
			$this->CE->tblpref.'total_inc_vat'=>$total_inc_vat, 
		);      
		$check = $this->check_ce($data);   
		if($check == TRUE){  
			
			$existing_ce = $this->module->record( $this->CE, array($this->CE->tblpref.'number' => $ce_code) );	  
			if($existing_ce){
				$id = get_value($existing_ce, $this->CE->tblid); 
				//update all ce data
				$params = array('model'=>$this->CE,'id'=>$id,'type'=>'Update CE Records'); 
				$this->load->library('ModuleLogs',$params);
				$parent_id = $this->modulelogs->create(); 
				
 				// delete all ce items
				$params = array('model'=>$this->CEI,'id'=>$id,'parent_id'=>$parent_id,'type'=>'Update CE Items'); 
				$this->create_logs_items($params, $parent_id);     
				$result = $this->Data->update_data( $this->CE, $data, array( $this->CE->tblid => $id ) );	
				$return = $id;
				$notify_params = array('action'=>'add','type'=>'success','text'=>'CE has been updated.');
				$this->notify->set( $notify_params ); 
				$this->send_email_confirmation( $id, $parent_id );
			}
			else{
				$data[$this->CE->tblpref.'created_by_user_id'] = $user_id; 
				$return = $this->Data->insert_data( $this->CE, $data );	
				$notify_params = array('action'=>'add','type'=>'success','text'=>'NEW CE has been added.');
				$this->notify->set( $notify_params );	
			} 
			return $return;
		}	 	
		else{
			redirect( site_url('c='.$this->CE->page) );
		}
	}
	function check_ce( $data ){
		// printx($data);
		if($data){
			$x = 0;
			//calculations for checking
			// $grand_total = $data[$this->CE->tblpref.'total'] + $data[$this->CE->tblpref.'plus_asf']; 
			//for CE code
			if( $data[$this->CE->tblpref.'number'] == '' ){
				$x++;
				$notify_params = array('action'=>'add','type'=>'error','text'=>'In Row #: 4, the CE Code field must not be blank.');
				
			} 
			else{
				$code_length = strlen($data[$this->CE->tblpref.'number']);
				if($code_length < 10){
					$x++;
					$notify_params = array('action'=>'add','type'=>'error','text'=>'In Row #: 4, the length of CE Code must be greater than or equal to 10.');
				}
			}  
			if( strlen($data[$this->CE->tblpref.'project_name']) <= 0 ){ 
				$x++;
				$notify_params = array('action'=>'add','type'=>'error','text'=>'In Row #: 2, the Project Name field must not be blank.'); 
			} 
			if( strlen($data[$this->CE->tblpref.'client_name']) <= 0 ){ 
				$x++;
				$notify_params = array('action'=>'add','type'=>'error','text'=>'In Row #: 5, the Client field must not be blank.'); 
			}
			if( strlen($data[$this->CE->tblpref.'project_cost']) <= 0 ){ 
				$x++;
				$notify_params = array('action'=>'add','type'=>'error','text'=>'In Row #: 11, the Project Cost must not be blank.'); 
			} 
			if( strlen($data[$this->CE->tblpref.'total_asf']) <= 0 ){ 
				$x++;
				$notify_params = array('action'=>'add','type'=>'error','text'=>'In Row #: 12, the ASF must not be blank.'); 
			}   
			if($x > 0){
				$this->notify->set( $notify_params ); 
				return false;
			}
			else{
				return true;
			}
		} 
	}
	function save_ce_items( $ce_id, $file ){ 
		if($file){ 
			$x = 17;
			$result = '';
			$codes_array = array();
			while(  $csv_line = fgetcsv( $file,1024) ){  			
				if($x > 18){
					// $no = isset($csv_line[0])?$csv_line[0]:'';
					$code = isset($csv_line[0])?$csv_line[0]:'';
					$name = isset($csv_line[1])?$csv_line[1]:'';
					$description = isset($csv_line[2])?$csv_line[2]:'';
					$unit_cost = isset($csv_line[3])?$csv_line[3]:'';
					$unit_measure = isset($csv_line[4])?$csv_line[4]:'';
					$header_1 = isset($csv_line[5])?$csv_line[5]:'';
					$header_2 = isset($csv_line[6])?$csv_line[6]:''; 
					$header_3 = isset($csv_line[7])?$csv_line[7]:'';
					$header_4 = isset($csv_line[8])?$csv_line[8]:'';
					$multiplier_product = isset($csv_line[9])?$csv_line[9]:'';
					$asf = isset($csv_line[10])?$csv_line[10]:'';
					$asf_amount = isset($csv_line[11])?$csv_line[11]:'';
					$total = isset($csv_line[12])?$csv_line[12]:'';
					
					
					
					$data = array(  
						$this->CE->tblid=>$ce_id,   
						$this->CEI->tblpref.'code'=>$code, 
						$this->CEI->tblpref.'name'=>$name, 
						$this->CEI->tblpref.'description'=>$description, 
						$this->CEI->tblpref.'unit_cost'=>$unit_cost, 
						$this->CEI->tblpref.'unit_measure'=>$unit_measure, 
						$this->CEI->tblpref.'header_1'=>$header_1, 
						$this->CEI->tblpref.'header_2'=>$header_2, 
						$this->CEI->tblpref.'header_3'=>$header_3, 
						$this->CEI->tblpref.'header_4'=>$header_4, 
						$this->CEI->tblpref.'multiplier_product'=>$multiplier_product, 
						$this->CEI->tblpref.'asf'=>$asf, 
						$this->CEI->tblpref.'asf_amount'=>$asf_amount, 
						$this->CEI->tblpref.'total'=>$total, 
					);     
					$existing_cei_id = $this->check_item_code($code, $ce_id);
					if($existing_cei_id){
						$result = $this->Data->update_data( $this->CEI, $data, array( $this->CEI->tblid => $existing_cei_id ) );
					}
					else{
						$result = $this->Data->insert_data( $this->CEI, $data );
					} 
					$codes_array[] = $code; 
				}
				$x++;
				
			}
			$codes_array = implode (", ", $codes_array);
			if($codes_array){
				$this->delete_excess_items($codes_array,$ce_id);
			}
		
			return $result;
		}
	}
	
	function check_item_code( $code, $ce_id ){  
	
		$existing_code = $this->module->record( $this->CEI, array($this->CEI->tblname.'.'.$this->CEI->tblpref.'code' => $code, $this->CE->tblname.'.'.$this->CE->tblid => $ce_id ) );	
		$item_id = get_value($existing_code, $this->CEI->tblid);
		return $item_id;
		
	}
	
	public function create_logs_items( $params=array(),$parent_id ){
		 
		$params = $params ? $params: $this->params;
		extract($params);
 	 	$data = array();
		$logged_user_id = $this->logged->get_login_session('user_id');
		$table_name = $this->CEI->tblname.'_logs';
		 
		
		$table_exists = $this->db->table_exists( $table_name );
		
		$result = FALSE;
		if( $table_exists ){
			if( isset($id) == FALSE ){
				$post_id = $this->input->post('id');
				$get_id = $this->input->get('id');
				$id = $post_id ? $post_id : $get_id;
			}
			
			$main_data_array = (array) $this->Data->get_records( $this->CEI, array('where_params'=>array($this->CE->tblid=>$id)) );
			
			if( $main_data_array && $parent_id ){
				$log_data = array(
					$this->CE->tblpref.'logs_id'=>$parent_id,
					$this->CEI->tblpref.'logs_user_id'=>$logged_user_id,
					$this->CEI->tblpref.'logs_type'=>$type,
					$this->CEI->tblpref.'logs_created'=>app_cdt()
				);			
			
				foreach ($main_data_array as $main_data ) {
					 
					$main_array = json_decode(json_encode($main_data), True);
					$data[] = array_merge($main_array, $log_data);  
				}
				
				$this->db->insert_batch($table_name, $data); 
				// $this->db->insert_batch($table_name, $data); 
				$result = $this->db->insert_id();

			}  
			
		}
			
		return $result;
	}
	
	public function delete_excess_items($items,$ce_id){
		$table = $this->db->dbprefix($this->CEI->tblname);
		$ce_id_name = $this->CE->tblid;
		$item_code_name = $this->CEI->tblpref.'code';
		$query = $this->db->query("
			DELETE 
			FROM $table
			WHERE $ce_id_name = $ce_id
			AND $item_code_name NOT IN ($items)
		"); 
		/* DELETE 
			FROM $table
			WHERE $ce_id_name = $ce_id
			AND $item_code_name NOT IN ($items)*/
	} 
	
	public function notify_user( $creator_user_id ){
		$user_detail = $this->User->get_logged_user();
		$user_id = get_value( $user_detail, $this->User->tblid ); 
	} 
	
	public function send_email_confirmation( $ce_id, $ce_log_id){ 
		$this->load->library('excel'); 
		$this->load->library('email');  
		/*---------------------------------------------*/
		$this->load->model('modules/Cost_Estimate_Logs_Model', 'CEL'); 
		$this->load->model('modules/Working_Budget_Model', 'WB'); 
		/*---------------------------------------------*/
		$attach = FALSE;
		$attach1 = FALSE;
		$date_now = date('Y-m-d-H-i');
		/* START of Updated Information */ 
		if (!is_dir('uploads/emails/'.$this->CE->view.'/')) {
			mkdir('./uploads/emails/'.$this->CE->view.'/', 0777, TRUE); 
		}  
		$filename='Cost Estimate List -'.$date_now; 
		$result_main = $this->CE->get_report_individual( $ce_id ); 
		$file_path = 'uploads/emails/'.$this->CE->view.'/'.$filename.'.xls'; 
		 $columns = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 30,'L' => 30,'M' => 30
		);
		if( $result_main ){
			//forattachment 
			$this->excel->export_report_individual_nodownload( $result_main, $filename, $file_path, $columns );
			$attach = TRUE;			
		}   
		/* END of Updated Information */
		
		/* START of Log Information */
		if (!is_dir('uploads/emails/'.$this->CEL->view.'/')) {
			mkdir('./uploads/emails/'.$this->CEL->view.'/', 0777, TRUE); 
		}  
		
		$filename_logs ='Log For Cost Estimate List -'.$date_now; 
		$file_path_logs = 'uploads/emails/'.$this->CEL->view.'/'.$filename_logs.'.xls'; 
		$result_logs = $this->CEL->get_report_individual( $ce_log_id ); 
		$columns_logs = array(
			'A' => 15, 'B' => 20, 'C' => 20,'D' => 20,'E' => 25,'F' => 20,'G' => 20,'H' => 20,'I' => 20,'J' => 30,'K' => 30,'L' => 30,'M' => 30
		); 
		if( $result_logs ){
			//forattachment 
			$this->excel->export_report_individual_nodownload( $result_logs, $filename_logs, $file_path_logs, $columns_logs );
			$attach1 = TRUE; 
		}  
		/* END of Log Information */
		 
		$ce_detail = $this->module->record( $this->CE, $ce_id);  
		$project_name = get_value($ce_detail, $this->CE->tblpref.'project_name');
		$data['project_name'] = get_value($ce_detail, $this->CE->tblpref.'project_name');
		$created_by_user_id = get_value($ce_detail, $this->CE->tblpref.'created_by_user_id');
		$date_modified = get_value($ce_detail, $this->CE->tblpref.'modified');
		$creator_detail = $this->User->get_by_id( $created_by_user_id );  		 
		$data['firstname'] = get_value($creator_detail, $this->User->tblpref.'firstname');
		$firstname = get_value($creator_detail, $this->User->tblpref.'firstname');
		$data['lastname'] = get_value($creator_detail, $this->User->tblpref.'lastname'); 
		$lastname = get_value($creator_detail, $this->User->tblpref.'lastname'); 
		$receiver_email = get_value($creator_detail, $this->User->tblpref.'email'); 
		$user_detail = $this->User->get_logged_user(); 
		$username = get_value($user_detail, $this->User->tblpref.'username');
		$date_now = date("jS F, Y", strtotime($date_now));
		
		// $htmlContent = $this->load->view('backend/modules/cost-estimate/email-template',$data, TRUE );
		 
		$message = '<p>Dear '.$firstname.',</p>'; 
		$message .= '<table border="0" cellpadding="0" cellspacing="0">';
		$message .= '<tr><td><p>Please be notified that your CE for the project <b>'.$project_name.'</b> was overwritten by <b>'.$username.'</b> on '.$date_modified.'. Please see your original CE attached for your reference.</p></td></tr></table>';
		$subject = 'CE Overwrite: '.$project_name; 
		$this->email->clear();
		$config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'marktestingemail@gmail.com';
        $config['smtp_pass']    = 'iWPEC@16';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);
		  
		$this->email->from('rubin@iwebprovider.com'); 
		$this->email->to($receiver_email);  
		$this->email->subject($subject);
		$this->email->message($message);
		 
		/* if( $attach ){
			$this->email->attach($file_path); 
		} */
		if( $attach1 ){
			$this->email->attach($file_path_logs); 
		} 
		if (!$this->email->send()){
			show_error($this->email->print_debugger()); 
		}  
	}
}

