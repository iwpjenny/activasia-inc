<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Area_Model extends CI_Model {
	
	public $page = 'ModArea';
	public $view = 'area';
	public $fpage = '';
	public $tblname = 'user_area';
	public $tblpref = 'ua_';
	public $tblid = 'ua_id'; 
	public $name = 'Area';
	public $description = 'Area'; 
	
    function __construct(){
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->helper('functions');
		$this->load->database();
		
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */
		$this->load->model('User_Model','User');
		$this->load->model('modules/Area_Model','Area');
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
		 $data = array(
			array($this->Area->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->Area->tblid.'='.$this->Area->tblname.'.'.$this->Area->tblid,'LEFT'),
		 
		);
		return $data;
	}
	
	public function set_search_fields(){ 
		 
		$data = array(
			$this->tblname.'.'.$this->tblid, 
			$this->tblpref.'title', 
			$this->tblpref.'description',
			$this->tblpref.'latitude',
			$this->tblpref.'longitude',
			$this->tblpref.'created',
			$this->User->tblpref.'firstname', 
			$this->User->tblpref.'lastname' 
		);
		
		return $data;
	} 
	 
	/*---------------------------------------------*/ 
	/*Custom functions here------------------------*/
	function get_checked_items($user_id, $region_id = ''){  
		$query_params = array( 
			'where_params' => array($this->User->tblid => $user_id),
			
		); 
		if($region_id){
			$join_params = $this->set_join_params();	
			$query_params['join_params'] = $join_params;
			$query_params['where_params']['reg_id'] = $region_id;
		}
		    
		$rows = $this->Data->get_records($this, $query_params);   
		return $rows; 
		 
	} 
	function save_checked_items($areas, $user_id){   
		if($areas){
			$delete = $this->Data->delete_data( $this, array($this->User->tblid=>$user_id) ); 
			foreach($areas as $area){ 
				$id = get_value($area, $this->tblid);
				$entry = array(
					$this->User->tblid => $user_id, 
					$this->Area->tblid => $id 
				); 
				$this->records->save( $this, $entry);    
			}  
		}
	}
	
}
