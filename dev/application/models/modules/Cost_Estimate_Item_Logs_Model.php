<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cost_Estimate_Item_Logs_Model extends CI_Model {
	
	public $page = 'ModCostEstimateItems';
	public $view = 'cost-estimate-items';
	public $fpage = '';
	public $tblname = 'cost_estimate_items_logs';
	public $tblpref = 'cei_';
	public $tblid = 'cei_id';
	public $urc_name = 'cost_estimate_items';
	public $name = 'Cost Estimate Items';
	public $description = 'Cost Estimate Items';
	
	public $user_role = array(
		array( 
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'export','name'=>'Export Excel','value'=>'yes'),
			array('field'=>'import','name'=>'Import Excel','value'=>'yes'),
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
	public $member_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes') 
		),
		array( 
			array('field'=>'trash','name'=>'Trash','value'=>'yes')
		)
	);
	
    function __construct(){
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->helper('functions');
		$this->load->database();
		
        parent::__construct();
    }
	
	public function ini_custom_models(){
		
		/* Initialize Custom Model here */ 
		$this->load->model('modules/Cost_Estimate_Model', 'CE');  
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
		 $data = array(
			array($this->CE->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->CE->tblid.'='.$this->CE->tblname.'.'.$this->CE->tblid,'LEFT'), 
		);
		
		return $data;
	}
	
	public function set_search_fields(){ 
		$this->load->model('modules/CE_Records_Model', 'CE'); 
		$this->load->model('modules/Region_Model', 'Reg');
		$this->load->model('modules/Area_Model', 'Area');
		/*---------------------------------------------*/
		$data = array( 
		
		);	 
		
		return $data;
	} 
	 
	function get_select( $ModelUsedBy, $record_fields ){
		
		$id = get_value($record_fields, $this->tblid);
		
		$select = $this->records->select($this, $ModelUsedBy, $id, array(
			'query_params'=>array('orderby'=>$this->tblpref.'title','ordertype'=>'ASC'),
			'fields'=>array('title')
		));
		
		return $select;
	}
	 
	/*---------------------------------------------*/
	
	/*Custom functions here------------------------*/ 
	function get_items( $ce_id ){ 
		
		$query_params = array( 
			'where_params' => array( $this->CE->tblpref.'logs_id' => $ce_id )
		);    
		$return = $this->Data->get_records( $this, $query_params );   
		return $return;
		
	}
	function check_item_code($code){ 
		if($code){
			$result = $this->Data->get_record( $this , array(
				'where_params' =>array( $this->tblpref.'code like' => $code ) 
			));    
			return $result;
		}
		
	} 
}
