<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_Model  extends CI_Model {
	
	public $page = 'Member';
	public $view = 'member';
	public $fpage = 'FMember';
	public $tblname = 'std_member';
	public $tblpref = 'mem_';
	public $tblid = 'mem_id';
	public $name = 'Member';
	public $urc_name = 'member';
	public $description = 'Member';
	public $folder = 'member';
	public $display_section = array('menu-sidebar-left'=>array('order'=>2),'dashboard'=>array('order'=>1));
	
	public $user_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'view_user_role','name'=>'View User Role','value'=>'yes'),
			array('field'=>'picture','name'=>'Picture Upload','value'=>'yes')
		) 
	);
	
	public $member_role = array(
		array(
			array('field'=>'update_profile','name'=>'Update Profile','value'=>'yes'), 
			array('field'=>'update_password','name'=>'Update Password','value'=>'yes'),
			array('field'=>'picture','name'=>'Picture Upload','value'=>'yes')
		)
	);
	
    function __construct(){
		
        parent::__construct();
    }

	public function create_table(){
		$charset = $this->db->char_set;
		$collate = $this->db->dbcollat;
		$table_name = $this->db->dbprefix.$this->tblname;
		$prefix = $this->tblpref;
		
		$table_query_structure = "CREATE TABLE IF NOT EXISTS `".$table_name."` (
		  `".$this->tblid."` int(10) NOT NULL AUTO_INCREMENT COMMENT 'User ID',
		  `".$this->MR->tblid."` int(10) NOT NULL COMMENT 'User Role ID',
		  `".$prefix."username` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."password` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."email` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."firstname` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."midname` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."lastname` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."created` datetime NOT NULL,
		  `".$prefix."modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		  `".$prefix."published` tinyint(1) NOT NULL DEFAULT '1',
		  PRIMARY KEY (`".$this->tblid."`)
		) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=".$charset." COLLATE=".$collate." ROW_FORMAT=DYNAMIC;";
		  
		$this->load->library('DBTable');
		$this->dbtable->execute( $table_name, $table_query_structure );
	}

	public function ini_custom_models(){
		$this->create_table();
		$this->MP->create_table();
		$this->MR->create_table();
		$this->ML->create_table();
		/* Initialize Custom Model here */
		
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
		
		$data = array( 
			array($this->MP->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->tblid.'='.$this->db->dbprefix.$this->MP->tblname.'.'.$this->tblid,'LEFT'), 
			array($this->MR->tblname,$this->db->dbprefix.$this->tblname.'.'.$this->MR->tblid.'='.$this->db->dbprefix.$this->MR->tblname.'.'.$this->MR->tblid,'LEFT') 
		);
		
		return $data;
	}
	
	public function set_search_fields(){ 
		
		$data = array(
			$this->tblpref.'firstname',
			$this->tblpref.'midname',
			$this->tblpref.'lastname', 
			$this->tblpref.'username'
		);
		return $data;
	}
	
	function get_by_id( $id ){
		
		$join_params = $this->set_join_params(); 
		$where_params = array( $this->tblname.'.'.$this->tblid=>$id, $this->tblpref.'published'=>1 );
		
		$query_params = array(
			'join_params'=>$join_params,
			'where_params'=> $where_params
		);
		
		$result = $this->Data->get_record( $this, $query_params );
		
		return $result;
	}
	
	function get_by_username_email( $username, $by_username_email=FALSE ){
		
		if( $by_username_email ){   
			$where_params = "(".$this->tblpref."username='".$username."' OR ".$this->tblpref."email='".$username."')";
			$where_params .= " AND ".$this->tblpref."published='1'"; 
		} else {
			$where_params = array(
				$this->tblpref.'username'=>$username,
				$this->tblpref.'published'=>1
			);
		}
		
		$join_params = $this->set_join_params(); 
		
		$query_params = array(
			'join_params'=>$join_params,
			'where_params'=> $where_params
		);
		
		$result = $this->Data->get_record( $this, $query_params );
		
		return $result;
	}
	
	function get_logged_user(){
		
		$id = $this->logged->get_login_session('user_id');
				
		$result = $this->get_by_id( $id );
		
		return $result;
	}

	function get_select( $model_used_by, $record_fields ){
		
		$id = get_value($record_fields, $this->tblid);
		
		$select = $this->fields->select($this, $model_used_by, $id, array(
			'query_params'=>array('orderby'=>$this->tblpref.'lastname','ordertype'=>'ASC'),
			'fields'=>array('lastname','firstname')
		));
		
		return $select;
	}
	
	public function get_total_records(){
		
		$this->db->from($this->tblname);
		$this->db->where(array($this->tblpref.'published' =>'1'));
		$total = $this->db->count_all_results();
		
		return $total;
	}
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
}
