<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backend_Log_Model extends CI_Model {

	public $page = 'BLog';
	public $view = 'log';
	public $tblname = 'std_user';
	public $tblpref = 'user_';
	public $tblid = 'user_id';
	public $login_user_type = 'admin';
	public $dashboard_page = 'BHome';
	public $location = 'backend';

	function __construct(){
		
        parent::__construct();
    }
}