<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configurations_Model extends CI_Model {
	
	public $page = 'configurations';
	public $view = 'configurations';
	public $tblname = 'std_settings';
	public $tblpref = 'set_';
	public $tblid = 'set_id';
	public $per_page = 15;
	
	public $name = 'Configurations';
	public $urc_name = 'urc_configurations';
	public $description = 'Configurations User Role';
	
	public $user_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		)	 
	);
	
    function __construct(){
		
        parent::__construct();
    }

	public function ini_custom_models(){
		$this->load->model('Settings_Model', 'Set');
		$this->load->model('Settings_Format_Model', 'SFM');
		
		/* Initialize Custom Model here */
		
		/* End of initialize custom model here */	
	}
	
	public function set_join_params(){
				
		$data = array();
		
		return $data;
	}
	
	public function set_search_fields(){
		
		$data = array(
			$this->tblname.'.'.$this->tblid,
			$this->tblpref.'title',
			$this->tblpref.'created'
		);
		
		return $data;
	}
		
}