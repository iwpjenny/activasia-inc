<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Profile_Model  extends CI_Model {
	
	public $page = 'userprofile';
	public $view = 'userprofile';
	public $tblname = 'std_user_profile';
	public $tblpref = 'upf_';
	public $tblid = 'upf_id';
	public $per_page = 15;
	public $folder = 'user';

    function __construct(){
		
        parent::__construct();
    }

	function trash_data( $where_params ){ 
		$params = array(
			$this->tblpref.'trashed'=>1
		);
		
		$this->db->where($where_params);
		$result = $this->db->update($this->tblname, $params); 
		
		return $result;
    }

	public function create_table(){
		$charset = $this->db->char_set;
		$collate = $this->db->dbcollat;
		$table_name = $this->db->dbprefix.$this->tblname;
		$prefix = $this->tblpref;
		
		$table_query_structure = "CREATE TABLE IF NOT EXISTS `".$table_name."` (
		  `".$this->tblid."` int(11) NOT NULL AUTO_INCREMENT COMMENT 'User Profile ID',
		  `".$this->User->tblid."` int(11) NOT NULL COMMENT 'User ID',
		  `".$prefix."address` varchar(100) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."contact_no` char(15) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."picture` varchar(255) CHARACTER SET ".$charset." COLLATE ".$collate." DEFAULT NULL,
		  `".$prefix."created` datetime NOT NULL,
		  `".$prefix."modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY (`".$this->tblid."`)
		) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=".$charset." COLLATE=".$collate." ROW_FORMAT=DYNAMIC;";
		  
		$this->load->library('DBTable');
		$this->dbtable->execute( $table_name, $table_query_structure );
	}

	public function ini_custom_models(){
		$this->create_table();
		
		/* Initialize Custom Model here */
		
		/* End of initialize custom model here */	
	}
}
