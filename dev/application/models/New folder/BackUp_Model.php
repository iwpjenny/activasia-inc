<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BackUp_Model extends CI_Model {
	
	public $page = 'BackUp';
	public $view = 'backup';
	public $tblname = 'backup';
	public $tblpref = 'backup_';
	public $tblid = 'backup_id';
	public $per_page = 15;
	
	public $name = 'BackUp';
	public $urc_name = 'backup';
	public $description = 'Manager BackUp';
	public $display_section = array('menu-sidebar-left'=>array('order'=>4),'dashboard'=>array('order'=>6));
	
	public $user_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes'),
			array('field'=>'download','name'=>'Download','value'=>'yes'),
			array('field'=>'delete_session','name'=>'Delete Other Session','value'=>'yes'),
		),
		array(
			array('field'=>'backup_database','name'=>'BackUp Database','value'=>'yes'),
			array('field'=>'backup_application','name'=>'BackUp Application Files Only','value'=>'yes'),
			array('field'=>'backup_all','name'=>'BackUp All','value'=>'yes'),
			array('field'=>'trancate_module_records','name'=>'Trancate Module Records','value'=>'yes'),
			array('field'=>'trancate_logs','name'=>'Trancate Logs','value'=>'yes')
		)
	);
	
	public $format = 'zip';
	public $backup_path;
	public $dir_path;
	private $base_path;

    function __construct(){
		
		$this->base_path = str_replace('\\','/',FCPATH);
		$this->backup_path = 'backup/';
		$this->dir_path = $this->base_path.$this->backup_path;
		
        parent::__construct();
    }

	public function ini_custom_models(){
		
		/* Initialize Custom Model here */
		
		/* End of initialize custom model here */	
	}

    function records( $dir_path='' ){
		
		$records = array();
		$dir_path = $dir_path?$dir_path:$this->dir_path;
		$backup_files = app_get_file_open_dir( $dir_path );
		
		if( $backup_files ){
			$id=1;
			foreach($backup_files as $name){
				if( !in_array($name, array('index.html')) ){
					$pathinfo = pathinfo($dir_path.$name);
					$size = filesize($dir_path.$name);
					$datetime = date('Y-m-d H:i:s',filemtime($dir_path.$name));
					$datetime_changed = date('Y-m-d H:i:s',filectime($dir_path.$name));
					$basename = get_value( $pathinfo, 'basename' );
					$filename = get_value( $pathinfo, 'filename' );
					$extension = get_value( $pathinfo, 'extension' );
					
					$records[] = (object) array(
						'id'=>$id,
						'basename'=>$basename,
						'extension'=>$extension,
						'dir_path'=>$dir_path,
						'filename'=>$filename,
						'size'=>$size,
						'datetime'=>$datetime,
						'datetime_changed'=>$datetime_changed
					);
					$id++;
				}
			}
		}
		
		return $records;
    }

    function backup_database( $dir='./backup/', $name='' ){
		$this->load->dbutil();
		$this->load->helper('file');
		
		
		$datetime = date('Ymd-His');
		$filename = $name?$name:'db-'.$datetime.'.'.$this->format;
		$filepath = $dir.$filename;
		$sql_name = $this->db->database.'-'.$datetime.'.sql';
		
		$params = array(
			'format'      => $this->format,		/* gzip, zip, txt */
			'filename'    => $sql_name,		/* File name - NEEDED ONLY WITH ZIP FILES */
			'add_drop'    => TRUE,			/* Whether to add DROP TABLE statements to backup file */
			'add_insert'  => TRUE,			/* Whether to add INSERT data to backup file */
			'newline'     => "\n"			/* Newline character used in backup file */
		);
		$backup = $this->dbutil->backup($params);
		$result = @write_file($filepath, $backup);
		
		return $result;
    }

    function backup_database_and_application_folder(){
		$this->load->library('zip');
		
		$base_path = str_replace('\\','/',FCPATH);
		
		$datetime = date('Ymd-His');
		$app_path = $this->base_path.'application/';
		$filename = 'application-'.$datetime.'.'.$this->format;
		
		$this->backup_database( $app_path );
		$this->zip->read_dir( $app_path, FALSE ); 
		$result = $this->zip->archive($this->backup_path.$filename);
		
		return $result;
    }

    function backup_all(){
		$this->load->library('zip');
		$this->load->helper('directory');
		
		$environment = $this->Set->get_settings('general','environment','local'); 
			
		$datetime = date('Ymd-His');
		$dirpath_backup_temp = $this->backup_path.$environment.'-backup-'.$datetime.'/';
		
		$dirpath_app = $this->base_path.'application';
		$dirpath_system = $this->base_path.'system';
		$dirpath_includes = $this->base_path.'includes';
		$dirpath_uploads = $this->base_path.'uploads';

		$result = FALSE;		
		
		if( file_exists( $this->backup_path ) ){
			if( mkdir( $dirpath_backup_temp ) ){
				copy( $this->base_path.'index.php', $dirpath_backup_temp.'index.php' );
				copy( $this->base_path.'license.txt', $dirpath_backup_temp.'license.txt' );
				$this->copy_dir( $dirpath_app, $dirpath_backup_temp.'application' );
				$this->copy_dir( $dirpath_system, $dirpath_backup_temp.'system' );
				$this->copy_dir( $dirpath_includes, $dirpath_backup_temp.'includes' );
				if( file_exists( $dirpath_backup_temp.'uploads' ) ){
					$this->copy_dir( $dirpath_uploads, $dirpath_backup_temp.'uploads' );
				}
				$this->backup_database( $dirpath_backup_temp );
			
				$filename = $environment.'-backup-'.$datetime.'.'.$this->format;
				
				$this->zip->read_dir( $dirpath_backup_temp, FALSE ); 
				$result = $this->zip->archive($this->backup_path.$filename);
				
				$this->remove_dir( $dirpath_backup_temp );
			}
		} else {
			mkdir( $this->backup_path );
		}
		
		return $result;
    }
	
	function copy_dir($src, $dst) {
		$dir = opendir($src);
		@mkdir($dst);
		while(FALSE !== ( $file = readdir($dir)) ){
			if (( $file != '.' ) && ( $file != '..' )){
				if ( is_dir($src . '/' . $file) ) {
					$this->copy_dir($src . '/' . $file,$dst . '/' . $file);
				} else {
					copy($src . '/' . $file,$dst . '/' . $file);
				}
			}
		}
		closedir($dir);
	}
	
	function remove_dir( $dir ){
		
		$dir = rtrim($dir,'/');
		
		$result = FALSE;
	
		if( is_dir($dir) ){
			$files = scandir($dir);
			foreach($files as $file){
				if($file != '.' && $file != '..'){
					//if (!is_dir($dir.'/'.$file)){
						//unlink($dir.'/'.$file);
					//} else {
						$this->remove_dir($dir.'/'.$file);
					//}
				}
			}
			$result = rmdir($dir);
		} elseif( file_exists($dir) ){
			$result = unlink($dir);
		}

		return $result;
	}
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/	 
	
	function get_dashboard_section_view( $data ){
			
		$location = app_get_val($data,'location');
		
		$data['display_latest_backups'] = app_get_val($data['settings'],'display_latest_backups');		
		$data['grid_date_time_format'] = app_get_val($data['settings'],'grid_date_time_format'); 
		$data['view_date_time_format'] = app_get_val($data['settings'],'view_date_time_format'); 
		$data['character_limit'] = app_get_val($data['settings'],'grid_text_limit');
		
		$data['records'] = $this->records();
		
		$result = $this->load->view($location.'/'.$this->view.'/section-dashboard', $data, TRUE);
		
		return $result;
	}
	
	function truncate_logs(){ 
		$this->db->truncate($this->db->dbprefix.$this->UL->tblname); 
		$this->db->truncate($this->db->dbprefix.$this->ML->tblname);
    } 
	
	function truncate_transactions(){ 
		$this->load->model('modules/Transactions_Model', 'Trans');
		$this->load->model('modules/Transactions_Sub_Model', 'TransSub');
		$this->load->model('modules/Transactions_Files_Model', 'TransF');
		
		$this->db->truncate($this->db->dbprefix.$this->Trans->tblname); 
		$this->db->truncate($this->db->dbprefix.$this->TransSub->tblname); 
		$this->db->truncate($this->db->dbprefix.$this->TransF->tblname);
    }
}
