<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_Format_Model extends CI_Model {
	
	public $page = 'settings';
	public $view = 'settings-format';
	public $tblname = 'std_settings_format';
	public $tblpref = 'setf_';
	public $tblid = 'setf_id';
	public $per_page = 15;	
	public $name = 'Settings Format';
	public $urc_name = 'urc_settings';
	public $description = 'Settings User Role';
	
    function __construct(){
		
        parent::__construct();
    }

	public function create_table(){
		$charset = $this->db->char_set;
		$collate = $this->db->dbcollat;
		$table_name = $this->db->dbprefix.$this->tblname;
		$prefix = $this->tblpref;
		$datetime = app_cdt();
		
		$table_query_structure = "CREATE TABLE IF NOT EXISTS `".$table_name."` (
		  `".$this->tblid."` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Settings Format ID',
		  `".$this->Set->tblid."` int(10) NOT NULL DEFAULT '0' COMMENT 'Settings ID',
		  `".$prefix."title` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."name` char(30) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."description` char(20) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."default_value` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."values` longtext CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."type` enum('text','textarea','checkbox','radio','select','email','password','date','datetime','multiselect') CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL DEFAULT 'text',
		  `".$prefix."max_chars` int(10) NOT NULL,
		  `".$prefix."multiple_records` tinyint(1) NOT NULL,
		  `".$prefix."created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		  `".$prefix."modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
		  `".$prefix."published` tinyint(1) NOT NULL DEFAULT '1',
		  PRIMARY KEY (`".$this->tblid."`)
		) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=".$charset." COLLATE=".$collate." ROW_FORMAT=DYNAMIC;
		INSERT INTO ".$table_name." VALUES ('1', '2', 'View Date Time Format', 'view_date_time_format', 'View Date Time Forma', 'Y-m-d H:i:s', '', 'text', '50', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('2', '2', 'Grid Date Time Format', 'grid_date_time_format', 'Grid Date Time Forma', 'Y-m-d H:i:s', '', 'text', '50', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('3', '2', 'View Time Format', 'view_time_format', 'View Time Format', 'H:i:s', '', 'text', '50', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('4', '2', 'View Date Format', 'view_date_format', 'View Date Format', 'Y-m-d', '', 'text', '50', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('5', '2', 'Grid Time Format', 'grid_time_format', 'Grid Time Format', 'H:i:s', '', 'text', '50', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('6', '2', 'Grid Date Format', 'grid_date_format', 'Grid Date Format', 'Y-m-d', '', 'text', '50', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('7', '6', 'Grid Text Limit', 'grid_text_limit', 'Grid Text Character', '100', '', 'text', '99', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('8', '10', 'Grid Text Limit', 'grid_text_limit', 'Grid Text Character', '100', '', 'text', '50', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('9', '1', 'System Name', 'system_name', 'System Name', 'Web Application Framework', '', 'text', '100', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('10', '1', 'Email', 'email', 'Email', '', '', 'email', '150', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('11', '5', 'Image Thumbnail Width', 'image_thumbnail_width', 'Image Thumbnail Widt', '150', '', 'text', '5', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('12', '5', 'Image Thumbnail Height', 'image_thumbnail_height', 'Image Thumbnail Heig', '150', '', 'text', '4', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('13', '6', 'View Text Limit', 'view_text_limit', 'View Text Character', '100', '', 'text', '99', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('14', '6', 'Grid Paragraph Limit', 'grid_paragraph_limit', 'Grid Paragraph Limit', '100', '', 'text', '99', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('15', '6', 'View Paragraph Limit', 'view_paragraph_limit', 'View Paragraph Limit', '100', '', 'text', '99', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('16', '13', 'Image Large Width', 'image_large_width', 'Image Large Width', '1500', '', 'text', '4', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('17', '13', 'Image Large Height', 'image_large_height', 'Image Large Height', '1500', '', 'text', '4', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('18', '13', 'Image Medium Width', 'image_medium_width', 'Image Medium Width', '700', '', 'text', '4', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('19', '13', 'Image Medium Height', 'image_medium_height', 'Image Medium Height', '700', '', 'text', '4', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('20', '13', 'Image Thumbnail Width', 'image_thumbnail_width', 'Image Thumbnail Widt', '300', '', 'text', '4', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('21', '13', 'Image Thumbnail Height', 'image_thumbnail_height', 'Image Thumbnail Heig', '300', '', 'text', '4', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('22', '10', 'View Text Limit', 'view_text_limit', 'View Text Limit', '100', '', 'text', '99', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('23', '10', 'Grid Paragraph Limit', 'grid_paragraph_limit', 'Grid Paragraph Limit', '100', '', 'text', '99', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('24', '10', 'View Paragraph Limit', 'view_paragraph_limit', 'View Paragraph Limit', '100', '', 'text', '99', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('25', '15', 'Meta Title', 'meta_title', 'Meta Title', 'Web Application Framework', '', 'text', '255', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('26', '15', 'Meta Description', 'meta_description', 'Meta Description', '', '', 'textarea', '10000', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('27', '15', 'Meta Author', 'meta_author', 'Meta Author', 'IWP', '', 'text', '255', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('28', '12', 'title', 'title', 'Content Title', 'Department of Environment and Natural Resources', '', 'text', '700', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('29', '12', 'Left Content Text', 'left_content_text', 'Left Content Text', '', '', 'textarea', '99999999', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('37', '4', 'Display Latest Member Logs', 'display_latest_member_logs', 'Display Latest Membe', 'yes', 'yes|Yes\r\nno|No', 'radio', '0', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('30', '12', 'Right Content Text', 'right_content_text', 'Right Content Text', '', '', 'textarea', '99999999', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('34', '4', 'Display Latest Users', 'display_latest_users', 'Display Latest Users', 'yes', 'yes|Yes\r\nno|No', 'radio', '0', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('35', '4', 'Display Latest Members', 'display_latest_members', 'Display Latest Membe', 'yes', 'yes|Yes\r\nno|No', 'radio', '0', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('36', '4', 'Display Latest Transactions', 'display_latest_transactions', 'Display Latest Trans', 'yes', 'yes|Yes\r\nno|No', 'radio', '0', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('31', '16', 'Default Status', 'default_status', 'Default Status', 'publish', 'publish|Publish\r\nunpublish|UnPublish', 'radio', '0', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('32', '4', 'Display Protected Area', 'display_protected_area', 'Display Protected Ar', 'yes', 'yes|Yes\r\nno|No', 'radio', '0', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('33', '4', 'Display Tribe', 'display_tribe', 'Display Tribe', 'yes', 'yes|Yes\r\nno|No', 'radio', '0', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('38', '4', 'Display Latest User Logs', 'display_latest_user_logs', 'Display Latest User', 'yes', 'yes|Yes\r\nno|No', 'radio', '0', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('39', '4', 'Display Latest Backups', 'display_latest_backups', 'Display Latest Backu', 'yes', 'yes|Yes\r\nno|No', 'radio', '0', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('40', '1', 'Company/Organization', 'companyorganization', 'Company/Organization', 'Company', '', 'text', '999', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('41', '1', 'Short Desctiption', 'short_desctiption', 'Short Desctiption', 'Desctiption', '', 'textarea', '2000', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('42', '1', 'Full Desctiption', 'full_desctiption', 'Full Desctiption', 'Full Desctiption', '', 'textarea', '3000', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('43', '10', 'Copyright', 'copyright', 'Copyright', 'Copyright © 2017', '', 'text', '1000', '0', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('44', '12', 'Sub Title', 'sub_title', 'Sub Title', 'Region X', '', 'text', '500', '0', '".$datetime."', '".$datetime."', '1');";
		  
		$this->load->library('DBTable');
		$this->dbtable->execute( $table_name, $table_query_structure );
	}

	public function ini_custom_models(){
		$this->create_table();
		
		/* Initialize Custom Model here */
		
		/* End of initialize custom model here */	
	}
	
	function get_config_format( $id='', $multiple_records=0){
		$this->load->model('Settings_Model', 'Set');
		
		$result = $this->Data->get_records( $this, array( 'where_params' => array($this->Set->tblid => $id, $this->SFM->tblpref.'multiple_records' => $multiple_records ) ) );    
		
		return $result;
	}
	
	function get_format_type( $type='', $name='', $description='', $values ='', $id, $maxchar='', $default_value=''){	
		 
		$setr_value = '';
		$setf_values = explode(PHP_EOL, $values); 
		 
		if($type == 'multiselect' || $type == 'checkbox'){
			$results = $this->Data->get_records( $this->SRM, array( 'where_params' => array($this->tblid => $id) ) );  
			
			if($results){ 
				$setr_value = $results;   
			}
			else if($default_value && $results == NULL){
				$setr_value = $default_value;  
				
			}
			 
			if($type == 'multiselect'){
				$result = '<select name="'.$name.'[]'.'" class="form-control" multiple>';
				foreach($setf_values as $values){
					$value = explode('|', $values); 
					$result .= '<option value="'.$value[0].'"';
					if(is_array($setr_value)){ 
						foreach($setr_value as $setr){  
							if($value[0] == $setr->setr_value){
								$result .='selected';
							}  
						} 
					}
					else{
						if($value[0] == $setr_value){ 
							$result .='selected'; 
						}   
					}
					$result .= '>'.$value[1].'</option>';
				}
				$result .= '</select>';
				 
			}
			else if($type == 'checkbox'){ 
				$result = '<input name="'.$name.'[]'.'" value="0" type="hidden">'; 
				foreach($setf_values as $values){
					$value = explode('|', $values);    
					$result .='<input type="checkbox" name="'.$name.'[]'.'" value="'.$value[0].'"'; 
					  
					if(is_array($setr_value)){
						foreach($setr_value as $setr){  
							if($value[0] == $setr->setr_value){ 
								$result .='checked'; 
							}   
						}	 			
					}
					else{
						if($value[0] == $setr_value){ 
							$result .='checked'; 
						}   
					}
					 	 
					$result .='>&nbsp'.$value[1].'&nbsp;'; 
				} 
							
			} 
		} 
		else{
			$result = $this->Data->get_record( $this->SRM, array( 'where_params' => array($this->tblid => $id) ) );  
			if($result){ 
				$setr_value = $result->setr_value;   
			}	 
			else if($default_value && $result == NULL){
				$setr_value = $default_value;  
				
			}
			if($type == 'text'){ 
				$result = '<input type="text" class="form-control" name="'.$name.'" value="'.$setr_value.'"  placeholder="'.$description.'" maxlength="'.$maxchar.'"/>';
			}
			else if($type == 'email'){
				$result = '<input type="email" class="form-control" name="'.$name.'" value="'.$setr_value.'"  placeholder="'.$description.'" maxlength="'.$maxchar.'"/>';
			}
			else if($type == 'password'){
				$result = '<input type="password" class="form-control" name="'.$name.'" value="'.$setr_value.'"  placeholder="'.$description.'" maxlength="'.$maxchar.'"/>';
			}
			else if($type == 'date'){
				$result = '<input type="text" name="'.$name.'" value="'.$setr_value.'" class="form-control datepicker" placeholder="Date" readonly="readonly" />';
			}
			else if($type == 'datetime'){
				$result = '<input type="text" name="'.$name.'" value="'.$setr_value.'" class="form-control datetimepicker" placeholder="Date" readonly="readonly" />';
			}
			else if($type == 'textarea'){
				$result = '<textarea class="form-control" name="'.$name.'" row="2" placeholder="'.$description.'" maxlength="'.$maxchar.'"/>'.$setr_value.'</textarea>';
			}
			else if($type == 'select'){
				$result = '<select name="'.$name.'" class="form-control">';
				$result .= '<option>--Select--</option>';
				foreach($setf_values as $values){
					$value = explode('|', $values); 
					if($value[0] == $setr_value){
						$result .= '<option value="'.$value[0].'" selected>'.$value[1].'</option>';
					}
					else{
						$result .= '<option value="'.$value[0].'" >'.$value[1].'</option>';
					}
				}
				$result .= '</select>';
			}
			
			else if($type == 'radio'){ 
				$result = '';
				foreach($setf_values as $values){
					$value = explode('|', $values);     
					if($value[0] == $setr_value){
						$result .='<input type="radio" name="'.$name.'" value="'.$value[0].'" checked>&nbsp'.$value[1].'&nbsp;';
					}
					else{
						$result .='<input type="radio" name="'.$name.'" value="'.$value[0].'">&nbsp'.$value[1].'&nbsp;';
					}
				} 
			}
			// else{
				// $result = 'Invalid type';
			// }
		}
		//printx($result);	
		return $result;
	} 
	
	function get_format_type_multiple( $type='', $name='', $description='', $values ='', $id, $maxchar='', $default_value='', $setr_id='', $setf_id){	 
		$setr_value = '';
		$setf_values = explode(PHP_EOL, $values);  
			if($setr_id){
				$result = $this->Data->get_record( $this->SRM, array( 'where_params' => array($this->SRM->tblid => $setr_id, $this->SFM->tblid => $setf_id) ) );  
				if($result){ 
					$setr_value = $result->setr_value;   
				}
			}
			else{
				$setr_value = $default_value;  
			}  
			if($type == 'text'){ 
				$result = '<input type="text" class="form-control" name="'.$name.'" value="'.$setr_value.'"  placeholder="'.$description.'" maxlength="'.$maxchar.'"/>';
			}
			else if($type == 'email'){
				$result = '<input type="email" class="form-control" name="'.$name.'" value="'.$setr_value.'"  placeholder="'.$description.'" maxlength="'.$maxchar.'"/>';
			}
			else if($type == 'password'){
				$result = '<input type="password" class="form-control" name="'.$name.'" value="'.$setr_value.'"  placeholder="'.$description.'" maxlength="'.$maxchar.'"/>';
			}
			else if($type == 'date'){
				$result = '<input type="text" name="'.$name.'" value="'.$setr_value.'" class="form-control datepicker" placeholder="Date" readonly="readonly" />';
			}
			else if($type == 'datetime'){
				$result = '<input type="text" name="'.$name.'" value="'.$setr_value.'" class="form-control datetimepicker" placeholder="Date" readonly="readonly" />';
			}
			else if($type == 'textarea'){
				$result = '<textarea class="form-control" name="'.$name.'" row="2" placeholder="'.$description.'" maxlength="'.$maxchar.'"/>'.$setr_value.'</textarea>';
			}  
			
			else if($type == 'select'){    
				$results = $this->Data->get_records( $this->SRM, array( 'where_params' => array($this->SFM->tblid => $setf_id) ) );  
				if($results){ 
					$setr_value = $results;   
				} 
				else{
					$setr_value = $default_value;  
				}     
				
				$result = '<select name="'.$name.'" class="form-control">'; 
				$result .= '<option>--Select--</option>';
				
				foreach($setf_values as $key => $values){
					$value = explode('|', $values);   
					if(is_array($setr_value)){ 
						foreach($setr_value as $setr){    
							if($value[0] == $setr->setr_value){  
								unset($setf_values[$key]); 
							} 					
						}     
					}  	
				}    
				foreach($setf_values as $values){
					$value = explode('|', $values);   
					if(is_array($setr_value)){  
						foreach($setr_value as $key => $setr){
							if($value[0] != $setr->setr_value){
								$result .= '<option value="'.$value[0].'" >'.$value[1].'</option>';
							}
						}
					} 
					else{
						if($value[0] != $setr_value){ 
							$result .= '<option value="'.$value[0].'" >'.$value[1].'</option>'; 	
						} 						
					} 		 				
				}  
				$result .= '</select>';
			}
			 
			
			else if($type == 'radio'){ 
				$result = '';
				foreach($setf_values as $values){
					$value = explode('|', $values);     
					if($value[0] == $setr_value){
						$result .='<input type="radio" name="'.$name.'" value="'.$value[0].'" checked>&nbsp'.$value[1].'&nbsp;';
					}
					else{
						$result .='<input type="radio" name="'.$name.'" value="'.$value[0].'">&nbsp'.$value[1].'&nbsp;';
					}
				} 
			}
			else{
				$result = 'Invalid type';
			}
		
		return $result;
	}  
}