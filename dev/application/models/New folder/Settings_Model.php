<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_Model extends CI_Model {
	
	public $page = 'settings';
	public $view = 'settings';
	public $tblname = 'std_settings';
	public $tblpref = 'set_';
	public $tblid = 'set_id';
	public $per_page = 15;
	
	public $name = 'Settings';
	public $urc_name = 'settings';
	public $description = 'Settings User Role';
	public $display_section = array('menu-sidebar-left'=>array('order'=>3));
	
	public $user_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		)  
	);
	
    function __construct(){
		
        parent::__construct();
    }

	public function create_table(){
		$charset = $this->db->char_set;
		$collate = $this->db->dbcollat;
		$table_name = $this->db->dbprefix.$this->tblname;
		$prefix = $this->tblpref;
		$datetime = app_cdt();
		
		$table_query_structure = "CREATE TABLE IF NOT EXISTS `".$table_name."` (
		  `".$this->tblid."` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Settings ID',
		  `".$prefix."parent_id` int(10) NOT NULL,
		  `".$prefix."name` char(30) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."title` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."description` varchar(100) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."multiple_records` tinyint(1) unsigned zerofill NOT NULL DEFAULT '0',
		  `".$prefix."serialize` text CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		  `".$prefix."modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		  `".$prefix."published` tinyint(1) NOT NULL DEFAULT '1',
		  PRIMARY KEY (`".$this->tblid."`)
		) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=".$charset." COLLATE=".$collate." ROW_FORMAT=DYNAMIC;
		INSERT INTO ".$table_name." VALUES ('1', '0', 'general', 'General', 'General', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('2', '0', 'date_and_time', 'Date and Time', 'Date and Time', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('3', '0', 'backend', 'Backend', 'Backend', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('4', '0', 'admin_dashboard', 'Admin Dashboard', 'Admin Dashboard', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('5', '0', 'admin_uploads', 'Admin Uploads', 'Admin Uploads', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('6', '0', 'backend_appearance', 'Backend Appearance', 'Backend Appearance', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('7', '0', 'frontend', 'Frontend', 'Frontend', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('8', '0', 'member_dashboard', 'Member Dashboard', 'Member Dashboard', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('9', '0', 'member_uploads', 'Member Uploads', 'Member Uploads', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('10', '0', 'frontend_appearance', 'Frontend Appearance', 'Frontend Appearance', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('11', '0', 'module', 'Module', 'Module', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('12', '0', 'home_page', 'Home Page', 'Home Page', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('13', '0', 'uploads', 'Uploads', 'Uploads', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('14', '0', 'export', 'Export', 'Export', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('15', '0', 'meta', 'Meta', '', '0', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('16', '0', 'records', 'Records', 'Records', '0', '', '".$datetime."', '".$datetime."', '1');";
		  
		$this->load->library('DBTable');
		$this->dbtable->execute( $table_name, $table_query_structure );
	}

	public function ini_custom_models(){
		$this->load->model('Configurations_Model', 'Config');
		$this->load->model('Settings_Format_Model', 'SFM'); 
		$this->load->model('Settings_Records_Model', 'SRM');
		// $this->create_table();
		// $this->SFM->create_table();
		// $this->SRM->create_table(); 
		
		/* Initialize Custom Model here */
		
		/* End of initialize custom model here */	
	}
	
	public function set_search_fields(){
		
		$data = array(
			$this->tblname.'.'.$this->tblid,
			$this->tblpref.'title',
			$this->tblpref.'created'
		);
		
		return $data;
	}
	
	function set_page_login( $data ){
		
		$data['user_total'] = $this->Data->count_records_row( $this->User, array('where_params'=>array( $this->User->tblpref.'published'=>1) ) );
		$data['user_logs_total'] = $this->Data->count_records_row( $this->UL );
		
		return $data;
	}

	function select( $selected_value='', $class='', $attr='' ){	

		$rows = timezone_identifiers_list();

		$result = '<select name="custome[timezone]" class="form-control '.$class.'" '.$attr.'>';
		if( $selected_value == '' || $selected_value == '0' ){
			$result .= '<option value="" selected="selected">---select---</option>';
		} else {
			$result .= '<option value="">---select---</option>';
		}
		if( $rows ){
			foreach($rows as $col => $key){
			
				if( $selected_value == $col ){
					$result .= '<option value="'.$col.'" selected="selected">'.$key.'</option>';
				} else {
					$result .= '<option value="'.$col.'">'.$key.'</option>';
				}
			}
		}
		$result .= '</select>';

		return $result;
	} 
	
	function get_settings($name, $custom_field='', $default_value=''){
		
		$settings = $this->Data->get_record( $this, array('where_params'=>array( $this->tblpref.'name'=>$name, $this->tblpref.'published'=>'1') ) );
		$serialize = $this->tblpref.'serialize';
		 
		if( $serialize && isset( $settings->$serialize ) ){
			$unserialize = unserialize($settings->$serialize);
		} else {
			$unserialize = '';
		}
		
		if( $custom_field ){
			$result = isset($unserialize[$custom_field])?$unserialize[$custom_field]:$default_value;
		} else {
			$result = (object) $unserialize;
		}
		
		return $result;	
	
	}
	
	function get_full_settings( $name ){
		
		$settings = $this->Data->get_record( $this, array('where_params'=>array( $this->tblpref.'name'=>$name, $this->tblpref.'published'=>'1') ) );
		
		$field = $this->tblpref.'serialize';
		$serialize = isset($settings->$field)?$settings->$field:'';
		$unserialize = unserialize($serialize);
		
		if( $unserialize ){
			$result = (object) array_merge((array)$settings, $unserialize);
		} else {
			$result = array();
		}
		
		return $result;
	}
	
	function textarea_value_and_text( $param ){
		$this->load->model('Settings_Model', 'Set');
		
		$value = array();
		$text = array();
		
		$explode = explode("\n", $param);
		
		if( $explode ){
			for( $x=0; $x < count( $explode ); $x++ ){
				$text_and_value = explode( "|", $explode [$x] );
				for( $y=0; $y < count( $text_and_value ); $y++ ){
					if( !empty( $text_and_value ) ){
						if( $y == 0 ){
							$get_value = isset($text_and_value[$y])?$text_and_value[$y]:null;
							array_push( $value , $get_value);
						} elseif( $y == 1){
							$get_text = isset($text_and_value[$y])?$text_and_value[$y]:null;
							array_push( $text , $get_text);
						}	
					}
					
				}
				
			}
		}
		
		$return_params = array(
			'value' => $value,
			'text' => $text
		);
			
		return $return_params;
	}
	
	function select_dropdown( $param=array(), $selected_value='', $select_name='' , $id= '', $class='' , $attr=''){
		
		$result = '<select name="'.$select_name.'" id="'.$id.'" class="'.$class.' form-control" '.$attr.' >';
		if( $selected_value == '' || $selected_value == '0' || $selected_value == null ){
			$result .= '<option value="" selected="selected">---select---</option>';
		} else {
			$result .= '<option value="">---select---</option>';
		}
		
		if( $param ){
			$value = get_value( $param , 'value' );
			$text = get_value( $param , 'text' );
			
			$count = count($value);
			
			for( $x=0; $x<$count; $x++){
				if(!empty( $value[$x] )){
					if( $selected_value == $value[$x] ){
					
						$result .= '<option value="'.$value[$x].'" selected="selected">'.$text[$x].'</option>';
					} else {
						$result .= '<option value="'.$value[$x].'" >'.$text[$x].'</option>';
					}	
				}
			}		
		}
		$result .= '</select>';
		return $result;
    }
	
	function dropdown( $dbparams, $selected_value='', $where_params_data = array() ,$name='', $class='', $attr=''){	 
		$this->load->model('Data_Model','Data');
		$params = $this->Data->set_params( $dbparams );
		extract($params); 
		extract($where_params_data['where_params']);   
		$rows = $this->Data->get_records( $dbparams, $where_params_data ); 
		
		$result = '<select name="'.$name.'" class="form-control '.$class.'" '.$attr.'>';
		if( $selected_value == '' || $selected_value == '0' ){
			$result .= '<option value="" selected="selected">---select---</option>';
			
		} else {
			$result .= '<option value="">---select---</option>'; 
		}
		if( $rows ){ 
			foreach( $rows as $col ){
				$idvar = $tblid;
				$titlevar = $tblpref.'title';

				$id = $col->$idvar;
				$title = $col->$titlevar;

				if( $selected_value == $id ){
					$result .= '<option value="'.$id.'" selected="selected">'.$title.'</option>';
				} else {
					$result .= '<option value="'.$id.'">'.$title.'</option>';
				}
			}
		}
		$result .= '</select>';

		return $result;
	}
	
	// currency
	public function get_currency_format(){
		
		$currency_settings = $this->get_settings('currency'); 
		$currency_settings = $currency_settings?$currency_settings:'';
		$currency_format = get_value($currency_settings, 'currency_format'); 
		
		return $currency_format;
	}
	
	// header export
	public function get_header_export_title( $file_format ){
	
		$get_header_title = $this->Set->get_settings('header-export-title'); 
	
		if( $file_format == 'pdf' ){
			$get_header_title = $get_header_title?$get_header_title:'';
			$header_title = get_value($get_header_title, 'pdf_header'); 
		} elseif( $file_format == 'excel' ){
			$get_header_title = $get_header_title?$get_header_title:'';
			$header_title = get_value($get_header_title, 'excel_header'); 
		}
		
		return $header_title;
	}
	
	public function show_config_format( $config_format, $prefix_format, $set_name, $setr_id=''){ 
		if( $config_format ){   
			$setf_id = get_value($config_format,$prefix_format.'id');
			$setf_title = get_value($config_format,$prefix_format.'title'); 
			$setf_type = get_value($config_format,$prefix_format.'type');  
			$setf_name = get_value($config_format,$prefix_format.'name');  
			$setf_max_chars = get_value($config_format,$prefix_format.'max_chars');  
			$setf_multiple_records = get_value($config_format,$prefix_format.'multiple_records');    
			$setf_description = get_value($config_format,$prefix_format.'description');  
			$setf_values = get_value($config_format,$prefix_format.'values');  
			$setf_default_value = get_value($config_format,$prefix_format.'default_value');  
			$name = $set_name.'['.$setf_id.']';  
			
			if($setf_multiple_records == 1){ 
				$format_type = $this->SFM->get_format_type_multiple($setf_type, $name, $setf_description, $setf_values, $setf_id, $setf_max_chars, $setf_default_value, $setr_id, $setf_id); 
			}
			else{
				$format_type = $this->SFM->get_format_type($setf_type, $name, $setf_description, $setf_values, $setf_id, $setf_max_chars, $setf_default_value);  
			}	
		$data = array (
			'format_type' => $format_type,
			'setf_title' => $setf_title
		);
		}
		return $data;
	} 
	
		
}