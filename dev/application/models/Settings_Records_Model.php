<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_Records_Model extends CI_Model {
	
	public $page = 'settings';
	public $view = 'settings-records';
	public $tblname = 'std_settings_records';
	public $tblpref = 'setr_';
	public $tblid = 'setr_id';
	public $per_page = 15;
	
	public $name = 'Settings Records';
	public $urc_name = 'urc_settings';
	public $description = 'Settings User Role';
		
    function __construct(){
		
        parent::__construct();
    }

	public function create_table(){
		$charset = $this->db->char_set;
		$collate = $this->db->dbcollat;
		$table_name = $this->db->dbprefix.$this->tblname;
		$prefix = $this->tblpref;
		$datetime = app_cdt();
		
		$table_query_structure = "CREATE TABLE IF NOT EXISTS `".$table_name."` (
		  `".$this->tblid."` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Settings Records ID',
		  `".$this->SFM->tblid."` int(10) NOT NULL DEFAULT '0' COMMENT 'Settings Format ID',
		  `".$prefix."value` longtext CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."text` varchar(255) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		  `".$prefix."modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
		  `".$prefix."published` tinyint(1) NOT NULL DEFAULT '1',
		  PRIMARY KEY (`".$this->tblid."`)
		) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=".$charset." COLLATE=".$collate." ROW_FORMAT=DYNAMIC;
		INSERT INTO ".$table_name." VALUES ('1', '6', 'Y-m-d', '', ".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('2', '5', 'H:i:s', '', ".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('3', '4', 'Y-m-d', '', ".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('4', '3', 'H:i:s', '', ".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('5', '2', 'Y-m-d H:i a', '', ".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('6', '1', 'Y-m-d H:i:s', '', ".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('7', '21', '300', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('8', '20', '300', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('9', '19', '700', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('10', '18', '700', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('11', '17', '1500', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('12', '16', '1500', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('13', '24', '100', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('14', '23', '100', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('15', '22', '100', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('16', '8', '100', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('17', '15', '100', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('18', '14', '100', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('19', '13', '100', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('20', '7', '100', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('21', '12', '150', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('22', '11', '150', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('23', '10', 'accounts@iwebprovider.com', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('24', '9', 'Web Application Framework', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('25', '27', 'IWP', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('26', '26', 'Sample Description', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('27', '25', 'DENR R 10', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('28', '28', 'System Name Here...', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('29', '29', 'Content Here...', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('30', '30', 'Content Here...', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('31', '31', 'publish', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('32', '37', 'yes', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('33', '34', 'yes', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('34', '35', 'yes', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('35', '36', 'yes', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('36', '32', 'yes', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('37', '33', 'yes', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('38', '38', 'yes', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('39', '39', 'yes', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('40', '40', 'decimal_degrees', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('41', '41', 'dms', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('42', '45', 'Copyright © 2017', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('43', '42', 'Company', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('44', '43', 'Description', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('45', '44', 'Full Description', '', '".$datetime."', '".$datetime."', '1');
		INSERT INTO ".$table_name." VALUES ('46', '46', 'Full Description', '', '".$datetime."', '".$datetime."', '1');";
		  
		$this->load->library('DBTable');
		$this->dbtable->execute( $table_name, $table_query_structure );
	}

	public function ini_custom_models(){
		$this->create_table();
		
		/* Initialize Custom Model here */
		
		/* End of initialize custom model here */	
	}
}