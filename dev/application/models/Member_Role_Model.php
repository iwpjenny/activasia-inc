<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_Role_Model extends CI_Model {
	
	public $page = 'MemberRole';
	public $view = 'memberrole';
	public $tblname = 'std_member_role';
	public $tblpref = 'mur_';
	public $tblid = 'mur_id';
	public $per_page = 15;
	public $name = 'Member Role';
	public $urc_name = 'member_role';
	public $description = 'Member Role';
	
	public $user_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'name_editable','name'=>'Name Editable','value'=>'yes'),
			array('field'=>'view_user_role','name'=>'View User Role','value'=>'yes')
		)
	);
	
    function __construct(){
		
        parent::__construct();
    }

	public function create_table(){
		$charset = $this->db->char_set;
		$collate = $this->db->dbcollat;
		$table_name = $this->db->dbprefix.$this->tblname;
		$prefix = $this->tblpref;
		
		$table_query_structure = "CREATE TABLE IF NOT EXISTS `".$table_name."` (
		  `".$this->tblid."` int(10) NOT NULL AUTO_INCREMENT COMMENT 'User Role ID',
		  `".$prefix."parent_id` int(10) DEFAULT '0',
		  `".$prefix."title` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."name` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."position` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."description` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." DEFAULT NULL,
		  `".$prefix."capabilities_serialized` text CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."created` datetime NOT NULL,
		  `".$prefix."modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		  `".$prefix."published` tinyint(1) NOT NULL DEFAULT '1',
		  PRIMARY KEY (`".$this->tblid."`)
		) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=".$charset." COLLATE=".$collate." ROW_FORMAT=DYNAMIC;";
		  
		$this->load->library('DBTable');
		$this->dbtable->execute( $table_name, $table_query_structure );
	}

	public function ini_custom_models(){
		$this->create_table();
		
		/* Initialize Custom Model here */
		
		/* End of initialize custom model here */	
	}
	
	public function set_search_fields(){
		
		$data = array(
			$this->tblpref.'title',
			$this->tblpref.'position',
			$this->tblpref.'description'
		);
		
		return $data;
	}
	
	public function set_join_params(){
				
		$data = array();
		
		return $data;
	}
	
	function get_by_id( $id ){
				
		$result = $this->Data->get_record($this, array('where_params'=>array( $this->tblid=>$id )));
		
		return $result;
	}
	
	public function contains_word($str, $word){		
		return !!preg_match('#\b' . preg_quote($word, '#') . '\b#i', $str);
	}
	
	public function urc_records( $records, $capabilities ){
				
		$data['records'] = $records;
		$data['capabilities'] = $capabilities;		
		$contents = $this->load->view('backend/'.$this->view.'/capabilities',$data,TRUE);
		
		echo $contents;
	}

	public function get_select( $ModelUsedBy, $record_fields ){
		
		$id = get_value($record_fields, $this->tblid);		
		$select = $this->fields->select( $this, $ModelUsedBy, $id, array('req'=>TRUE), TRUE );
		
		return $select;
	}
	
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
}