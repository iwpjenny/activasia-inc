<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_Role_Model extends CI_Model {
	
	public $page = 'UserRole';
	public $view = 'userrole';
	public $tblname = 'std_user_role';
	public $tblpref = 'ur_';
	public $tblid = 'ur_id';
	public $per_page = 15;
	public $name = 'User Role';
	public $urc_name = 'user_role';
	public $description = 'User Role';
	
	public $user_role = array(
		array(
			array('field'=>'records','name'=>'Records','value'=>'yes'),
			array('field'=>'view','name'=>'View','value'=>'yes'),
			array('field'=>'add','name'=>'Add','value'=>'yes'),
			array('field'=>'edit','name'=>'Edit','value'=>'yes'),
			array('field'=>'delete','name'=>'Delete','value'=>'yes')
		),
		array(
			array('field'=>'name_editable','name'=>'Name Editable','value'=>'yes'),
			array('field'=>'view_user_role','name'=>'View User Role','value'=>'yes')
		)
	);
	
    function __construct(){
		
        parent::__construct();
    }

	public function create_table(){
		$charset = $this->db->char_set;
		$collate = $this->db->dbcollat;
		$table_name = $this->db->dbprefix.$this->tblname;
		$prefix = $this->tblpref;
		$datetime = app_cdt();
		
		$table_query_structure = "CREATE TABLE IF NOT EXISTS `".$table_name."` (
		  `".$this->tblid."` int(10) NOT NULL AUTO_INCREMENT COMMENT 'User Role ID',
		  `".$prefix."parent_id` int(10) DEFAULT '0',
		  `".$prefix."title` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."name` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."position` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."description` char(50) CHARACTER SET ".$charset." COLLATE ".$collate." DEFAULT NULL,
		  `".$prefix."capabilities_serialized` text CHARACTER SET ".$charset." COLLATE ".$collate." NOT NULL,
		  `".$prefix."created` datetime NOT NULL,
		  `".$prefix."modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		  `".$prefix."published` tinyint(1) NOT NULL DEFAULT '1',
		  PRIMARY KEY (`".$this->tblid."`)
		) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=".$charset." COLLATE=".$collate." ROW_FORMAT=DYNAMIC;
		INSERT INTO ".$table_name." VALUES ('1', '0', 'Admin', 'admin', 'admin', '', 'a:38:{s:6:\"access\";a:1:{s:4:\"full\";s:3:\"yes\";}s:6:\"backup\";a:8:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:8:\"download\";s:3:\"yes\";s:23:\"trancate_module_records\";s:3:\"yes\";s:13:\"trancate_logs\";s:3:\"yes\";}s:18:\"urc_configurations\";a:5:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";}s:11:\"member_logs\";a:2:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";}s:6:\"member\";a:7:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:14:\"view_user_role\";s:3:\"yes\";s:7:\"picture\";s:3:\"yes\";}s:11:\"member_role\";a:7:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:13:\"name_editable\";s:3:\"yes\";s:14:\"view_user_role\";s:3:\"yes\";}s:8:\"settings\";a:5:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";}s:9:\"user_logs\";a:2:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";}s:4:\"user\";a:8:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:7:\"profile\";s:3:\"yes\";s:15:\"update_password\";s:3:\"yes\";s:7:\"picture\";s:3:\"yes\";}s:9:\"user_role\";a:7:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:13:\"name_editable\";s:3:\"yes\";s:14:\"view_user_role\";s:3:\"yes\";}s:8:\"barangay\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:16:\"classified_caves\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:4:\"city\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:19:\"conservation_status\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:15:\"data_collection\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:11:\"demographic\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:8:\"economic\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:25:\"housing_health_sanitation\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:28:\"indigenous_people_demograhic\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:28:\"indigenous_people_livelihood\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:17:\"indigenous_people\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:24:\"priority_island_wetlands\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:10:\"livelihood\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:22:\"name_of_protected_area\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:5:\"penro\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:28:\"population_survey_monitoring\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:23:\"updating_protected_area\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:8:\"province\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:5:\"sitio\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:16:\"threaten_species\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:12:\"transactions\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:16:\"transactions_sub\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:20:\"transactions_uploads\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:6:\"upload\";s:3:\"yes\";s:8:\"download\";s:3:\"yes\";}s:13:\"name_of_tribe\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:7:\"uploads\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:6:\"upload\";s:3:\"yes\";s:8:\"download\";s:3:\"yes\";}s:32:\"waterbird_population_per_species\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:16:\"wetland_location\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}s:19:\"wildlife_violations\";a:6:{s:7:\"records\";s:3:\"yes\";s:4:\"view\";s:3:\"yes\";s:4:\"edit\";s:3:\"yes\";s:3:\"add\";s:3:\"yes\";s:6:\"delete\";s:3:\"yes\";s:5:\"trash\";s:3:\"yes\";}}', '".$datetime."', '".$datetime."', '1');";
		  
		$this->load->library('DBTable');
		$this->dbtable->execute( $table_name, $table_query_structure );
	}

	public function ini_custom_models(){
		$this->create_table();
		
		/* Initialize Custom Model here */
		$this->load->model('modules/Transactions_Model', 'Trans');
		/* End of initialize custom model here */	
	}
	
	public function set_search_fields(){
		
		$data = array(
			$this->tblpref.'title',
			$this->tblpref.'position',
			$this->tblpref.'description'
		);
		
		return $data;
	}
	
	public function set_order_by(){

		$field = $this->tblpref.'created';

		return $field;
	}
	
	public function set_join_params(){
				
		$data = array();
		
		return $data;
	}
	
	function get_by_id( $id ){
				
		$result = $this->Data->get_record($this, array('where_params'=>array( $this->tblid=>$id )));
		
		return $result;
	}
	
	public function contains_word($str, $word){		
		return !!preg_match('#\b' . preg_quote($word, '#') . '\b#i', $str);
	}
	
	public function urc_records( $records, $capabilities ){
				
		$data['records'] = $records;
		$data['capabilities'] = $capabilities;		
		$contents = $this->load->view('backend/'.$this->view.'/capabilities',$data,TRUE);
		
		echo $contents;
	}

	public function get_select( $ModelUsedBy, $record_fields ){
		
		$id = get_value($record_fields, $this->tblid);		
		$select = $this->fields->select( $this, $ModelUsedBy, $id, array('req'=>TRUE), TRUE );
		
		return $select;
	}
	
	/*---------------------------------------------*/
	/*Custom functions here------------------------*/
	public function get_models_urc( $sub_dir='' ){
		
		$restricted_files = array('Data_Model.php','Generic_Model.php','Upload_Settings_Model.php','Settings_Format_Model.php','Settings_Records_Model.php','index.html');
		
		$this->load->helper('directory');
		$base_path = str_replace('\\','/',FCPATH);
		$module_path = $base_path.'application/models/'.$sub_dir;
		$module_array = app_get_file_open_dir( $module_path );
		
		$result = array();
		if( $module_array ){
			foreach($module_array as $key=>$module){
				if( in_array($module, $restricted_files) == FALSE ){
					include_once($module_path.$module);
					
					$objname = str_replace('.php','',$module);
					$objname = $sub_dir.$objname;
					
					$this->load->model($objname,$objname);
					
					if( isset($this->$objname->user_role) ){
						$result[] = (object) array(
							'name'			=> $this->$objname->name,
							'urc_name'		=> $this->$objname->urc_name,
							'description'	=> $this->$objname->description,
							'user_role'		=> $this->$objname->user_role
						);
					}
				}
			}
		}
		
		return $result;
	}
	
	public function get_email_recepients($capability){ 
	 
		$records = $this->Data->get_records( $this );  
		
		$records_array = array();
		foreach($records as $record){ 
			$capabilities_serialized = get_value($record, $this->tblpref.'capabilities_serialized');
			$capabilities_serialized = unserialize($capabilities_serialized);
			$cash_advance_capabilities = $capabilities_serialized[$this->CA->tblname];
			$assigned_capability = get_value($cash_advance_capabilities,$capability);
			if($assigned_capability){
				$records_array[] = get_value($record, $this->tblid);
			}
		}  
		
		$users = $this->User->get_recepients($records_array);
		
		return $users;
	}
	
	public function get_email_recepients_role($role){ 
	 
		$result = $this->Data->get_record($this, array('where_params'=>array( $this->tblpref.'name'=>$role )));
		$id = get_value($result, $this->tblid); 
		$users = $this->User->get_recepients($id);
		
		return $users;
	}
}