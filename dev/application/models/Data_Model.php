<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_Model extends CI_Model {
	
	public $query;
	public $per_page = 10;
	public $total_rows;
	public $pagination_link;
	
	private $tblpref;
	private $paging;
	private $filter_type;
	private $ordertype='DESC';

    function __construct(){
		$this->load->database();
		
        parent::__construct();
    }
	
	function set_params( $model, $query_params=array() ){
		
		$params = array();
		
		$params_default = array_merge(array(
			'field'=>'*',
			'compound_select'=>TRUE,
			'where_params'=>array(),
			'where_in_field'=>NULL,
			'where_in'=>array(),
			'where_not_in_field'=>NULL,
			'where_not_in'=>array(),
			'where_in_key'=>'',
			'where_in_value'=>array(),
			'join_params'=>array(),
			'orderby'=>'',
			'ordertype'=>'ASC',
			'groupby' => NULL,
			'limit' => NULL,
			'limit_offset' => NULL,
			'base_url' => '',
			'search_fields' => '',
			'filter_type' => NULL,
			'paging' => FALSE
			), $query_params
		);
		extract($params_default);
		
		if( is_object($model) ){
			$tblpref = $model->tblpref;
			$tblname = $model->tblname;
			
			$params['page'] = isset($model->page)?$model->page:'';
			$params['tblid'] = $model->tblid;
			$params['tblpref'] = $tblpref;
			$params['tblname'] = $tblname;
			$params['per_page'] = isset($model->per_page)?$model->per_page:$this->per_page;
			$params['dbprefix'] = isset($model->dbprefix)?$model->dbprefix:'';
			
			$params = array_merge( $params_default, $params );
		} else {
			$params = array_merge( $model, $query_params );
		}
		
		return $params;
	}
	
	function db_search_keyword( $field_params ){
		$keyword = $this->input->get('keyword');
		$keyword = $this->security->xss_clean( $keyword );
		
		if( $this->paging === TRUE && $field_params && $keyword ){
			/* $count = count($params);
			for( $x=0; $x < $count; $x++ ){
				$field = is_array($params[$x])?$params[$x][0]:$params[$x];
				$this->db->or_like( $field, $keyword );
			} */
			$n=1;
			$query = '(';
			foreach($field_params as $field){
				if( $n === 1 ){
					$query .= " `".$field."` LIKE '%".$keyword."%' ESCAPE '!'";
				} else {
					$query .= " OR `".$field."` LIKE '%".$keyword."%' ESCAPE '!'";
				}
				$n++;
			}
			$query .= ')';
			$this->db->where( $query );			
		}
	}
	
	function db_records_filter(){
		
		if( $this->paging === TRUE && $this->filter_type ){
			$where = array();
		
			if( $this->filter_type == 'month-year' ){
				$year = $this->input->get('year');
				$month = $this->input->get('month');
				
				if( $month && $year ){
					$where = "MONTH(`".$this->tblpref."created`) ='".$month."' AND YEAR(`".$this->tblpref.'created'."`) ='".$year."'";
				} elseif( empty($month) && $year ){
					$where = "YEAR(`".$this->tblpref."created`) ='".$year."'";
				}
			}
			if( $this->filter_type == 'date-range' ){
				$start_date = $this->input->get('start_date');
				$end_date = $this->input->get('end_date');
				
				if( $start_date && $end_date ){
					$where = "DATE(`".$this->tblpref."created`) BETWEEN '".$start_date."' AND '".$end_date."'";
				} elseif( $start_date && empty($end_date) ){
					$where = "DATE(`".$this->tblpref."created`)='".$start_date."'";
				}
			}
			$this->db->where( $where );
		}
	}
	
	function get_records( $dbparams, $query_params=array() ){ 
	
		$params = $this->set_params( $dbparams, $query_params );
		extract($params);
		
		$this->tblpref = $tblpref;
		$this->paging = $paging;
		$this->filter_type = $filter_type ? $filter_type : $this->input->get('filter_type');
		
		if( $paging === TRUE ){
			$this->load->library('pagination');
			
			$per_page = $this->per_page;
			$ordertype = $this->input->get('ordertype') ? $this->input->get('ordertype') : $this->ordertype;
			$current_page = $this->input->get('per_page');
			
			if( $dbprefix ){
				$this->db->set_dbprefix($dbprefix);
			}
			
			$this->db->select($field,$compound_select);
			$this->db->from($tblname);
			$this->db_join($join_params);
			$this->db_records_filter();
			$this->db_search_keyword( $search_fields );
			$this->db->where($where_params);
			if( $where_in_value ){
				$this->db->where_in($where_in_key, $where_in_value);
			}
			if( isset($where_not_in) && $where_not_in_field ){
				$this->db->where_not_in($where_not_in_field, $where_not_in);
			}
			if( isset($groupby) ){
				$this->db->group_by($groupby);
			}
			if( $orderby && $ordertype ){
				$this->db->order_by($orderby, $ordertype);
			} else {
				$this->db->order_by($orderby);
			}
			$total_rows = $this->db->get()->num_rows();
			
			if( empty($base_url) ){			
				parse_str($_SERVER['QUERY_STRING'],$query_string_array);
				unset($query_string_array['per_page']);
				$query_string = http_build_query($query_string_array);
				
				$base_url = site_url($query_string);
			}
			
			$config['base_url'] = $base_url;
			$config['total_rows'] = $total_rows;
			$config['per_page'] = $per_page;
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize( $config );
			
			$limit = $per_page;
			$limit_offset = $current_page;
		}
		
		$this->db->select($field,$compound_select);
		$this->db->from($tblname);
		$this->db_join($join_params);
		$this->db_search_keyword( $search_fields );
		$this->db_records_filter();
		$this->db->where($where_params);
		if( $where_in_value ){
			$this->db->where_in($where_in_key, $where_in_value);
		}
		if( isset($where_not_in) && $where_not_in ){
			$this->db->where_not_in($where_not_in_field, $where_not_in);
		}
		if( isset($groupby) ){
			$this->db->group_by($groupby);
		}
		if( $orderby && $ordertype ){
			$this->db->order_by($orderby, $ordertype);
		} else {
			$this->db->order_by($orderby);
		}
		$this->db->limit($limit, $limit_offset);
		$records = $this->db->get()->result();
		
		$this->query = $this->db->last_query();
		
		if( $paging === TRUE ){
			$this->total_rows = $total_rows;
			$this->per_page = $per_page;
			$this->current_page = $current_page;
			$this->pagination_link = $this->pagination->create_links();
		}
		
		return $records;
	}
	
    function get_record( $dbparams, $query_params=array() ){
		
		$params = $this->set_params( $dbparams, $query_params ); 
		extract($params);
		
		if( $dbprefix ){
			$this->db->set_dbprefix($dbprefix);
		}
		$this->db->select($field);
		$this->db->from($tblname);
		$this->db_join( $join_params );
		$this->db->where($where_params);
		if( isset($orderby) && isset($ordertype) ){
			$this->db->order_by( $orderby, $ordertype );
		}
		$result = $this->db->get()->row();
		$this->query = $this->db->last_query();
		 
		return $result;
    }
		
    function insert_data( $dbparams, $data ){
		   
		$params = $this->set_params( $dbparams );
		extract($params);
		
		$constant = array(
			$dbparams->tblpref.'modified'=>current_datetime(),
			$dbparams->tblpref.'created'=>current_datetime()
		);
		$merged_data = array_merge($data, $constant);
		
		$data = $this->format_data( $merged_data );

		if( $dbprefix ){
			$this->db->set_dbprefix($dbprefix);
		}
		$this->db->insert($tblname, $data); 
		$latest_id = $this->db->insert_id();
		$this->query = $this->db->last_query();
		
		return $latest_id;
    }

	/* old update function */
    function update_data( $dbparams, $data, $where_params ){
		
		$params = $this->set_params( $dbparams, array('where_params'=>$where_params) );
		extract($params);
	
		$constant = array(
			$dbparams->tblpref.'modified'=>current_datetime()
		);
			
		$merged_data = array_merge($data, $constant);	
		
		$data = $this->format_data( $merged_data );
				
		if( $dbprefix ){
			$this->db->set_dbprefix($dbprefix);
		}
		$this->db->where($where_params);
		if( $where_in_value ){
			$this->db->where_in($where_in_key, $where_in_value);
		}
		$result = $this->db->update($tblname, $data); 
		$this->query = $this->db->last_query();
		
		return $result;
    }
	
	/* new update function */
    function update_record( $dbparams, $data, $query_params ){
		
		$params = $this->set_params( $dbparams, $query_params );
		extract($params);
	
		$constant = array(
			$dbparams->tblpref.'modified'=>current_datetime()
		);
			
		$merged_data = array_merge($data, $constant);	
		
		$data = $this->format_data( $merged_data );
				
		if( $dbprefix ){
			$this->db->set_dbprefix($dbprefix);
		}
		$this->db->where($where_params);
		if( $where_in_value ){
			$this->db->where_in($where_in_key, $where_in_value);
		}
		$result = $this->db->update($tblname, $data); 
		$this->query = $this->db->last_query();
		
		return $result;
    }

    function delete_data( $dbparams, $where_params ){		 
		$this->load->helper('file');
		
		$params = $this->set_params( $dbparams, array('where_params'=>$where_params) );
		extract($params);
		
		if( $dbprefix ){
			$this->db->set_dbprefix($dbprefix);
		}
		$result = $this->db->delete($tblname, $where_params); 
		$this->query = $this->db->last_query();
		
		return $result;
    }
	
    function update( $dbparams, $data, $query_params ){
		
		$params = $this->set_params( $dbparams, $query_params );
		extract($params);
	
		$constant = array(
			$dbparams->tblpref.'modified'=>current_datetime()
		);
			
		$merged_data = array_merge($data, $constant);	
		
		$data = $this->format_data( $merged_data );
				
		if( $dbprefix ){
			$this->db->set_dbprefix($dbprefix);
		}
		$this->db->where($where_params);
		if( isset($where_in) && $where_in ){
			$this->db->where_in($where_in_field, $where_in);
		} 
		$result = $this->db->update($tblname, $data); 
		$this->query = $this->db->last_query();
		
		return $result;
    }

    function delete( $dbparams, $query_params ){		 
		$this->load->helper('file');
		
		$params = $this->set_params( $dbparams, $query_params );
		extract($params);
		
		if( $dbprefix ){
			$this->db->set_dbprefix($dbprefix);
		}
		$this->db->where($where_params);
		if( isset($where_in) && $where_in ){
			$this->db->where_in($where_in_field, $where_in);
		} 
		$result = $this->db->delete($tblname); 
		$this->query = $this->db->last_query();
		
		return $result;
    }

    function format_data( $data ){
		
		$result = $data;
		$data_formatted = array();
		
		if( $data ){
			foreach($data as $key=>$value){
				if( is_array($value) ){
					$value = trim(serialize($value));
				}
				$data_formatted[$key] = $value;
			}
			$result = $data_formatted;
		}
		
		return $result;
    }
	
	function select( $dbparams, $selected_value='', $class='', $attr='required="required"', $placeholder='---select---' ){	
		$params = $this->set_params( $dbparams );
		extract($params);
			 
		$query_params = array('where_params'=>array($tblpref.'published'=>'1'),'orderby'=>$dbparams->tblid,'ordertype'=>'ASC'); 
		  
		$rows = $this->get_records( $dbparams, $query_params );

		$result = '<select name="'.$tblid.'" class="form-control '.$class.'" '.$attr.'>';
		$placeholder = isset( $placeholder)?$placeholder:'select';
		
		if( $selected_value == '' || $selected_value == '0' ){
			$result .= '<option value="" selected="selected">'.$placeholder.'</option>';
		} else {
			$result .= '<option value="">'.$placeholder.'</option>';
		}
		if( $rows ){
			foreach( $rows as $col ){
				$idvar = $tblid;
				$titlevar = $tblpref.'title';

				$id = $col->$idvar;
				$title = $col->$titlevar;

				if( $selected_value == $id ){
					$result .= '<option value="'.$id.'" selected="selected">'.$title.'</option>';
				} else {
					$result .= '<option value="'.$id.'">'.$title.'</option>';
				}
			}
		}
		$result .= '</select>';

		return $result;
	}
	
	function count_records_row( $dbparams, $query_params=array(), $join_params= array() ){
		
		$params = $this->set_params( $dbparams, $query_params );
		extract($params);
		
		if( $dbprefix ){
			$this->db->set_dbprefix($dbprefix);
		}
		$this->db->select($field);
		$this->db->from($tblname);
		if( $join_params ){
			$this->db_join( $join_params );
		}
		$this->db->where($where_params);
		$this->db->order_by($orderby, $ordertype);
		if( !empty( $limit ) ){
			$this->db->limit( $limit );
		}
		if( !empty( $groupby ) ){
			$this->db->group_by( $tblname.'.'.$groupby );
		}
		$result = $this->db->get()->num_rows();
		
		return $result;
    }
	
	function get_title( $dbparams, $query_params=array()){
		
		$params = $this->set_params( $dbparams, $query_params );
		extract($params);
		
		if( $dbprefix ){
			$this->db->set_dbprefix($dbprefix);
		}
		$this->db->select($field);
		$this->db->from($tblname);		
		$this->db->where($where_params);		
		$result = $this->db->get()->row();		
		
		$title = get_value( $result, $tblpref.'title');		
		
		return $title;
    }
	
	
	function db_join( $params ){
	
		$result = array();
		if( $params ){			
			foreach($params as $col){
				list($join_table, $on_field, $join_type) = $col;
				$result = $this->db->join( $join_table, $on_field, $join_type );
			}
		}
			
		return $result;
	}
	
	/* do not use this anymore */
	/* function db_like( $params, $keyword ){
		
		$result = array();
		if( $params && $keyword ){
			$count = count($params);
			for( $x=0; $x < $count; $x++ ){
				$field = is_array($params[$x])?$params[$x][0]:$params[$x];
				$result = $this->db->or_like( $field, $keyword );
			}			
		}
		
		return $result;
	} */
	
	function insert_update_data( $dbparams, $data, $id='', $where_params=array() ){
		if( $id ){
			$this->update_data( $dbparams, $data, $where_params );
		} else {
			$this->insert_data( $dbparams, $data );
		}
	}
}
