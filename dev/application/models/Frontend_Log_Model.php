<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frontend_Log_Model extends CI_Model {

	public $page = 'FLog';
	public $view = 'log';
	public $tblname = 'std_member';
	public $tblpref = 'mem_';
	public $tblid = 'mem_id';
	public $login_user_type = 'member';
	public $dashboard_page = 'FHome';
	public $location = 'frontend';

	function __construct(){
		
        parent::__construct();
    }
}