<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UsersLogs {
	
	private $Data;
	private $model;
	private $model_user;
	private $model_user_logs;
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	
	/* parameters */
	
    function __construct( $params=array() ){
        $CI =& get_instance();
		
		$params_default = array_merge(array(
			'model'=>NULL,
			'model_user'=>NULL,
			'model_user_logs'=>NULL
			), $params
		);
		extract($params_default);
		
		$this->model = $model;
		$this->model_user = $model_user;
		$this->model_user_logs = $model_user_logs;
		
		$this->Data = $CI->Data;
    }

    function __destruct(){
        $this->Data = NULL;
		
		$this->model = NULL;
		$this->model_user = NULL;
		$this->model_user_logs = NULL;
    }
	
	public function record( $query_params_or_id=array() ){
				
		if( is_array($query_params_or_id) ){
			$where_params = $query_params_or_id;
		} else {	
			$where_params = array( $this->model->tblname.'.'.$this->model->tblid => $query_params_or_id ); 
		}
		
		$join_params = $this->model->set_join_params(); 
		$query_params = array('where_params'=>$where_params,'join_params'=>$join_params );
		
		$fields = $this->Data->get_record( $this->model, array(
			'where_params'=>$where_params,
			'join_params'=>$join_params 
		));
		
		return $fields;
	}
	
	public function records( $data, $where_params=array() ){ 
        $CI =& get_instance();
		
		$search_fields = $this->model->set_search_fields(); 
		$join_params = $this->model->set_join_params();
		$orderby = method_exists($this->model,'set_order_by')?$this->model->set_order_by():NULL;
		
		$query_params = array(
			'join_params'=>$join_params,
			'search_fields'=> $search_fields,
			'where_params'=> $where_params,
			'groupby'=> $CI->db->dbprefix.$this->model->tblname.'.'.$this->model->tblid,
			'orderby'=> $orderby,
			'ordertype'=>'DESC',
			'paging'=>TRUE
		);  
		$records = $this->Data->get_records( $this->model, $query_params ); 
		$data['record_rows'] = $records;
		$data['pagination'] =  $this->Data->pagination_link; 
		$data['per_page'] = $this->Data->per_page;
		$data['total_rows'] = $this->Data->total_rows;
		
		return $data;
	}
	
	function save( $params, $action='view', $user_id='' ){
        $CI =& get_instance();
		
		$logged_user_id = $CI->logged->get_login_session('user_id');
		
		if( is_array($params) ){
			extract($params);
		} else {
			$description = $params;
		}
		$user_id = $user_id?$user_id:$logged_user_id;
		
		$data = array(
			$this->model_user->tblid=>$user_id,
			$this->model_user_logs->tblpref.'action'=>$action,
			$this->model_user_logs->tblpref.'description'=>$description
		);
		
		$id = $this->Data->insert_data( $this->model_user_logs, $data );
		
		return $id;
	}

	public function get_data_view( $data ){
		$CI =& get_instance();
		
		$id = $CI->input->get('id');
		$data['id'] = $id;
		
		$data['fields'] = $this->record( $id );
		
		return $data;
	}
	
	public function get_data_records_logged_users( $data ){
		$CI =& get_instance();
				
		$logged_user_id = $CI->logged->get_login_session('user_id');
		
		$where_params = array($this->model_user->tblname.'.'.$this->model_user->tblid=>$logged_user_id);
		$data = $this->records($data, $where_params);
		
		return $data;
	}
	
	public function get_data_view_logged_user( $data ){
		$CI =& get_instance();
		
		$id = $CI->input->get('id');
		$data['id'] = $id;
		
		$logged_user_id = $CI->logged->get_login_session('user_id');
		$where_params = array(
			$this->model->tblname.'.'.$this->model->tblid=>$id,
			$this->model_user->tblname.'.'.$this->model_user->tblid=>$logged_user_id
		);
		
		$fields = $this->record( $where_params );
		$data['fields'] = $fields;
		
		return $data;
	}
}
?>