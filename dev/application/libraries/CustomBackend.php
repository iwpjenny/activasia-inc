<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CustomBackend {

    public function __construct( $params=array() ){
        
    }

	public function set(){
        $CI =& get_instance();
		
	}

	public function set_libaries( $model=NULL ){
        $CI =& get_instance();
				
	}
	
	
	public function get_ca_pending_count(){
        $CI =& get_instance();
		$CI->load->model('modules/Cash_Advance_Model', 'CA'); 
	 	$count = $CI->CA->get_pending_count();  
		   
		return $count;
				
	}
	public function get_ca_release_count(){
        $CI =& get_instance();
		$CI->load->model('modules/Cash_Advance_Model', 'CA'); 
	 	$count = $CI->CA->get_release_count();  
		   
		return $count;
				
	}
	
	public function display_ca_action(){
		$return = array();
        $CI =& get_instance();
		$CI->load->model('modules/Cash_Advance_Model', 'CA');  
		
		$return['show_status_approve_button'] = $CI->usersrole->check( $CI->CA->urc_name, 'approve');
		$return['show_status_decline_button'] = $CI->usersrole->check( $CI->CA->urc_name, 'decline');
		$return['show_status_pending_button'] = $CI->usersrole->check( $CI->CA->urc_name, 'pending');
		$return['show_status_release_button'] = $CI->usersrole->check( $CI->CA->urc_name, 'release');
		$return['show_status_notification'] = $CI->usersrole->check( $CI->CA->urc_name, 'notify');
		   
		return $return;
				
	}
}
?>