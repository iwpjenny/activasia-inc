<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModuleLogs {
	
	private $Data;
	private $params;
	private $model;
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	public $notification;
	
	/* parameters */

    function __construct( $params=array() ){
        $CI =& get_instance();
		
		$this->params = $params;
		
		$this->Data = $CI->Data;
    }

    function __destruct(){
        $this->Data = NULL;
		
		$this->model = NULL;
    }
	
	public function create( $params=array() ){
		$CI =& get_instance();
		
		$params = $params ? $params: $this->params;
		extract($params);
		
		$logged_user_id = $CI->logged->get_login_session('user_id');
		$table_name = $model->tblname.'_logs';
		
		$this->create_table( $model, $table_name );
		
		$table_exists = $CI->db->table_exists( $table_name );
		
		$result = FALSE;
		if( $table_exists ){
			if( isset($id) == FALSE ){
				$post_id = $CI->input->post('id');
				$get_id = $CI->input->get('id');
				$id = $post_id ? $post_id : $get_id;
			}
			
			$main_data = (array) $this->Data->get_record( $model, array('where_params'=>array($model->tblid=>$id)) );
			
			$log_data = array(
				$model->tblpref.'logs_user_id'=>$logged_user_id,
				$model->tblpref.'logs_type'=>$type,
				$model->tblpref.'logs_created'=>app_cdt()
			);			
			
			$data = array_merge($main_data, $log_data);
			$CI->db->insert($table_name, $data); 
			$result = $CI->db->insert_id();
		}
			
		return $result;
	}
	
	private function create_table( $model, $table_name ){
		$CI =& get_instance();		
		
		$table_exists = $CI->db->table_exists( $table_name );
		
		if( $table_exists == FALSE ){
			
			$field_attributes = $CI->db->field_data( $model->tblname );		

			$fields = array();
			$field_standard_array = array();
			$field_custom_array = array();
			$NULL = FALSE;
			$primary_field = $model->tblpref.'logs_id';
			
			$field_standard_array = array(
				$model->tblpref.'logs_id'=>array('type'=>'INT','constraint'=>11,'auto_increment'=>TRUE,'null'=>$NULL),
				$model->tblpref.'logs_user_id'=>array('type'=>'INT','constraint'=>11,'unsigned'=>FALSE,'null'=>$NULL),
				$model->tblpref.'logs_type'=>array('type'=>'CHAR','constraint'=>30,'null'=>$NULL),
				$model->tblpref.'logs_created'=>array('type'=>'DATETIME','null'=>$NULL)
			);
			
			if( $field_attributes ){
				foreach($field_attributes as $val){
					
					$type = strtoupper($val->type);
					$name = $val->name;
					$max_length = $val->max_length;
					$default = $val->default;
					
					if( in_array($type,array('INT','INTEGER','TINYINT','SMALLINT','MEDIUMINT','BIGINT'))  ){
						$field_custom_array[$name] = array(
							'type'=> $type,
							'constraint'=> $max_length,
							'auto_increment'=> FALSE,
							'default'=> $default,
							'unsigned'=> FALSE,
							'null'=>$NULL
						);
					} elseif( in_array($type,array('DATE','TIME','DATETIME','TIMESTAMP')) ){
						if( $type == 'TIMESTAMP' ){
							$type = 'DATETIME';
							$default = NULL;
						}
						$field_custom_array[$name] = array(
							'type'=> $type,
							'default'=> $default,
							'null'=>$NULL
						);
					} else {
						$field_custom_array[$name] = array(
							'type'=> $type,
							'constraint'=> $max_length,
							'default'=> $default,
							'null'=>$NULL
						);
					}
				}
			}
			
			$fields = array_merge($field_standard_array, $field_custom_array);
			
			$params = array('primary_field'=>$primary_field);
			$CI->load->library('dbtable',$params);
			$CI->dbtable->create( $table_name, $fields );
		}
	}
}
?>