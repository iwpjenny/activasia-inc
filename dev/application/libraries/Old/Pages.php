<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages {
	
	private $model;
	private $location;
	private $login_user_type;

    public function __construct( $params=array() ){
        
    }

	public function html_header(){
        $CI =& get_instance();
		
		$login_user_type = $CI->logged->get_login_user_type();
		
		if( $login_user_type == $CI->BLog->login_user_type ){
			$header = $CI->BLog->location.'/header-page-logged';
		} elseif( $login_user_type == $CI->FLog->login_user_type ){
			$header = $CI->FLog->location.'/header-page-logged';
		} else {
			$header = 'header';
		}
		
		return $header;
	}
}
?>