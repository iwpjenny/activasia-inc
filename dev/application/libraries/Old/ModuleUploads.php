<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModuleUploads {
	
	private $settings;
	private $Data;
	private $model = NULL;
	private $model_user = NULL;
	private $model_log = NULL;
	private $location;
	private $logged_user_only = FALSE;
	
	private $folder;
	private $base_path;
	private $dir_root;
	private $dir_perms;
	private $default_pic;
	private $fieldname;
	private $width;
	private $height;
	private $default_image_url_fullpath;
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	public $notification;
	
	/* parameters */
	public $redirect = TRUE;
	public $field_data = array();
	public $primary_field = 'title';

    function __construct( $params=array() ){
        $CI =& get_instance();
		
		$params_default = array_merge(array(
			'default_image'=>'image-default.jpg',
			'folder'=>'modules',
			'fieldname'=>'files',
			'base_path'=>str_replace('\\','/',FCPATH),
			'dir_root'=>'uploads',
			'dir_perms'=>DIR_WRITE_MODE,
			'model'=>NULL,
			'model_user'=>NULL,
			'model_log'=>NULL,
			'width'=>'300px',
			'height'=>'300px'
			), $params
		);
		extract($params_default);
		
		$this->model = $model;
		$this->model_user = $model_user;		
		$this->model_log = $model_log;		
		$this->folder = $folder;
		$this->default_image = $default_image;
		$this->fieldname = $fieldname;
		$this->base_path = $base_path;
		$this->dir_root = $dir_root;
		$this->dir_perms = $dir_perms;
		$this->width = $width;
		$this->height = $height;		
		$this->default_image_url_fullpath = base_url($this->dir_root.'/'.$this->default_image);
		
		$CI->load->model('Data_Model','Data');		
		$this->Data = $CI->Data;
    }

    function __destruct(){
        $this->Data = NULL;
		
		$this->dashboard_page = NULL;
		$this->model = NULL;
		$this->model_user = NULL;
		$this->model_log = NULL;
		$this->field_data = NULL;
    }
	
	public function record( $where_params_or_id ){
        $CI =& get_instance();
		
		if( is_array($where_params_or_id) ){
			$where_params = $where_params_or_id; 
		} else {
			$where_params = array( $this->model->tblname.'.'.$this->model->tblid => $where_params_or_id ); 
		}
		
		$join_params = $this->model->set_join_params(); 
		$fields = $this->Data->get_record( $this->model, array(
			'where_params'=>$where_params,
			'join_params'=>$join_params
		));
		
		return $fields;
	}
	
	public function records( $data, $where_params=array() ){ 
        $CI =& get_instance();
		
		$search_fields = $this->model->set_search_fields(); 
		$join_params = $this->model->set_join_params();
		$orderby = method_exists($this->model,'set_order_by')?$this->model->set_order_by():NULL;
		
		$query_params = array(
			'join_params'=>$join_params,
			'search_fields'=> $search_fields,
			'where_params'=> $where_params,
			'orderby'=> $orderby,
			'ordertype'=>'DESC',
			'paging'=>TRUE
		);
		$records = $this->Data->get_records( $this->model, $query_params ); 
		$data['record_rows'] = $records;
		$data['pagination'] =  $this->Data->pagination_link; 
		$data['per_page'] = $this->Data->per_page;
		$data['total_rows'] = $this->Data->total_rows;
		
		return $data;
	}
	
	public function save_data( $model, $field_data, $id_or_params='' ){
		$CI =& get_instance();
		
		$data = array_merge($field_data, $this->field_data);
				
		if( $id_or_params ){
			if( is_array($id_or_params) ){
				$result = $this->Data->update_data( $model, $data, $id_or_params );
			} else {
				$result = $this->Data->update_data( $model, $data, array( $model->tblid=>$id_or_params ) );
			}
		} else {
			$result = $this->Data->insert_data( $model, $data );
		}
		
		$this->field_data = array();
		
		return $result;
	}
	
	public function save(){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$model = $this->model;
		$page = $model->page;
		/*---------------------------------------------*/
		
		$id = $CI->input->post('id'); 
		$field_data = $CI->input->post($model->tblname);
		
		$field_data = $this->logged_user_only( $field_data );
		
		if( $id ){
			$CI->usersrole->user_has_capability($model->urc_name,'edit');
			
			$save = $this->save_data( $model, $field_data, $id );
			
			if( $save ){
				$details = $this->record( $id );
				$name = get_value($details, $model->tblpref.$this->primary_field);
								
				$notify_params = array('action'=>'update','type'=>'success','log'=>TRUE);
				$CI->fields->unset_session();
			} else {
				$notify_params = array('action'=>'update','type'=>'error','log'=>TRUE);
			}
			$CI->notify->set( $notify_params );
			
			redirect_current_page();
		} else {
			$CI->usersrole->user_has_capability($model->urc_name,__FUNCTION__);
			
			$id = $this->save_data( $model, $field_data );
			
			if( $id ){
			
				$details = $this->record( $id );
				$name = get_value($details,$model->tblpref.$this->primary_field);
				
				$notify_params = array('action'=>'add','type'=>'success','log'=>TRUE);
				$CI->notify->set( $notify_params );
				$CI->fields->unset_session();
				
				redirect( site_url( 'c='.$page.'&m=edit&id='.$id ) );
			} else {
				$notify_params = array('action'=>'add','type'=>'error','log'=>TRUE);
				$CI->notify->set( $notify_params );
				
				redirect_current_page();
			}					
		}
	}
	
	public function delete(){
		$CI =& get_instance();
		$CI->load->library('files');
		/*---------------------------------------------*/
		$model = $this->model;
		$page = $model->page;
		/*---------------------------------------------*/
		
		$CI->usersrole->user_has_capability($model->urc_name,__FUNCTION__);
		
		$id = $CI->input->get('id');
		
		$details = $this->record( $id ); 
		
		if( $details ){
			$name = get_value( $details, $model->tblpref.$this->primary_field);
			$filepath = get_value( $details, $model->tblpref.'filepath');
			
			$delete = $this->Data->delete_data( $model, array($model->tblid=>$id) );
			
			if( $delete ){
				$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE,'text'=>'ID:'.$id.' - '.$name);
				$CI->notify->set( $notify_params );
			
				$delete_file = $CI->files->delete_file( $filepath, TRUE );
				
				if( $delete_file ){
					$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE);
					$CI->notify->set( $notify_params );
				} else {
					$notify_params = array('action'=>'file','type'=>'delete-error','log'=>TRUE);
					$CI->notify->set( $notify_params );
				}
			} else {
				$notify_params = array('action'=>'file','type'=>'delete-error','log'=>TRUE);
				$CI->notify->set( $notify_params );
			}
		} else {
			$notify_params = array('action'=>'record','type'=>'not-exist','log'=>TRUE);
			$CI->notify->set( $notify_params );
		}
		
		redirect( site_url('c='.$page) );
	}
	
	private function serialize_array_data( $field_data ){
		$CI =& get_instance();
		
		$new_field_data = $field_data;
		if( $field_data ){
			foreach($field_data as $key=>$value){
				if( is_array($value) ){
					$value = serialize($value);
				}
				$new_field_data[$key] = $value;
			}
		}
		
		return $new_field_data;
	}
	
	private function logged_user_only( $field_data ){
		$CI =& get_instance();
		
		if( $this->logged_user_only ){
			$logged_user_id = $CI->logged->get_login_session('user_id');
			$field_data = array_merge($field_data,array($this->model_user->tblid=>$logged_user_id));
		}
		
		return $field_data;
	}
	
	public function get_data_records( $data ){
		$CI =& get_instance();
		
		$where_params = array($this->model->tblpref.'module_type'=>$this->model->page);
		
		if( $this->logged_user_only ){		
			$logged_user_id = $CI->logged->get_login_session('user_id');	
			$where_params = array_merge($where_params,array($this->model_user->tblname.'.'.$this->model_user->tblid=>$logged_user_id));
		}
		
		$data = $this->records( $data, $where_params );
		
		return $data;
	}
	
	public function get_data_view( $data ){
		$CI =& get_instance();
			
		$id = $CI->input->get('id');
		$data['id'] = $id;
		
		if( $this->logged_user_only ){		
			$logged_user_id = $CI->logged->get_login_session('user_id');
			
			$where_params_or_id = array(
				$this->model->tblid=>$id,
				$this->model->tblpref.'module_type'=>$this->model->page,
				$this->model_user->tblname.'.'.$this->model_user->tblid=>$logged_user_id
			);
		} else {
			$where_params_or_id = array(
				$this->model->tblid=>$id,
				$this->model->tblpref.'module_type'=>$this->model->page
			);
		}
		
		$fields = $this->record( $where_params_or_id );
		$data['fields'] = $fields;
		
		return $data;
	}
	
	public function get_data_edit( $data ){
		$CI =& get_instance();
		
		$data = $this->get_data_view( $data );
		
		$CI->load->library('ModuleTag',array('model'=>$this->model,'data'=>$data['fields']));
		
		return $data;
	}
	
	private function create_folders(){
		
		$upload_path = $this->base_path.$this->dir_root.'/';
		$upload_path_files = $upload_path.$this->folder.'/';
		$upload_path_module = $upload_path_files.$this->model->folder.'/';
		
		if(is_dir($upload_path) == FALSE){
			mkdir($upload_path, $this->dir_perms, TRUE);
		}
		if(is_dir($upload_path_files) == FALSE){
			mkdir($upload_path_files, $this->dir_perms, TRUE);
		}
		if(is_dir($upload_path_module) == FALSE){
			mkdir($upload_path_module, $this->dir_perms, TRUE);
		}
		
		return $upload_path_module;
	}
			
	public function upload(){
		$CI =& get_instance();
		
		$result = FALSE;
		$uploaded_array = array();
		
		if( isset($_FILES[$this->fieldname]['name']) && $_FILES[$this->fieldname]['name'] != '' ){
			
			$dist_dir = $this->create_folders();
				
			$config = array(
				'fieldname'=>$this->fieldname,
				'dist_dir'=>$dist_dir
			);
			$CI->load->library('FileUpload', $config);
			
			$upload = $CI->fileupload->upload();
			$upload_filepath = $CI->fileupload->upload_filepath;
			$filename_new = $CI->fileupload->filename_new;
			$filename_old = $CI->fileupload->filename_old;
			$file_title = $CI->fileupload->file_title;
			$file_type = $CI->fileupload->file_type;
			$this->error = $CI->fileupload->error;
			
			if ( $upload ){	
			
				$logged_user_id = $CI->logged->get_login_session('user_id');
				
				foreach($upload as $key=>$upload_filepath){
					$title = $file_title[$key];
					$type = $file_type[$key];
					$file_old = $filename_old[$key];
					$module_type = $this->model->page;
					
					$id = $CI->Data->insert_data($this->model, array(
						$this->model_user->tblid => $logged_user_id,
						$this->model->tblpref.'module_type' => $module_type,
						$this->model->tblpref.'filepath' => $upload_filepath,
						$this->model->tblpref.'type' => $type,
						$this->model->tblpref.'title' => $title
					));
					
					$result[] = array(
						'id'=>$id,
						'module_type'=>$module_type,
						'filepath'=>$upload_filepath,
						'type'=>$type,
						'title'=>$title
					);
					
					$notify_params = array('action'=>'file','type'=>'upload-success','log'=>TRUE);					
					$CI->notify->set( $notify_params );
				}
			}
		}
		
		if( $this->redirect === TRUE ){
			redirect_current_page();
		} else {
			return $result;
		}
	}
			
	public function get_upload(){
		
		$this->redirect = FALSE;
		$result = $this->upload();
		
		return $result;
	}
			
	public function download(){
		$CI =& get_instance();		
		
		$CI->load->library('files');
		$CI->load->helper('download');
		
		$id = $CI->input->get('id');
		$details = $CI->moduleuploads->record( $id );
		$filepath = app_get_val($details, $this->model->tblpref.'filepath');
		
		$file_pathinfo = pathinfo($filepath);
		$basename = app_get_val($file_pathinfo,'basename');
		
		$full_filepath = $CI->files->set_full_filepath( $filepath );
		$file_content = file_get_contents($full_filepath);
		
		force_download($basename, $file_content, TRUE);
	}
	
	public function field(){
		
		$result = form_upload(array('name'=>$this->fieldname,'accept'=>'file_extension|audio/*|video/*|image/*|media_type'));
		
		echo $result;
	}
	
	public function field_multiple(){
		
		$result = form_upload(array('name'=>$this->fieldname.'[]','multiple'=>'multiple','accept'=>'file_extension|audio/*|video/*|image/*|media_type'));
		
		echo $result;
	}
}
?>