<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RecordsFilter {
	
	private $CI;
	private $Data;
	
	/* return */
	public $error;
	public $result;
	public $notification;
	
	/* parameters */
	public $model;
	public $form_action;

    function __construct( $params=array() ){
        $CI =& get_instance();
		
		$params_default = array_merge(array(
			'model'=>NULL,
			'form_action'=>NULL
			), $params
		);
		extract($params_default);
		
		$this->model = $model;
		$this->form_action = $form_action?$form_action:site_url('c='.$this->model->page);
		
		$CI->load->model('Data_Model','Data');
		
		$this->Data = $CI->Data;
		$this->CI = $CI;
    }

    function __destruct(){
        $this->Data = NULL;
        $this->error = NULL;		
		$this->result = NULL;		
    }
	
	public function show(){
		
		$page = $this->model->page;
		
		$month = $this->CI->input->get('month');
		$month = $month?$month:date('m');
		$year = $this->CI->input->get('year');
		$year = $year?$year:date('Y');
		
		$start_date = $this->CI->input->get('start_date');
		$start_date = $start_date?$start_date:date('Y-m-d');
		$end_date = $this->CI->input->get('end_date');
		$end_date = $end_date?$end_date:$start_date;
		
		$filter_type = $this->CI->input->get('filter_type');
		$keyword = $this->CI->input->get('keyword');
		$ordertype = $this->CI->input->get('ordertype');
		$ordertype = $ordertype?$ordertype:'DESC';
		$per_page = $this->CI->input->get('per_page');
		 
		$select_month = app_select_month( $month );
		$select_year = app_select_year( $year );
		$select_ordertype = app_select_ordertype( $ordertype );
		//$select_ordertype = app_radio_button_ordertype( $ordertype );
				
		ob_start();
		?>
		<script type="text/javascript">
		$(document).ready(function(){
			var form = $("form#records-filter");			
			<?php
			/* if( $filter_type == 'month-year' ){
			?>
			$("select[name=month]",form).removeAttr('disabled');
			$("select[name=year]",form).removeAttr('disabled');
			<?php
			} else {
			?>
			$("select[name=month]",form).attr('disabled','disabled');
			$("select[name=year]",form).attr('disabled','disabled');
			<?php
			}
			if( $filter_type == 'date-range' ){
			?>
			$("input[name=start_date]",form).removeAttr('disabled');
			$("input[name=end_date]",form).removeAttr('disabled');
			<?php
			} else {
			?>
			$("input[name=start_date]",form).attr('disabled','disabled');
			$("input[name=end_date]",form).attr('disabled','disabled');
			<?php
			} */
			/* if( $filter_type ){
			?>
			$("select[name=ordertype]",form).removeAttr('disabled');
			<?php
			} else {
			?>
			$("select[name=ordertype]",form).attr('disabled','disabled');
			<?php
			} */
			?>			
			$("input[name=filter_type]",form).on("click",function(){
				var filter_type = $(this).val();
				
				if( filter_type == "date-range" ){
					$("select[name=month]",form).attr('disabled','disabled');
					$("select[name=year]",form).attr('disabled','disabled');
					$("input[name=start_date]",form).removeAttr('disabled');
					$("input[name=end_date]",form).removeAttr('disabled');
				}
				if( filter_type == "month-year" ){
					$("select[name=month]",form).removeAttr('disabled');
					$("select[name=year]",form).removeAttr('disabled');
					$("input[name=start_date]",form).attr('disabled','disabled');
					$("input[name=end_date]",form).attr('disabled','disabled');
				}
				//$("select[name=ordertype]",form).removeAttr('disabled');
			});
		});
		</script>
		<div class="row">
			<div class="col-md-6">
				<form action="<?php echo $this->form_action; ?>" method="get" name="search" class="form-horizontal">
					<input type="hidden" name="c" value="<?php echo $page; ?>" />
					<div class="form-group">
						<div class="col-md-12">
							<label>Search Records:</label>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-5">
							<input type="text" name="keyword" class="form-control input-sm" placeholder="Type Keyword to Search..." value="<?php echo $keyword; ?>"  />
						</div>
						<div class="col-md-3">
							<?php echo $select_ordertype; ?>
						</div>
						<div class="col-md-4">
							<button type="submit" class="btn btn-primary btn-sm">Search</button>
							<a href="<?php echo site_url('c='.$page); ?>" class="btn btn-default btn-sm">Reset</a>
						</div>
					</div>
					<!-- <input type="hidden" name="per_page" value="<?php echo $per_page; ?>" /> -->
				</form>
			</div>
			<div class="col-md-6">
				<form action="<?php echo $this->form_action; ?>" method="get" name="records-filter" id="records-filter" class="form-horizontal">
					<input type="hidden" name="c" value="<?php echo $page; ?>" />
					<div class="form-group">
						<div class="col-md-12">
							<label>Filter Records:</label>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-3">
							<input type="text" name="start_date" value="<?php echo $start_date; ?>" class="form-control datepicker input-sm" placeholder="Start Date" readonly="readonly" />
						</div>
						<div class="col-md-3">
							<input type="text" name="end_date" value="<?php echo $end_date; ?>" class="form-control datepicker input-sm" placeholder="End Date" readonly="readonly" />
						</div> 
						<div class="col-md-3">
							<?php echo $select_ordertype; ?>
						</div>
						<div class="col-md-3">
							<input type="submit" class="btn btn-sm btn-primary" value="Filter" />
							<a href="<?php echo site_url('c='.$page); ?>" class="btn btn-default btn-sm">Reset</a>
						</div>
					</div>
					<input type="hidden" name="filter_type" value="date-range" />
					<input type="hidden" name="per_page" value="<?php echo $per_page; ?>" />
				</form>
			</div>
			<?php /* ?>
			<form method="get" action="<?php echo $this->form_action; ?>" name="records-filter" id="records-filter" class="form-horizontal">
				<div class="form-group">
					<div class="col-md-3">
						<input type="submit" class="btn btn-sm btn-default" value="Filter" />
						<a href="<?php echo site_url('c='.$page); ?>" class="btn btn-default btn-sm">Reset</a>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-2">
						<?php echo $select_month; ?>
					</div>
					<div class="col-md-2">
						<?php echo $select_year; ?>	
					</div>
					<div class="col-md-1">
						<input type="text" name="start_date" value="<?php echo $start_date; ?>" class="form-control datepicker input-sm" placeholder="Start Date" readonly="readonly" />
					</div>
					<div class="col-md-1">
						<input type="text" name="end_date" value="<?php echo $end_date; ?>" class="form-control datepicker input-sm" placeholder="End Date" readonly="readonly" />
					</div> 
					<div class="col-md-3">
						<?php echo $select_ordertype; ?>
					</div>
					<div class="col-md-3">
						Filter Type:
						<label><input type="radio" name="filter_type" value="month-year" <?php app_checked($filter_type,array('month-year','')); ?> required="required" /> Month and Year</label>
						<label><input type="radio" name="filter_type" value="date-range" <?php app_checked($filter_type,'date-range'); ?> required="required" /> Date Range</label>
					</div>
				</div>
				<input type="hidden" name="c" value="<?php echo $page; ?>" />
				<input type="hidden" name="per_page" value="<?php echo $per_page; ?>" />
			</form>
			<?php */ ?>
		</div>
		<?php
		$contents = ob_get_contents();
		ob_end_clean();

		echo $contents;
	}
}
?>