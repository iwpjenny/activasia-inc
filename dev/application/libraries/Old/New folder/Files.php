<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Files {
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	
	/* parameters */
	public $id;
	public $folder;
	public $base_path;
	public $dir_root;
	public $dir_perms;
	public $default_pic;
	public $fieldname;
	public $model_main;
	public $model_files;
	public $width;
	public $height;
	public $default_image_url_fullpath;

    function __construct( $params=array() ){
		
		$params_default = array_merge(array(
			'id'=>NULL,
			'default_image'=>'image-default.jpg',
			'folder'=>'modules',
			'fieldname'=>'files',
			'base_path'=>str_replace('\\','/',FCPATH),
			'dir_root'=>'uploads',
			'dir_perms'=>DIR_WRITE_MODE,
			'model'=>NULL,
			'model_main'=>NULL,
			'model_files'=>NULL,
			'width'=>'300px',
			'height'=>'300px'
			), $params
		);
		extract($params_default);
		
		if( $params ){
			$this->id = $id;
			$this->model = $model;
			$this->model_main = $model_main;
			$this->model_files = $model_files;
		}		
		
		$this->folder = $folder;
		$this->default_image = $default_image;
		$this->fieldname = $fieldname;
		$this->base_path = $base_path;
		$this->dir_root = $dir_root;
		$this->dir_perms = $dir_perms;
		$this->width = $width;
		$this->height = $height;		
		$this->default_image_url_fullpath = base_url($this->dir_root.'/'.$this->default_image);
    }

    function __destruct(){
		$this->trigger_before = NULL;
		$this->trigger_after = NULL;
		$this->trigger_on_success = NULL;
		$this->trigger_on_error = NULL;
		$this->error = NULL;
		
        $this->id = NULL;
        $this->folder = NULL;
        $this->model_main = NULL;
        $this->model_files = NULL;
		
        $this->default_image = NULL;
        $this->fieldname = NULL;
        $this->dir_root = NULL;
        $this->dir_perms = NULL;
        $this->width = NULL;
        $this->height = NULL;
    }
	
	private function trigger( $triggers=array() ){
		
		$result = FALSE;
		
		if( array_key_exists(0,$triggers) ){
			foreach($triggers as $trigger){
				$result[] = $this->get_trigger( $trigger );
			}
		} else {
			$result = $this->get_trigger( $triggers );
		}
		
		return $result;
	}
	
	private function get_trigger( $trigger ){
		
		$result = FALSE;
		
		if( is_array($trigger) && $trigger ){
			extract($trigger);
			
			if( isset($params) ){
				if( is_object($obj) ){
					$result = $obj->$func( $params );
				} else {
					$result = $func( $params );
				}
			} else {
				if( is_object($obj) ){
					$result = $obj->$func();
				} else {
					$result = $func();
				}
			}
		}
		
		return $result;
	}
	
	private function create_user_folders(){
		
		$upload_path = $this->base_path.$this->dir_root.'/';
		$upload_path_files = $upload_path.$this->folder.'/';
		$upload_path_module = $upload_path_files.$this->model_main->tblname.'/';
		
		if(is_dir($upload_path) == FALSE){
			mkdir($upload_path, $this->dir_perms, TRUE);
		}
		if(is_dir($upload_path_files) == FALSE){
			mkdir($upload_path_files, $this->dir_perms, TRUE);
		}
		if(is_dir($upload_path_module) == FALSE){
			mkdir($upload_path_module, $this->dir_perms, TRUE);
		}
		
		return $upload_path_module;
	}
	
	/* private function create_table(){
		$CI =& get_instance();
		
		$table_name = $this->model_files->tblname;
		$primary_field = $this->model_files->tblid;
		$prefix = $this->model_files->tblpref;
		$foreign_key = $this->model_main->tblid;
		$NULL = FALSE;
		
		$fields = array(
			$primary_field=>array('type'=>'INT','constraint'=>11,'auto_increment'=>TRUE,'null'=>$NULL),
			$foreign_key=>array('type'=>'INT','constraint'=>11,'unsigned'=>FALSE,'null'=>$NULL),
			$prefix.'filepath'=>array('type'=>'VARCHAR','constraint'=>255,'null'=>$NULL),
			$prefix.'type'=>array('type'=>'CHAR','constraint'=>20,'null'=>$NULL),
			$prefix.'title'=>array('type'=>'VARCHAR','constraint'=>100,'null'=>$NULL),
			$prefix.'description'=>array('type'=>'VARCHAR','constraint'=>255,'null'=>$NULL),
			$prefix.'created'=>array('type'=>'DATETIME','null'=>$NULL),
			$prefix.'modified'=>array('type'=>'TIMESTAMP','default'=>'CURRENT_TIMESTAMP','null'=>$NULL),
			$prefix.'published'=>array('type'=>'TINYINT','default'=>1,'null'=>$NULL)
		);
		$params = array('primary_field'=>$primary_field);
		$CI->load->library('dbtable',$params);
		
		$result = $CI->dbtable->create( $table_name, $fields );
		
		return $result;
	} */
	
	public function upload(){
		$CI =& get_instance();
		
		$result = FALSE;
		
		if( isset($_FILES[$this->fieldname]['name']) && $_FILES[$this->fieldname]['name'] != '' ){
			
			$this->trigger( $this->trigger_before );
			
			$dist_dir = $this->create_user_folders();
										
			$config = array(
				'fieldname'=>$this->fieldname,
				'dist_dir'=>$dist_dir
			);
			$CI->load->library('FileUpload', $config);
			
			$upload = $CI->fileupload->upload();
			$upload_filepath = $CI->fileupload->upload_filepath;
			$filename_new = $CI->fileupload->filename_new;
			$filename_old = $CI->fileupload->filename_old;
			$file_title = $CI->fileupload->file_title;
			$file_type = $CI->fileupload->file_type;
			$this->error = $CI->fileupload->error;
			
			if ( $upload ){				
				foreach($upload as $key=>$upload_filepath){
					$title = $file_title[$key];
					$type = $file_type[$key];
					$file_old = $filename_old[$key];
					
					$result = $CI->Data->insert_data($this->model_files, array(
						$this->model_main->tblid => $this->id,
						$this->model_files->tblpref.'filepath' => $upload_filepath,
						$this->model_files->tblpref.'type' => $type,
						$this->model_files->tblpref.'title' => $title,
						$this->model_files->tblpref.'description' => $title
					));
					
					$notify_params = array('action'=>'file','type'=>'upload-success','log'=>TRUE);					
					$CI->notify->set( $notify_params );
				}
				
				$this->trigger( $this->trigger_on_success );
			} else {
				//$notify_params = array('action'=>'file','type'=>'upload-error','log'=>TRUE,'text'=>$this->error);			
				//$CI->notify->set( $notify_params ); //Temporarily Commented 
				
				$this->trigger( $this->trigger_on_error );
			}
			
			$this->trigger( $this->trigger_after );
		}
		
		return $result;
	}
	
	public function field(){
		
		$result = form_upload(array('name'=>$this->fieldname.'[]','multiple'=>'multiple','accept'=>'file_extension|audio/*|video/*|image/*|media_type'));
		
		echo $result;
	}
	
	public function get_fileinfo($filepath, $key='filename'){
		
		$fileinfo = pathinfo($filepath);
		$result = $fileinfo[$key];
		
		return $result;
	}
	
	public function record( $query_params_or_fid ){
		$CI =& get_instance();
		$CI->load->model('Data_Model','Data');
		
		if( is_array($query_params_or_fid) ){
			$query_params = $query_params_or_fid;
		} else {
			$query_params = array('where_params'=>array($this->model_files->tblid=>$query_params_or_fid));
		}
		
		$result = $CI->Data->get_record($this->model_files, $query_params);
		
		return $result;
	}
	
	public function records( $query_params_or_mid ){
		$CI =& get_instance();
		$CI->load->model('Data_Model','Data');
		
		if( is_array($query_params_or_mid) ){
			$query_params = $query_params_or_mid;
		} else {
			$query_params = array('where_params'=>array($this->model_main->tblid=>$query_params_or_mid));
		}
		
		$result = $CI->Data->get_records($this->model_files, $query_params);
		
		return $result;
	}
	
	public function record_grid_thumbnail( $Model, $id ){
		$CI =& get_instance();		
		
		$query_params = array('where_params'=>array($Model->tblid=>$id));
		$params = array('fancybox'=>TRUE,'attr'=>'style="width:60px;height:60px;" class="img-thumbnail"');
		
		$image = $this->image( $query_params, $params );
		
		return $image;
	}
	
	public function image( $query_params_or_fid, $params=array() ){
		$CI =& get_instance();
		$CI->load->model('Data_Model','Data');
		
		$params_default = array_merge(array(
			'thumbnail'=>FALSE,
			'fancybox'=>FALSE,
			'attr'=>''
			), $params
		);
		extract($params_default);
		
		$prefix = $this->model_files->tblpref;
		
		$image = $this->record( $query_params_or_fid );
		
		$result = '';
		if( $image ){
			$fid = get_value($image,$this->model_files->tblid);
			$filepath = get_value($image,$prefix.'filepath');
			$title = get_value($image,$prefix.'title');
			
			$src = base_url($filepath);
			
			if( $fancybox === TRUE ){
				$result .= '<a href="'.$src.'" class="fancybox" rel="gridimage" title="'.$title.'">';
				$result .= '<img src="'.$src.'" alt="'.$filepath.'" '.$attr.' />';
				$result .= '</a>';
			} else {
				$result = '<img src="'.$src.'" alt="'.$filepath.'" />';
			}
		} else {
			$src = $this->default_image_url_fullpath;
			$result = '<img src="'.$src.'" alt="No Image" '.$attr.' />';
		}
		
		return $result;
	}
	
	public function images( $mid ){
		$CI =& get_instance();
		$CI->load->model('Data_Model','Data');
		
		$prefix = $this->model_files->tblpref;
		$images = $this->records( $mid );
		
		$result = '';
		if( $images ){
			foreach($images as $image){
				$fid = get_value($image,$this->model_files->tblid);
				$filepath = get_value($image,$prefix.'filepath');
				$title = get_value($image,$prefix.'title');
				$description = get_value($image,$prefix.'description');
				
				$src = base_url($filepath);
				
				/* $result .= '<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">';
				$result .= '<a href="'.$src.'" class="thumbnail fancybox" rel="group'.$this->id.'" title="'.$title.'">';
				$result .= '<img src="'.$src.'" alt="'.$filepath.'" />';
				$result .= '</a></div>'; */
				
				$delete_url = site_url('c='.$this->model_main->page.'&m=delfile&id='.$mid.'&fid='.$fid);
				
				$result .= '<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">';
				$result .= '<div class="thumbnail">';
				$result .= '<div class="caption">';
				$result .= '<a href="'.$delete_url.'" class="btn btn-danger btn-xs" role="button">Delete</a></div>';
				$result .= '<a href="'.$src.'" class="fancybox" rel="group'.$mid.'" title="'.$title.'">';
				$result .= '<img src="'.$src.'" alt="'.$filepath.'" />';
				$result .= '</a>';
				$result .= '</div>';
				$result .= '</div>';
			}
		}
		
		echo $result;
	}
	
	public function delete( $fid ){
		$CI =& get_instance();
		$CI->load->model('Data_Model','Data');
		
		$this->trigger( $this->trigger_before );
				
		$details = $this->record( $fid );
		
		$result = FALSE;		
				
		if( $details ){
			$filepath = get_value($details,$this->model_files->tblpref.'filepath');
			$full_filepath = $this->base_path.$filepath;
			$basename = $this->get_fileinfo($filepath,'basename');
			
			$delete_file = $this->delete_file( $full_filepath );
			
			$delete_data = $CI->Data->delete_data($this->model_files,
				array($this->model_files->tblid=>$fid)
			);
			
			if( $delete_file ){
				$notify_params = array('action'=>'file','type'=>'delete-success','log'=>TRUE,'text'=>'('.$basename.', ID:'.$fid.')');
				$CI->notify->set( $notify_params );
				
				$this->trigger( $this->trigger_on_success );
			} else {
				$notify_params = array('action'=>'file','type'=>'delete-error','log'=>TRUE,'text'=>'('.$basename.', ID:'.$fid.')');
				$CI->notify->set( $notify_params );
				$this->trigger( $this->trigger_on_error );
			}
			
			if( $delete_data ){
				$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE,'text'=>'('.$basename.', ID:'.$fid.')');
				$CI->notify->set( $notify_params );
				$this->trigger( $this->trigger_on_success );
			} else {
				$notify_params = array('action'=>'delete','type'=>'error','log'=>TRUE,'text'=>'('.$basename.', ID:'.$fid.')');
				$CI->notify->set( $notify_params );
				$this->trigger( $this->trigger_on_error );
			}
		} else {
			$notify_params = array('action'=>'record','type'=>'not-exist','log'=>TRUE,'text'=>'(ID:'.$fid.')');
			$CI->notify->set( $notify_params );
		}
		
		$this->trigger( $this->trigger_after );
		
		return $result;
	}
	
	public function delete_all( $mid ){
		$CI =& get_instance();
		$CI->load->model('Data_Model','Data');
		
		$this->trigger( $this->trigger_before );
				
		$records = $this->records( $mid );
		
		$result = FALSE;		
				
		if( $records ){
			foreach($records as $col){
				$fid = get_value($col,$this->model_files->tblid);
				$filepath = get_value($col,$this->model_files->tblpref.'filepath');
				$full_filepath = $this->base_path.$filepath;
				$basename = $this->get_fileinfo($filepath,'basename');
				
				$delete_file = $this->delete_file( $full_filepath );
							
				if( $delete_file ){
					$notify_params = array('action'=>'file','type'=>'delete-success','log'=>TRUE,'text'=>'('.$basename.', Module ID:'.$fid.')');
					$CI->notify->set( $notify_params );
				} else {
					$notify_params = array('action'=>'file','type'=>'delete-error','log'=>TRUE,'text'=>'('.$basename.', Module ID:'.$fid.')');
					$CI->notify->set( $notify_params );
				}
			}
			
			$delete_data = $CI->Data->delete_data($this->model_files,
				array($this->model_main->tblid=>$mid)
			);
				
			if( $delete_data ){
				$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE,'text'=>'(Module ID:'.$mid.')');
				$CI->notify->set( $notify_params );
				$this->trigger( $this->trigger_on_success );
				$result = TRUE;
			} else {
				$notify_params = array('action'=>'delete','type'=>'error','log'=>TRUE,'text'=>'(Module ID:'.$mid.')');
				$CI->notify->set( $notify_params );
				$this->trigger( $this->trigger_on_error );
			}
		} else {
			$notify_params = array('action'=>'record','type'=>'not-exist','log'=>TRUE,'text'=>'(Module ID:'.$mid.')');
			$CI->notify->set( $notify_params );
		}
		
		$this->trigger( $this->trigger_after );
		
		return $result;
	}
	
	public function set_full_filepath( $filepath ){
				
		$full_filepath = $this->base_path.$filepath;
		
		return $full_filepath;
	}
	
	public function delete_file( $filepath, $add_full_path=FALSE ){
				
		$result = FALSE;
		
		if( $add_full_path === TRUE ){
			$filepath = $this->set_full_filepath( $filepath );
		}
		
		if( file_exists($filepath) ){
			$result = unlink($filepath);
		}
		
		return $result;
	}
}
?>