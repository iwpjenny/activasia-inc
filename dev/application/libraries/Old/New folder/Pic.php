<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pic {
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	
	/* parameters */
	public $codeigniter = FALSE;
	public $id;
	public $default_pic;
	public $folder;
	public $fieldname;
	public $base_path;
	public $dir_root;
	public $dir_perms;
	public $model_main;
	public $model_prof;
	public $width;
	public $height;
	public $default_pic_url_fullpath;

    function __construct( $params=array() ){
		
		$params_default = array_merge(array(
			'id'=>NULL,
			'default_pic'=>'picture-default.jpg',
			'folder'=>'user',
			'fieldname'=>'picture',
			'base_path'=>str_replace('\\','/',FCPATH),
			'dir_root'=>'uploads',
			'dir_perms'=>DIR_WRITE_MODE,
			'model_main'=>NULL,
			'model_prof'=>NULL,
			'width'=>'300px',
			'height'=>'300px'
			), $params
		);
		extract($params_default);
		
		if( $params ){
			$this->id = $id;
			$this->folder = $folder;
			$this->model_main = $model_main;
			$this->model_prof = $model_prof;
		}
		
		$this->default_pic = $default_pic;
		$this->fieldname = $fieldname;
		$this->base_path = $base_path;
		$this->dir_root = $dir_root;
		$this->dir_perms = $dir_perms;
		$this->width = $width;
		$this->height = $height;
		
		$this->default_pic_url_fullpath = base_url($this->dir_root.'/'.$this->default_pic);
    }

    function __destruct(){
		$this->trigger_before = NULL;
		$this->trigger_after = NULL;
		$this->trigger_on_success = NULL;
		$this->trigger_on_error = NULL;
		$this->error = NULL;
		
        $this->id = NULL;
        $this->folder = NULL;
        $this->model_main = NULL;
        $this->model_prof = NULL;
		
        $this->default_pic = NULL;
        $this->fieldname = NULL;
        $this->dir_root = NULL;
        $this->dir_perms = NULL;
        $this->width = NULL;
        $this->height = NULL;
    }
	
	private function trigger( $triggers=array() ){
		
		$result = FALSE;
		
		if( array_key_exists(0,$triggers) ){
			foreach($triggers as $trigger){
				$result[] = $this->get_trigger( $trigger );
			}
		} else {
			$result = $this->get_trigger( $triggers );
		}
		
		return $result;
	}
	
	private function get_trigger( $trigger ){
		
		$result = FALSE;
		
		if( is_array($trigger) && $trigger ){
			extract($trigger);
			
			if( isset($params) ){
				if( is_object($obj) ){
					$result = $obj->$func( $params );
				} else {
					$result = $func( $params );
				}
			} else {
				if( is_object($obj) ){
					$result = $obj->$func();
				} else {
					$result = $func();
				}
			}
		}
		
		return $result;
	}
	
	private function create_user_folders(){
		
		$upload_path = $this->base_path.$this->dir_root.'/';
		$upload_path_user = $upload_path.$this->folder.'/';
		$upload_path_user_id = $upload_path_user.$this->folder.'-'.$this->id.'/';
		
		if(is_dir($upload_path) == FALSE){
			mkdir($upload_path, $this->dir_perms, TRUE);
		}
		if(is_dir($upload_path_user) == FALSE){
			mkdir($upload_path_user, $this->dir_perms, TRUE);
		}
		if(is_dir($upload_path_user_id) == FALSE){
			mkdir($upload_path_user_id, $this->dir_perms, TRUE);
		}
		
		return $upload_path_user_id;
	}
	
	public function upload(){
		$CI =& get_instance();
		$CI->load->library('notify');
		$CI->load->model('Data_Model','Data');
		
		$result = FALSE;
		
		if( isset($_FILES[$this->fieldname]['name']) && $_FILES[$this->fieldname]['name'] != '' ){
			
			$this->trigger( $this->trigger_before );
		
			$id = $this->delete();
			
			$dist_dir = $this->create_user_folders();
			
			if( $this->codeigniter ){
				
				$filename_new = 'profile-'.$this->id;
				
				$config = array(
					'upload_path'=>$dir_upload_path,
					'file_name'=>$filename_new,
					'overwrite'=>TRUE,
					'remove_spaces'=>TRUE,
					'file_ext_tolower'=>TRUE,
					'allowed_types'=>'jpg|jpeg|png',
					'max_size'=>0,
					'max_width'=>1024,
					'max_height'=>1024
				);
				$CI->load->library('upload',$config);
				$upload = $CI->upload->do_upload($this->fieldname);
				
				$details = $CI->upload->data();
				$filename_new = get_value($details,'client_name');
				$full_filepath_new = get_value($details,'full_path');
				
				$this->error = $CI->upload->display_errors();
			} else {
				$config = array(
					'fieldname'=>$this->fieldname,
					'dist_dir'=>$dist_dir
				);
				$CI->load->library('FileUpload', $config);
				
				$upload = $CI->fileupload->upload();
				$upload_filepath = $CI->fileupload->upload_filepath;
				$filename_new = $CI->fileupload->filename_new;
				$this->error = $CI->fileupload->error;
			}		
			
			if ( $upload ){				
				$result = $CI->Data->update_data($this->model_prof, 
					array($this->model_prof->tblpref.$this->fieldname => $upload_filepath), 
					array($this->model_prof->tblid => $id)
				);
				
				$notify_params = array('action'=>'picture','type'=>'upload-success','log'=>TRUE);
				$CI->notify->set( $notify_params );
				
				$this->trigger( $this->trigger_on_success );
			} else {
				
				$notify_params = array('action'=>'picture','type'=>'upload-error','log'=>TRUE,'text'=>$this->error);
				$CI->notify->set( $notify_params );
				
				$this->trigger( $this->trigger_on_error );
			}
			
			$this->trigger( $this->trigger_after );
		}
		
		return $result;
	}
	
	public function field(){
		
		$result = form_upload(array('name'=>$this->fieldname,'accept'=>'file_extension|image/*'));
		
		echo $result;
	}
	
	private function delete(){
		$CI =& get_instance();
				
 		$details = $CI->Data->get_record($this->model_prof, array('where_params'=>array($this->model_main->tblid=>$this->id)));
		
		$id = '';
		if( $details ){
			$id = get_value($details,$this->model_prof->tblid);
			$upload_filepath = get_value($details,$this->model_prof->tblpref.$this->fieldname);
			
			if( $upload_filepath ){
				$full_filepath = $this->base_path.$upload_filepath;
				if( file_exists($full_filepath) ){
					@unlink($full_filepath);
				}
			}
		}
		
		return $id;
	}
	
	public function get_img_by_id( $id ){
		$CI =& get_instance();
		$CI->load->model('Data_Model','Data');
		
		$result = '';
		
		$src = $this->default_pic_url_fullpath;
		
		$profile = $CI->Data->get_record($this->model_prof, array('where_params'=>array($this->model_main->tblid=>$id)));
		$profile_id = get_value($profile,$this->model_prof->tblid);
		$current_picture = get_value($profile,$this->model_prof->tblpref.$this->fieldname);
		
		if( $current_picture ){
			$current_picture_path = $this->base_path.$current_picture;
			if( file_exists($current_picture_path) ){
				$src = base_url($current_picture);
			}
		}
		
		$result = '<img src="'.$src.'" alt="'.$current_picture.'" class="img-thumbnail" />';
		
		return $result;
	}
	
	public function get_img_by_src( $current_src, $params=array() ){
		$CI =& get_instance();
		
		extract($params);
		
		$width = isset($width)?$width:$this->width;
		$height = isset($height)?$height:$this->height;
		$class = isset($class)?$class:'img-responsive';
		
		$result = '';
		
		$src = $this->default_pic_url_fullpath;
		
		if( empty($current_src) == FALSE ){
			$picture_path = $this->base_path.$current_src;
			
			if( file_exists($picture_path) ){
				$src = base_url($current_src);
			}
		}
		
		if( $width || $height ){
			$dimension = 'style="width:'.$width.';height:'.$height.';"';
		}
		
		$result = '<img src="'.$src.'" alt="'.$current_src.'" '.$dimension.' class="'.$class.'" />';
		
		return $result;
	}
}
?>