<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GoogleMap {

	public $model;
	public $longitude;
	public $latitude;

    function __construct( $params=array() ){
		$params_default = array_merge(array(
			'model'=>NULL,
			'longitude'=>'',
			'latitude'=>'',
			), $params
		);
		extract($params_default);

		if( $params ){
			$this->model = $model;
		}

		$this->longitude = $longitude;
		$this->latitude = $latitude;
    }

    function __destruct(){
        $this->model = NULL;
        $this->longitude = NULL;
    }
    // zzz
    private function is_decimal_degrees($lat, $long) {
        if (($lat >= -90 && $lat <= 90) && ($long >= -180 && $long <= 180)) {
            return true;
        }
        return false;
        // http://www.phpliveregex.com/
        // ^\d{1,2}?°\s+\d{1,2}?\'{1}\s+\d{1,2}?\.?\d{1,6}?("|'{2})?\s?[NESWnesw]?$
        $pattern = preg_match("/^\d{1,2}?°\s+\d{1,2}?\'{1}\s+\d{1,2}?\.?\d{1,6}?(\"|'{2})?\s?[NESWnesw]?$/", $input_line, $output_array);
    }

	public function generate_map( $latitude, $longitude, $title = '' ){
        $CI = get_instance();
        $coordinates = $CI->setconf->get('location','coordinates');

		ob_start();

		if( $latitude && $longitude ){
			?>
			<div id="map" style="width:80%; min-height:256px; height: 100%;"></div>
			<script>
			function init_map() {
                var title = "<?php echo $title; ?>";
				var latitude = "<?php echo $latitude; ?>";
				var longitude = "<?php echo $longitude; ?>";
                var format = "<?= $coordinates ?>";

                if (format == 'dms') {
                    latitude = dms_to_dd(latitude);
                    longitude = dms_to_dd(longitude);
                }

                var label = title + '\nLatitude: ' + latitude.toFixed(6) + '\nLongitude: ' + longitude.toFixed(6);
				var mapCanvas = document.getElementById('map');
				var mapOptions = {
					center: new google.maps.LatLng(latitude, longitude),
					zoom: 15
				}
				var map = new google.maps.Map(mapCanvas, mapOptions);
                var marker = new google.maps.Marker({
                  map: map,
                  position: { lat: Number(latitude), lng: Number(longitude) },
                  title: label
                });
			}

            function dms_to_dd (dms) {
                if (!dms) {
                    return Number.NaN;
                }
                var neg = dms.match(/(^\s?-)|(\s?[SW]\s?$)/) != null ? -1.0 : 1.0;
                dms = dms.replace(/(^\s?-)|(\s?[NSEW]\s?)$/, '');
                dms = dms.replace(/\s/g, '');
                var parts = dms.match(/(\d{1,3})[.,°d ]?\s*(\d{0,2})[']?(\d{0,2})[.,]?(\d{0,})(?:["]|[']{2})?/);
                if (parts == null) {
                    return Number.NaN;
                }
                // parts:
                // 0 : degree
                // 1 : degree
                // 2 : minutes
                // 3 : secondes
                // 4 : fractions of seconde
                var d = (parts[1] ? parts[1] : '0.0') * 1.0;
                var m = (parts[2] ? parts[2] : '0.0') * 1.0;
                var s = (parts[3] ? parts[3] : '0.0') * 1.0;
                var r = (parts[4] ? ('0.' + parts[4]) : '0.0') * 1.0;
                var dec = (d + (m / 60.0) + (s / 3600.0) + (r / 3600.0)) * neg;
                return dec;
            }
			</script>
			<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC1_m0UeNdZj2R5hmoOVZ_vU1J6CR2r-b4&callback=init_map" async defer></script>
			<?php
		} else {
			?>
			<p class="bg-info">Sorry, no available map for this record.</p>
			<?php

		}
		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}
}

?>
