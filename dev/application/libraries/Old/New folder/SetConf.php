<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SetConf {
	
	private $Set;
	private $Conf;
	private $SetF;
	private $SetR;
	public $settings;
	
	/* return */
	public $result;

	function __construct( $params=array() ){
		$CI =& get_instance();
		
		$CI->load->model('Data_Model', 'Data');
		$CI->load->model('Settings_Model', 'Set');
		$CI->load->model('Configurations_Model', 'Conf');
		$CI->load->model('Settings_Format_Model', 'SetF'); 
		$CI->load->model('Settings_Records_Model', 'SetR'); 
		
		$this->Data = $CI->Data;
		$this->Set = $CI->Set;
		$this->Conf = $CI->Conf;
		$this->SetF = $CI->SetF;
		$this->SetR = $CI->SetR;
    }
	
	public function get(){
		
		$params = func_get_args();
		
		if( isset($params[1]) ){
			$value = get_value($this->settings[$params[0]],$params[1]);
		} elseif( isset($params[2]) ){
			$value = get_value($this->settings[$params[0][1]],$params[2]);
		} else {
			$value = get_value($this->settings,$params[0]);
		}
		
		return $value;
	}
	
	public function value( $array_object, $name, $default_value=NULL ){
		
		$value = get_value( $array_object, $name, $default_value );
		
		echo $value;
	}

	public function get_value( $array_object, $name, $default_value=NULL ){
		
		if( is_array($array_object) ){
			$value = isset($array_object[$name])?$array_object[$name]:NULL;
		} elseif( is_object($array_object) ){
			$value = isset($array_object->$name)?$array_object->$name:NULL;
		} else {
			$value = $array_object;
		}
		
		if( $value == '' && isset($default_value) ){
			$value = $default_value;
		}
		
		return $value;
	}
	
	public function set_join_params(){
		$CI =& get_instance();
		
		$data = array( 
			array($this->SetF->tblname,$CI->db->dbprefix.$this->SetF->tblname.'.'.$this->Set->tblid.'='.$CI->db->dbprefix.$this->Set->tblname.'.'.$this->Set->tblid,'INNER'),
			array($this->SetR->tblname,$CI->db->dbprefix.$this->SetR->tblname.'.'.$this->SetF->tblid.'='.$CI->db->dbprefix.$this->SetF->tblname.'.'.$this->SetF->tblid,'INNER')
		);
		
		return $data;
	}

    private function query_params( $name='', $format='' ){ 
		
		$where_params = array($this->Set->tblpref.'published'=>1);
		if( $name ){
			$where_params = array_merge($where_params, array($this->Set->tblpref.'name'=>$name));
		}
		if( $format ){
			$where_params = array_merge($where_params, array($this->SetF->tblpref.'name'=>$format));
		}
		$join_params = $this->set_join_params();
		
		$result = array('where_params'=>$where_params,'join_params'=>$join_params);
		
		return $result;
    }

    public function records( $name='', $format='', $multiple=TRUE ){
		
		$query_params = $this->query_params( $name, $format );
		
		if( $multiple ){
			$result = $this->Data->get_records( $this->Set, $query_params );
		} else {
			$result = $this->Data->get_record( $this->Set, $query_params );
		}
		
		return $result;
    }
	
    public function all(){ 
		
		$rows = $this->records();
		
		$settings = array();
		if( $rows ){
			$n=0;
			foreach( $rows as $k=>$col ){	
				$config_name = $this->get_value($col,$this->Set->tblpref.'name');
				$key = $this->get_value($col,$this->SetF->tblpref.'name');
				$default = $this->get_value($col,$this->SetF->tblpref.'default_value');
				$multiple_records = $this->get_value($col,$this->SetF->tblpref.'multiple_records');
				$value = $this->get_value($col,$this->SetR->tblpref.'value');
				$value = $value?$value:$default;
				
				if( $multiple_records ){
					$settings[$config_name][$key][$n] = $value;
					$settings[$key][$n] = $value;
					$n++;
				} else {
					$settings[$config_name][$key] = $value;
					$settings[$key] = $value;
					$n=0;
				}
			}
		}
		
		$this->settings = $settings;
		
		return $settings;
    }

    public function values( $name, $format='', $multiple=TRUE ){ 
		
		$rows = $this->records( $name, $format );
			
		if( $multiple ){
			$settings = array();
			if( $rows ){
				$n=0;
				foreach( $rows as $k=>$col ){					
					if( $format ){
						$settings[] = $this->get_value($col,$this->SetR->tblpref.'value');
					} else {
						$key = $this->get_value($col,$this->SetF->tblpref.'name');
						$multiple_records = $this->get_value($col,$this->SetF->tblpref.'multiple_records');
						$value = $this->get_value($col,$this->SetR->tblpref.'value');
						
						if( $multiple_records ){
							$settings[$key][$n] = $value;
							$n++;
						} else {
							$settings[$key] = $value;
							$n=0;
						}
					}
				}
			}
		} else {
			$settings = $this->get( $name, $format );
		}
		
		return $settings;
    }
}