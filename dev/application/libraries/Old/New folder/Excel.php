<?php
if (!defined('BASEPATH')) exit('No direct script access allowed'); 

require_once APPPATH."/third_party/Classes/PHPExcel.php";

class Excel extends PHPExcel {
    public function __construct() {
        parent::__construct();
		
    }
	
	public function export_report( $results_array=array(), $filename='' , $columns){
		$CI =& get_instance(); 
		$CI->excel->setActiveSheetIndex( 0 ); 
		$CI->excel->getActiveSheet()->setTitle( $filename );
		$CI->excel->getDefaultStyle()->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_CENTER );		
		
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		); 
		$row = 2;
		
		foreach($columns as $key=>$column){
			$CI->excel->getActiveSheet()->getColumnDimension( $key )->setAutoSize(false);
			$CI->excel->getActiveSheet()->getColumnDimension( $key )->setWidth($column);
		}
		$keys = array_keys($columns);
		$first_column = reset($keys).'1';
		$last_column = end($keys).'1';
		$bold = $first_column.':'.$last_column;
		$CI->excel->getActiveSheet()->getStyle($bold)->getFont()->setBold(true);
		
		foreach( $results_array as $key => $value ){
			$col = 0;
			foreach( $value as $title => $text ){
				$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,1)->applyFromArray( $styleArray );
				$CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $title );
				$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,$row)->applyFromArray( $styleArray );
				$CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $text );
				$col++; 
			} 
			$row++;
		} 

		$filename= $filename.'.xls'; //save our workbook as this file name 
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="'.$filename.'"'); 
		header('Cache-Control: max-age=0');
		  
		$objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel5');  
		$objWriter->save('php://output'); 
	} 
	
	public function export_report_individual( $results_array=array(), $filename='' , $columns){  
		$CI =& get_instance(); 
		$CI->excel->setActiveSheetIndex( 0 ); 
		$CI->excel->getActiveSheet()->setTitle( $filename );
		$CI->excel->getDefaultStyle()->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_CENTER );		
		
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);  
		$result_ce = $results_array['head'];
		$row = 1;
		$col = 1; 
		foreach( $result_ce as $title => $text ){ 
			$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,1);
			$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $title );
			$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,$row);
			$CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $text );
			$row++;
		} 
		$row_original = $row + 1;  
		$row += 2;    
		
		$result_ce_items = $results_array['items'];
		foreach( $result_ce_items as $key => $value ){
			$col = 0;
			foreach( $value as $title => $text ){
				$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,1);
				
				if($row_original == $row - 1 ){ 
					$CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row_original, $title );
				}
				$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,$row);
				$CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $text );
				$col++; 
			} 
			$row++;
		} 
		
		$filename= $filename.'.xls'; //save our workbook as this file name 
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="'.$filename.'"'); 
		header('Cache-Control: max-age=0');
		  
		$objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel5');  
		$objWriter->save('php://output'); 
	} 

}

