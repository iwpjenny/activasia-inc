<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fields {
	
	private $CI;
	private $Data;
	private $field_data;
	
	/* return */
	public $error;
	public $result;
	public $notification;
	
	/* parameters */
	public $model;
	public $fields_post;

    function __construct(){
        $CI =& get_instance();
		
		$CI->load->model('Data_Model','Data');
		
		$this->Data = $CI->Data;
		$this->CI = $CI;
    }

    function __destruct(){
        $this->Data = NULL;
        $this->error = NULL;		
		$this->result = NULL;
		$this->notification = NULL;
		$this->fields_post = NULL;
    }
	
	public function validate( $field_data ){
		$CI =& get_instance();
		
		$action = 'save';
		$error = array();
		$fields_post = $this->fields_post;
		
		if( $fields_post ){
			foreach($fields_post as $key=>$type){				
				if( array_key_exists($key, $field_data) ){
					
					$value = app_get_val($field_data, $key);
					$field_name = str_replace($this->model->tblpref,'',$key);
					$text = ' Field: <strong>('.$field_name.')</strong>.';
					$notify_params = array('action'=>$action,'type'=>$type,'text'=>$text);
					
					if( $type == 'required' ){
						if( $value == '' ){
							$CI->notify->set( $notify_params );
							$error[] = $CI->notify->notification;
						}
					}
					if( $type == 'numeric' ){
						if( is_numeric($value) == FALSE ){
							$CI->notify->set( $notify_params );							
							$error[] = $CI->notify->notification;
						}
					}
					if( $type == 'email' ){
						if( filter_var($value, FILTER_VALIDATE_EMAIL) == FALSE ){
							$CI->notify->set( $notify_params );							
							$error[] = $CI->notify->notification;
						}
					}
					if( $type == 'name' ){
						$regex = "/^[a-zA-Z\ñ ]*$/";
						if( preg_match($regex,$value) == FALSE ){
							$CI->notify->set( $notify_params );							
							$error[] = $CI->notify->notification;
						}
					}
					if( $type == 'url' ){
						$regex = "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";
						if( preg_match($regex,$value) == FALSE ){
							$CI->notify->set( $notify_params );							
							$error[] = $CI->notify->notification;
						}
					}
				}
				$new_field_data[$key] = $value;
			}
		}
		
		$this->error = $error;
		$this->field_data = $field_data;
		
		if( $error ){
			$this->set_session( $field_data );
		}		
		
		return $error;
	}	
	
	public function set_session( $field_data ){
		$CI =& get_instance();
		
		$CI->session->set_userdata('field_data', $field_data);
	}
	
	public function get_session(){
		$CI =& get_instance();
		
		$field_data = $CI->session->userdata('field_data');
		
		return $field_data;
	}
	
	public function unset_session(){
		$CI =& get_instance();
		
		$field_data = $CI->session->unset_userdata('field_data');
		
		return $field_data;
	}

	public function show( $params ){
		
		$result = $this->get( $params );
		
		echo $result;
	}

	public function redirect( $redirect_url='' ){
		
		if( $this->error ){
			$this->set_session( $this->field_data );
			
			if( $redirect_url ){
				redirect( $redirect_url );
			} else {
				redirect_current_page();
			}
		}
	}

	public function get( $params ){
		$CI =& get_instance();
		
		$data = $this->data;
		$field_data = $this->get_session();
		
		$params_default = array_merge(array(
			'name'=>'title',
			'type'=>'text',
			'group'=>$this->model->tblname,
			'prefix'=>$this->model->tblpref,
			'field'=>'',
			'class'=>'',
			'id'=>'',
			'holder'=>'',
			'attr'=>'',
			'max'=>'',
			'min'=>'',
			'req'=>FALSE,
			'read'=>FALSE,
			'has_value'=>TRUE,
			'compare_values'=>1,
			'value'=>'',
			'default'=>''
			), $params
		);
		extract($params_default);
		
		$required = $req?'required="required"':'';
		$readonly = $read?'readonly="readonly"':'';
		$id = $id?'id="'.$id.'"':'';
		$maxlength = $max?'maxlength="'.$max.'"':'';
		$minlength = $min?'maxlength="'.$min.'"':'';
		$placeholder = $holder?'placeholder="'.$holder.'"':'';
		
		$attributes = $attr.' '.$required.' '.$readonly.' '.$id.' '.$maxlength.' '.$minlength.' '.$placeholder;
			
		$field_name = $prefix.$name;
		$tag_name = $group.'['.$field_name.']';
		
		$current_value = '';
		$default_value = FALSE;
		if( $has_value ){
			$current_value = app_get_val( $data, $field_name );			
			
			if( empty($current_value) && $default ){
				if( is_bool($default) && $default === TRUE ){
					$current_value = $value;
				} else {
					$current_value = $default;
				}
				$default_value = TRUE;
			} elseif( empty($current_value) && isset($field_data[$field_name]) ) {
				$current_value = $field_data[$field_name];
			}
		}
		
		if( in_array($type,array('checkbox','radio')) ){
			if( $type == 'checkbox' ){
				if( $default_value == FALSE ){
					$current_value = $current_value?@unserialize($current_value):$current_value;
				}
				$tag_name = $tag_name.'[]';		
			}
			
			$checked = app_get_checked($value, $current_value);
			
			$result = '<input type="'.$type.'" class="'.$class.'" name="'.$tag_name.'" value="'.$value.'" '.$attributes.' '.$checked.' />';
		} elseif( $type == 'textarea' ){
			$value = $value?$value:$current_value;
			$result = '<textarea class="form-control '.$class.'" name="'.$tag_name.'" '.$attributes.'>'.$value.'</textarea>';
		} else {
			$value = $value?$value:$current_value;
			
			$result = '<input type="'.$type.'" class="form-control '.$class.'" name="'.$tag_name.'" value="'.$value.'" '.$attributes.' />';
		}
		
		return $result;
	}
	
	function elem( $name, $model, $data, $params ){
		
		$field_data = $this->get_session();
		
		$defaults = array_merge(array(
			'type'=>'text',
			'class'=>'',
			'id'=>'',
			'holder'=>'',
			'attr'=>'',
			'max'=>'',
			'min'=>'',
			'req'=>FALSE,
			'read'=>FALSE,
			'has_value'=>TRUE,
			'compare_values'=>1,
			'value'=>'',
			'default'=>''
			), $params
		);
		extract($defaults);
		
		$required = $req?'required="required"':'';
		$readonly = $read?'readonly="readonly"':'';
		$id = $id?'id="'.$id.'"':'';
		$maxlength = $max?'maxlength="'.$max.'"':'';
		$minlength = $min?'maxlength="'.$min.'"':'';
		$placeholder = $holder?'placeholder="'.$holder.'"':'';
		
		$attributes = $attr.' '.$required.' '.$readonly.' '.$id.' '.$maxlength.' '.$minlength.' '.$placeholder;
		
		
		if( is_object($model) ){
			$field_name = $model->tblpref.$name;
			$tag_name = $model->tblname.'['.$field_name.']';
		} else {
			$prefix = $model;
			$field_name = $prefix.$name;
			$tag_name = $field_name;
		}
		
		$current_value = '';
		$default_value = FALSE;
		if( $has_value ){		
			$current_value = app_get_value( $data, $field_name );
			if( empty($current_value) && $default ){
				if( is_bool($default) && $default === TRUE ){
					$current_value = $value;
				} else {
					$current_value = $default;
				}
				$default_value = TRUE;
			} elseif( empty($current_value) && isset($field_data[$field_name]) ) {
				$current_value = $field_data[$field_name];
			}
		}
		
		if( in_array($type,array('checkbox','radio')) ){
			if( $type == 'checkbox' ){
				if( $default_value == FALSE ){
					$current_value = $current_value?@unserialize($current_value):$current_value;
				}
				$tag_name = $tag_name.'[]';		
			}
			$checked = get_checked($value, $current_value);
			
			$result = '<input type="'.$type.'" class="'.$class.'" name="'.$tag_name.'" value="'.$value.'" '.$attributes.' '.$checked.' />';
		} elseif( $type == 'textarea' ){		
			$value = $value?$value:$current_value;
			$result = '<textarea class="form-control '.$class.'" name="'.$tag_name.'" '.$attributes.'>'.$value.'</textarea>';
		} else {
			$value = $value?$value:$current_value;
			
			$result = '<input type="'.$type.'" class="form-control '.$class.'" name="'.$tag_name.'" value="'.$value.'" '.$attributes.' />';
		}
		
		echo $result;
	}
	
	public function select( $Model, $ModelUsedBy, $current_id='', $params=array(), $hierarchy=FALSE ){	
		$CI =& get_instance();
		
		$field_data = $this->get_session();
		
		$current_id = app_get_val($field_data, $Model->tblid, $current_id);
		
		$result = $this->dropdown( $Model, $ModelUsedBy, $current_id, $params, $hierarchy );
		
		return $result;
	}
	
	public function dropdown( $Model, $ModelUsedBy, $current_id='', $params=array(), $hierarchy=FALSE, $level=0, $id=0, $parent_counts=0, $parent_level=1 ){	
		$CI =& get_instance();
		
		$params_default = array_merge(array(
			'multiple'=>FALSE,
			'name'=>$ModelUsedBy->tblname.'['.$Model->tblid.']',
			'default_value'=>'',
			'default_text'=>'--Select '.$Model->name.'--',
			'class'=>'',
			'elem_id'=>'',
			'attr'=>'',
			'req'=>TRUE,
			'read'=>FALSE,
			'query_params'=>array('where_params'=>array($Model->tblid.'<>'=>0),'orderby'=>$Model->tblpref.'name','ordertype'=>'ASC'),
			'fields'=>'title',
			'seperator'=>', ',
			'prefix'=>'-',
			'debug'=>FALSE
			), $params
		);		
		extract($params_default);
		
		if( $hierarchy === TRUE ){
			$query_params = array_merge($query_params,array('where_params'=>array($Model->tblpref.'parent_id'=>$id)));
		}
		
		if( $multiple === TRUE ){
			$name = $name.'[]';
			$selected_value = is_serialized($selected_value)?unserialize($selected_value):'';
		}
		
		$attr = $attr?$attr:'';
		$multiple = $multiple?'multiple="multiple"':'';
		$required = $req?'required="required"':'';
		$readonly = $read?'readonly="readonly"':'';
		$elem_id = $elem_id?'id="'.$elem_id.'"':'';
		
		$rows = $this->Data->get_records( $Model, $query_params );
		
		$result = '';
		if( $hierarchy === FALSE || ($hierarchy === TRUE && $level === 0) ){
			$parent_counts = count($rows);
			
			$attributes = $attr.' '.$required.' '.$readonly.' '.$elem_id.' '.$multiple;
			$result .= "\n".'<select name="'.$name.'" class="form-control '.$class.'" '.$attributes.'>'."\n";
			$selected = empty($current_id)?'selected="selected"':'';
			$result .= '<option value="'.$default_value.'" '.$selected.'>'.$default_text.'</option>'."\n";
		}
		
		$prfx = '';
		if( $level ){
			$cp = 1;
			while($cp <= $level){
				$prfx .= $prefix;
				$cp++;
			}
		}
		   
		$level++;	
		
		if( $rows ){
			foreach($rows as $col){
				$id = get_value($col,$Model->tblid);
				
				if( is_array($fields) ){
					$text = '';
					$c=1;
					foreach($fields as $field){
						$value = get_value($col,$Model->tblpref.$field);
						if( $field == 'id' ){
							$value = 'ID-'.$value;
						}
						if( $c > 1 ){
							$text .= $seperator.$value;
						} else {
							$text .= $value;
						}
						$c++;
					}
				} else {
					$text = get_value($col,$Model->tblpref.$fields);
				}
				$text = $prfx.$text;
								
				$selected = ($current_id==$id)?'selected="selected"':'';
				if( $debug === TRUE ){
					$result .= '<option value="'.$id.'" '.$selected.'>'.$text.' '.$level.'-'.$parent_counts.'-'.$parent_level.'</option>'."\n";
				} else {
					$result .= '<option value="'.$id.'" '.$selected.'>'.$text.'</option>'."\n";
				}
				
				if( $hierarchy === TRUE ){
					
					$result2 = $this->dropdown( $Model, $ModelUsedBy, $current_id, $params, $hierarchy, $level, $id, $parent_counts, $parent_level );
					
					if( $result2 ){
						$result .= $result2;
					}
		
					if( $level == 1 && ($parent_level < $parent_counts) ){
						$parent_level++;
					}
				}
			}
		} else {
			if( $hierarchy === TRUE && $parent_level == $parent_counts ){
				$result .= '</select>'."\n";
			}
		}
		
		if( $hierarchy === FALSE || (count($rows) <= 0 && $level == 1) ){
			$result .= '</select>'."\n";
		}
		
		return $result;
    }
}
?>