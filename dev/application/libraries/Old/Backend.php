<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backend {
	
	private $model_name = 'User';
	private $location;
	private $login_user_type;
	private $dashboard_page;

    public function __construct( $params=array() ){
        
    }

	public function set(){
        $CI =& get_instance();
		
		$CI->load->model('BackUp_Model', 'BU');
		$CI->load->model('Backend_Log_Model', 'BLog');
		
		$CI->load->model($this->model_name.'_Model', 'User');
		$CI->load->model($this->model_name.'_Logs_Model', 'UL');
		$CI->load->model($this->model_name.'_Role_Model', 'UR');
		$CI->load->model($this->model_name.'_Profile_Model', 'UP');
	}

	public function set_libaries( $model=NULL ){
        $CI =& get_instance();
		
		$this->location = $CI->BLog->location;
		$this->login_user_type = $CI->BLog->login_user_type;
		$this->dashboard_page = $CI->BLog->dashboard_page;
		
		$CI->load->library('Logged');
		$CI->logged->dashboard_page = $this->dashboard_page;
		$CI->logged->login_user_type = $this->login_user_type;
		$CI->logged->model_user = $CI->User;
		$CI->logged->model_user_role = $CI->UR;
		$CI->logged->model_user_logs = $CI->UL;
		$CI->logged->model_user_profile = $CI->UP;
		$CI->logged->model_log = $CI->BLog;
		
		$userlogs_params = array(
			'model'=>$model,
			'model_user'=>$CI->User,
			'model_user_logs'=>$CI->UL
		);
		$CI->load->library('UsersLogs',$userlogs_params);
				
		$CI->load->library('Method');
		$CI->method->model = $model;
		$CI->method->login_user_type = $this->login_user_type;
		$CI->method->location = $CI->BLog->location;
		
		$CI->method->model_user = $CI->User;
		$CI->method->model_user_role = $CI->UR;
		$CI->method->model_user_logs = $CI->UL;
		$CI->method->model_user_profile = $CI->UP;
		$CI->method->model_log = $CI->BLog;
		
		$usersrole_params = array(
			'model'=>$model,
			'model_user'=>$CI->User,
			'model_log'=>$CI->BLog
		);
		$CI->load->library('UsersRole',$usersrole_params);
	}
}
?>