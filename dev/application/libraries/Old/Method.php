<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Method {

	private $backend_model = 'user';
	private $frontend_model = 'member';
	private $logged_user_prefix = 'logged_';
	private $role;
	
	public $model;
	public $page;
	public $fpage;
	public $login_user_type;
	public $model_user;
	public $model_user_role;
	public $model_user_logs;
	public $model_user_profile;
	public $model_log;
	public $location;

    function __construct( $params=array() ){
        $CI =& get_instance();
		
		/* $params_default = array_merge(array(
			'model'=>NULL,
			'page'=>'bhome',
			'fpage'=>NULL,
			'login_user_type'=>'admin',
			'model_user'=>NULL,
			'model_user_role'=>NULL,
			'model_user_logs'=>NULL,
			'model_user_profile'=>NULL,
			'model_log'=>NULL
			), $params
		);
		extract($params_default);
		
		$this->model = $model?$model:$this->model;
		$this->page = $page?$page:$this->page;
		$this->fpage = $fpage?$fpage:$this->fpage;
		$this->login_user_type = $login_user_type?$login_user_type:$this->login_user_type;
		
		$this->model_user = $model_user?$model_user:$this->model_user;
		$this->model_user_role = $model_user_role?$model_user_role:$this->model_user_role;
		$this->model_user_logs = $model_user_logs?$model_user_logs:$this->model_user_logs;
		$this->model_user_profile = $model_user_profile?$model_user_profile:$this->model_user_profile;
		$this->model_log = $model_log?$model_log:$this->model_log; */
    }
	 
	public function set( $type_array=array() ){
        $CI =& get_instance();
		
		if( array_key_exists('helper',$type_array) ){
			if( isset($type_array['helper']) ){
				$helper_row = $type_array['helper'];
				if( is_array($helper_row) ){
					foreach($helper_row as $helper_name){
						$CI->load->helper($helper_name);
					}
				}
			}
		}
		
		if( array_key_exists('library',$type_array) ){
			if( isset($type_array['library']) ){
				$library_row = $type_array['library'];
				if( is_array($library_row) ){
					foreach($library_row as $library_name){
						$CI->load->library($library_name);
					}
				}
			}
		}
				
		if( array_key_exists('model',$type_array) ){			
			if( isset($type_array['model']) ){
				$model_row = $type_array['model'];
				if( is_array($model_row) ){
					foreach($model_row as $key=>$model_name){
						$CI->load->model($model_name.'_Model', $key);
					}
				}
			}
		}
	}

	public function set_admin_models(){
        $CI =& get_instance();
			
		$user_model = ucwords($this->backend_model);
		
		$CI->load->model($user_model.'_Model', 'User');
		$CI->load->model($user_model.'_Logs_Model', 'UL');
		$CI->load->model($user_model.'_Role_Model', 'UR');
		$CI->load->model($user_model.'_Profile_Model', 'UP');
	}

	public function set_member_models(){
        $CI =& get_instance();
		
		$user_model = ucwords($this->frontend_model);
		
		$CI->load->model($user_model.'_Model', 'Mem');
		$CI->load->model($user_model.'_Logs_Model', 'ML');
		$CI->load->model($user_model.'_Role_Model', 'MR');
		$CI->load->model($user_model.'_Profile_Model', 'MP');
	}

    public function set_controller( $params=array() ){
        $CI =& get_instance();
		extract($params); /* params: $type, $log, $role, $method, $log_action */
		
		if( isset($log) ){
			if( $log == 'login' ){
				$CI->logged->check_login_permission();
			} elseif( $log == 'logout' ){
				$CI->logged->check_if_user_logout();
			}
		}
		if( isset($role) ){
			$id = $CI->input->post('id');
			
			if( $role != '' ){
				if( in_array($role, array('index','json_display_record')) ){
					$role = 'records';
				} elseif( in_array($role, array('save','json_save')) && empty($id) ){
					$role = 'add';
				} elseif( in_array($role, array('save','json_save')) && $id ){
					$role = 'edit';
				} elseif( in_array($role, array('save_profile')) ){
					$role = 'profile';
				} elseif( in_array($role, array('json_delete')) ){
					$role = 'delete';
				} elseif( in_array($role, array('trashed','untrash','json_trash','json_untrash')) ){
					$role = 'trash';
				} elseif( in_array($role, array('delete_file','delfile')) ){
					$role = 'delete_file';
				} elseif( is_bool($role) && $role === TRUE ){
					$role = 'records';
				}
				$CI->usersrole->user_has_capability($this->model->urc_name, $role);
			}
		}
		if( isset($method) && is_array($method) ){
			$this->set( $method );	
		}
		if( isset($form) ){
			if( is_bool($form) && $form === TRUE ){
				$CI->logged->set_login_security_key();
			}
		}
		if( isset($type) ){
			if( $type == 'process' ){
				$data = $this->set_data_process();
			} elseif( $type == 'logged' ){
				/*page function init----------------------------------*/
				$data = $this->set_data_page();
				/*logged user function init---------------------------*/
				$data = $this->set_logged_page_user( $data );
			} elseif( $type == 'json-page' ){
				/*page function init----------------------------------*/
				$data = $this->set_data_page();				
				if( isset($log) && $log == 'login' ){
					/*logged user function init---------------------------*/
					$data = $this->set_logged_page( $data );
					$data = $this->set_logged_page_user( $data );
				}
			} elseif( $type == 'json-process' ){
				$data = $this->set_data_process();
			} else {
				/*page function init----------------------------------*/
				$data = $this->set_data_page();	
				if( isset($log) && $log == 'login' ){
					/*logged user function init---------------------------*/
					$data = $this->set_logged_page( $data );
					$data = $this->set_logged_page_user( $data );
				}
			}
		} else {
			/*page function init----------------------------------*/
			$data = $this->set_data_page();	
			if( isset($log) && $log == 'login' ){
				/*logged user function init---------------------------*/
				$data = $this->set_logged_page( $data );
				$data = $this->set_logged_page_user( $data );
			}
		}
		
		$data = $this->set_logged_user( $data );
		
		return $data;
	}
	
    public function get_model_page( $model='' ){
		
		$model = $model ? $model : $this->model;
		
		if( isset($model) ){
			if( isset($this->model_log) && $this->model_log->location == 'frontend' ){
				$page = isset($model->fpage) ? $model->fpage : $this->page;
			} else {
				$page = $model->page;
			}
		} else {
			$page = $this->page;
		}
		
		return $page;
	}
	
    public function set_data_page(){
		$CI =& get_instance();
		$CI->config->load('config_developer', TRUE);
		$CI->config->load('config_client', TRUE);
		$CI->load->library('form_validation');
		
		$data = array();
		$data['elem_body_class_log'] = 'logout';
		
		$action = $CI->input->get('m');
		
		if( isset($this->model_log) ){
			$data['location'] = $this->model_log->location;
		}
		
		$page = $this->get_model_page();
		$data['page'] = $page;
		
		if( isset($this->model) ){
			if( $action == 'edit' ){
				$form_action = site_url('c='.$page.'&m=save');
			} else {
				$form_action = site_url('c='.$page);
			}
			$data['model'] = $this->model;
			$data['tblpref'] = $this->model->tblpref;
			$data['tblid'] = $this->model->tblid;
			$data['tblname'] = $this->model->tblname;
			$data['form_action'] = $form_action;
			$data['form_action_search'] = FALSE;
			if( isset($this->model->fpage) ){
				$data['fpage'] = $this->model->fpage;
			}
		} else {
			/* $data['page'] = $this->page;
			$data['fpage'] = $this->fpage; */
		}
		
		$data['settings'] = $CI->setconf->all();		
		$data['config_notifications'] = $CI->config->item('config_notifications');		
		$data['config_developer'] = $CI->config->item('config_developer');
		$data['config_client'] = $CI->config->item('config_client');
		
		$CI->notify->data = $data;
		
		return $data;
	}

    public function set_data_process(){
        $CI =& get_instance();
		
		$CI->config->load('config_notifications', TRUE);
		
		$data = array();		
		$data['settings'] = $CI->setconf->all();		
		$data['config_notifications'] = $CI->config->item('config_notifications');
		$CI->notify->data = $data;
		
		return $data;
	}

    public function set_logged_page_user( $data=array() ){
        $CI =& get_instance();
		
		if( isset($this->model_user) ){
			$user = $this->model_user->get_logged_user();
			
			if( $user ){			
				$clean_user = $this->clean_query_array($user, $this->model_user, $this->logged_user_prefix);
				
				$ur_id = app_get_val($user, $this->model_user_role->tblid);
				$user_position = app_get_val($user, $this->model_user_role->tblpref.'position');
				$user_position_name = app_get_val($user, $this->model_user_role->tblpref.'name');
				$user_position_title = app_get_val($user, $this->model_user_role->tblpref.'title');
				$capabilities_serialized = app_get_val($user, $this->model_user_role->tblpref.'capabilities_serialized');
				$picture = app_get_val($user, $this->model_user_profile->tblpref.'picture');
				
				$data = array_merge($data, $clean_user);
				
				$data['capabilities'] = unserialize($capabilities_serialized);;	
				$CI->logged->set_login_session('capabilities_serialized',$capabilities_serialized);
							
				$params = array(
					'model'=>$this->model_user,
					'model_profile'=>$this->model_user_profile,
					'folder'=>$this->model_user_profile->folder,
					'width'=>'100%',
					'height'=>'100%'
				);
				$CI->load->library('pic',$params);
				$data['header_menu_logged_user_picture'] = $CI->pic->get_img_by_src( $picture, $params );
							
				$data['elem_body_class_log'] = 'login';
				$data['logged_user_position'] = $user_position;
				$data['logged_user_position_name'] = $user_position_name;
				$data['logged_user_position_title'] = $user_position_title;
			}
		}
		
		return $data;
    }

    public function set_logged_user( $data ){
        $CI =& get_instance();
		
		if( isset($this->model_user) && isset($this->model) ){
			$user = $this->model_user->get_logged_user();
			if( $user ){
				$data['user_role_model_capabilities'] = $CI->usersrole->get_model_role( $this->model->urc_name );
			}
		}
		
		return $data;
	}
	
	function set_logged_page( $data ){
        $CI =& get_instance();
		
		$data['user_total'] = $CI->Data->count_records_row( $this->model_user, array('where_params'=>array( $this->model_user->tblpref.'published'=>1) ) );
		$data['user_logs_total'] = $CI->Data->count_records_row( $this->model_user_logs );
		
		return $data;
	}
	
	public function clean_query_array( $records, $model, $prefix='' ){
		
		$result = array();
		
		if( $records ){
			foreach($records as $key=>$value){
				if($key != $model->tblid){
					$key = str_replace($model->tblpref, $prefix, $key);
				}
				$result[$key] = $value;
			}
		} else {
			$result = $records;
		}
		
		return $result;
	}
	
	public function clean_query_object( $records, $model, $prefix='' ){
		
		$result = array();
		
		if( $records ){
			foreach($records as $key=>$value){
				if($key != $model->tblid){
					$key = str_replace($model->tblpref, $prefix, $key);
				}
				$result[$key] = $value;
			}
		} else {
			$result = $records;
		}
		
		$result = (object)$result;
		
		return $result;
	}
}