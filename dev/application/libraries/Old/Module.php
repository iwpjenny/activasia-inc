<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Module {
	
	private $settings;
	private $Data;
	private $views_editable = TRUE;
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	public $notification;
	
	/* parameters */
	public $id = NULL;
	public $page = NULL;
	public $model = NULL;
	public $model_parent = NULL;
	public $model_user = NULL;
	public $model_log = NULL;
	public $model_files = NULL;
	public $module = NULL;
	public $location;
	public $logged_user_only = FALSE;
	public $fields;
	public $field_data = array();
	public $fields_post = array();
	public $sub_section_join_params = array();
	public $sub_section_groupby;
	public $primary_field = 'title';

    function __construct(){
        $CI =& get_instance();
		
		$this->Data = $CI->Data;
		
		$this->settings = $CI->setconf->all();
    }

    function __destruct(){
        $this->id = NULL;
        $this->Data = NULL;
        $this->settings = NULL;
		$this->views_editable = NULL;
		
		$this->dashboard_page = NULL;
		$this->login_user_type = NULL;
		$this->model = NULL;
		$this->model_user = NULL;
		$this->model_files = NULL;
		$this->model_log = NULL;
		$this->field_restrictions = NULL;
    }
	
	public function record( $model, $where_params_or_id ){
        $CI =& get_instance();
		
		if( is_array($where_params_or_id) ){
			$where_params = $where_params_or_id; 
		} else {
			$where_params = array( $model->tblname.'.'.$model->tblid => $where_params_or_id ); 
		}
		
		$join_params = $model->set_join_params(); 
		$fields = $this->Data->get_record( $model, array(
			'where_params'=>$where_params,
			'join_params'=>$join_params
		));
		
		return $fields;
	}
	
	public function records( $model, $data, $where_params=array() ){ 
        $CI =& get_instance();
		
		$search_fields = $model->set_search_fields(); 
		$join_params = $model->set_join_params();
		$orderby = method_exists($model,'set_order_by')?$model->set_order_by():NULL;
		
		$query_params = array(
			'join_params'=>$join_params,
			'search_fields'=> $search_fields,
			'where_params'=> $where_params,
			'orderby'=> $orderby,
			'ordertype'=>'DESC',
			'paging'=>TRUE
		);
		$records = $this->Data->get_records( $model, $query_params ); 
		$data['record_rows'] = $records;
		$data['pagination'] =  $this->Data->pagination_link; 
		$data['per_page'] = $this->Data->per_page;
		$data['total_rows'] = $this->Data->total_rows;
		
		return $data;
	}
	
	public function save_data( $model, $field_data, $id_or_params='' ){
		$CI =& get_instance();
		
		$data = array_merge($field_data, $this->field_data);
				
		if( $id_or_params ){
			if( is_array($id_or_params) ){
				$result = $this->Data->update_data( $model, $data, $id_or_params );
			} else {
				$result = $this->Data->update_data( $model, $data, array( $model->tblid=>$id_or_params ) );
			}
		} else {
			$result = $this->Data->insert_data( $model, $data );
		}
		
		$this->field_data = array();
		
		return $result;
	}
	
	public function save( $model='', $model_files='' ){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$model = $model?$model:$this->model;
		$model_files = $model_files?$model_files:$this->model_files;
		$page = $this->page?$this->page:$model->page;
		/*---------------------------------------------*/
		
		$id = $CI->input->post('id'); 
		$field_data = $CI->input->post($model->tblname);
				
		$field_data = $this->logged_user_only( $field_data );
		$field_data = $this->serialize_array_data( $field_data );
		
		$CI->fields->model = $this->model;
		$CI->fields->fields_post = $this->fields_post;
		$CI->fields->validate( $field_data );
		$CI->fields->redirect();
		
		if( $id ){			
			$save = $this->save_data( $model, $field_data, $id );
			
			if( $save ){
				$details = $this->record($model, $id);
				$name = get_value($details, $model->tblpref.$this->primary_field);
								
				$notify_params = array('action'=>'update','type'=>'success','log'=>TRUE);
				$CI->fields->unset_session();
			} else {
				$notify_params = array('action'=>'update','type'=>'error','log'=>TRUE);
			}
			$CI->notify->set( $notify_params );
			
			$this->upload_file( $id );		
			
			redirect_current_page();
		} else {			
			$id = $this->save_data( $model, $field_data );
			
			if( $id ){
				$this->upload_file( $id );
			
				$details = $this->record($model, $id);
				$name = get_value($details,$model->tblpref.$this->primary_field);
				
				$notify_params = array('action'=>'add','type'=>'success','log'=>TRUE);
				$CI->notify->set( $notify_params );
				$CI->fields->unset_session();
				
				redirect( site_url( 'c='.$page.'&m=edit&id='.$id ) );
			} else {
				$notify_params = array('action'=>'add','type'=>'error','log'=>TRUE);
				$CI->notify->set( $notify_params );
				
				redirect_current_page();
			}					
		}
	}
	
	public function delete( $model='', $model_files='' ){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$model = $model?$model:$this->model;
		$model_files = $model_files?$model_files:$this->model_files;
		$page = $this->page?$this->page:$model->page;
		/*---------------------------------------------*/
		
		$id = $CI->input->get('id');
		
		$details = $this->Data->get_record( $model, array( 'where_params'=>array($model->tblid=>$id) ) ); 
		
		if( $details ){
			$name = get_value( $details, $model->tblpref.$this->primary_field);
			
			$delete = $this->Data->delete_data( $model, array($model->tblid=>$id) );
			
			if( $delete ){
				$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE,'text'=>'ID:'.$id.' - '.$name);
				$CI->notify->set( $notify_params );
				
				if( isset($CI->modulefiles) ){
					$CI->modulefiles->delete_all( $id );
				}
			} else {
				$notify_params = array('action'=>'file','type'=>'delete-error','log'=>TRUE);
				$CI->notify->set( $notify_params );
			}
		} else {
			$notify_params = array('action'=>'record','type'=>'not-exist','log'=>TRUE);
			$CI->notify->set( $notify_params );
		}
		
		redirect( site_url('c='.$page) );
	}
	
	private function upload_file( $id ){
		$CI =& get_instance();
		
		if( isset($CI->modulefiles) ){
			$CI->modulefiles->id = $id;
			$CI->modulefiles->upload();
		}
	}
	
	public function trash( $trash_value=1 ){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$model = $model?$model:$this->model;
		$page = $this->page?$this->page:$model->page;
		/*---------------------------------------------*/
		
		$id = $CI->input->get('id');
		
		$details = $this->Data->get_record( $model, array( 'where_params'=>array($model->tblid=>$id) ) ); 
		
		if( $details ){
			$name = get_value( $details, $model->tblpref.$this->primary_field);
			
			$data = array($model->tblpref.'trashed'=>$trash_value);
			$trash = $this->Data->update_data( $model, $data, array($model->tblid=>$id) );
			
			if( $trash ){
				$notify_params = array('action'=>'trash','type'=>'success','log'=>TRUE);
				$CI->notify->set( $notify_params );	
			} else {
				$notify_params = array('action'=>'trash','type'=>'error','log'=>TRUE);
				$CI->notify->set( $notify_params );	
			}
			
		} else {
			$notify_params = array('action'=>'record','type'=>'not-exist','log'=>TRUE);
			$CI->notify->set( $notify_params );
		}
		
		if( $trash_value === 1 ){
			redirect( site_url('c='.$page.'&m=trashed') );
		} else {
			redirect( site_url('c='.$page) );
		}
	}
	
	public function untrash(){
		
		$this->trash( 0 );
	}
	
	public function delete_file( $model='', $model_files='' ){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$model = $model?$model:$this->model;
		$model_files = $model_files?$model_files:$this->model_files;
		$page = $this->page?$this->page:$model->page;
		/*---------------------------------------------*/
		
		$id = $CI->input->get('id'); 
		$fid = $CI->input->get('fid'); 
		
		$CI->modulefiles->id = $id;
		$CI->modulefiles->delete( $fid );
					
		redirect( site_url( 'c='.$page.'&m=edit&id='.$id ) );
	}
	
	public function modal_sub_section_view( $model, $parent_id ){
		
		$this->views_editable = FALSE;
		/* //echo 'HELOOOOOOO';
		printr('views_editable: '.$this->views_editable);		 */
		$contents = $this->modal_sub_section( $model, $parent_id );
		
		return $contents;
	}
	
	public function modal_sub_section( $model, $parent_id ){ 
		$CI =& get_instance();
		
		$contents = '';
		$model_parent = $this->model;
		$page = $CI->method->get_model_page( $model );
		
		if( $parent_id ){
			$data['settings'] = $this->settings;
			$data['location'] = $this->location;
			$data['views_editable'] = $this->views_editable;
			
			$tbody = $this->grid_tbody( $model, $model_parent, $parent_id );
			$thead = $CI->load->view($this->location.'/modules/'.$model->view.'/grid-thead',$data,TRUE);
			
			$data['id'] = '';
			$data['thead'] = $thead;
			$data['tbody'] = $tbody;
			$data['parent_id'] = $parent_id;
			$data['model'] = $model;
			$data['model_parent'] = $model_parent;
			$data['name'] = $model->name;
			$data['page'] = $page;
			//$data['fpage'] = $model->fpage; /* to be remove */
			$data['fpage'] = $page; /* to be remove */
			$data['tblid'] = $model->tblid;
			$data['tblpref'] = $model->tblpref;
			$data['tblname'] = $model->tblname;
			$data['fields'] = array();
			$data['form_action'] = site_url('c='.$model->page);
			
			$CI->load->library('ModuleTag',array('model'=>$model));
			
			$contents = $CI->load->view($this->location.'/modules/modal-sub-section',$data,TRUE);
		}
				
		return $contents;
	}
	
	public function grid_tbody( $model, $model_parent, $parent_id ){
		$CI =& get_instance();
		
		$page = $CI->method->get_model_page( $model );
				
		$data['settings'] = $this->settings;
		$data['capabilities'] = $CI->logged->get_user_capabilities();
		
		$data['model'] = $model;
		$data['page'] = $page;
		$data['fpage'] = $page; /* to be remove */
		$data['parent_id'] = $parent_id;
		$data['records'] = $this->grid_sub_records( $model, $model_parent, $parent_id );
		$data['views_editable'] = $this->views_editable;
				
		$contents = $CI->load->view($this->location.'/modules/'.$model->view.'/grid-tbody',$data,TRUE);
				
		return $contents;
	}
	
	public function grid_sub_records( $model, $model_parent, $query_params_or_id ){ 
		$CI =& get_instance();
		
		$id = $query_params_or_id;
				
		if( is_array($query_params_or_id) ){
			$query_params = $query_params_or_id;
		} else {
			$query_params = array(
				'join_params'=>$this->sub_section_join_params,
				'where_params'=>array(
					$model->tblname.'.'.$model_parent->tblid=>$id,
					$model->tblname.'.'.$model->tblpref.'trashed'=>0
				),
				'groupby'=>$this->sub_section_groupby
			);
		}
			
		$records = $this->Data->get_records( $model, $query_params ); 
		
		return $records;
	}
	
	private function logged_user_only( $field_data ){
		$CI =& get_instance();
		
		if( $this->logged_user_only ){
			$logged_user_id = $CI->logged->get_login_session('user_id');
			$field_data = array_merge($field_data,array($this->model_user->tblid=>$logged_user_id));
		}
		
		return $field_data;
	}
	
	private function serialize_array_data( $field_data ){
		$CI =& get_instance();
		
		$new_field_data = $field_data;
		if( $field_data ){
			foreach($field_data as $key=>$value){
				if( is_array($value) ){
					$value = serialize($value);
				}
				$new_field_data[$key] = $value;
			}
		}
		
		return $new_field_data;
	}
	
	public function json_save( $model='', $model_parent='', $model_files='' ){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$model = $model?$model:$this->model;
		$model_parent = $model_parent?$model_parent:$this->model_parent;
		$model_files = $model_files?$model_files:$this->model_files;
		//$page = $this->page?$this->page:$model->page;
		/*---------------------------------------------*/
		
		$parent_id = $CI->input->post('parent_id');
		$id = $CI->input->post('id'); 
		$field_data = $CI->input->post($model->tblname);
		
		$result = FALSE;
		
		if( $id ){			
			$save = $CI->records->save( $model, $field_data, $id );
			
			if( $save ){				
				$details = $this->Data->get_record($model, array($model->tblid=>$id));
				$name = get_value($details,$model->tblpref.$this->primary_field);
								
				$notify_params = array('action'=>'update','type'=>'success','log'=>TRUE);
				$result = TRUE;
			} else {
				$notify_params = array('action'=>'update','type'=>'error','log'=>TRUE);
			}
			$CI->notify->set( $notify_params );	
			
			$this->upload_file( $id );	
		} else {			
			$id = $CI->records->save( $model, $field_data );
			
			if( $id ){
				$this->upload_file( $id );
			
				$details = $this->Data->get_record($model, array($model->tblid=>$id));
				$name = get_value($details,$model->tblpref.$this->primary_field);
				
				$notify_params = array('action'=>'add','type'=>'success','log'=>TRUE);				
				$result = TRUE;
			} else {
				$notify_params = array('action'=>'add','type'=>'error','log'=>TRUE);
			}
			$CI->notify->set( $notify_params );	
		}
		$notification = $CI->notify->get( array('start','end') );
		
		$this->json_return_grid_tbody( $model, $model_parent, $parent_id, $notification, $result, $id );
	}
	
	public function json_display_record( $model='' ){
		$CI =& get_instance();
		
		$model = $model?$model:$this->model;
		$page = $CI->method->get_model_page( $model );
		
		$id = $CI->input->post('id');
		$details = $this->record( $model, $id );
		
		$result = FALSE;
		$field = array();
		if( $details ){
			foreach($details as $key=>$val){
				$key = str_replace($model->tblpref,'',$key);
				$field[$key] = $val;
			}
			
			$result = TRUE;
		}
		
		$json = array();
		$json['field'] = $field;
		//$json['page'] = $model->page;
		$json['page'] = $page;
		$json['notification'] = '';
		$json['result'] = $result;
		$json['redirect_url'] = '';
		
		echo json_encode($json);
		exit();
	}
	
	public function json_delete(){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$model = $this->model;
		$model_parent = $this->model_parent;
		/*---------------------------------------------*/
		
		$parent_id = $CI->input->post('parent_id');
		$id = $CI->input->post('id');
		
		$details = $this->Data->get_record( $model, array( 'where_params'=>array($model->tblid=>$id) ) ); 
		
		$result = FALSE;
		if( $details ){
			$name = get_value( $details, $model->tblpref.$this->primary_field);
			
			$delete = $this->Data->delete_data( $model, array($model->tblid=>$id) );
			
			if( $delete ){
				$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE);
				$result = TRUE;
			} else {
				$notify_params = array('action'=>'delete','type'=>'error','log'=>TRUE);
			}
		} else {
			$notify_params = array('record'=>'delete','type'=>'not-exist','log'=>TRUE);
		}
		$CI->notify->set( $notify_params );
		$notification = $CI->notify->get( array('start','end') );
		
		$this->json_return_grid_tbody( $model, $model_parent, $parent_id, $notification, $result );
	}
	
	public function json_trash( $trash_value=1 ){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$model = $this->model;
		$model_parent = $this->model_parent;
		/*---------------------------------------------*/
		
		$parent_id = $CI->input->post('parent_id');
		$id = $CI->input->post('id');
		
		$details = $this->Data->get_record( $model, array( 'where_params'=>array($model->tblid=>$id) ) ); 
		
		$result = FALSE;
		if( $details ){
			$name = get_value( $details, $model->tblpref.$this->primary_field);
			
			$data = array($model->tblpref.'trashed'=>$trash_value);
			$trash = $this->Data->update_data( $model, $data, array($model->tblid=>$id) );

			if( $trash ){
				$notify_params = array('action'=>'trash','type'=>'success','log'=>TRUE);
				$result = TRUE;
			} else {
				$notify_params = array('action'=>'trash','type'=>'error','log'=>TRUE);
			}			
		} else {
			$notify_params = array('record'=>'delete','type'=>'not-exist','log'=>TRUE);
		}		
		$CI->notify->set( $notify_params );	
		$notification = $CI->notify->get( array('start','end') );
		
		$this->json_return_grid_tbody( $model, $model_parent, $parent_id, $notification, $result );
	}
	
	public function json_untrash(){
		$this->json_trash( 0 );
	}
	
	public function json_return_grid_tbody( $model, $model_parent, $parent_id, $notification, $result, $id='' ){
		$CI =& get_instance();
		$page = $CI->method->get_model_page( $model );
		
		$field_data = array();
		if( $id ){
			$data = $this->Data->get_record( $model, array( 'where_params'=>array($model->tblid=>$id) ) );
			
			if( $data ){
				foreach($data as $key=>$val){
					$key = str_replace($model->tblpref,'',$key);
					$field_data[$key] = $val;
				}
			}
		}
		
		$tbody = $this->grid_tbody( $model, $model_parent, $parent_id );
		
		$json = array();
		$json['tbody'] = $tbody;
		//$json['page'] = $model->page;
		$json['page'] = $page;
		$json['field_data'] = $field_data;
		$json['notification'] = $notification;
		$json['result'] = $result;
		$json['redirect_url'] = '';
		
		echo json_encode($json);
		exit();
	}
	
	public function get_data_records( $data, $trashed=0 ){
		$CI =& get_instance();
		
		$where_params = array($this->model->tblpref.'trashed'=>$trashed);
		
		if( $this->logged_user_only ){		
			$logged_user_id = $CI->logged->get_login_session('user_id');	
			$where_params = array_merge($where_params,array($this->model_user->tblname.'.'.$this->model_user->tblid=>$logged_user_id));
		}
		
		$data = $this->records( $this->model, $data, $where_params );
		
		return $data;
	}
	
	public function get_data_view( $data ){
		$CI =& get_instance();
			
		$id = $CI->input->get('id');
		$data['id'] = $id;
		
		if( $this->logged_user_only ){		
			$logged_user_id = $CI->logged->get_login_session('user_id');
			
			$where_params = array(
				$this->model->tblid=>$id,
				$this->model_user->tblname.'.'.$this->model_user->tblid=>$logged_user_id
			);			
			$where_params_or_id = $where_params;
		} else {
			$where_params_or_id = $id;
		}
		
		$fields = $this->record( $this->model, $where_params_or_id );
		$data['fields'] = $fields;
		
		if( isset($CI->modulefiles) ){
			$CI->modulefiles->id = $id;
		}
		
		return $data;
	}
	
	public function get_data_edit( $data ){
		$CI =& get_instance();
		
		$data = $this->get_data_view( $data );
		
		$CI->load->library('ModuleTag',array('model'=>$this->model,'data'=>$data['fields']));
		
		return $data;
	}
	
	public function get_data_records_sub( $data, $trashed=0 ){
		$CI =& get_instance();
		
		$where_params = array($this->model->tblpref.'trashed'=>$trashed);
		
		if( $this->logged_user_only ){		
			$logged_user_id = $CI->logged->get_login_session('user_id');	
			$where_params = array_merge($where_params,
				array($this->model_user->tblname.'.'.$this->model_user->tblid=>$logged_user_id)
			);
		}
		
		$data = $this->records( $this->model, $data, $where_params );
		
		return $data;
	}
	
	public function get_data_view_sub( $data ){
		$CI =& get_instance();
			
		$id = $CI->input->get('id');
		$data['id'] = $id;
		
		$where_params_select = array();
		if( $this->logged_user_only ){		
			$logged_user_id = $CI->logged->get_login_session('user_id');
			
			$where_params = array(
				$this->model->tblid=>$id,
				$this->model_user->tblname.'.'.$this->model_user->tblid=>$logged_user_id
			);			
			$where_params_or_id = $where_params;			
			
			$where_params_select = array($this->model_user->tblid=>$logged_user_id);
		} else {
			$where_params_or_id = $id;
		}		
		
		$fields = $this->record( $this->model, $where_params_or_id );
		$data['fields'] = $fields;
		
		return $data;
	}
	
	public function get_data_edit_sub( $data ){
		$CI =& get_instance();
		
		$data = $this->get_data_view_sub( $data );
		
		$CI->load->library('ModuleTag',array('model'=>$this->model,'data'=>$data['fields']));
		
		return $data;
	}
}
?>