<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UsersRole {
	
	private $Data;
	private $model;
	private $model_user;
	private $model_log;
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	
	/* parameters */
	
    function __construct( $params=array() ){
        $CI =& get_instance();
		
		$params_default = array_merge(array(
			'model'=>NULL,
			'model_user'=>NULL,
			'model_log'=>NULL
			), $params
		);
		extract($params_default);
		
		$this->model = $model;
		$this->model_user = $model_user;
		$this->model_log = $model_log;
		
		$this->Data = $CI->Data;
    }

    function __destruct(){		
        $this->Data = NULL;
        $this->model = NULL;
		
        $this->model_user = NULL;
        $this->model_log = NULL;
    }
	
	public function record( $query_params_or_id=array() ){
				
		if( is_array($query_params_or_id) ){
			$where_params = $query_params_or_id;
		} else {	
			$where_params = array( $this->model->tblname.'.'.$this->model->tblid => $query_params_or_id ); 
		}
		
		$join_params = $this->model->set_join_params(); 
		$query_params = array('where_params'=>$where_params,'join_params'=>$join_params );
		
		$fields = $this->Data->get_record( $this->model, array(
			'where_params'=>$where_params,
			'join_params'=>$join_params 
		));
		
		return $fields;
	}
	
	public function records( $data, $where_params=array() ){ 
        $CI =& get_instance();
		
		$search_fields = $this->model->set_search_fields(); 
		$join_params = $this->model->set_join_params();
		$orderby = method_exists($this->model,'set_order_by')?$this->model->set_order_by():NULL;
		
		$query_params = array(
			'join_params'=>$join_params,
			'search_fields'=> $search_fields,
			'where_params'=> $where_params,
			'groupby'=> $CI->db->dbprefix.$this->model->tblname.'.'.$this->model->tblid,
			'orderby'=> $orderby,
			'ordertype'=>'DESC',
			'paging'=>TRUE
		);  
		$records = $this->Data->get_records( $this->model, $query_params ); 
		$data['record_rows'] = $records;
		$data['pagination'] =  $this->Data->pagination_link; 
		$data['per_page'] = $this->Data->per_page;
		$data['total_rows'] = $this->Data->total_rows;
		
		return $data;
	}
	
	public function save(){
		$CI =& get_instance();
		
		$model = $this->model;
		$id = $CI->input->post($model->tblid); 
		
		$capabilities_selected = $CI->input->post('capabilities_selected'); 
		$capabilities_serialized = serialize($capabilities_selected);
		
		$array_capabilities = array(
			$model->tblpref.'capabilities_serialized' => $capabilities_serialized
		);
		$field_data = $CI->input->post($model->tblname);
		$field_data = array_merge($field_data, $array_capabilities);
		
		$position = app_get_val($field_data,$model->tblpref.'position');
		
		if( $id ){
			$this->user_has_capability($model->urc_name,'edit');
			
			$update = $CI->records->save( $model, $field_data, $id );
			
			if( $update ){
				$notify_params = array('action'=>'update','type'=>'success','log'=>TRUE);
			} else {
				$notify_params = array('action'=>'update','type'=>'error','log'=>TRUE);
			}
			$CI->notify->set( $notify_params );
			
			redirect_current_page();
		} else {
			$this->user_has_capability($model->urc_name,'add');
			
			$array_published = array(
				$model->tblpref.'published'=>1
			);
			$field_data = array_merge($field_data, $array_published);
			
			$id = $CI->records->save( $model, $field_data );
			
			if( $id ){
				$notify_params = array('action'=>'add','type'=>'success','log'=>TRUE);
			} else {
				$notify_params = array('action'=>'add','type'=>'error','log'=>TRUE);
			}
			$CI->notify->set( $notify_params );
			
			redirect( site_url('c='.$model->page.'&m=edit&id='.$id) );
		}
	}
	
	public function delete(){
		$CI =& get_instance();
		
		$model = $this->model;
		$id = $CI->input->get('id');

		// $user_role = $this->record($model, $id);
		$user_role = $this->record( $id);
		$position = app_get_val($user_role,$model->tblpref.'position'); 
		
		if( $user_role ){
			$name = app_get_val($user_role, $model->tblpref.'name');
			
			if( in_array($name, $this->delete_exemption) == FALSE ){
				$delete = $this->Data->delete_data($model, array($model->tblid=>$id));
				
				if( $delete ){					
					$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE);
				} else {
					$notify_params = array('action'=>'delete','type'=>'error','log'=>TRUE);
				}
				$CI->notify->set( $notify_params );
			}
		}
		
		redirect( site_url('c='.$model->page) );
	}
	
	public function get_capability($capabilities, $urc_name, $field){
	 
		$result = isset($capabilities[$urc_name][$field])?$capabilities[$urc_name][$field]:'';
	  
		return $result;
	}
	
	public function check( $module, $name ){
		
		$CI =& get_instance();
		
		$dev_mode_urc = FALSE;
		
		$capabilities = $CI->logged->get_user_capabilities();
	 
		$result = $this->get_capability($capabilities, $module, $name);
	
		if( $dev_mode_urc ){
			echo '<br /><strong style="color:blue;">module: '.$module.' | name: '.$name.' | result: '.$result.'</strong>';
		}
		
		return $result;
	}
	
	public function user_has_capability( $module, $name, $value='yes' ){
		$CI =& get_instance();
		 
		$result = $this->check($module, $name); 
		if( $result != $value ){
		
			$data = array();
			$data['url_login'] = site_url('c='.$this->model_log->page.'&url_current='.urlencode( url_current() ));
			$data['url_logout'] = site_url('c='.$this->model_log->page.'&m=logout&url_current='.urlencode( url_current() ));
			
			echo $CI->load->view('error-no-user-capability-permission',$data,TRUE);
			exit();
		}
	}
	
	public function get_model_role( $role_name ){
		$CI =& get_instance();
		
		$capabilities = $CI->logged->get_user_capabilities();
		$model_capabilities = app_get_val($capabilities, $role_name);
		
		return $model_capabilities;		
	}

	public function get_data_view( $data ){
		$CI =& get_instance();
		
		$id = $CI->input->get('id');
		
		$fields = $this->record( $id );
		$data['fields']= $fields;
		
		$capabilities_serialized = app_get_val($fields, $this->model->tblpref.'capabilities_serialized');
		$data['selected_capabilities'] = unserialize($capabilities_serialized);
		
		$data['urc_standard_records'] = $this->model->get_models_urc();
		$data['urc_modules_records'] = $this->model->get_models_urc('modules/');
		
		return $data;
	}

	public function get_data_edit( $data ){
		$CI =& get_instance();
		
		$data = $this->get_data_view( $data );
		
		return $data;
	}
}
?>