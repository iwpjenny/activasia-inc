<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModUser {
	 
    function __construct(){
        $CI =& get_instance();
		$CI->load->model('User_Model', 'User');
		$this->User = $CI->User;
		$this->tblid = $CI->User->tblid;
		$this->tblpref = $CI->User->tblpref;
    }
	
	function get_login_user_fullname( $format='{firstname} {middlename} {lastname}', $text_transform='capitalize' ){ 
		/*---------------------------------------------*/
	 
				
		$login_user_details = $this->User->get_logged_user();
		$login_user_id = get_value($login_user_details,$this->tblid ); 
		$fullname = '';
		
		if( $login_user_id ){ 
			$user_details = $this->User->get_by_id( $login_user_id );
			
			$firstname = get_value($user_details,$this->tblpref.'firstname');
			$middlename = get_value($user_details,$this->tblpref.'midname');
			$lastname = get_value($user_details,$this->tblpref.'lastname');
			
			$fullname = $this->set_fullname_format( $firstname, $middlename, $lastname, $format, $text_transform );
		}
		
		return $fullname;
	}
	
	function get_user_fullname_by_id( $user_id, $format='{firstname} {middlename} {lastname}', $text_transform='capitalize' ){ 
		/*---------------------------------------------*/
		
		$user_details = $this->User->get_by_id( $user_id );
		
		$firstname = get_value($user_details,$this->tblpref.'firstname');
		$middlename = get_value($user_details,$this->tblpref.'midname');
		$lastname = get_value($user_details,$this->tblpref.'lastname');
		
		$fullname = $this->set_fullname_format( $firstname, $middlename, $lastname, $format, $text_transform );
		
		return $fullname;
	}
	
	function set_fullname_format( $firstname, $middlename, $lastname, $format='{firstname} {middlename} {lastname}', $text_transform='capitalize' ){
		
		$mi = substr($middlename, 0, 1);
		
		$format = str_replace('{firstname}', $firstname, $format);
		$format = str_replace('{middlename}', $middlename, $format);
		$format = str_replace('{mi}', $mi, $format);
		$fullname = str_replace('{lastname}', $lastname, $format);
		
		if( $text_transform == 'uppsercase' ){
			$fullname = strtoupper($fullname);
		} elseif($text_transform == 'lowercase') {
			$fullname = strtolower($fullname);
		} else {
			$fullname = ucwords($fullname);
		}
		
		return $fullname;
	}
     
}
?>