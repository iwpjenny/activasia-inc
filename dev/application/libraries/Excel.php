<?php
if (!defined('BASEPATH')) exit('No direct script access allowed'); 

require_once APPPATH."/third_party/Classes/PHPExcel.php";

class Excel extends PHPExcel {
    public function __construct() {
        parent::__construct();
		
    }
	
	public function export_report( $results_array=array(), $filename='' , $columns){
		$CI =& get_instance(); 
		$CI->excel->setActiveSheetIndex( 0 ); 
		$CI->excel->getActiveSheet()->setTitle( $filename );
		$CI->excel->getDefaultStyle()->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_CENTER );		
		
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		); 
		$row = 2;
		
		foreach($columns as $key=>$column){
			$CI->excel->getActiveSheet()->getColumnDimension( $key )->setAutoSize(false);
			$CI->excel->getActiveSheet()->getColumnDimension( $key )->setWidth($column);
		}
		$keys = array_keys($columns);
		$first_column = reset($keys).'1';
		$last_column = end($keys).'1';
		$bold = $first_column.':'.$last_column;
		$CI->excel->getActiveSheet()->getStyle($bold)->getFont()->setBold(true);
		
		foreach( $results_array as $key => $value ){
			$col = 0;
			foreach( $value as $title => $text ){
				$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,1)->applyFromArray( $styleArray );
				$CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $title );
				$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,$row)->applyFromArray( $styleArray ); 
				$CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $text );
				 
				if( is_numeric( $text )){ 
					$cell =  $CI->excel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getColumn();   
					$CI->excel->getActiveSheet()->getStyle($cell.$row)->getNumberFormat()->setFormatCode('#,##0.00');
				}
				$col++; 
			} 
			$row++;
		} 

		$filename= $filename.'.xls'; //save our workbook as this file name 
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="'.$filename.'"'); 
		header('Cache-Control: max-age=0');
		  
		$objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel5');  
		$objWriter->save('php://output'); 
	} 
	
	public function export_report_individual( $results_array=array(), $filename='' , $columns){  
		$CI =& get_instance(); 
		$CI->excel->setActiveSheetIndex( 0 ); 
		$CI->excel->getActiveSheet()->setTitle( $filename );
		$CI->excel->getDefaultStyle()->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_CENTER );		
		
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);  
		$result_ce = $results_array['head'];
		$row = 1;
		$col = 1; 
		foreach( $result_ce as $title => $text ){ 
			$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,1);
			$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $title );
			$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,$row);
			$CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $text );
			if( is_numeric( $text )){ 
				$cell =  $CI->excel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getColumn();   
				$CI->excel->getActiveSheet()->getStyle($cell.$row)->getNumberFormat()->setFormatCode('#,##0.00');
			}
			$row++;
		} 
		$row_original = $row + 1;  
		$row += 2;    
		
		$result_ce_items = $results_array['items'];
		foreach( $result_ce_items as $key => $value ){
			$col = 0;
			foreach( $value as $title => $text ){
				$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,1);
				
				if($row_original == $row - 1 ){ 
					$CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row_original, $title );
				}
				$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,$row);
				$CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $text );
				if( is_numeric( $text )){ 
					$cell =  $CI->excel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getColumn();   
					$CI->excel->getActiveSheet()->getStyle($cell.$row)->getNumberFormat()->setFormatCode('#,##0.00');
				}
				$col++; 
			} 
			$row++;
		} 
		
		$filename= $filename.'.xls'; //save our workbook as this file name 
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="'.$filename.'"'); 
		header('Cache-Control: max-age=0');
		  
		$objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel5');  
		$objWriter->save('php://output'); 
	} 
	
	public function export_report_individual_ca( $results_array=array(), $filename='' , $columns){  

		$CI =& get_instance(); 
		$CI->excel->setActiveSheetIndex( 0 ); 
		$CI->excel->getActiveSheet()->setTitle( $filename );
		$CI->excel->getDefaultStyle()->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_LEFT ); 
		$total_amount = 0;
		
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);  
		$BStyle = array(
		  'borders' => array(
			'bottom' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		  )
		);
		$topborder = array(
		  'borders' => array(
			'top' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THICK
			)
		  )
		);
		$rightborder = array(
		  'borders' => array(
			'right' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THICK
			)
		  )
		);
		$bold_style = array(
			'font' => array(
				'bold' => true
			)
		);
		$grayfont = array(
			'font'  => array( 
				'color' => array('rgb' => '808080'), 
		));		
		$small_font = array(
			'font'  => array( 
				 'size' => 10
		));		
		$result_ca = $results_array['head'];    
		// row: 1 start col: 0 start
		// $CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $text ); 
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'HR-Finance-02-2015' );
		$CI->excel->getActiveSheet()->getStyle('G1')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->getStyle('G1')->applyFromArray($grayfont);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, '291 Saint Joseph Street, Oranbo Drive, Pasig City' );
		$CI->excel->getActiveSheet()->getStyle('D2')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->getStyle('D2')->applyFromArray($grayfont);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 3, 'Tel No. (632) 637-6798  Fax No.: (632) 631-1674' );
		$CI->excel->getActiveSheet()->getStyle('D3')->applyFromArray($grayfont);
		$CI->excel->getActiveSheet()->mergeCells('C6:H6');
		$CI->excel->getActiveSheet()->mergeCells('C7:H7');
		$CI->excel->getActiveSheet()->getStyle('C6:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$CI->excel->getActiveSheet()->getStyle('C7:H7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'CASH ADVANCE REQUEST FORM' );
		$CI->excel->getActiveSheet()->getStyle('C6')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 7, 'Finance Department' );
		$CI->excel->getActiveSheet()->getStyle('C7')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->getStyle('C7')->applyFromArray($grayfont);
		$CI->excel->getActiveSheet()->getStyle('B9:D9')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 9, 'Date: ' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 9, date("Y-m-d") );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 9, 'Project Code: ' ); 
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 9, $result_ca['CE #']); 
		$CI->excel->getActiveSheet()->getStyle('F9')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->getStyle('G9:I9')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 11, 'Project Title: ' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 11, $result_ca['Project Name'] );
		$CI->excel->getActiveSheet()->getStyle('A11')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->getStyle('B11:I11')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 12, 'For Release to: ' );
		$CI->excel->getActiveSheet()->getStyle('A12')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->getStyle('B12:D12')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 12, 'Date Needed: ' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 12,$result_ca['Date Needed'] );
		$CI->excel->getActiveSheet()->getStyle('F12')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->getStyle('G12:I12')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 13, 'Type of Activity: ' );
		$CI->excel->getActiveSheet()->getStyle('A13')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 13, 'Events' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 14, 'Activation' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 15, 'Travel' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 16, 'RF/PCF' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 17, 'AVP' ); 
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 13, 'Purpose:' );
		$CI->excel->getActiveSheet()->getStyle('F13')->applyFromArray($bold_style);		
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 14, 'Pre-Work' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 15, 'Manpower' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 16, 'Talent Fee' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 17, 'Per Diem' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 14, 'Panel Hire' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 15, 'Materials/Supplies' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 16, 'Travel' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 17, 'Others: ' );
		$CI->excel->getActiveSheet()->getStyle('I17')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->getStyle('H18:I18')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 19, 'Please provide complete details: ' );
		$CI->excel->getActiveSheet()->getStyle('A19')->applyFromArray($bold_style);	
		$CI->excel->getActiveSheet()->mergeCells('A20:C20');
		$CI->excel->getActiveSheet()->mergeCells('H20:I20');
		$CI->excel->getActiveSheet()->mergeCells('D20:G20');
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 20, 'PARTICULARS' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 20, 'DESCRIPTION' );
		$CI->excel->getActiveSheet()->getStyle('A20')->applyFromArray($bold_style);	
		$CI->excel->getActiveSheet()->getStyle('D20')->applyFromArray($bold_style);	
		$CI->excel->getActiveSheet()->getStyle('A20:I20')->applyFromArray($topborder);	 
		$CI->excel->getActiveSheet()->getStyle('C20')->applyFromArray($rightborder);	
		$CI->excel->getActiveSheet()->getStyle('G20')->applyFromArray($rightborder);	
		$CI->excel->getActiveSheet()->getStyle('A20:G20')->applyFromArray($BStyle);	
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 20, 'AMOUNT' );
		$CI->excel->getActiveSheet()->getStyle('H20')->applyFromArray($bold_style); 	
		$CI->excel->getActiveSheet()->getStyle('H20:I20')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->getStyle('I20')->applyFromArray($rightborder);		
		$row = 21;
		if($results_array['items']){
			$result_ce_items = $results_array['items'];
			$total_amount = 0;
			foreach( $result_ce_items as $key => $value ){  
				$particulars = $value['Particulars'];
				$description = $value['Description'];
				$amount = $value['Total']; 
				$string_1 = 'A'.$row.':'.'C'.$row; 
				$string_2 = 'H'.$row.':'.'I'.$row;  
				$string_3 = 'D'.$row.':'.'G'.$row;  
				$CI->excel->getActiveSheet()->mergeCells($string_1);  
				$CI->excel->getActiveSheet()->mergeCells($string_2);  
				$CI->excel->getActiveSheet()->mergeCells($string_3);  
				$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $particulars );
				$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $description );
				$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $amount );
				$CI->excel->getActiveSheet()->getStyle($string_2)->getNumberFormat()->setFormatCode('#,##0.00');
				$CI->excel->getActiveSheet()->getStyle($string_1)->applyFromArray($BStyle);					
				$CI->excel->getActiveSheet()->getStyle($string_2)->applyFromArray($BStyle);	
				$CI->excel->getActiveSheet()->getStyle($string_3)->applyFromArray($BStyle);	
				$CI->excel->getActiveSheet()->getStyle($string_1)->applyFromArray($rightborder);					
				$CI->excel->getActiveSheet()->getStyle($string_2)->applyFromArray($rightborder);					
				$CI->excel->getActiveSheet()->getStyle($string_3)->applyFromArray($rightborder);		
				$CI->excel->getActiveSheet()->getStyle($string_3)->applyFromArray($small_font);					
				$row++;
				$total_amount += $amount;
			} 
		} 
		$row = $row++;
		$string = 'H'.$row.':'.'I'.$row; 
		$CI->excel->getActiveSheet()->mergeCells($string);  
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, 'TOTAL AMOUNT:' );
		$CI->excel->getActiveSheet()->getStyle($string)->getNumberFormat()->setFormatCode('#,##0.00');
		$string = 'F'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($bold_style);	
		$string = 'A'.$row.':'.'G'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($topborder);	
		$string = 'H'.$row.':'.'I'.$row;
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($topborder);	
		$CI->excel->getActiveSheet()->getStyle($string)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
		$CI->excel->getActiveSheet()->mergeCells('H20:I20');
		$result_ca = $results_array['head'];  
		// $ca_id = $result_ca['total'];   	 
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $total_amount );
		$row += 2;
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, 'Requested by:' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, 'Noted / Recommendation by:' );
		$row += 2;
		$string = 'A'.$row.':'.'D'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($BStyle);
		$string = 'F'.$row.':'.'G'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($BStyle);
		$string = 'I'.$row.':'.'K'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($BStyle);
		$row += 1;
		
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, 'Signature over printed Name' );	
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, 'Execom / Dept Head' ); 
		$string = 'F'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($bold_style);	
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, 'Perry L. Villa III' );  
		$string = 'I'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($bold_style);	
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row+1, 'COO' );  
		$row += 2;
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, 'Important Note:' ); 
		$string = 'A'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+1, 'Please attach all pertinent documents to faciliate processing of your '); 
		$string = 'A'.($row+1); 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+2, 'cash advance request. Please READ the undertaking at the back. '); 
		$string = 'A'.($row+2); 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($bold_style);
		
		$row += 5;
		$string = 'A'.$row.':'.'I'.$row; 
		$CI->excel->getActiveSheet()->mergeCells($string);  	
		$CI->excel->getActiveSheet()->getStyle($string)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, 'ACCOUNTABILITY AND AUTHORITY TO DEDUCT');
		$string = 'A'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($bold_style);		
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+1, 'It is hereby agreed and understood that I shall fully liquidated the above amount subject of my advance and support the'); 
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+2, 'regulations of ActivAsia Inc, failing which I agree that the amount due and unliquidated shall be deducted from my'); 
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+3, 'salary at the company.'."'".'s discretion.');  	
		$row += 5;
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, 'I indemnify and hold free and harmless ActivAsia from any and all claims arising from such salary deduction.');  	
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+2, 'Signed:');  
		$row_2 = $row +3;
		$string = 'A'.$row_2.':'.'D'.$row_2; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+4, 'Signature over printed name / Date');  	
		$filename= $filename.'.xls'; //save our workbook as this file name 
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="'.$filename.'"'); 
		header('Cache-Control: max-age=0');
		  
		$objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel5');  
		$objWriter->save('php://output'); 
	} 
	
	public function export_report_individual_ca_nodownload( $results_array=array(), $filename='' , $file_path, $columns){ 
		// printx($results_array);
		$CI =& get_instance(); 
		$CI->excel->setActiveSheetIndex( 0 ); 
		$CI->excel->getActiveSheet()->setTitle( $filename );
		$CI->excel->getDefaultStyle()->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_LEFT ); 
		
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);  
		$BStyle = array(
		  'borders' => array(
			'bottom' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		  )
		);
		$topborder = array(
		  'borders' => array(
			'top' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THICK
			)
		  )
		);
		$rightborder = array(
		  'borders' => array(
			'right' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THICK
			)
		  )
		);
		$bold_style = array(
			'font' => array(
				'bold' => true
			)
		);
		$grayfont = array(
			'font'  => array( 
				'color' => array('rgb' => '808080'), 
		));		
		$small_font = array(
			'font'  => array( 
				 'size' => 10
		));		
		$result_ca = $results_array['head'];    
		// row: 1 start col: 0 start
		// $CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $text ); 
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'HR-Finance-02-2015' );
		$CI->excel->getActiveSheet()->getStyle('G1')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->getStyle('G1')->applyFromArray($grayfont);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, '291 Saint Joseph Street, Oranbo Drive, Pasig City' );
		$CI->excel->getActiveSheet()->getStyle('D2')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->getStyle('D2')->applyFromArray($grayfont);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 3, 'Tel No. (632) 637-6798  Fax No.: (632) 631-1674' );
		$CI->excel->getActiveSheet()->getStyle('D3')->applyFromArray($grayfont);
		$CI->excel->getActiveSheet()->mergeCells('C6:H6');
		$CI->excel->getActiveSheet()->mergeCells('C7:H7');
		$CI->excel->getActiveSheet()->getStyle('C6:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$CI->excel->getActiveSheet()->getStyle('C7:H7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'CASH ADVANCE REQUEST FORM' );
		$CI->excel->getActiveSheet()->getStyle('C6')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 7, 'Finance Department' );
		$CI->excel->getActiveSheet()->getStyle('C7')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->getStyle('C7')->applyFromArray($grayfont);
		$CI->excel->getActiveSheet()->getStyle('B9:D9')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 9, 'Date: ' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 9, date("Y-m-d") );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 9, 'Project Code: ' ); 
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 9, $result_ca['CE #']); 
		$CI->excel->getActiveSheet()->getStyle('F9')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->getStyle('G9:I9')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 11, 'Project Title: ' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 11, $result_ca['Project Name'] );
		$CI->excel->getActiveSheet()->getStyle('A11')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->getStyle('B11:I11')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 12, 'For Release to: ' );
		$CI->excel->getActiveSheet()->getStyle('A12')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->getStyle('B12:D12')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 12, 'Date Needed: ' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 12,$result_ca['Date Needed'] );
		$CI->excel->getActiveSheet()->getStyle('F12')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->getStyle('G12:I12')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 13, 'Type of Activity: ' );
		$CI->excel->getActiveSheet()->getStyle('A13')->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 13, 'Events' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 14, 'Activation' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 15, 'Travel' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 16, 'RF/PCF' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 17, 'AVP' ); 
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 13, 'Purpose:' );
		$CI->excel->getActiveSheet()->getStyle('F13')->applyFromArray($bold_style);		
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 14, 'Pre-Work' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 15, 'Manpower' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 16, 'Talent Fee' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 17, 'Per Diem' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 14, 'Panel Hire' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 15, 'Materials/Supplies' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 16, 'Travel' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 17, 'Others: ' );
		$CI->excel->getActiveSheet()->getStyle('I17')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->getStyle('H18:I18')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 19, 'Please provide complete details: ' );
		$CI->excel->getActiveSheet()->getStyle('A19')->applyFromArray($bold_style);	
		$CI->excel->getActiveSheet()->mergeCells('A20:C20');
		$CI->excel->getActiveSheet()->mergeCells('H20:I20');
		$CI->excel->getActiveSheet()->mergeCells('D20:G20');
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 20, 'PARTICULARS' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 20, 'DESCRIPTION' );
		$CI->excel->getActiveSheet()->getStyle('A20')->applyFromArray($bold_style);	
		$CI->excel->getActiveSheet()->getStyle('D20')->applyFromArray($bold_style);	
		$CI->excel->getActiveSheet()->getStyle('A20:I20')->applyFromArray($topborder);	 
		$CI->excel->getActiveSheet()->getStyle('C20')->applyFromArray($rightborder);	
		$CI->excel->getActiveSheet()->getStyle('G20')->applyFromArray($rightborder);	
		$CI->excel->getActiveSheet()->getStyle('A20:G20')->applyFromArray($BStyle);	
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 20, 'AMOUNT' );
		$CI->excel->getActiveSheet()->getStyle('H20')->applyFromArray($bold_style); 	
		$CI->excel->getActiveSheet()->getStyle('H20:I20')->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->getStyle('I20')->applyFromArray($rightborder);		
		$row = 21;
		if($results_array['items']){
			$result_ce_items = $results_array['items'];
			$total_amount = 0;
			foreach( $result_ce_items as $key => $value ){  
				$particulars = $value['Particulars'];
				$description = $value['Description'];
				$amount = $value['Total']; 
				$string_1 = 'A'.$row.':'.'C'.$row; 
				$string_2 = 'H'.$row.':'.'I'.$row;  
				$string_3 = 'D'.$row.':'.'G'.$row;  
				$CI->excel->getActiveSheet()->mergeCells($string_1);  
				$CI->excel->getActiveSheet()->mergeCells($string_2);  
				$CI->excel->getActiveSheet()->mergeCells($string_3);  
				$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $particulars );
				$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $description );
				$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $amount );
				$CI->excel->getActiveSheet()->getStyle($string_2)->getNumberFormat()->setFormatCode('#,##0.00');
				$CI->excel->getActiveSheet()->getStyle($string_1)->applyFromArray($BStyle);					
				$CI->excel->getActiveSheet()->getStyle($string_2)->applyFromArray($BStyle);	
				$CI->excel->getActiveSheet()->getStyle($string_3)->applyFromArray($BStyle);	
				$CI->excel->getActiveSheet()->getStyle($string_1)->applyFromArray($rightborder);					
				$CI->excel->getActiveSheet()->getStyle($string_2)->applyFromArray($rightborder);					
				$CI->excel->getActiveSheet()->getStyle($string_3)->applyFromArray($rightborder);		
				$CI->excel->getActiveSheet()->getStyle($string_3)->applyFromArray($small_font);					
				$row++;
				$total_amount += $amount;
			} 
		} 
		$row = $row++;
		$string = 'H'.$row.':'.'I'.$row; 
		$CI->excel->getActiveSheet()->mergeCells($string);  
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, 'TOTAL AMOUNT:' );
		$CI->excel->getActiveSheet()->getStyle($string)->getNumberFormat()->setFormatCode('#,##0.00');
		$string = 'F'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($bold_style);	
		$string = 'A'.$row.':'.'G'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($topborder);	
		$string = 'H'.$row.':'.'I'.$row;
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($topborder);	
		$CI->excel->getActiveSheet()->getStyle($string)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
		$CI->excel->getActiveSheet()->mergeCells('H20:I20');
		$result_ca = $results_array['head'];  
		// $ca_id = $result_ca['total'];   	 
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $total_amount );
		$row += 2;
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, 'Requested by:' );
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, 'Noted / Recommendation by:' );
		$row += 2;
		$string = 'A'.$row.':'.'D'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($BStyle);
		$string = 'F'.$row.':'.'G'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($BStyle);
		$string = 'I'.$row.':'.'K'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($BStyle);
		$row += 1;
		
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, 'Signature over printed Name' );	
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, 'Execom / Dept Head' ); 
		$string = 'F'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($bold_style);	
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, 'Perry L. Villa III' );  
		$string = 'I'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($bold_style);	
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row+1, 'COO' );  
		$row += 2;
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, 'Important Note:' ); 
		$string = 'A'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+1, 'Please attach all pertinent documents to faciliate processing of your '); 
		$string = 'A'.($row+1); 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($bold_style);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+2, 'cash advance request. Please READ the undertaking at the back. '); 
		$string = 'A'.($row+2); 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($bold_style);
		
		$row += 5;
		$string = 'A'.$row.':'.'I'.$row; 
		$CI->excel->getActiveSheet()->mergeCells($string);  	
		$CI->excel->getActiveSheet()->getStyle($string)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, 'ACCOUNTABILITY AND AUTHORITY TO DEDUCT');
		$string = 'A'.$row; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($bold_style);		
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+1, 'It is hereby agreed and understood that I shall fully liquidated the above amount subject of my advance and support the'); 
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+2, 'regulations of ActivAsia Inc, failing which I agree that the amount due and unliquidated shall be deducted from my'); 
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+3, 'salary at the company.'."'".'s discretion.');  	
		$row += 5;
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, 'I indemnify and hold free and harmless ActivAsia from any and all claims arising from such salary deduction.');  	
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+2, 'Signed:');  
		$row_2 = $row +3;
		$string = 'A'.$row_2.':'.'D'.$row_2; 
		$CI->excel->getActiveSheet()->getStyle($string)->applyFromArray($BStyle);
		$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+4, 'Signature over printed name / Date');  	
		
		$filename= $filename.'.xls'; //save our workbook as this file name   
		$objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel5');      
		$objWriter->save(str_replace(__FILE__,$file_path,__FILE__));  
	} 
	
	
	
	public function export_report_individual_nodownload( $results_array=array(), $filename, $filepath='', $columns){ 
		$CI =& get_instance(); 
		$CI->excel->setActiveSheetIndex( 0 ); 
		$CI->excel->getActiveSheet()->setTitle( $filename );
		$CI->excel->getDefaultStyle()->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_CENTER );		
		
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);  
		$result_ce = $results_array['head'];
		$row = 1;
		$col = 1; 
		foreach( $result_ce as $title => $text ){ 
			$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,1);
			$CI->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $title );
			$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,$row);
			$CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $text );
			if( is_numeric( $text )){ 
				$cell =  $CI->excel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getColumn();   
				// $CI->excel->getActiveSheet()->getStyle($cell.$row)->getNumberFormat()->setFormatCode('#,##0.00');
			}
			$row++;
		} 
		$row_original = $row + 1;  
		$row += 2;    
		
		$result_ce_items = $results_array['items'];
		foreach( $result_ce_items as $key => $value ){
			$col = 0;
			foreach( $value as $title => $text ){
				$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,1);
				
				if($row_original == $row - 1 ){ 
					$CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row_original, $title );
				}
				$CI->excel->getActiveSheet()->getStyleByColumnAndRow($col,$row);
				$CI->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $text );
				if($col > 1){
					if( is_numeric( $text )){ 
						$cell =  $CI->excel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getColumn();   
						$CI->excel->getActiveSheet()->getStyle($cell.$row)->getNumberFormat()->setFormatCode('#,##0.00');
				}
				}
				$col++; 
			} 
			$row++;
		} 
		 // printx($filepath);
		$filename= $filename.'.xls'; //save our workbook as this file name   
		$objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel5');      
		$objWriter->save(str_replace(__FILE__,$filepath,__FILE__)); 
	} 
}

