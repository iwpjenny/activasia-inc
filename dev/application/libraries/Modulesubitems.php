<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModuleSubItems {
	
	private $Data;
	private $views_editable = TRUE;
	
	/* return */
	public $error;
	public $result;
	public $notification;
	
	/* parameters */
	public $id = NULL;
	public $page = NULL;
	public $model = NULL;
	public $model_parent = NULL;
	public $model_items = NULL;
	public $location;
	public $logged_user_only = FALSE;
	public $fields;
	public $field_data = array();
	public $fields_post = array();
	public $primary_field;

    function __construct( $params=array() ){
        $CI =& get_instance();
		
		$params_default = array_merge(array(
			'model'=>NULL,
			'model_parent'=>NULL,
			'model_items'=>NULL,
			'location'=>NULL,
			'primary_field'=>'title'
			), $params
		);
		extract($params_default);
		
		$this->model = $model;
		$this->model_parent = $model_parent;
		$this->model_items = $model_items;
		$this->location = $location;
		$this->primary_field = $primary_field;
		
		$this->Data = $CI->Data;
    }

    function __destruct(){
        $this->id = NULL;
        $this->Data = NULL;
		$this->views_editable = NULL;
		
		$this->page = NULL;
		$this->model = NULL;
		$this->model_parent = NULL;
		$this->model_items = NULL;
		$this->location = NULL;
		$this->logged_user_only = NULL;
		$this->fields = NULL;
		$this->field_data = NULL;
		$this->fields_post = NULL;
    }
	
	public function modal( $model, $model_items, $parent_id ){ 
		$CI =& get_instance();
		
		$contents = '';
		$model_parent = $this->model_parent;
		$page = $CI->method->get_model_page( $model );
				
		if( $parent_id ){
			$data['capabilities'] = $CI->logged->get_user_capabilities();
			$data['settings'] = $CI->setconf->all();
			$data['location'] = $this->location;
			$data['views_editable'] = $this->views_editable;			
			$data['id'] = '';
			$data['parent_id'] = $parent_id;
			$data['model'] = $model;
			$data['model_items'] = $model_items;
			$data['model_parent'] = $this->model_parent;
			$data['name'] = $model->name;
			$data['page'] = $page;
			$data['tblid'] = $model->tblid;
			$data['tblpref'] = $model->tblpref;
			$data['tblname'] = $model->tblname;
			$data['fields'] = array();
			$data['form_action'] = site_url('c='.$model->page);
			
			$data['records_selected'] = $this->grid_sub_items_selected( $model, $model_parent, $model_items, $parent_id );
			$data['records_selection'] = $this->grid_sub_items_selection( $model, $model_parent, $model_items, $parent_id );
			
			$CI->load->library('ModuleTag',array('model'=>$model));
			
			$contents = $CI->load->view($this->location.'/modules/modal-sub-items',$data,TRUE);
		}
				
		return $contents;
	}
	
	public function grid_sub_items_selected( $model, $model_parent, $model_items, $parent_id ){ 
		$CI =& get_instance();
		
		$query_params = array(
			'join_params'=>$model->set_join_params(),
			'where_params'=>array(
				$model->tblname.'.'.$model_parent->tblid=>$parent_id,
				$model_items->tblname.'.'.$model_items->tblpref.'trashed'=>0
			),
			'orderby'=>$model->tblid,
			'ordertype'=>'DESC'
		);
			
		$records = $this->Data->get_records( $model, $query_params ); 
		
		return $records;
	}
	
	public function grid_sub_items_selection( $model, $model_parent, $model_items, $parent_id ){ 
		$CI =& get_instance();
		
		$records_selected = $this->grid_sub_items_selected( $model, $model_parent, $model_items, $parent_id );
		
		$selected_items = array();
		if( $records_selected ){
			foreach( $records_selected as $col ){
				$item_id = app_get_val($col,$model_items->tblid);
				$selected_items[] = $item_id;				
			}
		}
		
		$query_params = array(
			'join_params'=>$model_items->set_join_params(),
			'where_params'=>array(
				$model_items->tblname.'.'.$model_items->tblpref.'trashed'=>0
			),
			'where_not_in_field'=>$model_items->tblname.'.'.$model_items->tblid,
			'where_not_in'=>$selected_items,
			//'orderby'=>$model->tblid,
			'orderby'=>$model_items->tblpref.$this->primary_field,
			'ordertype'=>'ASC'
		);
			
		$records = $this->Data->get_records( $model_items, $query_params ); 
		
		return $records;
	}
	
	public function modal_selected_content( $data, $model, $model_parent, $model_items, $parent_id ){
		$CI =& get_instance();
		
		$data['records_selected'] = $this->grid_sub_items_selected( $model, $model_parent, $model_items, $parent_id );
				
		$contents = $CI->load->view($this->location.'/modules/'.$model->view.'/items-grid-tbody',$data,TRUE);
				
		return $contents;
	}
	
	public function modal_selection_content( $data, $model, $model_parent, $model_items, $parent_id ){
		$CI =& get_instance();
		
		$data['records_selection'] = $this->grid_sub_items_selection( $model, $model_parent, $model_items, $parent_id );
				
		$contents = $CI->load->view($this->location.'/modules/'.$model->view.'/items-selection',$data,TRUE);
				
		return $contents;
	}
	
	public function json_add(){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$model = $this->model;
		$model_parent = $this->model_parent;
		$model_items = $this->model_items;
		/*---------------------------------------------*/
		
		$parent_id = $CI->input->post('parent_id');
		$item_id_array = $CI->input->post('item_id_array');
		$custom_field_array = $CI->input->post('custom_field_array');
		
		$parent_field = $model_parent->tblid;
		$item_field = $model_items->tblid;
				
		$result = FALSE;
		if( $item_id_array ){
			foreach( $item_id_array as $key=>$item_id ){
				$field_data = array(
					$parent_field=>$parent_id,
					$item_field=>$item_id
				);
				if( $custom_field_array ){
					foreach( $custom_field_array as $custom_field_key=>$custom_field_value ){						
						$custom_field_value = $custom_field_array[$custom_field_key][$key];
						$field_data[$model->tblpref.$custom_field_key] = $custom_field_value;
					}
				}
				$result = $CI->Data->insert_data( $model, $field_data );
			}
			$result = TRUE;
		}
		if( $result ){					
			$notify_params = array('action'=>'add','type'=>'success','log'=>TRUE);
		} else {
			$notify_params = array('action'=>'add','type'=>'error','log'=>TRUE);
		}
		$CI->notify->set( $notify_params );
		$notification = $CI->notify->get( array('size'=>'sm','start','end') );
		
		$this->json_return_grid_tbody( $model, $model_parent, $model_items, $parent_id, $notification, $result );
	}
	
	public function json_save(){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$model = $this->model;
		$model_parent = $this->model_parent;
		$model_items = $this->model_items;
		/*---------------------------------------------*/
		
		$id = $CI->input->post('id');
		$parent_id = $CI->input->post('parent_id');
		$field_data = $CI->input->post('fields');
		//printr($field_data);
		$save = $this->Data->update_data( $model, $field_data, array( $model->tblid=>$id ) );
		
		$result = FALSE;
		if( $save ){					
			$notify_params = array('action'=>'update','type'=>'success','log'=>TRUE);
			$result = TRUE;
		} else {
			$notify_params = array('action'=>'update','type'=>'error','log'=>TRUE);
		}
		$CI->notify->set( $notify_params );
		$notification = $CI->notify->get( array('size'=>'sm','start','end') );
		
		$this->json_return_grid_tbody( $model, $model_parent, $model_items, $parent_id, $notification, $result );
	}
	
	public function json_edit(){
		$CI =& get_instance();
		
		$model = $this->model;
		$model_items = $this->model_items;
		$page = $CI->method->get_model_page( $model );
		
		$id = $CI->input->post('id');
		
		$main_details = $this->Data->get_record($model,array('where_params'=>array($model->tblid =>$id)));
		
		$result = FALSE;
		$main_fields = array();
		if( $main_details ){
			foreach($main_details as $key=>$val){
				$main_fields[$key] = $val;
			}			
			$result = TRUE;
		}
		
		$item_id = app_get_val($main_details,$model_items->tblid);
		$item_details = $this->Data->get_record($model_items,array('where_params'=>array($model_items->tblid =>$item_id)));
		
		$item_fields = array();
		if( $item_details ){
			foreach($item_details as $key=>$val){
				if( $model_items->tblid != $key ){
					$key = str_replace($model_items->tblpref,'',$key);
				}
				/* $key = str_replace($model_items->tblpref,'',$key); */
				$item_fields[$key] = $val;
			}
		}
		
		$json = array();
		$json['main_fields'] = $main_fields;
		$json['item_fields'] = $item_fields;
		$json['page'] = $page;
		$json['notification'] = '';
		$json['result'] = $result;
		$json['redirect_url'] = '';
		
		echo json_encode($json);
		exit();
	}
	
	public function json_delete(){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$model = $this->model;
		$model_parent = $this->model_parent;
		$model_items = $this->model_items;
		/*---------------------------------------------*/
		
		$parent_id = $CI->input->post('parent_id');
		$id = $CI->input->post('id');
		
		$details = $this->Data->get_record( $model, array( 'where_params'=>array($model->tblid=>$id) ) ); 
		
		$result = FALSE;
		if( $details ){			
			$delete = $this->Data->delete_data( $model, array($model->tblid=>$id) );			
			if( $delete ){
				$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE);
				$result = TRUE;
			} else {
				$notify_params = array('action'=>'delete','type'=>'error','log'=>TRUE);
			}
		} else {
			$notify_params = array('action'=>'delete','type'=>'not-exist','log'=>TRUE);
		}
		$CI->notify->set( $notify_params );
		$notification = $CI->notify->get( array('size'=>'sm','start','end') );
		
		$this->json_return_grid_tbody( $model, $model_parent, $model_items, $parent_id, $notification, $result );
	}
	
	public function json_delete_multiple(){
		$CI =& get_instance();
		/*---------------------------------------------*/
		$model = $this->model;
		$model_parent = $this->model_parent;
		$model_items = $this->model_items;
		/*---------------------------------------------*/
		
		$parent_id = $CI->input->post('parent_id');
		$id = $CI->input->post('id');
		$item_id_array = $CI->input->post('item_id_array');
		
		$result = FALSE;
		$delete = $this->Data->delete( $model, array('where_in_field'=>$model->tblid,'where_in'=>$item_id_array) );			
		if( $delete ){
			$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE);
			$result = TRUE;
		} else {
			$notify_params = array('action'=>'delete','type'=>'error','log'=>TRUE);
		}
		$CI->notify->set( $notify_params );
		$notification = $CI->notify->get( array('size'=>'sm','start','end') );
		
		$this->json_return_grid_tbody( $model, $model_parent, $model_items, $parent_id, $notification, $result );
	}
	
	public function json_return_grid_tbody( $model, $model_parent, $model_items, $parent_id, $notification, $result, $id='' ){
		$CI =& get_instance();		
		$page = $CI->method->get_model_page( $model );
				
		$data['settings'] = $CI->setconf->all();
		$data['capabilities'] = $CI->logged->get_user_capabilities();
		
		$data['model'] = $model;
		$data['page'] = $page;
		$data['parent_id'] = $parent_id;
		$data['model_items'] = $model_items;
		$data['views_editable'] = $this->views_editable;
		
		$modal_selected_content = $this->modal_selected_content( $data, $model, $model_parent, $model_items, $parent_id );		
		$modal_selection_content = $this->modal_selection_content( $data, $model, $model_parent, $model_items, $parent_id );
		
		$json = array();
		$json['modal_selected_content'] = $modal_selected_content;
		$json['modal_selection_content'] = $modal_selection_content;
		$json['page'] = $page;
		$json['notification'] = $notification;
		$json['result'] = $result;
		$json['redirect_url'] = '';
		
		echo json_encode($json);
		exit();
	}
}
?>