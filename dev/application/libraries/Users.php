<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users {
	
	private $Data;
	private $folder;
	private $model;
	private $model_role;
	private $model_profile;
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	
	/* parameters */
	public $id = NULL;
	public $session_user_id = FALSE;
	public $exempted_role = array('developer','iwpadmin');
	public $fields_post;

    function __construct( $params=array() ){
        $CI =& get_instance();
		
		$params_default = array_merge(array(
			'model'=>NULL,
			'model_role'=>NULL,
			'model_logs'=>NULL,
			'model_profile'=>NULL,
			), $params
		);
		extract($params_default);
		
		$this->model = $model;
		$this->model_role = $model_role;
		$this->model_logs = $model_logs;
		$this->model_profile = $model_profile;
		
		$this->Data = $CI->Data;
		
		$params = array(
			'model'=>$model,
			'model_profile'=>$model_profile
		);
		$CI->load->library('pic',$params);
    }

    function __destruct(){
        $this->exempted_role = NULL;
		
        $this->Data = NULL;
        $this->model = NULL;
        $this->model_role = NULL;
        $this->model_logs = NULL;
        $this->model_profile = NULL;
        $this->fields_post = NULL;
    }
	
	public function record( $query_params_or_id=array() ){
				
		if( is_array($query_params_or_id) ){
			$where_params = $query_params_or_id;
		} else {	
			$where_params = array( $this->model->tblname.'.'.$this->model->tblid => $query_params_or_id ); 
		}
		
		$join_params = $this->model->set_join_params(); 
		$query_params = array('where_params'=>$where_params,'join_params'=>$join_params );
		
		$fields = $this->Data->get_record( $this->model, array(
			'where_params'=>$where_params,
			'join_params'=>$join_params 
		));
		
		return $fields;
	}
	
	public function records( $data, $where_params=array() ){ 
        $CI =& get_instance();
		
		$search_fields = $this->model->set_search_fields(); 
		$join_params = $this->model->set_join_params();
		$orderby = method_exists($this->model,'set_order_by')?$this->model->set_order_by():NULL;
		
		$query_params = array(
			'join_params'=>$join_params,
			'search_fields'=> $search_fields,
			'where_params'=> $where_params,
			'orderby'=> $orderby,
			'ordertype'=>'DESC',
			'paging'=>TRUE
		);  
		$records = $this->Data->get_records( $this->model, $query_params ); 
		$data['record_rows'] = $records;
		$data['pagination'] =  $this->Data->pagination_link;
		$data['per_page'] = $this->Data->per_page;
		$data['total_rows'] = $this->Data->total_rows;
		
		return $data;
	}
	
	public function update_password( $model, $id, $password, $confirm_password ){
		$CI =& get_instance();
		
		$result = FALSE;
		
		if( empty($password) == FALSE ){
			if( $password == $confirm_password ){
				$data = array(
					$model->tblpref.'password'=>$password
				);
				$update_password = $CI->Data->update_data( $model, $data, array( $model->tblid => $id ) ); 
				
				if( $update_password ){
					$result = TRUE;
					
					$details = $CI->Data->get_record( $model, array('where_params'=>array($model->tblid=>$id)));
					$lastname = app_get_val($details,$model->tblpref.'lastname');
					$firstname = app_get_val($details,$model->tblpref.'firstname');
					
					$notify_params = array('action'=>'password','type'=>'change-success','log'=>TRUE);
				} else {
					$notify_params = array('action'=>'password','type'=>'change-error','log'=>TRUE);
				}
			} else {
				$notify_params = array('action'=>'password','type'=>'did-not-match','log'=>TRUE);
			}
			$CI->notify->set( $notify_params );
		}
		
		return $result;
	}
	
	public function save(){ //( $pic_folder )
		$CI =& get_instance();
		
		$model = $this->model;
		$model_profile = $this->model_profile;
		
		if( $this->session_user_id === TRUE ){
			$id = $CI->logged->get_logged_user_id();
		} else {
			$id = $CI->input->post('id');
		}
		
		$field_data = $CI->input->post($model->tblname);

		$username = app_get_val($field_data,$model->tblpref.'username');
		$email = app_get_val($field_data,$model->tblpref.'email');
		
		$password = $CI->input->post($model->tblpref.'password');
		$confirm_password = $CI->input->post($model->tblpref.'confirm_password');
		/*---------------------------------------------*/
		
		if( $this->fields_post ){
			$CI->fields->model = $model;
			$CI->fields->fields_post = $this->fields_post;
			$CI->fields->validate( $field_data );
			$CI->fields->redirect();
		}
		
		$if_username_exist = $this->if_username_exist( $model, $username, $id );
		$if_email_exist = $this->if_email_exist( $model, $email, $id );
		
		if( $if_username_exist ){
			$notify_params = array('action'=>'username','type'=>'taken','log'=>TRUE);
			$CI->notify->set( $notify_params );
		} elseif( $if_email_exist ){
			$notify_params = array('action'=>'email','type'=>'taken','log'=>TRUE);
			$CI->notify->set( $notify_params );
		} else { 
			$field_data_user = $CI->input->post($model->tblname);
			$field_data_profile = $CI->input->post($model_profile->tblname);
			
			if( $id ){ 
				$CI->usersrole->user_has_capability($model->urc_name,'edit');
				
				$update_main = $CI->records->save( $model, $field_data_user, $id );
				
				if( $update_main ){					
					$this->update_password( $model, $id, $password, $confirm_password );
					
					$update_profile = $CI->records->save( $model_profile, $field_data_profile, array($model->tblid=>$id) );
					 
					if( $update_profile ){						
						$user = $CI->Data->get_record($model, array($model->tblid=>$id));
						$lastname = app_get_val($user,$model->tblpref.'lastname');
						$firstname = app_get_val($user,$model->tblpref.'firstname');
						
						$notify_params = array('action'=>'profile','type'=>'update-success','log'=>TRUE);
					} else {
						$notify_params = array('action'=>'profile','type'=>'update-error','log'=>TRUE);
					}			
				} else {
					$notify_params = array('action'=>'update','type'=>'success','log'=>TRUE);
				}
				$CI->notify->set( $notify_params );
				
				$this->upload_picture( $id );				
			} else {
				$CI->usersrole->user_has_capability($model->urc_name,'add'); //__FUNCTION__
				
				if( empty($password) ){
					$CI->notify->set('Password must not be empty', 'warning');
				} elseif( $password != $confirm_password ){
					$CI->notify->set('Password and Confirm Password must be equal', 'warning');
				} else {
					$id = $CI->records->save( $model, $field_data_user );
						
					if( $id ){
						$this->update_password( $model, $id, $password, $confirm_password );
						
						$field_data_profile = array_merge($field_data_profile,array($model->tblid=>$id));
						$profile_id = $CI->records->save( $model_profile, $field_data_profile );
						
						if( $profile_id ){
							$this->upload_picture( $id );
							
							$user = $CI->Data->get_record($model, array($model->tblid=>$id));
							$lastname = app_get_val($user,$model->tblpref.'lastname');
							$firstname = app_get_val($user,$model->tblpref.'firstname');
							
							$notify_params = array('action'=>'profile','type'=>'add-success','log'=>TRUE);
						} else {
							$notify_params = array('action'=>'profile','type'=>'add-error','log'=>TRUE);
						}
						$CI->notify->set( $notify_params );
					
						redirect( site_url( 'c='.$model->page.'&m=edit&id='.$id ) );
					} else {
						$notify_params = array('action'=>'add','type'=>'success','log'=>TRUE);
						$CI->notify->set( $notify_params );
					}
				}
			}
		}
		
		redirect_current_page();
	}
	
	private function upload_picture( $id ){
		$CI =& get_instance();
		
		$CI->pic->id = $id;
		$CI->pic->upload();
	}
	
	public function delete(){
		$CI =& get_instance();
		
		$model = $this->model;
		$model_profile = $this->model_profile;
		
		$CI->usersrole->user_has_capability($model->urc_name,__FUNCTION__);
		
		$id = $CI->input->get('id');
		$popup = $CI->input->get('popup');
		
		$details = $this->record( $id ); 
		
		if( $details ){
			$username = app_get_val( $details, $model->tblpref.'username');
			$lastname = app_get_val( $details, $model->tblpref.'lastname'); 
			$firstname = app_get_val( $details, $model->tblpref.'firstname');
			
			if( in_array($username, $exempted_role) == FALSE ){
				$delete1 = $CI->Data->delete_data( $model, array($model->tblid=>$id) );
				$delete2 = $CI->Data->delete_data( $model_profile, array($model_profile->tblid=>$id) );

				if( $delete1 && $delete2 ){
					$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE);
				} else {
					$notify_params = array('action'=>'delete','type'=>'error','log'=>TRUE);
				}
			} else {
				$notify_params = array('action'=>'user','type'=>'can-not-delete','log'=>TRUE);
			}			
			$CI->notify->set( $notify_params );
			
			if( $popup ){
				redirect_current_page();
			}
		} else {
			$notify_params = array('action'=>'record','type'=>'not-exist','log'=>TRUE);
			$CI->notify->set( $notify_params );
		}
		
		redirect( site_url('c='.$model->page) );
	}
	
	public function if_username_exist( $model, $username, $user_id='' ){
		$CI =& get_instance();
		
		if( $user_id ){
			$exist = $CI->Data->get_record( $model, 
				array('where_params'=>array(
					$model->tblpref.'username'=>$username,
					$model->tblid.' <>'=>$user_id
				)
			));
		} else {
			$exist = $CI->Data->get_record( $model, 
				array('where_params'=>array($model->tblpref.'username'=>$username)) 
			);
		}
		
		return $exist;
	}
	
	public function if_email_exist( $model, $email, $user_id='' ){
		$CI =& get_instance();
		
		if( $user_id ){
			$exist = $CI->Data->get_record( $model, 
				array('where_params'=>array(
					$model->tblpref.'email'=>$email,
					$model->tblid.' <>'=>$user_id
				)
			));
		} else {
			$exist = $CI->Data->get_record( $model, 
				array('where_params'=>array($model->tblpref.'email'=>$email)) 
			);
		}
		
		return $exist;
	}
	
	public function get_data_records( $data ){
		$CI =& get_instance();
		
		$model = $this->model;
					
		$data = $this->records( $data );
		
		$CI->pic->width = '35px';
		$CI->pic->height = '35px';
		
		return $data;
	}
	
	public function get_data_records_except_logged_user( $data ){
		$CI =& get_instance();
		
		$model = $this->model;
		
		$logged_user_id = $CI->logged->get_logged_user_id();
		$where_params = array($model->tblname.'.'.$model->tblid.' <>' => $logged_user_id);
		
		$CI->pic->width = '35px';
		$CI->pic->height = '35px';
		
		$data = $this->records( $data, $where_params );		
		
		return $data;
	}

	public function get_data_view( $data ){
		$CI =& get_instance();
		
		$id = $CI->input->get('id');
		$data['id'] = $id;
		
		$fields = $this->record( $id );
		$data['fields'] = $fields;
		
		$picture_src = get_value($fields,$this->model_profile->tblpref.'picture');		
		$data['picture'] = $CI->pic->get_img_by_src( $picture_src );
		
		return $data;
	}

	public function get_data_edit( $data ){
		$CI =& get_instance();
		
		$data = $this->get_data_view( $data );
		
		$data['select_role'] = $this->model_role->get_select( $this->model, $data['fields'] );
		
		return $data;
	}

	public function get_data_view_except_logged_user( $data ){
		$CI =& get_instance();
		
		$model = $this->model;
		
		$id = $CI->input->get('id');
		$data['id'] = $id;
		$logged_user_id = $CI->logged->get_logged_user_id();
		
		$where_params = array(
			$model->tblname.'.'.$model->tblid => $id,
			$model->tblname.'.'.$model->tblid.' <>' => $logged_user_id
		);
		$fields = $this->record( $where_params );
		$data['fields'] = $fields;
		
		$picture_src = get_value($fields,$this->model_profile->tblpref.'picture');
		$data['picture'] = $CI->pic->get_img_by_src( $picture_src );
		
		return $data;
	}

	public function get_data_edit_except_logged_user( $data ){
		$CI =& get_instance();
		
		$data = $this->get_data_view_except_logged_user( $data );
		
		$data['select_role'] = $this->model_role->get_select( $this->model, $data['fields'] );
		
		return $data;
	}

	public function get_data_edit_profile( $data, $update_role=FALSE ){
		$CI =& get_instance();
		
		$model = $this->model;
		
		$id = $CI->logged->get_logged_user_id();
		$data['id'] = $id;
		
		$fields = $this->record( $id );
		$data['fields'] = $fields;
		
		if( $update_role ){
			$data['select_role'] = $this->model_role->get_select( $model, $fields );
		} else {
			$data['select_role'] = app_get_val($fields,$this->model_role->tblpref.'title');
		}
		
		$data['picture'] = $CI->pic->get_img_by_id( $id );
		
		return $data;
	}
}
?>