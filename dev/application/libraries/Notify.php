<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notify {
	
	public $next_line;
	public $data;
	public $notification;
	public $message;

    function __construct( $params=array() ){
		
		$params_default = array_merge(array(
			'next_line'=>'<br />'
			), $params
		);
		extract($params_default);
		
		$this->next_line = $next_line;
    }

    function __destruct(){
		
    }
	
	public function set($notification, $alert='success', $action='add', $log=FALSE){
		$CI =& get_instance();
		
		$array = array();
				
		if( is_array($notification) ){
			if( is_numeric(key($notification)) ){
				$action = app_get_val($notification,0);
				$type = app_get_val($notification,1);
				$text = app_get_val($notification,2,'');
				$log = app_get_val($notification,3);
			} else {
				$action = app_get_val($notification,'action');
				$type = app_get_val($notification,'type');
				$text = app_get_val($notification,'text','');
				$log = app_get_val($notification,'log');
			}
			
			$alert = key($this->data['config_notifications'][$action][$type]);
			$notification = $this->data['config_notifications'][$action][$type][$alert];
			$message = $text ? $notification.' '.$text : $notification;	
		} else {
			$message = $notification;
		}
		
		$this->notification = $message;
		
		$array['alerts'] = $this->get_alerts();
		
		if( $log  ){
			$user_id = $log;
			$action = $type?$action.'-'.$type:$action;
			if( is_bool($log) && $log === TRUE ){
				$user_id = $CI->logged->get_login_session('user_id');				
			}
			$CI->userslogs->save($message, $action, $user_id);
		}
		
		$type = $this->get_by_type($alert);
		if( $type ){
			$set_message = $type.$this->next_line.$message;
		} else {
			$set_message = $message;
		}
		$array['alerts'][$alert] = $set_message;
		
		$CI->session->set_userdata($array);
		
	}

	public function remove(){
		$CI =& get_instance();
		
		$CI->session->unset_userdata('alerts');
	}

	public function get_alerts(){
		$CI =& get_instance();
		
		$alert = $CI->session->userdata('alerts');
		
		return $alert;
	}
		
	public function show( $params=array() ){
		
		$result = $this->get( $params );
		
		echo $result;
	}
		
	public function get( $params=array() ){
		$CI =& get_instance();
		
		$defaults = array_merge(array(
			'tag'=>'div',
			'start'=>'<div id="notify">',
			'end'=>'</div>',
			'size'=>'md'
			), $params
		);
		extract($defaults);
		
		$result = '';
		
		$alerts = $this->get_alerts();
		
		if( $alerts ){
			$success_notification = $this->get_by_type('success');
			$warning_notification = $this->get_by_type('warning');
			$danger_notification = $this->get_by_type('danger');
			$info_notification = $this->get_by_type('info');
			
			$button = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
			 
			if( $success_notification ){
				$result .= '<'.$tag.' class="alert alert-success alert-'.$size.'" role="alert">'.$button.'<strong>Success!</strong> '.$success_notification.'</'.$tag.'>';
			}
			if( $warning_notification ){
				$result .= '<'.$tag.' class="alert alert-warning alert-'.$size.'" role="alert">'.$button.'<strong>Warning!</strong> '.$warning_notification.'</'.$tag.'>';
			}
			if( $danger_notification ){
				$result .= '<'.$tag.' class="alert alert-danger alert-'.$size.'" role="alert">'.$button.'<strong>Error!</strong> '.$danger_notification.'</'.$tag.'>';
			}
			if( $info_notification ){
				$result .= '<'.$tag.' class="alert alert-info alert-'.$size.'" role="alert">'.$button.'<strong>Info!</strong> '.$info_notification.'</'.$tag.'>';
			}
		}
		
		$result = $start.$result.$end;
		
		$this->remove();
		
		return $result;
	}
		
	public function get_by_type( $type ){
		$CI =& get_instance();
		
		$result = '';		
		$alerts = $this->get_alerts();
		
		if( $alerts ){
			$result = isset($alerts[$type])?$alerts[$type]:'';
		}
		
		return $result;
	}
		
	public function warning( $type=0 ){
		
		if( $type == 1 ){
			$message = 'Please fill-out the required fields.';
		} else {
			$message = 'No records or data found.';
		}
		
		$result = '<div class="alert alert-warning">'.$message.'</div>';
		
		echo $result;
	}
		
	public function records( $records ){
		
		$alert = '';
		if( is_array($records) || is_object($records) ){
			if( count($records) <= 0 ){
				$message = 'No records or data found. Click the <strong>Add New</strong> button to add new data.';		
				$alert = '<div class="alert alert-warning">'.$message.'</div>';				
			}
		}
		
		echo '<div id="notify-record">'.$alert.'</div>';
	}
}
?>