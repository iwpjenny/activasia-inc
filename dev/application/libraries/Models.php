<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Models {
		
	/* return */
	public $error;
	public $result = FALSE;
	public $notification;	
	private $model_dir_path;
	private $view_dir_path;
	private $module_path;
	private $location;
	
	private $restricted_files = array(
		'Backend_Log_Model.php',
		'Frontend_Log_Model.php',
		'Data_Model.php',
		'Generic_Model.php',
		'Upload_Settings_Model.php',
		'Settings_Format_Model.php',
		'index.html'		
	);
	
	/* parameters */

    function __construct(){
        $CI =& get_instance();
		
		$this->Data = $CI->Data;
		
		$base_path = str_replace('\\','/',FCPATH);
		$this->model_dir_path = $base_path.'application/models/';
		$this->view_dir_path = $base_path.'application/views/';
    }

    function __destruct(){
        $this->id = NULL;
    }
	
	public function get_models_urc( $sub_dir='' ){
        $CI =& get_instance();
		
		$CI->load->helper('directory');
		$model_path = $this->model_path.$sub_dir;
		$module_array = app_get_file_open_dir( $model_path );
		
		$result = array();
		if( $module_array ){
			foreach($module_array as $key=>$module){
				if( in_array($module, $this->restricted_files) == FALSE ){
					include_once($model_path.$module);
					
					$objname = str_replace('.php','',$module);
					$objname = $sub_dir.$objname;
					
					$CI->load->model($objname, $objname);
					
					if( isset($CI->$objname->user_role) ){
						
						$dashboard = isset($CI->$objname->dashboard) ? $CI->$objname->dashboard : NULL;
						
						$result[] = (object) array(
							'name'			=> $CI->$objname->name,
							'urc_name'		=> $CI->$objname->urc_name,
							'description'	=> $CI->$objname->description,
							'user_role'		=> $CI->$objname->user_role
						);
					}
				}
			}
		}
					
		return $result;
	}
	
	/* public function get_standard_dashboard_sections( $data ){
        $CI =& get_instance();
		
		$CI->load->helper('directory');
		$model_path = $this->model_path;
		$view_path = $this->view_path;
		$module_array = app_get_file_open_dir( $model_path );
		
		$result = '';
		if( $module_array ){
			foreach($module_array as $key=>$module){
				if( in_array($module, $this->restricted_files) == FALSE ){
					include_once($model_path.$module);
					
					$objname = str_replace('.php','',$module);
					$objname = $objname;
					
					$CI->load->model($objname, $objname);
					
					if( isset($CI->$objname->user_role) ){
						
						$model = $CI->$objname;						
						$display_section = isset($model->display_section) ? $model->display_section : NULL;
						
						if( $display_section ){
							$data['model'] = $model;
							if( app_get_val($display_section,'dashboard') == TRUE ){
								$dashboard_section = $this->get_dashboard_section_view( $data, $model, $view_path );
							
								$result .= $dashboard_section;
							} //elseif(){
								
							//}
						}
					}
				}
			}
		}
					
		return $result;
	} */
	
	public function set_data_standard_dashboard_sections( $data ){
        $CI =& get_instance();
		
		$CI->load->helper('directory');
		$model_dir_path = $this->model_dir_path;
		$view_dir_path = $this->view_dir_path;
		
		$standard_model_array = app_get_file_open_dir( $model_dir_path, TRUE, '' );
		$module_model_array = app_get_file_open_dir( $model_dir_path.'modules/', TRUE, 'modules' );		
		$module_array = array_merge($standard_model_array, $module_model_array);
		
		$dashboard = '';
		$menus_left = array();
		$dashboard_sections = array();
		$data_view = $data;
		$data['dashboard_sections_standard'] = '';
		$data['menu_sidebar_left'] = '';
		$this->location = app_get_val($data,'location');
		
		if( $module_array ){
			foreach($module_array as $col){
				$module = app_get_val($col,'name');
				$dir = app_get_val($col,'dir');
				$type = app_get_val($col,'type');
				$sub_dir = $type;
				
				if( in_array($module, $this->restricted_files) == FALSE ){					
					$objname = str_replace('.php','',$module);
					
					$model_path = $objname;
					if( $type ){
						$model_path = $sub_dir.'/'.$objname;						
					}					
					
					$CI->load->model($model_path, $objname);
					
					if( isset($CI->$objname) ){
						
						$model = $CI->$objname;
						
						if( isset($model->display_section) ){
							
							$display_section = $model->display_section;
							
							$view_dir_full_path = $view_dir_path.$this->location;					
							$view_path = $this->location.'/'.$model->view.'/';
							
							if( $type ){
								$view_dir_full_path = $view_dir_path.$this->location.'/'.$sub_dir;
								$view_path = $this->location.'/'.$sub_dir.'/'.$model->view.'/';
							}						
							
							$data_view['model'] = $model;
							if( app_get_val($display_section,'dashboard') == TRUE ){
								$dashboard_params = app_get_val($display_section,'dashboard');
								$order = app_get_val($dashboard_params,'order');
								
								if( method_exists($model,'get_dashboard_section_view') ){
									$dashboard = $model->get_dashboard_section_view( $data_view );
								} else {
									$dashboard = $this->get_dashboard_section_view( $data_view, $model, $view_dir_full_path, $view_path );
								}
								
								$dashboard_sections[$order] = $dashboard;
							}
							if( app_get_val($display_section,'menu-sidebar-left') ){
								$menu_left_params = app_get_val($display_section,'menu-sidebar-left');
								$order = app_get_val($menu_left_params,'order');
								
								$menu_left_view = $this->get_menu_left_view( $data_view, $model, $view_dir_full_path, $view_path );
								$menus_left[$order] = $menu_left_view;
							}
						}
					}
				}
			}
			
			$data['menu_sidebar_left'] = $this->get_short_array( $menus_left );
			$data['dashboard_sections_standard'] = $this->get_short_array( $dashboard_sections );
		}
					
		return $data;
	}
	
	private function get_short_array( $arrays ){
		$result = '';
		if($arrays){
			ksort($arrays);
			foreach($arrays as $content){
				$result .= $content;
			}
		}
		return $result;
	}
	
	private function get_dashboard_section_view( $data, $model, $view_dir_full_path, $view_path ){
        $CI =& get_instance();
		
		$table_name = $CI->db->dbprefix.$model->tblname;
		$table_exists = $CI->db->table_exists( $table_name );
		
		$result = '';
		if( $table_exists ){			
			$data['display_latest_users'] = app_get_val($data['settings'],'display_latest_users'); 
			$data['grid_date_time_format'] = app_get_val($data['settings'],'grid_date_time_format'); 
			$data['character_limit'] = app_get_val($data['settings'],'grid_text_limit');
			
			$join_params = method_exists($model,'set_join_params') ? $model->set_join_params() : array(); 
			$query_params = array(
				'limit'=>5,
				'join_params'=>$join_params
			); 
			$data['records'] = $CI->Data->get_records( $model, $query_params );
			
			$path = $view_dir_full_path.'/'.$model->view.'/section-dashboard.php';
			
			if( file_exists($path) ){
				$result = $CI->load->view($view_path.'section-dashboard', $data, TRUE);
			}
			 
		}
		
		return $result;
	}
	
	private function get_menu_left_view( $data, $model, $view_dir_full_path, $view_path ){
        $CI =& get_instance();
		
		$result = '';	
		$path = $view_dir_full_path.'/'.$model->view.'/menu-sidebar-left.php';		
		if( file_exists($path) ){
			$result = $CI->load->view($view_path.'menu-sidebar-left', $data, TRUE);
		}
		
		return $result;
	}
}
?>