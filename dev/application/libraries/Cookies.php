<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cookies {

	function __construct(){
        $CI =& get_instance();
		$CI->load->helper('cookie');
    }

	function get( $login_user_type, $variable ){
		
		$return = get_cookie($login_user_type.'_'.$variable);
		
		return $return;
	}

	function set_login_user( $login_user_type, $username, $password, $remember_me ){
		
		set_cookie($login_user_type.'_remember_me',$remember_me,time()+(10*365*24*60*60));			
		set_cookie($login_user_type.'_username',$username,time()+(10*365*24*60*60));			
		set_cookie($login_user_type.'_password',$password,time()+(10*365*24*60*60));
	}

	function delete_login_user( $login_user_type ){
		delete_cookie($login_user_type.'_remember_me');	
		delete_cookie($login_user_type.'_username');	
		delete_cookie($login_user_type.'_password');
	}
}