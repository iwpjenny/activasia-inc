<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModuleFiles {
	
	public $model;
	public $model_files;
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	
	/* parameters */
	public $id;
	public $folder;
	public $base_path;
	public $dir_root;
	public $dir_perms;
	public $default_pic;
	public $fieldname;
	public $width;
	public $height;
	public $default_image_url_fullpath;

    function __construct( $params=array() ){
		
		$params_default = array_merge(array(
			'model'=>NULL,
			'model_files'=>NULL,
			'id'=>NULL,
			'default_image'=>'image-default.jpg',
			'default_file'=>'file-default.png',
			'folder'=>'modules',
			'fieldname'=>'files',
			'base_path'=>str_replace('\\','/',FCPATH),
			'dir_root'=>'uploads',
			'dir_perms'=>DIR_WRITE_MODE,
			'width'=>'300px',
			'height'=>'300px'
			), $params
		);
		extract($params_default);
		
		/* $this->model = $model;
		$this->model_files = $model_files; */
		
		$this->id = $id;
		$this->folder = $folder;
		$this->default_image = $default_image;
		$this->default_file = $default_file;
		$this->fieldname = $fieldname;
		$this->base_path = $base_path;
		$this->dir_root = $dir_root;
		$this->dir_perms = $dir_perms;
		$this->width = $width;
		$this->height = $height;
		
		$this->default_image_url_fullpath = base_url($this->dir_root.'/'.$this->default_image);
		$this->default_file_url_fullpath = base_url($this->dir_root.'/'.$this->default_file);
    }

    function __destruct(){
		$this->trigger_before = NULL;
		$this->trigger_after = NULL;
		$this->trigger_on_success = NULL;
		$this->trigger_on_error = NULL;
		$this->error = NULL;
		
        $this->id = NULL;
        $this->model = NULL;
        $this->model_files = NULL;
		
        $this->folder = NULL;		
        $this->default_image = NULL;
        $this->fieldname = NULL;
        $this->dir_root = NULL;
        $this->dir_perms = NULL;
        $this->width = NULL;
        $this->height = NULL;
    }
	
	private function trigger( $triggers=array() ){
		
		$result = FALSE;
		
		if( array_key_exists(0,$triggers) ){
			foreach($triggers as $trigger){
				$result[] = $this->get_trigger( $trigger );
			}
		} else {
			$result = $this->get_trigger( $triggers );
		}
		
		return $result;
	}
	
	private function get_trigger( $trigger ){
		
		$result = FALSE;
		
		if( is_array($trigger) && $trigger ){
			extract($trigger);
			
			if( isset($params) ){
				if( is_object($obj) ){
					$result = $obj->$func( $params );
				} else {
					$result = $func( $params );
				}
			} else {
				if( is_object($obj) ){
					$result = $obj->$func();
				} else {
					$result = $func();
				}
			}
		}
		
		return $result;
	}
	
	public function record( $query_params_or_fid ){
		$CI =& get_instance();
		
		if( is_array($query_params_or_fid) ){
			$query_params = $query_params_or_fid;
		} else {
			$query_params = array('where_params'=>array($this->model_files->tblid=>$query_params_or_fid));
		}
		
		$result = $CI->Data->get_record($this->model_files, $query_params);
		
		return $result;
	}
	
	public function records( $query_params_or_mid ){
		$CI =& get_instance();
		
		if( is_array($query_params_or_mid) ){
			$query_params = $query_params_or_mid;
		} else {
			$query_params = array('where_params'=>array($this->model->tblid=>$query_params_or_mid));
		}
		
		$result = $CI->Data->get_records($this->model_files, $query_params);
		
		return $result;
	}
	
	public function create_table( $model, $model_files ){
		$CI =& get_instance();
		
		$table_name = $model_files->tblname;
		$primary_field = $model_files->tblid;
		$prefix = $model_files->tblpref;
		$foreign_key = $model->tblid;
		$NULL = FALSE;
		
		$fields = array(
			$primary_field=>array('type'=>'INT','constraint'=>11,'auto_increment'=>TRUE,'null'=>$NULL),
			$foreign_key=>array('type'=>'INT','constraint'=>11,'unsigned'=>FALSE,'null'=>$NULL),
			$prefix.'filepath'=>array('type'=>'VARCHAR','constraint'=>255,'null'=>$NULL),
			$prefix.'type'=>array('type'=>'CHAR','constraint'=>20,'null'=>$NULL),
			$prefix.'title'=>array('type'=>'VARCHAR','constraint'=>100,'null'=>$NULL),
			$prefix.'description'=>array('type'=>'VARCHAR','constraint'=>255,'null'=>$NULL),
			$prefix.'created'=>array('type'=>'DATETIME','null'=>$NULL),
			$prefix.'modified'=>array('type'=>'TIMESTAMP','null'=>$NULL),
			$prefix.'published'=>array('type'=>'TINYINT','default'=>1,'null'=>$NULL)
		);
		$params = array('primary_field'=>$primary_field);
		
		$CI->load->library('DBTable',$params);		
		$result = $CI->dbtable->create( $table_name, $fields );
		
		return $result;
	}
	
	private function create_folders(){
		
		$upload_path = $this->base_path.$this->dir_root.'/';
		$upload_path_files = $upload_path.$this->folder.'/';
		$upload_path_module = $upload_path_files.$this->model_files->folder.'/';
		$upload_path_module_record = $upload_path_module.$this->id.'/';
		
		if(is_dir($upload_path) == FALSE){
			mkdir($upload_path, $this->dir_perms, TRUE);
		}
		if(is_dir($upload_path_files) == FALSE){
			mkdir($upload_path_files, $this->dir_perms, TRUE);
		}
		if(is_dir($upload_path_module) == FALSE){
			mkdir($upload_path_module, $this->dir_perms, TRUE);
		}
		if(is_dir($upload_path_module_record) == FALSE){
			mkdir($upload_path_module_record, $this->dir_perms, TRUE);
		}
		
		return $upload_path_module_record;
	}
	
	public function upload(){
		$CI =& get_instance();
		
		$result = FALSE;
		
		if( isset($_FILES[$this->fieldname]['name']) && $_FILES[$this->fieldname]['name'] != '' ){
			
			$this->trigger( $this->trigger_before );
			
			$dist_dir = $this->create_folders();
										
			$config = array(
				'fieldname'=>$this->fieldname,
				'dist_dir'=>$dist_dir
			);
			$CI->load->library('FileUpload', $config);
			
			$upload = $CI->fileupload->upload();
			$upload_filepath = $CI->fileupload->upload_filepath;
			$filename_new = $CI->fileupload->filename_new;
			$filename_old = $CI->fileupload->filename_old;
			$file_title = $CI->fileupload->file_title;
			$file_type = $CI->fileupload->file_type;
			$this->error = $CI->fileupload->error;
			
			if ( $upload ){				
				foreach($upload as $key=>$upload_filepath){
					$title = $file_title[$key];
					$type = $file_type[$key];
					$file_old = $filename_old[$key];
					
					$result = $CI->Data->insert_data($this->model_files, array(
						$this->model->tblid => $this->id,
						$this->model_files->tblpref.'filepath' => $upload_filepath,
						$this->model_files->tblpref.'type' => $type,
						$this->model_files->tblpref.'title' => $title,
						$this->model_files->tblpref.'description' => $title
					));
					
					$notify_params = array('action'=>'file','type'=>'upload-success','log'=>TRUE);					
					$CI->notify->set( $notify_params );
				}
				
				$this->trigger( $this->trigger_on_success );
			} else {
				$notify_params = array('action'=>'file','type'=>'upload-error','log'=>TRUE,'text'=>$this->error);			
				$CI->notify->set( $notify_params );
				
				$this->trigger( $this->trigger_on_error );
			}
			
			$this->trigger( $this->trigger_after );
		}
		
		return $result;
	}
	
	public function field(){
		
		$result = form_upload(array('name'=>$this->fieldname.'[]','multiple'=>'multiple','accept'=>'file_extension|audio/*|video/*|image/*|media_type'));
		
		echo $result;
	}
	
	public function get_fileinfo($filepath, $key='filename'){
		
		$fileinfo = pathinfo($filepath);
		$result = $fileinfo[$key];
		
		return $result;
	}
	
	public function record_grid_thumbnail( $id, $params=array() ){
		$CI =& get_instance();		
		
		$query_params = array('where_params'=>array($this->model->tblid=>$id));
		
		$constant_params = array(
			'fancybox'=>TRUE,
			'attr'=>'style="width:60px;height:60px;" class="img-thumbnail"'
		);
		$params = array_merge($constant_params, $params);
		
		$result = $this->image( $query_params, $params );		
		
		return $result;
	}
	
	public function image( $query_params_or_fid, $params=array() ){
		$CI =& get_instance();
		
		$params_default = array_merge(array(
			'thumbnail'=>FALSE,
			'fancybox'=>FALSE,
			'exact_file'=>TRUE,
			'attr'=>''
			), $params
		);
		extract($params_default);
		
		$prefix = $this->model_files->tblpref;
		
		$image = $this->record( $query_params_or_fid );
		
		$result = '';
		if( $image ){
			$fid = app_get_val($image,$this->model_files->tblid);
			$filepath = app_get_val($image,$prefix.'filepath');
			$title = app_get_val($image,$prefix.'title');
			
			$src = base_url($filepath);
			
			if( $exact_file === TRUE ){
				$img = '<img src="'.$src.'" alt="'.$filepath.'" '.$attr.' />';
			} else {
				$img = '<span class="glyphicon glyphicon-picture"></span>';
			}
			
			if( $fancybox === TRUE ){
				$result = '<a href="'.$src.'" class="fancybox" rel="gridimage" title="'.$title.'">'.$img.'</a>';
			} else {
				$result = $img;
			}
		} else {
			if( $exact_file === TRUE ){
				$src = $this->default_image_url_fullpath;
				$result = '<img src="'.$src.'" alt="No Image" '.$attr.' />';
			}
		}
		
		return $result;
	}
	
	public function get_file_by_record_id( $record_id, $params=array() ){
		
		$query_params = array('where_params'=>array($this->model->tblid=>$record_id));
		
		$constant_params = array(
			'fancybox'=>TRUE,
			'attr'=>'style="width:50px;height:50px;" class="img-thumbnail"'
		);
		$params = array_merge($constant_params, $params);
		
		$result = $this->get( $query_params, $params );
		
		return $result;
	}
	
	private function get_file_type_icon( $file_type, $src, $filepath, $title, $attr='', $exact_file=TRUE, $fancybox=TRUE ){
		
		$file_pathinfo = pathinfo($filepath);
		$basename = $file_pathinfo['basename'];
				
		if( $exact_file === TRUE ){
			$default_src = $this->default_file_url_fullpath;
			$tag = '<img src="'.$default_src.'" alt="No Image" '.$attr.' />';
		} else {
			$tag = '<span class="glyphicon glyphicon-file"></span>';
		}
		
		$href = $src;
		if( $file_type == 'image' ){
			if( $exact_file === TRUE ){
				$tag = '<img src="'.$src.'" alt="'.$filepath.'" '.$attr.' />';
			} else {
				$tag = '<span class="glyphicon glyphicon-picture"></span>';
			}
			if( $fancybox === TRUE ){
				$result = '<a href="'.$href.'" class="fancybox" rel="file-group" title="'.$basename.'">'.$tag.'</a>';
			} else {
				$result = $tag;
			}
		} else {				
			$result = '<a href="'.$href.'" title="'.$basename.'" target="_blank">'.$tag.'</a>';
		}
		
		return $result;
		
	}
	
	public function get( $query_params_or_file_id, $params=array() ){
		$CI =& get_instance();
		
		$params_default = array_merge(array(
			'thumbnail'=>FALSE,
			'fancybox'=>FALSE,
			'exact_file'=>TRUE,
			'attr'=>''
			), $params
		);
		extract($params_default);
		
		$model = $this->model_files;
		$details = $this->record( $query_params_or_file_id );
		
		if( $details ){
			$prefix = $model->tblpref;
			$fid = app_get_val($details,$model->tblid);
			$filepath = app_get_val($details,$prefix.'filepath');
			$type = app_get_val($details,$prefix.'type');
			$title = app_get_val($details,$prefix.'title');
			
			$file_type = $this->get_file_type( $type );
			$src = base_url($filepath);
			$href = $src;
			
			$result = $this->get_file_type_icon( $file_type, $src, $filepath, $title, $attr, FALSE );			
		} else {
			$result = '';
		}
		
		return $result;
	}
	
	public function images( $mid ){
		$CI =& get_instance();
		
		$action = $CI->input->get('m'); 
		
		$model = $this->model_files;
		$prefix = $model->tblpref;
		$images = $this->records( $mid );
		
		$result = '';
		if( $images ){
			foreach($images as $image){
				$fid = app_get_val($image,$model->tblid);
				$filepath = app_get_val($image,$prefix.'filepath');
				$type = app_get_val($image,$prefix.'type');
				$title = app_get_val($image,$prefix.'title');
				$description = app_get_val($image,$prefix.'description');
				
				$src = base_url($filepath);
				$file_type = $this->get_file_type( $type );
				
				$file_pathinfo = pathinfo($filepath);
				$extension = strtoupper($file_pathinfo['extension']);
				
				$delete_url = site_url('c='.$this->model->page.'&m=delfile&id='.$mid.'&fid='.$fid);
				
				$file = $this->get_file_type_icon( $file_type, $src, $filepath, $title );
				
				$result .= '<div class="col-lg-1 col-md-2 col-sm-3 col-xs-6">';
				$result .= '<div class="thumbnail">';
				$result .= '<div class="caption">';
				if( $action == 'edit' ){
					$result .= '<a href="'.$delete_url.'" class="btn btn-danger btn-xs" role="button"><span class="glyphicon glyphicon-remove"></span></a>';
					$result .= ' <small><strong>(.'.$extension.')</strong></small>';
				} else {
					$result .= '<small><strong>(.'.$extension.')</strong></small>';
				}
				$result .= '</div>';
				$result .= $file;
				$result .= '</div>';
				$result .= '</div>';
			}
		}
		
		echo $result;
	}
	
	private function get_file_type( $type ){
		$explode_type = explode('/',$type);
		$file_type = app_get_val($explode_type,0);
		
		return $file_type;
	}
	
	public function delete( $fid ){
		$CI =& get_instance();
		
		$this->trigger( $this->trigger_before );
				
		$details = $this->record( $fid );
		
		$result = FALSE;		
				
		if( $details ){
			$filepath = app_get_val($details,$this->model_files->tblpref.'filepath');
			$full_filepath = $this->base_path.$filepath;
			$basename = $this->get_fileinfo($filepath,'basename');
			
			$delete_file = $this->delete_file( $full_filepath );
			
			$delete_data = $CI->Data->delete_data($this->model_files,
				array($this->model_files->tblid=>$fid)
			);
			
			if( $delete_file ){
				$notify_params = array('action'=>'file','type'=>'delete-success','log'=>TRUE,'text'=>'('.$basename.', ID:'.$fid.')');
				$CI->notify->set( $notify_params );
				
				$this->trigger( $this->trigger_on_success );
			} else {
				$notify_params = array('action'=>'file','type'=>'delete-error','log'=>TRUE,'text'=>'('.$basename.', ID:'.$fid.')');
				$CI->notify->set( $notify_params );
				$this->trigger( $this->trigger_on_error );
			}
			
			if( $delete_data ){
				$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE,'text'=>'('.$basename.', ID:'.$fid.')');
				$CI->notify->set( $notify_params );
				$this->trigger( $this->trigger_on_success );
			} else {
				$notify_params = array('action'=>'delete','type'=>'error','log'=>TRUE,'text'=>'('.$basename.', ID:'.$fid.')');
				$CI->notify->set( $notify_params );
				$this->trigger( $this->trigger_on_error );
			}
		} else {
			$notify_params = array('action'=>'record','type'=>'not-exist','log'=>TRUE,'text'=>'(ID:'.$fid.')');
			$CI->notify->set( $notify_params );
		}
		
		$this->trigger( $this->trigger_after );
		
		return $result;
	}
	
	public function delete_all( $mid ){
		$CI =& get_instance();
		
		$this->trigger( $this->trigger_before );
				
		$records = $this->records( $mid );
		
		$result = FALSE;
				
		if( $records ){
			foreach($records as $col){
				$fid = app_get_val($col,$this->model_files->tblid);
				$filepath = app_get_val($col,$this->model_files->tblpref.'filepath');
				$full_filepath = $this->base_path.$filepath;
				$basename = $this->get_fileinfo($filepath,'basename');
				
				$delete_file = $this->delete_file( $full_filepath );
				
				if( $delete_file == FALSE ){
					$notify_params = array('action'=>'file','type'=>'delete-error','log'=>TRUE,'text'=>'('.$basename.', File ID:'.$fid.')');
					$CI->notify->set( $notify_params );
				}
			}
			
			$delete_data = $CI->Data->delete_data($this->model_files, array($this->model->tblid=>$mid));
			
			if( $delete_data ){
				$notify_params = array('action'=>'delete','type'=>'success','log'=>TRUE,'text'=>'(Module ID:'.$mid.')');
				$CI->notify->set( $notify_params );
				$this->trigger( $this->trigger_on_success );
				$result = TRUE;
			} else {
				$notify_params = array('action'=>'delete','type'=>'error','log'=>TRUE,'text'=>'(Module ID:'.$mid.')');
				$CI->notify->set( $notify_params );
				$this->trigger( $this->trigger_on_error );
			}
		} else {
			$notify_params = array('action'=>'record','type'=>'not-exist','log'=>TRUE,'text'=>'(Module ID:'.$mid.')');
			$CI->notify->set( $notify_params );
		}
		
		$this->trigger( $this->trigger_after );
		
		return $result;
	}
	
	public function set_full_filepath( $filepath ){
				
		$full_filepath = $this->base_path.$filepath;
		
		return $full_filepath;
	}
	
	public function delete_file( $filepath, $add_full_path=FALSE ){
				
		$result = FALSE;
		
		if( $add_full_path === TRUE ){
			$filepath = $this->set_full_filepath( $filepath );
		}
		
		if( file_exists($filepath) ){
			$result = unlink($filepath);
		}
		
		return $result;
	}
}
?>