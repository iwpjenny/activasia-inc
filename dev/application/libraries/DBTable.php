<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DBTable {
	
	private $Data;
	private $params;
	private $attributes = array('ENGINE'=>'InnoDB');
	private $if_not_exists = TRUE;
	
	/* return */
	public $error;
	public $result;
	public $notification;
	
	/* parameters */

    function __construct( $params=array() ){
        $CI =& get_instance();
		
		$this->params = $params;
		
		$this->Data = $CI->Data;
    }

    function __destruct(){
        $this->Data = NULL;
		
		$this->params = NULL;
		$this->attributes = NULL;
		$this->if_not_exists = NULL;
    }

	private function execute_query( $table_query_structure ){
        $CI =& get_instance();
		
		$sql_query_array = explode(";\n",$table_query_structure);
		
		if( is_array($sql_query_array) ){
			if( $sql_query_array ){
				foreach( $sql_query_array as $query ){
					$sql_query = trim($query);
					$sql_query = str_replace(array("\n","\r","\r\n","\s"),'',$sql_query);
					if( $sql_query ){
						$result = $CI->db->query( $sql_query );					
						if( $result == FALSE ){
							break;
						}
					}
				}
			}
		} else {
			$result = $CI->db->query( $sql_query );
		}
		
		return $result;
	}
	
	public function execute( $table_name, $sql_query ){
        $CI =& get_instance();
		
		$table_exists = $CI->db->table_exists( $table_name );
		
		$result = FALSE;
		if( $table_exists == FALSE ){
			$result = $this->execute_query( $sql_query );
		}
		
		return $result;
	}
	
	public function create( $table_name, $fields, $params=array() ){
		$CI =& get_instance();		
		
		$table_exists = $CI->db->table_exists( $table_name );
		
		$params = $params ? $params: $this->params;
		extract($params);
		
		$result = FALSE;
		if( $table_exists == FALSE ){
			$CI->load->dbforge();
			
			$attributes = isset($attributes) ? $attributes : $this->attributes;
			$if_not_exists = isset($if_not_exists) ? $if_not_exists : $this->if_not_exists;
			
			$CI->dbforge->add_field( $fields );
			if( isset($primary_field) ){
				$CI->dbforge->add_key( $primary_field, TRUE );
			}
			$result = $CI->dbforge->create_table($table_name, $if_not_exists, $this->attributes);
		}
		
		return $result;
	}
}
?>