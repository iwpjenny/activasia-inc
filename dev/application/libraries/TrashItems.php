<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TrashItems {
	 
    function __construct(){
        $CI =& get_instance();
		$CI->load->model('Data_Model','Data');		
		$CI->load->model('modules/Cost_Estimate_Model','CE');		
		$CI->load->model('modules/Working_Budget_Model','WB');		
		$CI->load->model('modules/Cash_Advance_Model','CA');		
    }
	
	function trash( $model_origin, $id ){ 
		$CI =& get_instance();
		/*---------------------------------------------*/ 
		
		$wb_details = $CI->Data->get_records( 
			$CI->WB, array( 'where_params' =>array( $model_origin->tblid => $id ) ) 
		); 
		$wb_id_array=array();
		foreach($wb_details as $col){
			$wb_id_array[] = app_get_val($col,$CI->WB->tblid);
		}
		/* printx($wb_id_array); */
		$trash_wb_item = array($CI->WB->tblpref.'trashed'=>1); 
		$trash_CA_item = array($CI->CA->tblpref.'trashed'=>1); 
		$trash_ca = $CI->Data->update_record( $CI->CA, $trash_CA_item, array('where_in_key'=>$CI->WB->tblid, 'where_in_value'=>$wb_id_array));   
		$trash_wb = $CI->Data->update_data( $CI->WB, $trash_wb_item, array($model_origin->tblid=>$id) ); 
		
	}   
}
?>