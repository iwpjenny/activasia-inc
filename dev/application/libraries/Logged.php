<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logged {
	
	private $Data;
	
	/* triggers */
	public $trigger_before = array();
	public $trigger_after = array();
	public $trigger_on_success = array();
	public $trigger_on_error = array();
	
	/* return */
	public $error;
	public $result;
	public $notification;
	
	/* parameters */	
	public $dashboard_page;
	public $login_user_type;
	public $model;
	public $model_user;
	public $model_user_role;
	public $model_user_logs;
	public $model_log;

	function __construct( $params=array() ){
		$CI =& get_instance();
		
		$params_default = array_merge(array(
			'dashboard_page'=>NULL,
			'login_user_type'=>NULL,
			'model_user'=>NULL,
			'model_user_role'=>NULL,
			'model_user_logs'=>NULL,
			'model_log'=>NULL
			), $params
		);
		extract($params_default);
		
		$this->dashboard_page = $dashboard_page?$dashboard_page:$this->dashboard_page;
		$this->login_user_type = $login_user_type?$login_user_type:$this->login_user_type;
		$this->model_user = $model_user?$model_user:$this->model_user;
		$this->model_user_role = $model_user_role?$model_user_role:$this->model_user_role;
		$this->model_user_logs = $model_user_logs?$model_user_logs:$this->model_user_logs;
		$this->model_log = $model_log?$model_log:$this->model_log;
				
		$this->Data = $CI->Data;
    }

    private function remember_me( $username, $password ){
		$CI =& get_instance();
		
		$remember_me = $CI->input->post('remember_me');
		
		if( $remember_me == 'yes' ){
			$CI->cookies->set_login_user( $this->login_user_type, $username, $password, $remember_me );
		} else {
			$CI->cookies->delete_login_user( $this->login_user_type );	
		}
	}

    public function login(){
		$CI =& get_instance();
		
		$username = $CI->input->post('username');
		$type_password = $CI->input->post('password');
		$url_current = $CI->input->post('url_current');
		
		$user = $this->model_user->get_by_username_email( $username );
					
		$form_security_key = $this->check_submit_form_security_key();
		
		$url = url_previous();
		
		if( $form_security_key ){
			if( $user ){				
				$user_id = app_get_val($user,$this->model_user->tblid);
				$role_id = app_get_val($user,$this->model_user_role->tblid);
				$role_name = app_get_val($user,$this->model_user_role->tblpref.'name');
				$email = app_get_val($user,$this->model_user->tblpref.'email');
				$firstname = app_get_val($user,$this->model_user->tblpref.'firstname');
				$midname = app_get_val($user,$this->model_user->tblpref.'midname');
				$lastname = app_get_val($user,$this->model_user->tblpref.'lastname');
				$password = app_get_val($user,$this->model_user->tblpref.'password');
				$capabilities_serialized = app_get_val($user,$this->model_user_role->tblpref.'capabilities_serialized');
				
				if( $type_password == $password ){
					
					$this->remember_me( $username, $password );
					
					$user_data = array(
						'email'=>$email,
						'role_id'=>$role_id,
						'role_name'=>$role_name,
						'username'=>$username,
						'firstname'=>$firstname,
						'midname'=>$midname,
						'lastname'=>$lastname,
						'capabilities_serialized'=>$capabilities_serialized
					);
					$this->set_session( $user_id, $user_data );
										
					$url = $url_current?$url_current:site_url('c='.$this->dashboard_page); 
					
					$notify_params = array('action'=>'login','type'=>'success','log'=>$user_id);
					$CI->notify->set( $notify_params );
					
					redirect( $url );
				} else {
					$notify_params = array('action'=>'login','type'=>'error','log'=>$user_id);
				}
			} else {
				$text = '('.$username.')';
				$notify_params = array('action'=>'login','type'=>'not-exist','log'=>TRUE,'text'=>$text);
			}
		} else {
			$notify_params = array('action'=>'form-security','type'=>'did-not-match','log'=>TRUE);
		}
		
		$CI->notify->set( $notify_params );
		
		redirect_current();
    }

    public function check_submit_form_security_key(){
		$CI =& get_instance();
		
		$security_key = $CI->input->post('security_key');
		$login_security_key = $this->get_login_security_key();
		
		$result = FALSE;
		if( $security_key == $login_security_key ){
			$result = TRUE;
		}
				
		return $result;
	}
	
	private function set_session( $user_id, $user_data ){
		$CI =& get_instance();
		
		extract($user_data);
					
		$fullname = app_get_fullname_format( $firstname, $midname, $lastname );
		
		$logged = array(		
			'user_id'=>$user_id,
			'role_id'=>$role_id,
			'role_name'=>$role_name,
			'username'=>$username,
			'email'=>$email,
			'fullname'=>$fullname,
			'firstname'=>$firstname,
			'capabilities_serialized'=>$capabilities_serialized
		);
		
		$array = array(
			'logged'=>$logged,
			'login_user_type'=>$this->login_user_type,
			$this->login_user_type.'_login_user_id'=>$user_id,
			$this->login_user_type.'_login_role_id'=>$role_id,
			$this->login_user_type.'_login_role_name'=>$role_name,
			$this->login_user_type.'_login_username'=>$username,
			$this->login_user_type.'_login_email'=>$email,
			$this->login_user_type.'_login_fullname'=>$fullname,
			$this->login_user_type.'_login_firstname'=>$firstname,
			$this->login_user_type.'_login_capabilities_serialized'=>$capabilities_serialized
			//$this->login_user_type.'_login_urc'=>$urc
			//$this->login_user_type.'_login_urc_serialize'=>$urc_serialize
		);
		$CI->session->set_userdata($array);
		$CI->session->set_flashdata('login','yes');	
				
		//$return = $this->session->all_userdata();
	}
	
	public function check_if_user_login( $login_user_type='admin', $redirect='' ){
		
		$loggedin = $this->get_login_session('user_id');
		
		if( empty($loggedin) ){
			if( $redirect == '' ){
				$redirect = site_url();
			}
			redirect( $redirect );
		}
	}
	
	public function check_if_user_logout( $login_user_type='admin', $redirect='' ){
		
		$loggedin = $this->get_login_session('user_id');
		
		if( $loggedin ){
			if( $redirect == '' ){
				$redirect = site_url('c='.$this->dashboard_page);
			}
			redirect( $redirect );
		}
	}

	public function logout(){
		$CI =& get_instance();
		
		$user_id = $this->get_login_session('user_id');
		$username = $this->get_login_session('username');
		$fullname = $this->get_login_session('fullname');
		
		$text = '('.$username.' - '.$fullname.') ';
		$notify_params = array('action'=>'logout','type'=>'success','log'=>$user_id,'text'=>$text);
		$CI->notify->set( $notify_params );
		
		$this->exec_logout($this->login_user_type,TRUE);
		
		$url_query = 'c='.$this->model_log->page.'&url_current='.urlencode($_SERVER['HTTP_REFERER']);
		
		redirect( site_url( $url_query ) );
	}
	
	public function exec_logout( $login_user_type='admin', $destroy_all=FALSE ){
		$CI =& get_instance();
		
		$array = array(
			$login_user_type.'_login_user_id'=>'',
			$login_user_type.'_login_role_id'=>'',
			$login_user_type.'_login_role_name'=>'',
			$login_user_type.'_login_username'=>'',
			$login_user_type.'_login_email'=>'',
			$login_user_type.'_login_fullname'=>'',
			$login_user_type.'_login_firstname'=>'',
			$login_user_type.'_login_urc_serialize'=>''
		);
		$CI->session->unset_userdata($array);
		
		if( $destroy_all ){
			$CI->session->sess_destroy();
		}
	}
	
	public function check_email( $email, $model ){
		
		$result = FALSE;
		
		if( $email ){
			$result = $this->Data->get_record($model, 
				array( 'where_params' => array(
					$model->tblpref.'email'=>$email,
					$model->tblpref.'published'=>1
				)
			));
		}
		
		return $result;
	}
	
	public function if_account_is_published_by_email( $email, $model ){
		
		$published = 0;
		
		if( $email ){
			$details = $this->Data->get_record($model, 
				array( 'where_params' => array($model->tblpref.'email'=>$email)
			));
			$published = app_get_val($details,$model->tblpref.'published');
		}
		
		return $published;
	}
	
	public function set_login_security_key(){
		$CI =& get_instance();
		
		$key = rand(100,999);
		$key = sha1($key);
		
		$security_key = $CI->session->set_userdata('security_key', $key);
	}
	
	public function get_login_security_key(){
		$CI =& get_instance();
		
		$security_key = $CI->session->userdata('security_key');
		
		return $security_key;
	}
	
	public function check_login_permission( $login_user_type=NULL ){
		$CI =& get_instance();
		
		$login_user_id = $this->get_login_session('user_id');
		
		if( $login_user_id == FALSE ){
		
			$data = array();
			$data['url_login'] = site_url('c='.$this->model_log->page.'&url_current='.urlencode( url_current() ));
			$data['url_logout'] = site_url('c='.$this->model_log->page.'&m=logout&url_current='.urlencode( url_current() ));
		
			echo $CI->load->view('error-no-login-permission',$data,TRUE);
			exit();
		}
	}
	
	public function generate_password( $length=6 ) {
		
		$chars = "ABCDEFGHIJKLMNPQRSTUVWXYZ0123456789";
		$password = substr( str_shuffle($chars), 2, $length );
		
		return $password;
	}
	
	public function get_login_session( $variable ){
		$CI =& get_instance();
			
		$login_user_type = $this->get_login_user_type();
		
		$return = $CI->session->userdata($login_user_type.'_login_'.$variable);
		
		return $return;
	}
	
	public function get_logged_user_id(){
			
		$user_id = $this->get_login_session('user_id');
		
		return $user_id;
	}
	
	public function get_login_user_type(){
		$CI =& get_instance();
		
		$login_user_type = $CI->session->userdata('login_user_type');		
		$login_user_type = $this->login_user_type ? $this->login_user_type : $login_user_type;
		
		return $login_user_type;
	}
	
	public function set_login_session( $variable, $value ){
		$CI =& get_instance();
		
		$return = $CI->session->set_userdata($this->login_user_type.'_login_'.$variable, $value);
		
		return $return;
	}
	
	public function get_user_capabilities(){
		
		$capabilities_serialized = $this->get_login_session('capabilities_serialized');
		$capabilities = unserialize($capabilities_serialized);
		
		return $capabilities;
	}
	
	public function send_new_password( $model='' ){
		$CI =& get_instance();
		
		$CI->load->library('email');
		$email = $CI->input->post('email');
		
		$model = $model?$model:$this->model_user;
		
		$form_security_key = $this->check_submit_form_security_key();
			
		if( $form_security_key ){			
		
			
			$published = $this->if_account_is_published_by_email( $email, $model );
			$check_mail = $this->check_email( $email, $model );
			
			if( $published == 0 ){
				$text = '('.$email.')';
				$notify_params = array('action'=>'email','type'=>'block','log'=>TRUE,'text'=>$text);
			} elseif( $check_mail == FALSE ){
				$text = '('.$email.')';
				$notify_params = array('action'=>'email','type'=>'not-found','log'=>TRUE,'text'=>$text);
			} else {
				$from_email = 'support@iwebprovider.com';
				$from_name = 'iWP Support';
		
				$user_id = get_value( $check_mail, $model->tblid);
				$password = get_value( $check_mail, $model->tblpref.'password');
				$firstname = get_value( $check_mail, $model->tblpref.'firstname');
				$midname = get_value( $check_mail, $model->tblpref.'midname');
				$lastname = get_value( $check_mail, $model->tblpref.'lastname');
				$username = get_value( $check_mail, $model->tblpref.'username');
				$new_password = $this->generate_password();
				
				$user_data = array($model->tblpref.'password'=>$new_password);				
				$update_password = $this->Data->update_data($model, $user_data, array($model->tblid=>$user_id ));
				
				if( $midname ){
					$fullname = $firstname.' '.$midname.' '.$lastname;
				} else {
					$fullname = $firstname.' '.$lastname;
				} 
				
				$config['protocol'] = 'mail';
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = 'html';
				$config['charset'] = 'utf-8';
				$CI->email->initialize( $config );
				
				$data['firstname'] = $firstname;
				$data['email'] = $email;
				$data['fullname'] = $fullname;
				$data['username'] = $username;
				$data['new_password'] = $new_password;
				$data['from_name'] = $from_name;
				$message_body = $CI->load->view($this->model_log->location.'/template/forgot-password',$data,TRUE);
				
				$CI->email->from($from_email, $from_name);
				$CI->email->to( $email );
				$CI->email->subject('Password Recovery for DENR R-10 Account');
				$CI->email->message($message_body);
				$send = $CI->email->send();
				
				if( $send ){
					$message = 'Email Successfully Sent.';
					$CI->notify->set($message, 'success');
					
					$text = '('.$email.' - '.$firstname.')';
					$notify_params = array('action'=>'password','type'=>'success','log'=>$user_id,'text'=>$text);
				} else {
					$print_debugger = $CI->email->print_debugger();
					
					$notify_params = array('action'=>'email','type'=>'error','log'=>$user_id,'text'=>'('.$email.' - '.$firstname.') '.$print_debugger);
				}	
			}	
		} else {
			$text = '('.$email.')';
			$notify_params = array('action'=>'form-security','type'=>'did-not-match','log'=>TRUE,'text'=>$text);
		}		
		
		$CI->notify->set( $notify_params );
		
		redirect( site_url('c='.$this->model_log->page.'&m=forgotpassword') );
	}
	
	public function set_data_login_form( $data ){
		$CI =& get_instance();
		
		$data['security_key'] = $this->get_login_security_key();
		$data['url_current'] = $CI->input->get('url_current');
		$data['form_action'] = site_url('c='.$this->model_log->page.'&m=login');
		
		$data['remember_me'] = $CI->cookies->get( $this->login_user_type, 'remember_me' );
		$data['username'] = $CI->cookies->get( $this->login_user_type, 'username' );
		$data['password'] = $CI->cookies->get( $this->login_user_type, 'password' );
		
		return $data;
	}
	
	public function set_data_forgot_password( $data ){
		$CI =& get_instance();
		
		$data['form_action'] = site_url('c='.$this->model_log->page.'&m=retrievepassword');
		 
		$data['security_key'] = $this->get_login_security_key();
		$data['url_current'] = $CI->input->get('url_current');
		
		return $data;
	}

}