<?php
$blank_line = '________________________________________';
$variable = 'sample';
?>
<html>
	<head>
		<style type="text/css"> 
		</style>
	</head>
	<body> 
		<table style="width:100%">
			<tr>
				<td width="5%" align="left"><img src="" style="width:80px;height:80px"></td>
				<td width="25%" align="left">SRPAO Form 3<br />Series of 2013</td>
				<td width="70%" align="right">Annex D</td>
			</tr>
		</table>
		<table style="width:100%">
		</table>
		<table style="width:100%">
			<tr><th align="center">DEPARTMENT OF ENVIRONMENT AND NATURAL RESOURCES</th></tr> 
			<tr><th align="center">REGION ____________________________________</th></tr>  
		</table> 
		<table style="width:100%;margin-top:20px;"> 
			<tr><th align="center">SURVEY AND REGISTRATION OF PROTECTED AREA OCCUPANTS</th></tr>   
		</table> 
		<table style="width:100%;margin-top:20px;"> 
			<tr><th align="center">FORM 3</th></tr>  
			<tr><th align="center">Summary Sheet</th></tr> 
		</table>  
		<table style="width:100%;margin-top:20px;">
			<tr>
				<td width="5%">Name of Protected Area:</td> 
				<td width="25%" style="border-bottom:1px solid black; padding-right: 15px"><?php echo $variable;?></td> 
				<td width="4%"></td> 
			</tr>
			<tr>  
				<td>Municipality:</td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $variable;?></td> 
				<td></td> 
			</tr>
		</table> 
		<table style="width:100%;margin-top:20px;"> 
			<tr><td align="left">Demographic Information:</td></tr>  
		</table>
		<table style="width:100%;border-collapse:collapse">
			<thead style="text-align:center;"> 
				<tr> 
					<th rowspan="2" style="border:1px solid black;">Name of Barangay</th>
					<th colspan="3" scope="colgroup" style="border:1px solid black;">Household Heads</th>
					<th colspan="3" scope="colgroup" style="border:1px solid black;">Household Members</th>
					<th colspan="3" scope="colgroup" style="border:1px solid black;">Total Area Occupied (ha)</th>
					<th rowspan="2" style="border:1px solid black;">Total No. of Qualified <br /> Tenured Migrants</th>
				</tr> 
				<tr>
					<th scope="col" style="border:1px solid black;">Male</th>
					<th scope="col" style="border:1px solid black;">Female</th>
					<th scope="col" style="border:1px solid black;">Total</th>
					<th scope="col" style="border:1px solid black;">Male</th>
					<th scope="col" style="border:1px solid black;">Female</th>
					<th scope="col" style="border:1px solid black;">Total</th>
					<th scope="col" style="border:1px solid black;">Male</th>
					<th scope="col" style="border:1px solid black;">Female</th>
					<th scope="col" style="border:1px solid black;">Total</th>
				</tr>
			</thead>
			<tbody style="text-align:center;"> 
				<tr> 
					<th scope="col" style="border:1px solid black;">Sample</th> 
					<th scope="col" style="border:1px solid black;">Sample</th> 
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th> 
					<th scope="col" style="border:1px solid black;">Sample</th> 
					<th scope="col" style="border:1px solid black;">Sample</th> 
					<th scope="col" style="border:1px solid black;">Sample</th> 
					<th scope="col" style="border:1px solid black;">Sample</th> 
					<th scope="col" style="border:1px solid black;">Sample</th> 
					<th scope="col" style="border:1px solid black;">Sample</th> 		
					<th scope="col" style="border:1px solid black;">Sample</th> 
				</tr> 
			</tbody>
		</table>  
		<table style="width:100%;margin-top:30px;"> 
			<tr> 
				<td width="30%"><em>Noted By:</em></td> 
				<td width="5%"></td> 
				<td width="30%"><em>Attested By:</em></td> 
				<td width="5%"></td> 
				<td width="30%"><em>Prepared By: Team Members</em></td>
			</tr>
		</table>
		<table style="width:100%;margin-top:30px;border-collapse:collapse">
			<tr>  
				<td width="30%" style="border-bottom:1px solid black;"></td> 
				<td width="5%"></td> 
				<td width="30%" style="border-bottom:1px solid black;"></td> 
				<td width="5%"></td> 
				<td width="30%" style="border-bottom:1px solid black;"></td> 
			</tr>
			<tr>  
				<td align="center"><em>PENRO/ CENRO</em></td> 
				<td ></td> 
				<td align="center"><em>Team Leader</em></td> 
			</tr>
		</table> 
		<table style="width:100%;margin-top:20px;border-collapse:collapse">
			<tr>  
				<td width="30%" style="border-bottom:1px solid white;"></td> 
				<td width="5%"></td> 
				<td width="30%" style="border-bottom:1px solid white;"></td> 
				<td width="5%"></td> 
				<td width="30%" style="border-bottom:1px solid black;"></td> 
			</tr>
		</table>
		<table style="width:100%;margin-top:35px;border-collapse:collapse">
			<tr>  
				<td width="30%" style="border-bottom:1px solid white;"></td> 
				<td width="5%"></td> 
				<td width="30%" style="border-bottom:1px solid black;"></td> 
				<td width="5%"></td> 
				<td width="30%" style="border-bottom:1px solid black;"></td> 
			</tr>
			<tr>  
				<td align="center"></td> 
				<td ></td> 
				<td align="center"><em>DATE</em></td> 
			</tr>
		</table>
		<table style="width:100%;margin-top:15px;border-collapse:collapse">
			<tr>  
				<td width="30%" style="border-bottom:1px solid white;"></td> 
				<td width="5%"></td> 
				<td width="30%" style="border-bottom:1px solid white;"></td> 
				<td width="5%"></td> 
				<td width="30%" style="border-bottom:1px solid black;"></td> 
			</tr>
			<tr>  
				<td align="center"></td> 
				<td ></td> 
				<td align="center"></td> 
			</tr>
		</table>
	</body>
</html>