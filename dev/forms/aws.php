<?php
$blank_line = '_____________________';
$variable = 'sample';
?>
<html>
	<head>
		<style type="text/css">
		th {
			height: 95px;
		}
		</style>
	</head>
	<body> 
	
		<table style="width:100%; margin-top:-50px;">
			<tr><td align="center">DEPARTMENT OF ENVIRONMENT AND NATURAL RESOURCES</td></tr> 
			<tr><td align="center">REGION ____________________________________</td></tr>  
		</table> 
		<table style="width:100%;margin-top:20px;">
			<tr><td align="center">WATERBIRD CENSUS</td></tr> 
		</table> 
		<table style="width:100%;margin-top:20px;"> 	
			<tr><td align="center">2017</td></tr> 
		</table>  
	
		<table style="width:100%;margin-top:40px;border-collapse:collapse;">
			<thead style="text-align:center;">  
				<tr> 
					<th colspan="1" style="border:1px solid black;">Name of Species</th>
					<th colspan="1" style="border:1px solid black;">Scientific Name</th>
					<th colspan="1" style="border:1px solid black;">Pulangi Wetland</th>
					<th colspan="1" style="border:1px solid black;">Megpangi Wetland Area</th>
					<th style="border:1px solid black;">Sinacaban Wetland Area</th>
					<th style="border:1px solid black;">Matampay Bukana Wetland</th>
					<th style="border:1px solid black;">Mukascaromatan</th>
					<th style="border:1px solid black;">Opol Wetland Area</th>
					<th style="border:1px solid black;">Alubijid Wetland</th>
					<th style="border:1px solid black;">Taytay Wetland</th>
					<th style="border:1px solid black;">Magsaysay Wetland<br /></th>
					<th style="border:1px solid black;">Pangasihan Wetland</th>
					<th style="border:1px solid black;">Total<br /></th>		
				</tr>
				<tr> 
					<th style="border:1px solid black;"></th>
					<th style="border:1px solid black;"></th>
					<th style="border:1px solid black;"></th>
					<th style="border:1px solid black;"></th>
					<th style="border:1px solid black;"></th>
					<th style="border:1px solid black;"></th>
					<th style="border:1px solid black;"></th>
					<th style="border:1px solid black;"></th>
					<th style="border:1px solid black;"></th>
					<th style="border:1px solid black;"></th>
					<th style="border:1px solid black;"></th>
					<th style="border:1px solid black;"></th>
					<th style="border:1px solid black;"></th>		
				</tr>
			</thead>
			<tbody style="text-align:center;"> 
				 <?php echo $waterbird_content;?>
			</tbody>
		</table> 
		<br></br>
	</body>
</html>