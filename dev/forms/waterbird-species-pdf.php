<?php
$blank_line = '_____________________';
$variable = 'sample';
?>
<html>
	<head>
		<style type="text/css">
		th {
			height: 40px;
		}
		

		</style>
	</head>
	<body> 
		<table style="width:100%">
			<tr>
				<td width="5%" align="left"><img src="" style="width:80px;height:80px"></td>
				<td width="25%" align="left">SRPAO <br />Series of 2013</td>
				<td width="70%" align="right"></td>
			</tr>
		</table>
		<table style="width:100%; margin-top:-50px;">
			<tr><td align="center">DEPARTMENT OF ENVIRONMENT AND NATURAL RESOURCES</td></tr> 
			<tr><td align="center">REGION ____________________________________</td></tr>  
		</table> 
		<table style="width:100%;margin-top:20px;"> 	
			<tr><td align="center">ASIAN WATERBIRD CENSUS</td></tr> 
		</table>  
		<table style="margin-left:100px;align:center;width:85%;margin-top:40px;border-collapse:collapse;"> 
			<tr> 
				<td colspan="2" style="font-size:160%;background:black;color:white;border:1px solid black;text-align:center;">Asian Waterbird Census (Southeast Asia)</td>
				<td width="10%" rowspan="2">Diri ang image</td>
			</tr>
			<tr> 
				<td width="40%" style="padding:15px;border:1px solid black;">Please return to your National Co-ordinator or Wetland International, 3A39, Kelana Centre Point, No. 3, Jalan SS7/19, Kelana Jaya 47301, MALAYSIA  (before March)</td>
				<td width="27%" style="margin-bottom:10px;border:1px solid black;">Country:</td>
			</tr>
			<tr> 
				<td colspan="3"> 
					<table style="width:100%;border-collapse:collapse;"> 	
						<tr>
							<td width="60%" style="border:1px solid black;padding:10px;">Name of Site:</td>
							<td width="20%" colspan="3"style="border:1px solid black;">Date:</td>
						</tr> 
					</table> 	
				</td>
			</tr>
			<tr> 
				<td colspan="3"> 
					<table style="width:100%;border-collapse:collapse;"> 	
						<tr>
							<td width="60%" style="border:1px solid black;padding:10px;">Province/State/Prefecture:<br />Nearest Large Town: </td>
							<td width="40%" style="border:1px solid black;">Site Code (only for official use):</td>
						</tr> 
					</table> 	
				</td>
			</tr>
			<tr> 
				<td colspan="3"> 
					<table style="width:100%;border-collapse:collapse;"> 	
						<tr>
							<td width="50%" style="border:1px solid black;padding:10px;">Type: A - Aerial,  F - On foot,  B - By boat,  M - Mixed<br /> Coverage: V-25%, W-25-50%, X-50-75%, Y-75-99%, Z-100% </td>
							<td width="50%" style="border:1px solid black;">Has the site been counted before?<p style="word-spacing:120px;">Yes No</p></td>
						</tr> 
					</table> 	
				</td>
			</tr>
			<td style="border:1px solid black;padding:10px;"> 
					<table style="width:90%;border-collapse:collapse;"> 	
						<tr>
							<td colspan="3"><h2>Waterfall Counts</h2></td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Little Grebe Tachybaptus ruficollis</td>
						</tr>
						<tr>
							<td></td>
							<td>CORMORANTS & DARTERS</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Unidentified cormorants</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Oriental Darter Anhinga melanogaster</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Little Cormorant P. nige</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Unidentified cormorant</td>
						</tr>
						<tr>
							<td></td>
							<td>HERONS & EGRETS</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Great Bittern Botaurus stellaris</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Yellow Bittern Ixobrychus sinensis</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Schrenck’s Bittern I. eurhythmus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Cinnamon Bittern /. cinnamomeus</td>
						</tr>
						<tr>
							<td></td>
							<td>Black Bittern I. flavicollis</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Japanese Night Heron Gorsachius goisagi</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Malayan Night Heron (Tiger Bittern) G. melanolophus </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Black-crowned Night Heron Nycticorax nycticorax</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Rufous Night Heron N. caledonicus</td>
						</tr>
						<tr>
							<td></td>
							<td>Indian Pond Heron Ardeola grayii</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Chinese Pond Heron A. bacchus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Javan Pond Heron A. speciosa</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Cattle Egret Bubulcus ibis</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Striated (Little Green) Heron Butorides striatus</td>
						</tr>
						<tr>
							<td></td>
							<td>Eastern Reef Egret Egretta sacra</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Chinese (Swinhoe’s) Egret E. eulophotes</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Little Egret E. garzetta </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Intermediate Egret E. intermedia</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Great Egret E. alba</td>
						</tr>
						<tr>
							<td></td>
							<td>Purple Heron Ardea purpurea</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Grey Heron A. cinerea</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Unidentified herons and egrets</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Cattle Egret Bubulcus ibis</td>
						</tr>
						<tr>
							<td></td>
							<td>STORKS</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Milky Stork Mycteria cinerea</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Painted Stork M. leucocephala</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Asian Openbill Anastomus oscitans</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Black Stork Ciconia nigra</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Wooly-necked Stork C. episcopus</td>
						</tr>
						<tr>
							<td></td>
							<td>Storm’s Stork C. stormi</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Black-necked Stork Ephippiorhynchus asiaticus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Lesser Adjutant Leptoptilos javanicus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Greater Adjutant L. dubius</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Unidentified storks</td>
						</tr>
						<tr>
							<td></td>
							<td>IBISES & SPOONBILLS</td>
						</tr>
						<tr>
							<td></td>
							<td>Black-headed (White) Ibis Threskiornis melanocephalus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>White-shouldered Ibis Pseudibis davisoni</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Giant Ibis Thaumatibis gigantea</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Glossy Ibis Plegadis falcinellus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>White Spoonbill Platalea leucorodia</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Black-faced Spoonbill P. minor</td>
						</tr>
						<tr>
							<td></td>
							<td>Unidentified Spoonbills</td>
						</tr>
						<tr>
							<td></td>
							<td>Moorhen Gallinula chloropus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Purple Swamphen Porphyrio porphyrio</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Common Coot Fulica atra</td>
						</tr>
						<tr>
							<td></td>
							<td>FINFOOT & JACANAS</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Masked Finfoot Heliopais personata</td>
						</tr>
						<tr>
							<td></td>
							<td>Comb-crested Jacana Irediparra gallinacea</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Pheasant-tailed Jacana Hydrophasianus chirurgus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Bronze-winged Jacana Metopidius indicus</td>
						</tr>
						<tr>
							<td></td>
							<td>SHOREBIRDS- WADERS</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Painted Snipe Rostratula benghalensis</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Crab Plover Dromas ardeola</td>
						</tr>
						<tr>
							<td></td>
							<td>Black-winged Stilt Himantopus himantopus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Australian (White-headed) Stilt H. leucocephalus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Avocet Recurvirostra avosetta</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Great Thick-knee Esacus recurvirostris</td>
						</tr>
						<tr>
							<td></td>
							<td>Beach Thick-knee E. magnirostris</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Oriental Pratincole Glareola maldivarum</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Little Pratincole G. lactea</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Northern Lapwing Vanellus vanellus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>River Lapwing V. duvaucelii</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Grey-headed Lapwing V. cinereus</td>
						</tr>
						<tr>
							<td></td>
							<td>Red-wattled Lapwing V. indicus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Pacific Golden Plover Pluvialis fulva</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Grey Plover P. squatarola</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Long-billed Plover Charadrius placidus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Little Ringed Plover C. dubius</td>
						</tr>
						<tr>
							<td></td>
							<td>Kentish Plover C. alexandrinus</td>
						</tr>
						<tr>
							<td></td>
							<td>Malaysian Plover C. peronii</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Mongolian Plover C. mongolus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Greater Sand Plover C. leschenaultii</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Oriental Plover C. veredus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Black-tailed Godwit Limosa limosa</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Bar-tailed Godwit L. lapponica</td>
						</tr>
						<tr>
							<td></td>
							<td>Little Curlew Numenius minutus</td>
						</tr>
						<tr>
							<td></td>
							<td>Whimbrel N. phaeopus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Eurasian Curlew N. arquata</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Far Eastern Curlew N. madagascariensis</td>
						</tr>
						<tr>
							<td></td>
							<td>Spotted Redshank Tringa erythropus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Redshank T. totanus</td>
						</tr>
						<tr>
							<td></td>
							<td>Marsh Sandpiper T. stagnatilis </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Greenshank T. nebularia </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Nordmann’s Greenshank T. guttifer </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Green Sandpiper T. ochropus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Wood Sandpiper T. glareola </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Terek Sandpiper Xenus cinereus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Common Sandpiper Actitis hypoleucos</td>
						</tr>
				</table>
				<!-- ====================================== -->
				
				<td colspan="3" style="border:1px solid black;padding:10px;"> 
					<table style="width:75%;border-collapse:collapse;"> 
						<tr>
							<td width="5%"></td>
							<td>GEESE & DUCKS</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Spotted Whistling Duck Dendrocygna guttata</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Fulvous (Large) Whistling Duck D. bicolor</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Wandering Whistling Duck D. arcuata </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Lesser Whistling Duck (Lesser Tree Duck) D. javanica</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Greylag Goose Anser anser </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Bar-headed Goose A. indicus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td> Ruddy Shelduck Tadorna ferruginea</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Common Shelduck T. tadorna </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>White-winged Wood Duck Cairina scutulata</td>
						</tr>
						<tr>
							<td></td>
							<td>Comb Duck Sarkidiornis melanotos </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Indian Cotton Teal Nettapus coromandelianus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Eurasian Wigeon Anas penelope</td>
						</tr>
						<tr>
							<td></td>
							<td>Falcated Teal A. falcata </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Gadwall A. strepera</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Common (Green-winged) Teal A. crecca </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Grey Teal A. gibberifrons</td>
						</tr>
						<tr>
							<td></td>
							<td>Mallard A. platyrhynchos</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Spot-billed Duck A. poecilorhyncha</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Philippine Duck A. luzonica </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Northern Pintail A. acuta </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Garganey A. querquedula</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Northern Shoveler A. clypeata</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Red-crested Pochard Netta rufina </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Common Pochard Aythya ferina</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Baer’s Pochard A. baeri </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Ferruginous Duck A. nyroca</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Tufted Duck A. fuligula</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Goosander M. merganser</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Unidentified ducks</td>
						</tr>
						<tr>
							<td></td>
							<td>CRANES</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Common Crane Grus grus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Sarus Crane G. antigone</td>
						</tr>
						<tr>
							<td></td>
							<td>RAILS, GALLINULES & COOTS </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Water Rail Rallus aquaticus </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Slaty-breasted Rail R. striatus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Banded Rail R. philippensis </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Barred Rail R. torquatus </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Red-legged Crake Rallina fasciata</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Slaty-legged Crake R. eurizonoides </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Baillon’s Crake Porzana pusilia</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Ruddy Crake P. fusca </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Band-bellied Crake P. paykullii </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Spotless Crake P. tabuensis</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>White-browed Crake  P. Cinereus (Poliolimnas cinereus) </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Brown Crake Amaurornis akool</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Bush-Hen A. olivacea</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>White-breasted Waterhen A. phoenicurus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Watercock Gallicrex cinerea</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Grey-tailed (Grey-rumped) Tattler Heteroscelus brevipes</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Ruddy Turnstone Arenaria interpres</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Red-necked Phalarope Phalaropus Iobatus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Eurasian Woodcock Scolopax rusticola</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Pintail Snipe Gallinago stenura</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Swinhoe’s Snipe G. megala</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Common Snipe G. gallinago</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Asiatic Dowitcher Limnodromus semipalmatus </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Red Knot Calidris canutus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Great Knot C. tenuirostris</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Sanderling C. alba</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Red-necked (Rufous-necked) Stint C. ruficollis </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Temminck’s Stint C. temminckii</td>
						</tr>
						<tr>
							<td>_________</td>
							<td> Long-toed Stint C. subminuta</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Sharp-tailed Sandpiper C. acuminata</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Dunlin C. alpina</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Curlew Sandpiper C. ferruginea</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Moorhen Gallinula chloropus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Spoon-billed Sandpiper Eurynorhynchus pygmeus </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Broad-billed Sandpiper Limicola falcinellus </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Unidentified shorebirds </td>
						</tr>
						<tr>
							<td width="10%"></td>
							<td>GULLS, TERNS & SKIMMERS </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Herring Gull Larus argentatus </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Brown-headed Gull L. brunnicephalus </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Black-headed Gull L. ridibundus </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Saunders’ Gull L. saundersi</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Unidentified gulls</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Whiskered Tern Chlidonias hybridus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>White-winged Black Tern C. leucopterus</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Gull-billed Tern Gelochelidon nilotica </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Avocet Recurvirostra avosetta</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Great Thick-knee Esacus recurvirostris</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Caspian Tern Hydroprogne caspia </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Indian River Tern Sterna aurantia </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Common Tern S. hirundo </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Black-naped Tern S. sumatrana </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Black-bellied Tern S. melanogaster </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Little Tern S. albifrons</td>
						</tr>
								<tr>
							<td>_________</td>
							<td>Great Crested Tern S. bergii </td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Lesser Crested Tern S. bengalensis</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Unidentified terns</td>
						</tr>
						<tr>
							<td>_________</td>
							<td>Indian Skimmer Rynchops albicollis</td>
						</tr>  
						<tr>
							<td colspan="2" align="center">ADDITIONAL SPECIES </td> 
						</tr> 
						<tr>
							<td colspan="2" style="border-bottom:1px solid black;padding:10px;"></td> 
						</tr> 
						<tr>
							<td colspan="2" style="border-bottom:1px solid black;padding:10px;"></td> 
						</tr> 
						<tr>
							<td colspan="2" style="border-bottom:1px solid black;padding:10px;"></td> 
						</tr> 
						<tr>
							<td colspan="2" style="border-bottom:1px solid black;padding:10px;"></td> 
						</tr> 
				</table>
				<tr> 
				<td colspan="3"> 
					<table style="width:100%;border-collapse:collapse;"> 	
						<tr>
							<td width="50%" style="word-spacing:2px;border:1px solid black;padding:10px;"><strong>USEFUL SITE INFORMATION:</strong> (please circle the relevant figures)<br /><u>CONDITION OF WETLAND:</u> 1. Wet (water present), 2 Totally dry,  3. Totally frozen<br /><br /><u>PROTECTION</u><br /><br /></td>
						</tr> 
						<tr>
							<td width="50%" style="border:1px solid black;padding:10px;"><u>THREATS AND USES:</u>	 0 Unknown, 1 None, 2 Sedimentation, 3 Excessive overgrowth of vegetation, 4 Cutting/clearance of vegetation, 5 Eutrophication, 6 Agriculture along drying margins, 7 Excessive cattle grazing, Pollution by: 8 domestic sewage, 9 solid waste, A industrial waste, B oil, C pesticides, D fertilizers, E Mining, F Hunting/trapping/poaching of birds, G Little fishing, H Large scale fishing, I Partial reclamation, J Complete reclamation, K Dam/barrage construction, L Tourism/recreation<br /><br /></td>
						</tr> 
						<tr>
							<td width="50%" style="word-spacing:10px;border:1px solid black;padding:10px;"><strong>THREATS AND USES:</strong>TIME OF COUNT:    START           am/pm FINISH am/pm<br /><br /><br /></td>
						</tr>
						<tr>
							<td width="50%" style="border:1px solid black;padding:10px;"><strong>PARTICIPANT(S) NAME (S) AND ADDRESS(ES):</strong><br /><br /><br /><br /></td>
						</tr> 	
					</table> 	
				</td>
			</tr>	
		</table> 
		<br><br />
	</body>
</html>