<?php
$blank_line = '________________________________________';
$variable = 'sample';
?>
<html>
	<head>
		<style type="text/css"> 
		</style>
	</head>
	<body> 
		<div id="footer">
			<p class="page" start="9"><?php $PAGE_NUM ?></p>
	   </div>
		<div id="content">
			<!--<table style="width:100%">
				<tr>
					<td width="5%" align="left"><img src="<?php echo base_url('uploads/logo/denr-logo.png');?>" style="width:80px;height:80px"></td>
					<td width="25%" align="left">SRPAO Form 6, page 1 0f 2<br />Series of 2013</td>
					<td width="70%" align="right">Annex G, Page 1 of 2</td>
				</tr>
			</table> -->
			<table style="width:100%">
				<tr><td align="center">DEPARTMENT OF ENVIRONMENT AND NATURAL RESOURCES</td></tr> 
				<tr><td align="center">REGION ____________________________________</td></tr>  
			</table> 
			<table style="width:100%;margin-top:20px;"> 
				<tr><td align="center">SURVEY AND REGISTRATION OF PROTECTED AREA OCCUPANTS</td></tr>  
			</table> 
			<table style="width:100%;margin-top:20px;"> 
				<tr><td align="center">FORM 6</td></tr>  
				<tr><td align="center">Questionnaire for Indigenous People</td></tr> 
			</table>  
			<table style="width:100%;margin-top:20px;">
				<tr>
					<td width="12%">1. PENRO</td> 
					<td width="1%">:</td> 
					<td width="25%" style="border-bottom:1px solid black; padding-right: 15px"><?php echo $variable;?></td> 
					<td width="4%"></td> 
					<td width="10%">6. Barangay</td> 
					<td width="1%">:</td> 
					<td width="25%" style="border-bottom:1px solid black; padding-right: 15px"><?php echo $variable;?></td> 
					<td width="4%"></td> 
				</tr>
				<tr>  
					<td>2. CENRO</td> 
					<td>:</td>  
					<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $variable;?></td> 
					<td></td> 
					<td>7. Sitio</td> 
					<td>:</td> 
					<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $variable;?></td> 
					<td></td> 
				</tr>
				</tr>
					<tr>
					<td>3. Name of Protected Area</td> 
					<td>:</td> 
					<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $variable;?></td> 
					<td></td> 
					<td>8. Name of Tribe</td> 
					<td>:</td> 
					<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $variable;?></td> 
					<td></td> 
				</tr>
				</tr>
					<tr>
					<td>4. Province</td> 
					<td>:</td> 
					<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $variable;?></td> 
					<td></td> 
					<td></td> 
					<td></td> 
					<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $variable;?></td> 
					<td></td>
				</tr>
				</tr>
					<tr>
					<td>5. City/Municipality</td> 
					<td>:</td> 
					<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $variable;?></td> 
					<td></td> 
				</tr>
			</table> 
			<table style="width:100%;margin-top:5px;border-collapse:collapse">
				<tr> 
					<td><h3>A. Demographic Information:</h3></td>
				</tr>
			</table>
			<table style="width:100%;margin-top:20px;border-collapse:collapse">
				<thead style="text-align:center;">  
					<tr>
					<col>
					</colgroup>
						<th colspan="3" scope="colgroup" style="border:1px solid black;">POPULATION</th>
						<th rowspan="2" style="border:1px solid black;">AREA<br />OCCUPIED<br />(hectare)</th>
						<th rowspan="2" style="border:1px solid black;">EXISTING LAND-USE/ ZONING</th>
						<th colspan="4" scope="colgroup" style="border:1px solid black;">COUNCIL OF ELDERS/ LEADERS</th>
					</tr>
					<tr>
						<th scope="col" style="border:1px solid black;">MALE</th>
						<th scope="col" style="border:1px solid black;">FEMALE</th>
						<th scope="col" style="border:1px solid black;">TOTAL</th>
						<th scope="col" style="border:1px solid black;">NAME</th>
						<th scope="col" style="border:1px solid black;">MALE/<br />FEMALE<br />(Please<br />Check)</th>  
					</tr> 
					<tr>
						<th scope="col" style="border:1px solid black;">M</th>
						<th scope="col" style="border:1px solid black;">F</th>
					</tr>
				</thead>
				<tbody style="text-align:center;">
					<tr> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
					</tr> 
				</tbody>
			</table>
			<span style="width:100%;">Note: All Pages of the form should be signed by the Team Leader</span>
			<div class="page">
				<div style="position: absolute;bottom: 10px; right:; ">18</div>
			</div>
			<div style="page-break-after: always;"></div> 
			<!--<table style="width:100%">
				<tr>
					<td width="5%" align="left"><img src="<?php echo base_url('uploads/logo/denr-logo.png');?>" style="width:80px;height:80px"></td>
					<td width="25%" align="left">SRPAO Form 6, page 2 0f 2<br />Series of 2013</td>
					<td width="70%" align="right">Annex G, Page 2 of 2</td>
				</tr>
			</table> -->
			<table style="width:100%;margin-top:10px;"> 
				<tr><td align="center"><strong>SURVEY AND REGISTRATION OF PROTECTED AREA OCCUPANTS</strong></td></tr>  
			</table> 
			<table style="width:100%"> 
				<tr><td align="center"><strong>(Form 6)</strong></td></tr>
			</table>  
			<table style="width:100%;margin-top:5px;border-collapse:collapse">
				<tr> 
					<td><h3>B. Livelihood Activities:</h3></td>
				</tr>
			</table>
			<table style="width:100%;margin-top:10px;border-collapse:collapse">
				<thead style="text-align:center;">  
					<tr> 
						<th rowspan="2" style="border:1px solid black;">Sources of<br />Income</th>
						<th rowspan="2" style="border:1px solid black;">Kinds of<br />Species/Crops</th>
						<th rowspan="2" style="border:1px solid black;">Area<br />Cultivated/<br />Developed<br />(ha)</th>
						<th rowspan="2" style="border:1px solid black;">Specific Site</th>
						<th rowspan="2" style="border:1px solid black;">Gathering/<br />Harvesting/<br />Collection<br />Method/Activity</th>
						<th rowspan="2" style="border:1px solid black;">Season/Period of<br />Gathering/<br />Harvesting<br />Time Spent or<br />Catch Effort</th>
						<th colspan="2" scope="colgroup" style="border:1px solid black;">Purpose for Gathering/<br />Collection</th>
						<th colspan="2" scope="colgroup" style="border:1px solid black;">Market</th>
						<th colspan="2" scope="colgroup" style="border:1px solid black;">Type of<br />Payment</th>
						<th rowspan="2" style="border:1px solid black;">Remarks</th>
					</tr> 
					<tr>
						<th scope="col" style="border:1px solid black;">Home<br />Consumption</th>
						<th scope="col" style="border:1px solid black;">Marketing</th>
						<th scope="col" style="border:1px solid black;">Middle<br /> Man</th>
						<th scope="col" style="border:1px solid black;">Direct</th>
						<th scope="col" style="border:1px solid black;">Cash</th>
						<th scope="col" style="border:1px solid black;">Non-Cash</th>
					</tr>
				</thead>
				<tbody style="text-align:center;"> 
					<tr> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
					</tr> 
					<tr> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
					</tr> 
					<tr> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
					</tr> 
					<tr> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
					</tr> 
					<tr> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
					</tr> 
					<tr> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
					</tr> 
				</tbody>
			</table> 
			<span style="width:100%;text-align:left;">Note: All Pages of the form should be signed by the Team Leader</span>
			
			<table style="width:100%;margin-top:20px;border-collapse:collapse">
				<tr> 
					<td><h4>CERTIFICATION</h4></td>
				</tr>
				<tr> 
					<td>I hereby certify under oath that the above information are<br />true and correct to the best of my knowledge and belief.</td>
				</tr>
			</table> 
			<table style="width:100%;margin-top:30px;border-collapse:collapse">
				<tr>  
					<td width="45%" style="border-bottom:1px solid black;"></td> 
					<td width="10%"></td> 
					<td width="45%" style="border-bottom:1px solid black;"></td> 
				</tr>
				<tr>  
					<td align="center">Printed name and signature of interviewer/ Enumerator</td> 
					<td ></td> 
					<td align="center">Printed name and signature of Team Leader</td> 
				</tr>
			</table> 
			<table style="width:100%;margin-top:25px;border-collapse:collapse">
				<tr>  
					<td width="45%" style="border-bottom:1px solid black;"></td> 
					<td width="55%"></td>  
				</tr>
				<tr>  
					<td align="center">Date</td> 
					<td ></td>
				</tr>
			</table>
			<table style="width:100%;margin-top:15px;border-collapse:collapse"> 
				<tr> 
					<td><small>Please attach map scale 1:50,000 showing the exact or a approximate location of the homelot/farmlot within the said protected area</small></td>
					<td style="text-align: right">19</td>
				</tr>	 
			</table>  
		</div> 
	</body>
</html>