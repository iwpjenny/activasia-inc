<?php
$var = null;
?>
<html>
	<head>
		<style type="text/css"> 
		</style>
	</head>
	<body> 
		<table style="width:100%">
			<tr>
				<td width="5%" align="left"><img src="<?php echo base_url('uploads/logo/denr-logo.png');?>" style="width:80px;height:80px"></td>
				<td width="25%" align="left">SRPAO Form 1, page 1 0f 3<br />Series of 2013</td>
				<td width="70%" align="right">Annex B, Page 1 of 3</td>
			</tr>
		</table>
		<table style="width:100%">
			<tr><td align="center">DEPARTMENT OF ENVIRONMENT AND NATURAL RESOURCES</td></tr> 
			<tr><td align="center">REGION ____________________________________</td></tr>  
		</table> 
		<table style="width:100%;margin-top:20px;"> 
			<tr><td align="center">SURVEY AND REGISTRATION OF PROTECTED AREA OCCUPANTS</td></tr>  
			<tr><td align="center">(Per Household)</td></tr>  
		</table> 
		<table style="width:100%;margin-top:20px;"> 
			<tr><td align="center">FORM 1</td></tr>  
			<tr><td align="center">Questionnaire</td></tr> 
		</table>  
		<table style="width:100%;margin-top:20px;">
			<tr>
				<td width="19%">1. PENRO</td> 
				<td width="2%">:</td> 
				<td width="25%" style="border-bottom:1px solid black; padding-right: 15px"><?php echo $penro;?></td> 
				<td width="4%"></td> 
				<td width="19%">10. Proofs of Occupancy</td> 
				<td width="2%">:</td> 
				<td width="25%" style="border-bottom:1px solid black; padding-right: 15px"><?php echo $proofs_occupancy;?></td> 
				<td width="4%"></td> 
			</tr>
			<tr>  
				<td>2. CENRO</td> 
				<td>:</td>  
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $cenro;?></td> 
				<td></td> 
				<td>11. Area of Homelot(ha)</td> 
				<td>:</td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $area_homelot;?></td> 
				<td></td> 
			</tr>
			</tr>
				<tr>
				<td>3. Name of Protected Area</td> 
				<td>:</td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $protected_area;?></td> 
				<td></td> 
				<td>Location (GPS Reading)</td> 
				<td>:</td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $homelot_latitude;?></td> 
				<td></td> 
			</tr>
			</tr>
				<tr>
				<td>4. Province</td> 
				<td>:</td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $province;?></td> 
				<td></td> 
				<td></td> 
				<td></td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $homelot_longitude;?></td> 
				<td></td> 
			</tr>
			</tr>
				<tr>
				<td>4. City/Municipality</td> 
				<td>:</td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $city;?></td> 
				<td></td> 
				<td>12. Area of Farmlot(ha)</td> 
				<td>:</td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $area_farmlot;?></td> 
				<td></td> 
			</tr>
			</tr>
				<tr>
				<td>5. Barangay and Sitio</td> 
				<td>:</td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $barangay.','.$sitio;?></td> 
				<td></td> 
				<td>Location (GPS Reading)</td> 
				<td>:</td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $farmlot_latitude;?></td> 
				<td></td> 
			</tr>
			</tr>
				<tr>
				<td>6. Date Accomplished</td> 
				<td>:</td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $date_accomplished;?></td> 
				<td></td> 
				<td></td> 
				<td></td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $farmlot_longitude;?></td> 
				<td></td> 
			</tr>
			</tr>
				<tr>
				<td>7. Date of Occupancy</td> 
				<td>:</td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $date_occupancy;?></td> 
				<td></td> 
				<td>13. Area of other Uses, if any(ha)</td> 
				<td>:</td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $area_other_uses;?></td> 
				<td></td> 
			</tr>
			</tr>
				<tr>
				<td>8. Household Tag No.</td> 
				<td>:</td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $htn;?></td> 
				<td></td> 
				<td>Location (GPS Reading)</td> 
				<td>:</td> 
				<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $area_other_uses_latitude.', '.$area_other_uses_longitude;?></td> 
				<td></td> 
			</tr>
		</table>  
		<table style="width:100%;margin-top:20px;border-collapse:collapse">
			<tr> 
				<td><h3>14. Demographic Information:</h3></td>
			</tr>
		</table> 
		<table style="width:100%;border-collapse:collapse">
			<thead style="text-align:center;"> 
				<tr> 
					<th style="border:1px solid black;">Name of Household Head</th>
					<th style="border:1px solid black;">Name of Household Members</th>
					<th style="border:1px solid black;">Relationship to Household Head</th>
					<th style="border:1px solid black;">Gender(Male/Female)</th>
					<th style="border:1px solid black;">Age</th>
					<th style="border:1px solid black;">Civil Status</th>
					<th style="border:1px solid black;">Religion</th>
					<th style="border:1px solid black;">Highest Educ. Attaiment</th>
					<th style="border:1px solid black;">Ethnic Origin</th>
					<th style="border:1px solid black;">Remarks</th>
				</tr> 
			</thead>
			<tbody style="text-align:center;"> 
				<?php echo $demographic_content; ?>
			</tbody>
		</table> 
		<span style="width:100%;text-align:left;">Note: All Pages of the form should be signed by the Team Leader</span>
	</body>
</html>