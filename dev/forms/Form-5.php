<?php
$blank_line = '_____________________';
$variable = 'sample';
?>
<html>
	<head>
		<style type="text/css"> 
		</style>
	</head>
	<body> 
		<table style="width:100%">
			<tr>
				<td width="5%" align="left"><img src="" style="width:80px;height:80px"></td>
				<td width="25%" align="left">SRPAO Form 5<br />Series of 2013</td>
				<td width="70%" align="right"><h3>Annex F</h3></td>
			</tr>
		</table>
		<table style="width:100%">
			<tr><td align="center">Republic of the Philippines</tr> 
			<tr><td align="center">SURVEY AND REGISTRATION OF PROTECTED AREA OCCUPANTS</td></tr> 
			<tr><td align="center">Region _____</td></tr>  
		</table> 
		<table style="width:100%;margin-top:20px;"> 
			<tr><td align="center">SURVEY AND REGISTRATION OF PROTECTED AREA OCCUPANTS</td></tr>  
			<tr><td align="center">Form 5</td></tr>  
		</table> 
		<table style="width:100%;margin-top:20px;"> 
			<tr><td align="center">Official List of Tenured Migrants </td></tr>  
		</table>   
		<br />
		<table style="width:100%;margin-top:20px;"> 
			<tr>
				<td align="center">This is to CERTIFY that based on the results of the Survey and Registration of Protected Area Occupants (SRPAO) conducted in accordance<br /></td>
			</tr> 
		</table> 
		
		<table style="width:100%;margin-top:20px;"> 
			<tr><td align="center"> with the Rule 15.2 of DENR Administrative Order No. 2008-26 also known as "Revised Implementing Rules and Regulations of Republic Act No. 7586 or</td></tr> 	
		</table> 
		<table style="width:100%;margin-top:20px;"> 
			<tr><td align="center">the National Integrated Protected Areas System (NIPAS) Act of 1992", the following is the list of tenured migrants of (Name of Protected Area)___________.</br></td></tr> 	
		</table>
		<table style="width:100%;margin-top:20px;"> 
			<tr><td align="center"> ________________________________________________________________________________________________________________________________</br></td></tr> 	
		</table>

		<table style="width:100%;margin-top:40px;border-collapse:collapse">
			<tr>  
				<td width="12%" style="border-bottom:1px solid black;"></td> 
			</tr>
		</table>
		
		<table style="width:100%;margin-top:20px;" >
			<tr style="text-align:center;">  
				<th width="20%" >No. of Household Members</th>
			</tr>		
		</table>
		
		<table style="width:100%;margin-top:1px;border-bottom: 1px solid" >  
			
				<tr style="text-align:center;">  
					<th width="13%" >Name of<br />Household Head</th>
					<td width="1%"></td> 
					<th width="11%" >Spouse Name</th>
					<td width="1%"></td> 
					<th width="11%" >Male</th>
					<td width="1%"></td> 
					<th width="11%" >Female</th>
					<td width="1%"></td> 
					<th width="11%" >Total</th>
					<td width="1%"></td> 
					<th width="11%" >House Tag No.</th>
					<td width="1%"></td> 
					<th width="11%" >Total Area<br />Occupied<br />(Farmlot+<br />Homelot)<br />(ha)</th>
				</tr> 
		</table>  
		<table style="width:100%;margin-top:20px;" >  
			<tr>
				<td width="2%">1.</td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
			</tr>
			<tr>
				<td width="2%">2.</td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
			</tr>
			<tr>
				<td width="2%">3.</td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
			</tr>
			<tr>
				<td width="2%">4.</td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
			</tr>
			<tr>
				<td width="2%">5.</td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
				<td width="1%"></td>
				<td width="11%" style="border-bottom: 1px solid" ></td>
			</tr>
		</table>  
		<table style="width:100%;margin-top:40px;"> 
				<tr><td align="center">This document is NOT a tenurial instrument that would grant security of tenure over their areas being occupied. This document merely</br></td></tr>  
		</table>
		<table style="width:100%;margin-top:20px;"> 
			<tr>
				<td align="center">recognizes that the above persons are qualified as tenure migrants of  ________________________________________________________.<br /></td>
			</tr> 	
		</table>
		<table style="width:100%;margin-top:20px;"> 
			<tr><td align="center"> Issued this_______ day of________________, at___________________________________________________.<br /></td></tr> 	
		</table>
		
		<table style="width:140%;margin-top:50px;"> 
				<tr>  
					<td width="9%" style=" text-align:center; border-bottom:1px solid white;"></td> 
					<td width="10%"></td>
					<td width="13%" style="margin-top:1000px; text-align:center; border-bottom:1px solid black;"></td> 
					<td width="10%"></td> 
					<td width="12%" style="border-bottom:1px solid white;"></td> 
				</tr>
				<tr>  
					<td align="center"></td> 
					<td ></td> 
					<td align="center">Regional Executive Director</td> 
				</tr>
		</table>
	</table>	
	</body>
	</br></br></br>
</html>