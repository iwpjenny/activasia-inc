<?php
$blank_line = '_____________________';
$variable = 'sample';
?>
<html>
	<head>
		<style type="text/css"> 
		</style>
	</head>
	<body> 
		<div id="footer">
			<p class="page" start="9"><?php $PAGE_NUM ?></p>
	   </div>
		<div id="content">
			
			<table style="width:100%">
				<tr><td align="center">DEPARTMENT OF ENVIRONMENT AND NATURAL RESOURCES</td></tr> 
				<tr><td align="center">REGION ____________________________________</td></tr>  
			</table> 
			<table style="width:100%;margin-top:20px;"> 
				<tr><td align="center">SURVEY AND REGISTRATION OF PROTECTED AREA OCCUPANTS</td></tr>  
			</table> 
			<table style="width:100%;margin-top:20px;"> 
				<tr><td align="center">FORM 2</td></tr>  
				<tr><td align="center">Summary per Barangay</td></tr> 
			</table>  
			<table style="width:90%;margin-top:20px;">
				<tr>
					<td width="5%"> Name of Protected Area:</td> 
					<td width="2%"></td> 
					<td width="30%" style="border-bottom:1px solid black; padding-right:100px;"><?php echo $variable;?></td> 
					<td width="4%"></td> 
				</tr>
				<tr>  
					<td> Municipality:</td> 
					<td></td>  
					<td style="margin-right:100px; border-bottom:1px solid black;"><?php echo $variable;?></td> 
					<td></td> 
				</tr>
				</tr>
					<tr>
					<td> Barangay:</td> 
					<td></td> 
					<td style="border-bottom:1px solid black; padding-right: 15px"><?php echo $variable;?></td> 
					<td></td> 
				</tr>
			</table> 
			<h3 style="margin-top:50px; margin-left:25px;"> A. Demographic Information </h3>
			<table style="width:100%;margin-top:10px;border-collapse:collapse">
				<thead style="text-align:center;">  
					<tr> 
						<col>
						</colgroup>
						<th rowspan="2" style="border:1px solid black;">Name of Household Head</th>
						<th colspan="2" scope="colgroup" style="border:1px solid black;">Gender</th>
						<th colspan="3" scope="colgroup" style="border:1px solid black;">Total Household Member</br> (Including household head)</th>
						<th style="border:1px solid black;">Total Area Farmlot(ha)</th>
						<th style="border:1px solid black;">Total Area Homelot(ha)</th>
						<th style="border:1px solid black;">Total Area Occupied(ha)</th>
						<th style="border:1px solid black;">Date of Occupancy</th>
						<th style="border:1px solid black;">Remarks</th>	
					</tr>
					<tr>
						<th scope="col" style="border:1px solid black;">M</th>
						<th scope="col" style="border:1px solid black;">F</th>
						<th scope="col" style="border:1px solid black;">Male</th>
						<th scope="col" style="border:1px solid black;">Female</th>
						<th scope="col" style="border:1px solid black;">Total</th>
						<th scope="col" style="border:1px solid black;"></th>
						<th scope="col" style="border:1px solid black;"></th>
						<th scope="col" style="border:1px solid black;"></th>
						<th scope="col" style="border:1px solid black;"></th>
						<th scope="col" style="border:1px solid black;"></th>
					</tr>
				</thead>
				<tbody style="text-align:center;"> 
					<tr> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
					</tr> 
				</tbody>
			</table> 
			<br></br>
			<span style="width:100%;text-align:left;">Note: All Pages of the form should be signed by the Team Leader</span>
			<div style="page-break-after: always;"></div>
			<table style="width:100%">
				<tr>
					<td width="5%" align="left"><img src="<?php echo base_url('uploads/logo/denr-logo.png');?>" style="width:80px;height:80px"></td>
					<td width="25%" align="left">SRPAO Form 2, page 2 of 2<br />Series of 2013</td>
					<td width="70%" align="right">Annex B, Page 2 of 2</td>
				</tr>
			</table>
			<table style="width:100%;margin-top:20px;"> 
				<tr><td align="center">SURVEY AND REGISTRATION OF PROTECTED AREA OCCUPANTS</td></tr>  
				<tr><td align="center">FORM 2</td></tr>  
			</table> 
			<h3 style="margin-top:50px; margin-left:25px;"> B. Economic/ Livelihood Activities </h3>
			<table style="width:100%;margin-top:10px;border-collapse:collapse">
				<thead style="text-align:center;">  
					<tr> 
						<col>
						</colgroup>
						<th rowspan="4" style="border:1px solid black;">Name of<br /> Household Head</th>
						<th colspan="2" scope="colgroup" style="border:1px solid black;">Timber Products</th>
						<th colspan="2" scope="colgroup" style="border:1px solid black;">Non-timber forest<br />products</th>
						<th colspan="2" scope="colgroup" style="border:1px solid black;">Wildlife (fauna)</br>Collection/Gathering</th>
						<th colspan="2" scope="colgroup" style="border:1px solid black;">Fishery</th>
						<th colspan="2" scope="colgroup" style="border:1px solid black;">Farming</th>
						<th colspan="2" style="border:1px solid black;">Livestock</th>
						<th colspan="2" style="border:1px solid black;">Others</th>
						<th rowspan="4" style="border:1px solid black;">Avg.</br>Income<br />Year</th>
						<th rowspan="4" style="border:1px solid black;">Remarks</th>	
					</tr>
					<tr>
						<th style="border:1px solid black;">Species</th>
						<th scope="col" style="border:1px solid black;">No.</th>
						<th scope="col" style="border:1px solid black;">Total No. <br /> Species</th>
						<th scope="col" style="border:1px solid black;">Area Covered </br>(ha)</th>
						<th scope="col" style="border:1px solid black;">Total No. <br /> Species</th>
						<th scope="col" style="border:1px solid black;">Area Covered </br>(ha)</th>
						<th scope="col" style="border:1px solid black;">Total No. <br /> Species</th>
						<th scope="col" style="border:1px solid black;">Catch Effort</br>(hours Spent)</th>
						<th scope="col" style="border:1px solid black;">Total No. <br /> Species/</br>Crops</th>
						<th scope="col" style="border:1px solid black;">Area<br /> Cultivated(ha)</th>
						<th scope="col" style="border:1px solid black;">Total No.</br>Species/Kind</th>
						<th scope="col" style="border:1px solid black;">Area</br>(ha)</th>
						<th scope="col" style="border:1px solid black;">Total No. Species/</br>Kind/</br>Products</th>
						<th scope="col" style="border:1px solid black;">Area</br>(ha)</th>
					</tr>
				</thead>
				<tbody style="text-align:center;"> 
					<tr> 
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
						<th scope="col" style="border:1px solid black;">Sample</th>
					</tr> 
				</tbody>
			</table> 
			<br></br>
			<table style="width:178%;margin-top:1px;border-collapse:collapse"> 
				<tr> 
					<td><strong>Noted By:<strong></td>
					<td><strong>Attested By:<strong></td>
					<td><strong>Prepared By: Team Members<strong></td>
				</tr>
			</table>
			<table style="width:100%;margin-top:40px;border-collapse:collapse">
				<tr>  
					<td width="9%" style=" text-align:center; border-bottom:1px solid black;"></td> 
					<td width="10%"></td>
					<td width="13%" style=" text-align:center; border-bottom:1px solid black;"></td> 
					<td width="10%"></td> 
					<td width="12%" style="border-bottom:1px solid black;"></td> 
				</tr>
				<tr>  
					<td align="center">PENRO/CENRO</td> 
					<td ></td> 
					<td align="center">Team Leader</td> 
				</tr>
			</table>
			<table style="width:100%;margin-top:20px;border-collapse:collapse">
					<tr>  
						<td width="9%" style=" text-align:center; border-bottom:1px solid white;"></td> 
						<td width="10%"></td>
						<td width="13%" style="margin-top:1000px; text-align:center; border-bottom:1px solid white;"></td> 
						<td width="10%"></td> 
						<td width="12%" style="border-bottom:1px solid black;"></td> 
					</tr>
			</table>
			<table style="width:100%;margin-top:20px;border-collapse:collapse">
					<tr>  
						<td width="9%" style=" text-align:center; border-bottom:1px solid white;"></td> 
						<td width="10%"></td>
						<td width="13%" style="margin-top:1000px; text-align:center; border-bottom:1px solid black;"></td> 
						<td width="10%"></td> 
						<td width="12%" style="border-bottom:1px solid white;"></td> 
					</tr>
					<tr>  
						<td align="center"></td> 
						<td ></td> 
						<td align="center">DATE</td> 
					</tr>
			</table>
			<table style="width:100%;margin-top:0px;border-collapse:collapse">
				<tr>  
					<td width="9%" style=" text-align:center; border-bottom:1px solid white;"></td> 
					<td width="10%"></td>
					<td width="14%" style=" text-align:center; border-bottom:1px solid white;"></td> 
					<td width="9%"></td> 
					<td width="12%" style="border-bottom:1px solid black;"></td> 
				</tr>
				<tr>  
					<td align="center"></td> 
					<td ></td> 
					<td align="center"></td> 
				</tr>
			</table>
			<table style="width:100%;margin-top:30px;border-collapse:collapse">
				<tr>  
					<td width="9%" style=" text-align:center; border-bottom:1px solid white;"></td> 
					<td width="10%"></td>
					<td width="14%" style=" text-align:center; border-bottom:1px solid white;"></td> 
					<td width="9%"></td> 
					<td width="12%" style="border-bottom:1px solid black;"></td> 
				</tr>
				<tr>  
					<td align="center"></td> 
					<td ></td> 
					<td align="center"></td> 
				</tr>
			</table>
		</div>
	</body>
</html>