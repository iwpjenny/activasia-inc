<?php
$blank_line = '_____________________';
$variable = 'sample';
?>
<html>
	<head>
		<style type="text/css"> 
		</style>
	</head>
	<body> 
	
		<table style="width:100%">
			<tr>
				<td width="5%" align="left"><img src="" style="width:80px;height:80px"></td>
				<td width="25%" align="left">SRPAO Form 2, page 2 of 2<br />Series of 2013</td>
				<td width="70%" align="right">Annex B, Page 2 of 2</td>
			</tr>
		</table>
		<table style="width:100%;margin-top:20px;"> 
			<tr><td align="center">SURVEY AND REGISTRATION OF PROTECTED AREA OCCUPANTS</td></tr>  
			<tr><td align="center">FORM 2</td></tr>  
		</table> 
		<h3 style="margin-top:50px; margin-left:25px;"> B. Economic/ Livelihood Activities </h3>
		<table style="width:100%;margin-top:10px;border-collapse:collapse">
			<thead style="text-align:center;">  
				<tr> 
					<col>
					</colgroup>
					<th rowspan="4" style="border:1px solid black;">Name of<br /> Household Head</th>
					<th colspan="2" scope="colgroup" style="border:1px solid black;">Timber Products</th>
					<th colspan="2" scope="colgroup" style="border:1px solid black;">Non-timber forest<br />products</th>
					<th colspan="2" scope="colgroup" style="border:1px solid black;">Wildlife (fauna)</br>Collection/Gathering</th>
					<th colspan="2" scope="colgroup" style="border:1px solid black;">Fishery</th>
					<th colspan="2" scope="colgroup" style="border:1px solid black;">Farming</th>
					<th colspan="2" style="border:1px solid black;">Livestock</th>
					<th colspan="2" style="border:1px solid black;">Others</th>
					<th rowspan="4" style="border:1px solid black;">Avg.</br>Income<br />Year</th>
					<th rowspan="4" style="border:1px solid black;">Remarks</th>	
				</tr>
				<tr>
					<th style="border:1px solid black;">Species</th>
					<th scope="col" style="border:1px solid black;">No.</th>
					<th scope="col" style="border:1px solid black;">Total No. <br /> Species</th>
					<th scope="col" style="border:1px solid black;">Area Covered </br>(ha)</th>
					<th scope="col" style="border:1px solid black;">Total No. <br /> Species</th>
					<th scope="col" style="border:1px solid black;">Area Covered </br>(ha)</th>
					<th scope="col" style="border:1px solid black;">Total No. <br /> Species</th>
					<th scope="col" style="border:1px solid black;">Catch Effort</br>(hours Spent)</th>
					<th scope="col" style="border:1px solid black;">Total No. <br /> Species/</br>Crops</th>
					<th scope="col" style="border:1px solid black;">Area<br /> Cultivated(ha)</th>
					<th scope="col" style="border:1px solid black;">Total No.</br>Species/Kind</th>
					<th scope="col" style="border:1px solid black;">Area</br>(ha)</th>
					<th scope="col" style="border:1px solid black;">Total No. Species/</br>Kind/</br>Products</th>
					<th scope="col" style="border:1px solid black;">Area</br>(ha)</th>
				</tr>
			</thead>
			<tbody style="text-align:center;"> 
				<tr> 
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
					<th scope="col" style="border:1px solid black;">Sample</th>
				</tr> 
			</tbody>
		</table> 
		<br></br>
		<table style="width:178%;margin-top:1px;border-collapse:collapse"> 
			<tr> 
				<td><strong>Noted By:<strong></td>
				<td><strong>Attested By:<strong></td>
				<td><strong>Prepared By: Team Members<strong></td>
			</tr>
		</table>
		<table style="width:100%;margin-top:40px;border-collapse:collapse">
			<tr>  
				<td width="9%" style=" text-align:center; border-bottom:1px solid black;"></td> 
				<td width="10%"></td>
				<td width="13%" style=" text-align:center; border-bottom:1px solid black;"></td> 
				<td width="10%"></td> 
				<td width="12%" style="border-bottom:1px solid black;"></td> 
			</tr>
			<tr>  
				<td align="center">PENRO/CENRO</td> 
				<td ></td> 
				<td align="center">Team Leader</td> 
			</tr>
		</table>
		<table style="width:100%;margin-top:20px;border-collapse:collapse">
				<tr>  
					<td width="9%" style=" text-align:center; border-bottom:1px solid white;"></td> 
					<td width="10%"></td>
					<td width="13%" style="margin-top:1000px; text-align:center; border-bottom:1px solid white;"></td> 
					<td width="10%"></td> 
					<td width="12%" style="border-bottom:1px solid black;"></td> 
				</tr>
		</table>
		<table style="width:100%;margin-top:20px;border-collapse:collapse">
				<tr>  
					<td width="9%" style=" text-align:center; border-bottom:1px solid white;"></td> 
					<td width="10%"></td>
					<td width="13%" style="margin-top:1000px; text-align:center; border-bottom:1px solid black;"></td> 
					<td width="10%"></td> 
					<td width="12%" style="border-bottom:1px solid white;"></td> 
				</tr>
				<tr>  
					<td align="center"></td> 
					<td ></td> 
					<td align="center">DATE</td> 
				</tr>
		</table>
		<table style="width:100%;margin-top:0px;border-collapse:collapse">
			<tr>  
				<td width="9%" style=" text-align:center; border-bottom:1px solid white;"></td> 
				<td width="10%"></td>
				<td width="14%" style=" text-align:center; border-bottom:1px solid white;"></td> 
				<td width="9%"></td> 
				<td width="12%" style="border-bottom:1px solid black;"></td> 
			</tr>
			<tr>  
				<td align="center"></td> 
				<td ></td> 
				<td align="center"></td> 
			</tr>
		</table>
		<table style="width:100%;margin-top:30px;border-collapse:collapse">
			<tr>  
				<td width="9%" style=" text-align:center; border-bottom:1px solid white;"></td> 
				<td width="10%"></td>
				<td width="14%" style=" text-align:center; border-bottom:1px solid white;"></td> 
				<td width="9%"></td> 
				<td width="12%" style="border-bottom:1px solid black;"></td> 
			</tr>
			<tr>  
				<td align="center"></td> 
				<td ></td> 
				<td align="center"></td> 
			</tr>
		</table>
	</body>
</html>