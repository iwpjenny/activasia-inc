/* Alphabet only */
$(document).ready(function(){
	$(".alphaonly").keypress(function(event){
		var inputValue = event.which;
		if((inputValue > 47 && inputValue < 58) && (inputValue != 32)){
			event.preventDefault();
		}
	});
});

/* White Space */
$(document).ready(function() {
	$(".js-required-fields").on("click", function(){
		var form = $(this).parents('form:first');
		var flag = true;
		
		$(".alert").hide();
		
		$(form).find("input[required]:visible").each(function(e) {
						
			var inputValue = $.trim($(this).val());

			if (inputValue === ''){
				var form_group = $(this).parents("div.form-group:first");
				$(form_group).addClass("has-warning");
				flag = false;
			}
		});	

		if( flag == false ){
			$("div#notify").after('<div class="alert alert-warning alert-dismissable">Warning, Please fill-up the required highlighted fields.</div>');

			return false;
		}
	});		
});
 
$(document).on("change", '.select_area_parent', function(){ 
	var id = $(this).val(); 
	
	 if( $('#check_box_parent_'+id).is(':checked') == true ){
		$('.area_child_'+id ).prop('checked',true);
		 $('.area_child_'+id).removeAttr("disabled");
	 } else {
		$('.area_child_'+id ).prop('checked',false);
		 $('.area_child_'+id).attr("disabled", true);
	 }
});
 
